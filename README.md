# README #

### What is this repository for? ###

OneAIM/Heart is an in-house built web based application for HSSE.  
See the links below for general information and see the “Introduction” PowerPoint linked from those pages.

OneAIM page on OneSpace
https://amec.sharepoint.com/sites/onespace/HSSE/Pages/oneAim.aspx

HEART page on OneSpace
https://amec.sharepoint.com/sites/onespace/HSSE/Pages/HEART.aspx

### How do I get set up? ###

Environment:
Web server:  Windows 2012R2 with IIS, ColdFusion v 11 (Standard is sufficient) Fusion Charts
Database server:  Windows 2012R2, MSSQL 2012R2

Adobe ColdFusion Support Site:
https://helpx.adobe.com/support/coldfusion.html

Here’s the link for ColdFusion online training for the development resource.  
www.learncfinaweek.com

### Contribution guidelines ###

Please see the ColdFusion Installation Guide for oneAIM and Heart.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact