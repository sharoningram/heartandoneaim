<cfparam name="OBNum" default="0">
<cfparam name="submittype" default="">
<cfparam name="restorecomments" default="">
<cfparam name="currstatus" default="">
<cfif trim(submittype) neq '' and OBNum gt 0>
	<cfswitch expression="#submittype#">
		<cfcase value="restore">
		<cftransaction>
			<cfquery name="addcomm" datasource="#request.HEART_DS#">
					insert into ObservationRestoreComments(ObservationNumber, Comment, DateEntered, EnteredBy)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obnum#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#restorecomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">)
			</cfquery>
			
			
			<cfquery name="addauditentry" datasource="#request.heart_ds#">
					insert into ObservationAudits (ObservationNumber, ToStatus, CompletedBy, CompletedDate, SentTo, ActionTaken, FromStatus, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obnum#">,'Submitted for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, '','Observation restored','Escalated to oneAIM',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Observation restored and withdrawn by #request.fname# #request.lname#">)
			</cfquery>
			
		<!--- 	<cfquery name="withdrawobs" datasource="#request.HEART_DS#">
					update Observations
					set Status = 'Submitted',
					withdrawnto = 1, 
					Escalateoneaim = 0
					where ObservationNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obnum#">
			</cfquery> --->
			
			
			
			
			
			
			
			
			
			
			
				<cfquery name="getEmailDetail" datasource="#request.HEART_DS#">
							SELECT   Group_Name,  SiteName,Business_Line_ID,OUid
								FROM    [#oneaimSQLname#].[dbo].[Groups],[#oneaimSQLname#].[dbo].[GroupLocations]
								WHERE 
								Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getObservationDetail.ProjectNumber#">
							and	GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getObservationDetail.SiteID#">
						</cfquery>
						
						<cfset cclist = "">
						<cfset revnames = "">
						<cfquery name="getreviewer" datasource="#request.HEART_DS#">
	SELECT DISTINCT Users.Useremail, Users.Firstname, Users.Lastname
FROM            #oneaimSQLname#.dbo.Users INNER JOIN
                         #oneaimSQLname#.dbo.UserRoles ON Users.UserId = #oneaimSQLname#.dbo.UserRoles.UserID INNER JOIN
                         #oneaimSQLname#.dbo.LocationAdvisors ON #oneaimSQLname#.dbo.Users.UserId = #oneaimSQLname#.dbo.LocationAdvisors.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'HSSE Advisor') AND (LocationAdvisors.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getObservationDetail.SiteID#">)
					</cfquery>  
						<!--- <cfquery name="getreviewer" datasource="#request.HEART_DS#">
							SELECT       Users.Useremail,Users.FirstName,Users.LastName
							FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
						[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
							WHERE     Users.status = 1 and   (UserRoles.UserRole = 'HSSE Advisor') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getEmailDetail.ouid#">)
						</cfquery> --->
						
						
						
						
						<cfif getreviewer.RecordCount GT 0>
							<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
							<cfloop query="getreviewer">
								<cfset revnames = listappend(revnames,"#firstname# #lastname#")>
							</cfloop>
						<cfelse>
							<cfquery name="getreviewer" datasource="#request.HEART_DS#">
								SELECT       Users.Useremail,Users.FirstName,Users.LastName
								FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
						[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
								WHERE     Users.status = 1 and   (UserRoles.UserRole = 'OU Admin') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getEmailDetail.ouid#">)
							</cfquery>
							
							<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
							<cfloop query="getreviewer">
								<cfset revnames = listappend(revnames,"#firstname# #lastname#")>
							</cfloop>
						</cfif>
						
				
<cfquery name="getEmailDetail" datasource="#request.HEART_DS#">
SELECT   Group_Name,  SiteName,Business_Line_ID,OUid
		FROM    [#oneaimSQLname#].[dbo].[Groups],[#oneaimSQLname#].[dbo].[GroupLocations]
		WHERE 
		Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#getObservationDetail.ProjectNumber#>
	and	GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#getObservationDetail.SiteID#>
</cfquery>
<cfset SubjectLine= "HEART - "&#getObservationDetail.TrackingYear#&#numberformat(getObservationDetail.TrackingNum, "00000")#&" - "&#getObservationDetail.ObservationBU#&" - "&#getObservationDetail.ObservationType#> 
	
	
	
			<cfquery name="withdrawobs" datasource="#request.HEART_DS#">
					update Observations
					set withdrawnto = 1, 
					withdrawndate = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, 
					returnTo = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cclist#">, 
					withdrawnby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
					where ObservationNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obnum#">
				</cfquery>
<cfif trim(cclist) neq '' and cclist neq "No OU Admin for OU">
	<cfmail to="#cclist#" from="#HeartEmail#" type="html" subject="#SubjectLine# - Observation withdrawn for amendment">
											
			<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by HEART.<br>  
The following observation is no longer escalated to oneAIM and has been withdrawn for amendment by #request.fname# #request.lname#.  <br>
Please go to HEART via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.heartPath#/index.cfm?fuseaction=main.ViewObservation&OBNum=#obnum#">###getObservationDetail.TrackingYear##numberformat(getObservationDetail.TrackingNum, "00000")#</a>
<br><Br></span>			
			<table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167">
														<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Comments:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#restorecomments#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Event Type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getObservationDetail.ObservationType#</td>
															</tr>
													<cfif  getObservationDetail.ObservationType NEQ 'Safe Behaviour' >		
														<cfquery name="getSafetyRule" datasource="#request.HEART_DS#">
														SELECT SafetyRule FROM [#oneaimSQLname#].[dbo].[SafetyRules]
														WHERE SafetyRuleID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#getObservationDetail.GlobalSafetyRules#>
														</cfquery>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation Category:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getSafetyRule.SafetyRule#</td>
															</tr>
													</cfif>	
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getObservationDetail.DateofObservation,sysdateformat)# #Trim(getObservationDetail.TimeofObservation)#</td>
															</tr>
													
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"> #getObservationDetail.Observations#</td>
															</tr>
															
													
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.bulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getObservationDetail.ObservationBU#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getObservationDetail.ObservationOU#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getObservationDetail.BusinessStream#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"> #getEmailDetail.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getEmailDetail.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Stakeholder:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getObservationDetail.PersonnelCategory#</td>
															</tr>
														</table>
														
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">
														For further details on this observation please go to HEART via this link - <a href="#starthttp#://#http_host#/#getappconfig.heartPath#/index.cfm?fuseaction=main.ViewObservation&OBNum=#obnum#">Record ###getObservationDetail.TrackingYear##numberformat(getObservationDetail.TrackingNum, "00000")#</a><br><br>
														Please do not reply to this email address. </span>
	</cfmail>	 
</cfif>
			
			
			
			
			
			</cftransaction>
			
			
			<cfoutput>
				<script type="text/javascript">
					// window.opener.location.assign("index.cfm?fuseaction=main.ReviewObservation&OBNum=#OBnum#");
					window.opener.location.assign("index.cfm?fuseaction=main.main");
					window.close();
				</script>
			</cfoutput>
			
		</cfcase>
	</cfswitch>
</cfif>