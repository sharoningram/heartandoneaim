

<cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "act_observationpdf.cfm", ""))>	
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "main\actions\act_observationpdf.cfm", "reports\exports"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "Observation_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes">
	
<cfoutput query="getObservationDetail">	
<style>
.purplebg {
background: ##5f2167;

}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	color: ##000000;
	font-style: normal;
}
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: ##FFFFFF;
	font-style: normal;
}
.bodytextgrey{
font-family: Segoe UI;
	font-size: 10pt;
	color: ##5f5f5f;
	font-style: normal;
	}
.bodyTextGreyresize{
background-color:##ffffff;
	font-family:Segoe UI;
	color: ##5f5f5f;
	font-size:10pt;
	width: 65%;
	 float: left;
text-align: left;
padding-top: 2px;
padding-left: 2px;
padding-bottom: 500em;
  margin-bottom: -500em;
display:block;

	}
.formlablelt{
background-color:##fdf8ff;
font-family: Segoe UI;
	font-size: 10pt;
	color: ##5f5f5f;
	font-style: normal;
}
.formlable{
background-color:##f7f1f9;
font-family: Segoe UI;
	font-size: 10pt;
	color: ##5f5f5f;
	font-style: normal;
}
.formlableresize{
	background-color:##f7f1f9;
	color: ##5f5f5f;
	font-family:Segoe UI;
	font-size:10pt;
	width: 34%;
	
	 float: left;
text-align: left;
padding-top: 2px;
padding-left: 2px;
padding-bottom: 500em;
  margin-bottom: -500em;
display:block;
clear: left;

}
</style>
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
		<tr>
			<td align="left" bgcolor="ffffff">
			
				<table cellpadding="0" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
							
							<tr>
								<td width="35%" class="formlable"><strong>Originator:</strong></td>
								<td width="65%" class="bodytextgrey">#CreatedBy#</td>
							</tr>
							<tr>		
								<td width="35%" class="formlable"><strong>Date Created:</strong></td>
								<td width="65%" class="bodytextgrey">#dateformat(DateCreated,"dd-mmm-yyyy")#</td>																
							</tr>
							<tr>
								<td width="35%" class="formlable"><strong>#request.bulabellong#:</strong></td>
								<td width="65%" class="bodytextgrey">#ObservationBU#</td>	
							</tr>
							<tr>								
								<td width="35%" class="formlable"><strong>#request.oulabellong#:</strong></td>
								<td width="65%" class="bodytextgrey">#ObservationOU#</td>							
							</tr>
							<cfif trim(getObservationDetail.cohabitOU) neq '' and trim(getObservationDetail.cohabitOU) gt 0 and isdefined("getcohab.recordcount") and listlen(getObservationDetail.cohabitOU) lt 2>
							<tr>								
								<td width="35%" class="formlable"><strong>Your Office or the #request.oulabellong# with whom you cohabit:</strong></td>
								<td width="65%" class="bodytextgrey">#getcohab.name#</td>							
							</tr>
							</cfif>
							<tr>
								<td width="35%" class="formlable"><strong>Project/Office:</strong></td>
								<td width="65%" class="bodytextgrey">#Group_Name#</td>
							</tr>
						<cfif BusinessStream NEQ ''>
							<tr>	
								<td width="35%" class="formlable"><strong>Business Stream:</strong></td>
								<td width="65%" class="bodytextgrey">#BusinessStream#</td>
							</tr>
						</cfif>	
							
						<tr>
							<td width="35%" class="formlable"><strong>Site/Office Name:</strong></td>
								<td width="65%" class="bodytextgrey">#SiteName#</td>
						
						</tr>												
						<tr>
							<td width="35%" class="formlable"><strong>Exact Location of Act/Condition/Safe Behaviour:</strong></td>
								<td width="65%" class="bodytextgrey">#ExactLocation#</td>
						</tr>
						
						<tr>
							<td width="35%" class="formlable"><strong>EventType:</strong></td>
								<td width="65%" class="bodytextgrey">#ObservationType#</td>											
						</tr>
						
						<tr>
							<td width="35%" class="formlable"><strong>Observer Name:</strong></td>
								<td width="65%" class="bodytextgrey">#ObserverName#</td>
						</tr>
						<tr>	
							<td width="35%" class="formlable"><strong>Observer Email Address:</strong></td>
								<td width="65%" class="bodytextgrey">#ObserverEmail#</td>
						</tr>		
						
						<tr>
							<td width="35%" class="formlable"><strong>Stakeholder:</strong></td>
								<td width="65%" class="bodytextgrey">#PersonnelCategory#</td>															
						</tr>
						
						<tr>
							<td width="35%" class="formlable"><strong>Observation Date:</strong></td>
								<td width="65%" class="bodytextgrey">#dateformat(DateofObservation,"dd-mmm-yyyy")#</td>
						</tr>
						<tr>	
							<td width="35%" class="formlable"><strong>Observation Time:</strong></td>
								<td width="65%" class="bodytextgrey">#TimeofObservation#</td>
						</tr>
						
						
						
						<cfif ObservationType EQ 'Safe Behaviour'>	
						<tr>
							<td width="35%" class="formlable"><strong>Name of Individual or Team:</strong></td>
								<td width="65%" class="bodytextgrey">#TeamName#</td>	
						</tr>
						</cfif>		
<cfif  ObservationType NEQ 'Safe Behaviour' >
						<tr>
							<td width="35%" class="formlable"><strong>Global Safety Rules:</strong></td>
								<td width="65%" class="bodytextgrey">#SafetyRule#</td>
						</tr>
							<cfif GlobalSafetyRules EQ 11>
						</tr>	
							<td width="35%" class="formlable"   id="brnonelbl"><strong>If none, why?:</strong></td>
								<td width="65%" class="bodytextgrey" id="brnonefld">#GlobalSafetyWhyNone#</td>
						</tr>	
							<cfelseif GlobalSafetyRules EQ 15>
						<tr>	
							<td width="35%" class="formlable"><strong>Other:</strong></td>
								<td width="65%" class="bodytextgrey">#GlobalSafetyOther#</td>
						</tr>	
							</cfif>
						
		
					<tr>
						<td width="35%" class="formlable"><strong>Safety Essential:</strong></td>
								<td width="65%" class="bodytextgrey">#SafetyEssDesc#</td>
					</tr>	
						<cfif SafetyEssential EQ 7>
					<tr>	
						<td width="35%" class="formlable"><strong>If none, why?:</strong></td>
								<td width="65%" class="bodytextgrey">#SafetyEssentialNone#</td>
					</tr>	
					
					</cfif>
				
</cfif>						
				<tr>
					<td width="35%" class="formlable"  id="brnonelbl"><strong>Details of safety observation:</strong></td>
								<td width="65%" class="bodytextgrey">#Observations#</td>
				</tr>		
				
				<tr>
					<td width="35%" class="formlable" id="brnonelbl"><strong>Immediate action taken/recommended:</strong></td>
								<td width="65%" class="bodytextgrey">#FollowUpActions#</td>
				</tr>	
			
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "Main\actions\act_ObservationPDF.cfm", "uploads\Observations\#OBNum#"))>		
				<tr>
					<td width="35%" class="formlable" id="brnonelbl"><strong>Supporting Information:</strong></td>
								<td width="65%" class="bodytextgrey">
				<cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getObsvfileslist">
							<cfif getObsvfileslist.recordcount gt 0>
								
								<cfloop query="getObsvfileslist">
									<!--- <a href="#self#?fuseaction=incidents.incidentmanament&submittype=deleteincfile&filename=#name#&Obs=#Obs#" onclick="return confirm('Are you sure you want to delete this file?');"><img src="images/trash.gif" border="0"></a> --->&nbsp;#name#<br>
								</cfloop>	
							
							</cfif>
						</cfif>
					
					
			</td>
				</tr>
			
			  <tr>
					<td width="35%" class="formlable"  id="brnonelbl"><strong>Feedback required?:</strong></td>
								<td width="65%" class="bodytextgrey"><cfif FeedBackRequired eq 1>Yes<cfelse>No</cfif></td>
												
				</tr> 
				
					<!--- 1 Reviewer comments--->
		<cfif FurtherActions NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from Reviewer</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 
						  <tr>
							<td width="35%" class="formlable"><strong>Action taken/Recommended:</strong></td>
								<td width="65%" class="bodytextgrey">#FurtherActions#</td>		
						</tr>	
					</table>	
				</td>	
			</tr>
		</cfif>
		<!--- 2  Reviewer comments --->
		<cfif status EQ 'Closed' and FurtherActions EQ '' and CloseComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from Reviewer</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 		 <tr>
							<td width="35%" class="formlable"><strong>Closing Comments:</strong></td>
								<td width="65%" class="bodytextgrey">#CloseComments#</td>		
						</tr>	
					</table>	
				</td>	
			</tr>
		</cfif>
		<!--- 3 Reviewer comments --->
		<cfif status EQ 'Cancelled' and FurtherActions EQ '' and CancelComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from Reviewer</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 		<tr>
							<td width="35%" class="formlable"><strong>Cancel Comments:</strong></td>
								<td width="65%" class="bodytextgrey">#CancelComments#</td>		
						</tr>	
						 
					</table>	
				</td>	
			</tr>
		</cfif>
		
		<!--- 4 OU Admin comments --->
		<cfif status EQ 'Cancelled' and FurtherActions NEQ '' and CancelComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from OU Admin</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 		<tr>
							<td width="35%" class="formlable"><strong>Cancel Comments:</strong></td>
								<td width="65%" class="bodytextgrey">#CancelComments#</td>		
						</tr>	
						 
					</table>	
				</td>	
			</tr>
		</cfif>
		
		<!--- 5 OU Admin comments--->
		<cfif status EQ 'Closed' and FurtherActions NEQ '' and CloseComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from OU Admin</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 	<tr>
							<td width="35%" class="formlable"><strong>Closing Comments:</strong></td>
								<td width="65%" class="bodytextgrey">#CloseComments#</td>		
						</tr>	
						
					</table>	
				</td>	
			</tr>
		</cfif>
		
		
				</table>
			</td>
		</tr>
		<cfif getrestorecomms.recordcount gt 0>
	<tr >
	<td><strong class="bodytextwhite">&nbsp;Restore Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getrestorecomms">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
	
	
	</cfif>	
	<cfif getamendcomms.recordcount gt 0>
	<tr >
	<td><strong class="bodytextwhite">&nbsp;Reasons for Amendment</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getamendcomms">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
	
	
	</cfif>	
		
	
</table>

<!--- 
 <cfif directoryexists(filedir)>
	<cfdirectory action="LIST" directory="#filedir#" name="getObsvfileslist">
		<cfif getObsvfileslist.recordcount gt 0>
			
			<cfloop query="getObsvfileslist">
				<cfif isImageFile("#filedir#\#name#")>
					<img src="/#getappconfig.heartPath#/uploads/Observations/#OBNum#/#name#" ><br>
				
				</cfif>
				
			</cfloop>	
		
		</cfif>
	</cfif> --->

 
</cfoutput>
 </cfdocument>
 
 
 
 <cfif directoryexists(filedir)>
	<cfdirectory action="LIST" directory="#filedir#" name="getObsvfileslist">
		<cfif getObsvfileslist.recordcount gt 0>
			<cfset ctr = 0>
			<cfloop query="getObsvfileslist">
			<cfset ctr = ctr+1>
				<cfif not isImageFile("#filedir#\#name#")>
					
						<cfif right(name,4) eq ".pdf">
							<cfpdf action="merge" destination="#PDFFileDir#\#fnamepdf#" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="#PDFFileDir#\#fnamepdf#">
    								<cfpdfparam source="#filedir#\#name#">
 							</cfpdf>
						<cfelse>
						
							<cfdocument 
    							 format="pdf" 
    							 srcfile="#filedir#\#name#" 
    							 filename="attc_#getObservationDetail.ObservationNumber#_#ctr#.pdf"
	  							overwrite="yes"> 
 							</cfdocument> 
							<cfpdf action="merge" destination="#PDFFileDir#\#fnamepdf#" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="#PDFFileDir#\#fnamepdf#">
    								<cfpdfparam source="attc_#getObservationDetail.ObservationNumber#_#ctr#.pdf">
 							</cfpdf>
							<cffile action="DELETE" file="#deldir#attc_#getObservationDetail.ObservationNumber#_#ctr#.pdf">
						
						</cfif>
				<cfelse>
				<cfdocument 
    							 format="pdf" 
    							filename="attc_#getObservationDetail.ObservationNumber#_#ctr#.pdf"
	  							overwrite="yes"><cfoutput><img src="#starthttp#://#http_host#/#getappconfig.heartPath#/uploads/Observations/#OBNum#/#name#" border="0"></cfoutput></cfdocument>
								<cfpdf action="merge" destination="#PDFFileDir#\#fnamepdf#" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="#PDFFileDir#\#fnamepdf#">
    								<cfpdfparam source="attc_#getObservationDetail.ObservationNumber#_#ctr#.pdf">
 							</cfpdf>
							<cffile action="DELETE" file="#deldir#attc_#getObservationDetail.ObservationNumber#_#ctr#.pdf"> 
				</cfif>
				
			</cfloop>	
		
		</cfif>
	</cfif>
 
 
 
<cflocation url="/#getappconfig.heartPath#/reports/exports/#fnamepdf#" addtoken="No">

