<!--- Determine who observation is pending action by --->
<cfset nexttoaction = "">
<!--- 
		SELECT       Users.Useremail,Users.FirstName,Users.LastName
		FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
		WHERE     Users.status = 1 and   (UserRoles.UserRole = 'HSSE Advisor') AND (UserRoles.AssignedLocs = (select ouid from #oneaimSQLname#.dbo.groups where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">)) --->
<cfif getobservationdetail.status eq "Submitted">
	<cfquery name="getreviewer" datasource="#request.dsn#">
		SELECT    DISTINCT     Users.Useremail, Users.Firstname, Users.Lastname
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID INNER JOIN
                         LocationAdvisors ON Users.UserId = LocationAdvisors.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'HSSE Advisor') AND (LocationAdvisors.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.siteid#">)
	</cfquery> 
	<!--- <cfquery name="getreviewer" datasource="#request.dsn#">
		SELECT       Users.Useremail,Users.FirstName,Users.LastName
		FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
		WHERE     Users.status = 1 and   (UserRoles.UserRole = 'HSSE Advisor') AND (UserRoles.AssignedLocs = (select ouid from #oneaimSQLname#.dbo.groups where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">))
	</cfquery> --->
	<cfloop query="getreviewer">
		<cfif listfindnocase(nexttoaction,"#firstname# #lastname#") eq 0>
			<cfset nexttoaction = listappend(nexttoaction,"#firstname# #lastname#")>
		</cfif>
	</cfloop>
	<cfif getreviewer.recordcount eq 0>
		<cfquery name="getreviewer" datasource="#request.HEART_DS#">
			SELECT  DISTINCT      Users.Useremail,Users.FirstName,Users.LastName
			FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
			WHERE     Users.status = 1 and   (UserRoles.UserRole = 'OU Admin') AND (UserRoles.AssignedLocs = (select ouid from #oneaimSQLname#.dbo.groups where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">))
		</cfquery>
		<cfloop query="getreviewer">
			<cfif listfindnocase(nexttoaction,"#firstname# #lastname#") eq 0>
				<cfset nexttoaction = listappend(nexttoaction,"#firstname# #lastname#")>
			</cfif>
		</cfloop>
	
	</cfif>
<cfelseif getobservationdetail.status eq "Reviewed">
	<cfquery name="getOUAdmin" datasource="#request.HEART_DS#">
		SELECT    DISTINCT    Users.Useremail,Users.FirstName,Users.LastName
		FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
		WHERE     Users.status = 1 and   (UserRoles.UserRole = 'OU Admin') AND (UserRoles.AssignedLocs = (select ouid from #oneaimSQLname#.dbo.groups where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">))
	</cfquery>
	
	<cfloop query="getOUAdmin">
		<cfif listfindnocase(nexttoaction,"#firstname# #lastname#") eq 0>
			<cfset nexttoaction = listappend(nexttoaction,"#firstname# #lastname#")>
		</cfif>
	</cfloop>
	
	
	
</cfif>

<!--- Build query object to return who observation is accessible by (global admin, bu admin, ou admin, initiator --->
<cfset ctr = 1>
<cfset viewerlist = arraynew(2)>

<cfquery name="getglbadmin" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
	FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
	WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
</cfquery>
<cfloop query="getglbadmin">
	<cfset viewerlist[ctr][1] = UserRole>
	<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
	<cfset ctr = ctr + 1>
</cfloop>

<cfquery name="getbuadmin" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
	FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
	WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">)))
</cfquery>
<cfloop query="getbuadmin">
	<cfset viewerlist[ctr][1] = UserRole>
	<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
	<cfset ctr = ctr + 1>
</cfloop>
	
<cfquery name="getouadmin" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
	FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
	WHERE        (UserRoles.UserRole = 'ou admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        ouid
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">)))
</cfquery>
<cfloop query="getouadmin">
	<cfset viewerlist[ctr][1] = UserRole>
	<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
	<cfset ctr = ctr + 1>
</cfloop>
	
<cfif listfindnocase("Submitted,Closed,Cancelled",getobservationdetail.status) gt 0>
<!--- <cfquery name="gethsseadv" datasource="#request.HEART_DS#">
		SELECT       Users.Useremail,Users.FirstName,Users.LastName, UserRoles.UserRole
		FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
		WHERE     Users.status = 1 and   (UserRoles.UserRole = 'HSSE Advisor') AND (UserRoles.AssignedLocs = (select ouid from #oneaimSQLname#.dbo.groups where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.projectnumber#">))
</cfquery> --->

<cfquery name="gethsseadv" datasource="#request.dsn#">
		SELECT   DISTINCT      Users.Useremail, Users.Firstname, Users.Lastname
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID INNER JOIN
                         LocationAdvisors ON Users.UserId = LocationAdvisors.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'HSSE Advisor') AND (LocationAdvisors.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.siteid#">)
	</cfquery>

	<cfloop query="gethsseadv">
	<cfset viewerlist[ctr][1] = "HSSE Advisor">
	<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
	<cfset ctr = ctr + 1>
</cfloop>
</cfif>

<cfif getobservationdetail.status eq "Reviewed">

<!--- <cfquery name="getadvisor" datasource="#request.HEART_DS#">
		SELECT   distinct    Users.Useremail,Users.FirstName,Users.LastName, UserRoles.UserRole
		FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
		WHERE     Users.status = 1 and   (UserRoles.UserRole = 'HSSE Advisor') and Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getobservationdetail.updatedby#">
</cfquery> --->
<cfquery name="getadvisor" datasource="#request.dsn#">
		SELECT     DISTINCT    Users.Useremail, Users.Firstname, Users.Lastname
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID INNER JOIN
                         LocationAdvisors ON Users.UserId = LocationAdvisors.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'HSSE Advisor') AND (LocationAdvisors.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getobservationdetail.siteid#">)
	</cfquery>
<cfloop query="getadvisor">
	<cfset viewerlist[ctr][1] = "HSSE Advisor">
	<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
	<cfset ctr = ctr + 1>
</cfloop>

</cfif>

<cfset viewerlist[ctr][1] = "Initiator">
<cfset viewerlist[ctr][2] = "#getobservationdetail.createdby#">
<cfset ctr = ctr + 1>
		
<cfset getwhocansee = QueryNew("userrole, name")>
<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
	<cfset QueryAddRow( getwhocansee ) />
	<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
	<cfset getwhocansee["name"][i] = viewerlist[i][2]>
</cfloop>


<cfif getobservationdetail.withdrawnto eq 1>
<cfif isvalid("email",getobservationdetail.withdrawnby)>
	<cfset nexttoaction = replace(listgetat(getobservationdetail.withdrawnby,1,"@"),"."," ","all")>
<cfelse>
	<cfset nexttoaction = getobservationdetail.withdrawnby>
</cfif>
</cfif>