<cfparam name="gnum" default="0">
<cfparam name="sitenum" default="0">
<cfparam name="currloc" default="">
<cfparam name="OperatingUnit" default="0">
<cfparam name="Project" default="0">
<cfparam name="ProjectBS" default="0">
<cfparam name="cohabitOU" default="0">
<cfparam name="cOU" default="0">
<cfparam name="pOU" default="0">

<cfif cOU EQ "">
	<cfset cOU=0>
</cfif>
<cfif pOU EQ "">
	<cfset pOU=0>
</cfif>
<cfif cohabitOU EQ "">
	<cfset cohabitOU=0>
</cfif>

<cfif OperatingUnit EQ "">
	<cfset OperatingUnit=0>
</cfif>

<cfif Project EQ "">
	<cfset Project=0>
</cfif>

<cfif ProjectBS EQ "">
	<cfset ProjectBS=0>
</cfif>

<cfif OperatingUnit gt 0>
<cfquery name="getOUs" datasource="#request.dsn#">
SELECT    NewDials.ID, NewDials.Name, NewDials.Parent, NewDials.isgroupdial, NewDials.GroupNumber, NewDials.businessline,NewDials.ViewOnDashboard AS active_group
FROM   NewDials 
WHERE (NewDials.status <> 99) AND (NewDials.isgroupdial <> 1) 
AND (NewDials.Parent IN
(SELECT   ID FROM  NewDials AS NewDials_1
WHERE        (status <> 99) AND (isgroupdial <> 1) AND (Parent = 2707)))
AND NewDials.Parent =<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OperatingUnit#">
ORDER BY NewDials.businessline, NewDials.ID 

</cfquery>

{kk{<select name="OperatingUnit" size="1" class="selectgen" id="lrgselect"  style="width:200px" onchange="populatelistProject(this.value);checkforcohab(this.value);">
	<option value="">-- Select One --</option>
	<cfoutput>
		<cfloop query="getOUs">
		<option value="#ID#">#name#</option>
		</cfloop> 
	</cfoutput>
</select>}kk}

{mm{<select name="project" size="1" class="selectgen" id="lrgselect"  style="width:200px" >
	<option value="">-- Select One --</option>	
</select>}mm}

{dd{
<select name="site" size="1" class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';" style="width:200px">
<option value="">-- Select One --</option>
</select>	
}dd}

<cfelseif OperatingUnit eq 0>
{kk{<select name="OperatingUnit" size="1" class="selectgen" id="lrgselect"  style="width:200px" onchange="populatelistProject(this.value);checkforcohab(this.value);">
	<option value="">-- Select One --</option>
</select>}kk}

</cfif>

<cfif Project gt 0>
<cfquery name="getProjects" datasource="#request.dsn#">
SELECT DISTINCT 
                         Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, 
                         Groups.Active_Group
FROM            Groups INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber
WHERE        (Groups.Active_Group = 1) AND (GroupLocations.entryType IN ('both', 'IncidentOnly'))
AND Groups.OUid =<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Project#">
ORDER BY Groups.Group_Name 
</cfquery>

{mm{<select name="project" size="1" class="selectgen" id="lrgselect"  style="width:200px" onchange="populateBS(this.value);">
	<option value="">-- Select One --</option>
	<cfoutput>
		<cfloop query="getProjects">
		<option value="#Group_Number#" >#Group_Name#</option>
		</cfloop> 
	</cfoutput>
</select>}mm}

{dd{
<select name="site" size="1" class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';" style="width:200px">
<option value="">-- Select One --</option>
</select>	
}dd}
</cfif>


<cfif ProjectBS gt 0>
<cfquery name="getgroupinfo" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, Countries.CountryName, 
                         NewDials_1.Name AS BU, NewDials.Name AS OU, NewDials_2.Name AS BS, GroupLocations.GroupLocID, GroupLocations.SiteName
FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         Groups INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON GroupLocations.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ProjectBS#">) AND (GroupLocations.isActive = 1) and GroupLocations.entryType != 'MHOnly'
</cfquery>

<cfoutput>{bb{#getgroupinfo.bs#}bb}</cfoutput> 


<!--- <cfoutput>{bb{<select name="BusinessStream" size="1" class="selectgen" id="lrgselect"  style="width:200px" >
	<!--- <option value="">-- Select One --</option> --->
	<option value="#getgroupinfo.bs#" selected>#getgroupinfo.bs#</option>	
</select>}bb}</cfoutput> --->


{dd{
<cfif getgroupinfo.recordcount eq 1>
<cfoutput><select name="site" size="1" class="selectgen" id="lrgselect" style="width:200px"><option value="#getgroupinfo.GroupLocID#" selected>#getgroupinfo.SiteName#</option></select></cfoutput>
<cfelseif getgroupinfo.recordcount gt 1><select name="site" size="1" class="selectgen" id="lrgselect" style="width:200px" onchange="this.style.border='1px solid 5f2167';"><option value="">-- Select One --</option>
<cfoutput query="getgroupinfo"><option value="#GroupLocID#">#SiteName#</option></cfoutput>
</select>
<cfelse>
<select name="site" size="1" class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';" style="width:200px">
	<option value="">-- Select One --</option>
</select>	 

</cfif>}dd}

</cfif>



<cfif (OperatingUnit eq 0) or (Project eq 0) or (ProjectBS eq 0)>
{mm{<select name="project" size="1" class="selectgen" id="lrgselect"  style="width:200px" >
	<option value="">-- Select One --</option>	
</select>}mm}

{dd{
<select name="site" size="1" class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';" style="width:200px">
<option value="">-- Select One --</option>
</select>	
}dd}

</cfif>

<cfif cohabitOU neq 0>
<cfquery name="getOUs" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials.Parent, NewDials.isgroupdial, NewDials.GroupNumber, NewDials.businessline, 
                         NewDials.ViewOnDashboard AS active_group, NewDials_2.Name AS parentname
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_2 ON NewDials.Parent = NewDials_2.ID
WHERE    newdials.parent <> 3234 and    (NewDials.status <> 99) AND (NewDials.isgroupdial <> 1) AND (NewDials.Parent IN
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (status <> 99) AND (isgroupdial <> 1) AND (Parent = 2707)))
ORDER BY parentname, NewDials.Name

</cfquery>
<cfif pOU neq 0>
	<cfquery name="getProjects" datasource="#request.dsn#">
SELECT DISTINCT 
                         Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, 
                         Groups.Active_Group
FROM            Groups INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber
WHERE        (Groups.Active_Group = 1) AND (GroupLocations.entryType IN ('both', 'IncidentOnly'))
AND Groups.OUid =<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pOU#">
ORDER BY Groups.Group_Name 
</cfquery>

</cfif>
{ee{<select name="cohabitou" size="1" class="selectgen" id="lrgselect"  style="width:200px" onchange="populatelistProjectcohab(this.value);">
	<option value="">-- Select One --</option>
	<cfif isdefined("getProjects.recordcount")>
	<cfoutput query="getProjects">
		<option value="#OUid#,#Group_Number#">#Group_Name#</option>
	</cfoutput>
	</cfif>
	<cfoutput query="getOUs" group="parentname">
		<option value="" disabled>#parentname#</option>
		<cfoutput>
		<option value="#ID#" >#name#</option>
		</cfoutput> 
	</cfoutput>
</select>}ee}

</cfif>

<cfif cOU neq 0 and pOU neq 0>

<cfquery name="getProjects" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, Groups.Active_Group, 
                         NewDials.Name
FROM            Groups INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID
WHERE Groups.Active_Group = 1
AND Groups.OUid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#cOU#" list="yes">)
ORDER BY NewDials.Name, Groups.Group_Name
</cfquery>

{vv{<select name="project" size="1" class="selectgen" id="lrgselect"  style="width:200px" onchange="populateBS(this.value);">
	<option value="">-- Select One --</option>
	<cfoutput  query="getProjects" group="Name">
	<option value="" disabled>#Name#</option>
		<cfoutput>
		<option value="#Group_Number#" >#Group_Name#</option>
		</cfoutput> 
	</cfoutput>
</select>}vv}

{dd{
<select name="site" size="1" class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';" style="width:200px">
<option value="">-- Select One --</option>
</select>	
}dd}


</cfif>
