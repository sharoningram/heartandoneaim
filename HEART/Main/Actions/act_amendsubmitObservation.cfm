<!------------------------------------------------------------------------------
Author : Liyakat Shaikh 
Date : 
Attributes:
Update History:
------------------------------------------------------------------------------->
	
<CFPARAM NAME = "BusinessStream" DEFAULT="">	
<CFPARAM NAME = "Country" DEFAULT="">	
<CFPARAM NAME = "site" DEFAULT="0">	
<CFPARAM NAME = "FBusinessUnit" DEFAULT="">	
<CFPARAM NAME = "FOperatingUnit" DEFAULT="">	
<cfparam name="obsnum" default="0">
<CFPARAM NAME = "cohabitou" DEFAULT="0">	
<CFPARAM NAME = "buid" DEFAULT="0">	
<CFPARAM NAME = "ouid" DEFAULT="0">
<cfparam name="devicetype" default="standard">
<cfparam name="incidentdateMOB" default="">
<cfparam name="INCIDENTDATE" default="">
<cfquery name="getBUDesc" datasource="#request.dsn#">
SELECT    NewDials.ID, NewDials.Name
FROM   NewDials
WHERE NewDials.ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BusinessUnit#">
</cfquery>

<cfset FBusinessUnit = getBUDesc.Name>

<cfquery name="getOUDesc" datasource="#request.dsn#">
SELECT    NewDials.ID, NewDials.Name
FROM   NewDials
WHERE NewDials.ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OperatingUnit#">
</cfquery>

<cfset FOperatingUnit = getOUDesc.Name>
<cfquery name="getracknum" datasource="#request.heart_ds#">
	select trackingyear, trackingnum, status
	from Observations
	where observationnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obsnum#">
</cfquery>

<cfif devicetype eq "mobile">
<cfif trim(incidentdateMOB) neq ''>
	
		<cfset INCIDENTDATE = incidentdateMOB>
	
</cfif>
</cfif>

<cfif getracknum.status eq "Closed">
<cfquery name="updateObservation" datasource="#request.HEART_DS#">
update Observations
set 

<cfif listlen(cohabitou) eq 2>
	ProjectNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#listgetat(cohabitou,2)#">,
<cfelse>
	ProjectNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#PROJECT#">,
</cfif>
SiteID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#site#>, 
ObservationBU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FBusinessUnit#">, 
ObservationOU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FOperatingUnit#">, 
BusinessStream = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#BusinessStream#">, 
Country = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Country#">, 
ExactLocation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#EXACTLOC#">, 
ObservationType = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OBVTYPE#">, 
IsWorkHome = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Work">, 
ObserverName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OBSERVERNAME#">, 
ObserverEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OBSERVEREMAIL#">, 
PersonnelCategory = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#STAKEHOLDER#">, 
<cfif isdefined ("form.INDIVIDUALTEAMNAME")>
		TeamName = 	<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#INDIVIDUALTEAMNAME#">,
<cfelse>	
		TeamName = 	<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>
DateofObservation = <cfqueryparam cfsqltype="CF_SQL_DATE" 	 value="#INCIDENTDATE#">, 
TimeofObservation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Trim(HOURS)#">, 
<cfif isdefined ("form.rulebreach") and (form.rulebreach NEQ '') >
	GlobalSafetyRules = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#rulebreach#">,
<cfelse>	
	GlobalSafetyRules = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=0>,
</cfif>
<cfif isdefined ("form.rbwhy")>
	GlobalSafetyWhyNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#rbwhy#">,
<cfelse>	
	GlobalSafetyWhyNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>
<cfif isdefined ("form.ruleother")>
	GlobalSafetyOther = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ruleother#">,
<cfelse>	
	GlobalSafetyOther = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>

<cfif isdefined ("form.ESSBREACH") and (form.ESSBREACH NEQ '')>
	SafetyEssential = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ESSBREACH#">,
<cfelse>	
	SafetyEssential = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=0>,
</cfif>			

<cfif isdefined ("form.ESSWHY")>
	SafetyEssentialNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ESSWHY#">,
<cfelse>	
	SafetyEssentialNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>
Observations = <cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#OBSERVATIONDESC#">, 
FollowUpActions = <cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#IMMEDIATEACTION#">,
<cfif isdefined ("form.FEEDBACKREQ")>
	FeedBackRequired = <cfqueryparam cfsqltype="CF_SQL_BIT" value=#FEEDBACKREQ#>,
<cfelse>
	FeedBackRequired = <cfqueryparam cfsqltype="CF_SQL_BIT" value=0>,
</cfif> 
<cfif isdefined ("form.FURTHERACTIONDESC")>
	FurtherActions = <cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#FURTHERACTIONDESC#">,
<cfelse>	
	FurtherActions = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif> 
withdrawnto = NULL, 
withdrawndate = NULL,
returnTo = NULL,
withdrawnby = NULL,

UpdatedBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,
updatedate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">,
<cfif buid eq 3234>
cohabitou = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cohabitou#">,
<cfelse>
cohabitou = 0,
</cfif>
buid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BusinessUnit#">,
ouid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OperatingUnit#">
where observationnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obsnum#">
	
</cfquery> 

	
		
<cfset filedirectory= trim(replacenocase(getcurrenttemplatepath(), 
						"main\actions\act_amendsubmitObservation.cfm", "uploads\Observations\#obsnum#"))>
											
<cfif not directoryexists("#filedirectory#")>
	<cfdirectory action="CREATE" directory="#filedirectory#">			
</cfif>

				
				<cfif trim(UPLOADDOC1) neq ''>
					<cffile action="upload" filefield="uploaddoc1" destination="#filedirectory#" result="file_result1" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC2) neq ''>
					<cffile action="upload" filefield="uploaddoc2" destination="#filedirectory#" result="file_result2" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC3) neq ''>
					<cffile action="upload" filefield="uploaddoc3" destination="#filedirectory#" result="file_result3" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC4) neq ''>
					<cffile action="upload" filefield="uploaddoc4" destination="#filedirectory#" result="file_result4" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC5) neq ''>
					<cffile action="upload" filefield="uploaddoc5" destination="#filedirectory#" result="file_result5" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif> 
				
				
	<cfquery name="addauditentry" datasource="#request.heart_ds#">
	insert into ObservationAudits (ObservationNumber, ToStatus, CompletedBy, CompletedDate, SentTo, ActionTaken, FromStatus, StatusDesc)
	values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obsnum#">,'Amended',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, '','Amended','',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Observation amended by #request.fname# #request.lname#">)
</cfquery>
<cfelse>

<CFTransaction>
<cfquery name="updateObservation" datasource="#request.HEART_DS#">
update Observations
set 

<cfif listlen(cohabitou) eq 2>
	ProjectNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#listgetat(cohabitou,2)#">,
<cfelse>
	ProjectNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#PROJECT#">,
</cfif>
SiteID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#site#>, 
ObservationBU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FBusinessUnit#">, 
ObservationOU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FOperatingUnit#">, 
BusinessStream = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#BusinessStream#">, 
Country = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Country#">, 
ExactLocation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#EXACTLOC#">, 
ObservationType = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OBVTYPE#">, 
IsWorkHome = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Work">, 
ObserverName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OBSERVERNAME#">, 
ObserverEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OBSERVEREMAIL#">, 
PersonnelCategory = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#STAKEHOLDER#">, 
<cfif isdefined ("form.INDIVIDUALTEAMNAME")>
		TeamName = 	<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#INDIVIDUALTEAMNAME#">,
<cfelse>	
		TeamName = 	<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>
DateofObservation = <cfqueryparam cfsqltype="CF_SQL_DATE" 	 value="#INCIDENTDATE#">, 
TimeofObservation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Trim(HOURS)#">, 
<cfif isdefined ("form.rulebreach") and (form.rulebreach NEQ '') >
	GlobalSafetyRules = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#rulebreach#">,
<cfelse>	
	GlobalSafetyRules = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=0>,
</cfif>
<cfif isdefined ("form.rbwhy")>
	GlobalSafetyWhyNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#rbwhy#">,
<cfelse>	
	GlobalSafetyWhyNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>
<cfif isdefined ("form.ruleother")>
	GlobalSafetyOther = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ruleother#">,
<cfelse>	
	GlobalSafetyOther = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>

<cfif isdefined ("form.ESSBREACH") and (form.ESSBREACH NEQ '')>
	SafetyEssential = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ESSBREACH#">,
<cfelse>	
	SafetyEssential = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=0>,
</cfif>			

<cfif isdefined ("form.ESSWHY")>
	SafetyEssentialNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ESSWHY#">,
<cfelse>	
	SafetyEssentialNone = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif>
Observations = <cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#OBSERVATIONDESC#">, 
FollowUpActions = <cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#IMMEDIATEACTION#">,
<cfif isdefined ("form.FEEDBACKREQ")>
	FeedBackRequired = <cfqueryparam cfsqltype="CF_SQL_BIT" value=#FEEDBACKREQ#>,
<cfelse>
	FeedBackRequired = <cfqueryparam cfsqltype="CF_SQL_BIT" value=0>,
</cfif> 
<cfif isdefined ("form.FURTHERACTIONDESC")>
	FurtherActions = <cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#FURTHERACTIONDESC#">,
<cfelse>	
	FurtherActions = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="">,
</cfif> 
withdrawnto = NULL, 
withdrawndate = NULL,
returnTo = NULL,
withdrawnby = NULL,
Status = 'Submitted',
UpdatedBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,
updatedate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">,
<cfif buid eq 3234>
cohabitou = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cohabitou#">,
<cfelse>
cohabitou = 0,
</cfif>
buid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BusinessUnit#">,
ouid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OperatingUnit#">
where observationnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obsnum#">
	
</cfquery> 

	
		
<cfset filedirectory= trim(replacenocase(getcurrenttemplatepath(), 
						"main\actions\act_amendsubmitObservation.cfm", "uploads\Observations\#obsnum#"))>
											
<cfif not directoryexists("#filedirectory#")>
	<cfdirectory action="CREATE" directory="#filedirectory#">			
</cfif>

				
				<cfif trim(UPLOADDOC1) neq ''>
					<cffile action="upload" filefield="uploaddoc1" destination="#filedirectory#" result="file_result1" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC2) neq ''>
					<cffile action="upload" filefield="uploaddoc2" destination="#filedirectory#" result="file_result2" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC3) neq ''>
					<cffile action="upload" filefield="uploaddoc3" destination="#filedirectory#" result="file_result3" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC4) neq ''>
					<cffile action="upload" filefield="uploaddoc4" destination="#filedirectory#" result="file_result4" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif>
				<cfif trim(UPLOADDOC5) neq ''>
					<cffile action="upload" filefield="uploaddoc5" destination="#filedirectory#" result="file_result5" nameconflict="makeunique" accept="#allowedfilelist#">
				</cfif> 
	

<cfquery name="getEmailDetail" datasource="#request.HEART_DS#">
SELECT   Group_Name,  SiteName,Business_Line_ID,OUid
		FROM    [#oneaimSQLname#].[dbo].[Groups],[#oneaimSQLname#].[dbo].[GroupLocations]
		WHERE 
		<cfif listlen(cohabitou) eq 2>
				Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#listgetat(cohabitou,2)#">
			<cfelse>
				Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#project#>
			</cfif>
	and	GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#site#>
</cfquery>

<cfset cclist = "">
<cfset revnames = "">
<cfquery name="getreviewer" datasource="#request.HEART_DS#">
	SELECT DISTINCT Users.Useremail, Users.Firstname, Users.Lastname
FROM            #oneaimSQLname#.dbo.Users INNER JOIN
                         #oneaimSQLname#.dbo.UserRoles ON Users.UserId = #oneaimSQLname#.dbo.UserRoles.UserID INNER JOIN
                         #oneaimSQLname#.dbo.LocationAdvisors ON #oneaimSQLname#.dbo.Users.UserId = #oneaimSQLname#.dbo.LocationAdvisors.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'HSSE Advisor') AND (LocationAdvisors.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">)
</cfquery> 

<!--- <cfquery name="getreviewer" datasource="#request.HEART_DS#">
	SELECT       Users.Useremail,Users.FirstName,Users.LastName
	FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
	WHERE     Users.status = 1 and   (UserRoles.UserRole = 'HSSE Advisor') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getEmailDetail.ouid#">)
</cfquery> --->



<cfif getreviewer.RecordCount GT 0>
	<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
	<cfloop query="getreviewer">
		<cfset revnames = listappend(revnames,"#firstname# #lastname#")>
	</cfloop>
<cfelse>
	<cfquery name="getreviewer" datasource="#request.HEART_DS#">
		SELECT       Users.Useremail,Users.FirstName,Users.LastName
		FROM     [#oneaimSQLname#].[dbo].[Users] INNER JOIN
[#oneaimSQLname#].[dbo].[UserRoles] ON Users.UserId = UserRoles.UserID
		WHERE     Users.status = 1 and   (UserRoles.UserRole = 'OU Admin') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getEmailDetail.ouid#">)
	</cfquery>
	<!--- <cfset revnames = "No HSSE Advisor for OU">
	<cfset cclist = "No HSSE Advisor found for this OU"> --->
	<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
	<cfloop query="getreviewer">
		<cfset revnames = listappend(revnames,"#firstname# #lastname#")>
	</cfloop>
</cfif>


<!--- Add audit entry --->



<cfquery name="addauditentry" datasource="#request.heart_ds#">
	insert into ObservationAudits (ObservationNumber, ToStatus, CompletedBy, CompletedDate, SentTo, ActionTaken, FromStatus, StatusDesc)
	values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obsnum#">,'Amended and Submitted for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revnames#">,'Amended and Submitted for Review','',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Observation amended and submitted for review by #request.fname# #request.lname# to #revnames#">)
</cfquery>

<!--- 
<cfquery name="getSafetyRule" datasource="#request.HEART_DS#">
SELECT SafetyRule FROM [#oneaimSQLname#].[dbo].[SafetyRules]
WHERE SafetyRuleID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#rulebreach#>
</cfquery>
<cfif  OBVTYPE EQ 'Safe Behavioural Observation' >		

	<cfset SubjectLine= #getracknum.TrackingYear#&#numberformat(qetMaxTrackNum.TrackingNum, "00000")#&" - "&#BusinessUnit#>
<cfelse>
	<cfset SubjectLine= #vTrackingYgetracknum.TrackingYearormat(qetMaxTrackNum.TrackingNum, "00000")#&" - "&#BusinessUnit#&" - "&#getSafetyRule.SafetyRule#>
	
</cfif>	 --->

<cfset SubjectLine= "HEART - "&#getracknum.TrackingYear#&#numberformat(getracknum.TrackingNum, "00000")#&" - "&#FBusinessUnit#&" - "&#OBVTYPE#>
<!--- <cfoutput>Subject: #SubjectLine#</cfoutput>
<cfabort> --->
<!--- Email H1 Email confirmation to Observer Block --->
<!--- <cfmail to="#TestEmailGroup#" from="#HeartEmail#" type="html" subject="#SubjectLine# - Safety observation recorded">
											To Observer: #OBSERVEREMAIL#<br><br> --->
<cfmail to="#OBSERVEREMAIL#" from="#HeartEmail#" type="html" subject="#SubjectLine# - Safety observation recorded">										
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><!--- #OBSERVEREMAIL# This is an automatic notification generated by HEART.<br>  --->
													Thank you for submitting an observation into HEART. This observation will now be reviewed and any appropriate action will be taken.<br><Br></span>
														<table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Event Type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#OBVTYPE#</td>
															</tr>
													<cfif  OBVTYPE NEQ 'Safe Behaviour' >		
														<cfquery name="getSafetyRule" datasource="#request.HEART_DS#">
														SELECT SafetyRule FROM [#oneaimSQLname#].[dbo].[SafetyRules]
														<cfif trim(rulebreach) neq ''>
															WHERE SafetyRuleID = 	<cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#rulebreach#>
														<cfelse>
															WHERE SafetyRuleID = 0
														</cfif>
														</cfquery>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation Category:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getSafetyRule.SafetyRule#</td>
															</tr>
													</cfif>	
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(INCIDENTDATE,sysdateformat)# #Trim(HOURS)#</td>
															</tr>
													<!--- 	</table>	 --->
														<!--- <br> --->
														<!--- <table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167">	 --->
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"> #OBSERVATIONDESC#</td>
															</tr>
														<!--- </table>	 --->
														<!--- <br> --->
														<!--- <table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167"> --->		
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.bulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#FBusinessUnit#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#FOperatingUnit#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#BusinessStream#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getEmailDetail.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getEmailDetail.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Stakeholder:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#STAKEHOLDER#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">
														For further details on this observation please go to HEART via this link - <a href="#starthttp#://#http_host#/#getappconfig.heartPath#/index.cfm?fuseaction=main.ViewObservation&OBNum=#obsnum#">Record ###getracknum.TrackingYear##numberformat(getracknum.TrackingNum, "00000")#</a><br><br>
														Please do not reply to this email address. <!--- If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# HEART Administrator. ---></span>
</cfmail>		
<!--- Email H1 Block End --->			

<!--- Email H2 Email Reviewers Block --->
<!--- Need to loop tru getreviewer query for UAT --->
<!--- <cfmail to="#TestEmailGroup#" from="#HeartEmail#" type="html" subject="#SubjectLine# - Notification of an observation">
											To HSSE Advisor:#cclist#<br><br> --->
<cfmail to="#cclist#" from="#HeartEmail#" type="html" subject="#SubjectLine# - Notification of an observation">
																	<!--- #cclist#	 --->			
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by HEART.<br>  
													The following observation is awaiting your review. Please go to HEART via this link to progress the record - <a href="#starthttp#://#http_host#/#getappconfig.heartPath#/index.cfm?fuseaction=main.ReviewObservation&OBNum=#obsnum#&ActionType=vReview">Record ###getracknum.TrackingYear##numberformat(getracknum.TrackingNum, "00000")#</a> <br><Br></span>
														<table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Event Type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#OBVTYPE#</td>
															</tr>
													<cfif  OBVTYPE NEQ 'Safe Behaviour' >		
														<cfquery name="getSafetyRule" datasource="#request.HEART_DS#">
														SELECT SafetyRule FROM [#oneaimSQLname#].[dbo].[SafetyRules]
														<cfif trim(rulebreach) neq ''>
															WHERE SafetyRuleID = 	<cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#rulebreach#>
														<cfelse>
															WHERE SafetyRuleID = 0
														</cfif>
														</cfquery>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation Category:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getSafetyRule.SafetyRule#</td>
															</tr>
													</cfif>	
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(INCIDENTDATE,sysdateformat)# #Trim(HOURS)#</td>
															</tr>
														<!--- </table> --->	
														<br>
														<!--- <table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167"> --->	
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Observation description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"> #OBSERVATIONDESC#</td>
															</tr>
													<!--- 	</table>	 --->
														<!--- <br> --->
														<!--- <table class="purplebg"  bgcolor="5f2167" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2167">	 --->	
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.bulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#FBusinessUnit#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#FOperatingUnit#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#BusinessStream#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getEmailDetail.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getEmailDetail.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Stakeholder:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#STAKEHOLDER#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">
														For further details on this observation please go to HEART via this link - <a href="#starthttp#://#http_host#/#getappconfig.heartPath#/index.cfm?fuseaction=main.ReviewObservation&OBNum=#obsnum#&ActionType=vReview">Record ###getracknum.TrackingYear##numberformat(getracknum.TrackingNum, "00000")#</a><br><br>
														Please do not reply to this email address. <!--- If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# HEART Administrator. ---></span>
</cfmail>		
<!--- Email H2 Block End --->												
</CFTRANSACTION>  
  
  </cfif>
<cflocation url="index.cfm?fuseaction=main.main" addtoken="No">  
