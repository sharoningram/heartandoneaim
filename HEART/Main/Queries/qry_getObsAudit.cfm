<cfparam name="qrysort" default="DESC">
<cfquery name="getobsaudit" datasource="#request.heart_ds#">
SELECT       AuditID, ObservationNumber, ToStatus, CompletedBy, CompletedDate, SentTo, ActionTaken, FromStatus, StatusDesc
FROM            ObservationAudits
WHERE        (ObservationNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OBNum#">)
ORDER BY CompletedDate #qrysort#
</cfquery>