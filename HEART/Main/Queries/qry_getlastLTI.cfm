<cfquery name="getlastlti" datasource="#request.dsn#">
SELECT        MAX(oneAIMincidents.incidentDate) AS ltidate, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType IN (1, 5)) AND (oneAIMInjuryOI.OSHAclass = 2)
GROUP BY Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo
ORDER BY ltidate desc, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo
</cfquery>
<!--- <cfquery name="getgloballastlti" datasource="#request.dsn#">
SELECT        MAX(oneAIMincidents.incidentDate) AS ltidate, oneAIMincidents.IncAssignedTo
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType IN (1, 5)) AND (oneAIMInjuryOI.OSHAclass = 2)
GROUP BY oneAIMincidents.IncAssignedTo
ORDER BY ltidate DESC, oneAIMincidents.IncAssignedTo
</cfquery> --->