<cfquery name="getObservationsForAction" datasource="#request.HEART_DS#">
Select ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated,UpdateDate
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number
WHERE        (Observations.Status = 'Reviewed') and Observations.withdrawnto is NULL
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="yes">)
<cfelse>
and 1 = 2
</cfif>
Order By TrackingNum

</cfquery> 

<!--- <cfquery name="getObservationsForReview" datasource="#request.HEART_DS#">
SELECT ObservationNumber,ProjectNumber,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated 
FROM dbo.Observations
Where Status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Submitted">	
Order By TrackingNum

</cfquery> --->