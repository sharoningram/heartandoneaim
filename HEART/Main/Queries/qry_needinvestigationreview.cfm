<cfquery name="needinvestingationreview" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, 
                         IncidentInvestigation.Status, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE      (oneAIMincidents.isWorkRelated = 'yes') AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed')) AND (IncidentInvestigation.Status = 'Sent for Review') AND (IncidentAudits.Status = 'Investigation Sent for Review')   
<cfif listfindnocase(request.userlevel,"Reviewer") gt 0>
and (Groups.OUid IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="Yes">))
</cfif>
<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
and (Groups.business_line_id IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">))
</cfif>
<cfif  listfindnocase(request.userlevel,"Reviewer") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
and 1 = 0
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, 
                         IncidentInvestigation.Status, oneAIMincidents.dateCreated
ORDER BY oneAIMincidents.TrackingNum
</cfquery>