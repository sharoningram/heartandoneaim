<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
<cfquery name="getfatsentforreview" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, Groups.OUid, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    oneAIMincidents.isfatality = 'Yes' and    (oneAIMincidents.Status IN ('sent for review')) AND  (IncidentAudits.Status = 'Sent for Review')
 AND (Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">))
GROUP BY oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, Groups.OUid, oneAIMincidents.dateCreated
ORDER BY oneAIMincidents.TrackingNum
</cfquery>
</cfif>