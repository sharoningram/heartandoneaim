<cfquery name="getObservationsForReviewWL" datasource="#request.HEART_DS#">
Select ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated, groups.ouid
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number
WHERE        (Observations.Status = 'Submitted')  and Observations.withdrawnto is NULL
<cfif showall>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif  listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"Global Admin") eq 0>
			and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="yes">)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0 and  listfindnocase(request.userlevel,"Global Admin") eq 0>
			and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="yes">)
		</cfif>
	<cfelse>
	and Observations.createdbyemail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
	</cfif>
<cfelse>
<cfif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	and ObservationNumber not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getObservationsForReview.ObservationNumber)#" list="Yes">)
	and Observations.siteid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.hsseadvlocs#" list="yes">)
<cfelse>
	and Observations.createdbyemail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
	and ObservationNumber not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getObservationsForReview.ObservationNumber)#" list="Yes">)
</cfif>
</cfif>
Order By TrackingNum
</cfquery> 