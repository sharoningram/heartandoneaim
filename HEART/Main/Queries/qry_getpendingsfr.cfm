<cfquery name="getpendingsfr" datasource="#request.dsn#">
SELECT   oneAIMincidents.irn, oneAIMincidents.isfatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, oneAIMincidents.dateCreated
FROM            IncidentTypes RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentTypes.IncTypeID = oneAIMincidents.PrimaryType LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status in ('started','initiated','withdrawn to initiator')) AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
ORDER BY oneAIMincidents.TrackingNum
</cfquery>