<cfquery name="getsafetyessentialchart" datasource="#request.HEART_DS#">
SELECT        COUNT(Observations.ObservationNumber) AS obscnt, Observations.SafetyEssential AS seID, #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE        (Observations.Status <> 'Cancelled') AND (YEAR(Observations.DateCreated) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">) AND (Observations.SafetyEssential > 0)
GROUP BY Observations.SafetyEssential, #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential
</cfquery>