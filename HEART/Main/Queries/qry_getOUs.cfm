<cfquery name="getOUs" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials.Parent, NewDials.isgroupdial, NewDials.GroupNumber, NewDials.businessline, 
                         NewDials.ViewOnDashboard AS active_group, NewDials_2.Name AS parentname
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_2 ON NewDials.Parent = NewDials_2.ID
WHERE (NewDials.status <> 99) AND (NewDials.isgroupdial <> 1) 
AND (NewDials.Parent IN
(SELECT   ID FROM  NewDials AS NewDials_1
WHERE        (status <> 99) AND (isgroupdial <> 1) AND (Parent = 2707)))
ORDER BY parentname, Name

</cfquery>