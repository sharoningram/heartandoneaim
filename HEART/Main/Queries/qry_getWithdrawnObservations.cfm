

<cfquery name="getWithdrawn" datasource="#request.HEART_DS#">
Select ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated,withdrawnby
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number
WHERE    Observations.withdrawnto is not NULL
<cfif showall>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif  listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"Global Admin") eq 0>
			and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="yes">)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0 and  listfindnocase(request.userlevel,"Global Admin") eq 0>
			and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="yes">)
		</cfif>
	<cfelse>
	and  Observations.withdrawnby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
	</cfif>
<cfelse>
and  Observations.withdrawnby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
</cfif>
Order By TrackingNum
</cfquery> 