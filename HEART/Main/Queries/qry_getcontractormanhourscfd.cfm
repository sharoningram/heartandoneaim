
<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Contractors.CoType, Groups.Business_Line_ID, 
                         YEAR(ContractorHours.WeekEndingDate) AS conyr, MONTH(ContractorHours.WeekEndingDate) AS conmonth
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE        (YEAR(ContractorHours.WeekEndingDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
GROUP BY YEAR(ContractorHours.WeekEndingDate), MONTH(ContractorHours.WeekEndingDate), Contractors.CoType, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID
ORDER BY conyr, conmonth, Contractors.CoType, GroupLocations.LocationDetailID

</cfquery>
