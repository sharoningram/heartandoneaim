<cfset qryouidlist = 0>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
	<cfset qryouidlist = request.userOUs>
	<cfquery name="lookupadvisor" datasource="#request.dsn#">
		SELECT        Users.UserId, UserRoles.AssignedLocs, Users.Firstname
		FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
		WHERE        (UserRoles.AssignedLocs IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="yes">)) AND (UserRoles.UserRole = 'HSSE Advisor')
	</cfquery>
	<cfif lookupadvisor.recordcount gt 0>
		<cfloop query="lookupadvisor">
			<cfif listfind(qryouidlist,AssignedLocs) gt 0>
				<cfset qryouidlist = listdeleteat(qryouidlist,listfind(qryouidlist,AssignedLocs))>
			</cfif>
		</cfloop>
	</cfif>
</cfif>
<cfquery name="getObservationsForReview" datasource="#request.HEART_DS#">
Select ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated 
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number
WHERE        (Observations.Status = 'Submitted') and Observations.withdrawnto is NULL
<cfif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
and Observations.siteid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.hsseadvlocs#" list="yes">) 

<cfelse>
	<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
		and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryouidlist#" list="yes">)
	<cfelse>
		and 1 = 2
	</cfif>
</cfif>
Order By TrackingNum
</cfquery> 

<!--- <cfquery name="getObservationsForReview" datasource="#request.HEART_DS#">
SELECT ObservationNumber,ProjectNumber,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated 
FROM dbo.Observations
Where Status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Submitted">	
Order By TrackingNum

</cfquery> --->