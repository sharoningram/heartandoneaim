<cfquery name="getleading" datasource="#request.dsn#">
SELECT         DashboardTrending.dbtID, DashboardTrending.dbtType, CASE WHEN DashboardTrending.zplanned IS NULL THEN 0 ELSE zplanned END AS zplanned, 
                         CASE WHEN DashboardTrending.zcomplete IS NULL THEN 0 ELSE zcomplete END AS zcomplete, DashboardTrending.dbtyear, DashboardTrending.updateby, 
                         DashboardTrending.updatedate, CASE WHEN DashboardTrending.psiPlanned IS NULL THEN 0 ELSE psiplanned END AS psiplanned, 
                         CASE WHEN DashboardTrending.psiComplete IS NULL THEN 0 ELSE psicomplete END AS psicomplete, CASE WHEN DashboardTrending.hsseaccplanned IS NULL 
                         THEN 0 ELSE hsseaccplanned END AS hsseaccplanned, CASE WHEN DashboardTrending.hsseacccomplete IS NULL 
                         THEN 0 ELSE hsseacccomplete END AS hsseacccomplete, CASE WHEN DashboardTrending.rmPlanned IS NULL THEN 0 ELSE rmplanned END AS rmplanned, 
                         CASE WHEN DashboardTrending.rmComplete IS NULL THEN 0 ELSE rmcomplete END AS rmcomplete,NewDials.ID AS DialID, NewDials.Name
FROM            DashboardTrending RIGHT OUTER JOIN
                         NewDials ON DashboardTrending.DialID = NewDials.ID AND DashboardTrending.dbtyear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">
WHERE        (NewDials.ID IN
                             (SELECT        ID
                               FROM            NewDials AS NewDials_2
                               WHERE        (Parent =
                                                             (SELECT        ID
                                                               FROM            NewDials AS NewDials_1
                                                               WHERE        (Parent = 0))))) AND (NewDials.status <> 99)
ORDER BY NewDials.ID DESC
</cfquery>