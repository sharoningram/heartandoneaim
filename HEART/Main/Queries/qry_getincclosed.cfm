<cfquery name="getincclsoed" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.Status, oneAIMincidents.incidentDate, oneAIMincidents.dateClosed, oneAIMincidents.needIRP, 
                         oneAIMIncidentIRP.Status AS asirpstat
FROM            oneAIMincidents LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN
WHERE        (oneAIMincidents.Status <> 'cancel') and year(incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">
ORDER BY oneAIMincidents.Status
</cfquery>