<cfquery name="getnonjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) AS jdehrs, 
                         SUM(LaborHoursTotal.SubHrs) AS sub, SUM(LaborHoursTotal.JVHours) AS jv, SUM(LaborHoursTotal.ManagedContractorHours) AS mgd, 
                         GroupLocations.LocationDetailID, Groups.Business_Line_ID, YEAR(LaborHoursTotal.WeekEndingDate) AS manyr, MONTH(LaborHoursTotal.WeekEndingDate) 
                         AS manmonth
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
WHERE        (YEAR(LaborHoursTotal.WeekEndingDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
GROUP BY YEAR(LaborHoursTotal.WeekEndingDate), MONTH(LaborHoursTotal.WeekEndingDate), Groups.Business_Line_ID, GroupLocations.LocationDetailID
ORDER BY manyr, manmonth, Groups.Business_Line_ID, GroupLocations.LocationDetailID
</cfquery>


