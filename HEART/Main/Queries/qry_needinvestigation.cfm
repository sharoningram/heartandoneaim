<cfquery name="needinvestingation" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, 
                         IncidentInvestigation.Status, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) AND (oneAIMincidents.isWorkRelated = 'yes') AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed')) AND ((IncidentInvestigation.Status = 'started') OR (IncidentInvestigation.Status IS NULL)) AND (IncidentAudits.ActionTaken = 'Review Complete')
GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, 
                         IncidentInvestigation.Status, oneAIMincidents.dateCreated
ORDER BY oneAIMincidents.TrackingNum
</cfquery>