<cfquery name="getObsDashboard" datasource="#request.HEART_DS#">
SELECT        COUNT(Observations.ObservationNumber) AS odbcnt, Observations.ObservationType, #oneaimSQLname#.dbo.Groups.Business_Line_ID, 
                         Observations.PersonnelCategory
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number
WHERE        (Observations.Status <> 'Cancelled') AND (YEAR(Observations.DateCreated) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">)
GROUP BY Observations.ObservationType, #oneaimSQLname#.dbo.Groups.Business_Line_ID, Observations.PersonnelCategory
</cfquery>