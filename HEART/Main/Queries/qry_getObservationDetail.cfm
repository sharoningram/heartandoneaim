<cfquery name="getObservationType" datasource="#request.HEART_DS#">
SELECT   ObservationType
FROM    dbo.Observations 
WHERE 
 ObservationNumber = <cfqueryparam  cfsqltype="CF_SQL_INTEGER" value="#OBNum#">
</cfquery> 


<cfif   getObservationType.ObservationType NEQ 'Safe Behaviour' >
	<!--- 	<cfquery name="getObservationDetail" datasource="#request.HEART_DS#">
		SELECT   ObservationNumber, CreatedBy, CreatedbyEmail, CreatedByUserID, DateCreated, ProjectNumber,Group_Name, SiteID, SiteName,ObservationBU, ObservationOU, 
		                         BusinessStream, Country, ExactLocation, ObservationType, IsWorkHome, ObserverName, ObserverEmail, PersonnelCategory, TeamName, DateofObservation, 
		                         TimeofObservation, GlobalSafetyRules,SafetyRule, GlobalSafetyOther, OB.SafetyEssential,ImageName,SE.SafetyEssential AS SafetyEssDesc, SafetyEssentialNone, Observations, FeedBackRequired, FurtherActionRequired, FurtherActions,FollowUpActions, TrackingYear, TrackingNum, OB.Status, GlobalSafetyWhyNone, UpdateDate, DateClosed, UpdatedBy, EscalateoneAIM,FurtherActions,CancelComments,CommentsoneAIM,CloseComments
		FROM    dbo.Observations OB,[#oneaimSQLname#].[dbo].[Groups],[#oneaimSQLname#].[dbo].[SafetyEssentials] SE,[#oneaimSQLname#].[dbo].[SafetyRules] SR,[#oneaimSQLname#].[dbo].[GroupLocations]
		WHERE 
		ProjectNumber=Group_Number
		AND OB.SafetyEssential=SafetyEssentialID
		AND GlobalSafetyRules = SR.SafetyRuleID
		AND SiteID=GroupLocID
		AND ObservationNumber = <cfqueryparam  cfsqltype="CF_SQL_INTEGER" value="#OBNum#">
		</cfquery>  --->
			<cfquery name="getObservationDetail" datasource="#request.HEART_DS#">
	SELECT        OB.ObservationNumber, OB.CreatedBy, OB.CreatedbyEmail, OB.CreatedByUserID, OB.DateCreated, OB.ProjectNumber, #oneaimSQLname#.dbo.Groups.Group_Name, 
                         OB.SiteID, #oneaimSQLname#.dbo.GroupLocations.SiteName, OB.ObservationBU, OB.ObservationOU, OB.BusinessStream, OB.Country, OB.ExactLocation, 
                         OB.ObservationType, OB.IsWorkHome, OB.ObserverName, OB.ObserverEmail, OB.PersonnelCategory, OB.TeamName, OB.DateofObservation, 
                         OB.TimeofObservation, OB.GlobalSafetyRules, SR.SafetyRule, OB.GlobalSafetyOther, OB.SafetyEssential, SE.imagename, SE.SafetyEssential AS SafetyEssDesc, 
                         OB.SafetyEssentialNone, OB.Observations, OB.FeedBackRequired, OB.FurtherActionRequired, OB.FurtherActions, OB.FollowUpActions, OB.TrackingYear, 
                         OB.TrackingNum, OB.Status, OB.GlobalSafetyWhyNone, OB.UpdateDate, OB.DateClosed, OB.UpdatedBy, OB.Escalateoneaim, OB.FurtherActions AS Expr1, 
                         OB.CancelComments, OB.Commentsoneaim, OB.CloseComments, OB.withdrawnto, OB.withdrawndate, OB.returnTo, OB.withdrawnby, ob.cohabitOU, ob.buid, ob.ouid
FROM            Observations AS OB INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON OB.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON OB.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules AS SR ON OB.GlobalSafetyRules = SR.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials AS SE ON OB.SafetyEssential = SE.SafetyEssentialID
WHERE        (OB.ObservationNumber = <cfqueryparam  cfsqltype="CF_SQL_INTEGER" value="#OBNum#">) 
		</cfquery> 
	<cfelse>
	
		<cfquery name="getObservationDetail" datasource="#request.HEART_DS#">
		SELECT   ObservationNumber, CreatedBy, CreatedbyEmail, CreatedByUserID, DateCreated, ProjectNumber,Group_Name, SiteID, SiteName,ObservationBU, ObservationOU, 
                         BusinessStream, Country, ExactLocation, ObservationType, IsWorkHome, ObserverName, ObserverEmail, PersonnelCategory, TeamName, DateofObservation, 
                         TimeofObservation, GlobalSafetyRules, GlobalSafetyOther, OB.SafetyEssential, SafetyEssentialNone, Observations, FollowUpActions, FeedBackRequired, 
                         FurtherActionRequired, FurtherActions, TrackingYear, TrackingNum, OB.Status, GlobalSafetyWhyNone, UpdateDate, DateClosed, UpdatedBy, Escalateoneaim,FurtherActions,CancelComments,Commentsoneaim,CloseComments, OB.withdrawnto, OB.withdrawndate, OB.returnTo, OB.withdrawnby,cohabitOU, ob.buid, ob.ouid
		FROM    dbo.Observations OB,[#oneaimSQLname#].[dbo].[Groups],[#oneaimSQLname#].[dbo].[GroupLocations]
		WHERE 
		ProjectNumber=Group_Number
		AND SiteID=GroupLocID
		AND ObservationNumber = <cfqueryparam  cfsqltype="CF_SQL_INTEGER" value="#OBNum#">
		</cfquery>
</cfif>

<cfif trim(getObservationDetail.cohabitOU) neq '' and trim(getObservationDetail.cohabitOU) gt 0>
	<cfquery name="getcohab" datasource="#request.dsn#">
	select name
	from newdials
	<cfif listlen(getObservationDetail.cohabitOU) eq 2>
	where id =  <cfqueryparam  cfsqltype="CF_SQL_INTEGER" value="#listgetat(getObservationDetail.cohabitOU,1)#">
	<cfelse>
	where id =  <cfqueryparam  cfsqltype="CF_SQL_INTEGER" value="#getObservationDetail.cohabitOU#">
	</cfif>
	</cfquery>
</cfif>