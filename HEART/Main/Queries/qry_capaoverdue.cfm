<cfquery name="getcapaoverdue" datasource="#request.dsn#">
SELECT        oneAIMCAPA.CAPAid, oneAIMCAPA.CAPAtype, NewDials.Name AS ouname, oneAIMCAPA.DueDate, oneAIMCAPA.AssignedTo, oneAIMCAPA.ActionDetail, 
                         oneAIMCAPA.EnteredBy, oneAIMCAPA.DateComplete, oneAIMincidents.IRN, Groups.Group_Name,oneAIMincidents.TrackingNum
FROM            oneAIMCAPA INNER JOIN
                         oneAIMincidents ON oneAIMCAPA.IRN = oneAIMincidents.IRN INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID
WHERE        (oneAIMCAPA.EnteredBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) AND (oneAIMCAPA.DueDate < <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) AND (oneAIMCAPA.DateComplete IS NULL)
ORDER BY oneAIMCAPA.CAPAtype
</cfquery>