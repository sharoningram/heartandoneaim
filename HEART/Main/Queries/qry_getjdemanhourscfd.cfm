<cfset rollstart = "1/1/#year(now())#">
<cfset rollend = "#now()#">
<cfset incyear = "#year(now())#">
<!---  SUM(LaborHours.HO_HRS) AS ho, SUM(LaborHours.FIELD_HRS) AS fld, SUM(LaborHours.CRAFT_HRS) AS crft --->
<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID, YEAR(LaborHours.WEEK_ENDING_DATE) AS jdeyr, MONTH(LaborHours.WEEK_ENDING_DATE) AS jdemonth
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE        (YEAR(LaborHours.WEEK_ENDING_DATE) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
GROUP BY YEAR(LaborHours.WEEK_ENDING_DATE), MONTH(LaborHours.WEEK_ENDING_DATE), Groups.Group_Number, Groups.Group_Name, 
                         GroupLocations.LocationDetailID, Groups.Business_Line_ID
ORDER BY jdeyr, jdemonth, Groups.Business_Line_ID
</cfquery>


<cfquery name="getjdeoh" datasource="#request.dsn#">
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, Groups.Business_Line_ID, YEAR(LaborHoursOffice.WEEK_ENDING_DATE) AS ohyr, 
                         MONTH(LaborHoursOffice.WEEK_ENDING_DATE) AS ohmonth
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE        (YEAR(LaborHoursOffice.WEEK_ENDING_DATE) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
GROUP BY YEAR(LaborHoursOffice.WEEK_ENDING_DATE), MONTH(LaborHoursOffice.WEEK_ENDING_DATE), Groups.Business_Line_ID, 
                         GroupLocations.LocationDetailID
ORDER BY ohyr, ohmonth, Groups.Business_Line_ID, GroupLocations.LocationDetailID

</cfquery>
