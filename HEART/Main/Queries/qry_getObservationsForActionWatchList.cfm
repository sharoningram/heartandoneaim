
<cfquery name="getObservationsForActionWL" datasource="#request.HEART_DS#">
Select distinct ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated,UpdateDate
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number
Where  Status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Reviewed">	  and Observations.withdrawnto is NULL
<cfif showall>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif  listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"Global Admin") eq 0>
			and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="yes">)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0 and  listfindnocase(request.userlevel,"Global Admin") eq 0>
			and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="yes">)
		</cfif>
	<cfelse>
	and Observations.createdbyemail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
	</cfif>
<cfelse>

	<cfif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
		and ((Observations.siteid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.hsseadvlocs#" list="yes">) and Observations.updatedBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) or Observations.createdbyemail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
	<Cfelse>
	and Observations.createdbyemail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
		
	</cfif>
	and ObservationNumber not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getObservationsForAction.ObservationNumber)#" list="Yes">)
</cfif>
Order By TrackingNum

</cfquery> 
