<cfquery name="getgroups" datasource="#request.dsn#">
SELECT        Group_Number, Group_Name, Country_Code, Business_Line_ID, OUid, BusinessStreamID, Active_Group
FROM            Groups
WHERE Active_Group = 1
<cfloop list="#request.userlevel#" index="ru">
	<cfif listfindnocase("user",ru) gt 0>
		and group_number in (SELECT        GroupNumber
FROM            GroupUserAssignment
WHERE        (status = 1) AND (GroupRole = 'dataentry') AND (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">))
	</cfif>
	<cfif listfindnocase("BU Admin",ru) gt 0>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserBUs#" list="Yes">)
	</cfif>
	<cfif listfindnocase("OU Admin",ru) gt 0>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserOUs#" list="Yes">)
	</cfif>
</cfloop>
order by Group_Name
</cfquery>