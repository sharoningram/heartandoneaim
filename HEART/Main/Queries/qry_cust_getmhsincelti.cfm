
<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">
			<cfset hrscount = 0>
			<cfquery name="getjdehrsc" datasource="#request.dsn#">
				SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs
				FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
				WHERE        (LaborHours.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#">)  and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getjdehrsc.jdehrs) neq ''>
				<cfset hrscount = hrscount+getjdehrsc.jdehrs>
			</cfif>
			<cfquery name="getjdeohc" datasource="#request.dsn#">
				SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh
				FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
				WHERE       LaborHoursOffice.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#">  and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getjdeohc.oh) neq ''>
				<cfset hrscount = hrscount+getjdeohc.oh>
			</cfif>
			<cfquery name="getnonjdehrsc" datasource="#request.dsn#">
				SELECT     SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) AS jdehrs
				FROM            LaborHoursTotal INNER JOIN
				                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
				WHERE     LaborHoursTotal.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#">  and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getnonjdehrsc.jdehrs) neq ''>
				<cfset hrscount = hrscount+getnonjdehrsc.jdehrs>
			</cfif>
			<cfif trim(hrscount) neq ''>
				<cfset hrscount = numberformat(hrscount)>
			</cfif>
		</cfcase>
		<cfcase value="3">
			<cfset hrscount = 0>
			<cfquery name="getnonjdehrsc" datasource="#request.dsn#">
				SELECT     SUM(LaborHoursTotal.SubHrs) AS hrs
				FROM            LaborHoursTotal INNER JOIN
				                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
				WHERE     LaborHoursTotal.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#"> and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getnonjdehrsc.hrs) neq ''>
				<cfset hrscount = hrscount+getnonjdehrsc.hrs>
			</cfif>
			<cfquery name="getcontractorhrs" datasource="#request.dsn#">
				SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs
				FROM            ContractorHours INNER JOIN
				                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
				                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
				WHERE       ContractorHours.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#"> and Contractors.cotype = 'sub' and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getcontractorhrs.conhrs) neq ''>
				<cfset hrscount = hrscount+getcontractorhrs.conhrs>
			</cfif>
			<cfif trim(hrscount) neq ''>
				<cfset hrscount = numberformat(hrscount)>
			</cfif>
		</cfcase>
		<cfcase value="4">
			<cfset hrscount = 0>
			<cfquery name="getnonjdehrsc" datasource="#request.dsn#">
				SELECT     SUM(LaborHoursTotal.ManagedContractorHours) AS hrs
				FROM            LaborHoursTotal INNER JOIN
				                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
				WHERE     LaborHoursTotal.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#"> and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getnonjdehrsc.hrs) neq ''>
				<cfset hrscount = hrscount+getnonjdehrsc.hrs>
			</cfif>
			<cfquery name="getcontractorhrs" datasource="#request.dsn#">
				SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs
				FROM            ContractorHours INNER JOIN
				                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
				                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
				WHERE       ContractorHours.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#"> and Contractors.cotype = 'mgd'  and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getcontractorhrs.conhrs) neq ''>
				<cfset hrscount = hrscount+getcontractorhrs.conhrs>
			</cfif>
			<cfif trim(hrscount) neq ''>
				<cfset hrscount = numberformat(hrscount)>
			</cfif>
		</cfcase>
		<cfcase value="11">
			<cfset hrscount = 0>
			<cfquery name="getnonjdehrsc" datasource="#request.dsn#">
				SELECT     SUM(LaborHoursTotal.JVHours) AS hrs
				FROM            LaborHoursTotal INNER JOIN
				                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
				WHERE     LaborHoursTotal.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#"> and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getnonjdehrsc.hrs) neq ''>
				<cfset hrscount = hrscount+getnonjdehrsc.hrs>
			</cfif>
			<cfquery name="getcontractorhrs" datasource="#request.dsn#">
				SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs
				FROM            ContractorHours INNER JOIN
				                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
				                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
				                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
				WHERE       ContractorHours.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#lastltidate#"> and Contractors.cotype = 'jv' and groups.business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getBUs.ID#">
			</cfquery>
			<cfif trim(getcontractorhrs.conhrs) neq ''>
				<cfset hrscount = hrscount+getcontractorhrs.conhrs>
			</cfif>
			<cfif trim(hrscount) neq ''>
				<cfset hrscount = numberformat(hrscount)>
			</cfif>
		</cfcase>
		
</cfswitch>