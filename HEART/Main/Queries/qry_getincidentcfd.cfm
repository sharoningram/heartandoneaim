<cfquery name="getcfdincs" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, 
                         YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) AS incmonth
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType IN (1, 5)) AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)) AND 
                         (YEAR(oneAIMincidents.incidentDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, 
                         oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating
ORDER BY incyr, incmonth, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating

</cfquery>
<cfquery name="getfatalities" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, YEAR(oneAIMincidents.incidentDate) AS fatyr, 
                         MONTH(oneAIMincidents.incidentDate) AS fatmonth
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (YEAR(oneAIMincidents.incidentDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">)) AND (oneAIMincidents.isFatality = 'yes')
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo
ORDER BY fatyr, fatmonth, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo
</cfquery>