<cfquery name="getneedfirstalert" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, 
                         oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) AND (oneAIMincidents.needFA = 1) AND (oneAIMincidents.IRN IN
                             (SELECT        oneAIMincidents.IRN
FROM            oneAimFirstAlerts RIGHT OUTER JOIN
                         oneAIMincidents ON oneAimFirstAlerts.IRN = oneAIMincidents.IRN AND oneAimFirstAlerts.Status NOT IN ('Sent for Review', 'Sent for Approval', 
                         'More Info Requested')
WHERE        (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) AND oneAIMincidents.needFA = 1))  AND 
                         (IncidentAudits.ActionTaken = 'Review Complete')
GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes.IncType, 
                         oneAIMincidents.dateCreated
ORDER BY oneAIMincidents.TrackingNum

</cfquery>