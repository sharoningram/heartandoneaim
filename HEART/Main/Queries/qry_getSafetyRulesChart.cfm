<cfquery name="getsafetyrulechart" datasource="#request.HEART_DS#">
SELECT        COUNT(Observations.ObservationNumber) AS obscnt, Observations.GlobalSafetyRules, #oneaimSQLname#.dbo.SafetyRules.SafetyRule
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID
WHERE        (Observations.Status <> 'Cancelled') AND (Observations.GlobalSafetyRules > 0) AND (YEAR(Observations.DateCreated) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">)
GROUP BY Observations.GlobalSafetyRules, #oneaimSQLname#.dbo.SafetyRules.SafetyRule
</cfquery>