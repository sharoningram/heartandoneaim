<cfquery name="getamendcomms" datasource="#request.heart_ds#">
SELECT        commentID, ObservationNumber, Comment, DateEntered, EnteredBy
FROM            ObservationAmendComments
WHERE        (ObservationNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obnum#">)
ORDER BY DateEntered DESC
</cfquery>