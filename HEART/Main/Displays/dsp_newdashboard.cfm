<!--- <dataset seriesname='Profits' renderas='area' showvalues='0'><set value='4000' /><set value='5000' /><set value='3000' /><set value='4000' /><set value='1000' /><set value='7000' /><set value='1000' /><set value='4000' /><set value='1000' /><set value='8000' /><set value='2000' /><set value='7000' /></dataset> syaxismaxvalue='0.5' --->
<cfset chart1xml = "<chart palettecolors='##ef730f' linecolor='##6d8833' showShadow='0' drawAnchors='1'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1' caption='Lost Time Incident Frequency' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint'><categories><category label='Jan' /><category label='Feb' /><category label='Mar' /><category label='Apr' /><category label='May' /><category label='Jun' /><category label='Jul' /><category label='Aug' /><category label='Sep' /><category label='Oct' /><category label='Nov' /><category label='Dec' /></categories><dataset seriesname='Incidents' renderas='bar'><set value='6' /><set value='23' /><set value='7' /><set value='23' /><set value='5' /><set value='17' /><set value='8' /><set value='3' /><set value='22' /><set value='15' /><set value='11' /><set value='9' /></dataset><dataset seriesname='Rate' parentyaxis='S' renderas='line' showvalues='0'><set value='0.32' /><set value='0.35' /><set value='0.26' /><set value='0.53' /><set value='0.19' /><set value='0.22' /><set value='0.37' /><set value='0.21' /><set value='0.13' /><set value='0.16' /><set value='0.11' /><set value='0.10' /></dataset></chart>">
<cfset chart2xml = "<chart palettecolors='##ef730f' linecolor='##6d8833' showShadow='0' drawAnchors='1'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1'  caption='Total Recordable Frequency' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix='' theme='fint'><categories><category label='Jan' /><category label='Feb' /><category label='Mar' /><category label='Apr' /><category label='May' /><category label='Jun' /><category label='Jul' /><category label='Aug' /><category label='Sep' /><category label='Oct' /><category label='Nov' /><category label='Dec' /></categories><dataset seriesname='Incidents' renderas='bar'><set value='6' /><set value='8' /><set value='7' /><set value='12' /><set value='5' /><set value='11' /><set value='8' /><set value='3' /><set value='17' /><set value='12' /><set value='11' /><set value='9' /></dataset><dataset seriesname='Rate' parentyaxis='S' renderas='line' showvalues='0'><set value='0.8' /><set value='0.6' /><set value='0.26' /><set value='0.4' /><set value='0.19' /><set value='0.22' /><set value='0.37' /><set value='0.3' /><set value='0.13' /><set value='0.16' /><set value='0.11' /><set value='0.10' /></dataset></chart>">
<cfset chart3xml = "<chart palettecolors='##ef730f' linecolor='##6d8833' showShadow='0' drawAnchors='1'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1'  caption='High Potential Incidents' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint'><categories><category label='Jan' /><category label='Feb' /><category label='Mar' /><category label='Apr' /><category label='May' /><category label='Jun' /><category label='Jul' /><category label='Aug' /><category label='Sep' /><category label='Oct' /><category label='Nov' /><category label='Dec' /></categories><dataset seriesname='Incidents' renderas='bar'><set value='1' /><set value='3' /><set value='1' /><set value='5' /><set value='5' /><set value='0' /><set value='2' /><set value='1' /><set value='3' /><set value='0' /><set value='2' /><set value='2' /></dataset><dataset seriesname='Rate' parentyaxis='S' renderas='line' showvalues='0'><set value='0.08' /><set value='0.06' /><set value='0.06' /><set value='0.04' /><set value='0.09' /><set value='0.02' /><set value='0.07' /><set value='0.03' /><set value='0.11' /><set value='0.06' /><set value='0.11' /><set value='0.10' /></dataset></chart>">
<cfset chart4xml = "<chart palettecolors='##ef730f' linecolor='##6d8833' showShadow='0' drawAnchors='1'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1'  caption='All Injury Frequency Rate' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint'><categories><category label='Jan' /><category label='Feb' /><category label='Mar' /><category label='Apr' /><category label='May' /><category label='Jun' /><category label='Jul' /><category label='Aug' /><category label='Sep' /><category label='Oct' /><category label='Nov' /><category label='Dec' /></categories><dataset seriesname='Incidents' renderas='bar'><set value='18' /><set value='13' /><set value='11' /><set value='15' /><set value='25' /><set value='10' /><set value='22' /><set value='21' /><set value='32' /><set value='10' /><set value='21' /><set value='27' /></dataset><dataset seriesname='Rate' parentyaxis='S' renderas='line' showvalues='0'><set value='0.48' /><set value='0.36' /><set value='0.46' /><set value='0.44' /><set value='0.49' /><set value='0.32' /><set value='0.27' /><set value='0.33' /><set value='0.41' /><set value='0.26' /><set value='0.31' /><set value='0.30' /></dataset></chart>">
<cfoutput><!--- scrollcombidy2d --->
<script type="text/javascript">

FusionCharts.ready(function () {
    var multiseriesChart = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer1",
        "width": "100%",
        "height": "98%",
        "dataFormat": "xml",
        "dataSource":  "#chart1xml#"
		
    }
	
	);

    multiseriesChart.render();
var multiseriesChart2 = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer2",
        "width": "98%",
        "height": "98%",
        "dataFormat": "xml",
        "dataSource":  "#chart2xml#"

    });

    multiseriesChart2.render();
var multiseriesChart3 = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer3",
        "width": "98%",
        "height": "98%",
        "dataFormat": "xml",
        "dataSource":  "#chart3xml#"

    });

    multiseriesChart3.render();
	var multiseriesChart4 = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer4",
        "width": "98%",
        "height": "98%",
        "dataFormat": "xml",
        "dataSource":  "#chart4xml#"

    });

    multiseriesChart4.render();
});
</script>
</cfoutput>
<div class="content4of4" ><div align="center" class="h2">Health, Safety, Security and Environmental Performance Dashboard</div></div>
<div class="content4of4" ><div class="h3">Headline Trending Data</div></div>

<div class="content1of4">
	<!--- <div style="padding-top: 0px;padding-bottom: 0px;padding-left: 1px;padding-right: 1px;border:1px solid;border-radius: 4px;"> ---><!--- <table align="center" cellspacing="0" border="0"><tr><td valign="top" align="center" style="border:1px solid;border-radius: 4px;"  > ---><div id="chartContainer1" class="homechartl" align="center"></div><div class="homechartr"  align="center"><table border="0" height="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="middle">YTD<br><strong style="color:#ff0000;">0.21</strong><br><img src="images/redArrow.png" border="0" width="40"></td></tr></table></div><!--- </td><td   align="center"  style="border:1px solid;border-radius: 4px;">YTD<br><br><strong style="color:#ff0000;">0.21</strong><br><br><img src="images/redArrow.png" border="0"></td></tr></table> ---><!--- </div> ---></div>
<div class="content1of4r">
<div id="chartContainer2" class="homechart"  align="center"></div><div  class="homechartr"  align="center"><table border="0" height="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="middle">YTD<br><strong style="color:#00b050;">0.17</strong><br><img src="images/greenArrow.png" border="0" width="40"></td></tr></table></div>
<!--- 
<div style="padding-top: 0px;padding-bottom: 0px;padding-left: 1px;padding-right: 1px;"><table align="center" cellspacing="0" border="0"><tr><td valign="top" align="center" style="border:1px solid;border-radius: 4px;" id="chartContainer2"></td><td  align="center"  style="border:1px solid;border-radius: 4px;">YTD<br><br><strong style="color:#00b050;">0.17</strong><br><br><img src="images/greenArrow.png" border="0"></td></tr></table></div> ---></div>
<div class="content1of4r">
<div id="chartContainer3" class="homechart"  align="center"></div><div  class="homechartr" align="center"><table border="0" height="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="middle">YTD<br><strong style="color:#ffc000;">0.14</strong><br><img src="images/yellowArrow.png" border="0" width="40"></td></tr></table></div>
<!--- 
<div style="padding-top: 0px;padding-bottom: 0px;padding-left: 1px;padding-right: 1px;"><table align="center" cellspacing="0" border="0"><tr><td valign="top" align="center" style="border:1px solid;border-radius: 4px;"  id="chartContainer3"></td><td  align="center"  style="border:1px solid;border-radius: 4px;">YTD<br><br><strong style="color:#ffc000;">0.14</strong><br><br><img src="images/yellowArrow.png" border="0"></td></tr></table></div> ---></div>
<div class="content1of4r">
<div id="chartContainer4" class="homechart"  align="center"></div><div class="homechartr"  align="center"><table border="0" height="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="middle">YTD<br><strong style="color:#00b050;">0.08</strong><br><img src="images/greenArrow.png" border="0" width="40"></td></tr></table></div>
<!--- 
<div class="content1of4r"><div style="padding-top: 0px;padding-bottom: 0px;padding-left: 1px;padding-right: 1px;"><table align="center" cellspacing="0" border="0"><tr><td valign="top" align="center" style="border:1px solid;border-radius: 4px;" id="chartContainer4"></td><td  align="center"  style="border:1px solid;border-radius: 4px;">YTD<br><br><strong style="color:#00b050;">0.08</strong><br><br><img src="images/greenArrow.png" border="0"></td></tr></table></div> ---></div>

<div class="content4of10">

	<table cellpadding="4" cellspacing="3" align="center" width="99%">
		<tr>
			<td class="h3" align="left">Leading indicator performance</td>
		</tr>
		<tr>
			<td style="border:1px solid;border-radius: 4px;">
				<table cellpadding="4" cellspacing="0" border="0" width="99%">
					<tr>
						<td  width="80%"><strong style="color:#8f8f8f;font-size:12pt;">Beyond zero refresh</strong><br>
						<span style="font-size:10pt;"><cfoutput>(#ztotplan# planned of which #ztotcomp# completed)</cfoutput><br>Combining Beyond Zero / Realising zero tools and processes.</span></td>
						<td align="center" >
						
						<cfif zplantodate gt ztotcomp>
							<img src="images/redLrg.png" border="0">
						<cfelseif zplantodate lt ztotcomp>
							<img src="images/greenLrg.png" border="0">
						<cfelse>
							<img src="images/yellowLrg.png" border="0">
						</cfif></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border:1px solid;border-radius: 4px;">
				<table cellpadding="4" cellspacing="0" border="0" width="99%">
					<tr>
						<td  width="80%"><strong style="color:#8f8f8f;">Performance standard implementation</strong><br>
						<span style="font-size:10pt;"><cfoutput>(#psitotplan# planned of which #psitotcomp# completed)</cfoutput><br>Assurance based process to benchmark against the company standards.</span></td>
						<td align="center" >
							<table cellpadding="1" border="0">
								
								<tr>
									<cfoutput query="getleading" group="dialid">
									<td align="center" width="49%">#name#<br><cfif structkeyexists(psistruct,dialid)><img src="images/#psistruct[dialid]#" border="0"></cfif>
									<cfif currentrow mod 2 eq 0></tr><cfelse></td></cfif>
									</cfoutput>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	<tr>
			<td style="border:1px solid;border-radius: 4px;">
				<table cellpadding="4" cellspacing="0" border="0" width="99%">
					<tr>
						<td  width="80%"><strong style="color:#8f8f8f;">HSSE Accountability</strong><br>
						<span style="font-size:10pt;"><cfoutput>(#hsseacctotplan# planned of which #hsseacctotcomp# completed)</cfoutput><br>Elevating accountability for critical risk roles.</span></td>
						<td align="center" >
							<table cellpadding="1" border="0">
								
								<tr>
									<cfoutput query="getleading" group="dialid">
									<td align="center" width="49%">#name#<br><cfif structkeyexists(hssestruct,dialid)><img src="images/#hssestruct[dialid]#" border="0"></cfif>
									<cfif currentrow mod 2 eq 0></tr><cfelse></td></cfif>
									</cfoutput>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border:1px solid;border-radius: 4px;">
				<table cellpadding="4" cellspacing="0" border="0" width="99%">
					<tr>
						<td  width="80%"><strong style="color:#8f8f8f;">Risk management</strong><br>
						<span style="font-size:10pt;"><cfoutput>(#rmtotplan# planned of which #rmtotcomp# completed)</cfoutput><br>Establish risks and control through the deployment of HSSE risk registers.</span></td>
						<td align="center" >
							<table cellpadding="1" border="0">
								
								<tr>
									<cfoutput query="getleading" group="dialid">
									<td align="center" width="49%">#name#<br><cfif structkeyexists(rmstruct,dialid)><img src="images/#rmstruct[dialid]#" border="0"></cfif>
									<cfif currentrow mod 2 eq 0></tr><cfelse></td></cfif>
									</cfoutput>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr> 
	</table>
</div>
<div class="content3of10r">
	<table cellpadding="4" cellspacing="3" align="center"  width="99%" >
		<tr>
			<td class="h3">Target performance to date</td>
		</tr>
		<tr>
			<td>
				<table cellpadding="4" cellspacing="1" border="0" width="99%">
					<tr bgcolor="#26898d">
						<td>&nbsp;</td>
						<td align="center"><strong style="color:#ffffff;">2015 Target</strong></td>
						<td align="center"><strong style="color:#ffffff;">YTD Performance</strong></td>
						<td align="center"><strong style="color:#ffffff;">Million Man Hours</strong></td>
					</tr>
					<tr bgcolor="#efefef">
						<td>TRFR</td>
						<td align="center">0.00</td>
						<td align="center" bgcolor="#ff0000">0.31</td>
						<td align="center" rowspan="2">53.43</td>
					</tr>
					<tr bgcolor="#bfbfbf">
						<td>LTIFR</td>
						<td align="center">0.00</td>
						<td align="center" bgcolor="#ff0000">0.068</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table cellpadding="4" cellspacing="1" bgcolor="#000000" width="99%" >
					<tr bgcolor="#26898d" style="height:37px;">
						<td rowspan="8" width="12%" align="center"><img src="images/consequences.png" border="0"></td>
						<td width="23%" align="center" rowspan="2"><strong style="font-size:12pt;color:#ffffff;">Potential</strong></td>
						<td width="60%" align="center"  colspan="5"><strong style="font-size:12pt;color:#ffffff;">No. of People</strong></td>
						
					</tr>
					<tr bgcolor="#26898d" style="height:37px;">
						
						
						<td width="12%" align="center"  ><strong style="font-size:12pt;color:#ffffff;">0</strong></td>
						<td width="12%" align="center"  ><strong style="font-size:12pt;color:#ffffff;">1</strong></td>
						<td width="12%" align="center"  ><strong style="font-size:12pt;color:#ffffff;">1-2</strong></td>
						<td width="12%" align="center"  ><strong style="font-size:12pt;color:#ffffff;">3-10</strong></td>
						<td width="12%" align="center"  ><strong style="font-size:12pt;color:#ffffff;">10+</strong></td>
					</tr>
					<tr style="height:37px;">
						
						<td width="23%"  align="center" rowspan="2" bgcolor="#dfdfdf"><strong style="font-size:12pt;color:#000000;">Low</strong></td>
						<td width="12%" align="center" bgcolor="#00b050" align="center"><span style="color:#ffffff;font-size:10pt;">98</span></td>
						<td width="12%" align="center" bgcolor="#00b050"><span style="color:#ffffff;font-size:10pt;">350</span></td>
						<td width="12%" align="center" bgcolor="#00b050"><span style="color:#ffffff;font-size:10pt;">28</span></td>
						<td width="12%" align="center" bgcolor="#00b050"><span style="color:#ffffff;font-size:10pt;">9</span></td>
						<td width="12%" align="center" bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;">98</span></td>
					</tr>
					<tr style="height:37px;">
						
						
						<td width="12%" align="center" bgcolor="#00b050"><span style="color:#ffffff;font-size:10pt;">1</span></td>
						<td width="12%" align="center" bgcolor="#00b050"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#00b050"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;"></span></td>
					</tr>
					<tr  style="height:37px;">
						
						<td width="23%"  align="center" bgcolor="#dfdfdf"><strong style="font-size:12pt;color:#000000;">Medium</strong></td>
						<td width="12%"  align="center" bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;"></span></td>
						<td width="12%" align="center"  bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;">5</span></td>
						<td width="12%" align="center" bgcolor="#ffc000"><span style="color:#000000;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;">28</span></td>
					</tr>
					<tr  style="height:37px;">
						
						<td width="23%"  align="center" rowspan="2" bgcolor="#dfdfdf"><strong style="font-size:12pt;color:#000000;">High</strong></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;">3</span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
					</tr>
					<tr style="height:37px;">
					
						
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
						<td width="12%" align="center" bgcolor="#ff0000"><span style="color:#ffffff;font-size:10pt;"></span></td>
					</tr>
					<tr style="height:37px;">
						
						<td width="23%"  align="center" rowspan="2" bgcolor="#dfdfdf" style="font-size:10pt;color:#000000;">Incident potential to be determined = <strong>0</strong></td>
						<td width="12%" align="center" bgcolor="#ffffff" ><strong>A</strong></td>
						<td width="12%" align="center" bgcolor="#ffffff"><strong>B</strong></td>
						<td width="12%" align="center" bgcolor="#ffffff"><strong>C</strong></td>
						<td width="12%" align="center" bgcolor="#ffffff"><strong>D</strong></td>
						<td width="12%" align="center" bgcolor="#ffffff"><strong>E</strong></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<div class="content3of10r">
	<table  cellpadding="4" cellspacing="2" width="99%" align="center" >
		<tr>
			<td class="h3">Monthly key Incident data</td>
		</tr>
		<tr>
			<td>
				<table cellpadding="4" cellspacing="2" border="0" width="99%" bgcolor="#ffffff">
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td bgcolor="#ffffff">&nbsp;</td>
						<td bgcolor="#26898d" align="center"><span style="color:#ffffff;font-size:12pt;">YTD</span></td>
						<td bgcolor="#26898d" align="center"><span style="color:#ffffff;font-size:12pt;">Current</span></td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">Total recordable cases</td>
						<td align="center" style="color:#000000;font-size:10pt;">12</td>
						<td align="center" style="color:#000000;font-size:10pt;">3</td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">Lost time incidents</td>
						<td align="center" style="color:#000000;font-size:10pt;">8</td>
						<td align="center" style="color:#000000;font-size:10pt;">0</td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">All injury incidents</td>
						<td align="center" style="color:#000000;font-size:10pt;">342</td>
						<td align="center" style="color:#000000;font-size:10pt;">23</td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">Environmental incidents</td>
						<td align="center" style="color:#000000;font-size:10pt;">7</td>
						<td align="center" style="color:#000000;font-size:10pt;">3</td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">Occupational health cases</td>
						<td align="center" style="color:#000000;font-size:10pt;">123</td>
						<td align="center" style="color:#000000;font-size:10pt;">29</td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">Security incidents</td>
						<td align="center" style="color:#000000;font-size:10pt;">23</td>
						<td align="center" style="color:#000000;font-size:10pt;">0</td>
					</tr>
					<tr bgcolor="#dfdfdf" style="height:50px;">
						<td style="color:#000000;font-size:10pt;">High potential incidents</td>
						<td align="center" style="color:#000000;font-size:10pt;">6</td>
						<td align="center" style="color:#000000;font-size:10pt;">1</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<div class="content4of4"><hr align="center" color="#5f2167" size="1" width="99%"></div>
<div class="content1of9"><div class="h3sm">Trending Colour Key:</div></div>
<div class="content1of9r"><div class="h3sm"><img src="images/greensm.png" border="0"  style="vertical-align: text-bottom;" height="18">&nbsp;Good/Ahead  </div></div>
<div class="content1of9r"><div class="h3sm"><img src="images/yellowsm.png" border="0" style="vertical-align: text-bottom;" height="18">&nbsp;Stable/On-Track  </div></div>
<div class="content1of9r"><div class="h3sm"><img src="images/redsm.png" border="0" style="vertical-align: text-bottom;" height="18">&nbsp;Poor/Behind Schedule</div></div>

