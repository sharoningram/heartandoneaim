<cfset chart1xml = "<chart caption='Lost Time Incident Frequency' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint'><categories><category label='Jan' /><category label='Feb' /><category label='Mar' /><category label='Apr' /><category label='May' /><category label='Jun' /><category label='Jul' /><category label='Aug' /><category label='Sep' /><category label='Oct' /><category label='Nov' /><category label='Dec' /></categories><dataset seriesname='Incidents' renderas='bar'><set value='6' /><set value='23' /><set value='7' /><set value='23' /><set value='5' /><set value='17' /><set value='8' /><set value='3' /><set value='22' /><set value='15' /><set value='11' /><set value='9' /></dataset><dataset seriesname='Rate' parentyaxis='S' renderas='line' showvalues='0'><set value='0.32' /><set value='0.35' /><set value='0.26' /><set value='0.53' /><set value='0.19' /><set value='0.22' /><set value='0.37' /><set value='0.21' /><set value='0.13' /><set value='0.16' /><set value='0.11' /><set value='0.10' /></dataset></chart>">
<cfoutput>
<script type="text/javascript">


FusionCharts.ready(function () {
    var multiseriesChart = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer1",
        "width": "100%",
        "height": "100%",
        "dataFormat": "xml",
        "dataSource":  "#chart1xml#"

    });

    multiseriesChart.render();

});
</script>
</cfoutput>
<div id="chartContainer1" style="height:40%;width:25%;"></div>