<cfset timel = "00:00,00:15,00:30,00:45,01:00,01:15,01:30,01:45,02:00,02:15,02:30,02:45,03:00,03:15,03:30,03:45,04:00,04:15,04:30,04:45,05:00,05:15,05:30,05:45,06:00,06:15,06:30,06:45,07:00,07:15,07:30,07:45,08:00,08:15,08:30,08:45,09:00,09:15,09:30,09:45,10:00,10:15,10:30,10:45,11:00,11:15,11:30,11:45,12:00,12:15,12:30,12:45,13:00,13:15,13:30,13:45,14:00,14:15,14:30,14:45,15:00,15:15,15:30,15:45,16:00,16:15,16:30,16:45,17:00,17:15,17:30,17:45,18:00,18:15,18:30,18:45,19:00,19:15,19:30,19:45,20:00,20:15,20:30,20:45,21:00,21:15,21:30,21:45,22:00,22:15,22:30,22:45,23:00,23:15,23:30,23:45">

<cfset fileguid = createuuid()>

<!--- <cfscript>
document.incidentfrm.ObvType.value='Form.OBSERVATIONTYPE';
</cfscript> --->

<script type="text/javascript">

 function popgroupinfo(gnum){

 
  <cfoutput>
 http = new XMLHttpRequest()
 var params = "&gnum=" + gnum + "&currloc=0";
		var myurl = "#self#?fuseaction=#attributes.xfa.popgroupinfo#" ;
		//alert(myurl + ' ' + params);
		http.open('POST', myurl , true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopupGinfo;
 
 </cfoutput>
 
 }

function popcountryinfo(sitenum){
 
 
  <cfoutput>
 http = new XMLHttpRequest()
 var params = "&sitenum=" + sitenum;
		var myurl = "#self#?fuseaction=#attributes.xfa.popgroupinfo#" ;
		// alert(myurl + ' ' + params);
		http.open('POST', myurl , true);\
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopupCinfo;
 
 </cfoutput>
 
 }

 	/* Function called when a user select BU. */
	function populatelistOU(val2) {
		// Set var params to the value of the BU
	  <cfoutput>	
	   http = new XMLHttpRequest()
		var params = "&OperatingUnit=" + val2;
		// Build the URL that we call to do the lookup
		
		var myurl = "#self#?fuseaction=#attributes.xfa.popgroupinfo#" ;
		//alert(myurl);
		// Open the connection and specify the handlePopup event when the state changes.
		http.open('POST', myurl , true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopupOUinfo;
  </cfoutput>		
	}	
	/* When the populatelist function returns its value it calls this function to change the drop down select list.
	the charater strings {kk{ and }kk} are used in the action file to identify the HTML we want to display and separate it from the rest
	of the page */
 
  function handlePopupOUinfo(){

		if (http.readyState == 4){
			
			var startpos11 = http.responseText.search("{kk{") + 4;
			var endpos11 = http.responseText.search("}kk}");
			var textout11 = http.responseText.substring(startpos11,endpos11);
			//alert("Here2");
			document.getElementById("OUTD").innerHTML=textout11;
			document.incidentfrm.project.value="";	
			document.incidentfrm.site.value="";	
			document.incidentfrm.BusinessStream.value="";							
			}
	}
 
 function populatelistProject(val3) {

		// Set var params to the value of the OU
	 <cfoutput>	
	   http = new XMLHttpRequest()
		var params = "&Project=" + val3;
		// Build the URL that we call to do the lookup
		
		var myurl = "#self#?fuseaction=#attributes.xfa.popgroupinfo#" ;
		//alert(myurl);
		// Open the connection and specify the handlePopup event when the state changes.
		http.open('POST', myurl , true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopupProjectinfo;
  </cfoutput>		
	}	
 
   function handlePopupProjectinfo(){

		if (http.readyState == 4){
			//document.getElementById("OUTD").style.visibility="hidden";
			var startpos11 = http.responseText.search("{mm{") + 4;
			var endpos11 = http.responseText.search("}mm}");
			var textout11 = http.responseText.substring(startpos11,endpos11);
			//alert("Here2");
			document.getElementById("ProjTD").innerHTML=textout11;
			document.incidentfrm.site.value="";	
			document.incidentfrm.BusinessStream.value="";		
			
								
			}
	}
 
  function populateBS(val4) {

		// Set var params to the value of the OU
	 <cfoutput>	
	   http = new XMLHttpRequest()
		var params = "&ProjectBS=" + val4;
		// Build the URL that we call to do the lookup
		
		var myurl = "#self#?fuseaction=#attributes.xfa.popgroupinfo#" ;
		//alert(params);
		// Open the connection and specify the handlePopup event when the state changes.
		http.open('POST', myurl , true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopupBSinfo;
  </cfoutput>		
	}	
 
   function handlePopupBSinfo(){

		if (http.readyState == 4){		
			
			var startpos2 = http.responseText.search("{bb{") + 4;
			var endpos2 = http.responseText.search("}bb}");
			var textout2 = http.responseText.substring(startpos2,endpos2);
			document.getElementById("BSTD").innerHTML=textout2;
			//document.incidentfrm.BusinessStream.value=textout2;		
			
			var startpos3 = http.responseText.search("{dd{") + 4;
			var endpos3 = http.responseText.search("}dd}");
			var textout3 = http.responseText.substring(startpos3,endpos3);
			document.getElementById("SOTD").innerHTML=textout3;
						
			}
	}
 
  function handlePopupCinfo(){
		if (http.readyState == 4){
			var startpos4 = http.responseText.search("{cc{") + 4;
			var endpos4 = http.responseText.search("}cc}");
			var textout4 = http.responseText.substring(startpos4,endpos4);
			document.getElementById("CNTYTD").innerHTML=textout4;
			document.incidentfrm.Country.value=textout4;
		
		}
		}

						
		
 function handlePopupGinfo(){
		if (http.readyState == 4){
			// document.getElementById("BUTD").style.visibility="visible";
			var startpos8 = http.responseText.search("{zz{") + 4;
			var endpos8 = http.responseText.search("}zz}");
			var textout8 = http.responseText.substring(startpos8,endpos8);
			document.getElementById("BUTD").innerHTML=textout8;
			//alert(textout8);
			document.incidentfrm.BusinessUnit.value=textout8;
			
				var startpos11 = http.responseText.search("{oo{") + 4;
			var endpos11 = http.responseText.search("}oo}");
			var textout11 = http.responseText.substring(startpos11,endpos11);
			document.getElementById("OUTD").innerHTML=textout11;
			document.incidentfrm.OperatingUnit.value=textout11;
			
				var startpos2 = http.responseText.search("{bb{") + 4;
			var endpos2 = http.responseText.search("}bb}");
			var textout2 = http.responseText.substring(startpos2,endpos2);
			document.getElementById("BSTD").innerHTML=textout2;
			document.incidentfrm.BusinessStream.value=textout2;
			
				var startpos3 = http.responseText.search("{dd{") + 4;
			var endpos3 = http.responseText.search("}dd}");
			var textout3 = http.responseText.substring(startpos3,endpos3);
			document.getElementById("SOTD").innerHTML=textout3;
			
				var startpos4 = http.responseText.search("{cc{") + 4;
			var endpos4 = http.responseText.search("}cc}");
			var textout4 = http.responseText.substring(startpos4,endpos4);
			document.getElementById("CNTYTD").innerHTML=textout4;
			document.incidentfrm.Country.value=textout4;
			
			}
		
	}

function checkdescvalu(descvalu){
if(document.incidentfrm.Observationdesc.value=="Please enter a short description of the Observation. Do not include any personal details, e.g. name of the individual(s) involved."){
	document.incidentfrm.Observationdesc.value="";
}
}	

//document.getElementById("enftypel").style.display='none';
//document.getElementById("enftypefld").style.display='none';
document.getElementById("file2").style.display='none';
document.getElementById("file3").style.display='none';
document.getElementById("file4").style.display='none';
document.getElementById("file5").style.display='none';

/*
function checkenftype(sntval){
	if(sntval==1){
	document.getElementById("enftypefld").style.display='';
	document.getElementById("enftypel").style.display='';
	}
	else {
	document.getElementById("enftypefld").style.display='none';
	document.getElementById("enftypel").style.display='none';
	}
}
*/
function checksrval(sntval){
document.getElementById("rulebrotherrw").style.display='none';
document.getElementById("rulebrotherrwfld").style.display='none';
document.getElementById("brnonelbl").style.display='none';
	document.getElementById("brnonefld").style.display='none';
 var x = 0;
    	 for (x=0;x<document.incidentfrm.rulebreach.length;x++)
         {
            if(document.incidentfrm.rulebreach[x].selected)
            {
				
				if(document.incidentfrm.rulebreach[x].value==11){
				document.getElementById("brnonelbl").style.display='';
				document.getElementById("brnonefld").style.display='';
				}
				if(document.incidentfrm.rulebreach[x].value==15){
				document.getElementById("rulebrotherrw").style.display='';
				document.getElementById("rulebrotherrwfld").style.display='';
				}
				
			}
			
			
            } 

}		

function highlimgNone(sntval){
document.getElementById("sebnonefld").style.display='none';
document.getElementById("sebnonelbl").style.display='none';

 var x = 0;
   	 for (x=0;x<document.incidentfrm.essbreach.length;x++)
       {
         if(document.incidentfrm.essbreach[x].selected)
         {
	
	if(document.incidentfrm.essbreach[x].value==7){
	
	document.getElementById("sebnonefld").style.display='';
	document.getElementById("sebnonelbl").style.display='';
	}
	
	}


          } 

}

function updateObvName(obname,obemail){
document.incidentfrm.ObserverName.value=obname;
document.incidentfrm.ObserverEmail.value=obemail;
document.incidentfrm.ObserverName.style.border = "1px solid 5f2167";
}
// Displaying and hiding Team field if pre-selected
/* Do not need to compute the display anymore
if(document.incidentfrm.ObvType.value=='Safe Behaviour'){

				document.getElementById("PostiveObs").style.display='';
				document.getElementById("PostiveObsfld").style.display='';
				document.getElementById("SafetyEss").style.display='none';
				document.getElementById("SafetyRules").style.display='none';
				}
				else {
document.getElementById("PostiveObs").style.display='none';
document.getElementById("PostiveObsfld").style.display='none';
document.getElementById("SafetyEss").style.display='';
document.getElementById("SafetyRules").style.display='';

}
*/

function checkObType(sntval){
document.getElementById("PostiveObs").style.display='none';
//document.getElementById("PostiveObsfld").style.display='none';
document.getElementById("SafetyEss").style.display='';
document.getElementById("SafetyRules").style.display='';
 var x = 0;
    	 for (x=0;x<document.incidentfrm.ObvType.length;x++)
         {
            if(document.incidentfrm.ObvType[x].selected)
            {
				if(document.incidentfrm.ObvType[x].value=='Safe Behaviour'){
				document.getElementById("PostiveObs").style.display='';
				//document.getElementById("PostiveObsfld").style.display='';
				document.getElementById("SafetyEss").style.display='none';
				document.getElementById("SafetyRules").style.display='none';
				}
				
			}
			
			
            } 

}	


	function longcalendarwday_open(dateObj, rowno, dayofweekObj)

{

 var vRet = window.showModalDialog( 'long_calendar.htm', null, 'dialogHeight: 222px; dialogWidth: 266px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;');
    if ( null != vRet ) {
     //set the date object with the return value from the calendar window.
     dateObj.value = vRet;
	// dateObj.style.border = "1px solid 5f2167";
	
	var tday = vRet.substring(0,2);
	var startpos = vRet.length - 4;
	
	var tyear = vRet.substring(startpos,vRet.length);
	var tmonth = vRet.substring(3,vRet.length-5);
	var myDate = new Date(eval('"' + tday + " " + tmonth + " " + tyear + '"'));
	var myDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
	var dayoweek = myDays[myDate.getDay()];
	
	dayofweekObj.value = dayoweek;
	dayofweekObj.style.border = "1px solid 5f2167";
	// checkfordays();

        if (rowno > 0)
       markchangedid(rowno);
          dateObj.style.backgroundcolor="##F7FE6B";
		 
    }

}
</script>

<!--- Validation Starts --->
<script type="text/javascript">
function isEmail (s,message,name)
{
	var i,ii;
	var j;
	var k,kk;
    var jj;
    var len;
    if (s.length > 0)
	{
		i=s.indexOf("@");
		ii=s.indexOf("@",i+1);
		j=s.indexOf(".",i);
		k=s.indexOf(",");
		kk=s.indexOf(" ");
		jj=s.lastIndexOf(".")+1;
		len=s.length;
		if ((i>0) && (j>(1+1)) && (k==-1) && (ii==-1) && (kk==-1) &&
			(len-jj >=2) && (len-jj<=3)) 
		{}
		else
		{
	 		alert(message);
			return false;
		}
		return true;
	}
	else   
	{
	   alert(message);
       return false;
	}   
}		


function submitsfrm(){

var oktosubmit = "yes"

if(document.incidentfrm.BusinessUnit.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.BusinessUnit.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.BusinessUnit.style.border = "1px solid 5f2167";
	}
	
if(document.incidentfrm.OperatingUnit.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.OperatingUnit.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.OperatingUnit.style.border = "1px solid 5f2167";
	}
	
if(document.incidentfrm.project.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.project.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.project.style.border = "1px solid 5f2167";
	}	

if(document.incidentfrm.site.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.site.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.site.style.border = "1px solid 5f2167";
	}	
	
if(document.incidentfrm.ExactLoc.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.ExactLoc.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.ExactLoc.style.border = "1px solid 5f2167";
	}	

if(document.incidentfrm.ObvType.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.ObvType.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.ObvType.style.border = "1px solid 5f2167";
	}	
	
if(document.incidentfrm.ObserverName.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.ObserverName.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.ObserverName.style.border = "1px solid 5f2167";
	}	
	
if(document.incidentfrm.ObserverEmail.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.ObserverEmail.style.border = "2px solid F00000";

	}
	else{
	
	if (!isEmail(document.incidentfrm.ObserverEmail.value,'Observer email must be a valid email address.')){
			var oktosubmit = "no";
			 document.incidentfrm.ObserverEmail.style.border = "2px solid F00000";
			 
		}	
	document.incidentfrm.ObserverEmail.style.border = "1px solid 5f2167";
}		

if((document.incidentfrm.IndividualTeamName.value=="")&&(document.getElementById('PostiveObs').style.display=='')){
	
	var oktosubmit = "no";
	 document.incidentfrm.IndividualTeamName.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.IndividualTeamName.style.border = "1px solid 5f2167";
	}	
		
if(document.incidentfrm.Stakeholder.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.Stakeholder.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.Stakeholder.style.border = "1px solid 5f2167";
	}	

if(document.incidentfrm.incidentdate.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.incidentdate.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.incidentdate.style.border = "1px solid 5f2167";
	}	

if(document.incidentfrm.incidentdate.value!=""){

// check date complete
	var tday = document.incidentfrm.incidentdate.value.substring(0,2);
	var startpos = document.incidentfrm.incidentdate.value.length - 4;
	var tyear = document.incidentfrm.incidentdate.value.substring(startpos,document.incidentfrm.incidentdate.length);
	var tmonth = document.incidentfrm.incidentdate.value.substring(3,document.incidentfrm.incidentdate.value.length-5);
	var myDate = new Date(tmonth + '' + tday + ',' + tyear + ' 00:00:00'); 
	
if(new Date() <= new Date(myDate))
	{//compare end <=, not >=
 		alert('Please do not select a future date');
			var oktosubmit = "no";
	 		document.incidentfrm.incidentdate.style.border = "2px solid F00000";
	}
}	
	
	
if(document.incidentfrm.hours.value==""){
	
	var oktosubmit = "no";
	 document.incidentfrm.hours.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.hours.style.border = "1px solid 5f2167";
	}	
		
	if((document.incidentfrm.rulebreach.value=="")&&(document.getElementById('SafetyRules').style.display=='')){
	
	var oktosubmit = "no";
	 document.incidentfrm.rulebreach.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.rulebreach.style.border = "1px solid 5f2167";
	}		
	
if((document.incidentfrm.ruleother.value=="")&&(document.getElementById('rulebrotherrwfld').style.display=='')){
	
	var oktosubmit = "no";
	 document.incidentfrm.ruleother.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.ruleother.style.border = "1px solid 5f2167";
	}		
			
if((document.incidentfrm.essbreach.value=="")&&(document.getElementById('SafetyEss').style.display=='')){
	
	var oktosubmit = "no";
	 document.incidentfrm.essbreach.style.border = "2px solid F00000";

	}
	else{
	document.incidentfrm.essbreach.style.border = "1px solid 5f2167";
	}				
				
if(document.incidentfrm.Observationdesc.value==""){
	//alert("Here no desc");	
	var oktosubmit = "no";
	 document.incidentfrm.Observationdesc.style.border = "2px solid F00000";
	}
	else{	
	document.incidentfrm.Observationdesc.style.border = "1px solid 5f2167";	
}


  if((document.getElementById('Feedback1').checked==false)&&(document.getElementById('Feedback2').checked==false)){
	
		var oktosubmit = "no";
		 document.getElementById('Feedback').style.border = "2px solid F00000";
			   
	}
	else{
	document.getElementById('Feedback').style.border = "0px solid 5f2167";
	}	

if (oktosubmit=="no"){
	document.getElementById("reqflds").style.display='';
	return false;
} 

}
</script>
<script>

$(function() {

    $( "#datepicker1" ).datepicker({dateFormat: 'dd-MM-yy',
				showOn: "button",
                buttonImage: "images/calendar.jpg",
				buttonText: "",
				changeMonth: true,
				changeYear: true,
				monthNamesShort: ["January","February","March","April","May","June",
			"July","August","September","October","November","December"],
                buttonImageOnly: true
 });

  });
  </script>
<!--- Validation Ends --->

<!--- <div align="center" class="midbox">
	<div align="center" class="midboxtitle">What would you like to report?</div> --->
<cfoutput>	
<cfform action="index.cfm?fuseaction=main.AddObservation" method="post" name="incidentfrm" enctype="multipart/form-data" onsubmit="return submitsfrm();">	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="90%">
							<tr id="reqflds">
								<td colspan="4" class="redText" align="center">Please fill in all required fields</td>
							</tr>
							<tr>
								<td colspan="4" class="redText">All data entered must be in English</td>
							</tr>
							<tr>
								<td class="formlable" align="left" width="25%"><strong>Originator:</strong></td>
								<td class="bodyTextGrey" width="25%">#request.fname# #request.lname#</td>
								<input type="hidden" name="originatorname" value="#request.fname# #request.lname#">
								<input type="hidden" name="originatoremail" value="#request.userlogin#">
								<td class="formlable" align="left" width="25%"><strong>Date Created:</strong></td>
								<td class="bodyTextGrey" width="25%">#dateformat(now(),"dd-mmm-yyyy")#</td>
								<input type="hidden" name="datecreated" value="#now()#">
								
							</tr>
							<tr>
								<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#:*</strong></td>
								<td class="bodyTextGrey" width="25%" >
								<!--- <input type="hidden" name="BusinessUnit" value="">	 --->
								<cfselect name="BusinessUnit" size="1" class="selectgen" id="lrgselect"  style="width:200px" onchange="populatelistOU(this.value);">
																<option value="">-- Select One --</option>
																<cfloop query="getBUs">
																<option value="#ID#" >#name#</option>
																</cfloop>
																</cfselect>
								</td>								
									
								<td class="formlable" align="left" width="25%">
								<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Project/Office:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=ProjectSite','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table>
								</td>
								<td class="bodyTextGrey" width="25%" id="ProjTD"><cfselect name="project" size="1" class="selectgen" id="lrgselect"   style="width:200px" onchange="populateBS(this.value);">
																<option value="">-- Select One --</option>
																<!--- <cfloop query="getProjects">
																<option value="#group_number#" >#group_name#</option>
																</cfloop>--->
																</cfselect> 
																</td>
							</tr>
							
							<tr>
								<td class="formlable" align="left" width="25%"><strong>#request.oulabellong#:*</strong></td>
								<td class="bodyTextGrey" width="25%" id="OUTD">								
								<cfselect name="OperatingUnit" size="1" class="selectgen" id="ouselect"  style="width:200px" onchange="populatelistProject(this.value);"> 
																<option value="">-- Select One --</option>														
																<!--- <cfloop query="getOUs">
																<option value="#ID#" >#name#</option>
																</cfloop> --->
																</cfselect>
								</td>
								<td class="formlable" align="left" width="25%">
								<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Site/Office Name:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=ProjectSite','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table>
							</td>
							<td class="bodyTextGrey" width="25%"  id="SOTD" >
													<cfselect name="site" size="1" class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';popcountryinfo(this.value);" style="width:200px">
															<option value="">-- Select One --</option>
															</cfselect>	 
											
							</td>	
																								
						</tr>
							
							
						<tr>
												
							<td class="formlable" align="left" width="25%"><strong>Business Stream:</strong></td>
								<td class="bodyTextGrey" width="25%" id="BSTD" colspan="3">
								<!--- <input type="hidden" name="BusinessStream" value=""> --->
								<cfselect name="BusinessStream" size="1" class="selectgen" id="lrgselect"  style="width:200px" > 
																<option value="">-- Select One --</option>													
																</cfselect>
							</td>	
							<input type="hidden" name="Country" value="">
						</tr>
						
						<tr>
							<td class="formlable" align="left">
							<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Exact Location of Act/Condition/Safe Behaviour:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=exactLocation','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table>
							</td>
							<td class="bodyTextGrey" colspan="3">
							<textarea name="ExactLoc" rows="4" cols="122" class="selectgen"   id="widetxt" ></textarea>
							<!--- <input type="text" name="ExactLoc"  value="" size="30" maxlength="110" class="selectgen" onkeypress="this.style.border='1px solid 5f2167';"> --->
							</td>
						</tr>
						
								<tr>
							<td class="formlable" align="left"><strong>Event Type:*</strong></td> 
							<td class="bodyTextGrey" colspan="3">
								<!--- <cfselect name="ObvType" size="1" class="selectgen" id="lrgselect" onchange="checkObType(this.value);"  style="width:200px">
									<option value="">-- Select One --</option>									
											<option value="Unsafe Act" <cfif form.ObservationType eq "Unsafe Act">selected</cfif>>Unsafe Act</option>
											<option value="Unsafe Condition" <cfif form.ObservationType eq "Unsafe Condition">selected</cfif>>Unsafe Condition</option>
											<option value="Safe Behaviour" <cfif form.ObservationType eq "Safe Behaviour">selected</cfif>>Safe Behaviour</option>									
								</cfselect>	 --->		
								<cfselect name="ObvType" size="1" class="selectgen" id="lrgselect" onchange="checkObType(this.value);"  style="width:200px">
									<option value="">-- Select One --</option>									
											<option value="Unsafe Act" >Unsafe Act</option>
											<option value="Unsafe Condition" >Unsafe Condition</option>
											<option value="Safe Behaviour" >Safe Behaviour</option>									
								</cfselect>													
							</td>		
							<!--- <td class="formlable" align="left" id="PostiveObs"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Name of Individual or Team:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=exactLocation','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table></td>
							<td class="bodyTextGrey" colspan="3"id="PostiveObsfld"><input type="text" name="IndividualTeamName"  value="" size="30" maxlength="110" class="selectgen" onkeypress="this.style.border='1px solid 5f2167';"></td>		 --->											
						</tr>
						
						<!--- <tr>
							<td class="formlable" align="left"><strong>Where did this happen?:*</strong></td>
							<td class="bodyTextGrey" colspan="3"><span  id="workrelatedtd">Home&nbsp;&nbsp;<input type="radio" name="HomeWork" id="HomeWork1" value="Home"  onclick="document.getElementById('HomeWork').style.border='0px solid ffffff';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Work&nbsp;&nbsp;<input type="radio" name="HomeWork" id="HomeWork2"  value="Work" onclick="document.getElementById('HomeWork').style.border='0px solid ffffff';"></span></td>
														
						</tr> --->
						
						<tr>
							<td class="formlable" align="left"><strong>Observer Name:*</strong></td>
							<td class="bodyTextGrey" colspan="1"><input type="text" name="ObserverName"  value="#request.fname#&nbsp;#request.lname#" size="30" maxlength="110" class="selectgen" onkeypress="this.style.border='1px solid 5f2167';"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/utils/phonebook.cfm?srcscriptname=updateObvName','Info','600','300','no');"><img src="images/phonebook.png" border="0" style="vertical-align:-2;padding-left:6px;"></a></td>
							<td class="formlable" align="left"><strong>Observer Email Address:*</strong></td>
							<td class="bodyTextGrey" colspan="1"><cfinput type="text" name="ObserverEmail"  value="#request.Useremail#" size="30" maxlength="110" class="selectgen" onkeypress="this.style.border='1px solid 5f2167';"></td>
						</tr>
						
						<tr>
							<td class="formlable" align="left"><strong>Stakeholder:*</strong></td>
							<td class="bodyTextGrey" colspan="3">
								<cfselect name="Stakeholder" size="1" class="selectgen" id="lrgselect" style="width:200px">
									<option value="">-- Select One --</option>									
											<option value="Amec Foster Wheeler" >Amec Foster Wheeler</option>											
											<option value="Client" >Client</option>		
											<option value="Contractor" >Contractor</option>
											<option value="3rd Party" >3rd Party</option>							
								</cfselect>															
							</td>															
						</tr>
						
						<tr id="PostiveObs">
							<td class="formlable" align="left" id="PostiveObs1">
							<strong>Name of Individual or Team:*</strong></td>
							<td class="bodyTextGrey" colspan="3"><input type="text" name="IndividualTeamName"  value="" size="30" maxlength="110" class="selectgen" onkeypress="this.style.border='1px solid 5f2167';"></td>	
						</tr> 											
																		
						
						
						
						<!--- <tr>
							<td class="formlable" align="left"><strong>Observer Email Address:*</strong></td>
							<td class="bodyTextGrey" colspan="3"><cfinput type="text" name="ObserverEmail"  value="#request.Useremail#" size="30" maxlength="110" class="selectgen" onkeypress="this.style.border='1px solid 5f2167';"></td>
						</tr> --->

						<tr>
							<td class="formlable" align="left"><strong>Observation Date:*</strong></td>
							<td><input type="text" class="selectgen" size="15" readonly name="incidentdate" value="" id="datepicker1"><!--- &nbsp;<a href="javascript:void(0);"   onClick="longcalendarwday_open(document.incidentfrm.incidentdate,0,document.incidentfrm.dayofweek);return false;"><img src="images/calendar.jpg" style="vertical-align:-4;padding-left:0px;" border="0"></a> ---></td>
							<td class="formlable" align="left"><strong>Observation Time:*</strong></td>
							<td class="bodyTextGrey"><cfselect name="hours" size="1" class="selectgen" onchange="this.style.border='1px solid 5f2167';">
															
								<option value="">-- Select One --</option>
								<cfloop list="#timel#" index="i">
									<option value="#i#" >#i#</option>
								</cfloop>
								</cfselect>
							</td>
						</tr>
									
																							
						
						<tr id="SafetyRules">
							<td class="formlable" align="left" width="25%"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Global Safety Rules:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=globalSafetyRules','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table></td><!--- this.style.border='1px solid 5f2167'; --->
							<td class="bodyTextGrey" width="25%"><cfselect name="rulebreach" size="1" class="selectgen" id="lrgselect" onchange="checksrval(this.value);" style="width:200px">
												<option value="">-- Select One --</option>
												<!--- <cfloop query="getrules">
												<option value="#SafetyRuleID#">#SafetyRule#</option>
												</cfloop> --->
												<cfloop query="getrules">
													<cfif listfindnocase("none,other (please state)",SafetyRule) eq 0>
													<option value="#SafetyRuleID#" <cfif listfindnocase(safetyrule,SafetyRuleID) gt 0>selected</cfif>>#SafetyRule#</option>
													</cfif>
												</cfloop>
												                
												<cfloop query="getrules">
													<cfif listfindnocase("none,other (please state)",SafetyRule) gt 0>
													<option value="#SafetyRuleID#" <cfif listfindnocase(safetyrule,SafetyRuleID) gt 0>selected</cfif>>#SafetyRule#</option>
													</cfif>
												</cfloop>            

												</cfselect>
							</td>
							<td class="formlable" align="left" width="25%" id="brnonelbl"><strong>If none, why?:</strong></td><!---  onkeypress="this.style.border='1px solid 5f2167';" --->
							<td class="bodyTextGrey" width="25%" id="brnonefld"><input type="text" size="30" class="selectgen" name="rbwhy"  value=""></td>
							
							<td class="formlable" align="left" width="25%" id="rulebrotherrw"><strong>Other:*</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
							<td class="bodyTextGrey" id="rulebrotherrwfld"><input type="text" size="30" class="selectgen" name="ruleother"   value=""></td>
					</tr>
					
					<!--- <tr id="rulebrotherrw">
						<td class="formlable" align="left" width="25%"><strong>Other:</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
						<td class="bodyTextGrey" colspan="3"><input type="text" size="30" class="selectgen" name="ruleother"   value=""></td>
					</tr> --->
				<!--- 	
					<script type="text/javascript">
				function highlimg(imgname,imgval){
				<cfloop query="getessentials">
					document.getElementById("essimg#currentrow#").style.border='0px solid ffffff';
				</cfloop>
				if(imgval==7){
				document.getElementById("sebnonefld").style.display='';
				document.getElementById("sebnonelbl").style.display='';
				}
				else{
				document.getElementById("sebnonefld").style.display='none';
				document.getElementById("sebnonelbl").style.display='none';
				}
				document.incidentfrm.essbreach.value=imgval;
				document.getElementById("sespan").style.border="0px solid ffffff";//alert(imgname);
				//alert(imgval);
				
				}
				
				
				</script>
				
				<input type="hidden" name="essbreach" value=""> --->
				<!--- <tr>
					<td class="formlable" align="left" width="25%"><strong>Safety Essential:</strong></td>				
					<td class="bodyTextGrey" width="25%"><!--- this.style.border='1px solid 5f2167'; --->
					<table border="0" cellpadding="2" cellspacing="0" id="sespan"><tr><!--- this.style.border='1px solid 5f2167'; ---><td align="center"><cfloop query="getessentials"><cfif trim(imagename) neq ''><img src="images/#imagename#" width="80" border="0" alt="#SafetyEssential#" onclick="highlimg('essimg#currentrow#',#SafetyEssentialID#);document.getElementById('essimg#currentrow#').style.border='1px solid 5f2167';" id="essimg#currentrow#" ><cfif currentrow eq 4></td></tr><tr><td align="center"></cfif><cfelse>#SafetyEssential#</cfif></cfloop></td></tr></table></td>
					
					<td class="formlable" align="left" width="25%" id="sebnonelbl"><strong>If none, why?</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					<td class="bodyTextGrey" width="25%" id="sebnonefld"><input type="text" size="25" class="selectgen" name="esswhy"   value=""></td>
				</tr>  --->
				<tr id="SafetyEss">
					<td class="formlable" align="left" width="25%">
					<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Safety Essential:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=essbreach','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table>
					</td>
					<td class="bodyTextGrey" width="25%"><cfselect name="essbreach" size="1" class="selectgen" id="lrgselect" onchange="highlimgNone(this.value);" style="width:200px">
										<option value="">-- Select One --</option>
										<cfloop query="getessentials">
										<option value="#SafetyEssentialID#">#SafetyEssential#</option>
										</cfloop>
										</cfselect></td>
					<td class="formlable" align="left" width="25%" id="sebnonelbl"><strong>If none, why?:</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					<td class="bodyTextGrey" width="25%" id="sebnonefld"><input type="text" size="30" class="selectgen" name="esswhy"   value=""></td>
				</tr>		
				<tr>
					<td class="formlable" align="left"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Details of safety observation:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=safetyObvervation','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table></td>
					<td class="bodyTextGrey" colspan="3"><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					
					<textarea name="Observationdesc" rows="4" cols="122" class="selectgen"   id="widetxt" ></textarea>
					
					</td>
				</tr>		
				
				<tr>
					<td class="formlable" align="left"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Immediate action taken/recommended:</strong></td><td valign="middle"></td></tr></table></td>
					<td class="bodyTextGrey" colspan="3"><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					
					<textarea name="ImmediateAction" rows="4" cols="122" class="selectgen"   id="widetxt" ></textarea>
					
					</td>
				</tr>	
			<!--- 	<cfset Obs = "01">		
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "Main\Displays\dsp_EnterObservation.cfm", "uploads\Observations\#Obs#"))>		 --->
				<tr>
					<td class="formlable" align="left"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Supporting Information:</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=supportingDoc','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table></td>
					<td class="bodyTextGrey" colspan="3">
					<!--- <cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">
							<cfif getincfileslist.recordcount gt 0>
								<cfoutput query="getincfileslist">
									<!--- <a href="#self#?fuseaction=incidents.incidentmanament&submittype=deleteincfile&filename=#name#&Obs=#Obs#" onclick="return confirm('Are you sure you want to delete this file?');"><img src="images/trash.gif" border="0"></a> --->&nbsp;<a href="/#getappconfig.heartPath#/uploads/Observations/#Obs#/#name#" target="_blank">#name#</a><br>
								</cfoutput>
							</cfif>
						</cfif> --->
					
					<input type="file" name="uploaddoc1" size="65" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file2').style.display='';" class="midboxtitletxt">+</a>
								<div id="file2"><br><input type="file" name="uploaddoc2" size="65" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file3').style.display='';" class="midboxtitletxt">+</a></div>
								<div id="file3"><br><input type="file" name="uploaddoc3" size="65" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file4').style.display='';" class="midboxtitletxt">+</a></div>
								<div id="file4"><br><input type="file" name="uploaddoc4" size="65" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file5').style.display='';" class="midboxtitletxt">+</a></div>
								<div id="file5"><br><input type="file" name="uploaddoc5" size="65" id="selectgen"></div> 
			</td>
				</tr>
			
				<tr>
					<td class="formlable" align="left"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Feedback required?:*</strong></td><td valign="middle"><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/info/index.cfm?infotype=feedBack','Info','400','140','no');"><img src="images/info.png" border="0" align="right"></a></td></tr></table></td>
					<td class="bodyTextGrey"><span id="Feedback">Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="FeedbackReq" value="1" id="Feedback1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;<input type="radio" name="FeedbackReq" value="0"  id="Feedback2"></span>
										</td>
												
				</tr>
			
				<!--- <tr>
					<td class="formlable" align="left"><strong>Is further action required?*</strong></td>
					<td class="bodyTextGrey"><span id="enfnotice">Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="FurtherAction" value="1"  onclick="document.getElementById('enfnotice').style.border = '0px solid FFFFFF';checkenftype(this.value);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;<input type="radio" name="FurtherAction"  value="0" onclick="document.getElementById('enfnotice').style.border = '0px solid FFFFFF';checkenftype(this.value);"></span>
													</td>
				
					<td class="formlable" align="left" id="enftypel"><strong>Action taken/Recommended:*</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					<td class="bodyTextGrey" id="enftypefld"><textarea name="FurtherActionDesc" rows="4" cols="40" class="selectgen"   id="widetxt" ></textarea></td>
				</tr> --->
				<tr align="left">		
					<td bgcolor="ffffff" colspan="4"><br>&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Send for Review" name="Review" onclick="">&nbsp;&nbsp;&nbsp;
													<!--- <input type="button" class="selectGenBTN" value="Cancel" name="oneAIM" onclick="location.href='index.cfm?fuseaction=main.main'">&nbsp;&nbsp; --->
					<br></td>
				</tr>			
				</table>
			</td>
		</tr>
		
</table>

</cfform>
</cfoutput>
<!--- </div>		 --->

<script type="text/javascript">
document.getElementById("reqflds").style.display='none';
	//document.getElementById("file2").style.display='none';
	//document.getElementById("file3").style.display='none';
	//document.getElementById("file4").style.display='none';
	//document.getElementById("file5").style.display='none';
	
	document.getElementById("brnonelbl").style.display='none';
	document.getElementById("brnonefld").style.display='none';

	document.getElementById("rulebrotherrw").style.display='none';
	document.getElementById("rulebrotherrwfld").style.display='none';
	
	document.getElementById("sebnonefld").style.display='none';
	document.getElementById("sebnonelbl").style.display='none';
	document.getElementById("PostiveObs").style.display='none';
	//document.getElementById("PostiveObsfld").style.display='none';  
	
</script>