<cfset hastasks = "no">
<cfset showkey = "no">
<div class="content1of5">
<table  cellpadding="3" cellspacing="0" border="0" width="100%">
			<td width="15%" valign="top"  align="center">
				<div align="center" class="midboxhome">
					<div align="center" class="midboxtitle">What would you like to do?</div>
					<table cellpadding="2" cellspacing="0" border="0" align="center" width="80%">
						<tr>
							<td colspan="2"><img src="images/spacer.gif" width="8"></td>
						</tr>
						<tr>
							<td><img src="images/spacer.gif" width="8"></td>
							<td class="midboxtitletxt"><a href="index.cfm?fuseaction=main.EnterObservation" class="midboxtitletxt">Record a Safety Observation</a></td> <!--- <td></td> --->
						</tr>
						<tr>
							<td colspan="2"><img src="images/spacer.gif" width="8"></td>
						</tr>
						<tr>
							<td><img src="images/spacer.gif" width="8"></td>
							<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.MyObservations" class="midboxtitletxt">My Observations</a></td> 
						</tr>
						
					<cfloop list="#request.userlevel#" index="ru">
						<cfif listfindnocase("Global Admin,BU Admin,OU Admin,HSSE Advisor,Senior Executive View,Reports Only",ru) gt 0>
						<tr>
							<td colspan="2"><img src="images/spacer.gif" width="8"></td>
						</tr>
						<tr id="reptr">
							<td><img src="images/spacer.gif" width="8"></td>
							<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.home" class="midboxtitletxt">Reports</a></td> <!--- <td></td> --->
						</tr>
						<cfbreak>
						</cfif>
					</cfloop>
					
					<cfloop list="#request.userlevel#" index="ru">
						<cfif listfindnocase("Global Admin,BU Admin,OU Admin,Senior Executive View,Reports Only,HSSE Advisor",ru) gt 0>	
						<tr id="repsptr">
							<td colspan="2"><img src="images/spacer.gif" width="8"></td>
						</tr>
						<tr id="rsertr">
						<td id="openserp"><a href="javascript:void(0);" class="midboxtitletxt" onclick="document.getElementById('obssersp').style.display='';document.getElementById('obsser').style.display='';document.getElementById('savesersp').style.display='';document.getElementById('saveser').style.display='';document.getElementById('closeserm').style.display='';document.getElementById('closeser').style.display='';document.getElementById('openser').style.display='none';document.getElementById('openserp').style.display='none';">+</a></td>
			<td class="midboxtitletxt" id="openser"><a href="javascript:void(0);" class="midboxtitletxt" onclick="document.getElementById('obssersp').style.display='';document.getElementById('obsser').style.display='';document.getElementById('savesersp').style.display='';document.getElementById('saveser').style.display='';document.getElementById('closeserm').style.display='';document.getElementById('closeser').style.display='';document.getElementById('openser').style.display='none';document.getElementById('openserp').style.display='none';">Search</a></td>
			<td id="closeserm"><a href="javascript:void(0);" class="midboxtitletxt" onclick="document.getElementById('obssersp').style.display='none';document.getElementById('obsser').style.display='none';document.getElementById('savesersp').style.display='none';document.getElementById('saveser').style.display='none';document.getElementById('closeserm').style.display='none';document.getElementById('closeser').style.display='none';document.getElementById('openser').style.display='';document.getElementById('openserp').style.display='';">-</a></td>
			<td class="midboxtitletxt" id="closeser"><a href="javascript:void(0);" class="midboxtitletxt" onclick="document.getElementById('obssersp').style.display='none';document.getElementById('obsser').style.display='none';document.getElementById('savesersp').style.display='none';document.getElementById('saveser').style.display='none';document.getElementById('closeserm').style.display='none';document.getElementById('closeser').style.display='none';document.getElementById('openser').style.display='';document.getElementById('openserp').style.display='';">Search</a></td>	
						</tr>
						<tr id="obssersp">
							<td colspan="2"><img src="images/spacer.gif" height="8"></td>
						</tr>
						<tr id="obsser">
							<td><img src="images/spacer.gif" width="8"></td>
							<td class="midboxtitletxt">&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.cfm?fuseaction=reports.observationsearch" class="midboxtitletxt">Observations</a></td>
						</tr>
						<tr id="savesersp">
							<td colspan="2"><img src="images/spacer.gif" height="8"></td>
						</tr>
						<tr id="saveser">
							<td><img src="images/spacer.gif" width="8"></td>
							<td class="midboxtitletxt">&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.cfm?fuseaction=reports.mysearches" class="midboxtitletxt">My Saved Searches</a></td>
						</tr>
						<cfbreak>
						</cfif>
						</cfloop>
						
						<cfloop list="#request.userlevel#" index="ru">
						<cfif listfindnocase("Global Admin,BU Admin,OU Admin,Reviewer,Senior Reviewer,User,BU View Only,OU View Only",ru) gt 0>
						<tr>
							<td colspan="2" id="oneaimtrsp"><img src="images/spacer.gif" width="8"></td>
						</tr>
						<tr id="oneaimtr">
							<td><img src="images/spacer.gif" width="8"></td>
							<td class="midboxtitletxt"><cfoutput><a href="..\#getappconfig.oneaimPath#\index.cfm?fuseaction=main.main" class="midboxtitletxt">Go to oneAIM</a></cfoutput></td> <!--- <td></td> --->
						</tr>
						<cfbreak>
						</cfif>
						</cfloop>
						<tr>
							<td colspan="2"><img src="images/spacer.gif" width="8"></td>
						</tr>
					</table>
				
					<script type="text/javascript">
						<cfloop list="#request.userlevel#" index="ru">
						<cfif listfindnocase("Global Admin,BU Admin,OU Admin,Senior Executive View,Reports Only,HSSE Advisor",ru) gt 0>	
							document.getElementById("obssersp").style.display='none';
							document.getElementById("obsser").style.display='none';
							document.getElementById("savesersp").style.display='none';
							document.getElementById("saveser").style.display='none';
							document.getElementById("closeserm").style.display='none';
							document.getElementById("closeser").style.display='none';
						<cfbreak>
						</cfif>
						</cfloop>
						if(window.innerWidth<=760){
						document.getElementById("reptr").style.display='none';
							<cfloop list="#request.userlevel#" index="ru">
								<cfif listfindnocase("Global Admin,BU Admin,OU Admin,Reviewer,Senior Reviewer,User,BU View Only,OU View Only",ru) gt 0>
									document.getElementById("oneaimtrsp").style.display='none';
									document.getElementById("oneaimtr").style.display='none';
								<cfbreak>
								</cfif>
								
							</cfloop>
							
							<cfloop list="#request.userlevel#" index="ru">
								<cfif listfindnocase("Global Admin,BU Admin,OU Admin,Senior Executive View,Reports Only,HSSE Advisor",ru) gt 0>	
									document.getElementById("repsptr").style.display='none';
									document.getElementById("rsertr").style.display='none';
								<cfbreak>
								</cfif>
								
							</cfloop>
						}
					</script>
				</div>
				<br>
					<!---<div align="center" class="midboxhome">
						 <div align="center" class="midboxtitle">Change Role</div> 
						
						<table cellpadding="8" cellspacing="0" border="0" align="center" width="80%">
								<tr>
									<td style="font-family:Segoe UI;font-size:9pt;height:25;">
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=global">Global Admin</a></li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=buadmin">BU Admin</a>	</li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=ouadmin">OU Admin</a>	</li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=User">User</a>	</li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=Reviewer">Reviewer</a>	</li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=SRreviewer">Senior Reviewer</a></li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=SrExec">Senior Executive View</a></li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=reportsonly">Reports Only View</a></li>
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=globalit">Global IT</a></li>
										<!--- <li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=AFW">AFW</a></li>	 --->	
										<li><a href="/#getappconfig.heartPath#/changelogin.cfm?type=advisor">HSSE Advisor</a></li>																
									</td>
								</tr>
					</table>
					</div>   --->
			</td>
		</tr>
 </table>
</div>
<div class="content4of5r">
<table  cellpadding="3" cellspacing="0" border="0" id="mytab"  width="100%">
<tr> 
			<td align="left" valign="top" width="85%" class="bodyTexthd" >
				<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="95%">
					<tr>
						<td class="bodyTexthd" width="50%">
							<!--- <img src="images/Heart_Logo.gif" align="middle" width="175" height="49"><br><br> --->
						<a href="index.cfm?fuseaction=main.main">My Observations</a>&nbsp;/&nbsp;Dashboard
						</td>
						<td align="right"><span class="bodytext"  id="para1"></span>&nbsp;<a href="index.cfm?fuseaction=main.main&ToggleView=Dashboard"><img src="images/recycle.gif" border="0" style="vertical-align:-3px"></a></td>
					</tr>
					<tr>
						<td valign="top"  colspan="2" >
							<table cellpadding="2" cellspacing="0" border="0" width="100%">
								<tr>
									<td valign="top" width="50%" align="center">
									<cfset cntlist = valuelist(getsafetyrulechart.obscnt)>
									<cfset maxNum = ArrayMax(ListToArray(cntlist))>
									
									<cfset charttop = 10>
									<cfif maxnum lt 10>
										<cfset charttop = maxnum + (10-maxnum)>
									<cfelseif maxnum eq 10>
										<cfset charttop = maxnum+10>
									<cfelse>
										<cfif maxnum mod 10 eq 0>
											<cfset charttop = maxnum+10>
										<cfelse>
											<cfset chknum = right(maxnum,1)>
											<cfloop from="1" to="10" index="i">
												<cfset thenumval = chknum+i>
												
												<cfif thenumval mod 10 eq 0>
													<cfset charttop = maxnum+i>
													<cfbreak>
												</cfif>
											</cfloop>
										</cfif>
									</cfif>
									<cfset chartxml="<chart caption='Global Safety Rules' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='0' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='1' canvasBorderColor='##dfdfdf' canvasBorderThickness='1' showPlotBorder='0' plotFillRatio='100'>">
									<cfoutput query="getsafetyrulechart">
										<cfset uselable = trim(SafetyRule)>
										<cfset uselable = replace(uselable,"<","&lt;","all")>
										<cfset uselable = replace(uselable,">","&gt;","all")>
										<cfset chartxml= chartxml & "<set label='#uselable#' value='#obscnt#' />">
									</cfoutput>
									<cfset chartxml= chartxml & "</chart>">
									<cfoutput>
									<div id="SafetyRuleContainer"></div>
									<script type="text/javascript">
										FusionCharts.ready(function () {
										    var thisChart = new FusionCharts({
										        "type": "column2d",
										        "renderAt": "SafetyRuleContainer",
										        "width": "95%",
										        "height": "350",
										        "dataFormat": "xml",
										        "dataSource":  "#chartxml#"
										    }
											);
										    thisChart.render();
										});
									</script>		
									</cfoutput>
									</td>
									<td valign="top" width="50%" align="center">
										<cfset chartxml="<chart caption='Safety Essentials Chart' subcaption='' numberprefix=''  bgcolor='ffffff' showborder='1' use3dlighting='0' showshadow='0' enablesmartlabels='0' startingangle='310' showlabels='0' showpercentvalues='0' showlegend='1' legendshadow='0' legendborderalpha='0' decimals='0' captionfontsize='14' subcaptionfontsize='14' subcaptionfontbold='0' tooltipcolor='ffffff' tooltipborderthickness='0' tooltipbgcolor='000000' tooltipbgalpha='80' tooltipborderradius='2' tooltippadding='5' usedataplotcolorforlabels='1'  exportenabled='0' exportAtClientSide='1' showValues='0'  doughnutRadius='76'>">
										<cfoutput query="getsafetyessentialchart">
											<cfset uselable = trim(SafetyEssential)>
											<cfset uselable = replace(uselable,"<","&lt;","all")>
											<cfset uselable = replace(uselable,">","&gt;","all")>
											<cfset chartxml= chartxml & "<set label='#uselable#' value='#obscnt#' />">
										</cfoutput>
					 					<cfset chartxml= chartxml & "</chart>">
										<cfoutput>
										<div id="SafetyEssentialContainer"></div>
										<script type="text/javascript">
											FusionCharts.ready(function () {
											    var thisChart = new FusionCharts({
											        "type": "doughnut2d",
											        "renderAt": "SafetyEssentialContainer",
											        "width": "95%",
											        "height": "350",
											        "dataFormat": "xml",
											        "dataSource":  "#chartxml#"
											    }
											);
											    thisChart.render();
											});
										</script>		
										</cfoutput>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<cfset shList = "Amec Foster Wheeler,Client,Contractor,3rd Party">
					<tr>
						<td valign="top"  colspan="2" >
							<table cellpadding="5" cellspacing="0" border="0" width="100%">
								<tr>
									<td valign="top" align="center">
										<table cellpadding="3" cellspacing="0"  width="98%" border="0" align="center">
											<tr>
												<td class="purplebg" align="center" colspan="14"><strong class="BodyTextWhite"><cfoutput>#year(now())#</cfoutput> YTD Observation Summary: Amec Foster Wheeler Global</strong></td>
											</tr>
											<tr>
												<td class="formlable" width="15%"><strong><cfoutput>#request.bulabellong#</cfoutput></strong></td>
												<td class="formlable" ><strong>Stakeholders</strong></td>
												<td class="formlable" align="center"><strong>Unsafe Act</strong></td>
												<td class="formlable" align="center"><strong>Unsafe Condition</strong></td>
												<td class="formlable" align="center"><strong>Safe Behaviour</strong></td>
											</tr>
											<tr>
												<td colspan="5" class="formlable" ><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2167;" width="100%"></td>
											</tr>
											<cfoutput query="getBUs">
											<cfset ctr = 0>
												<cfloop list="#shList#" index="i">
													<cfset ctr = ctr + 1>
													<tr>
														<td class="bodytextgrey"><cfif ctr eq 1>#getBUs.Name#</cfif></td>
														<td  class="bodytextgrey">#i#</td>
														<td  class="bodytextgrey" align="center"><cfif structkeyexists(ObsStruct,"#getBUs.ID#_#i#_Unsafe Act")>#ObsStruct["#getBUs.ID#_#i#_Unsafe Act"]#<cfelse>0</cfif></td>
														<td  class="bodytextgrey" align="center"><cfif structkeyexists(ObsStruct,"#getBUs.ID#_#i#_Unsafe Condition")>#ObsStruct["#getBUs.ID#_#i#_Unsafe Condition"]#<cfelse>0</cfif></td>
														<td  class="bodytextgrey" align="center"><cfif structkeyexists(ObsStruct,"#getBUs.ID#_#i#_Safe Behaviour")>#ObsStruct["#getBUs.ID#_#i#_Safe Behaviour"]#<cfelse>0</cfif></td>
													</tr>
												</cfloop>
											<tr>
												<td colspan="5"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2167;" width="100%"></td>
											</tr>
											</cfoutput>
											<cfset ctr = 0>
											<cfloop list="#shList#" index="i">
												<cfset ctr = ctr + 1>
												<cfoutput>
												<tr>
													<td class="formlable"><cfif ctr eq 1>Amec Foster Wheeler Global Total</cfif></td>
													<td  class="formlable">#i#</td>
													<td  class="formlable" align="center"><cfif structkeyexists(ObsStruct,"Global_#i#_Unsafe Act")>#ObsStruct["Global_#i#_Unsafe Act"]#<cfelse>0</cfif></td>
													<td  class="formlable" align="center"><cfif structkeyexists(ObsStruct,"Global_#i#_Unsafe Condition")>#ObsStruct["Global_#i#_Unsafe Condition"]#<cfelse>0</cfif></td>
													<td  class="formlable" align="center"><cfif structkeyexists(ObsStruct,"Global_#i#_Safe Behaviour")>#ObsStruct["Global_#i#_Safe Behaviour"]#<cfelse>0</cfif></td>
												</tr>
												</cfoutput>
											</cfloop>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			
		</tr>
		
		
</table>
 </div> 
<script type="text/javascript">
document.getElementById("para1").innerHTML = formatAMPM();

function formatAMPM() {

var d = new Date(),
    minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
    hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
    ampm = d.getHours() >= 12 ? 'PM' : 'AM',
    months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
    days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
return 'Last updated:' + ' ' + days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getFullYear()+' '+hours+':'+minutes;
}
</script>

		