<cfparam name="ActionType" default="">
<cfparam name="ReviewOK" default="Yes">

<cfset vStatus= getObservationDetail.Status>

<cfif ActionType EQ "vReview" and vStatus EQ "Submitted">
	<cfset ReviewOK = "Yes">
<cfelseif ActionType EQ "vReview" and vStatus NEQ "Submitted">
	<cfset ReviewOK = "No">
</cfif>

<!--- <div align="center" class="midbox">
	<div align="center" class="midboxtitle">What would you like to report?</div> --->
<cfoutput query="getObservationDetail">	
<cfform action="index.cfm?fuseaction=main.ObservationReview" method="post" name="incidentfrm" enctype="multipart/form-data">	
<input type="hidden" name="OBNum" value="#OBNum#">
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="99%">
		<tr>
			<td align="left" bgcolor="ffffff">
			
				<table cellpadding="0" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
							<tr>
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Originator:</strong></div>
							<div class="bodyTextGreyresize">#CreatedBy#</div>
							</div>
							</td>
							</tr>
							<tr>		
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Date Created:</strong></div>
							<div class="bodyTextGreyresize">#dateformat(DateCreated,"dd-mmm-yyyy")#</div>
							</div>
							</td>																
							</tr>
							<tr>
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>#request.bulabellong#:</strong></div>
							<div class="bodyTextGreyresize" id="BUTD">#ObservationBU#</div>
							</div>
							</td>	
							</tr>
							<tr>								
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>#request.oulabellong#:</strong></div>
							<div class="bodyTextGreyresize" id="OUTD">#ObservationOU#</div>
							</div>
							</td>							
							</tr>
							<cfif trim(getObservationDetail.cohabitOU) neq '' and trim(getObservationDetail.cohabitOU) gt 0 and isdefined("getcohab.recordcount") and listlen(getObservationDetail.cohabitOU) lt 2>
							<tr>								
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Your Office or the #request.oulabellong# with whom you cohabit:</strong></div>
							<div class="bodyTextGreyresize">#getcohab.name#</div>
							</div>
							</td>							
							</tr>
							</cfif>
							<tr>
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Project/Office:</strong></div>
							<div class="bodyTextGreyresize">#Group_Name#</div>
							</div>
							</td>
							</tr>
						<cfif BusinessStream NEQ ''>
							<tr>	
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Business Stream:</strong></div>
							<div class="bodyTextGreyresize" id="BSTD" >#BusinessStream#</div>
							</div>
							</td>
							</tr>
						</cfif>	
							
							
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Site/Office Name:</strong></div>
							<div class="bodyTextGreyresize" id="SOTD" >#SiteName#</div>
							</div>
							</td>
						
						</tr>												
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Exact Location of Act/Condition/Safe Behaviour:</strong></div>
							<div class="bodyTextGreyresize">#ExactLocation#</div>
							</div>
							</td>
						</tr>
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>EventType:</strong></div>
							<div class="bodyTextGreyresize">#ObservationType#</div>
							</div>
							</td>											
						</tr>
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observer Name:</strong></div>
							<div class="bodyTextGreyresize">#ObserverName#</div>
							</div>
							</td>
						</tr>
						<tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observer Email Address:</strong></div>
							<div class="bodyTextGreyresize">#ObserverEmail#</div>
							</div>
							</td>
						</tr>		
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Stakeholder:</strong></div>
							<div class="bodyTextGreyresize">#PersonnelCategory#</div>
							</div>
							</td>															
						</tr>
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observation Date:</strong></div>
							<div class="bodyTextGreyresize">#dateformat(DateofObservation,"dd-mmm-yyyy")#</div>
							</div>
							</td>
						</tr>
						<tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observation Time:</strong></div>
							<div class="bodyTextGreyresize">#TimeofObservation#</div>
							</div>
							</td>
						</tr>
						
						
						
						<cfif ObservationType EQ 'Safe Behaviour'>	
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Name of Individual or Team:</strong></div>
							<div class="bodyTextGreyresize">#TeamName#</div>
							</div>
							</td>	
						</tr>
						</cfif>		
<cfif  ObservationType NEQ 'Safe Behaviour' >
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Global Safety Rules:</strong></div>
							<div class="bodyTextGreyresize">#SafetyRule#</div>
							</div>
							</td>
						</tr>
							
							<cfif GlobalSafetyRules EQ 11>
						</tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>If none, why?:</strong></div>
							<div class="bodyTextGreyresize" id="brnonefld">#GlobalSafetyWhyNone#</div>
							</div>
							</td>
						</tr>	
							<cfelseif GlobalSafetyRules EQ 15>
						<tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Other:</strong></div>
							<div class="bodyTextGreyresize">#GlobalSafetyOther#</div>
							</div>
							</td>
						</tr>	
							</cfif>
						
					
					<!--- <tr id="rulebrotherrw">
						<td class="formlable" align="left" width="30%"><strong>Other:</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
						<td class="bodyTextGrey" colspan="3">#GlobalSafetyOther#</td>
					</tr> --->
		
					<tr>
						<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Safety Essential:</strong></div>
							<div class="bodyTextGreyresize">#SafetyEssDesc#</div>
							</div>
							</td>
					</tr>	
						<cfif SafetyEssential EQ 7>
					<tr>	
						<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>If none, why?:</strong></div>
							<div class="bodyTextGreyresize">#SafetyEssentialNone#</div>
							</div>
							</td>
					</tr>	
					<!--- <cfelse>
					<tr>
						<td ></td>
						<td colspan="3"></td>
					</tr>	 --->
					</cfif>
				
					</cfif>						
				<tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Details of safety observation:</strong></div>
							<div class="bodyTextGreyresize">#Observations#</div>
							</div>
						</td>
				</tr>		
				
				<tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Immediate action taken/recommended:</strong></div>
							<div class="bodyTextGreyresize">#FollowUpActions#</div>
							</div>
					</td>
				</tr>	
			
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "Main\Displays\dsp_ReviewObservation.cfm", "uploads\Observations\#OBNum#"))>		
				<tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Supporting Information:</strong></div>
							<div class="bodyTextGreyresize">
				<cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getObsvfileslist">
							<cfif getObsvfileslist.recordcount gt 0>
								
								<cfloop query="getObsvfileslist">
									<!--- <a href="#self#?fuseaction=incidents.incidentmanament&submittype=deleteincfile&filename=#name#&Obs=#Obs#" onclick="return confirm('Are you sure you want to delete this file?');"><img src="images/trash.gif" border="0"></a> --->&nbsp;<a href="/#getappconfig.heartPath#/uploads/Observations/#OBNum#/#name#" target="_blank">#name#</a><br>
								</cfloop>	
							
							</cfif>
						</cfif>
					</div>
							</div>
							
			</td>
				</tr>
			
				 <tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Feedback required?:</strong></div>
							<div class="bodyTextGreyresize"><cfif FeedBackRequired eq 1>Yes<cfelse>No</cfif></div>
							</div>
							</td>
												
				</tr> 
							<!--- <tr>
								<td class="formlable" align="left" width="30%"><strong>Originator:</strong></td>
								<td class="bodyTextGrey" colspan="3">#CreatedBy#</td>		
							</tr>
							<tr>		
								<td class="formlable" align="left" width="30%"><strong>Date Created:</strong></td>
								<td class="bodyTextGrey" colspan="3">#dateformat(DateCreated,"dd-mmm-yyyy")#</td>																
							</tr>
							<tr>
								<td class="formlable" align="left" width="30%"><strong>#request.bulabellong#:</strong></td>
								<td class="bodyTextGrey" id="BUTD" colspan="3">#ObservationBU#</td>	
							</tr>
							<tr>								
								<td class="formlable" align="left" width="30%"><strong>#request.oulabellong#:</strong></td>
								<td class="bodyTextGrey"  id="OUTD" colspan="3">#ObservationOU#</td>							
							</tr>
							<cfif trim(getObservationDetail.cohabitOU) neq '' and trim(getObservationDetail.cohabitOU) gt 0 and isdefined("getcohab.recordcount") and listlen(getObservationDetail.cohabitOU) lt 2>
							<tr>								
								<td class="formlable" align="left" width="30%"><strong>Your Office or the #request.oulabellong# with whom you cohabit:</strong></td>
								<td class="bodyTextGrey"  id="OUTD" colspan="3">#getcohab.name#</td>							
							</tr>
							</cfif>
							<tr>
								<td class="formlable" align="left" width="30%"><strong>Project/Office:</strong></td>
								<td class="bodyTextGrey"  colspan="3">#Group_Name#</td>
							</tr>
						<cfif BusinessStream NEQ ''>
							<tr>	
								<td class="formlable" align="left" width="30%"><strong>Business Stream:</strong></td>
								<td class="bodyTextGrey"  id="BSTD" colspan="3">#BusinessStream#</td>
							</tr>
						</cfif>	
							
							
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Site/Office Name:</strong></td>
							<td class="bodyTextGrey"  id="SOTD" colspan="3">#SiteName#	</td>
						
						</tr>												
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Exact Location of Act/Condition/Safe Behaviour:</strong></td>
							<td class="bodyTextGrey" colspan="3">#ExactLocation#</td>
						</tr>
						
						<tr>
							<td class="formlable" align="left" width="30%"><strong>EventType:</strong></td>
							<td class="bodyTextGrey" colspan="3">#ObservationType#</td>											
						</tr>
						
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Observer Name:</strong></td>
							<td class="bodyTextGrey" colspan="3">#ObserverName#</td>
						</tr>
						<tr>	
							<td class="formlable" align="left" width="30%"><strong>Observer Email Address:</strong></td>
							<td class="bodyTextGrey" colspan="3">#ObserverEmail#</td>
						</tr>		
						
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Stakeholder:</strong></td>
							<td class="bodyTextGrey" colspan="3">#PersonnelCategory#</td>															
						</tr>
						
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Observation Date:</strong></td>
							<td class="bodyTextGrey" colspan="3">#dateformat(DateofObservation,"dd-mmm-yyyy")#</td>
						</tr>
						<tr>	
							<td class="formlable" align="left" width="30%"><strong>Observation Time:</strong></td>
							<td class="bodyTextGrey" colspan="3">#TimeofObservation#</td>
						</tr>
						
						
						
						<cfif ObservationType EQ 'Safe Behaviour'>	
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Name of Individual or Team:</strong></td>
							<td class="bodyTextGrey" colspan="1" colspan="3">#TeamName#</td>	
						</tr>
						</cfif>		
<cfif  ObservationType NEQ 'Safe Behaviour' >
						<tr>
							<td class="formlable" align="left" width="30%"><strong>Global Safety Rules:</strong></td><!--- this.style.border='1px solid 5f2167'; --->
							<td class="bodyTextGrey"   colspan="3">#SafetyRule#</td>
						</tr>
							
							<cfif GlobalSafetyRules EQ 11>
						</tr>	
							<td class="formlable" align="left" width="30%" id="brnonelbl"><strong>If none, why?:</strong></td><!---  onkeypress="this.style.border='1px solid 5f2167';" --->
							<td class="bodyTextGrey" id="brnonefld"  colspan="3">#GlobalSafetyWhyNone#</td>
						</tr>	
							<cfelseif GlobalSafetyRules EQ 15>
						<tr>	
							<td class="formlable" align="left" width="30%"><strong>Other:</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
							<td class="bodyTextGrey" colspan="3">#GlobalSafetyOther#</td>
						</tr>	
							</cfif>
						
					
					<!--- <tr id="rulebrotherrw">
						<td class="formlable" align="left" width="30%"><strong>Other:</strong></td><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
						<td class="bodyTextGrey" colspan="3">#GlobalSafetyOther#</td>
					</tr> --->
		
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Safety Essential:</strong></td>				
						<td class="bodyTextGrey"  colspan="3"> #SafetyEssDesc#</td>
					</tr>	
						<cfif SafetyEssential EQ 7>
					<tr>	
						<td class="formlable" align="left" width="30%" ><strong>If none, why?:</strong></td>
						<td class="bodyTextGrey" colspan="3">#SafetyEssentialNone#</td>
					</tr>	
					<!--- <cfelse>
					<tr>
						<td ></td>
						<td colspan="3"></td>
					</tr>	 --->
					</cfif>
				
					</cfif>						
				<tr>
					<td class="formlable" align="left" width="30%"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Details of safety observation:</strong></td><td valign="middle"></td></tr></table></td>
					<td class="bodyTextGrey" colspan="3"><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					
					#Observations#
					
					</td>
				</tr>		
				
				<tr>
					<td class="formlable" align="left" width="30%"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" class="formlable" width="95%"><strong>Immediate action taken/recommended:</strong></td><td valign="middle"></td></tr></table></td>
					<td class="bodyTextGrey" colspan="3"><!--- onkeypress="this.style.border='1px solid 5f2167';" --->
					#FollowUpActions#
					</td>
				</tr>	
			
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "Main\Displays\dsp_ReviewObservation.cfm", "uploads\Observations\#OBNum#"))>		
				<tr>
					<td class="formlable" align="left" width="30%"><strong>Supporting Information:</strong></td>
					<td class="bodyTextGrey" colspan="3">
				<cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getObsvfileslist">
							<cfif getObsvfileslist.recordcount gt 0>
								
								<cfloop query="getObsvfileslist">
									<!--- <a href="#self#?fuseaction=incidents.incidentmanament&submittype=deleteincfile&filename=#name#&Obs=#Obs#" onclick="return confirm('Are you sure you want to delete this file?');"><img src="images/trash.gif" border="0"></a> --->&nbsp;<a href="/#getappconfig.heartPath#/uploads/Observations/#OBNum#/#name#" target="_blank">#name#</a><br>
								</cfloop>	
							
							</cfif>
						</cfif>
					
					
			</td>
				</tr>
			
				 <tr>
					<td class="formlable" align="left" width="30%"><strong>Feedback required?:</strong></td>
					<td class="bodyTextGrey"><cfif FeedBackRequired eq 1>Yes<cfelse>No</cfif>	</td>
												
				</tr>  --->
			
			<tr align="left" id="frmbuttonIE">		
			<td bgcolor="ffffff" colspan="2">
	
			<br>&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Review" name="Review" onclick="NewWindow('#self#?fuseaction=main.reviewObv&OBNum=#OBNum#','Info','500','325','no');">&nbsp;&nbsp;&nbsp;
<input type="button" class="selectGenBTN" value="Escalate to oneAIM" name="oneAIM" onclick="NewWindow('#self#?fuseaction=main.OneAim&OBNum=#OBNum#','Info','500','325','no');"> &nbsp;&nbsp;&nbsp;
<input type="button" class="selectGenBTN" value="Cancel Observation" name="Cancel" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');"> &nbsp;&nbsp;
<br>			</td>
	
		</tr>
		<tr align="center" id="frmbuttonMob">		
			<td bgcolor="ffffff" colspan="2" align="center" ><br><input type="button" class="selectGenBTN" value="Review" name="Review" onclick="NewWindow('#self#?fuseaction=main.reviewObv&OBNum=#OBNum#','Info','500','325','no');"><br><input type="button" class="selectGenBTN" value="Escalate to oneAIM" name="oneAIM" onclick="NewWindow('#self#?fuseaction=main.OneAim&OBNum=#OBNum#','Info','500','325','no');"><br><input type="button" class="selectGenBTN" value="Cancel Observation" name="Cancel" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');"><br>
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
			<input type="button" class="selectGenBTN" value="Amend" name="Amend" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');"><br>
		<cfelseif  listfindnocase(request.userlevel,"HSSE Advisor") gt 0 and listfind(request.hsseadvlocs,getObservationDetail.siteid) gt 0>
			<input type="button" class="selectGenBTN" value="Amend" name="Amend" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');"><br>
		<cfelse>
				<cfif getObservationDetail.createdbyemail eq request.userlogin>
					<input type="button" class="selectGenBTN" value="Amend" name="Amend" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');"><br>
				</cfif>
		</cfif>
</td>
	
		</tr>
				</table>
			</td>
		</tr>
			<cfif getrestorecomms.recordcount gt 0>
	<tr >
	<td><strong class="bodytextwhite">&nbsp;Restore Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getrestorecomms">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
	
	
	</cfif>	
				<cfif getamendcomms.recordcount gt 0>
	<tr >
	<td><strong class="bodytextwhite">&nbsp;Reasons for Amendment</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getamendcomms">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
	
	
	</cfif>	
	<!--- 	<cfif listfindnocase("Reviewed,Closed,Cancelled",status) gt 0>
				<tr>
					<td align="left" class="purplebg"><strong class="bodyTextWhite">Comments from Reviewer/OU Admin</strong></td>
				</tr>
				
				<tr align="left">	
				<td >
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				<cfif FurtherActions NEQ ''>	 
					 <tr>
						<td class="formlable" align="left" width="25.5%"><strong>Action taken/Recommended:</strong></td>
						<td class="bodyTextGrey" colspan="3">#FurtherActions#</td>		
					</tr>	
				</cfif>	
				<cfif CloseComments NEQ ''>
					<tr>
						<td class="formlable" align="left" width="25.5%"><strong>Closing Comments:</strong></td>
						<td class="bodyTextGrey" colspan="3">#CloseComments#</td>		
					</tr>
				</cfif>		
				<cfif CancelComments NEQ ''>			
					<tr>
						<td class="formlable" align="left" width="25.5%"><strong>Cancel Comments:</strong></td>
						<td class="bodyTextGrey" colspan="3">#CancelComments#</td>		
					</tr>	
				</cfif>					
					</table>	
				</td>	
						<!--- <td class="bodyTextGrey" colspan="3">ccc</td> --->
				</tr>
			</cfif> --->
		
		
</table>

</cfform>
<script type="text/javascript">

if(window.innerWidth<=760){
	document.getElementById("frmbuttonMob").style.display='';
	document.getElementById("frmbuttonIE").style.display='none';

	}
else{
	document.getElementById("frmbuttonIE").style.display='';
	document.getElementById("frmbuttonMob").style.display='none';

}
	//document.getElementById("PostiveObsfld").style.display='none';  
	 
</script>
</cfoutput>
