

<!--- <div align="center" class="midbox">
	<div align="center" class="midboxtitle">What would you like to report?</div> --->
<cfoutput query="getObservationDetail">	
<cfform action="index.cfm?fuseaction=main.ObservationReview" method="post" name="incidentfrm" enctype="multipart/form-data">	
<input type="hidden" name="OBNum" value="#OBNum#">
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
		<tr>
			<td align="left" bgcolor="ffffff">
			
				<table cellpadding="0" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
							
							<tr>
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Originator:</strong></div>
							<div class="bodyTextGreyresize">#CreatedBy#</div>
							</div>
							</td>
							</tr>
							<tr>		
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Date Created:</strong></div>
							<div class="bodyTextGreyresize">#dateformat(DateCreated,"dd-mmm-yyyy")#</div>
							</div>
							</td>																
							</tr>
							<tr>
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>#request.bulabellong#:</strong></div>
							<div class="bodyTextGreyresize" id="BUTD">#ObservationBU#</div>
							</div>
							</td>	
							</tr>
							<tr>								
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>#request.oulabellong#:</strong></div>
							<div class="bodyTextGreyresize" id="OUTD">#ObservationOU#</div>
							</div>
							</td>							
							</tr>
							<cfif trim(getObservationDetail.cohabitOU) neq '' and trim(getObservationDetail.cohabitOU) gt 0 and isdefined("getcohab.recordcount") and listlen(getObservationDetail.cohabitOU) lt 2>
							<tr>								
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Your Office or the #request.oulabellong# with whom you cohabit:</strong></div>
							<div class="bodyTextGreyresize">#getcohab.name#</div>
							</div>
							</td>							
							</tr>
							</cfif>
							<tr>
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Project/Office:</strong></div>
							<div class="bodyTextGreyresize">#Group_Name#</div>
							</div>
							</td>
							</tr>
						<cfif BusinessStream NEQ ''>
							<tr>	
								<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Business Stream:</strong></div>
							<div class="bodyTextGreyresize" id="BSTD" >#BusinessStream#</div>
							</div>
							</td>
							</tr>
						</cfif>	
							
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Site/Office Name:</strong></div>
							<div class="bodyTextGreyresize" id="SOTD" >#SiteName#</div>
							</div>
							</td>
						
						</tr>												
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Exact Location of Act/Condition/Safe Behaviour:</strong></div>
							<div class="bodyTextGreyresize">#ExactLocation#</div>
							</div>
							</td>
						</tr>
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>EventType:</strong></div>
							<div class="bodyTextGreyresize">#ObservationType#</div>
							</div>
							</td>											
						</tr>
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observer Name:</strong></div>
							<div class="bodyTextGreyresize">#ObserverName#</div>
							</div>
							</td>
						</tr>
						<tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observer Email Address:</strong></div>
							<div class="bodyTextGreyresize">#ObserverEmail#</div>
							</div>
							</td>
						</tr>		
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Stakeholder:</strong></div>
							<div class="bodyTextGreyresize">#PersonnelCategory#</div>
							</div>
							</td>															
						</tr>
						
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observation Date:</strong></div>
							<div class="bodyTextGreyresize">#dateformat(DateofObservation,"dd-mmm-yyyy")#</div>
							</div>
							</td>
						</tr>
						<tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Observation Time:</strong></div>
							<div class="bodyTextGreyresize">#TimeofObservation#</div>
							</div>
							</td>
						</tr>
						
						
						
						<cfif ObservationType EQ 'Safe Behaviour'>	
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Name of Individual or Team:</strong></div>
							<div class="bodyTextGreyresize">#TeamName#</div>
							</div>
							</td>	
						</tr>
						</cfif>		
<cfif  ObservationType NEQ 'Safe Behaviour' >
						<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Global Safety Rules:</strong></div>
							<div class="bodyTextGreyresize">#SafetyRule#</div>
							</div>
							</td>
						</tr>
							<cfif GlobalSafetyRules EQ 11>
						</tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>If none, why?:</strong></div>
							<div class="bodyTextGreyresize" id="brnonefld">#GlobalSafetyWhyNone#</div>
							</div>
							</td>
						</tr>	
							<cfelseif GlobalSafetyRules EQ 15>
						<tr>	
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Other:</strong></div>
							<div class="bodyTextGreyresize">#GlobalSafetyOther#</div>
							</div>
							</td>
						</tr>	
							</cfif>
						
		
					<tr>
						<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"><strong>Safety Essential:</strong></div>
							<div class="bodyTextGreyresize">#SafetyEssDesc#</div>
							</div>
							</td>
					</tr>	
						<cfif SafetyEssential EQ 7>
					<tr>	
						<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>If none, why?:</strong></div>
							<div class="bodyTextGreyresize">#SafetyEssentialNone#</div>
							</div>
							</td>
					</tr>	
					
					</cfif>
				
</cfif>						
				<tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Details of safety observation:</strong></div>
							<div class="bodyTextGreyresize">#Observations#</div>
							</div>
						</td>
				</tr>		
				
				<tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Immediate action taken/recommended:</strong></div>
							<div class="bodyTextGreyresize">#FollowUpActions#</div>
							</div>
					</td>
				</tr>	
			
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "Main\Displays\dsp_ViewObservation.cfm", "uploads\Observations\#OBNum#"))>		
				<tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Supporting Information:</strong></div>
							<div class="bodyTextGreyresize">
				<cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getObsvfileslist">
							<cfif getObsvfileslist.recordcount gt 0>
								
								<cfloop query="getObsvfileslist">
									<!--- <a href="#self#?fuseaction=incidents.incidentmanament&submittype=deleteincfile&filename=#name#&Obs=#Obs#" onclick="return confirm('Are you sure you want to delete this file?');"><img src="images/trash.gif" border="0"></a> --->&nbsp;<a href="/#getappconfig.heartPath#/uploads/Observations/#OBNum#/#name#" target="_blank">#name#</a><br>
								</cfloop>	
							
							</cfif>
						</cfif>
					</div>
							</div>
							
					
			</td>
				</tr>
			
			  <tr>
					<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize"  id="brnonelbl"><strong>Feedback required?:</strong></div>
							<div class="bodyTextGreyresize"><cfif FeedBackRequired eq 1>Yes<cfelse>No</cfif></div>
							</div>
							</td>
												
				</tr> 
				
					<!--- 1 Reviewer comments--->
		<cfif FurtherActions NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from Reviewer</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 
						  <tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>Action taken/Recommended:</strong></div>
							<div class="bodyTextGreyresize">#FurtherActions#</div>
							</div></td>		
						</tr>	
					</table>	
				</td>	
			</tr>
		</cfif>
		<!--- 2  Reviewer comments --->
		<cfif status EQ 'Closed' and FurtherActions EQ '' and CloseComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from Reviewer</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 		 <tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>Closing Comments:</strong></div>
							<div class="bodyTextGreyresize">#CloseComments#</div>
							</div></td>		
						</tr>	
					</table>	
				</td>	
			</tr>
		</cfif>
		<!--- 3 Reviewer comments --->
		<cfif status EQ 'Cancelled' and FurtherActions EQ '' and CancelComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from Reviewer</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 		<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>Cancel Comments:</strong></div>
							<div class="bodyTextGreyresize">#CancelComments#</div>
							</div></td>		
						</tr>	
						 
					</table>	
				</td>	
			</tr>
		</cfif>
		
		<!--- 4 OU Admin comments --->
		<cfif status EQ 'Cancelled' and FurtherActions NEQ '' and CancelComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from OU Admin</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 		<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>Cancel Comments:</strong></div>
							<div class="bodyTextGreyresize">#CancelComments#</div>
							</div></td>		
						</tr>	
						 
					</table>	
				</td>	
			</tr>
		</cfif>
		
		<!--- 5 OU Admin comments--->
		<cfif status EQ 'Closed' and FurtherActions NEQ '' and CloseComments NEQ ''>
			<tr>
				<td align="left" class="purplebg" colspan="2"><strong class="bodyTextWhite">Comments from OU Admin</strong></td>
			</tr>
			
			<tr align="left">	
				<td colspan="2">
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				 	<tr>
							<td width="100%" >
							<div id="containerfrm">
							<div class="formlableresize" ><strong>Closing Comments:</strong></div>
							<div class="bodyTextGreyresize">#CloseComments#</div>
							</div></td>		
						</tr>	
						
					</table>	
				</td>	
			</tr>
		</cfif>
		
		<tr align="left"  id="frmbuttonIE">		
			<td bgcolor="ffffff"  colspan="2"><br>&nbsp;&nbsp;
<input type="button" class="selectGenBTN" value="Back" name="Back" onclick="location.href='index.cfm?fuseaction=main.main'"> &nbsp;&nbsp;&nbsp;<br>
			</td>
		</tr>
		<tr align="center" id="frmbuttonMob">	
			<td bgcolor="ffffff"  colspan="2" align="center"><br><input type="button" class="selectGenBTN" value="Back" name="Back" onclick="location.href='index.cfm?fuseaction=main.main'"><br>
			</td>
		</tr>	
				</table>
			</td>
		</tr>
		<cfif getrestorecomms.recordcount gt 0>
	<tr >
	<td><strong class="bodytextwhite">&nbsp;Restore Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getrestorecomms">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
	
	
	</cfif>	
	<cfif getamendcomms.recordcount gt 0>
	<tr >
	<td><strong class="bodytextwhite">&nbsp;Reasons for Amendment</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getamendcomms">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
	
	
	</cfif>	
		<!--- <cfif listfindnocase("Reviewed,Closed,Cancelled",status) gt 0>
				<tr>
					<td align="left" class="purplebg"><strong class="bodyTextWhite">Comments from Reviewer/OU Admin</strong></td>
				</tr>
				
				<tr align="left">	
				<td >
					<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				<cfif FurtherActions NEQ ''>	 
					 <tr>
						<td class="formlable" align="left" width="30%"><strong>Action taken/Recommended:</strong></td>
						<td class="bodyTextGrey" colspan="3">#FurtherActions#</td>		
					</tr>	
				</cfif>	
				<cfif CloseComments NEQ ''>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Closing Comments:</strong></td>
						<td class="bodyTextGrey" colspan="3">#CloseComments#</td>		
					</tr>
				</cfif>		
				<cfif CancelComments NEQ ''>			
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Cancel Comments:</strong></td>
						<td class="bodyTextGrey" colspan="3">#CancelComments#</td>		
					</tr>	
				</cfif>					
					</table>	
				</td>	
						<!--- <td class="bodyTextGrey" colspan="3">ccc</td> --->
				</tr>
			</cfif> --->
	
</table>

</cfform>
</cfoutput>
<script type="text/javascript">

if(window.innerWidth<=760){
	document.getElementById("frmbuttonMob").style.display='';
	document.getElementById("frmbuttonIE").style.display='none';

	}
else{
	document.getElementById("frmbuttonIE").style.display='';
	document.getElementById("frmbuttonMob").style.display='none';

}
	
	 
</script>
