
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" id="mytab">
	<tr>
		<td align="center" bgcolor="ffffff">
			<table cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff" width="100%">
				<tr>
					<td colspan="2" align="center" class="purplebg"><strong class="bodyTextWhite">Observation Audit Trail</strong></td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellpadding="4" cellspacing="1" border="0" width="100%">
							<tr>
								<td class="formlable" valign="top" width="16%"><strong>From Status</strong></td>
								<td class="formlable" valign="top" width="16%"><strong>Action</strong></td>
								<td class="formlable" valign="top" width="16%"><strong>To Status</strong></td>
								<td class="formlable" valign="top" width="20%"><strong>Status</strong></td>
								<cfif qrysort eq "DESC">
									<cfset tosortres = "ASC">
								<cfelse>
									<cfset tosortres = "DESC">
								</cfif>
								<cfoutput><td class="formlable" valign="top" width="16%"><strong><a href="index.cfm?fuseaction=main.audit&OBnum=#OBnum#&qrysort=#tosortres#">Date/Time&nbsp;<cfif qrysort eq "DESC">&##x25BC;<cfelse>&##x25B2;</cfif></a></strong></td></cfoutput>
								<td class="formlable" valign="top" width="16%"><strong>User</strong></td>
							</tr>
							<cfoutput query="getobsaudit">
							<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
								<td class="bodyTextGrey" valign="top">#fromstatus#</td>
								<td class="bodyTextGrey" valign="top">#actiontaken#</td>
								<td class="bodyTextGrey" valign="top">#ToStatus#</td>
								<td class="bodyTextGrey" valign="top">#StatusDesc#</td>
								<td class="bodyTextGrey" valign="top">#dateTimeFormat(completeddate,sysdatetimeformat)#&nbsp;GMT</td>
								<td class="bodyTextGrey" valign="top">#completedby#</td>
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr> 
</table><br>


