<script type="text/javascript">
document.getElementById("enftypel").style.display='none';
document.getElementById("ShowText").style.display='none';
document.getElementById("CloseTxt").style.display='none';
document.getElementById("CloseComm").style.display='none';
function checkenftype(sntval){
	if(sntval==1){
	//document.getElementById("enftypefld").style.display='';
	document.getElementById("enftypel").style.display='';
	document.getElementById("ShowText").style.display='';
	document.getElementById("CloseTxt").style.display='none';
	document.getElementById("CloseComm").style.display='none';
	}
	else {
	//document.getElementById("enftypefld").style.display='none';
	document.getElementById("enftypel").style.display='none';
	document.getElementById("ShowText").style.display='none';
	document.getElementById("CloseTxt").style.display='';
	document.getElementById("CloseComm").style.display='';
	}
}

function submitsfrm(){

var oktosubmit = "yes"

if((document.furtherAction.FurtherActionDesc.value=="")&&(document.getElementById('enftypel').style.display=='')){
	
	var oktosubmit = "no";
	 document.furtherAction.FurtherActionDesc.style.border = "2px solid F00000";

	}
	else{
	document.furtherAction.FurtherActionDesc.style.border = "1px solid 5f2167";
	}	
	
if((document.furtherAction.CloseComments.value=="")&&(document.getElementById('CloseComm').style.display=='')){
	
	var oktosubmit = "no";
	 document.furtherAction.CloseComments.style.border = "2px solid F00000";

	}
	else{
	document.furtherAction.CloseComments.style.border = "1px solid 5f2167";
	}	
			
if (oktosubmit=="no"){
	//document.getElementById("reqflds").style.display='';
	return false;
}

}
</script>

	<cfform action="#self#?fuseaction=main.ObservationReviewed" method="post" name="furtherAction" enctype="multipart/form-data" onsubmit="return submitsfrm();">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					
					<input type="hidden" name="OBNum" value="#OBNum#">
					
									
					<tr>
						<td class="formlable" align="left" width="40%"><strong>Is further action required?</strong></td>
						<td class="bodytextgrey" >Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="FurtherAction" value="1" onchange="checkenftype(this.value);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;
						<input type="radio" name="FurtherAction" value="0" onchange="checkenftype(this.value);"></td>
					</tr>
					<cfif getObservationDetail.FeedBackRequired EQ 1>
					
					<tr id="CloseComm">
						<td class="formlable" align="left" width="40%"><strong>Comments:*</strong></td>
						<td class="bodytextgrey" ><cftextarea name="CloseComments" class="selectgen" rows="3" cols="35" required="no" message="Please enter feedback" id="widetxt" ></cftextarea>				
					</tr>	
					<tr id="CloseTxt">
						<td colspan="2" class="bodytextgrey" align="left">The person who recorded this observation requested feedback - please provide details which will be emailed to the observer.
						</td>
					</tr> 			
					</cfif>
					<tr id="enftypel">
						<td class="formlable" align="left" width="40%"><strong>Action taken/Recommended:*</strong></td>
						<td class="bodytextgrey" ><cftextarea name="FurtherActionDesc" class="selectgen" rows="3" cols="35" required="no" message="Please enter action or recommendation" id="widetxt" ></cftextarea>				
					</tr>
					<tr id="ShowText">
						<td colspan="2" class="bodytextgrey" align="left">Please provide as much detail as possible - this information will be passed to the OU Admin.<br><br>
						<!--- Once Facilities have completed the necessary actions they will respond to you via email, after which you will need to close the Observation. --->
						</td>
					</tr> 
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="submit" value="OK" class="selectGenBTN">&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>



