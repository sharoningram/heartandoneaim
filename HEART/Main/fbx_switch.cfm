
<cfparam name="type" default="">

<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="testq">
<cfquery name="getEmailDetail" datasource="#request.HEART_DS#">
SELECT  top 10  Group_Name,  SiteName,Business_Line_ID,OUid
		FROM    [#oneaimSQLname#].[dbo].[Groups],[#oneaimSQLname#].[dbo].[GroupLocations]
		<!--- WHERE 
		<cfif listlen(cohabitou) eq 2>
				Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#listgetat(cohabitou,2)#">
			<cfelse>
				Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#project#>
			</cfif>
	and	GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value=#site#> --->
</cfquery>

<cfoutput query="getEmailDetail">
#group_name# #sitename#<br>
</cfoutput>
</cfcase>
<cfcase value="main">	
	<cfset attributes.xfa.ReviewObservation = "main.ReviewObservation">
	<cfset attributes.xfa.ActionObservation = "main.ActionObservation">
	<cfparam name="showall" default="No">
	<cfif isdefined("ToggleView") and ToggleView EQ "Dashboard">
		<!--- <cfinclude template="queries/qry_getObservationsForReview.cfm"> --->
		<cfinclude template="queries/qry_getSafetyRulesChart.cfm">
		<cfinclude template="queries/qry_getSafetyEssentialsChart.cfm">
		<cfinclude template="queries/qry_getBUs.cfm">
		<cfinclude template="queries/qry_getObsDashboard.cfm">
		<cfinclude template="actions/act_buildObsDashboardStructs.cfm">
		<cfinclude template="displays/dsp_Dashboard.cfm">
	<cfelse>	

		<cfinclude template="queries/qry_getObservationsForReview.cfm">
		<cfinclude template="queries/qry_getObservationsForAction.cfm">
		<cfinclude template="queries/qry_getWithdrawnObservations.cfm">
		<cfinclude template="queries/qry_getObservationsForReviewWatchList.cfm">
		<cfinclude template="queries/qry_getObservationsForActionWatchList.cfm">
		<cfinclude template="displays/dsp_TaskBoard.cfm">
	
	</cfif>	
</cfcase>

<cfcase value="Audit">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getObsAudit.cfm">
	<cfinclude template="actions/act_whocanaccessobs.cfm">
	<cfinclude template="displays/dsp_ObsAudit.cfm">
	<cfinclude template="displays/dsp_auditTrail.cfm">
</cfcase>
<!--- <cfcase value="ObsType">

	<cfinclude template="displays/dsp_ObservationType.cfm">
</cfcase> --->

<cfcase value="mytasks">

	<cfinclude template="displays/dsp_mytasks.cfm">
</cfcase>

<cfcase value="Reports">

	<cfinclude template="displays/dsp_Reports.cfm">
</cfcase>

<cfcase value="ObsTypeReport">

	<cfinclude template="displays/dsp_ObsTypeReport.cfm">
</cfcase>

<cfcase value="EnterObservation">

	<cfset attributes.xfa.popgroupinfo = "main.popgroupinfo">
	<cfinclude template="queries/qry_getbus.cfm">
	<cfinclude template="queries/qry_getous.cfm">
	<cfinclude template="queries/qry_getprojects.cfm">
	<cfinclude template="queries/qry_getrules.cfm">
	<cfinclude template="queries/qry_getessentials.cfm">
	<cfinclude template="displays/dsp_EnterObservation.cfm">
</cfcase>

<cfcase value="AddObservation">

	<cfinclude template="actions/act_InsertObservation.cfm">
</cfcase>
<cfcase value="submitamendedObservation">
	<cfinclude template="actions/act_amendsubmitObservation.cfm">
</cfcase>

<cfcase value="ReviewObservation">
	<!--- <cfset attributes.xfa.popgroupinfo = "main.popgroupinfo">  --->
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getamedcomms.cfm">
	<cfinclude template="queries/qry_getrestorecomms.cfm">
		<cfquery name="getbuou" datasource="#request.dsn#">
			SELECT        Business_Line_ID, OUid
			FROM            Groups
			WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getObservationDetail.ProjectNumber#">)
		</cfquery>
		<!--- <cfquery name="getBUID" datasource="#request.dsn#">
		SELECT    NewDials.ID, NewDials.Name
		FROM   NewDials
		WHERE NewDials.Name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getObservationDetail.ObservationBU#">
		</cfquery>	 --->	
		<cfset BusinessUnitID = getbuou.Business_Line_ID>
		
		<!--- <cfquery name="getOUID" datasource="#request.dsn#">
		SELECT    NewDials.ID, NewDials.Name
		FROM   NewDials
		WHERE NewDials.Name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getObservationDetail.ObservationOU#">
		</cfquery> --->
		<cfif trim(getObservationDetail.cohabitOU) neq '' and trim(getObservationDetail.cohabitOU) gt 0>
			<cfset OperatingUnitID = getObservationDetail.cohabitOU>
		<cfelse>
			<cfset OperatingUnitID = getbuou.OUid>
		</cfif>
	
		<cfif getObservationDetail.withdrawnto eq 1>
			<cfif getObservationDetail.withdrawnby eq request.userlogin>
				<cfset attributes.xfa.popgroupinfo = "main.popgroupinfo">
				<cfinclude template="queries/qry_getbus.cfm">
				<cfinclude template="queries/qry_getous.cfm">
				<cfinclude template="queries/qry_getcohabits.cfm">
				<cfinclude template="queries/qry_getprojects.cfm">
				<cfinclude template="queries/qry_getamendsites.cfm">
				<cfinclude template="queries/qry_getrules.cfm">
				<cfinclude template="queries/qry_getessentials.cfm">
				<cfinclude template="displays/dsp_amendObservation.cfm">
			<cfelse>
				<cfif getObservationDetail.createdbyemail eq request.userlogin and getObservationDetail.status eq "Cancelled" and getObservationDetail.EscalateOneAim eq 1>
					<cfset attributes.xfa.popgroupinfo = "main.popgroupinfo">
					<cfinclude template="queries/qry_getbus.cfm">
					<cfinclude template="queries/qry_getous.cfm">
					<cfinclude template="queries/qry_getcohabits.cfm">
					<cfinclude template="queries/qry_getprojects.cfm">
					<cfinclude template="queries/qry_getamendsites.cfm">
					<cfinclude template="queries/qry_getrules.cfm">
					<cfinclude template="queries/qry_getessentials.cfm">
					<cfinclude template="displays/dsp_amendObservation.cfm">
				<cfelse>				
					<cfinclude template="displays/dsp_ViewObservation.cfm">	
				</cfif>
			</cfif>
		
		<cfelse>
		
		<cfif listfindnocase(request.userlevel,"Global Admin")  gt 0 and getObservationDetail.Status EQ "Submitted">
			<cfinclude template="displays/dsp_ReviewObservation.cfm">
		<cfelseif  listfindnocase(request.userlevel,"BU Admin")  gt 0 and getObservationDetail.Status EQ "Submitted" and listfind(request.userBUs,BusinessUnitID) gt 0>
			<cfinclude template="displays/dsp_ReviewObservation.cfm">
		<cfelseif  listfindnocase(request.userlevel,"OU Admin") gt 0  and getObservationDetail.Status EQ "Submitted" and listfind(request.userOUs,OperatingUnitID) gt 0>
			<cfinclude template="displays/dsp_ReviewObservation.cfm">
		<!--- <cfelseif  listfindnocase(request.userlevel,"HSSE Advisor") gt 0  and getObservationDetail.Status EQ "Submitted" and listfind(request.userOUs,OperatingUnitID) gt 0>
			<cfinclude template="displays/dsp_ReviewObservation.cfm"> --->

		<cfelseif  listfindnocase(request.userlevel,"HSSE Advisor") gt 0  and getObservationDetail.Status EQ "Submitted">
			<cfif listfind(request.hsseadvlocs,getObservationDetail.siteid) gt 0>
				<cfinclude template="displays/dsp_ReviewObservation.cfm">
			<cfelseif  listfind(request.userOUs,OperatingUnitID) gt 0>
				<cfinclude template="displays/dsp_ReviewObservation.cfm">
			<cfelse>
				<cfinclude template="displays/dsp_ViewObservation.cfm">	
			</cfif>
		<cfelse>
			<cfinclude template="displays/dsp_ViewObservation.cfm">	
		</cfif>
		
		</cfif>
		
		
	<!--- <cfloop list="#request.userlevel#" index="ru">
		<cfif listfindnocase("Global Admin",ru) gt 0 and getObservationDetail.Status EQ "Submitted">		
			<cfinclude template="displays/dsp_ReviewObservation.cfm">
			<cfbreak>
		<cfelseif listfindnocase("BU Admin",ru) gt 0 and getObservationDetail.Status EQ "Submitted" and listfind(request.userBUs,BusinessUnitID) gt 0>		
			<cfinclude template="displays/dsp_ReviewObservation.cfm">
			<cfbreak>	
		<cfelseif listfindnocase("OU Admin,HSSE Advisor",ru) gt 0 and getObservationDetail.Status EQ "Submitted" and listfind(request.userOUs,OperatingUnitID) gt 0>		
			<cfinclude template="displays/dsp_ReviewObservation.cfm">
			<cfbreak>	
		<cfelse>
			<cfinclude template="displays/dsp_ViewObservation.cfm">	
			<cfbreak>
		</cfif>	
	</cfloop>	 --->
</cfcase>

<cfcase value="amendObs">
	<cfset attributes.xfa.manageamendment = "main.manageamendment">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="displays/dsp_addAmendComment.cfm">
</cfcase>
<cfcase value="restore">
	<cfset attributes.xfa.managerestore = "main.managerestore">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="displays/dsp_addrestoreComment.cfm">
</cfcase>
<cfcase value="managerestore">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="actions/act_managerestore.cfm">
</cfcase>

<cfcase value="manageamendment">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="actions/act_manageamendment.cfm">
</cfcase>
 <cfcase value="reviewObv">
 	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="displays/dsp_ReviewFurtherAction.cfm">
</cfcase>

<cfcase value="ObservationReviewed">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="actions/act_ObservationReviewed.cfm">
</cfcase>

 <cfcase value="OneAim">
	<cfinclude template="displays/dsp_EscalateToOneAim.cfm">
</cfcase>

<cfcase value="ToOneAim">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="actions/act_ToOneAim.cfm">
</cfcase>

<cfcase value="CancelObv">
	<cfinclude template="displays/dsp_CancelObservation.cfm">
</cfcase>

<cfcase value="Cancelled">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="actions/act_CancelObservation.cfm">
</cfcase>

<cfcase value="ActionObservation">
	<!--- <cfset attributes.xfa.popgroupinfo = "main.popgroupinfo"> --->
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getamedcomms.cfm">
	<cfinclude template="queries/qry_getrestorecomms.cfm">
		<cfquery name="getbuou" datasource="#request.dsn#">
			SELECT        Business_Line_ID, OUid
			FROM            Groups
			WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getObservationDetail.ProjectNumber#">)
		</cfquery>
	<!--- <cfquery name="getBUID" datasource="#request.dsn#">
		SELECT    NewDials.ID, NewDials.Name
		FROM   NewDials
		WHERE NewDials.Name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getObservationDetail.ObservationBU#">
		</cfquery>	 --->	
		<cfset BusinessUnitID = getbuou.Business_Line_ID>
		
	<!--- 	<cfquery name="getOUID" datasource="#request.dsn#">
		SELECT    NewDials.ID, NewDials.Name
		FROM   NewDials
		WHERE NewDials.Name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getObservationDetail.ObservationOU#">
		</cfquery> --->
		<cfset OperatingUnitID = getbuou.OUid>
		
		
		<cfif listfindnocase(request.userlevel,"Global Admin")  gt 0 and getObservationDetail.Status EQ "Reviewed">
			<cfinclude template="displays/dsp_ActionObservation.cfm">
		<cfelseif  listfindnocase(request.userlevel,"BU Admin")  gt 0 and getObservationDetail.Status EQ "Reviewed" and listfind(request.userBUs,BusinessUnitID) gt 0>
			<cfinclude template="displays/dsp_ActionObservation.cfm">
		<cfelseif  listfindnocase(request.userlevel,"OU Admin") gt 0  and getObservationDetail.Status EQ "Reviewed" and listfind(request.userOUs,OperatingUnitID) gt 0>
			<cfinclude template="displays/dsp_ActionObservation.cfm">
		
		<cfelse>
			<cfinclude template="displays/dsp_ViewObservation.cfm">	
		</cfif>
		
		
		<!--- 
	<cfloop list="#request.userlevel#" index="ru">
		<cfif listfindnocase("Global Admin",ru) gt 0 and getObservationDetail.Status EQ "Reviewed">		
			<cfinclude template="displays/dsp_ActionObservation.cfm">
			<cfbreak>
		<cfelseif listfindnocase("BU Admin",ru) gt 0 and getObservationDetail.Status EQ "Reviewed" and listfind(request.userBUs,BusinessUnitID) gt 0>		
			<cfinclude template="displays/dsp_ActionObservation.cfm">
			<cfbreak>	
		<cfelseif listfindnocase("OU Admin,HSSE Advisor",ru) gt 0 and getObservationDetail.Status EQ "Reviewed" and listfind(request.userOUs,OperatingUnitID) gt 0>		
			<cfinclude template="displays/dsp_ActionObservation.cfm">
			<cfbreak>	
		<cfelse>
			<cfinclude template="displays/dsp_ViewObservation.cfm">	
			<cfbreak>
		</cfif>	
	</cfloop>	 --->
	
</cfcase>

<cfcase value="ViewObservation">
	<!--- <cfset attributes.xfa.popgroupinfo = "main.popgroupinfo"> --->
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getamedcomms.cfm">
	<cfinclude template="queries/qry_getrestorecomms.cfm">
	<cfinclude template="displays/dsp_ViewObservation.cfm">
</cfcase>
<cfcase value="exportobservation">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getamedcomms.cfm">
	<cfinclude template="queries/qry_getrestorecomms.cfm">
	<cfinclude template="actions/act_ObservationPDF.cfm">
</cfcase>
<cfcase value="CloseObv">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getamedcomms.cfm">
	<cfinclude template="queries/qry_getrestorecomms.cfm">
	<cfinclude template="displays/dsp_CloseObservation.cfm">
</cfcase>

<cfcase value="ObvClosed">
	<cfinclude template="queries/qry_getObservationDetail.cfm">
	<cfinclude template="queries/qry_getamedcomms.cfm">
	<cfinclude template="queries/qry_getrestorecomms.cfm">
	<cfinclude template="actions/act_ObservationAction.cfm">
</cfcase>



<cfcase value="popgroupinfo">
	<cfinclude template="actions/act_popgroupinfo.cfm">
</cfcase>

<cfcase value="diagnostic">
	<cfoutput>
		Cookie.Useremail: #cookie.useremail#<br>
		Request.Useremail: #request.Useremail#<br>
		UserOU: #request.userOUs#<br>
		UserBU:  #request.userBUs#<br>
		UserLev: #request.userlevel#<br>
	</cfoutput>
	
</cfcase>

<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
