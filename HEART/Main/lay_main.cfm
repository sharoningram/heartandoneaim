

<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="main">
		<cfset titleaddon = " - Main Page">
	</cfcase>
	<cfcase value="EnterObservation">
	<script type="text/javascript">
		if(window.innerWidth<=760){
			alert('If you need to upload an image please only use images already saved to your device. Please do not use your camera to capture an image while completing this form');
	}
	</script>
	</cfcase>
	<cfcase value="ReviewObservation">
		<cfif getObservationDetail.withdrawnto eq 1>
			<cfif getObservationDetail.withdrawnby eq request.userlogin>
				<script type="text/javascript">
					if(window.innerWidth<=760){
					alert('If you need to upload an image please only use images already saved to your device. Please do not use your camera to capture an image while completing this form');
						}
				</script>
			</cfif>
		</cfif>
	</cfcase>
</cfswitch>
<cfoutput>
<cfif fusebox.fuseaction eq "main">

<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr bgcolor="ffffff">
	<td align="left" colspan="2" width="90%">
		<table cellpadding="0" border="0" cellspacing="0" width="100%">
			<tr>
				<td width="50%"><img src="images/HEARTLogo.jpg" border="0" class="img" align="left"></td>
				<td width="50%" align="right"><img src="images/logoR.jpg" border="0" class="img2" align="right"></td>
			</tr>
		</table>
</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<!--- <tr class="purplebg"><td style="font-family:Segoe UI;color:ffffff;font-size:12pt;">&nbsp;<a href="/#getappconfig.heartPath#" style="color:ffffff;text-decoration:none;">HEART- Harm Elimination and Recognition Training</a></td><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;"><cfif trim(request.fname) neq ''>Welcome <cfoutput>#request.fname# #request.lname#</cfoutput>&nbsp;&nbsp;</cfif></td>
</tr> --->

<tr class="purplebg"><td class="purptxtlrg" width="90%" id="mainhearttd">&nbsp;HEART<span id="HEARTheadline">- Harm Elimination and Recognition Training</span></td><td align="right"  class="purptxtsm"><cfif trim(request.fname) neq ''>Welcome&nbsp;<cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td><td align="right" class="helptxt">&nbsp;<cfoutput><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');" class="helptxt">Help</a></cfoutput>&nbsp;</span><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;">&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<!--- <script type="text/javascript">
	if(window.innerWidth<=760){
	var textout = "HEART";
	document.getElementById("mainhearttd").innerHTML=textout;
	}
</script> --->
#Fusebox.layout#
<cfelse>
<div class="stripe1"><table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td>
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr>
	<td  id="mainhearttd" class="purptxtlrg" >
	
	<cfif fusebox.fuseaction eq "EnterObservation">
			&nbsp;HEART
	<cfelseif fusebox.fuseaction eq "ReviewObservation">
		&nbsp;HEART
		<cfelseif fusebox.fuseaction eq "ActionObservation">
		&nbsp;HEART
		<cfelseif fusebox.fuseaction eq "ViewObservation">
		&nbsp;HEART
		<cfelseif fusebox.fuseaction eq "audit">
		&nbsp;HEART
	</cfif>
	<span id="HEARTheadline">
	<cfif fusebox.fuseaction eq "ObsType">
		&nbsp;Select Observation Type
	<cfelseif fusebox.fuseaction eq "EnterObservation">
		 - Harm Elimination and Recognition Training
	<cfelseif fusebox.fuseaction eq "ReviewObservation">
		- Harm Elimination and Recognition Training
	<cfelseif fusebox.fuseaction eq "ActionObservation">
		- Harm Elimination and Recognition Training
	<cfelseif fusebox.fuseaction eq "Reports">
		&nbsp;Reports Home		
	<cfelseif fusebox.fuseaction eq "reviewObv">
		&nbsp;Review Observation	
	<cfelseif fusebox.fuseaction eq "OneAim">
		&nbsp;Escalate To oneAIM	
	<cfelseif fusebox.fuseaction eq "CancelObv">
		&nbsp;Cancel Observation	
	<cfelseif fusebox.fuseaction eq "CloseObv">
		&nbsp;Close Observation			
	<cfelseif fusebox.fuseaction eq "ViewObservation">
		- Harm Elimination and Recognition Training
	<cfelseif fusebox.fuseaction eq "audit">
		- Harm Elimination and Recognition Training
	<cfelseif fusebox.fuseaction eq "amendObs">
		&nbsp;Amend Observation
	</cfif>
	</span>

	</td><td align="right"  class="purptxtsm"><cfif trim(request.fname) neq ''>Welcome <cfoutput>#request.fname# #request.lname#</cfoutput>&nbsp;&nbsp;</cfif></td>
	<td align="right" class="helptxt" width="2%">&nbsp;<cfoutput><a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.heartPath#/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');" class="helptxt">Help</a></cfoutput>&nbsp;</span>
<td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;" width="1%">&nbsp;&nbsp;&nbsp;</td>
</tr>
</table></td></tr>
<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>

<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>

</table>

<!--- <script type="text/javascript">
	if(window.innerWidth<=760){
	var textout = "HEART";
	document.getElementById("mainhearttd").innerHTML=textout;
	}
</script> --->

</div>

<!--- Tube map start --->


<cfif listfindnocase("EnterObservation,ReviewObservation,ActionObservation,ViewObservation,Audit",fusebox.fuseaction) gt 0>
	<cfif isdefined("getObservationDetail.status") and getObservationDetail.status eq "Cancelled">
		<cfset usecolwide = 10>
	<cfelse>
		<cfset usecolwide = 8>
	</cfif>
<cfoutput> 
<table cellpadding="0" border="0" cellspacing="0" width="100%">
			<tr id="spcrow1ie">
				<td height="1" bgcolor="ffffff" colspan="#usecolwide#"><img src="images/spacer.gif" height="30" border="0"></td>
			</tr>
			<tr id="spcrow1mob">
				<td height="1" bgcolor="ffffff" colspan="#usecolwide#"><img src="images/spacer.gif" height="24" border="0"></td>
			</tr>
</table>
	<table cellpadding="0" border="0" cellspacing="0" width="100%">
			<tr>
				<td height="1" bgcolor="ffffff" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="6b84b3" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
			<tr>
			
			<td width="1" bgcolor="6b84b3"><img src="images/spacer.gif" height="1" border="0"></td>
			<td class="tubemapon" width="10%" align="center">Observation&nbsp;Recorded</td>
			
			<cfset obsimg = "arrowpink">
			<cfset revclass = "off">
			<cfset revimg = "arrowpink">
			<cfset waitclass = "off">
			<cfset waitimg = "arrowpink">
			<cfset clsdclass = "off">
			<cfset clsdimg = "arrowwhite">
			<cfif isdefined("getObservationDetail.recordcount")>
				<cfif getObservationDetail.status eq "Submitted">
					<cfset revclass = "on">
					<cfset revimg = "arrowpink">
					<cfset obsimg = "arrowclear">
				<cfelseif getObservationDetail.status eq "Reviewed">
					<cfset revclass = "on">
					<cfset revimg = "arrowclear">
					<cfset obsimg = "arrowclear">
					<cfset waitclass = "on">
					<cfset waitimg = "arrowpink">
				<cfelseif getObservationDetail.status eq "Closed">
					<cfset revclass = "on">
					<cfset revimg = "arrowclear">
					<cfset obsimg = "arrowclear">
					<cfset waitclass = "on">
					<cfset waitimg = "arrowclear">
					<cfset clsdclass = "on">
				<cfelseif getObservationDetail.status eq "Cancelled">
					<cfset revclass = "off">
					<cfset revimg = "arrowpink">
					<cfset obsimg = "arrowpink">
					<cfset waitclass = "off">
					<cfset waitimg = "arrowpink">
					<cfset clsdclass = "off">
					<cfset clsdimg = "arrowteal">
				</cfif>
				
			</cfif>
			<td bgcolor="5f2468"><img src="images/#obsimg#.png" border="0"></td>
			<td class="tubemap#revclass#" width="10%" align="center">Under&nbsp;Review</td>
			<td class="tubemap#revclass#"><img src="images/#revimg#.png" border="0"></td>
			<td class="tubemap#waitclass#" width="10%" align="center">Waiting&nbsp;for&nbsp;Action</td>
			<td class="tubemap#waitclass#"><img src="images/#waitimg#.png" border="0"></td>
			<td class="tubemap#clsdclass#" width="10%" align="center">Closed</td>
			<td class="tubemap#clsdclass#"><img src="images/#clsdimg#.png" border="0"></td>
			<cfif  isdefined("getObservationDetail.status") and getObservationDetail.status eq "Cancelled">
				<td class="tubemapcancel" width="10%" align="center">Cancelled</td>
				<td class="tubemapcancel"><img src="images/arrowwhite.png" border="0"></td>
			</cfif>
			<td width="80%">&nbsp;</td>
			<tr>
			<tr>
				<td height="1" bgcolor="6b84b3" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
</table>
</cfoutput>
</cfif>
<!--- Tube map end --->
<cfset showleftnav = "yes">	
<cfset obsstyle = "obstyle2">

<!--- <cfif fusebox.fuseaction neq "audit">
<table cellpadding="0" border="0" cellspacing="0"  id="mytab" class="mainouttable">
<cfelse> --->
<table cellpadding="0" border="0" cellspacing="0"  id="mytab" class="mainouttable">
<!--- </cfif> --->
	<tr id="spcrow2ie">
		<td colspan="2"><img src="images/spacer.gif" height="30" border="0"></td>
	</tr>
	<tr id="spcrow2mob">
		<cfif listfindnocase("reviewObv,OneAim,CancelObv,CloseObv,popgroupinfo,amendObs",fusebox.fuseaction) eq 0>
		<td colspan="2"><img src="images/spacer.gif" height="5" border="0"></td>
		<cfelse>
		<td colspan="2"><img src="images/spacer.gif" height="30" border="0"></td>
		</cfif>
	</tr>
	<tr>
	<td width="7%" align="center" id="ltoplempty">&nbsp;</td>
	<td width="93%">
	<cfif listfindnocase("reviewObv,OneAim,CancelObv,CloseObv,popgroupinfo,amendObs",fusebox.fuseaction) eq 0>
		<table  cellpadding="0" cellspacing="0" width="100%" border="0">
		
			<tr>
				<!--- Top tab navigation start --->
				<cfif listfindnocase("ReviewObservation,ActionObservation,ViewObservation,Audit,EnterObservation",fusebox.fuseaction) gt 0>
					<cfif listfindnocase("ReviewObservation,ActionObservation,ViewObservation,EnterObservation",fusebox.fuseaction) gt 0>
						<cfset linktofuse = fusebox.fuseaction>
						<cfset obsstyle = "obstyle1">
						<cfset obsvalign = "middle">
						<cfset obstabs = "">
						
						<cfset atstyle = "atstyle1">
						<cfset atvalign = "middle">
						<cfset attabs = "sm">
						<td  class="obstyle1bk" ><img src="images/tableft#obstabs#.png" border="0"></td>
						<td class="#obsstyle#" <cfif listfindnocase("ReviewObservation,ActionObservation,ViewObservation,EnterObservation",fusebox.fuseaction) eq 0>background="images/smtabbg.png"</cfif>><cfif isdefined("getObservationDetail.recordcount")><a href="index.cfm?fuseaction=main.#linktofuse#&OBNum=#getObservationDetail.observationnumber#" class="#obsstyle#">Observation&nbsp;-&nbsp;#getObservationDetail.TrackingYear##numberformat(getObservationDetail.TrackingNum,"00000")#<cfelse>Observation</cfif></a></td>
						<td  class="obstyle1bk" ><img src="images/tabright#obstabs#.png" border="0"></td>
				<!---  --->
						<cfif fusebox.fuseaction neq "EnterObservation">
							<td  class="atstyle1bk" ><img src="images/tableft#attabs#.png" border="0"></td>
							<td  class="#atstyle#"  <cfif fusebox.fuseaction neq "audit">background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=main.audit&OBNum=#getObservationDetail.observationnumber#" class="#atstyle#">Audit&nbsp;Trail</a></td>
							<td  class="atstyle1bk" ><img src="images/tabright#attabs#.png" border="0"></td>
						</cfif>
					<cfelseif  fusebox.fuseaction eq "audit">
						<cfif getObservationDetail.status eq "Submitted">
							<cfset linktofuse = "ReviewObservation">
						<cfelseif getObservationDetail.status eq "Reviewed">
							<cfset linktofuse = "ActionObservation">
						<cfelse>
							<cfset linktofuse = "ViewObservation">
						</cfif>
						<cfset obsstyle = "obstyle2">
						<cfset obsvalign = "middle">
						<cfset obstabs = "sm">
						
						
						<cfset atstyle = "atstyle2">
						<cfset atvalign = "middle">
						<cfset attabs = "">
						<td class="obstyle1bk"><img src="images/tableft#obstabs#.png" border="0"></td>
						<td class="#obsstyle#" <cfif listfindnocase("ReviewObservation,ActionObservation,ViewObservation,EnterObservation",fusebox.fuseaction) eq 0>background="images/smtabbg.png"</cfif>><cfif isdefined("getObservationDetail.recordcount")><a href="index.cfm?fuseaction=main.#linktofuse#&OBNum=#getObservationDetail.observationnumber#" class="#obsstyle#">Observation&nbsp;-&nbsp;#getObservationDetail.TrackingYear##numberformat(getObservationDetail.TrackingNum,"00000")#<cfelse>Enter&nbsp;Observation</cfif></a></td>
						<td class="obstyle1bk"><img src="images/tabright#obstabs#.png" border="0"></td>
						<td  class="atstyle1bk" ><img src="images/tableft#attabs#.png" border="0"></td>
						<td  class="#atstyle#"  <cfif fusebox.fuseaction neq "audit">background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=main.audit&OBNum=#getObservationDetail.observationnumber#" class="#atstyle#">Audit&nbsp;Trail</a></td>
						<td  class="atstyle1bk" ><img src="images/tabright#attabs#.png" border="0"></td>
						
					</cfif>
				</cfif>
				
				
				<!--- Top tab navigation end --->
				<!--- <td align="right" width="100%" style="font-family:Segoe UI;font-size:9pt;" valign="bottom"><a href="/#getappconfig.heartPath#" style="font-family:Segoe UI;font-size:10px;text-decoration:none;color:5f2468;"><<&nbsp;HEART&nbsp;Home</a></td>  --->
				<td width="90%"></td>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><cfoutput><a href="/#getappconfig.heartPath#" class="homelnk" ><<&nbsp;HEART&nbsp;Home</a></cfoutput>&nbsp;&nbsp;</td>
			</tr>
			
		</table>
	</cfif>	
	</td>
</tr>
<tr>
<!--- 	******** Begin Left navigation area ********* --->
	<td valign="top"  bgcolor="ffffff" id="mainlefttdcell">
		<cfswitch expression="#fusebox.fuseaction#">
			<cfcase value="ReviewObservation">		
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td class="bodyTextPurple">
					What&nbsp;Next...<br><br>
					<strong class="bodytextsmpurple">You can take the following actions on this observation:</strong> <br><br>
				<cfif getObservationDetail.Status EQ "Submitted">
				<cfif trim(getObservationDetail.withdrawnby) neq ''>
					<cfif getObservationDetail.withdrawnby eq request.userlogin>
					<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>	
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>		
					</cfif>
				<cfelse>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0><!---  or> --->
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.reviewObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.OneAim&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Escalate&nbsp;to&nbsp;oneAIM</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
				<cfelseif  listfindnocase(request.userlevel,"HSSE Advisor") gt 0 and listfind(request.hsseadvlocs,getObservationDetail.siteid) gt 0>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.reviewObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.OneAim&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Escalate&nbsp;to&nbsp;oneAIM</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
				<cfelseif  listfindnocase(request.userlevel,"HSSE Advisor") gt 0 and listfind(request.userOUs,OperatingUnitID) gt 0>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.reviewObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.OneAim&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Escalate&nbsp;to&nbsp;oneAIM</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
				<cfelse>
					<cfif getObservationDetail.createdbyemail eq request.userlogin>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
					</cfif>
				<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>	
				</cfif>
				
				</cfif>
				
				<cfelse>
					<cfif getObservationDetail.Status EQ "Reviewed" or getObservationDetail.Status EQ "Closed">
						<cfif trim(getObservationDetail.withdrawnby) neq ''>
							<cfif getObservationDetail.withdrawnby eq request.userlogin>
								<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>		
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>		
							</cfif>
						</cfif>
					<cfelseif  getObservationDetail.Status EQ "Cancelled" and getObservationDetail.Escalateoneaim eq 1>
						<cfif trim(getObservationDetail.withdrawnby) neq ''>
							<cfif getObservationDetail.withdrawnby eq request.userlogin>
								<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>		
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>	
							<cfelseif getObservationDetail.createdbyemail eq request.userlogin>
								<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>		
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>	
							</cfif>
						</cfif>
					</cfif>
				<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>	
				</cfif>
				
					<!--- 
			<cfloop list="#request.userlevel#" index="ru">
				<cfif listfindnocase("Global Admin,BU Admin,OU Admin,HSSE Advisor",ru) gt 0 and getObservationDetail.Status EQ "Submitted">		
				
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.reviewObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.OneAim&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Escalate&nbsp;to&nbsp;oneAIM</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>
					<cfbreak>
				<cfelse>				
					<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>	
					<cfbreak>
				</cfif>	
				
			</cfloop>		 --->	
					</td>
				</tr>
				</table>
			</cfcase>
			
			<cfcase value="ActionObservation">				
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td class="bodyTextPurple">
					What&nbsp;Next...<br><br>
					<strong class="bodytextsmpurple">You can take the following actions on this observation:</strong> <br><br>
					
					
			<cfif getObservationDetail.Status EQ "Reviewed">
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
				<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CloseObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Close</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>
				<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
				<cfelse>
					<cfif getObservationDetail.createdbyemail eq request.userlogin>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
					<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
					</cfif>
					<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>	
				</cfif>
				<cfelse>
					<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>	
				</cfif>
				
		<!--- 		
			<cfloop list="#request.userlevel#" index="ru">
				<cfif listfindnocase("Global Admin,BU Admin,OU Admin",ru) gt 0 and getObservationDetail.Status EQ "">		
							
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CloseObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Close</a><br>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>
				<cfbreak>
				<cfelse>				
					<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>	
				<cfbreak>
				</cfif>	
			</cfloop>		 --->	
					
					</td>
				</tr>
				</table>
			</cfcase>
			
			<cfcase value="ViewObservation">
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td class="bodyTextPurple">
					What&nbsp;Next...<br><br>
					<strong class="bodytextsmpurple">You can take the following actions on this observation:</strong> <br><br>		
					<cfif getObservationDetail.Status EQ "Submitted" or getObservationDetail.Status EQ  "Reviewed">
						<cfif getObservationDetail.createdbyemail eq request.userlogin>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
						</cfif>
					<cfelseif getObservationDetail.Status EQ "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.amendObs&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Amend</a><br>
						</cfif>
					<cfelseif getObservationDetail.Status EQ "Cancelled" and getObservationDetail.Escalateoneaim eq 1 and trim(getObservationDetail.withdrawnby) eq ''>
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.restore&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Restore</a><br>
						</cfif>
					<cfelseif getObservationDetail.Status EQ "Cancelled" and getObservationDetail.Escalateoneaim eq 1 and trim(getObservationDetail.withdrawnby) neq ''>
							<cfif getObservationDetail.withdrawnby eq request.userlogin>
								<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>		
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>	
							<cfelseif getObservationDetail.createdbyemail eq request.userlogin>
								<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>		
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>	
							<cfelseif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
							<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Resubmit</a><br>		
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=main.CancelObv&OBNum=#OBNum#','Info','500','325','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel&nbsp;Observation</a><br>	
							</cfif>
					</cfif>			
					<a href="javascript:void(0);" onclick="location.href='index.cfm?fuseaction=main.main'" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Back</a><br>					
					
					
					</td>
				</tr>
				</table>
			</cfcase>
			
			<cfcase value="EnterObservation">
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td class="bodyTextPurple">
					What&nbsp;Next...<br><br>
					<strong class="bodytextsmpurple">You can take the following actions on this observation:</strong> <br><br>					
					<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>					
					
					
					</td>
				</tr>
				</table>
			</cfcase>
			
		</cfswitch>
	</td>
<!--- 	******** End Left navigation area ********* --->
	<td width="100%">#Fusebox.layout#</td></tr>
</table>
<!--- 
<script type="text/javascript">
if(window.innerWidth<=760){
	//document.getElementById("ltoplempty").style.display='none';
	//document.getElementById("mainlefttdcell").style.display='none';
	document.getElementById("spcrow2ie").style.display='none';
	document.getElementById("spcrow2mob").style.display='';
 	document.getElementById("spcrow1ie").style.display='none';
	document.getElementById("spcrow1mob").style.display='';
	}
	else {
	document.getElementById("spcrow2ie").style.display='';
	document.getElementById("spcrow2mob").style.display='none';
	document.getElementById("spcrow1ie").style.display='';
	document.getElementById("spcrow1mob").style.display='none';
	}
</script> --->

</cfif>
<!--- <table border=0 width="100%"  cellpadding=0 cellspacing=0>
	<tr>
		<td width="100%" valign="top"> --->
		
	<!--- 	</td>
	</tr>
</table> --->

</cfoutput>