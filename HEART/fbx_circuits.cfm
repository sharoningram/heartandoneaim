<!---
<fusedoc 
	fuse="FBX_Circuits.cfm" 
	language="ColdFusion" 
	version="2.0">
	<responsibilities>
		I define the Circuits structure used with Fusebox 3.0
	</responsibilities>	
	<io>
		<in>
			<structure name="fusebox.circuits">
			</structure>
		</in>
		<out>
			<string 
				name="fusebox.circuits.*" 
				comments="each circuit must have an entry into fusebox.circuits"> 
		</out>
	</io>
</fusedoc>
--->

<!--- this file contains all the circuit definitions for the fusebox --->
<cfinclude template="appconfig.cfm">

<cfset fusebox.Circuits.home="#heartroot#">
<cfset fusebox.Circuits.main="#heartroot#/main">
<cfset fusebox.Circuits.reports="#heartroot#/reports">
<cfset fusebox.Circuits.users="#heartroot#/users">


