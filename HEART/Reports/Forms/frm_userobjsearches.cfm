<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="65%" align="center">
	<tr><td colspan="2" class="bodyTextWhite">Saved Searches</td>
	</tr>

		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="4" border="0" bgcolor="ffffff" width="100%">
						<tr>
							<td class="formlable" align="center" width="50%"><strong>Observation Searches</strong></td>
						</tr>
						<tr>
							<cfif getobjsearches.recordcount gt 0>
							<td valign="top">
								<table cellpadding="4" cellspacing="0" border="0" width="100%">
								<cfoutput query="getobjsearches">
									<tr>
										<td class="bodytextgrey" width="95%"><a href="#self#?fuseaction=#attributes.xfa.observationsearch#&uincsearch=#UserSearchID#">#SearchName#</a></td>
										<td class="bodytextgrey"><a href="#self#?fuseaction=#attributes.xfa.deleteincsearch#&prjsearch=#UserSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');"><img src="images/trash.gif" border="0"></a></td>
										
									</tr>
								</cfoutput>
								</table>			
							</td>
							<cfelse>
							<td class="bodytextgrey" align="center">You have no saved searches</td>
							</cfif>
						</tr>
				</table>
			</td>
		</tr>
	</table>