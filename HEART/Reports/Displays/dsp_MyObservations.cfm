
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
	
	<tr >
	<td class="bodyTextWhite"><strong>My Observations</strong></td>
	<td align="right"></td>
	</tr>

		<tr>
			<td align="center" bgcolor="ffffff" colspan="2">

				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
							<td class="formlable" ><strong>Observation Number</strong></td>
							<cfoutput>
							<td class="formlable" ><strong>#request.bulabellong#</strong></td>
							<td class="formlable" ><strong>#request.oulabellong#</strong></td>
							</cfoutput>
							<td class="formlable" ><strong>Project/Office</strong></td>
							<td class="formlable" ><strong>Event Type</strong></td>
							<td class="formlable" ><strong>Observation Date</strong></td>							
							<td class="formlable" ><strong>Status</strong></td>
						</tr>
				<cfif  GetMyObservations.RecordCount GT 0>	
						<cfoutput query="GetMyObservations">
					
						
						<tr <cfif currentrow mod 2 is 0>class="formlable"</cfif>>
							<td class="bodytextgrey">
								<a href="#self#?fuseaction=main.exportobservation&OBNum=#ObservationNumber#" target="_blank"><img src="images/pdfsm.png" border="0" style="padding-right:4px;vertical-align:-10"></a>
								<cfif status eq "Submitted">
									<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
										<a href="index.cfm?fuseaction=main.ReviewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
									<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
										<cfif listfind(request.userous,ouid) gt 0>
											<a href="index.cfm?fuseaction=main.ReviewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
										<cfelse>
											<a href="index.cfm?fuseaction=main.ViewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
										</cfif>
									<cfelse>
										<a href="index.cfm?fuseaction=main.ViewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
									</cfif>
								<cfelseif status eq "Reviewed">
									<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
										<a href="index.cfm?fuseaction=main.ActionObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
									<cfelse>
										<a href="index.cfm?fuseaction=main.ViewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
									</cfif>
								<cfelseif status eq "cancelled" and EscalateOneAim eq 1 and trim(withdrawnby) neq ''>
									<a href="index.cfm?fuseaction=main.ReviewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum, "00000")#</a>
								<cfelse>
									<a href="index.cfm?fuseaction=main.ViewObservation&OBNum=#ObservationNumber#">#TrackingYear##numberformat(TrackingNum,"00000")#</a>
								</cfif>
								
								
																	
								</td>
							<td class="bodytextgrey">#ObservationBU#</td>
							<td class="bodytextgrey">#ObservationOU#</td>
							<td class="bodytextgrey">#Group_Name#</td>
							<td class="bodytextgrey">#ObservationType#</td>
							<td class="bodytextgrey">#dateformat(DateofObservation,sysdateformat)#</td>
							<td class="bodytextgrey"><cfif status eq "Submitted">Awaiting Review<cfelseif status eq "Reviewed">Awaiting Action<cfelseif status eq "Cancelled"><cfif EscalateOneAim eq 1>Escalated to oneAIM<cfelse>Cancelled</cfif><cfelse>#status#</cfif></td>
						</tr>
						</cfoutput> 
				<cfelse>
					<tr><td class="bodytextgrey" colspan="7">You have no safety observations</td></tr>		
				</cfif>		
				</table>
			</td>
		</tr>
	</table> 
	
	