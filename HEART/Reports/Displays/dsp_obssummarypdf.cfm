<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "displays\dsp_obssummarypdf.cfm", "exports"))>
<cfset thefilename = "ObservationSearch_#datetimeformat(now(),'HHnnssl')#.xlsx">

<cfset fnamepdf = "ObservationSummary#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="portrait" marginright="0.25" marginleft="0.25" margintop="0.5" marginbottom="0.5"> 

<cfoutput>
<cfparam name="openou" default="0">
<cfparam name="openbu" default="0">
<cfparam name="dosearch" default="">

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Observation Summary</td>
</tr>

</table>

</cfoutput>
<table cellpadding="3" cellspacing="0"  width="100%" border="0">
	<tr>
		
		<td class="purplebg" width="15%" bgcolor="5f2167"><strong class="BodyTextWhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">BU</strong></td>
		<td class="purplebg"  bgcolor="5f2167"><strong class="BodyTextWhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Stakeholders</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2167"><strong class="BodyTextWhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Unsafe Act</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2167"><strong class="BodyTextWhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Unsafe Condition</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2167"><strong class="BodyTextWhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Safe Behaviour</strong></td>
		
	</tr>
	<cfoutput>
	<cfloop query="getbus" group="ID">
	<cfset butotUA = 0>
	<cfset butotUB = 0>
	<cfset butotSB = 0>
	
		<cfset buctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset buctr = buctr+1>
		<tr>
		
			<td class="bodytextgrey" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif buctr eq 1>#getBUs.Name#</cfif></td>
		
			
			<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#i#</td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Act")><cfset butotUA = butotUA+etstruct["#ID#_#i#_Unsafe Act"]>#numberformat(etstruct["#ID#_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Condition")><cfset butotUB = butotUB+etstruct["#ID#_#i#_Unsafe Condition"]>#numberformat(etstruct["#ID#_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(etstruct,"#ID#_#i#_Safe Behaviour")><cfset butotSB = butotSB+etstruct["#ID#_#i#_Safe Behaviour"]>#numberformat(etstruct["#ID#_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		
		</cfloop>
		<tr>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Total:</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(butotUA)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(butotUB)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(butotSB)#</strong></td>
		</tr>
		<cfloop query="getOU">
		<cfif getou.qrybuid eq getbus.id and listfind(openbu,getou.qrybuid) gt 0>
			<cfset outotUA = 0>
		<cfset outotUB = 0>
		<cfset outotSB = 0>
		
		<cfset ouctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset ouctr = ouctr+1>
		<tr>
			
		<td class="bodytextgrey" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif ouctr eq 1>&nbsp;&nbsp;#getou.Name#</cfif></td>
	
			<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#i#</td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Act")><cfset outotUA = outotUA+etstruct["#ID#_#i#_Unsafe Act"]>#numberformat(etstruct["#ID#_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Condition")><cfset outotUB = outotUB+etstruct["#ID#_#i#_Unsafe Condition"]>#numberformat(etstruct["#ID#_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(etstruct,"#ID#_#i#_Safe Behaviour")><cfset outotSB = outotSB+etstruct["#ID#_#i#_Safe Behaviour"]>#numberformat(etstruct["#ID#_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		
		</cfloop>
		<tr>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Total:</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(outotUA)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(outotUB)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(outotSB)#</strong></td>
		</tr>
		<cfloop query="getgroups">
		<cfif getgroups.ouid eq getou.id and listfind(openou,getou.id) gt 0>
		<cfset gtotUA = 0>
		<cfset gtotUB = 0>
		<cfset gtotSB = 0>
		<cfset gctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset gctr = gctr+1>
		<tr>
			<td class="bodytextgrey" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif gctr eq 1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#group_name#<cfelse>&nbsp;</cfif></td>
			<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#i#</td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(prjetstruct,"#group_number#_#i#_Unsafe Act")><cfset gtotUA = gtotUA+prjetstruct["#group_number#_#i#_Unsafe Act"]>#numberformat(prjetstruct["#group_number#_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(prjetstruct,"#group_number#_#i#_Unsafe Condition")><cfset gtotUB = gtotUB +prjetstruct["#group_number#_#i#_Unsafe Condition"]>#numberformat(prjetstruct["#group_number#_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(prjetstruct,"#group_number#_#i#_Safe Behaviour")><cfset gtotSB = gtotSB +prjetstruct["#group_number#_#i#_Safe Behaviour"]>#numberformat(prjetstruct["#group_number#_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		
		</cfloop>
		<tr>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Total:</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(gtotUA)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(gtotUB)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(gtotSB)#</strong></td>
		</tr>
		</cfif>
		</cfloop>
	</cfif>
	</cfloop>
		<tr>
			<td colspan="5"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2167;" width="100%"></td>
		</tr>
	</cfloop>
	</cfoutput>
	
	<cfoutput>
	<cfset afwtotUA = 0>
	<cfset afwtotUB = 0>
	<cfset afwtotSB = 0>
	<cfset buctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset buctr = buctr+1>
		<tr>
			<td class="formlable" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfif buctr eq 1>Amec Foster Wheeler Global Total<cfelse>&nbsp;</cfif></td>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">#i#</td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfif structkeyexists(etstruct,"global_#i#_Unsafe Act")><cfset afwtotUA = afwtotUA+etstruct["global_#i#_Unsafe Act"]>#numberformat(etstruct["global_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfif structkeyexists(etstruct,"global_#i#_Unsafe Condition")><cfset afwtotUB = afwtotUB+etstruct["global_#i#_Unsafe Condition"]>#numberformat(etstruct["global_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfif structkeyexists(etstruct,"global_#i#_Safe Behaviour")><cfset afwtotSB = afwtotSB+etstruct["global_#i#_Safe Behaviour"]>#numberformat(etstruct["global_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		</cfloop>
		<tr>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
			<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Total:</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(afwtotUA)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(afwtotUB)#</strong></td>
			<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(afwtotSB)#</strong></td>
		</tr>
	</cfoutput>
	
</table>
 </cfdocument>
<cflocation url="/#getappconfig.heartPath#/reports/exports/#fnamepdf#" addtoken="No">

