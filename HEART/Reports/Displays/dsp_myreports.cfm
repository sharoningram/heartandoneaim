<!--- <cfset userrepstruct = {}>
<cfloop query="getuserreps">
	<cfset repname = SearchName>
	<cfif trim(SaveCtr) gt 1><cfset repname = repname & " (#SaveCtr#)"></cfif>
	<cfif not structkeyexists(userrepstruct,reportType)>
		<cfset userrepstruct[reportType] = repname>
	<cfelse>
		<cfset userrepstruct[reportType] = listappend(userrepstruct[reportType],repname,"~")>
	</cfif>
</cfloop> --->
<div align="center" class="midbox">
	<div align="center" class="midboxtitle">What report would you like to view?</div>
	<table cellpadding="8" cellspacing="0" border="0" align="center" width="100%">
		<cfif getuserreps.recordcount gt 0>
		<cfoutput query="getuserreps" group="reportType">
		<cfswitch expression="#reportType#">
		<cfcase value="obssummary">
			<tr>
			<td class="formlable" colspan="2">Observation Summary</td>
			</tr>
		</cfcase>
		<cfcase value="Diamond">
			<tr>
			<td class="formlable" colspan="2">Mining the Diamond</td>
			</tr>
		</cfcase>
		<cfcase value="safetyrules">
			<tr>
			<td class="formlable" colspan="2">BU Global Safety Rules</td>
			</tr>
		</cfcase>
		<cfcase value="ousafetyrules">
			<tr>
			<td class="formlable" colspan="2">OU Global Safety Rules</td>
			</tr>
		</cfcase>
		<cfcase value="prjsafetyrules">
			<tr>
			<td class="formlable" colspan="2">Project Global Safety Rules</td>
			</tr>
		</cfcase>
		<cfcase value="safetyessentials">
			<tr>
			<td class="formlable" colspan="2">BU Safety Essentials</td>
			</tr>
		</cfcase>
		<cfcase value="ousafetyessentials">
			<tr>
			<td class="formlable" colspan="2">OU Safety Essentials</td>
			</tr>
		</cfcase>
		<cfcase value="prjsafetyessentials">
			<tr>
			<td class="formlable" colspan="2">Project Safety Essentials</td>
			</tr>
		</cfcase>
		<cfcase value="eventtype">
			<tr>
			<td class="formlable" colspan="2">BU Event Type</td>
			</tr>
		</cfcase>
		<cfcase value="oueventtype">
			<tr>
			<td class="formlable" colspan="2">OU Event Type</td>
			</tr>
		</cfcase>
		<cfcase value="prjeventtype">
			<tr>
			<td class="formlable" colspan="2">Project Event Type</td>
			</tr>
		</cfcase>
		</cfswitch>
			<cfoutput>
			<tr>
			<td class="midboxtitletxt" width="95%"><a href="index.cfm?fuseaction=reports.directmyreport&rep=#UserReportID#" class="midboxtitletxt">#SearchName#</a></td>
			<td><a href="#self#?fuseaction=#attributes.xfa.deletemyreports#&prjsearch=#UserReportID#" title="Delete"><img src="images/trash.gif" border="0" alt="Delete"></a></td><!--- <td><a href="index.cfm?fuseaction=reports.directmyreport&rep=#UserReportID#"><img src="images/smbutton.png" border="0"></a></td> --->
			</tr>
		</cfoutput>
		
	</cfoutput>
	<cfelse>
		<tr>
			<td align="center" class="bodytextgrey" colspan="2">You have no saved reports</td>
		</tr>
		</cfif>
		
		
	
	</table>



</div>