<cfparam name="dosearch" default="">
<cfif  listfindnocase("safetyrules,printsafetyrules",fusebox.fuseaction) gt 0>


<cfset buildurl = "">


<cfif isdefined("form.fieldnames")>
<cfloop list="#form.fieldnames#" index="i">
	<cfif i neq "fieldnames">
		<cfif listfindnocase("bu,ou",i) eq 0>
			<cfset buildurl = listappend(buildurl,"#i#=#evaluate(i)#","&")>
		</cfif>
	</cfif>
</cfloop>
</cfif>

<cfset immcausestruct = {}>
<cfset itemctrstruct = {}>
<cfset catstruct = {}>

<cfloop query="getsafetyrules">
	<cfif not structkeyexists(immcausestruct,azcatid)>
		<cfset immcausestruct[azcatid] = actname>
	</cfif>
	<cfif not structkeyexists(itemctrstruct,"#buid#_#azcatid#")>
		<cfset itemctrstruct["#buid#_#azcatid#"] = 1>
	<cfelse>
		<cfset itemctrstruct["#buid#_#azcatid#"] = itemctrstruct["#buid#_#azcatid#"] + 1>
	</cfif>
	<cfif not structkeyexists(catstruct,buid)>
		<cfset catstruct[buid] = buname>
	</cfif>
</cfloop>



<cfset cntlist = 0>
<cfloop list="#structkeylist(itemctrstruct)#" index="i">
	<cfset cntlist = listappend(cntlist,itemctrstruct[i])>
</cfloop>

<cfset maxNum = ArrayMax(ListToArray(cntlist))>
 
 <cfset charttop = 10>
<cfif maxnum lt 10>
	<cfset charttop = maxnum + (10-maxnum)>
<cfelseif maxnum eq 10>
	<cfset charttop = maxnum+10>
<cfelse>
	<cfif maxnum mod 10 eq 0>
		<cfset charttop = maxnum+10>
	<cfelse>
		<cfset chknum = right(maxnum,1)>
		<cfloop from="1" to="10" index="i">
			<cfset thenumval = chknum+i>
			
			<cfif thenumval mod 10 eq 0>
				<cfset charttop = maxnum+i>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>
</cfif>



<cfif  listfindnocase("printsafetyrules",fusebox.fuseaction) eq 0>

<cfoutput>

<form action="#self#?fuseaction=#attributes.xfa.printsafetyrules#" name="printfrm" method="post" target="_blank">
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">
	</form>
	</cfoutput>

</cfif>



<cfset chartxml="<chart caption='#request.bulabellong# Global Safety Rules' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='1' canvasBorderColor='##dfdfdf' canvasBorderThickness='1' showPlotBorder='0' plotFillRatio='100'  legendBorderThickness='0' legendShadow='0'><categories>">

<cfloop list="#structkeylist(catstruct)#" index="i">
	<cfset chartxml= chartxml & "<category label='#catstruct[i]#' />"> 
</cfloop>
<cfset chartxml= chartxml & "</categories>">

<cfloop list="#structkeylist(immcausestruct)#" index="o">
	<cfset chartxml= chartxml & "<dataset seriesname='#immcausestruct[o]#'>">
	<cfloop list="#structkeylist(catstruct)#" index="g">
		<cfset thekey = "#g#_#o#">
		<cfif structkeyexists(itemctrstruct,"#g#_#o#")>
			<cfset chartxml= chartxml & "<set value='#itemctrstruct[thekey]#'  link='index.cfm?fuseaction=reports.ousafetyrules&bu=#g#&#buildurl#' />">
		<cfelse>
			<cfset chartxml= chartxml & "<set value='0'  link='index.cfm?fuseaction=reports.ousafetyrules&bu=#g#' />">
		</cfif>
	</cfloop>
<cfset chartxml= chartxml & "</dataset>">
</cfloop>


<cfset chartxml= chartxml & "</chart>"> 

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2">Global Safety Rules Chart</td>
</tr><cfoutput>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Observations</cfif></td>
	<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printsafetyrules"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
</tr></cfoutput>
</table>
<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Global Safety Rules Chart</td>
	</tr>
	<tr>
		<td bgcolor="ffffff" align="center">
<div id="chartdiv"></div>
		</td>
	</tr>
</table>

                    
	<cfoutput>
	
	<script type="text/javascript">
		FusionCharts.ready(function () {
		    var thisChart = new FusionCharts({
		        "type": "mscolumn2d",
		        "renderAt": "chartdiv",
		        "width": "60%",
		        "height": "450",
		        "dataFormat": "xml",
		        "dataSource":  "#chartxml#"
		    }
			);
		    thisChart.render();
		});
	</script>		
	</cfoutput>




<cfelseif  listfindnocase("ousafetyrules,printousafetyrules",fusebox.fuseaction) gt 0>


<cfset buildurl = "">


<cfif isdefined("form.fieldnames")>
<cfloop list="#form.fieldnames#" index="i">
	<cfif i neq "fieldnames">
		<cfif listfindnocase("bu,ou",i) eq 0>
			<cfset buildurl = listappend(buildurl,"#i#=#evaluate(i)#","&")>
		</cfif>
	</cfif>
</cfloop>
<cfelse>
	<cfset buildurl = cgi.query_string>
	<cfset buildurl = replacenocase(buildurl,"fuseaction=reports.ousafetyrules","","all")>
	<cfset buildurl = rereplacenocase(buildurl,"&bu=[0-9]*","","all")>
</cfif>

<cfset immcausestruct = {}>
<cfset itemctrstruct = {}>
<cfset catstruct = {}>

<cfloop query="getsafetyrules">
	<cfif not structkeyexists(immcausestruct,azcatid)>
		<cfset immcausestruct[azcatid] = actname>
	</cfif>
	<cfif not structkeyexists(itemctrstruct,"#ouid#_#azcatid#")>
		<cfset itemctrstruct["#ouid#_#azcatid#"] = 1>
	<cfelse>
		<cfset itemctrstruct["#ouid#_#azcatid#"] = itemctrstruct["#ouid#_#azcatid#"] + 1>
	</cfif>
	<cfif not structkeyexists(catstruct,ouid)>
		<cfset catstruct[ouid] = ouname>
	</cfif>
</cfloop>


<!--- 
<cfdump var="#catstruct#">
<cfdump var="#immcausestruct#">
<cfdump var="#itemctrstruct#"> --->

<cfset cntlist = 0>
<cfloop list="#structkeylist(itemctrstruct)#" index="i">
	<cfset cntlist = listappend(cntlist,itemctrstruct[i])>
</cfloop>

<cfset maxNum = ArrayMax(ListToArray(cntlist))>
 
 <cfset charttop = 10>
<cfif maxnum lt 10>
	<cfset charttop = maxnum + (10-maxnum)>
<cfelseif maxnum eq 10>
	<cfset charttop = maxnum+10>
<cfelse>
	<cfif maxnum mod 10 eq 0>
		<cfset charttop = maxnum+10>
	<cfelse>
		<cfset chknum = right(maxnum,1)>
		<cfloop from="1" to="10" index="i">
			<cfset thenumval = chknum+i>
			
			<cfif thenumval mod 10 eq 0>
				<cfset charttop = maxnum+i>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>
</cfif>



<cfif  listfindnocase("printousafetyrules",fusebox.fuseaction) eq 0>

<cfoutput>

<form action="#self#?fuseaction=#attributes.xfa.printousafetyrules#" name="printfrm" method="post" target="_blank">
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">	
	</form>
	</cfoutput>

</cfif>

<cfset dspBU = "">

<cfloop query="getbus">
	<cfif listfind(bu,id) gt 0><cfset dspBU = Name><cfbreak></cfif>
</cfloop>
<cfset dspBU = replace(dspBU,"<","&lt;","all")>
<cfset dspBU = replace(dspBU,">","&gt;","all")>
<cfset dspBU = replace(dspBU,"'","","all")>
<cfset chartxml="<chart caption='#dspBU# - #request.oulabellong# Global Safety Rules' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='1' canvasBorderColor='##dfdfdf' canvasBorderThickness='1' showPlotBorder='0' plotFillRatio='100' legendBorderThickness='0' legendShadow='0'><categories>">

<cfloop list="#structkeylist(catstruct)#" index="i">
	<cfset chartxml= chartxml & "<category label='#catstruct[i]#' />"> 
</cfloop>
<cfset chartxml= chartxml & "</categories>">

<cfloop list="#structkeylist(immcausestruct)#" index="o">
	<cfset chartxml= chartxml & "<dataset seriesname='#immcausestruct[o]#'>">
	<cfloop list="#structkeylist(catstruct)#" index="g">
	<cfset thekey = "#g#_#o#">
		<cfif structkeyexists(itemctrstruct,"#g#_#o#")>
			<cfset chartxml= chartxml & "<set value='#itemctrstruct[thekey]#'  link='index.cfm?fuseaction=reports.prjsafetyrules&ou=#g#&#buildurl#' />">
		<cfelse>
			<cfset chartxml= chartxml & "<set value='0'  link='index.cfm?fuseaction=reports.prjsafetyrules&ou=#g#&#buildurl#' />">
		</cfif>
		
	</cfloop>
<cfset chartxml= chartxml & "</dataset>">
</cfloop>


<cfset chartxml= chartxml & "</chart>"> 

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2">Global Safety Rules Chart</td>
</tr><cfoutput>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Observations</cfif></td>
	<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printousafetyrules"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
</tr></cfoutput>
</table>
<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Global Safety Rules Chart</td>
	</tr>
	<tr>
		<td bgcolor="ffffff" align="center">
<div id="chartdiv"></div>
		</td>
	</tr>
</table>

                    
	<cfoutput>
	
	<script type="text/javascript">
		FusionCharts.ready(function () {
		    var thisChart = new FusionCharts({
		        "type": "mscolumn2d",
		        "renderAt": "chartdiv",
		        "width": "60%",
		        "height": "450",
		        "dataFormat": "xml",
		        "dataSource":  "#chartxml#"
		    }
			);
		    thisChart.render();
		});
	</script>		
	</cfoutput>


<cfelseif  listfindnocase("prjsafetyrules,printprjsafetyrules",fusebox.fuseaction) gt 0>
<cfset dspOU = "">

<cfloop query="getOU">
	<cfif listfind(ou,id) gt 0><cfset dspOU = Name><cfbreak></cfif>
</cfloop>
<cfset dspOU = replace(dspOU,"<","&lt;","all")>
<cfset dspOU = replace(dspOU,">","&gt;","all")>
<cfset dspOU = replace(dspOU,"'","","all")>

<cfset immcausestruct = {}>
<cfset itemctrstruct = {}>
<cfset catstruct = {}>

<cfloop query="getsafetyrules">
	<cfif not structkeyexists(immcausestruct,azcatid)>
		<cfset immcausestruct[azcatid] = actname>
	</cfif>
	<cfif not structkeyexists(itemctrstruct,"#group_number#_#azcatid#")>
		<cfset itemctrstruct["#group_number#_#azcatid#"] = 1>
	<cfelse>
		<cfset itemctrstruct["#group_number#_#azcatid#"] = itemctrstruct["#group_number#_#azcatid#"] + 1>
	</cfif>
	<cfif not structkeyexists(catstruct,group_number)>
		<cfset catstruct[group_number] = group_name>
	</cfif>
</cfloop>


<!--- 
<cfdump var="#catstruct#">
<cfdump var="#immcausestruct#">
<cfdump var="#itemctrstruct#"> --->

<cfset cntlist = 0>
<cfloop list="#structkeylist(itemctrstruct)#" index="i">
	<cfset cntlist = listappend(cntlist,itemctrstruct[i])>
</cfloop>

<cfset maxNum = ArrayMax(ListToArray(cntlist))>
 
 <cfset charttop = 10>
<cfif maxnum lt 10>
	<cfset charttop = maxnum + (10-maxnum)>
<cfelseif maxnum eq 10>
	<cfset charttop = maxnum+10>
<cfelse>
	<cfif maxnum mod 10 eq 0>
		<cfset charttop = maxnum+10>
	<cfelse>
		<cfset chknum = right(maxnum,1)>
		<cfloop from="1" to="10" index="i">
			<cfset thenumval = chknum+i>
			
			<cfif thenumval mod 10 eq 0>
				<cfset charttop = maxnum+i>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>
</cfif>



<cfif  listfindnocase("printprjsafetyrules",fusebox.fuseaction) eq 0>

<cfoutput>

<form action="#self#?fuseaction=#attributes.xfa.printprjsafetyrules#" name="printfrm" method="post" target="_blank">
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">
	</form>
	</cfoutput>

</cfif>



<cfset chartxml="<chart caption='#dspOU# - Project Global Safety Rules' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='1' canvasBorderColor='##dfdfdf' canvasBorderThickness='1' showPlotBorder='0' plotFillRatio='100' legendBorderThickness='0' legendShadow='0' rotateLabels='0'><categories>">

<cfloop list="#structkeylist(catstruct)#" index="i">
	<cfset dspprj1 = replace(catstruct[i],"<","&lt;","all")>
	<cfset dspprj1 = replace(dspprj1,">","&gt;","all")>
	<cfset dspprj1 = replace(dspprj1,"'","","all")>
	<cfset chartxml= chartxml & "<category label='#dspprj1#' />"> 
</cfloop>
<cfset chartxml= chartxml & "</categories>">

<cfloop list="#structkeylist(immcausestruct)#" index="o">
	<cfset dspprj = replace(immcausestruct[o],"<","&lt;","all")>
	<cfset dspprj = replace(dspprj,">","&gt;","all")>
	<cfset dspprj = replace(dspprj,"'","","all")>
	<cfset chartxml= chartxml & "<dataset seriesname='#dspprj#'>">
	<cfloop list="#structkeylist(catstruct)#" index="g">
	<cfset thekey = "#g#_#o#">
		<cfif structkeyexists(itemctrstruct,"#g#_#o#")>
			<cfset chartxml= chartxml & "<set value='#itemctrstruct[thekey]#' />">
		<cfelse>
			<cfset chartxml= chartxml & "<set value='0'  />">
		</cfif>
		
	</cfloop>
<cfset chartxml= chartxml & "</dataset>">
</cfloop>


<cfset chartxml= chartxml & "</chart>"> 

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2">Global Safety Rules Chart</td>
</tr><cfoutput>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Observations</cfif></td>
	<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printprjsafetyrules"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
</tr></cfoutput>
</table>
<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Global Safety Rules Chart</td>
	</tr>
	<tr>
		<td bgcolor="ffffff" align="center">
<div id="chartdiv"></div>
		</td>
	</tr>
</table>

                    
	<cfoutput>
	
	<script type="text/javascript">
		FusionCharts.ready(function () {
		    var thisChart = new FusionCharts({
		        "type": "mscolumn2d",
		        "renderAt": "chartdiv",
		        "width": "60%",
		        "height": "450",
		        "dataFormat": "xml",
		        "dataSource":  "#chartxml#"
		    }
			);
		    thisChart.render();
		});
	</script>		
	</cfoutput>

</cfif>