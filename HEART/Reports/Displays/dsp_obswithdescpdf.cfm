<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "displays\dsp_obswithdescpdf.cfm", "exports"))>
<cfset thefilename = "Observationswithdescription_#datetimeformat(now(),'HHnnssl')#.xlsx">

<cfset fnamepdf = "Observationswithdescription#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="portrait" marginright="0.25" marginleft="0.25" margintop="0.5" marginbottom="0.5"> 


<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Observations with Description</td>
</tr>

</table>

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" bgcolor="5f2167">
	<tr>
		<td class="bodyTextWhite">&nbsp;Observations</td>
	</tr>
	<tr>
		<td bgcolor="ffffff">
<table cellpadding="2" cellspacing="1" border="0" class="purplebg"width="100%" bgcolor="5f2167">
	<tr>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="77"><strong>Incident Number</strong></td>
		<cfoutput>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="52"><strong>#request.bulabellong#</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="83"><strong>#request.oulabellong#</strong></td>
		</cfoutput>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="105"><strong>Project/Office</strong></td>
<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Site/Office Name</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"  ><strong>Event Type</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Stakeholder</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Observation Date</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="9%"><strong>Observer's Name</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="7%"><strong>Global Safety Rule</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" width="7%"><strong>Safety Essential</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"  width="11%"><strong>Details of Observation</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"  width="11%"><strong>Immediate action taken/recommended</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"  width="6%"><strong>Status</strong></td>
		
	</tr>
	<cfoutput query="getobswithdesc">
	<tr <cfif currentrow mod 2 is 0>class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#trackingnum#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#buname#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#ouname#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#Group_Name#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#Sitename#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#ObservationType#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#PersonnelCategory#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#dateformat(DateofObservation,sysdateformat)#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#ObserverName#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#SafetyRule#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#SafetyEssential#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#Observations#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#FollowUpActions#</td>
		<td class="bodytextgrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#status#</td>
	</tr>
	
	</cfoutput>
	
</table>
	</td></tr>
	<tr bgcolor="ffffff">
		<td colspan="14" class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Count of Observations: <cfoutput>#getobswithdesc.recordcount#</cfoutput></td>
	</tr>
	</table>

</cfdocument>
<cflocation url="/#getappconfig.heartPath#/reports/exports/#fnamepdf#" addtoken="No">