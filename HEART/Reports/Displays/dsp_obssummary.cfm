<cfoutput>
<cfparam name="openou" default="0">
<cfparam name="openbu" default="0">
<cfparam name="dosearch" default="">
<cfif fusebox.fuseaction neq "printobssummary">
<form action="#self#?fuseaction=#attributes.xfa.printobssummary#" name="printfrm" method="post" target="_blank">
			
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">
	<input type="hidden" name="openou" value="#openou#">
	<input type="hidden" name="openbu" value="#openbu#">
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.obssummarypdf#" name="obssummarypdf" method="post" target="_blank">
		
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="openou" value="#openou#">
<input type="hidden" name="openbu" value="#openbu#">
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.exportobssummary#" name="exportfrm" method="post" target="_blank">
	
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="openou" value="#openou#">
<input type="hidden" name="openbu" value="#openbu#">
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.obssummary#" name="expandrows" method="post">
	
	<input type="hidden" name="bu" value="#bu#">
	<input type="hidden" name="ou" value="#ou#">
	<input type="hidden" name="businessstream" value="#businessstream#">
	<input type="hidden" name="projectoffice" value="#projectoffice#">
	<input type="hidden" name="site" value="#site#">
	<input type="hidden" name="eventtype" value="#eventtype#">
	<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
	<input type="hidden" name="OBSERVERNAME" value="#OBSERVERNAME#">
	<input type="hidden" name="STAKEHOLDER" value="#STAKEHOLDER#">
	<input type="hidden" name="SAFETYRULES" value="#SAFETYRULES#">
	<input type="hidden" name="SAFETYESSENTIALS" value="#SAFETYESSENTIALS#">
	<input type="hidden" name="STATUS" value="#STATUS#">
	<input type="hidden" name="incyear" value="#incyear#">
	<input type="hidden" name="startdate" value="#startdate#">
	<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="toOneAIM" value="#toOneAIM#">
	<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="openou" value="#openou#">
<input type="hidden" name="openbu" value="#openbu#">
	</form>
	</cfif>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Observation Summary</td>
</tr>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Observations<cfelse>For #incyear#; All Observations</cfif></td>
	<td align="right"><cfif fusebox.fuseaction neq "printobssummary"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td><td>&nbsp;&nbsp;</td><td><a href="javascript:void(0);" onclick="document.exportfrm.submit();"><img src="images/excel.png" border="0"  style="padding-right:8px;padding-top:0px;"></a></td><td><a href="javascript:void(0);" onclick="document.obssummarypdf.submit();"><img src="images/pdfsm.png" border="0"></a></td></tr></table></cfif></td>
</tr>
</table>

</cfoutput>
<table cellpadding="3" cellspacing="0"  width="100%" border="0">
	<tr>
		
		<td class="purplebg" width="15%"><strong class="BodyTextWhite">BU</strong></td>
		<td class="purplebg" ><strong class="BodyTextWhite">Stakeholders</strong></td>
		<td class="purplebg" align="center"><strong class="BodyTextWhite">Unsafe Act</strong></td>
		<td class="purplebg" align="center"><strong class="BodyTextWhite">Unsafe Condition</strong></td>
		<td class="purplebg" align="center"><strong class="BodyTextWhite">Safe Behaviour</strong></td>
		
	</tr>
	<cfoutput>
	<cfloop query="getbus" group="ID">
	<cfset butotUA = 0>
	<cfset butotUB = 0>
	<cfset butotSB = 0>
	<script type="text/javascript">
		function openou#getBUs.id#(){
		document.expandrows.openbu.value = document.expandrows.openbu.value + ',' + #getBUs.id#;
		document.expandrows.submit();
		}
		function closeou#getBUs.id#(){
			var glist = document.expandrows.openbu.value;
			var valueArray = glist.split(',');
			document.expandrows.openbu.value = 0;
			for(var i=0; i<valueArray.length; i++){
  				if((valueArray[i]!=0)&&(valueArray[i]!=#getBUs.id#)){
		//alert(valueArray[i]);
				document.expandrows.openbu.value = document.expandrows.openbu.value + ',' + valueArray[i];
		
				}
		
			}
			
			
			
			document.expandrows.submit();
			
		}
		</script>
	
		<cfset buctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset buctr = buctr+1>
		<tr>
		<cfif fusebox.fuseaction neq "printobssummary">
			<cfif buctr eq 1>
				<cfif listfind(openbu,getBUs.id) eq 0>
					<td class="bodytextgrey"  id="openbu#getBUs.id#" nowrap><a href="javascript:void(0);" onclick="openou#getBUs.id#();" style="text-decoration:none;">+</a>&nbsp;#getBUs.Name#</td>
				<cfelse>
					<td class="bodytextgrey"   id="closebu#getBUs.id#" nowrap><a href="javascript:void(0);" onclick="closeou#getBUs.id#();" style="text-decoration:none;">-</a>&nbsp;#getBUs.Name#</td>
				</cfif>
			<Cfelse>
				<td></td>
			</cfif>
		<cfelse>
			<td class="bodytextgrey" nowrap><cfif buctr eq 1>#getBUs.Name#</cfif></td>
		</cfif>
			
			<td class="bodytextgrey">#i#</td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Act")><cfset butotUA = butotUA+etstruct["#ID#_#i#_Unsafe Act"]>#numberformat(etstruct["#ID#_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Condition")><cfset butotUB = butotUB+etstruct["#ID#_#i#_Unsafe Condition"]>#numberformat(etstruct["#ID#_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(etstruct,"#ID#_#i#_Safe Behaviour")><cfset butotSB = butotSB+etstruct["#ID#_#i#_Safe Behaviour"]>#numberformat(etstruct["#ID#_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		
		</cfloop>
		<tr>
			<td class="formlable"></td>
			<td class="formlable"><strong>Total:</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(butotUA)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(butotUB)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(butotSB)#</strong></td>
		</tr>
		<cfloop query="getOU">
		<cfif getou.qrybuid eq getbus.id and listfind(openbu,getou.qrybuid) gt 0>
		<cfset outotUA = 0>
		<cfset outotUB = 0>
		<cfset outotSB = 0>
		<script type="text/javascript">
		function opengroups#getou.id#(){
		
		document.expandrows.openou.value = document.expandrows.openou.value + ',' + #getou.id#;
		document.expandrows.submit();
		}
		function closeou#getou.id#(){
		
			var glist = document.expandrows.openou.value;
			var valueArray = glist.split(',');
			document.expandrows.openou.value = 0;
			for(var i=0; i<valueArray.length; i++){
  				if((valueArray[i]!=0)&&(valueArray[i]!=#getou.id#)){
		
				document.expandrows.openou.value = document.expandrows.openou.value + ',' + valueArray[i];
		
				}
		
			}
			
			document.expandrows.submit(); 
			
		}
		</script>
		<cfset ouctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset ouctr = ouctr+1>
		<tr>
			<cfif fusebox.fuseaction neq "printobssummary">
		<cfif ouctr eq 1>
			<cfif listfind(openou,getou.id) eq 0>
				<td class="bodytextgrey"  id="opengroups#getou.id#" nowrap>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="opengroups#getou.id#();" style="text-decoration:none;">+</a>&nbsp;#getou.Name#</td>
			<cfelse>
				<td class="bodytextgrey"   id="closegroups#getou.id#" nowrap>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="closeou#getou.id#();" style="text-decoration:none;">-</a>&nbsp;#getou.Name#</td>
			</cfif>
		<Cfelse>
			<td></td>
		</cfif>
	<cfelse>
		<td class="bodytextgrey" nowrap><cfif ouctr eq 1>&nbsp;&nbsp;#getou.Name#</cfif></td>
	</cfif>
			<td class="bodytextgrey">#i#</td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Act")><cfset outotUA = outotUA+etstruct["#ID#_#i#_Unsafe Act"]>#numberformat(etstruct["#ID#_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Condition")><cfset outotUB = outotUB+etstruct["#ID#_#i#_Unsafe Condition"]>#numberformat(etstruct["#ID#_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(etstruct,"#ID#_#i#_Safe Behaviour")><cfset outotSB = outotSB+etstruct["#ID#_#i#_Safe Behaviour"]>#numberformat(etstruct["#ID#_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		
		</cfloop>
		<tr>
			<td class="formlable"></td>
			<td class="formlable"><strong>Total:</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(outotUA)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(outotUB)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(outotSB)#</strong></td>
		</tr>
		<cfloop query="getgroups">
		<cfif getgroups.ouid eq getou.id and listfind(openou,getou.id) gt 0>
		<cfset gtotUA = 0>
		<cfset gtotUB = 0>
		<cfset gtotSB = 0>
		<cfset gctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset gctr = gctr+1>
		<tr>
			<td class="bodytextgrey" nowrap><cfif gctr eq 1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#group_name#<cfelse>&nbsp;</cfif></td>
			<td class="bodytextgrey">#i#</td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(prjetstruct,"#group_number#_#i#_Unsafe Act")><cfset gtotUA = gtotUA+prjetstruct["#group_number#_#i#_Unsafe Act"]>#numberformat(prjetstruct["#group_number#_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(prjetstruct,"#group_number#_#i#_Unsafe Condition")><cfset gtotUB = gtotUB +prjetstruct["#group_number#_#i#_Unsafe Condition"]>#numberformat(prjetstruct["#group_number#_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="bodytextgrey" align="center"><cfif structkeyexists(prjetstruct,"#group_number#_#i#_Safe Behaviour")><cfset gtotSB = gtotSB +prjetstruct["#group_number#_#i#_Safe Behaviour"]>#numberformat(prjetstruct["#group_number#_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		
		</cfloop>
		<tr>
			<td class="formlable"></td>
			<td class="formlable"><strong>Total:</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(gtotUA)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(gtotUB)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(gtotSB)#</strong></td>
		</tr>
		</cfif>
		</cfloop>
	</cfif>
	</cfloop>
		<tr>
			<td colspan="5"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2167;" width="100%"></td>
		</tr>
	</cfloop>
	</cfoutput>
	
	<cfoutput>
	<cfset afwtotUA = 0>
	<cfset afwtotUB = 0>
	<cfset afwtotSB = 0>
	<cfset buctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset buctr = buctr+1>
		<tr>
			<td class="formlable" nowrap><cfif buctr eq 1>Amec Foster Wheeler Global Total<cfelse>&nbsp;</cfif></td>
			<td class="formlable">#i#</td>
			<td class="formlable" align="center"><cfif structkeyexists(etstruct,"global_#i#_Unsafe Act")><cfset afwtotUA = afwtotUA+etstruct["global_#i#_Unsafe Act"]>#numberformat(etstruct["global_#i#_Unsafe Act"])#<cfelse>0</cfif></td>
			<td class="formlable" align="center"><cfif structkeyexists(etstruct,"global_#i#_Unsafe Condition")><cfset afwtotUB = afwtotUB+etstruct["global_#i#_Unsafe Condition"]>#numberformat(etstruct["global_#i#_Unsafe Condition"])#<cfelse>0</cfif></td>
			<td class="formlable" align="center"><cfif structkeyexists(etstruct,"global_#i#_Safe Behaviour")><cfset afwtotSB = afwtotSB+etstruct["global_#i#_Safe Behaviour"]>#numberformat(etstruct["global_#i#_Safe Behaviour"])#<cfelse>0</cfif></td>
		</tr>
		</cfloop>
		<tr>
			<td class="formlable"></td>
			<td class="formlable"><strong>Total:</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(afwtotUA)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(afwtotUB)#</strong></td>
			<td class="formlable" align="center"><strong>#numberformat(afwtotSB)#</strong></td>
		</tr>
	</cfoutput>
	
</table>
<br>
