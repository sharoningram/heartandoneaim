
<div align="center" class="midbox">
	<div align="center" class="midboxtitle">What report would you like to view?</div>
	<table cellpadding="8" cellspacing="0" border="0" align="center" width="100%">
			
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.eventtype" class="midboxtitletxt">Event Type</a></td></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.safetyrules" class="midboxtitletxt">Global Safety Rules</a></td></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.obswithdesc" class="midboxtitletxt">Observations with Description</a></td></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.obssummary" class="midboxtitletxt">Observation Summary</a></td></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.safetyessentials" class="midboxtitletxt">Safety Essentials</a></td></td>
		</tr>
		<tr>
			<td class="formlable"><img src="images/spacer.gif" height="3"></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.diamond" class="midboxtitletxt">Mining the Diamond</a></td></td>
		</tr>
		<tr>
			<td class="formlable"><img src="images/spacer.gif" height="3"></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.myreports" class="midboxtitletxt">My Saved Reports</a></td>
	
		</tr>
		
		
	
	</table>



</div>