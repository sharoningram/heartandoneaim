<cfset cntlist = valuelist(getlsb.cnt)>
<cfset maxNum = ArrayMax(ListToArray(cntlist))>

<cfset charttop = 10>
<cfif maxnum lt 10>
	<cfset charttop = maxnum + (10-maxnum)>
<cfelseif maxnum eq 10>
	<cfset charttop = maxnum+10>
<cfelse>
	<cfif maxnum mod 10 eq 0>
		<cfset charttop = maxnum+10>
	<cfelse>
		<cfset chknum = right(maxnum,1)>
		<cfloop from="1" to="10" index="i">
			<cfset thenumval = chknum+i>
			
			<cfif thenumval mod 10 eq 0>
				<cfset charttop = maxnum+i>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>
</cfif>
<cfset chartlines = 4>

<table cellpadding="0" cellspacing="0"  width="50%">
	
	<tr>
		<td>		
			<cfif getlsb.recordcount gt 0>
			<cfswitch expression="#charttype#">
				<cfcase value="table">
<script type="text/javascript">
function explsbjs(exptype){

document.explsbfrm.diamondtype.value=exptype;
document.explsbfrm.submit();

}
</script>
<cfoutput>
<cfform action="#self#?fuseaction=#attributes.xfa.exportrundiamond#" method="post" name="explsbfrm" target="_blank">
<input type="hidden" name="diamondtype" value="">	
<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="charttype" value="#charttype#">
<input type="hidden" name="chartvalue" value="lsb">	
<input type="hidden" name="eventtype" value="#eventtype#">
<input type="hidden" name="WHEREHAPPEN" value="#WHEREHAPPEN#">
<input type="hidden" name="stakeholder" value="#stakeholder#">	
<input type="hidden" name="status" value="#status#">	

</cfform>		
</cfoutput>		
					<table cellpadding="4" cellspacing="1"  width="100%">
						<tr>
							<td align="right"><a href="javascript:void(0);" onclick="explsbjs('pdf');"><img src="images/pdfsm.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="explsbjs('xcl');"><img src="images/excel.png" border="0"></a></td>
						</tr>
					</table>
					<table cellpadding="4" cellspacing="1"  width="100%" class="purplebg">
						<tr>
							<td class="purplebg" colspan="2"  align="center"><strong class="BodyTextWhite">Global Safety Rules</strong></td>
						</tr>
						<cfoutput query="getlsb">
							<tr>
								<td bgcolor="ffffff" class="bodytext"  width="80%" >#lbl#</td>
								<Td bgcolor="ffffff" class="bodytext" align="center">#cnt#</td>
							</tr>
						</cfoutput>
					</table>
				</cfcase>
				<cfcase value="bar">
					<cfset chartxml="<chart caption='Global Safety Rules' subcaption='' xaxisname='' yaxisname='Number of Observations' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='#chartlines#' yAxisMinValue='0' yAxisMaxValue='#charttop#'>">
					<cfoutput query="getlsb">
						<cfset uselable = trim(lbl)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable#' value='#cnt#' />">
					</cfoutput>
					<cfset chartxml= chartxml & "</chart>">
					<div id="chartContainerLSB"></div>
					<cfoutput>
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "bar3d",
						        "renderAt": "chartContainerLSB",
						        "width": "750",
						        "height": "500",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>
				</cfcase>
				<cfcase value="column">
					<cfset chartxml="<chart caption='Global Safety Rules' subcaption='' xaxisname='' yaxisname='Number of Observations' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='#chartlines#' yAxisMinValue='0' yAxisMaxValue='#charttop#'>">
					<cfoutput query="getlsb">
						<cfset uselable = trim(lbl)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable#' value='#cnt#' />">
					</cfoutput>
					<cfset chartxml= chartxml & "</chart>">
					<div id="chartContainerLSB"></div>
					<cfoutput>
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "column3d",
						        "renderAt": "chartContainerLSB",
						        "width": "750",
						        "height": "550",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>
				</cfcase>
				
				<cfcase value="Doughnut">
					<cfset chartxml="<chart caption='Global Safety Rules' subcaption='' numberprefix=''  bgcolor='ffffff' showborder='0' use3dlighting='0' showshadow='0' enablesmartlabels='1' startingangle='310' showlabels='0' showpercentvalues='0' showlegend='1' legendshadow='0' legendborderalpha='0' decimals='0' captionfontsize='14' subcaptionfontsize='14' subcaptionfontbold='0' tooltipcolor='ffffff' tooltipborderthickness='0' tooltipbgcolor='000000' tooltipbgalpha='80' tooltipborderradius='2' tooltippadding='5' usedataplotcolorforlabels='1'  exportenabled='1' exportAtClientSide='1'>">
					<cfoutput query="getlsb">
						<cfset uselable = trim(lbl)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable#' value='#cnt#' />">
					</cfoutput>
 					<cfset chartxml= chartxml & "</chart>">
					<div id="chartContainerLSB"></div>
					<cfoutput>
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "doughnut3d",
						        "renderAt": "chartContainerLSB",
						        "width": "750",
						        "height": "550",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>
				
				</cfcase>
			</cfswitch>
			<cfelse>
				<table cellpadding="4" cellspacing="1"  width="100%" class="purplebg">
					<tr>
						<td class="purplebg" colspan="2"  align="center"><strong class="BodyTextWhite">Global Safety Rules</strong></td>
					</tr>
					<tr>
						<td bgcolor="ffffff" class="bodytext" align="center" width="100%" >This is no data available to run this report</td>
					</tr>
				</table>
			</cfif>
		</td>
	</tr>
</table>