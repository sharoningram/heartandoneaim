<cfparam name="sortordby" default="obsnum">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="eventtype" default="all">
<cfparam name="WHEREHAPPEN" default="all">
<cfparam name="OBSERVERNAME" default="">
<cfparam name="STAKEHOLDER" default="all">
<cfparam name="SAFETYRULES" default="all">
<cfparam name="SAFETYESSENTIALS" default="all">
<cfparam name="STATUS" default="all">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">
<cfparam name="ObservationNum" default="">



<cfset statobsids = "">

<cfif listfindnocase(STATUS,"All") eq 0>
	<cfif listfindnocase(status,"Open") gt 0>
		
		

<cfquery name="getopenobs" datasource="#request.HEART_DS#">
SELECT        Observations.ObservationNumber
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.OUid = #oneaimSQLname#.dbo.NewDials.ID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE 1 = 1

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(eventtype,"All") eq 0>
	and Observations.ObservationType in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#eventtype#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(WHEREHAPPEN,"All") eq 0>
	and Observations.IsWorkHome in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#WHEREHAPPEN#" list="Yes">)
</cfif> --->
<cfif trim(OBSERVERNAME) neq ''>
	and Observations.ObserverName like <cfqueryparam cfsqltype="CF_SQL_varchar" value="%#ObserverName#%">
</cfif>
<cfif listfindnocase(STAKEHOLDER,"All") eq 0>
	and Observations.PersonnelCategory in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#STAKEHOLDER#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYRULES,"All") eq 0>
	and Observations.GlobalSafetyRules in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYRULES#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYESSENTIALS,"All") eq 0>
	and Observations.SafetyEssential in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYESSENTIALS#" list="Yes">)
</cfif>

		and Observations.Status in ('Submitted','Reviewed')
	
<cfif trim(ObservationNum) neq ''>
	and ((CAST(Observations.TrackingYear AS Varchar(4)) + RIGHT('00000' + CAST(Observations.TrackingNum AS VARCHAR(5)), 5)) IN (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#ObservationNum#" list="yes">))
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
	<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)

	<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	AND (Observations.siteid IN
                             (SELECT        LocationID
                               FROM            #oneaimSQLname#.dbo.LocationAdvisors
                               WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">))) 
	</cfif>
	</cfif>
<cfelse>
	and Observations.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#cookie.useremail#">
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="obsnum">
ORDER BY Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="bu">
ORDER BY buname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="ou">
ORDER BY ouname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="prj">
ORDER BY Groups.Group_Name, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="obsdate">
ORDER BY Observations.DateofObservation, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="eventtype">
ORDER BY Observations.ObservationType, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="gsr">
ORDER BY #oneaimSQLname#.dbo.SafetyRules.SafetyRule, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="se">
ORDER BY #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="status">
ORDER BY Observations.Status, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="originator">
ORDER BY Observations.CreatedBy, Observations.TrackingYear, Observations.TrackingNum
</cfcase>
</cfswitch>

</cfquery>
		
<cfset statobsids = listappend(statobsids,valuelist(getopenobs.ObservationNumber))>
		
		</cfif>
		
		
		
		
	<cfif listfindnocase(status,"Escalated to oneAIM") gt 0>
	
<cfquery name="getescobs" datasource="#request.HEART_DS#">
SELECT        Observations.ObservationNumber
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.OUid = #oneaimSQLname#.dbo.NewDials.ID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE 1 = 1

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(eventtype,"All") eq 0>
	and Observations.ObservationType in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#eventtype#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(WHEREHAPPEN,"All") eq 0>
	and Observations.IsWorkHome in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#WHEREHAPPEN#" list="Yes">)
</cfif> --->
<cfif trim(OBSERVERNAME) neq ''>
	and Observations.ObserverName like <cfqueryparam cfsqltype="CF_SQL_varchar" value="%#ObserverName#%">
</cfif>
<cfif listfindnocase(STAKEHOLDER,"All") eq 0>
	and Observations.PersonnelCategory in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#STAKEHOLDER#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYRULES,"All") eq 0>
	and Observations.GlobalSafetyRules in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYRULES#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYESSENTIALS,"All") eq 0>
	and Observations.SafetyEssential in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYESSENTIALS#" list="Yes">)
</cfif>

and Observations.Status = 'cancelled' and EscalateOneAim = 1
	
<cfif trim(ObservationNum) neq ''>
	and ((CAST(Observations.TrackingYear AS Varchar(4)) + RIGHT('00000' + CAST(Observations.TrackingNum AS VARCHAR(5)), 5)) IN (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#ObservationNum#" list="yes">))
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
	<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)

	<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	AND (Observations.siteid IN
                             (SELECT        LocationID
                               FROM            #oneaimSQLname#.dbo.LocationAdvisors
                               WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">))) 
	</cfif>
	</cfif>
<cfelse>
	and Observations.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#cookie.useremail#">
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="obsnum">
ORDER BY Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="bu">
ORDER BY buname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="ou">
ORDER BY ouname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="prj">
ORDER BY Groups.Group_Name, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="obsdate">
ORDER BY Observations.DateofObservation, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="eventtype">
ORDER BY Observations.ObservationType, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="gsr">
ORDER BY #oneaimSQLname#.dbo.SafetyRules.SafetyRule, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="se">
ORDER BY #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="status">
ORDER BY Observations.Status, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="originator">
ORDER BY Observations.CreatedBy, Observations.TrackingYear, Observations.TrackingNum
</cfcase>
</cfswitch>

</cfquery>
		
<cfset statobsids = listappend(statobsids,valuelist(getescobs.ObservationNumber))>
		</cfif>
	<cfif listfindnocase(status,"cancelled") gt 0>
	
<cfquery name="getcancelobs" datasource="#request.HEART_DS#">
SELECT        Observations.ObservationNumber
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.OUid = #oneaimSQLname#.dbo.NewDials.ID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE 1 = 1

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(eventtype,"All") eq 0>
	and Observations.ObservationType in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#eventtype#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(WHEREHAPPEN,"All") eq 0>
	and Observations.IsWorkHome in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#WHEREHAPPEN#" list="Yes">)
</cfif> --->
<cfif trim(OBSERVERNAME) neq ''>
	and Observations.ObserverName like <cfqueryparam cfsqltype="CF_SQL_varchar" value="%#ObserverName#%">
</cfif>
<cfif listfindnocase(STAKEHOLDER,"All") eq 0>
	and Observations.PersonnelCategory in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#STAKEHOLDER#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYRULES,"All") eq 0>
	and Observations.GlobalSafetyRules in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYRULES#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYESSENTIALS,"All") eq 0>
	and Observations.SafetyEssential in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYESSENTIALS#" list="Yes">)
</cfif>

and Observations.Status = 'cancelled' and EscalateOneAim = 0
	
<cfif trim(ObservationNum) neq ''>
	and ((CAST(Observations.TrackingYear AS Varchar(4)) + RIGHT('00000' + CAST(Observations.TrackingNum AS VARCHAR(5)), 5)) IN (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#ObservationNum#" list="yes">))
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
	<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)

	<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	AND (Observations.siteid IN
                             (SELECT        LocationID
                               FROM            #oneaimSQLname#.dbo.LocationAdvisors
                               WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">))) 
	</cfif>
	</cfif>
<cfelse>
	and Observations.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#cookie.useremail#">
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="obsnum">
ORDER BY Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="bu">
ORDER BY buname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="ou">
ORDER BY ouname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="prj">
ORDER BY Groups.Group_Name, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="obsdate">
ORDER BY Observations.DateofObservation, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="eventtype">
ORDER BY Observations.ObservationType, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="gsr">
ORDER BY #oneaimSQLname#.dbo.SafetyRules.SafetyRule, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="se">
ORDER BY #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="status">
ORDER BY Observations.Status, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="originator">
ORDER BY Observations.CreatedBy, Observations.TrackingYear, Observations.TrackingNum
</cfcase>
</cfswitch>

</cfquery>
		
<cfset statobsids = listappend(statobsids,valuelist(getcancelobs.ObservationNumber))>
</cfif>
<cfif listfindnocase(status,"closed") gt 0>
		
		
<cfquery name="getcloseobs" datasource="#request.HEART_DS#">
SELECT        Observations.ObservationNumber
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.OUid = #oneaimSQLname#.dbo.NewDials.ID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE 1 = 1

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(eventtype,"All") eq 0>
	and Observations.ObservationType in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#eventtype#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(WHEREHAPPEN,"All") eq 0>
	and Observations.IsWorkHome in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#WHEREHAPPEN#" list="Yes">)
</cfif> --->
<cfif trim(OBSERVERNAME) neq ''>
	and Observations.ObserverName like <cfqueryparam cfsqltype="CF_SQL_varchar" value="%#ObserverName#%">
</cfif>
<cfif listfindnocase(STAKEHOLDER,"All") eq 0>
	and Observations.PersonnelCategory in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#STAKEHOLDER#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYRULES,"All") eq 0>
	and Observations.GlobalSafetyRules in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYRULES#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYESSENTIALS,"All") eq 0>
	and Observations.SafetyEssential in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYESSENTIALS#" list="Yes">)
</cfif>

and Observations.Status = 'closed'
	
<cfif trim(ObservationNum) neq ''>
	and ((CAST(Observations.TrackingYear AS Varchar(4)) + RIGHT('00000' + CAST(Observations.TrackingNum AS VARCHAR(5)), 5)) IN (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#ObservationNum#" list="yes">))
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
	<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)

	<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	AND (Observations.siteid IN
                             (SELECT        LocationID
                               FROM            #oneaimSQLname#.dbo.LocationAdvisors
                               WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">))) 
	</cfif>
	</cfif>
<cfelse>
	and Observations.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#cookie.useremail#">
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="obsnum">
ORDER BY Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="bu">
ORDER BY buname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="ou">
ORDER BY ouname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="prj">
ORDER BY Groups.Group_Name, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="obsdate">
ORDER BY Observations.DateofObservation, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="eventtype">
ORDER BY Observations.ObservationType, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="gsr">
ORDER BY #oneaimSQLname#.dbo.SafetyRules.SafetyRule, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="se">
ORDER BY #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="status">
ORDER BY Observations.Status, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="originator">
ORDER BY Observations.CreatedBy, Observations.TrackingYear, Observations.TrackingNum
</cfcase>
</cfswitch>

</cfquery>
		
<cfset statobsids = listappend(statobsids,valuelist(getcloseobs.ObservationNumber))>
	</cfif>
</cfif>



<cfset tblUID = replace(createUUID(),"-","","all")>
<cfif trim(statobsids) neq ''>
	<cfquery name="newtbl" datasource="#request.HEART_DS#"> 
		 Create Table ##incsrchTemp#tblUID# (obid int null) 
	</cfquery>
	<cfloop list="#statobsids#" index="i">
		<cfquery name="addtotemp" datasource="#request.HEART_DS#">
			insert into ##incsrchTemp#tblUID# 
                values (#i# )
		</cfquery>
	</cfloop>
	
</cfif>





<cfquery name="getobs" datasource="#request.HEART_DS#">
SELECT        Observations.ObservationNumber, Observations.TrackingYear, Observations.TrackingNum, Observations.ProjectNumber, Observations.SiteID, 
                         #oneaimSQLname#.dbo.Groups.Group_Name,#oneaimSQLname#.dbo.NewDials.ID AS ouid,  #oneaimSQLname#.dbo.NewDials.Name AS ouname, Observations.DateofObservation, Observations.ObservationType, 
                         Observations.PersonnelCategory, Observations.Status, NewDials_1.Name AS buname, #oneaimSQLname#.dbo.SafetyRules.SafetyRule, 
                         #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, Observations.CreatedbyEmail,Observations.CreatedBy, Observations.EscalateOneAim, Observations.ObservationBU, 
                         Observations.ObservationOU, Observations.withdrawnto, Observations.withdrawnby
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.OUid = #oneaimSQLname#.dbo.NewDials.ID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE 1 = 1

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(eventtype,"All") eq 0>
	and Observations.ObservationType in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#eventtype#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(WHEREHAPPEN,"All") eq 0>
	and Observations.IsWorkHome in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#WHEREHAPPEN#" list="Yes">)
</cfif> --->
<cfif trim(OBSERVERNAME) neq ''>
	and Observations.ObserverName like <cfqueryparam cfsqltype="CF_SQL_varchar" value="%#ObserverName#%">
</cfif>
<cfif listfindnocase(STAKEHOLDER,"All") eq 0>
	and Observations.PersonnelCategory in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#STAKEHOLDER#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYRULES,"All") eq 0>
	and Observations.GlobalSafetyRules in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYRULES#" list="Yes">)
</cfif>
<cfif listfindnocase(SAFETYESSENTIALS,"All") eq 0>
	and Observations.SafetyEssential in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#SAFETYESSENTIALS#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(STATUS,"All") eq 0 and trim(statobsids) neq ''>
	and Observations.ObservationNumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#statobsids#" list="Yes">)
</cfif> --->
<cfif listfindnocase(STATUS,"All") eq 0>
	<cfif trim(statobsids) neq ''>
		and Observations.ObservationNumber in (select obid from ##incsrchTemp#tblUID#)
	<cfelse>
	and Observations.ObservationNumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="Yes">)
	</cfif>
</cfif> 


<cfif trim(ObservationNum) neq ''>
	and ((CAST(Observations.TrackingYear AS Varchar(4)) + RIGHT('00000' + CAST(Observations.TrackingNum AS VARCHAR(5)), 5)) IN (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#ObservationNum#" list="yes">))
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
	<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)

	<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0>
		and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	
	<cfelseif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	AND (Observations.siteid IN
                             (SELECT        LocationID
                               FROM            #oneaimSQLname#.dbo.LocationAdvisors
                               WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">))) 
	</cfif>
	</cfif>
<cfelse>
	and Observations.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#cookie.useremail#">
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="obsnum">
ORDER BY Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="bu">
ORDER BY buname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="ou">
ORDER BY ouname, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="prj">
ORDER BY Groups.Group_Name, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="obsdate">
ORDER BY Observations.DateofObservation, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="eventtype">
ORDER BY Observations.ObservationType, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="gsr">
ORDER BY #oneaimSQLname#.dbo.SafetyRules.SafetyRule, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="se">
ORDER BY #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="status">
ORDER BY Observations.Status, Observations.TrackingYear, Observations.TrackingNum
</cfcase>

<cfcase value="originator">
ORDER BY Observations.CreatedBy, Observations.TrackingYear, Observations.TrackingNum
</cfcase>
</cfswitch>

</cfquery>

<cfset withdlist = "">
<cfloop query="getobs">
	<cfif listfind(withdlist,withdrawnby) eq 0>
		<cfset withdlist = listappend(withdlist,withdrawnby)>
	</cfif>
</cfloop>
<cfquery name="getwithdrawn" datasource="#request.dsn#">
select firstname + ' ' + lastname as wname, useremail
from users
where useremail in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#withdlist#" list="yes">)
</cfquery>
<cfset withdstruct = {}>
<cfloop query="getwithdrawn">
	<cfif not structkeyexists(withdstruct,useremail)>
		<cfset withdstruct[useremail] = wname>
	</cfif>
</cfloop>


<cfset siteidlist = "">
<cfloop query="getobs">
	<cfif listfind(siteidlist,siteid) eq 0>
		<cfset siteidlist = listappend(siteidlist,siteid)>
	</cfif>
</cfloop>

<cfquery name="getadv" datasource="#request.dsn#">
SELECT        LocationAdvisors.LocationID, Users.Firstname + ' ' + Users.Lastname AS advname
FROM            LocationAdvisors INNER JOIN
                         Users ON LocationAdvisors.UserID = Users.UserId
WHERE        (Users.Status = 1) and LocationAdvisors.LocationID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#siteidlist#" list="Yes">)
</cfquery>
<cfset siteadvstruct = {}>
<cfloop query="getadv">
	<cfif not structkeyexists(siteadvstruct,LocationID)>
		<cfset siteadvstruct[LocationID] = advname>
	<cfelse>
		<cfset siteadvstruct[LocationID] = listappend(siteadvstruct[LocationID],advname)>
	</cfif>
</cfloop>

<cfset ouidlist = "">
<cfloop query="getobs">
	<cfif listfind(ouidlist,ouid) eq 0>
		<cfset ouidlist = listappend(ouidlist,ouid)>
	</cfif>
</cfloop>

<cfquery name="getouadm" datasource="#request.dsn#">
SELECT        UserRoles.AssignedLocs, UserRoles.DateUpdated, Users.Firstname + ' ' + Users.Lastname AS ouaname
FROM            UserRoles INNER JOIN
                         Users ON UserRoles.UserID = Users.UserId
WHERE        (UserRoles.UserRole = 'ou admin') AND (UserRoles.AssignedLocs IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ouidlist#" list="Yes">)) AND (Users.Status = 1)
</cfquery>
<cfset ouastruct = {}>
<cfloop query="getouadm">
	<cfif not structkeyexists(ouastruct,AssignedLocs)>
		<cfset ouastruct[AssignedLocs] = ouaname>
	<cfelse>
		<cfset ouastruct[AssignedLocs] = listappend(ouastruct[AssignedLocs],ouaname)>
	</cfif>
</cfloop>


<cfset gettheincs = QueryNew("ObservationNumber, TrackingYear, TrackingNum, ProjectNumber,SiteID, Group_Name, ouid,  ouname, DateofObservation, ObservationType, PersonnelCategory, Status, buname,SafetyRule,SafetyEssential, CreatedbyEmail,CreatedBy, EscalateOneAim, ObservationBU, ObservationOU, withdrawnto, withdrawnby, pendingactby, dspstat")>

<cfloop query="getobs">
			<cfset QueryAddRow( gettheincs ) />
			<cfset gettheincs["ObservationNumber"][currentrow] = ObservationNumber />
			<cfset gettheincs["TrackingYear"][currentrow] = TrackingYear />
			<cfset gettheincs["TrackingNum"][currentrow] = TrackingNum />
			<cfset gettheincs["ProjectNumber"][currentrow] = ProjectNumber />
			<cfset gettheincs["SiteID"][currentrow] = SiteID />
			<cfset gettheincs["Group_Name"][currentrow] = Group_Name />
			<cfset gettheincs["ouid"][currentrow] = ouid />
			<cfset gettheincs["ouname"][currentrow] = ouname />
			<cfset gettheincs["DateofObservation"][currentrow] = DateofObservation />
			<cfset gettheincs["ObservationType"][currentrow] = ObservationType />
			<cfset gettheincs["PersonnelCategory"][currentrow] = PersonnelCategory />
			<cfset thestat = status>
			<cfif status eq "Submitted"><cfset thestat = "Awaiting Review"><cfelseif status eq "Reviewed"><cfif withdrawnto eq 1><cfset thestat = "Withdrawn for Amendment"><cfelse><cfset thestat = "Awaiting Action"></cfif><cfelseif status eq "Cancelled"><cfif EscalateOneAim eq 1><cfif withdrawnto eq 1><cfset thestat = "Withdrawn for Amendment"><cfelse><cfset thestat = "Escalated to oneAIM"></cfif><cfelse><cfset thestat = "Cancelled"></cfif><cfelse><cfset thestat = status></cfif>
			
			
			<cfset gettheincs["Status"][currentrow] = status />
			<cfset gettheincs["buname"][currentrow] = buname />
			<cfset gettheincs["SafetyRule"][currentrow] = SafetyRule />
			<cfset gettheincs["SafetyEssential"][currentrow] = SafetyEssential />
			<cfset gettheincs["CreatedbyEmail"][currentrow] = CreatedbyEmail />
			<cfset gettheincs["CreatedBy"][currentrow] = CreatedBy />
			<cfset gettheincs["EscalateOneAim"][currentrow] = EscalateOneAim />
			<cfset gettheincs["ObservationBU"][currentrow] = ObservationBU />
			<cfset gettheincs["ObservationOU"][currentrow] = ObservationOU />
			<cfset gettheincs["withdrawnto"][currentrow] = withdrawnto />
			<cfset gettheincs["withdrawnby"][currentrow] = withdrawnby />
			<cfset thependact = "">
				<cfif status eq "Submitted">
									<cfif structkeyexists(siteadvstruct,siteid)>
										<cfset thependact = replace(siteadvstruct[siteid],",",", ","all")>
									<cfelse>
										<cfif structkeyexists(ouastruct,ouid)>
											<cfset thependact = replace(ouastruct[ouid],",",", ","all")>
										<cfelse>
											<cfset thependact = "NA">
										</cfif>
									</cfif>
								<cfelseif status eq "Reviewed">
									<cfif withdrawnto eq 1>
										<cfif structkeyexists(withdstruct,withdrawnby)>
											<cfset thependact = replace(withdstruct[withdrawnby],",",", ","all")>
										<cfelse>
											<cfset thependact = "NA">
										</cfif>
									<cfelse>
										<cfif structkeyexists(ouastruct,ouid)>
											<cfset thependact = replace(ouastruct[ouid],",",", ","all")>
										<cfelse>
											<cfset thependact = "NA">
										</cfif>
									</cfif>
								<cfelseif status eq "Cancelled">
									<cfif EscalateOneAim eq 1>
										<cfif withdrawnto eq 1>
											<cfif structkeyexists(withdstruct,withdrawnby)>
											<cfset thependact = replace(withdstruct[withdrawnby],",",", ","all")>
										<cfelse>
											<cfset thependact = "NA">
										</cfif>
										
											
										</cfif>
									
									
									</cfif>
									
									
									</cfif>
			<cfset gettheincs["pendingactby"][currentrow] = thependact />
			<cfset gettheincs["dspstat"][currentrow] = thestat />
		</cfloop>
<cfquery name="getincidents" dbtype="query">
select * from gettheincs

<cfswitch expression="#sortordby#">
<cfcase value="obsnum">
ORDER BY TrackingYear, TrackingNum
</cfcase>

<cfcase value="bu">
ORDER BY buname, TrackingYear, TrackingNum
</cfcase>

<cfcase value="ou">
ORDER BY ouname,TrackingYear, TrackingNum
</cfcase>

<cfcase value="prj">
ORDER BY Group_Name, TrackingYear, TrackingNum
</cfcase>

<cfcase value="obsdate">
ORDER BY DateofObservation, TrackingYear, TrackingNum
</cfcase>

<cfcase value="eventtype">
ORDER BY ObservationType, TrackingYear, TrackingNum
</cfcase>

<cfcase value="gsr">
ORDER BY SafetyRule, TrackingYear, TrackingNum
</cfcase>

<cfcase value="se">
ORDER BY SafetyEssential, TrackingYear, TrackingNum
</cfcase>

<cfcase value="status">
ORDER BY Status, TrackingYear, TrackingNum
</cfcase>

<cfcase value="originator">
ORDER BY CreatedBy, TrackingYear, TrackingNum
</cfcase>

<cfcase value="pendact">
ORDER BY pendingactby, TrackingYear, TrackingNum
</cfcase>
</cfswitch>
</cfquery>