<cfquery name="getobservationyears" datasource="#request.HEART_DS#">
SELECT DISTINCT YEAR(DateofObservation) AS incyear
FROM            Observations
WHERE        (DateofObservation IS NOT NULL)
ORDER BY incyear
</cfquery>