<cfparam name="qrybus" default="#valuelist(getBUs.id)#">
<cfif isdefined("bu")>
<cfif listfindnocase(bu,"All") eq 0>
	<cfset qrybus = bu>
</cfif>
</cfif>

<cfquery name="getOU" datasource="#request.dsn#">
		SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS buname, NewDials_1.ID AS qrybuid
					FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
		WHERE        (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qrybus#" list="Yes">)) AND (NewDials.status <> 99) and NewDials.isgroupdial = 0
		<cfif listfindnocase("observationsearch,doobservationsearch,obswithdesc,diamond",fusebox.fuseaction) gt 0>
		<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0>
			<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
				and NewDials.id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
			</cfif>
		</cfif>
		<cfif  listfindnocase(request.userlevel,"User") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and  listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"HSSE Advisor") eq 0>
		and NewDials.id in (SELECT DISTINCT Groups.ouid
		FROM            GroupUserAssignment INNER JOIN
		                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
		WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
		</cfif>
		</cfif>
		</cfif>
		ORDER BY buname,NewDials.Name
	</cfquery>