<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="eventtype" default="all">
<cfparam name="WHEREHAPPEN" default="all">
<cfparam name="OBSERVERNAME" default="">
<cfparam name="STAKEHOLDER" default="all">
<cfparam name="SAFETYRULES" default="all">
<cfparam name="SAFETYESSENTIALS" default="all">
<cfparam name="STATUS" default="all">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">
<cfparam name="ObservationNum" default="">
<cfparam name="searchname" default="">
<cfparam name="toOneAIM" default="">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">

<cfset statlist = "">

<cfif listfindnocase(STATUS,"All") eq 0>
	<cfloop list="#STATUS#" index="i">
		<cfif i eq "oneaim">
			<cfset statlist = listappend(statlist,"Cancelled1")>
		<cfelse>
			<cfset  statlist = listappend(statlist,"#i#0")>
		</cfif>
	</cfloop>
</cfif> 



<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfset showouonly = "no">
</cfif>

<cfquery name="geteventtype" datasource="#request.heart_ds#">
SELECT        Observations.ObservationNumber AS irn, #oneaimSQLname#.dbo.Groups.Group_Name, #oneaimSQLname#.dbo.Groups.Group_Number, #oneaimSQLname#.dbo.NewDials.ID AS buid, 
                         #oneaimSQLname#.dbo.NewDials.Name AS buname, NewDials_1.ID AS ouid, NewDials_1.Name AS ouname, Observations.ObservationType
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID AND 
                         #oneaimSQLname#.dbo.Groups.Group_Number = #oneaimSQLname#.dbo.GroupLocations.GroupNumber INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = #oneaimSQLname#.dbo.NewDials.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.OUid = NewDials_1.ID
WHERE   1 = 1
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND Observations.DateofObservation BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">
and (Observations.Status <> 'Cancelled')
<cfif trim(bu) neq "All">
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif trim(ou) neq "All">
	and #oneaimSQLname#.dbo.Groups.OUid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfelse>

<cfif trim(bu) neq "All">
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif trim(ou) neq "All">
	and #oneaimSQLname#.dbo.Groups.OUid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif wherehappen neq "all">
	and Observations.isworkhome = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wherehappen#">
</cfif>
<cfif listfindnocase(stakeholder,"All") eq 0>
	and Observations.PersonnelCategory in  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#stakeholder#" list="Yes">)
</cfif>

<cfif listfindnocase(STATUS,"All") eq 0>
	and Observations.status+CONVERT(varchar, Observations.EscalateOneAim) in  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#statlist#" list="yes">)
</cfif>


</cfif>
<cfif showouonly>and #oneaimSQLname#.dbo.Groups.OUid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

ORDER BY buname, ouname, Groups.Group_Name
</cfquery>
