<cfparam name="qrygou" default="">
<cfif isdefined("getOU.recordcount")>
<cfset qrygou = valuelist(getOU.id)>
</cfif>
<cfif isdefined("ou")>
<cfif listfindnocase(ou,"All") eq 0>
	<cfset qrygou = ou>
</cfif>
</cfif>

<cfquery name="getgroups" datasource="#request.dsn#">
SELECT        Group_Number, Group_Name, OUID, Business_line_id
FROM            Groups
WHERE        (Active_Group = 1) <cfif trim(qrygou) neq ''> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qrygou#" list="yes">)</cfif>

<cfif listfindnocase("observationsearch,doobservationsearch",fusebox.fuseaction) gt 0>
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	and business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
			and ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	</cfif>
</cfif>
<cfif  listfindnocase(request.userlevel,"User") gt 0>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and  listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"HSSE Advisor") eq 0>
		and Group_Number in (SELECT DISTINCT Groups.Group_Number
		FROM            GroupUserAssignment INNER JOIN
		                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
		WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
</cfif>
</cfif>
</cfif>
ORDER BY Business_line_id, ouid, Group_Name
</cfquery>