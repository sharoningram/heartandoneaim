<cfquery name="getBS" datasource="#request.dsn#">
	SELECT        ID, Name
	FROM            NewDials
	WHERE        (Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getOU.id)#" list="yes">)) AND (status <> 99) and isgroupdial = 0
	<cfif listfindnocase("observationsearch,doobservationsearch",fusebox.fuseaction) gt 0>
	<cfif  listfindnocase(request.userlevel,"User") gt 0>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and  listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"HSSE Advisor") eq 0>
	and id in (SELECT DISTINCT Groups.BusinessStreamID
	FROM            GroupUserAssignment INNER JOIN
	                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
	WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
	</cfif>
	</cfif>
	</cfif>
	ORDER BY Name
</cfquery>