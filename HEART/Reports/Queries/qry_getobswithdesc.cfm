<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="eventtype" default="all">
<cfparam name="WHEREHAPPEN" default="all">
<cfparam name="OBSERVERNAME" default="">
<cfparam name="STAKEHOLDER" default="all">
<cfparam name="SAFETYRULES" default="all">
<cfparam name="SAFETYESSENTIALS" default="all">
<cfparam name="STATUS" default="all">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">
<cfparam name="ObservationNum" default="">
<cfparam name="searchname" default="">
<cfparam name="toOneAIM" default="No">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">
<cfparam name="sortordby" default="obsdate">

<cfset statlist = "">

<cfif listfindnocase(STATUS,"All") eq 0>
	<cfloop list="#STATUS#" index="i">
		<cfif i eq "oneaim">
			<cfset statlist = listappend(statlist,"Cancelled1")>
		<cfelse>
			<cfset  statlist = listappend(statlist,"#i#0")>
			<cfset  statlist = listappend(statlist,"#i#1")>
		</cfif>
	</cfloop>
</cfif> 

<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0>
	<cfset showouonly = "no">
</cfif>


<cfquery name="getobswithdesc" datasource="#request.heart_ds#">
SELECT        Observations.ObservationNumber, CAST(Observations.TrackingYear AS Varchar(4)) + RIGHT('00000' + CAST(Observations.TrackingNum AS VARCHAR(5)), 5) 
                         AS trackingnum,  #oneaimSQLname#.dbo.NewDials.Name AS buname, NewDials_1.Name AS ouname, Observations.FollowUpActions,
                         #oneaimSQLname#.dbo.Groups.Group_Name, Observations.ObservationType, Observations.DateofObservation, #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssential, 
                         #oneaimSQLname#.dbo.SafetyRules.SafetyRule, Observations.Observations,  Observations.EscalateOneAim, Observations.PersonnelCategory,
						 CASE  WHEN Observations.Status = 'Submitted' THEN 'Awaiting Review' WHEN Observations.Status = 'Reviewed' THEN 'Awaiting Action' WHEN Observations.Status = 'Cancelled' AND Observations.EscalateOneAim = 1 THEN 'Escalated to oneAIM'  ELSE Observations.Status END AS status, Observations.ObserverName, 
                         #oneaimSQLname#.dbo.GroupLocations.SiteName
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.NewDials ON #oneaimSQLname#.dbo.Groups.Business_Line_ID = #oneaimSQLname#.dbo.NewDials.ID INNER JOIN
                         #oneaimSQLname#.dbo.NewDials AS NewDials_1 ON #oneaimSQLname#.dbo.Groups.OUid = NewDials_1.ID INNER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID LEFT OUTER JOIN
                         #oneaimSQLname#.dbo.SafetyEssentials ON Observations.SafetyEssential = #oneaimSQLname#.dbo.SafetyEssentials.SafetyEssentialID
WHERE    1 = 1   
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND Observations.DateofObservation BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">
<!--- and  (Observations.Status <> 'Cancelled') --->

<cfelse>

<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and #oneaimSQLname#.dbo.groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Observations.ProjectNumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and Observations.SiteID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>

<cfif trim(startdate) neq ''>
	and Observations.DateofObservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.DateofObservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.DateofObservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif wherehappen neq "all">
	and Observations.isworkhome = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wherehappen#">
</cfif>
<cfif listfindnocase(stakeholder,"All") eq 0>
	and Observations.PersonnelCategory in  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#stakeholder#" list="Yes">)
</cfif>
<cfif listfindnocase(eventtype,"All") eq 0>
	and Observations.ObservationType in  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#eventtype#" list="Yes">)
</cfif>
<cfif listfindnocase(STATUS,"All") eq 0>
	and Observations.status+CONVERT(varchar, Observations.EscalateOneAim) in  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#statlist#" list="yes">)
</cfif>
<!--- <cfif toOneAIM eq "No">
	and Observations.EscalateOneAim = 0
<cfelseif toOneAIM eq "Yes">
	and Observations.EscalateOneAim = 1
</cfif>
 --->
</cfif>
<cfif showouonly>and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

<cfif sortordby eq "obsname">
	ORDER BY ObserverName 
</cfif>
<cfif sortordby eq "obsdate">
	ORDER BY DateofObservation desc
</cfif>
<cfif sortordby eq "obsnum">
	ORDER BY trackingnum
</cfif>
<cfif sortordby eq "bu">
	ORDER BY buname
</cfif>
<cfif sortordby eq "ou">
	ORDER BY ouname
</cfif>
<cfif sortordby eq "project">
	ORDER BY group_name
</cfif>
<cfif sortordby eq "site">
	ORDER BY sitename
</cfif>
<cfif sortordby eq "event">
	ORDER BY ObservationType
</cfif>
<cfif sortordby eq "stakeholder">
	ORDER BY PersonnelCategory
</cfif>
<cfif sortordby eq "gsr">
	ORDER BY SafetyRule
</cfif>
<cfif sortordby eq "se">
	ORDER BY SafetyEssential
</cfif>
<cfif sortordby eq "detail">
	ORDER BY CAST(Observations as Varchar(Max))
</cfif>
<cfif sortordby eq "status">
	ORDER BY Status
</cfif>
<cfif sortordby eq "acttaken">
	ORDER BY CAST(Observations.FollowUpActions as Varchar(Max))
</cfif>

</cfquery>