<cfquery name="getBUs" datasource="#request.dsn#">
SELECT        ID, Name
FROM            NewDials
WHERE        (Parent = 2707) AND (status <> 99)

<cfif listfindnocase("observationsearch,doobservationsearch,obswithdesc,diamond",fusebox.fuseaction) gt 0>

<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and  listfindnocase(request.userlevel,"Reports Only") eq 0>
and (id = 0
<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	or id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer')
</cfif>
<cfif  listfindnocase(request.userlevel,"BU Admin") gt 0>
	or id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
	or id in (
	select distinct parent from newdials
	where id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin'))
</cfif>
<cfif listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	or id in (
	select distinct parent from newdials
	where id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'HSSE Advisor'))
</cfif>
<cfif listfindnocase(request.userlevel,"Reviewer") gt 0>
	or id in (
	select distinct parent from newdials
	where id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Reviewer'))
</cfif>
<cfif  listfindnocase(request.userlevel,"User") gt 0>
or id in (
	select distinct parent from newdials
	where id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'User'))
</cfif>
)
</cfif>

</cfif>
<cfif listfindnocase("configdash,printconfigdash,exportconfigdash",fusebox.fuseaction) gt 0>
ORDER BY Name
<cfelse>
ORDER BY Parent
</cfif>
</cfquery>