
<!--- Observations.Status <> 'Cancelled' --->
<cfquery name="GetMyObservations" datasource="#request.HEART_DS#">
 Select ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated,EscalateOneAim,#oneaimSQLname#.dbo.groups.OUID,withdrawnby
FROM dbo.Observations,[#oneaimSQLname#].[dbo].[Groups]
Where ProjectNumber=Group_Number
AND CreatedbyEmail =<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
UNION
 Select ObservationNumber,ProjectNumber,Group_Name,ObservationBU,ObservationOU,ObservationType,TrackingYear,TrackingNum,Status,DateofObservation,ObserverEmail,CreatedbyEmail,DateCreated,EscalateOneAim,#oneaimSQLname#.dbo.groups.OUID,withdrawnby
FROM dbo.Observations,[#oneaimSQLname#].[dbo].[Groups]
Where 
1=1
AND ProjectNumber=Group_Number
AND ObserverEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">

</cfquery>
