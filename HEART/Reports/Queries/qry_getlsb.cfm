
<cfquery name="getlsb" datasource="#request.heart_ds#">
SELECT        COUNT(Observations.GlobalSafetyRules) AS cnt, #oneaimSQLname#.dbo.SafetyRules.SafetyRule AS lbl
FROM            Observations INNER JOIN
                         #oneaimSQLname#.dbo.SafetyRules ON Observations.GlobalSafetyRules = #oneaimSQLname#.dbo.SafetyRules.SafetyRuleID INNER JOIN
                         #oneaimSQLname#.dbo.Groups ON Observations.ProjectNumber = #oneaimSQLname#.dbo.Groups.Group_Number INNER JOIN
                         #oneaimSQLname#.dbo.GroupLocations ON Observations.SiteID = #oneaimSQLname#.dbo.GroupLocations.GroupLocID
WHERE          (Observations.Status <> 'Cancelled') 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND (Observations.dateofobservation BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">)
<cfelse>
<cfif trim(startdate) neq ''>
	and Observations.dateofobservation >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and Observations.dateofobservation <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(Observations.dateofobservation) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.Business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and #oneaimSQLname#.dbo.Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and #oneaimSQLname#.dbo.GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>

<cfif listfindnocase(STATUS,"All") eq 0>
	<cfif status eq "Open">
		and Observations.Status in ('Submitted','Reviewed')
	<cfelse>
		and Observations.Status = 'closed'
	</cfif>
</cfif>
<cfif listfindnocase(STAKEHOLDER,"All") eq 0>
	and Observations.PersonnelCategory in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#STAKEHOLDER#" list="Yes">)
</cfif>
<cfif listfindnocase(WHEREHAPPEN,"All") eq 0>
	and Observations.IsWorkHome in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#WHEREHAPPEN#" list="Yes">)
</cfif>
</cfif>
GROUP BY Observations.GlobalSafetyRules, #oneaimSQLname#.dbo.SafetyRules.SafetyRule

</cfquery>
