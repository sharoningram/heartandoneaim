<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="home">
	<cfinclude template="displays/dsp_reports.cfm">
</cfcase>
<cfcase value="myreports">
	<cfset attributes.xfa.directmyreport = "reports.directmyreport">
	<cfset attributes.xfa.deletemyreports = "reports.deletemyreports">
	<cfinclude template="queries/qry_getuserreports.cfm">
	<cfinclude template="displays/dsp_myreports.cfm">
</cfcase>

<cfcase value="MyObservations">
	<cfset attributes.xfa.exportobservationsearch = "reports.exportobservationsearch">
	<cfinclude template="queries/qry_myobservations.cfm">
	<cfinclude template="displays/dsp_MyObservations.cfm">
</cfcase>

<cfcase value="deletemyreports">
	<cfset attributes.xfa.myreports = "reports.myreports">
	<cfinclude template="actions/act_deletemyreports.cfm">
</cfcase>
<cfcase value="directmyreport">
	<cfinclude template="queries/qry_getreportinfo.cfm">
	<cfinclude template="actions/act_redirecttoreport.cfm">
</cfcase>
<cfcase value="observationsearch">
	<cfset attributes.xfa.doobservationsearch = "reports.doobservationsearch">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfinclude template="queries/qry_getuserobssearchdet.cfm">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	<cfinclude template="queries/qry_getrules.cfm">
	<cfinclude template="queries/qry_getessentials.cfm">
	<cfinclude template="forms/frm_observationsearch.cfm">
</cfcase>
<cfcase value="createsublistsmulti">
	<cfinclude template="actions/act_createsublistsmulti.cfm">
</cfcase>
<cfcase value="doobservationsearch">
	<cfset attributes.xfa.observationsearch = "reports.observationsearch">
	<cfset attributes.xfa.doobservationsearch = "reports.doobservationsearch">
	<cfset attributes.xfa.exportobservationsearch = "reports.exportobservationsearch">
	<cfset attributes.xfa.incidentreport = "incidents.incidentreport">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	<cfinclude template="queries/qry_getrules.cfm">
	<cfinclude template="queries/qry_getessentials.cfm">
	<cfinclude template="queries/qry_getobservationsearchresults.cfm">
	<cfinclude template="actions/act_saveobservationsearch.cfm">
	<cfinclude template="displays/dsp_observationsearch.cfm">
</cfcase>
<cfcase value="exportobservationsearch">
	<cfinclude template="queries/qry_getobservationsearchresults.cfm">
	<cfinclude template="actions/act_exportobjsearch.cfm">
</cfcase>
<cfcase value="createsublists">
	<cfinclude template="actions/act_createsublists.cfm">
</cfcase>
<cfcase value="mysearches">
	<cfset attributes.xfa.observationsearch = "reports.observationsearch">
	<cfset attributes.xfa.deleteincsearch = "reports.deleteincsearch">
	<cfinclude template="queries/qry_getuseronjsearches.cfm">
	<cfinclude template="forms/frm_userobjsearches.cfm">
</cfcase>
<cfcase value="deleteincsearch">
	<cfset attributes.xfa.mysearches = "reports.mysearches">
	<cfinclude template="actions/act_deleteobssearch.cfm">
</cfcase>
<cfcase value="diamond">
	<cfset attributes.xfa.rundiamond = "reports.rundiamond">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	<cfinclude template="forms/frm_diamond.cfm">
</cfcase>
<cfcase value="rundiamond,exportrundiamond">
	<cfset attributes.xfa.rundiamond = "reports.rundiamond">
	<cfset attributes.xfa.exportrundiamond = "reports.exportrundiamond">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	<cfinclude template="actions/act_getdiamondrecs.cfm">
</cfcase>
<cfcase value="eventtype,printeventtype,oueventtype,printoueventtype,prjeventtype,printprjeventtype">
	<cfset attributes.xfa.filteraction = "reports.#fusebox.fuseaction#">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfset attributes.xfa.printeventtype = "reports.printeventtype">
	<cfset attributes.xfa.printoueventtype = "reports.printoueventtype">
	<cfset attributes.xfa.printprjeventtype = "reports.printprjeventtype">
	
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	
	<cfinclude template="queries/qry_eventtype.cfm">
		<cfif listfindnocase("printsafetyessentials,printousafetyessentials,printprjsafetyessentials",fusebox.fuseaction) eq 0>
			<cfinclude template="actions/act_saveReportCriteria.cfm">
			<cfinclude template="forms/frm_reportfilter.cfm">
		</cfif>
		<cfinclude template="displays/dsp_reportfilter.cfm">
		<cfinclude template="displays/dsp_eventtypechart.cfm">
</cfcase>

<cfcase value="obssummary,printobssummary,obssummarypdf">
	<cfset attributes.xfa.filteraction = "reports.#fusebox.fuseaction#">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfset attributes.xfa.obssummary = "reports.obssummary">
	<cfset attributes.xfa.exportobssummary = "reports.exportobssummary">
	<cfset attributes.xfa.printobssummary = "reports.printobssummary">
	<cfset attributes.xfa.obssummarypdf = "reports.obssummarypdf">
	
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	
	<cfinclude template="queries/qry_obssummary.cfm">
	<cfinclude template="actions/act_buildobssummstructs.cfm">
		<cfif listfindnocase("printobssummary,obssummarypdf",fusebox.fuseaction) eq 0>
			<cfinclude template="actions/act_saveReportCriteria.cfm">
			<cfinclude template="forms/frm_reportfilter.cfm">
		</cfif>
		<cfif fusebox.fuseaction eq "obssummarypdf">
			<cfinclude template="displays/dsp_obssummarypdf.cfm">
		<cfelse>
			<cfinclude template="displays/dsp_reportfilter.cfm">
			<cfinclude template="displays/dsp_obssummary.cfm">
		</cfif>
</cfcase>
<cfcase value="exportobssummary">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	<cfinclude template="queries/qry_obssummary.cfm">
	<cfinclude template="actions/act_buildobssummstructs.cfm">
	<cfinclude template="actions/act_exportobssummary.cfm">
</cfcase>

<cfcase value="safetyrules,printsafetyrules,ousafetyrules,printousafetyrules,prjsafetyrules,printprjsafetyrules">
	<cfset attributes.xfa.filteraction = "reports.#fusebox.fuseaction#">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfset attributes.xfa.printsafetyrules = "reports.printsafetyrules">
	<cfset attributes.xfa.printousafetyrules = "reports.printousafetyrules">
	<cfset attributes.xfa.printprjsafetyrules = "reports.printprjsafetyrules">
	
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	
	<cfinclude template="queries/qry_getsafetyrules.cfm">
		<cfif listfindnocase("printsafetyessentials,printousafetyessentials,printprjsafetyessentials",fusebox.fuseaction) eq 0>
			<cfinclude template="actions/act_saveReportCriteria.cfm">
			<cfinclude template="forms/frm_reportfilter.cfm">
		</cfif>
		<cfinclude template="displays/dsp_reportfilter.cfm">
		<cfinclude template="displays/dsp_safetyrules.cfm">
</cfcase>



<cfcase value="obswithdesc,printobswithdesc,printobswithdescpdf">
	<cfset attributes.xfa.filteraction = "reports.#fusebox.fuseaction#">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfset attributes.xfa.obswithdesc = "reports.obswithdesc">
	<cfset attributes.xfa.exportobswithdesc = "reports.exportobswithdesc">
	<cfset attributes.xfa.printobswithdesc = "reports.printobswithdesc">
	<cfset attributes.xfa.printobswithdescpdf = "reports.printobswithdescpdf">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	<cfinclude template="queries/qry_getobswithdesc.cfm">
	
		<cfif listfindnocase("printobswithdesc,printobswithdescpdf",fusebox.fuseaction) eq 0>
			<cfinclude template="actions/act_saveReportCriteria.cfm">
			<cfinclude template="forms/frm_reportfilter.cfm">
		</cfif>
		<cfif fusebox.fuseaction eq "printobswithdescpdf">
			<cfinclude template="displays/dsp_obswithdescpdf.cfm">
		<cfelse>
			<cfinclude template="displays/dsp_reportfilter.cfm">
			<cfinclude template="displays/dsp_obswithdesc.cfm">
		</cfif>
	
</cfcase>
<cfcase value="exportobswithdesc">
	<cfinclude template="queries/qry_getobswithdesc.cfm">
	<cfinclude template="actions/act_exportobswdesc.cfm">
</cfcase>



<cfcase value="safetyessentials,printsafetyessentials,ousafetyessentials,printousafetyessentials,prjsafetyessentials,printprjsafetyessentials">
	<cfset attributes.xfa.filteraction = "reports.#fusebox.fuseaction#">
	<cfset attributes.xfa.createsublistsmulti = "reports.createsublistsmulti">
	<cfset attributes.xfa.printsafetyessentials = "reports.printsafetyessentials">
	<cfset attributes.xfa.printousafetyessentials = "reports.printousafetyessentials">
	<cfset attributes.xfa.printprjsafetyessentials = "reports.printprjsafetyessentials">
	
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getBSs.cfm">
	<cfinclude template="queries/qry_getGroups.cfm">
	<cfinclude template="queries/qry_getsites.cfm">
	<cfinclude template="queries/qry_getobservationyears.cfm">
	
	<cfinclude template="queries/qry_getsafetyessentials.cfm">
		<cfif listfindnocase("printsafetyessentials,printousafetyessentials,printprjsafetyessentials",fusebox.fuseaction) eq 0>
			<cfinclude template="actions/act_saveReportCriteria.cfm">
			<cfinclude template="forms/frm_reportfilter.cfm">
		</cfif>
		<cfinclude template="displays/dsp_reportfilter.cfm">
		<cfinclude template="displays/dsp_safetyessentials.cfm">
</cfcase>


<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>
</cfswitch>
