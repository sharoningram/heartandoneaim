<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportobswdesc.cfm", "exports"))>
<cfset thefilename = "ObservationsWithDescription_#incyear#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">

<cfset theSheet = spreadsheetnew("ObservationsWithDescription_","true")>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Incident Number",1,1);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,4);
	SpreadsheetSetCellValue(theSheet,"Site/Office Name",1,5);
	SpreadsheetSetCellValue(theSheet,"Event Type",1,6);
	SpreadsheetSetCellValue(theSheet,"Stakeholder",1,7);
	SpreadsheetSetCellValue(theSheet,"Observation Date",1,8);
	SpreadsheetSetCellValue(theSheet,"Observer's Name",1,9);
	SpreadsheetSetCellValue(theSheet,"Global Safety Rule",1,10);
	SpreadsheetSetCellValue(theSheet,"Safety Essential",1,11);
	SpreadsheetSetCellValue(theSheet,"Details of Observation",1,12);
	SpreadsheetSetCellValue(theSheet,"Immediate action taken/recommended",1,13);
	SpreadsheetSetCellValue(theSheet,"Status",1,14);
</cfscript>
<cfset ctr = 1>
<cfoutput query="getobswithdesc">
<cfset ctr = ctr + 1>
	<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#trackingnum#",#ctr#,1);
		SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,2);
		SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,3);
		SpreadsheetSetCellValue(theSheet,"#Group_Name#",#ctr#,4);
		SpreadsheetSetCellValue(theSheet,"#sitename#",#ctr#,5);
		SpreadsheetSetCellValue(theSheet,"#ObservationType#",#ctr#,6);
		SpreadsheetSetCellValue(theSheet,"#PersonnelCategory#",#ctr#,7);
		SpreadsheetSetCellValue(theSheet,"#dateformat(DateofObservation,sysdateformatxcel)#",#ctr#,8);
		SpreadsheetSetCellValue(theSheet,"#ObserverName#",#ctr#,9);
		SpreadsheetSetCellValue(theSheet,"#SafetyRule#",#ctr#,10);
		SpreadsheetSetCellValue(theSheet,"#SafetyEssential#",#ctr#,11);
		SpreadsheetSetCellValue(theSheet,"#Observations#",#ctr#,12);
		SpreadsheetSetCellValue(theSheet,"#FollowUpActions#",#ctr#,13);
		SpreadsheetSetCellValue(theSheet,"#status#",#ctr#,14);
	</cfscript>
</cfoutput>

<cfset ctr = ctr + 1>
<cfoutput>
	<cfscript> 
		SpreadsheetSetCellValue(theSheet,"Count of Observations:",#ctr#,1);
		SpreadsheetSetCellValue(theSheet,"#getobswithdesc.recordcount#",#ctr#,2);
	</cfscript>
</cfoutput>	

<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.heartPath#/reports/exports/#thefilename#">