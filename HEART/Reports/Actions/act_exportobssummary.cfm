<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportobssummary.cfm", "exports"))>
<cfset thefilename = "ObservationSummary_#incyear#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfparam name="openbu" default="0">
<cfparam name="openou" default="0">
<cfset theSheet = spreadsheetnew("ObservationSummary","true")>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"BU",1,1);
	SpreadsheetSetCellValue(theSheet,"Stakeholders",1,2);
	SpreadsheetSetCellValue(theSheet,"Unsafe Act",1,3);
	SpreadsheetSetCellValue(theSheet,"Unsafe Condition",1,4);
	SpreadsheetSetCellValue(theSheet,"Safe Behaviour",1,5);
</cfscript>

<cfset expctr = 1>




<cfloop query="getbus" group="ID">
	<cfset butotUA = 0>
	<cfset butotUB = 0>
	<cfset butotSB = 0>
	
		<cfset buctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset expctr = expctr+1>
		<cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Act")><cfset butotUA = butotUA+etstruct["#ID#_#i#_Unsafe Act"]><cfset dspua = numberformat(etstruct["#ID#_#i#_Unsafe Act"])><cfelse><cfset dspua = 0></cfif>
		<cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Condition")><cfset butotUB = butotUB+etstruct["#ID#_#i#_Unsafe Condition"]><cfset dspuc = numberformat(etstruct["#ID#_#i#_Unsafe Condition"])><cfelse><cfset dspuc = 0></cfif>
		<cfif structkeyexists(etstruct,"#ID#_#i#_Safe Behaviour")><cfset butotSB = butotSB+etstruct["#ID#_#i#_Safe Behaviour"]><cfset dspsb = numberformat(etstruct["#ID#_#i#_Safe Behaviour"])><cfelse><cfset dspsb = 0></cfif>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getBUs.Name#",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"#i#",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#dspua#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#dspuc#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#dspsb#",#expctr#,5);
		</cfscript>
		
		
		</cfloop>
		
		<cfset expctr = expctr + 1>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getBUs.Name# Total",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#butotUA#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#butotUB#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#butotSB#",#expctr#,5);
		</cfscript>
		
		<cfloop query="getOU">
		<cfif getou.qrybuid eq getbus.id and listfind(openbu,getou.qrybuid) gt 0>
			<cfset outotUA = 0>
		<cfset outotUB = 0>
		<cfset outotSB = 0>
		
		<cfset ouctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset expctr = expctr+1>
		<cfset ouctr = ouctr+1>
		
		<cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Act")><cfset outotUA = outotUA+etstruct["#ID#_#i#_Unsafe Act"]><cfset dspua = numberformat(etstruct["#ID#_#i#_Unsafe Act"])><cfelse><cfset dspua = 0></cfif>
		<cfif structkeyexists(etstruct,"#ID#_#i#_Unsafe Condition")><cfset outotUB = outotUB+etstruct["#ID#_#i#_Unsafe Condition"]><cfset dspuc = numberformat(etstruct["#ID#_#i#_Unsafe Condition"])><cfelse><cfset dspuc = 0></cfif>
		<cfif structkeyexists(etstruct,"#ID#_#i#_Safe Behaviour")><cfset outotSB = outotSB+etstruct["#ID#_#i#_Safe Behaviour"]><cfset dspsb = numberformat(etstruct["#ID#_#i#_Safe Behaviour"])><cfelse><cfset dspsb = 0></cfif>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getou.Name#",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"#i#",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#dspua#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#dspuc#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#dspsb#",#expctr#,5);
		</cfscript>
		
		
		
		</cfloop>
		
		<cfset expctr = expctr + 1>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getou.Name# Total",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#outotUA#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#outotUB#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#outotSB#",#expctr#,5);
		</cfscript>
		
		<cfloop query="getgroups">
		<cfif getgroups.ouid eq getou.id and listfind(openou,getou.id) gt 0>
		<cfset gtotUA = 0>
		<cfset gtotUB = 0>
		<cfset gtotSB = 0>
		<cfset gctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset expctr = expctr+1>
		<cfset gctr = gctr+1>
		<cfif structkeyexists(prjetstruct,"#group_number#_#i#_Unsafe Act")><cfset gtotUA = gtotUA+prjetstruct["#group_number#_#i#_Unsafe Act"]><cfset dspua = numberformat(prjetstruct["#group_number#_#i#_Unsafe Act"])><cfelse><cfset dspua = 0></cfif>
		<cfif structkeyexists(prjetstruct,"#group_number#_#i#_Unsafe Condition")><cfset gtotUB = gtotUB +prjetstruct["#group_number#_#i#_Unsafe Condition"]><cfset dspuc = numberformat(prjetstruct["#group_number#_#i#_Unsafe Condition"])><cfelse><cfset dspuc = 0></cfif>
		<cfif structkeyexists(prjetstruct,"#group_number#_#i#_Safe Behaviour")><cfset gtotSB = gtotSB +prjetstruct["#group_number#_#i#_Safe Behaviour"]><cfset dspsb = numberformat(prjetstruct["#group_number#_#i#_Safe Behaviour"])><cfelse><cfset dspsb = 0></cfif>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#group_name#",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"#i#",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#dspua#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#dspuc#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#dspsb#",#expctr#,5);
		</cfscript>
		
		
	
		
		</cfloop>
		<cfset expctr = expctr + 1>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#group_name# Total",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#gtotUA#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#gtotUB#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#gtotSB#",#expctr#,5);
		</cfscript>
		</cfif>
		</cfloop>
	</cfif>
	</cfloop>
		
	</cfloop>
	<cfset afwtotUA = 0>
	<cfset afwtotUB = 0>
	<cfset afwtotSB = 0>
	<cfset buctr = 0>
		<cfloop list="#shlist#" index="i">
		<cfset expctr = expctr+1>
		<cfset buctr = buctr+1>
		<cfif structkeyexists(etstruct,"global_#i#_Unsafe Act")><cfset afwtotUA = afwtotUA+etstruct["global_#i#_Unsafe Act"]><cfset dspua = numberformat(etstruct["global_#i#_Unsafe Act"])><cfelse><cfset dspua = 0></cfif>
		<cfif structkeyexists(etstruct,"global_#i#_Unsafe Condition")><cfset afwtotUB = afwtotUB+etstruct["global_#i#_Unsafe Condition"]><cfset dspuc = numberformat(etstruct["global_#i#_Unsafe Condition"])><cfelse><cfset dspuc = 0></cfif>
		<cfif structkeyexists(etstruct,"global_#i#_Safe Behaviour")><cfset afwtotSB = afwtotSB+etstruct["global_#i#_Safe Behaviour"]><cfset dspsb = numberformat(etstruct["global_#i#_Safe Behaviour"])><cfelse><cfset dspsb = 0></cfif>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler Global",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"#i#",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#dspua#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#dspuc#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#dspsb#",#expctr#,5);
		</cfscript>
		
		</cfloop>
		<cfset expctr = expctr + 1>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler Global Total",#expctr#,1);
			SpreadsheetSetCellValue(theSheet,"",#expctr#,2);
			SpreadsheetSetCellValue(theSheet,"#afwtotUA#",#expctr#,3);
			SpreadsheetSetCellValue(theSheet,"#afwtotUB#",#expctr#,4);
			SpreadsheetSetCellValue(theSheet,"#afwtotSB#",#expctr#,5);
		</cfscript>
		 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.heartPath#/reports/exports/#thefilename#">