<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="eventtype" default="all">
<cfparam name="WHEREHAPPEN" default="all">
<cfparam name="OBSERVERNAME" default="">
<cfparam name="STAKEHOLDER" default="all">
<cfparam name="SAFETYRULES" default="all">
<cfparam name="SAFETYESSENTIALS" default="all">
<cfparam name="STATUS" default="all">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">
<cfparam name="ObservationNum" default="">
<cfparam name="searchname" default="">
<cfparam name="toOneAIM" default="No">

<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.heart_ds#">
		select UserReportID
		from UserSavedReports
		where userid = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cookie.useremail#">  and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#"> and reportType = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#reptype#">
	</cfquery>
	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.heart_ds#">
			delete from UserSavedReports
			where UserReportID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.UserReportID#">
		</cfquery>
	</cfif>
	

		<cfquery name="addsearch" datasource="#request.heart_ds#">
			insert into UserSavedReports (SearchName, UserID, incyear, bu, ou, bs, project, siteoffice, startdate, enddate, DateSaved, reportType, eventtype, wherehappen, stakeholder, status, toOneAIM)
			values(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cookie.useremail#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incyear#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#reptype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#eventtype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wherehappen#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#stakeholder#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#status#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#toOneAIM#">)			 
						 
			
		</cfquery>
	
</cfif>
</cfif>