<cfparam name="diamondtype" default="">

<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_lsb.cfm", "exports"))>
 <cfif not directoryexists("#FileDir#")>
	<cfdirectory action="CREATE" directory="#FileDir#">
</cfif>

<cfif diamondtype eq "pdf">
<cfset thefilename = "GlobalSafetyRules_#datetimeformat(now(),'HHnnssl')#.pdf">
<cfdocument format="PDF" filename="#filedir#\#thefilename#"  overwrite="yes">
<table cellpadding="4" cellspacing="1" bgcolor="5f2167" width="60%" align="center">
	<tr>
		<td bgcolor="5f2167" colspan="2"  align="center"><strong style="font-family: Segoe UI;font-size:10pt;color:ffffff;">Global Safety Rules</strong></td>
	</tr>
	<cfoutput query="getlsb">
		<tr>
			<td bgcolor="ffffff"  width="80%" bgcolor="ffffff" style="font-family: Segoe UI;font-size:10pt;">#lbl#</td>
			<Td bgcolor="ffffff" align="center" bgcolor="ffffff" style="font-family: Segoe UI;font-size:10pt;">#cnt#</td>
		</tr>
	</cfoutput>
</table>
</cfdocument>

<cflocation addtoken="No" url="/#getappconfig.heartPath#/reports/exports/#thefilename#">

<cfelseif diamondtype eq "xcl">
	<cfset thefilename = "GlobalSafetyRules_#datetimeformat(now(),'HHnnssl')#.xlsx">
	<cfset thefile = "#filedir#\#thefilename#">
	<cfset theSheet = spreadsheetnew("Global Safety Rules","true")>
	<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"Global Safety Rules",1,1);
	 	SpreadsheetSetCellValue(theSheet,"Number of Incidents",1,2);
	</cfscript>
	<cfset ctr = 1>
	<cfoutput query="getlsb">
	<cfset ctr = ctr + 1>
		<cfscript> 
	 		SpreadsheetSetCellValue(theSheet,"#lbl#",#ctr#,1);
	 		SpreadsheetSetCellValue(theSheet,"#cnt#",#ctr#,2);
		</cfscript>
	</cfoutput>
 	<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
	<cflocation addtoken="No" url="/#getappconfig.heartPath#/reports/exports/#thefilename#">



</cfif>