<cfparam name="type"  default="">
<cfparam name="BU"  default="">
<cfparam name="OU"  default="">
<cfparam name="PRJ"  default="">
<cfparam name="fromdiamond"  default="no">
<cfif trim(type) neq ''>
	<cfswitch expression="#type#">
		<cfcase value="OU">
			<cfif trim(BU) neq ''>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS buname
					FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
					WHERE  
					<cfif listfindnocase(bu,"all") eq 0> 
					     (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes"> )) AND
					<cfelse>
						 (NewDials.Parent in (select id from newdials where status <> 99 and isgroupdial = 0 and parent = (select id from newdials where status <> 99 and isgroupdial = 0 and parent = 0))) and
					</cfif> 
						  (NewDials.status <> 99) and NewDials.isgroupdial = 0
						  
					<!--- <cfif fromdiamond eq "no"> --->
					<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
						<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
							and NewDials.id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
						</cfif>
					</cfif>
					<cfif  listfindnocase(request.userlevel,"User") gt 0>
					<cfif trim(request.UserID) neq ''>
					and NewDials.id in (SELECT DISTINCT Groups.ouid
					FROM            GroupUserAssignment INNER JOIN
					                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
					WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
					</cfif>
					</cfif>
					<!--- </cfif> --->
					ORDER BY buname , NewDials.Name
				</cfquery>
				{zz{<select name="ou" size="5"  class="selectgen" id="lrgselect"  onchange="this.style.border='1px solid 5f2167';buildbslist(this.value);" multiple>
					<option value="all" selected>All</option>
						<cfoutput query="getOU" group="buname"><option value="" disabled>#buname#</option><cfoutput><option value="#id#">&nbsp;&nbsp;#name#</option></cfoutput></cfoutput>
					</select>}zz}
				
					<cfquery name="getBS" datasource="#request.dsn#">
						SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS ouname
					FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
						WHERE        (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getOU.id)#" list="Yes">)) AND (NewDials.status <> 99) and NewDials.isgroupdial = 0
					<cfif fromdiamond eq "no">
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
							and NewDials.id in (SELECT DISTINCT Groups.BusinessStreamID
							FROM            GroupUserAssignment INNER JOIN
					                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
							WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
					</cfif>
						ORDER BY ouname, NewDials.Name
					</cfquery>
					{bb{<select name="businessstream"  size="5"  class="selectgen" id="lrgselect" multiple>
					<option value="all" selected>All</option>
					<cfoutput query="getBS" group="ouname"><option value="" disabled>#ouname#</option><cfoutput><option value="#id#">&nbsp;&nbsp;#name#</option></cfoutput></cfoutput>
					</select>}bb}
					
					
					<cfquery name="getgroups" datasource="#request.dsn#">
						SELECT        Groups.Group_Number, Groups.Group_Name, NewDials.Name
						FROM            Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID
						WHERE        (Groups.Active_Group = 1)  
						
						<cfif listfindnocase(bu,"all") eq 0> 
							and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes"> ) 
						</cfif>
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
								<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
								</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
								<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
									and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and Groups.Group_Number in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
						ORDER BY NewDials.Name,Groups.Group_Name
						</cfquery>
						{gg{<select name="projectoffice"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';buildprjlist(this.value);">
					<option value="all"  selected>All</option>
					<cfoutput query="getgroups" group="name"><option value="" disabled>#name#</option><cfoutput><option value="#group_number#" >&nbsp;&nbsp;#group_name#</option></cfoutput></cfoutput>
					</select>}gg}
						
						<cfquery name="getsites" datasource="#request.dsn#">
						SELECT        GroupLocations.GroupLocID, GroupLocations.SiteName, GroupLocations.isActive, NewDials.Name
						FROM            GroupLocations INNER JOIN
                         Groups ON GroupLocations.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID
						WHERE        (GroupLocations.isActive = 1)
					<!--- 	<cfif getgroups.recordcount gt 0> --->
						<cfif listfindnocase(bu,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
							<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
							
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
						ORDER BY NewDials.Name, GroupLocations.SiteName
						</cfquery>
							{ss{<select name="site"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all"  selected>All</option>
					<cfoutput query="getsites" group="name"><option value="" disabled>#name#</option><cfoutput><option value="#GroupLocID#" >&nbsp;&nbsp;#sitename#</option></cfoutput></cfoutput>
					</select>}ss}
					
					<cfquery name="getcontractors" datasource="#request.dsn#">
					SELECT DISTINCT ContractorNames.ContractorID, ContractorNames.ContractorName, ContractorNames.CoType
						FROM            ContractorNames LEFT OUTER JOIN
                         Contractors ON ContractorNames.ContractorID = Contractors.ContractorNameID LEFT OUTER JOIN
                         Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID ON Contractors.GroupNumber = Groups.Group_Number
					WHERE        (ContractorNames.Status = 1)
					<!--- 	<cfif getgroups.recordcount gt 0> --->
						<cfif listfindnocase(bu,"all") eq 0>
						and Contractors.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (Groups.Group_Number IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (Groups.Group_Number IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
							</cfif>
								
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and Groups.Group_Number in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY ContractorNames.CoType, ContractorNames.ContractorName
					</cfquery>
					
					{mc{<select name="frmmgdcontractor"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "mgd"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}mc}
					
					{jv{<select name="frmjv"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "jv"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}jv}
					
					{su{<select name="frmsubcontractor"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "sub"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}su}
					
					<cfquery name="getclients" datasource="#request.dsn#">
					SELECT   distinct     Clients.ClientName, Clients.ClientID
					FROM            GroupLocations RIGHT OUTER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID
					WHERE        (Clients.status = 1)
					<!--- <cfif getgroups.recordcount gt 0> --->
					<cfif listfindnocase(bu,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
					</cfif>
					<!--- 	</cfif> --->
					<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
							</cfif>
								
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY Clients.ClientName
					</cfquery>
					{cc{<select name="frmclient"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all" selected>All</option>
					<cfoutput query="getclients"><option value="#ClientID#" >#ClientName#</option></cfoutput>
					</select>}cc}
					<cfquery name="getlocdetails" datasource="#request.dsn#">
					SELECT DISTINCT LocationDetails.LocationDetailID, LocationDetails.LocationDetail
					FROM            GroupLocations RIGHT OUTER JOIN
                         LocationDetails ON GroupLocations.LocationDetailID = LocationDetails.LocationDetailID
					WHERE        (LocationDetails.Status = 1)
					<!--- <cfif getgroups.recordcount gt 0> --->
					<cfif listfindnocase(bu,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
													   </cfif>
								
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY LocationDetail
					</cfquery>
					{ld{<select name="locdetail"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all" selected>All</option>
					<cfoutput query="getlocdetails"><option value="#LocationDetailID#">#LocationDetail#</option></cfoutput>
					</select>}ld}
					<cfquery name="getcountries" datasource="#request.dsn#">
					SELECT DISTINCT Countries.CountryID, Countries.CountryName
					FROM            GroupLocations RIGHT OUTER JOIN
                         Countries ON GroupLocations.CountryID = Countries.CountryID
					where Countries.status = 1
					<!--- <cfif getgroups.recordcount gt 0> --->
					<cfif listfindnocase(bu,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY Countries.CountryName
					</cfquery>
					{ct{<select name="frmcountryid"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcountries"><option value="#CountryID#" >#CountryName#</option></cfoutput>
					</select>}ct}
			</cfif>
		</cfcase>
		<cfcase value="BS">
			<cfif trim(BU) neq '' and trim(ou) neq ''>
			
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS buname
					FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
					WHERE  
					<cfif listfindnocase(bu,"all") eq 0> 
					     (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes"> )) AND
					<cfelse>
						 (NewDials.Parent in (select id from newdials where status <> 99 and isgroupdial = 0 and parent = (select id from newdials where status <> 99 and isgroupdial = 0 and parent = 0))) and
					</cfif> 
						  (NewDials.status <> 99) and NewDials.isgroupdial = 0
					<!--- <cfif fromdiamond eq "no"> --->
					<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
						and NewDials.id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
							</cfif>
					</cfif>
					<cfif  listfindnocase(request.userlevel,"User") gt 0>
					and NewDials.id in (SELECT DISTINCT Groups.ouid
					FROM            GroupUserAssignment INNER JOIN
					                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
					WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
					</cfif>
					<!--- </cfif> --->
					ORDER BY buname , NewDials.Name
				</cfquery>
			
				<cfquery name="getBS" datasource="#request.dsn#">
						SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS ouname
					FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
						WHERE     
						<cfif listfindnocase(ou,"all") eq 0>   (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)) AND
						
						<cfelse>
						 (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getOU.id)#" list="Yes">)) and
						
						 </cfif> (NewDials.status <> 99) and NewDials.isgroupdial = 0
						 <cfif fromdiamond eq "no">
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
							and NewDials.id in (SELECT DISTINCT Groups.BusinessStreamID
							FROM            GroupUserAssignment INNER JOIN
					                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
							WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						</cfif>
						ORDER BY ouname, NewDials.Name
					</cfquery>
					{bb{<select name="businessstream"  size="5"  class="selectgen" id="lrgselect" multiple>
					<option value="all" selected>All</option>
					<cfoutput query="getBS" group="ouname"><option value="" disabled>#ouname#</option><cfoutput><option value="#id#">&nbsp;&nbsp;#name#</option></cfoutput></cfoutput>
					</select>}bb}
					
					
					<cfquery name="getgroups" datasource="#request.dsn#">
						SELECT        Groups.Group_Number, Groups.Group_Name, NewDials.Name
						FROM            Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID
						WHERE        (Groups.Active_Group = 1)  
						<cfif listfindnocase(ou,"all") eq 0> 
							and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes"> ) 
						</cfif>
						<cfif listfindnocase(bu,"all") eq 0> 
							and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes"> ) 
						</cfif>
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
						</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
								<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
									and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and Groups.Group_Number in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
						ORDER BY NewDials.Name,Groups.Group_Name
						</cfquery>
						{gg{<select name="projectoffice"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" onchange="this.style.border='1px solid 5f2167';buildprjlist(this.value);">
					<option value="all"  selected>All</option>
					<cfoutput query="getgroups" group="name"><option value="" disabled>#name#</option><cfoutput><option value="#group_number#" >&nbsp;&nbsp;#group_name#</option></cfoutput></cfoutput>
					</select>}gg}
					
					
					<cfquery name="getsites" datasource="#request.dsn#">
						SELECT        GroupLocations.GroupLocID, GroupLocations.SiteName, GroupLocations.isActive, NewDials.Name
						FROM            GroupLocations INNER JOIN
                         Groups ON GroupLocations.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID
						WHERE        (GroupLocations.isActive = 1)
						<!--- <cfif getgroups.recordcount gt 0> --->
						<cfif listfindnocase(bu,"all") eq 0 or listfindnocase(ou,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
						ORDER BY NewDials.Name, GroupLocations.SiteName
						</cfquery>
							{ss{<select name="site"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all"  selected>All</option>
					<cfoutput query="getsites" group="name"><option value="" disabled>#name#</option><cfoutput><option value="#GroupLocID#" >&nbsp;&nbsp;#sitename#</option></cfoutput></cfoutput>
					</select>}ss}
					
					
					<cfquery name="getcontractors" datasource="#request.dsn#">
					SELECT DISTINCT ContractorNames.ContractorID, ContractorNames.ContractorName, ContractorNames.CoType
						FROM            ContractorNames LEFT OUTER JOIN
                         Contractors ON ContractorNames.ContractorID = Contractors.ContractorNameID LEFT OUTER JOIN
                         Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID ON Contractors.GroupNumber = Groups.Group_Number
					WHERE        (ContractorNames.Status = 1)
						<!--- <cfif getgroups.recordcount gt 0> --->
						<cfif listfindnocase(bu,"all") eq 0 or listfindnocase(ou,"all") eq 0>
						and Contractors.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (Groups.Group_Number IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (Groups.Group_Number IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and Groups.Group_Number in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY ContractorNames.CoType, ContractorNames.ContractorName
					</cfquery>
					
					{mc{<select name="frmmgdcontractor"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "mgd"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}mc}
					
					{jv{<select name="frmjv"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "jv"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}jv}
					
					{su{<select name="frmsubcontractor"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "sub"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}su}
					
					
					<cfquery name="getclients" datasource="#request.dsn#">
					SELECT   distinct     Clients.ClientName, Clients.ClientID
					FROM            GroupLocations RIGHT OUTER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID
					WHERE        (Clients.status = 1)
					<!--- <cfif getgroups.recordcount gt 0> --->
					<cfif listfindnocase(bu,"all") eq 0 or  listfindnocase(ou,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
					</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY Clients.ClientName
					</cfquery>
					{cc{<select name="frmclient"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all" selected>All</option>
					<cfoutput query="getclients"><option value="#ClientID#" >#ClientName#</option></cfoutput>
					</select>}cc}
					<cfquery name="getlocdetails" datasource="#request.dsn#">
					SELECT DISTINCT LocationDetails.LocationDetailID, LocationDetails.LocationDetail
					FROM            GroupLocations RIGHT OUTER JOIN
                         LocationDetails ON GroupLocations.LocationDetailID = LocationDetails.LocationDetailID
					WHERE        (LocationDetails.Status = 1)
					<!--- <cfif getgroups.recordcount gt 0> --->
					<cfif listfindnocase(bu,"all") eq 0 or  listfindnocase(ou,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY LocationDetail
					</cfquery>
					{ld{<select name="locdetail"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all" selected>All</option>
					<cfoutput query="getlocdetails"><option value="#LocationDetailID#">#LocationDetail#</option></cfoutput>
					</select>}ld}
					<cfquery name="getcountries" datasource="#request.dsn#">
					SELECT DISTINCT Countries.CountryID, Countries.CountryName
					FROM            GroupLocations RIGHT OUTER JOIN
                         Countries ON GroupLocations.CountryID = Countries.CountryID
					where Countries.status = 1
					<!--- <cfif getgroups.recordcount gt 0> --->
					<cfif listfindnocase(bu,"all") eq 0 or  listfindnocase(ou,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getgroups.group_number)#" list="Yes">)
						</cfif>
						<!--- </cfif> --->
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY Countries.CountryName
					</cfquery>
					{ct{<select name="frmcountryid"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcountries"><option value="#CountryID#" >#CountryName#</option></cfoutput>
					</select>}ct}
					
					
			</cfif>
		</cfcase>
		<cfcase value="PRJ">
			<cfif trim(BU) neq '' and trim(ou) neq '' and trim(prj) neq ''>
				<cfquery name="getcontractors" datasource="#request.dsn#">
					SELECT DISTINCT ContractorNames.ContractorID, ContractorNames.ContractorName, ContractorNames.CoType
						FROM            ContractorNames LEFT OUTER JOIN
                         Contractors ON ContractorNames.ContractorID = Contractors.ContractorNameID LEFT OUTER JOIN
                         Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID ON Contractors.GroupNumber = Groups.Group_Number
					WHERE        (ContractorNames.Status = 1)
						<cfif listfindnocase(bu,"All") eq 0>
							and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
						</cfif>
						<cfif listfindnocase(ou,"All") eq 0>
							and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
						</cfif>
						<cfif listfindnocase(prj,"All") eq 0>
						and Contractors.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#prj#" list="Yes">)
						</cfif>
						
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (Groups.Group_Number IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (Groups.Group_Number IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and Groups.Group_Number in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
						<!--- </cfif> --->
					ORDER BY ContractorNames.CoType, ContractorNames.ContractorName
					</cfquery>
					
					{mc{<select name="frmmgdcontractor"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "mgd"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}mc}
					
					{jv{<select name="frmjv"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "jv"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}jv}
					
					{su{<select name="frmsubcontractor"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect">
					<option value="all" selected>All</option>
					<cfoutput query="getcontractors"><cfif cotype eq "sub"><option value="#ContractorID#" >#ContractorName#</option></cfif></cfoutput>
					</select>}su}
					
					
					<cfquery name="getsites" datasource="#request.dsn#">
						SELECT        GroupLocations.GroupLocID, GroupLocations.SiteName, GroupLocations.isActive, NewDials.Name
						FROM            GroupLocations INNER JOIN
                         Groups ON GroupLocations.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID
						WHERE        (GroupLocations.isActive = 1)
						
						<cfif trim(prj) neq '' and listfindnocase(prj,"all") eq 0>
						and GroupLocations.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#prj#" list="Yes">)
						</cfif>
						<!--- <cfif fromdiamond eq "no"> --->
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
						<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
							</cfif>
						</cfif>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
							<cfif listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0>
							AND (GroupLocations.GroupNumber IN
						                             (SELECT        Group_Number
						                               FROM            Groups
						                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
								</cfif>
						</cfif>
						<cfif  listfindnocase(request.userlevel,"User") gt 0>
							<cfif trim(request.UserID) neq ''>
								and GroupLocations.GroupNumber in (SELECT DISTINCT Groups.Group_Number
								FROM            GroupUserAssignment INNER JOIN
								                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
								WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
							</cfif>
						</cfif>
					<!--- 	</cfif> --->
						ORDER BY NewDials.Name, GroupLocations.SiteName
						</cfquery>
							{ss{<select name="site"  size="5"   multiple="Yes"  class="selectgen" id="lrgselect" >
					<option value="all"  selected>All</option>
					<cfoutput query="getsites" group="name"><option value="" disabled>#name#</option><cfoutput><option value="#GroupLocID#" >&nbsp;&nbsp;#sitename#</option></cfoutput></cfoutput>
					</select>}ss}
					
			</cfif>
		</cfcase> 
	</cfswitch>
</cfif>