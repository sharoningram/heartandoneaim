<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="eventtype" default="all">
<cfparam name="WHEREHAPPEN" default="all">
<cfparam name="OBSERVERNAME" default="">
<cfparam name="STAKEHOLDER" default="all">
<cfparam name="SAFETYRULES" default="all">
<cfparam name="SAFETYESSENTIALS" default="all">
<cfparam name="STATUS" default="all">
<cfparam name="INCYEAR" default="">
<cfparam name="STARTDATE" default="">
<cfparam name="ENDDATE" default="">
<cfparam name="searchname" default="">
<cfparam name="searchtype" default="usersearch">
<cfparam name="ObservationNum" default="">

<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.heart_ds#">
		select UserSearchID
		from UserSearches
		where UserEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cookie.useremail#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
	</cfquery>
	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.heart_ds#">
			delete from UserSearches
			where UserSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.UserSearchID#">
		</cfquery>
	</cfif>
	
	
	
		<cfquery name="addsearch" datasource="#request.heart_ds#">
			insert into UserSearches  (UserEmail, BU, OU, BS, projectoffice, Site, EventType, WhereHappen, ObserverName, StakeHolder, SafetyRules, SafetyEssentials, Status, IncYear, StartDate, EndDate, SearchType, SearchName,ObservationNum)
			values  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cookie.useremail#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Site#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#EventType#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#WhereHappen#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ObserverName#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#StakeHolder#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#SafetyRules#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#SafetyEssentials#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Status#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#IncYear#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#StartDate#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#EndDate#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#SearchType#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#SearchName#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ObservationNum#">)
		</cfquery>
	
</cfif>
</cfif>