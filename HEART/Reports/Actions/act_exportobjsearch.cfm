<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportobjsearch.cfm", "exports"))>
<cfset thefilename = "ObservationSearch_#datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Observation Search Results","true")>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Observation Number",1,1);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,4);
	SpreadsheetSetCellValue(theSheet,"Event Type",1,5);
	SpreadsheetSetCellValue(theSheet,"Observation Date",1,6);
	SpreadsheetSetCellValue(theSheet,"Global Safety Rules",1,7);
	SpreadsheetSetCellValue(theSheet,"Safety Essentials",1,8);
	SpreadsheetSetCellValue(theSheet,"Status",1,9);
	SpreadsheetSetCellValue(theSheet,"Originator",1,10);
	SpreadsheetSetCellValue(theSheet,"Pending Action By",1,11);
</cfscript>

						<cfset ctr = 1>
						<cfoutput query="getincidents">
						
						
						
						<cfset ctr = ctr+1>
						<cfscript> 
							SpreadsheetSetCellValue(theSheet,"#TrackingYear##numberformat(TrackingNum,'0000')#",#ctr#,1);
							SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,2);
							SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,3);
							SpreadsheetSetCellValue(theSheet,"#Group_Name#",#ctr#,4);
							SpreadsheetSetCellValue(theSheet,"#ObservationType#",#ctr#,5);
							SpreadsheetSetCellValue(theSheet,"#dateformat(DateofObservation,sysdateformatxcel)#",#ctr#,6);
							SpreadsheetSetCellValue(theSheet,"#SafetyRule#",#ctr#,7);
							SpreadsheetSetCellValue(theSheet,"#SafetyEssential#",#ctr#,8);
							SpreadsheetSetCellValue(theSheet,"#dspstat#",#ctr#,9);
							SpreadsheetSetCellValue(theSheet,"#CreatedBy#",#ctr#,10);
							SpreadsheetSetCellValue(theSheet,"#pendingactby#",#ctr#,11);
						</cfscript>
						
						</cfoutput>
		
					
		 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.heartPath#/reports/exports/#thefilename#">
