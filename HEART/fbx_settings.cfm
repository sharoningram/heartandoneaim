<!--- Set application settings --->
<cfinclude template="appconfig.cfm">
<cfapplication 
	name="HEART" 
		clientmanagement="yes" 
		sessionmanagement="yes" 
		setclientcookies="yes" 
		sessiontimeout="#CreateTimeSpan(0, 10, 0, 0)#" 
		applicationtimeout="#CreateTimeSpan(0, 10, 0, 0)#">

<!--- Set default variables that will be used throughout the appication --->	
<cfparam name="attributes.fuseaction" default="main.main">
<cfparam name="request.self" default="index.cfm">
<cfparam name="self" default="index.cfm">
<cfparam name="request.isAdmin" default="0">
<cfparam name="request.userlevel" default="">
<cfparam name="request.fname" default="">
<cfparam name="request.lname" default="">
<cfparam name="request.userBUs" default="">
<cfparam name="request.userOUs" default="">
<cfparam name="request.userlogin" default="">
<cfparam name="request.UserID" default="0">
<cfparam name="request.Useremail" default="">
<cfset request.dsn = oneaimdsn>
<cfset request.HEART_DS = heartdsn>
<cfset request.commondsn = "common">

<!--- If the useremail cookie is not defined direct the user to the login script --->

<cfquery name="getappconfig" datasource="#request.dsn#">
	select  oneAIMpath, heartPath
	FROM            app_config
</cfquery>


<cfset request.bulabelshort = "BL">
<cfset request.bulabellong = "Business Line">
<cfset request.bulabellongnobr = "Business&nbsp;Line">
<cfset request.oulabelshort = "PG">
<cfset request.oulabellong = "Project Group">
<cfset request.oulabellongnobr = "Project&nbsp;Group">


<cfif not isdefined("cookie.useremail")>

		<cfoutput>
	 <form action="loginScripts/index.cfm" method="post" name="lfrm">
			<input type="hidden" name="qs" value="#query_string#">
		</form>
		<script type="text/javascript">
			document.lfrm.submit();
		</script> 
	</cfoutput>
	
	
<cfelse>

<!--- If the cookie is set check for the user's permissions. We check on every page load in case permissions change during the user's visit --->
<cfquery name="getrights" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, Users.Status, Users.Entered_By,  UserRoles.UserRole, 
                         UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cookie.useremail#">) AND (Users.Status = 1)
order by users.UserId
<!--- 	SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, Users.Status, Users.Entered_By, Users.Is_Admin, UserBusinessLines.BusinessLine
	FROM            Users LEFT OUTER JOIN
                         UserBusinessLines ON Users.UserId = UserBusinessLines.UserID
	WHERE        (Users.Useremail = ) and status = 1
	order by users.UserId --->
</cfquery>
<cfset request.userlogin = cookie.useremail>
<!--- Set the variables to values pulled from the query. If these values are left to default value the user will have view only right. --->
<cfif getrights.recordcount gt 0>
	<cfset request.userlevel = valuelist(getrights.UserRole)>
	<cfloop query="getrights" group="userid">
		
		<cfif UserRole eq "Global Admin">
			<cfset request.isAdmin = 1>
		</cfif>
		<cfset request.fname = Firstname>
		<cfset request.lname = Lastname>
		<cfset request.UserID = UserID>
		<cfset request.Useremail = Useremail>
		<!--- <cfset request.userlevel = listappend(request.userlevel,UserRole)> --->
		<cfloop query="getrights">
		<cfswitch expression="#userrole#">
			<cfcase value="Global Admin">
				<cfset request.isAdmin = 1>
			</cfcase>
			<cfcase value="BU Admin,Senior Reviewer">
				<!--- <cfloop query="getrights"> --->
					<cfset request.userBUs = listappend(request.userBUs,AssignedLocs)>
				<!--- </cfloop> --->
			</cfcase>
			<cfcase value="User,Reviewer,OU Admin,HSSE Advisor">
				<!--- <cfloop query="getrights"> --->
					<cfset request.userOUs = listappend(request.userOUs,AssignedLocs)>
				<!--- </cfloop> --->
			</cfcase>
		</cfswitch>
		</cfloop>
	</cfloop>
</cfif>

</cfif>
<cfif trim(request.userBUs) eq ''>
	<cfset request.userBUs = 0>
</cfif>
<cfif trim(request.userOUs) eq ''>
	<cfset request.userOUs = 0>
</cfif>

<cfset request.hsseadvlocs = 0>
<cfif  listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
<cfquery name="getsitesidsadv" datasource="#request.dsn#">
	SELECT        LocationID
     FROM         LocationAdvisors
     WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">)
</cfquery>
<cfset request.hsseadvlocs = valuelist(getsitesidsadv.LocationID)>
</cfif>
<cfif trim(request.hsseadvlocs) eq ''>
	<cfset request.hsseadvlocs = 0>
</cfif>
<!--- <cfoutput>#request.userlevel#</cfoutput> --->
<cfif trim(request.fname) eq ''>
	<cfif isdefined("cookie.userfname")>
		<cfset request.fname = cookie.userfname>
	</cfif>
</cfif>
<cfif trim(request.lname) eq ''>
	<cfif isdefined("cookie.userlname")>
		<cfset request.lname = cookie.userlname>
	</cfif>
</cfif>


<cfif trim(request.userlogin) neq ''>
	<cfif request.userlogin contains "@">
		<cfset empre = listgetat(request.userlogin,1,"@")>
		<cfif empre contains ".">
			<cfset empfname = listgetat(empre,1,".")>
			<cfset emplname = listgetat(empre,2,".")>
		<cfelse>
			<cfset empfname = empre>
			<cfset emplname = " ">
		</cfif>
		
		<cfif trim(request.fname) eq ''>
			<cfset request.fname = empfname>
		</cfif>
		<cfif trim(request.lname) eq ''>
			<cfset request.lname = emplname>
		</cfif>

	</cfif>

</cfif>


<cfset sysdateformatxcel = "dd-mmm-yy">
<cfset sysdateformat = "dd-mmmm-yyyy">
<cfset sysdatetimeformat = "dd-mmmm-yyyy HH:nn">
<cfset HeartEmail = heartemail>
<cfset TestEmailGroup = erroremail> 
<!--- <cfset TestEmailGroup ="liyakat.shaikh@amecfw.com,douglas.koppe@amecfw.com">  --->
<!--- <cfset TestEmailGroup ="liyakat.shaikh@amecfw.com,melanie.garside@amecfw.com,douglas.koppe@amecfw.com">  --->

<cfset allowedfilelist = "application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,image/tiff,application/pdf,application/x-pdf,text/plain,image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,application/x-zip-compressed,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,video/x-ms-wmv,image/png,application/zip,application/x-tika-msoffice,application/vnd.ms-outlook,message/rfc822,application/vnd.ms-word.document.macroenabled.12,application/vnd.ms-excel.sheet.macroenabled.12,text/csv,application/vnd.oasis.opendocument.text,application/vnd.ms-word.template.macroenabled.12,application/vnd.openxmlformats-officedocument.presentationml.template">

<cfif http_host eq "10.151.237.46" or http_host eq "10.151.237.117">
	<cfset starthttp = "http">
<cfelse>
	<cfset starthttp = "http">
</cfif>
<!--- Include error handling  --->
<!--- <cfoutput>#cookie.useremail# #request.userlevel#  #request.userOUs# #request.userBUs#</cfoutput> --->
<cferror type="EXCEPTION" mailto="#erroremail#" template="errorpage.cfm"> 



