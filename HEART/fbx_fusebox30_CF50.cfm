<cfsetting enablecfoutputonly="yes"><cfprocessingdirective suppresswhitespace="Yes">


<cfscript>
/*******************************************************************************************
SECTION TWO
The fusebox structure below is a structure encompassing the public Fusebox API. We recommend making no changes to this structure as it will render your application non-compliant to the Fusebox 3.0 standard.

	fusebox.IsCustomTag:
The boolean variable is set by the Fusebox framework which will automatically determine if it is being called as a custom tag. Currently, Fusebox offers to expanded support for applications being called as Custom Tags. But you can programatically alter your application if it's being called as Custom Tag by checking the value of this variable. This can be helpful for changing (or removing altogether) layout files in fbx_layouts.cfm.
	fusebox.IsHomeCircuit:
This boolean variable is set and re-set as the Fusebox framework does its business pulling in fbx_settings.cfm files and the fbx_switch.cfm file and fbx_layouts.cfm files (and associated layout files). It is TRUE only when the currently accessed circuit is the home circuit running this application.
	fusebox.IsTargetCircuit:
Like isHomeCircuit above, this boolean variable is used during the process of cfincluding files. It is TRUE only when the currently accessed circuit is the target circuit running specified by circuit.fuseaction.
	fusebox.Circuit:
This is the first part of the compound fuseaction that gets passed as attributes.fuseaction.
	fusebox.Fuseaction:
This is the second part of a compound fuseaction that gets passed as attributes.fuseaction. fusebox.fuseaction is the variable expression evaluated in fbx_switch.cfm.
	fusebox.HomeCircuit:
This variable is set to the root-level circuit as defined in fusebox.circuits strucure.
	fusebox.TargetCircuit:
This is the circuit the requested fuseaction is to run in. The difference between this variable and fusebox.circuit above, is that this variable is the circuit alias that was found in the fusebox.circuits file as opposed to the circuit that is being attempted to be found. In all non-error situations fusebox.TargetCircuit and fusebox.Circuit will be the same.
	fusebox.ThisCircuit:
Like IsTargetCircuit and IsHomeCircuit above, this variable is set and re-set during the process of running the fbx_settings.cfm files and the fbx_switch.cfm file, and refers to the circuit alias of the circuit from which files are currently being accessed.
	fusebox.ThisLayoutPath:
This is the directory path that the layout file being used is called from. This variable changes as the layouts are nested one inside another, building the overall page layout.
	fusebox.suppressErrors:
A boolean variable, which defaults to FALSE. If TRUE, the Fusebox framework will attempt to give you "smarter" errors that may occur from within its own code as it applies to the Framework itself. If FALSE (default), you will receive the native CF error messages. During development you may want to turn this to TRUE and FALSE alternately to ensure you've got your framework set up properly. Set this to TRUE in a production enviroment, since at that point errors that occur will not be Fusebox framework errors but rahter erros in your fuseactions and fuses.	
	fusebox.Circuits:
This variable is a structure whose aliases are the circuit names created in fbx_circuits.cfm and whose values are the directory paths to those circuits.
	fusebox.currentPath:
This variable takes you from the root circuit to any location it is called. If you use images in directories beneath individual circuits, this variable will point to that circuit like "directory/directory/".
	fusebox.rootPath:
This variable takes you from the circuit it is being called from, back to the root. This is helpful to determine your location relative to the root application.
*******************************************************************************************/
fusebox = structNew();
if (findNoCase("cf_", "," & getBaseTagList()))
	fusebox.IsCustomTag=TRUE;
else fusebox.IsCustomTag=FALSE;
fusebox.IsHomeCircuit=FALSE;
fusebox.IsTargetCircuit=FALSE;
fusebox.fuseaction="";
fusebox.circuit="";
fusebox.HomeCircuit="";
fusebox.TargetCircuit="";
fusebox.thisCircuit="";
fusebox.thislayoutpath="";
fusebox.suppressErrors=FALSE;
fusebox.Circuits=structNew();
fusebox.currentPath="";
fusebox.rootPath="";

/*******************************************************************************************
SECTION THREE
FB_ is a structure encompassing "private" variables used by the underlying Fusebox framework. Make no changes to it without a full understanding of the ramifications of those changes. 
*******************************************************************************************/
FB_=structNew();

/*******************************************************************************************
SECTION FOUR
This code used to be in a Custom Tag called formURL2attributes.cfm. It copies all incoming FORM and URL variables to ATTRIBUTES scope.
*******************************************************************************************/
if (NOT IsDefined("attributes"))
    attributes=structNew();
StructAppend(attributes, url, "no");
StructAppend(attributes, form, "no");
</cfscript>

<cftry>
   <cfinclude template="fbx_Circuits.cfm"> 
	<cfcatch>
		<cfif fusebox.suppressErrors>
			<cfoutput>The Fusebox framework could not find the file fbx_Circuits.cfm. If you think this error is incorrect, turn off the Fusebox suppress error messages flag by setting fusebox.SuppressErrors to FALSE, and you will receive ColdFusion's "normal" error output.</cfoutput><cfabort>
		<cfelse><cfrethrow></cfif>
	</cfcatch>
</cftry>

<cftry>
	<cfscript>
	FB_.ReverseCircuitPath=StructNew();
	for (aCircuitName in fusebox.Circuits){
		FB_.ReverseCircuitPath[fusebox.Circuits[aCircuitName]]=aCircuitName;
		if (ListLen(fusebox.Circuits[aCircuitName], "/") EQ 1){
			fusebox.HomeCircuit=aCircuitName;
			fusebox.IsHomeCircuit=TRUE;}}
	</cfscript>
	<cfcatch>
		<cfif fusebox.suppressErrors>
			<cfoutput>The circuits structure does not exist. This must be defined in the Fusebox application's root fbx_circuits.cfm file. If you think this error is incorrect, turn off the Fusebox suppress error messages flag by setting fusebox.SuppressErrors to FALSE, and you will receive ColdFusion's "normal" error output.</cfoutput><cfabort>
		<cfelse><cfrethrow></cfif>
	</cfcatch>
</cftry>

<cftry>
	<cfinclude template="fbx_Settings.cfm">
	<cfcatch>
		<cfif fusebox.suppressErrors>
			<cfoutput>The Fusebox framework could not find the file fbx_Settings.cfm. If you think this error is incorrect, turn off the Fusebox suppress error messages flag by setting fusebox.SuppressErrors to FALSE, and you will receive ColdFusion's "normal" error output.</cfoutput><cfabort>
		<cfelse><cfrethrow></cfif>
	</cfcatch>
</cftry>

<cftry>
	<cfscript>
	FB_.rawFA = attributes.fuseaction; //preserve the original fuseaction
	if (ListLen(FB_.rawFA, '.') is 1 and Right(FB_.rawFA,1) is '.')
		//circuit only specified, no fuseaction such as "fuseaction=circuit."
		fusebox.fuseaction = "fusebox.defaultfuseaction";
	else
		fusebox.fuseaction = ListGetAt( FB_.rawFA, 2,  '.' );
	fusebox.circuit = ListGetAt( FB_.rawFA, 1, '.');
	fusebox.TargetCircuit=fusebox.circuit; //preserve for later
	</cfscript>
	<cfcatch>
		<cfif not fusebox.suppressErrors>
			<cfoutput>The variable "attributes.fuseaction" is not available or the Fusebox framework could not find the circuit you requested: "#fusebox.circuit#". If you think this error is incorrect, turn off the Fusebox suppress error messages flag by setting fusebox.SuppressErrors to FALSE, and you will receive ColdFusion's "normal" error output.</cfoutput><cfabort>
		<cfelse><cfrethrow></cfif>
	</cfcatch>
</cftry>

<cfscript>
FB_.fullPath=ListRest(fusebox.Circuits[fusebox.TargetCircuit], "/"); //make a variable to hold the full path down to the target, excluding the root
FB_.Corepath=""; //initialize
fusebox.thisCircuit=fusebox.HomeCircuit; //current circuit, set to root now
</cfscript>
<cfloop list="#FB_.fullpath#" index="aPath" delimiters="/">
	<cfscript>
	FB_.Corepath=ListAppend(FB_.Corepath, aPath, "/"); //add the current circuit with / as delim
	fusebox.IsHomeCircuit=FALSE; //fbx_settings.cfm files included in this loop are not the home circuit because the home circuit's fbx_Settings is needed much earlier in the process
	fusebox.currentPath=FB_.Corepath & "/";
	fusebox.rootPath=repeatString("../", ListLen(fusebox.currentPath, '/'));
	</cfscript>
	<cftry>
		<cfif StructKeyExists(FB_.ReverseCircuitPath, fusebox.Circuits[fusebox.HomeCircuit] & "/" & FB_.CorePath)>
			<cfset fusebox.thisCircuit=FB_.ReverseCircuitPath[fusebox.Circuits[fusebox.HomeCircuit] & "/" & FB_.CorePath] > 
			<cfif fusebox.thisCircuit EQ fusebox.TargetCircuit>
				<cfset fusebox.IsTargetCircuit=TRUE>
			<cfelse>
				<cfset fusebox.IsTargetCircuit=FALSE>
			</cfif>
			<cfinclude template="#fusebox.currentPath#fbx_Settings.cfm">
		</cfif>
		<cfcatch>
			<cfif fusebox.suppressErrors>
			<cfelse><cfrethrow></cfif>
		</cfcatch>		
	</cftry>
</cfloop>

<cfscript>
fusebox.thisCircuit=fusebox.TargetCircuit;
fusebox.IsTargetCircuit= TRUE;
FB_.fuseboxpath=FB_.fullpath; //make directory path to the target circuit
if (Len(FB_.fuseboxpath)){
	//if the target circuit is NOT the root circuit
	FB_.fuseboxpath=FB_.fuseboxpath & "/";
	fusebox.IsHomeCircuit = FALSE;}
else
	fusebox.IsHomeCircuit = TRUE;
fusebox.currentPath=fb_.fuseboxpath;
fusebox.rootPath=repeatString("../", ListLen(fb_.fuseboxpath, '/'));
</cfscript>
<cftry>
	<cfsavecontent variable="fusebox.layout">
		<cfoutput><cfinclude template="#FB_.fuseboxpath#fbx_Switch.cfm"></cfoutput>
	</cfsavecontent>
	<cfcatch>
		<cfif fusebox.suppressErrors>
			<cfoutput>I could not find #FB_.fuseboxpath#fbx_Switch.cfm (or one of its components such as an included fuse) in the "#fusebox.circuit#" circuit. If you think this error is incorrect, turn off the Fusebox suppress error messages flag by setting fusebox.SuppressErrors to FALSE, and you will receive ColdFusion's "normal" error output.</cfoutput><cfabort>
		<cfelse><cfrethrow></cfif>
	</cfcatch>
</cftry> 


<cfset FB_.circuitalias = fusebox.Circuits[fusebox.TargetCircuit] >
<cfset FB_.layoutpath = fusebox.Circuits[fusebox.TargetCircuit] >
<cfloop condition="Len(FB_.layoutpath) GT 0"> 
	<cfif StructKeyExists(FB_.ReverseCircuitPath, FB_.circuitalias)>
		<cftry>
			<cfset fusebox.thisCircuit = FB_.ReverseCircuitPath[FB_.circuitalias] >
			<cfcatch>
				<cfset fusebox.thisCircuit = "">
				<cfif fusebox.suppressErrors>
				<cfelse><cfrethrow></cfif>
			</cfcatch>
		</cftry>
		<cfscript>
		if (fusebox.thisCircuit EQ fusebox.Targetcircuit) fusebox.IsTargetCircuit=TRUE;
			else fusebox.IsTargetCircuit=FALSE;
		if (fusebox.thisCircuit EQ fusebox.HomeCircuit)	fusebox.IsHomeCircuit=TRUE;
			else fusebox.IsHomeCircuit=FALSE;
		fusebox.ThisLayoutPath=ListRest(FB_.layoutpath,"/");
		if (Len(fusebox.thislayoutpath)) fusebox.thislayoutpath=fusebox.thislayoutpath & "/";
		fusebox.currentPath=fusebox.thislayoutpath;
		fusebox.rootPath=repeatString("../", ListLen(fusebox.thislayoutpath, '/'));
		</cfscript>
	 	<cftry> 
			
			<cfinclude template="#fusebox.thislayoutpath#fbx_Layouts.cfm">
	 		<cfcatch>
				<cfset fusebox.layoutfile = ""><cfset fusebox.layoutdir = "">
			</cfcatch>
	 	</cftry>
		<cftry>
			
			<cfif Len(fusebox.layoutfile)>
				<cfsavecontent variable="fusebox.layout">	
					<cfoutput><cfinclude template="#fusebox.thislayoutpath##fusebox.layoutdir##fusebox.layoutfile#"></cfoutput>
				</cfsavecontent>
			</cfif>
		<cfcatch>
			<cfif fusebox.suppressErrors>
				<cfoutput>I could not find the layoutfile #fusebox.thislayoutpath##fusebox.layoutdir##fusebox.layoutfile# specified by #fusebox.thislayoutpath#fbx_Layouts.cfm. If you think this error is incorrect, turn off the Fusebox suppress error messages flag by setting fusebox.SuppressErrors to FALSE, and you will receive ColdFusion's "normal" error output.</cfoutput><cfabort>
			<cfelse><cfrethrow></cfif>
		</cfcatch>
		</cftry>
	</cfif>
	<cfset FB_.layoutpath = ListDeleteAt(FB_.layoutpath, ListLen(FB_.layoutpath, "/"), "/")>
	<cfset FB_.circuitalias = ListDeleteAt(FB_.circuitalias, ListLen(FB_.circuitalias, "/"), "/")>
</cfloop>

<cfoutput>#trim(fusebox.layout)#</cfoutput>

</cfprocessingdirective><cfsetting enablecfoutputonly="no">