<cfparam name="request.fname" default="First">
<cfparam name="request.lname" default="Last">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html>
<head>
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	
	<meta name="description" content="This is the Responsive Grid System, a quick, easy and flexible way to create a responsive web site.">
	<meta name="keywords" content="responsive, grid, system, web design">

	<meta name="author" content="www.grahamrobertsonmiller.co.uk">

	<meta http-equiv="cleartype" content="on">

	<link rel="shortcut icon" href="/favicon.ico">

	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="shared/css/html5reset.css" media="all">
	<!-- <link rel="stylesheet" href="shared/css/responsivegridsystem.css" media="all"> -->
	<link rel="stylesheet" href="shared/css/col.css" media="all">
	<link rel="stylesheet" href="shared/css/2cols.css" media="all">
	<link rel="stylesheet" href="shared/css/3cols.css" media="all">
	<link rel="stylesheet" href="shared/css/4cols.css" media="all">
	<link rel="stylesheet" href="shared/css/5cols.css" media="all">
	<link rel="stylesheet" href="shared/css/6cols.css" media="all">
	<link rel="stylesheet" href="shared/css/7cols.css" media="all">
	<link rel="stylesheet" href="shared/css/8cols.css" media="all">
	<link rel="stylesheet" href="shared/css/9cols.css" media="all">
	<link rel="stylesheet" href="shared/css/10cols.css" media="all">
	<link rel="stylesheet" href="shared/css/11cols.css" media="all">
	<link rel="stylesheet" href="shared/css/12cols.css" media="all">

	<!-- Responsive Stylesheets -->
	<link rel="stylesheet" media="only screen and (max-width: 1024px) and (min-width: 769px)" href="/css/1024.css">
	<link rel="stylesheet" media="only screen and (max-width: 768px) and (min-width: 481px)" href="/css/768.css">
	<link rel="stylesheet" media="only screen and (max-width: 480px)" href="/css/480.css">

	<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
	<script src="shared/js/modernizr-2.5.3-min.js"></script>
	<title>HSSE Sample 2: Top Menu</title>
<style>
#leftnav {
    float: left;
    width: 10%;
    height: 100%;
    background: #cfcfcf; 
    }
#midstripe {
 background: #10b2ba;
	width: 1%;
  float: left;
  height: 100%;
}
#bodycontent {
   
    background: #ffffff;
	padding-top: 4px;
	padding-bottom: 4px;
	padding-left: 12px;
	padding-right: 12px;
    }
	
.stripe1 {
    width: 100%;
    
    background: #5f2167;
	font-family:Segoe UI;
	font-size:12pt;
	color:#ffffff;
	padding-top: 8px;
	padding-bottom: 8px;
	      }
.stripe2 {
    width: 100%;
    height: 33;
    background: #cfcfcf;
    }
	.stripe3 {
    width: 100%;
    height: 2;
    background: #cfdf24;
    }
.leftstrong {
	font-family:Segoe UI;
	font-size:11pt;
	color:#5f2167;
	padding-left: 1px;
}
.leftlink {
	font-family:Segoe UI;
	font-size:10pt;
	color:#5f2167;
	text-decoration:none;
	padding-left: 8px;
	
}
.leftlink:hover {
	text-decoration:underline;
}


</style>
</head>
<!--- cfdf24 --->
<body leftmargin="0" topmargin="0">

<!--- <div align="left"><img src="images/logo.jpg"></div> --->
<div class="stripe1" align="right">Hello <cfoutput>#request.fname# #request.lname#</cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;</div>
<div class="stripe2" align="center"><!--- <cfinclude template="topnav.cfm"> --->
<script type="text/javascript">
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose;
</script>	
	
<style>
#sddm
{	margin: 0;
	padding: 0;
	z-index: 30;
	padding-top: 3px;
	float: center}

#sddm li
{	margin: 0;
	padding-left: 10;
	padding-right: 10;
	list-style: none;
	float: left;
	font:  12pt Segoe UI}

#sddm li a
{	display: block;
	margin: 0 1px 0 0;
	padding: 4px 10px;
	
	
	color: #5f2167;
	text-align: center;
	text-decoration: none}



#sddm div
{	position: absolute;
	visibility: hidden;
	margin: 0;
	padding-top:4px;
	
	background: #cfcfcf;
	border: 1px solid #cfcfcf}

	#sddm div a
	{	position: relative;
		display: block;
		margin: 0;
		padding: 5px 10px;
		width: auto;
		white-space: nowrap;
		text-align: left;
		text-decoration: none;
		background: #ffffff;
		color: #5f2167;
		font: 12pt Segoe UI}

	#sddm div a:hover
	{	background: #cfdb00;
		color: #5f2167}


	
	
</style>
<table><tr><td>
<div id="wrapper">
	
	<div id="maincontentcontainer">
				<div class="section group">
					<div class="col span_1_of_2">
<ul id="sddm">
    <li><a href="#" 
        onmouseover="mopen('m1')" 
        onmouseout="mclosetime()">Incidents</a>
        <div id="m1" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
        <a href="#">First Incident</a>
        <a href="#">Update Incidents</a>
        <a href="#">Search Incidents</a>
        </div>
    </li>
    <li><a href="#" 
        onmouseover="mopen('m2')" 
        onmouseout="mclosetime()">Project Groups</a>
        <div id="m2" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
        <a href="#">Add Project Group</a>
        <a href="#">Update/Disable Project Group</a>
        <a href="#">Assign Contracts to Group</a>
        <a href="#">Re-Assign Contracts</a>
		<a href="#">Dial Admin</a>
        </div>
    </li>
    <li><a href="#" onmouseover="mopen('m3')" 
        onmouseout="mclosetime()">Users</a>
		<div id="m3" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
			<a href="#">Add/Update/Delete User</a>
			<a href="#">Update Groups by User</a>
		</div>
	</li>
	</ul>
	</div>
	<div class="col span_2_of_2" >
	<ul id="sddm">
    <li><a href="#" onmouseover="mopen('m4')" 
        onmouseout="mclosetime()">Distribution</a>
		<div id="m4" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
			<a href="#">Add Email Group</a>
			<a href="#">Update Email Group</a>
			<a href="#">Assign Users to Group</a>
		</div>
	
	</li>

    <li><a href="#" onmouseover="mopen('m5')" 
        onmouseout="mclosetime()">Labor</a>
		<div id="m5" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
			<a href="#">Labor Manhours</a>
		</div></li>
	 <li><a href="#" onmouseover="mopen('m6')" 
        onmouseout="mclosetime()">Reports</a>
	 <div id="m6" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
			<a href="#">Dashboard</a>
			<a href="#">Charts</a>
			<a href="#">Executive Summary</a>
			<a href="#">Client Reports</a>
			<a href="#">Severity/Risk Matrix</a>
			<a href="#">Mining the Diamond</a>
			<a href="#">Manhours</a>
			<a href="#">Monthly Performance</a>
			<a href="#">Dashboard History</a>
			<a href="#">Project Summary</a>
			<a href="#">Surface & Root Cause</a>
			<a href="#">Incident Knowledge DB</a>
			<a href="#">Safety Alerts</a>
			<a href="#">Weekly Executive Report</a>
			<a href="#">Incident Review Board</a>
		</div></li>
</ul>
</div></div></div></div></td></tr></table>
</div>