<cfset titleaddon = "User Management">
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="main">
		<cfset titleaddon = titleaddon & " - Add/Update User Access">
	</cfcase>
</cfswitch>
<cfoutput>
<cfif request.isAdmin>
<cfset irbgcol = "e8bdf9">
<cfset irstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<cfset irvalign = "middle">
<cfset irtabs = "sm">
<cfparam name="groupid" default="0">
<cfif fusebox.fuseaction neq "buildoulistslookup">
	<div class="stripe1"><table width="100%" cellpadding="0" cellspacing="0" border="0">

<tr><td>
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr>
	<td style="font-family:Segoe UI;color:ffffff;font-size:12pt;">
&nbsp;<a href="/oneAIM" style="color:ffffff;text-decoration:none;">User Access Management</a></td><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;"><cfif trim(request.fname) neq ''>Welcome <cfoutput>#request.fname# #request.lname#</cfoutput>&nbsp;&nbsp;</cfif></td></tr>
</table></td></tr>
<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>

<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>
</table>
</div>
	<table cellpadding="0" border="0" cellspacing="0" width="75%" id="mytab">
	<tr>
		<td><img src="images/spacer.gif" height="30" border="0"></td>
	</tr>
	<tr>
	<td>
		<table  cellpadding="0" cellspacing="0">
		<cfoutput>
			<tr>
				<cfif user eq 0>
				<td bgcolor="e4c9ff"><img src="images/tableft.png" border="0"></td>
				<td bgcolor="e4c9ff" style="#irstyle#" >Add&nbsp;User</td>
				<td bgcolor="e4c9ff"><img src="images/tabright.png" border="0"></td>
				<cfelse>
				
				<td bgcolor="e4c9ff"><img src="images/tableftsm.png" border="0"></td>
				<td bgcolor="e4c9ff" background="images/smtabbg.png"><a href="index.cfm?fuseaction=users.main" style="color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">Add&nbsp;User</a></td>
				<td bgcolor="e4c9ff"><img src="images/tabrightsm.png" border="0"></td>
				<td bgcolor="e5f49a"><img src="images/tableft.png" border="0"></td>
				<td bgcolor="e5f49a" style="#irstyle#" >Update&nbsp;User</td>
				<td bgcolor="e5f49a"><img src="images/tabright.png" border="0"></td>
				</cfif>
				
				<td align="right" width="100%" style="font-family:Segoe UI;font-size:9pt;" valign="bottom"><a href="/oneAIM" style="font-family:Segoe UI;font-size:10px;text-decoration:none;color:5f2468;"><<&nbsp;Back&nbsp;to&nbsp;HSSE</a></td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
<tr>
<td>#Fusebox.layout#</td></tr></table>
<cfelse>
#Fusebox.layout#
</cfif>
</cfif>
</cfoutput>