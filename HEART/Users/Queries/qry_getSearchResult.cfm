<cfparam name="sfirstname" default="">
<cfparam name="slastname" default="">
<cfparam name="sUseremail" default="">
<cfparam name="sstatus" default="">
<cfparam name="sisadmin" default="">
<cfparam name="sishsselead" default="">
<cfparam name="submittype" default="">
<cfif trim(submittype) eq "search"><!--- SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, Users.Status, Users.Entered_By, Users.Is_Admin, Users.ishsselead,UserBusinessLines.BusinessLine
FROM            Users LEFT OUTER JOIN
                         UserBusinessLines ON Users.UserId = UserBusinessLines.UserID --->
<cfquery name="getusersearch" datasource="#request.dsn#">
SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, Users.Status, Users.Entered_By, Users.Is_Admin, Users.IsHSSElead, Users.useradmin, 
                         UserRoles.UserRole, UserRoles.AssignedLocs
FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID						
WHERE    
<cfif sstatus eq "all">Users.status in (0,1)<cfelseif trim(sstatus) neq ''>Users.status = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#sstatus#"><cfelse>users.status in (0,1)</cfif>

<!--- and UserBusinessLines.BusinessLine in  (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="yes">) --->
<cfif trim(sfirstname) neq ''>
and Users.Firstname like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#sfirstname#%">
</cfif>

<cfif trim(slastname) neq ''>
and Users.Lastname like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#slastname#%">
</cfif>

<cfif trim(sUseremail) neq ''>
and Users.Useremail like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#sUseremail#%">
</cfif>

<!--- <cfif sisadmin eq "all"> and Users.is_admin in (0,1)<cfelseif trim(sisadmin) neq ''> and Users.is_admin = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#sisadmin#"><cfelse> and users.is_admin in (0,1)</cfif>

<cfif sishsselead eq "all"> and Users.IsHSSElead in (0,1)<cfelseif trim(sishsselead) neq ''> and Users.IsHSSElead = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#sishsselead#"><cfelse>and users.IsHSSElead in (0,1)</cfif> --->
order by  Users.Lastname,Users.Firstname,UserRoles.UserRole
</cfquery>

</cfif>