<cfquery name="getusersearch" datasource="#request.dsn#">
SELECT     top 25  Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, Users.Status, Users.Entered_By, Users.Is_Admin, UserBusinessLines.BusinessLine
FROM            Users LEFT OUTER JOIN
                         UserBusinessLines ON Users.UserId = UserBusinessLines.UserID
						
WHERE  Users.Lastname like '%e%'
</cfquery>
<cfquery name="getbusinesslines" datasource="#request.dsn#">
SELECT        Name, businessline
FROM            NewDials
WHERE        (Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (isgroupdial = 0)
</cfquery>

<cfset ublstruct = {}>
<cfloop query="getbusinesslines">
	<cfif not structkeyexists(ublstruct,businessline)>
		<cfset ublstruct[businessline] = name>
	</cfif>
</cfloop>
<script src="/oneAIM/shared/js/jquery-1.8.3.min.js"></script>
<style>
#mytab2 th { font-weight: bold;font-family:Segoe UI;font-size:11pt;color:#5f2167; }
#mytab2 td, #mytab2 th { padding: 4px 4px; text-align: left; }
#mytab2 td {font-family:Segoe UI;font-size:10pt; }
/* Mobile */
@media only screen and (max-width: 767px) {
	
	#mytab2.responsive { margin-bottom: 0; }
	
	.pinned { position: absolute; left: 0; top: 0; background: #fff; width: 45%; overflow: hidden; overflow-x: scroll; border-right: 1px solid #ccc; border-left: 1px solid #ccc; }
	.pinned #mytab2 { border-right: none; border-left: none; width: 100%; }
	.pinned #mytab2 th, .pinned #mytab2 td { white-space: nowrap; }
	.pinned td:last-child { border-bottom: 0; }
	
	div.table-wrapper { position: relative; margin-bottom: 20px; overflow: hidden; border-right: 1px solid #ccc; }
	div.table-wrapper div.scrollable #mytab2 { margin-left: 45%; }
	div.table-wrapper div.scrollable { overflow: scroll; overflow-y: hidden; }	
	
	#mytab2.responsive td, #mytab2.responsive th { position: relative; white-space: nowrap; overflow: hidden; }
	#mytab2.responsive th:first-child, #mytab2.responsive td:first-child, #mytab2.responsive td:first-child, #mytab2.responsive.pinned td { display: none; }
	
}
</style>


<script src="/oneAIM/shared/js/responsive-tables.js"></script>
<div style="border:1px solid;width:100%;border-color:#5f2167" >
<div class="purplebg" style="color:#ffffff;width:100%;font-family:Segoe UI;font-weight:bold;font-size:12pt;text-align:left;padding: 2px 2px;">Table Title</div> 
<table cellpadding="3" cellspacing="0" border="0" class="responsive" id="mytab2" width="100%">
		
			<tr>
				<th align="center">Name</th>
				<th align="center">Email</th>
				<th align="center">Status</th>
				<th align="center">Admin</th>
				<th align="center">Business Line</th>
			</tr>
	
	
	<cfset ctr = 0>
	<cfoutput query="getusersearch" group="userid"><cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>class="whitebg"<cfelse>class="ltTeal"</cfif>>
		<td class="bodytext" ><a href="javascript:void(0);" onclick="document.editusersearch.user.value=#userid#;document.editusersearch.submit();">#lastname#, #firstname#</a></td>
		<td class="bodytext">#Useremail#</td>
		<td class="bodytext" nowrap align="center"><cfif status neq 1>In-</cfif>Active</td>
		<td class="bodytext" align="center"><cfif Is_Admin>Yes<cfelse>No</cfif></td>
		<td class="bodytext" nowrap><cfoutput><cfif structkeyexists(ublstruct,businessline)>#ublstruct[BusinessLine]#<br></cfif></cfoutput></td>
	</tr>
	</cfoutput>

</table>
</div>
<!--- 
<table cellpadding="0" cellspacing="0" border="1" class="responsive" id="mytab2" width="800">
			<tr>
				<th align="center" width="160">Name</th>
				<th align="center" width="220">Email</th>
				<th align="center" width="80">Status</th>
				<th align="center" width="80">Admin</th>
				<th align="center" width="220">Business Line</th>
			</tr>
	
	
	<cfset ctr = 0>
	<cfoutput query="getusersearch" group="userid"><cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>class="whitebg"<cfelse>class="ltTeal"</cfif>>
		<td class="selectgen" width="160"><a href="javascript:void(0);" onclick="document.editusersearch.user.value=#userid#;document.editusersearch.submit();">#lastname#, #firstname#</a></td>
		<td colspan="4" width="640">
		<table border="1" width="640" cellpadding="0" cellspacing="0">
		<tr>
			<td  width="220">#Useremail#</td>
			<td   nowrap align="left" width="100"><cfif status neq 1>In-</cfif>Active</td>
			<td   align="left" width="100"><cfif Is_Admin>Yes<cfelse>No</cfif></td>
			<td   nowrap width="220"><cfoutput><cfif structkeyexists(ublstruct,businessline)>#ublstruct[BusinessLine]#<br></cfif></cfoutput></td>
		</tr>
		</table>
		</td>
	</tr>
	</cfoutput>

</table>
 --->
<!--- <table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="98%" >
<tr>
		<td colspan="4"><strong class="bodyTextWhite">Search Results</strong></td>
	</tr>
	<tr>
		<td>  ---><!--- 
<table class="responsive" bgcolor="ffffff" id="mytab2">
<tr>
<th>Header 1</th>
<th>Header 2</th>
<th>Header 3</th>
<th>Header 4</th>
<th>Header 5</th>
<th>Header 6</th>
<th>Header 7</th>
<th>Header 8</th>
</tr>
<tr>
<td>row 1, cell 1</td>
<td>row 1,<br>cell 2</td>
<td>row 1, cell 3</td>
<td>row 1, cell 4</td>
<td>row 1, cell 5</td>
<td>row 1, cell 6</td>
<td>row 1, cell 7</td>
<td>row 1, cell 8</td>
</tr>
<tr>
<td>row 2, cell 1</td>
<td>row 2, cell 2</td>
<td>row 2, cell 3</td>
<td>row 2,<br>cell<br>4</td>
<td>row 2, cell 5</td>
<td>row 2, cell 6</td>
<td>row 2, cell 7</td>
<td>row 2, cell 8</td>
</tr>
<tr>
<td>row 3, cell 1</td>
<td>row 3, cell 2</td>
<td>row 3, cell 3</td>
<td>row 3, cell 4</td>
<td>row 3, cell 5</td>
<td>row 3, cell 6</td>
<td>row 3, cell 7</td>
<td>row 3, cell 8</td>
</tr>
<tr>
<td>row 4, cell 1</td>
<td>row 4, cell 2</td>
<td>row 4, cell 3</td>
<td>row 4, cell 4</td>
<td>row 4, cell 5</td>
<td>row 4, cell 6</td>
<td>row 4, cell 7</td>
<td>row 4, cell 8</td>
</tr>
</table> --->
<!--- </td></tr></table> --->
<!--- 
<table class="responsive">
<tbody>
<tr>
<th>Perk</th>
<th>Description</th>
<th>ID</th>
<th>Skill Req</th>
<th>Perk Req</th>
</tr>
<tr>
<td>Steel Smithing</td>
<td>Can create Steel armor and weapons at forges,<br>and improve them twice as much.</td>
<td>000cb40d</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Arcane Blacksmith</td>
<td>You can improve magical weapons and armor.</td>
<td><span style="font-size: x-small" class="idref">0005218e</span></td>
<td>60 Smithing</td>
<td>Steel Smithing</td>
</tr>
<tr>
<td>Dwarven Smithing</td>
<td>Can create Dwarven armor and weapons at forges, and improve them twice as much.</td>
<td>000cb40e</td>
<td>30 Smithing</td>
<td>Steel Smithing</td>
</tr>
<tr>
<td>Orcish Smithing</td>
<td>Can create Orcish armor and weapons at forges, and improve them twice as much.</td>
<td>000cb410</td>
<td>50 Smithing</td>
<td>Dwarven Smithing</td>
</tr>
<tr>
<td>Ebony Smithing</td>
<td>Can create Ebony armor and weapons at forges<br>, and improve them twice as much.</td>
<td>000cb412</td>
<td>80 Smithing</td>
<td>Orcish Smithing</td>
</tr>
<tr>
<td>Daedric Smithing</td>
<td>Can create Daedric armor and weapons at forges, and improve them twice as much.</td>
<td>000cb413</span></td>
<td>90 Smithing</td>
<td>Ebony Smithing</td>
</tr>
<tr>
<td>Elven Smithing</td>
<td>Can create Elven armor and weapons at forges, and improve them twice as much.</td>
<td>000cb40f</td>
<td>30 Smithing</td>
<td>Steel Smithing</td>
</tr>
<tr>
<td>Advanced Armors</td>
<td>Can create Scaled and Plate armor at forges, and improve them twice as much.
<td>000cb414</td>
<td>50 Smithing</td>
<td>Elven Smithing</td>
</tr>
<tr>
<td>Glass Smithing</td>
<td>Can create Glass armor and weapons at forges, and improve them twice as much.</td>
<td>000cb411</td>
<td>70 Smithing</td>
<td>Advanced Armors</td>
</tr>
<tr>
<td>Dragon Armor</td>
<td>Can create Dragon armor at forges, and improve them twice as much.</td>
<td>00052190</td>
<td>100 Smithing</td>
<td>Daedric Smithing or Glass Smithing</td>
</tr>
</tbody>
</table> --->


<!--- 		<table cellpadding="3" cellspacing="0" border="0" class="responsive">
			<tr class="whitebg">
				<td align="center" ><strong class="selectgen">Name</strong></td>
				<td align="center"><strong class="selectgen">Email</strong></td>
				<td align="center"><strong class="selectgen">Status</strong></td>
				<td align="center"><strong class="selectgen">Admin</strong></td>
				<td align="center"><strong class="selectgen">Business Line</strong></td>
			</tr>
	
	<tbody>
	<cfset ctr = 0>
	<cfoutput query="getusersearch" group="userid"><cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>class="whitebg"<cfelse>class="ltTeal"</cfif>>
		<td class="selectgen" nowrap><a href="javascript:void(0);" onclick="document.editusersearch.user.value=#userid#;document.editusersearch.submit();">#lastname#, #firstname#</a></td>
		<td class="selectgen">#Useremail#</td>
		<td class="selectgen" nowrap align="center"><cfif status neq 1>In-</cfif>Active</td>
		<td class="selectgen" align="center"><cfif Is_Admin>Yes<cfelse>No</cfif></td>
		<td class="selectgen" nowrap><cfoutput><cfif structkeyexists(ublstruct,businessline)>#ublstruct[BusinessLine]#<br></cfif></cfoutput></td>
	</tr>
	</cfoutput>
	</tbody>
</table> --->