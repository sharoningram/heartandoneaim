<cfparam name="submittype" default="">
<cfparam name="firstname" default="">
<cfparam name="lastname" default="">
<cfparam name="Useremail" default="">
<cfparam name="isadmin" default="0">
<cfparam name="ishsselead" default="0">
<cfparam name="Status" default="1">
<cfparam name="BusinessLine_1" default="">
<cfparam name="ou_1" default="">
<cfparam name="role_1" default="">

<cfparam name="buadminbu" default="">
<cfparam name="ouadminou" default="">
<cfparam name="userou" default="">
<cfparam name="reviewerou" default="">
<cfparam name="srreviewerbu" default="">
<cfparam name="mhiou" default="">

<cfset msg = 0>
<cfswitch expression="#submittype#">
	<cfcase value="add">
		<cfif trim(firstname) neq '' and trim(lastname) neq '' and trim(useremail) neq '' and trim(role_1) neq ''>
			<cfquery name="chk4user" datasource="#request.dsn#">
				select userid
				from users
				where useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(useremail)#">
			</cfquery>
			<cfif chk4user.recordcount gt 0>
				<cfset msg = 1>
			<cfelse>
				<cftry>
				<cftransaction>
				<cfquery name="adduser" datasource="#request.dsn#">
					insert into users (Firstname, Lastname, Useremail, Status, Entered_By,  ishsselead)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(firstname)#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(lastname)#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(useremail)#">,<cfqueryparam cfsqltype="CF_SQL_BIT" value="#status#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_BIT" value="#ishsselead#">)
					select ident_current('users') as newuser
				</cfquery>
				
				<cfloop list="#role_1#" index="i">
					<cfset assignedlocs = "0">
					<cfswitch expression="#i#">
						<cfcase value="BU Admin">
							<cfset assignedlocs = buadminbu>
						</cfcase>
						<cfcase value="Senior Reviewer">
							<cfset assignedlocs = srreviewerbu>
						</cfcase>
						<cfcase value="User">
							<cfset assignedlocs = userou>
						</cfcase>
						<cfcase value="Reviewer">
							<cfset assignedlocs = reviewerou>
						</cfcase>
						<cfcase value="Man Hour Input">
							<cfset assignedlocs = mhiou>
						</cfcase>
						<cfcase value="OU Admin">
							<cfset assignedlocs = ouadminou>
						</cfcase>
					</cfswitch>
					<cfloop list="#assignedlocs#" index="o">
					<cfquery name="addrole" datasource="#request.dsn#">
						insert into UserRoles (UserID, UserRole,AssignedLocs)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#adduser.newuser#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#o#">)
					</cfquery>
					</cfloop>
				</cfloop>
				
				<!--- 
				<cfset assignedlocs = "">
					<cfif listfindnocase("BU Admin,Senior Reviewer",role_1) gt 0>
						<cfset assignedlocs = BusinessLine_1>
					</cfif>
					<cfif listfindnocase("User,Reviewer,Man Hour Input",role_1) gt 0>
						<cfset assignedlocs = ou_1>
					</cfif>
				<cfquery name="addrole" datasource="#request.dsn#">
					insert into UserRoles (UserID, UserRole,AssignedLocs)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#adduser.newuser#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(role_1)#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#assignedlocs#">)
				</cfquery> --->
				
				<!--- <cfif listfindnocase(role_1,"BU Admin,Senior Reviewer") gt 0>
				<cfloop list="#BusinessLine_1#" index="i">
					<cfquery name="addbljoin" datasource="#request.dsn#">
						insert into UserBusinessLines (UserID, BusinessLine)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#adduser.newuser#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
					</cfquery>
				</cfloop>
				
				
				</cfif>
				
				BU Admin')||(roleval=='Senior Reviewer
				<cfloop list="#BusinessLine_1#" index="i">
					<cfquery name="addbljoin" datasource="#request.dsn#">
						insert into UserBusinessLines (UserID, BusinessLine)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#adduser.newuser#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
					</cfquery>
				</cfloop>
				 --->
				
				
				</cftransaction>
				<cfset msg = 3>
				<cfcatch type="Any">
					<cfset msg = 2>
				</cfcatch>
				
				</cftry>
			</cfif>
		
		</cfif>
		<cfoutput>
			<form name="gomain" method="post" action="#self#?fuseaction=#attributes.xfa.main#">
			<input type="hidden" name="msg" value="#msg#">
			</form>
		</cfoutput>
		<script type="text/javascript">
		document.gomain.submit();
		</script>
		<cflocation url="#self#?fuseaction=#attributes.xfa.main#&msg=#msg#" addtoken="No">
	</cfcase>
	<cfcase value="update">
		<cfif trim(firstname) neq '' and trim(lastname) neq '' and trim(useremail) neq '' and trim(role_1) neq ''>
			<cfquery name="chk4user" datasource="#request.dsn#">
				select userid
				from users
				where useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(useremail)#"> and userid <> <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#user#">
			</cfquery>
			<cfif chk4user.recordcount gt 0>
				<cfset msg = 4>
			<cfelse>
				<!--- <cftry> --->
					<cftransaction>
					<cfquery name="updateuser" datasource="#request.dsn#">
						update users 
						set Firstname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(firstname)#">, 
							Lastname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(lastname)#">, 
							Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#trim(useremail)#">, 
							Status = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#status#">, 
							Entered_By = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#"><!--- , 
							ishsselead = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#ishsselead#">,
							Is_Admin = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#isadmin#"> --->
						where userid =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#user#">
				</cfquery>
				
				<cfquery name="deletejoin" datasource="#request.dsn#">
					delete from UserRoles
					where userid =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#user#">
				</cfquery>
				
				
				<cfloop list="#role_1#" index="i">
					<cfset assignedlocs = "0">
					<cfswitch expression="#i#">
						<cfcase value="BU Admin">
							<cfset assignedlocs = buadminbu>
						</cfcase>
						<cfcase value="Senior Reviewer">
							<cfset assignedlocs = srreviewerbu>
						</cfcase>
						<cfcase value="User">
							<cfset assignedlocs = userou>
						</cfcase>
						<cfcase value="Reviewer">
							<cfset assignedlocs = reviewerou>
						</cfcase>
						<cfcase value="Man Hour Input">
							<cfset assignedlocs = mhiou>
						</cfcase>
						<cfcase value="OU Admin">
							<cfset assignedlocs = ouadminou>
						</cfcase>
					</cfswitch>
					<cfloop list="#assignedlocs#" index="o">
					<cfquery name="addrole" datasource="#request.dsn#">
						insert into UserRoles (UserID, UserRole,AssignedLocs)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#user#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#o#">)
					</cfquery>
					</cfloop>
				</cfloop>
				
				<!--- <cfquery name="deletejoin" datasource="#request.dsn#">
					delete from UserBusinessLines
					where userid =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#user#">
				</cfquery>
				<cfloop list="#BusinessLine#" index="i">
					<cfquery name="addbljoin" datasource="#request.dsn#">
						insert into UserBusinessLines (UserID, BusinessLine)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#user#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
					</cfquery>
				</cfloop> --->
				
				</cftransaction>
				<!--- <cfset msg = 5>
				<cfcatch type="Any">
					<cfset msg = 6>
				</cfcatch>
				
				</cftry> --->
			</cfif>
		</cfif>
		<cfoutput>
<form name="editusersearch" method="post" action="#self#?fuseaction=#attributes.xfa.main#">
<input type="hidden" name="user" value="#user#">
<input type="hidden" name="submittype" value="search">
<input type="hidden" name="sfirstname" value="#sfirstname#">
<input type="hidden" name="slastname" value="#slastname#">
<input type="hidden" name="sUseremail" value="#sUseremail#">
<input type="hidden" name="sstatus" value="#sstatus#">
<input type="hidden" name="sisadmin" value="#sisadmin#">
<input type="hidden" name="sishsselead" value="#sishsselead#">
<input type="hidden" name="msg" value="#msg#">
</form>
</cfoutput>
<script type="text/javascript">
document.editusersearch.submit();
</script>
	</cfcase>
</cfswitch>

