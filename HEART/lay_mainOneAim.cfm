<cfif listlen(request.userlevel eq 1)>
	<cfif request.userlevel eq "Global IT">
		<cflocation url="/oneAIM/index.cfm?fuseaction=adminfunctions.main" addtoken="No">
	</cfif>
</cfif>

<cfset titleaddon = "Incidents">
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="first">
		<cfset titleaddon = titleaddon & " - Incident Notice">
	</cfcase>
	<cfcase value="azmanagement">
		<cfset titleaddon = titleaddon & " - Manage A-Z Categories">
	</cfcase>
</cfswitch>
<cfoutput>
<cfif trim(request.UserID) neq '' and trim(request.UserID) neq 0>


	<div class="stripe1">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">

<tr><td>
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr>
	<td style="font-family:Segoe UI;color:ffffff;font-size:12pt;" width="90%">
&nbsp;<a href="/oneAIM" style="color:ffffff;text-decoration:none;">Analysis Incident Management</a></td><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;"><cfif trim(request.fname) neq ''>Welcome&nbsp;<cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td><cfif listfindnocase("drawdropchart,popgroupinfo,incidentmanament,createsublists,manageinvestigation,manageCAPA,manageIRP,reviewirn,managereview,famanagement,reviewinvestigation,raiseCAPA,adddateval,fieldaudit,cancel,getconnames,addcontractor",fusebox.fuseaction) eq 0><td align="right" style="font-family:Segoe UI;color:5F2167;font-size:10pt;" bgcolor="88DBDF">&nbsp;<a href="javascript:void(0);" onclick="NewWindow('/oneAIM/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');" style="font-family:Segoe UI;color:5F2167;font-size:10pt;text-decoration:none;">Help</a>&nbsp;</span><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;">&nbsp;&nbsp;&nbsp;</td></cfif></td></tr>
</table></td></tr>
<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>
<cfif listfindnocase("reviewinvestigation,reviewirn,raiseCAPA,adddateval,fieldaudit,cancel,addcontractor",fusebox.fuseaction) eq 0>
<cfset irtab = "on">
<cfset irimg = "arrowpink">
<cfset revtab = "off">
<cfset revimg = "arrowclear">
<cfset invtab = "off">
<cfset invimg = "arrowclear">
<cfset frtab = "off">
<cfset frimg = "arrowclear">
<cfset clotab = "off">
<cfset cloimg = "arrowwhite">
<cfset isfat = "no">
<cfset needpendsdp = "">
<cfset iswrrec = "yes">
<Cfif fusebox.fuseaction eq "fatalityreport">
	<cfif not isdefined("getincidentdetails.recordcount")>
		<cfset isfat = "yes">
	<cfelseif getincidentdetails.recordcount eq 0>
		<cfset isfat = "yes">
	</cfif>
</cfif>

<cfif isdefined("getincidentdetails.recordcount")>

<cfif trim(getincidentdetails.irn) neq ''>
<cfquery name="getinvstat" datasource="#request.dsn#">
SELECT        Status
FROM            IncidentInvestigation
WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.irn#">)
</cfquery>
</cfif>
	<cfif getincidentdetails.isfatality eq "Yes">
		<cfset isfat = "yes">
	</cfif>
	<cfif getincidentdetails.isworkrelated eq "No">
		<cfset iswrrec = "No">
	</cfif>
	
	<cfif isdefined("getinvstat.status")>
		<cfif listfindnocase("Approved,IRP Pending,Sent for Review",getinvstat.status) gt 0>
			<cfset irtab = "on">
			<cfset irimg = "arrowclear">
			<cfset revtab = "on">
			<cfset revimg = "arrowclear">
			<cfset invtab = "on">
			<cfset invimg = "arrowclear">
			<cfset frtab = "on">
			<cfset frimg = "arrowpink">
			<cfset clotab = "off">
			<cfset cloimg = "arrowwhite">
		</cfif>
		
	</cfif>
	<cfif getincidentdetails.status eq "Closed">
		<cfset irtab = "on">
		<cfset irimg = "arrowclear">
		<cfset revtab = "on">
		<cfset revimg = "arrowclear">
		<cfset invtab = "on">
		<cfset invimg = "arrowclear">
		<cfset frtab = "on">
		<cfset frimg = "arrowclear">
		<cfset clotab = "on">
		<cfset cloimg = "arrowwhite">
	<cfelseif getincidentdetails.status eq "Sent for Review" or getincidentdetails.status eq "More info Requested">
		<cfset irtab = "on">
		<cfset irimg = "arrowclear">
		<cfset revtab = "on">
		<cfset revimg = "arrowpink">
		<cfset invtab = "off">
		<cfset invimg = "arrowclear">
		<cfset frtab = "off">
		<cfset frimg = "arrowclear">
		<cfset clotab = "off">
		<cfset cloimg = "arrowwhite">
	
	
	<cfelseif getincidentdetails.status eq "Review Completed">
			<cfset irtab = "on">
			<cfset irimg = "arrowclear">
			<cfset revtab = "on">
			<cfset revimg = "arrowclear">
			<cfset invtab = "on">
			<cfset invimg = "arrowpink">
			<cfset frtab = "off">
			<cfset frimg = "arrowclear">
			<cfset clotab = "off">
			<cfset cloimg = "arrowwhite">
			
			<cfif getincidentdetails.isworkrelated eq "No">
								<cfset whypendlist = "">
									<cfquery name="getopenca" datasource="#request.dsn#">
										SELECT        status
										FROM            oneAIMCAPA
										WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (CAPAtype = 'corrective') AND (status = 'Open')
									</cfquery>
									<cfif getopenca.recordcount gt 0>
										<cfset whypendlist = listappend(whypendlist,"CA")>
									</cfif>
									<cfif listfind("1,5",getincidentdetails.PrimaryType) gt 0 or listfind("1,5",getincidentdetails.SecondaryType) gt 0>
										<cfquery name="getoshacat" datasource="#request.dsn#">
											SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
											FROM            oneAIMInjuryOI
											where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
										</cfquery>
										<cfif getoshacat.OSHAclass eq 2>
											<cfif trim(getoshacat.LTIDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"LTI")>
											</cfif>
										<cfelseif getoshacat.OSHAclass eq 4> 
											<cfif trim(getoshacat.RWCDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"RWC")>
											</cfif>
										</cfif> 
									</cfif>
					
									<cfset needpendsdp = "Pending Closure">
									<cfif trim(whypendlist) neq ''><cfset needpendsdp = needpendsdp & " - #whypendlist#"></cfif>
									
								
			<cfelse>
			<cfif isdefined("getinvstat.status")>
			<cfif listfindnocase("Sent for Review",getinvstat.status) gt 0>
				<cfset irtab = "on">
				<cfset irimg = "arrowclear">
				<cfset revtab = "on">
				<cfset revimg = "arrowclear">
				<cfset invtab = "on">
				<cfset invimg = "arrowclear">
				<cfset frtab = "on">
				<cfset frimg = "arrowpink">
				<cfset clotab = "off">
				<cfset cloimg = "arrowwhite">
			<cfelseif listfindnocase("Started",getinvstat.status) gt 0>
				<cfset irtab = "on">
				<cfset irimg = "arrowclear">
				<cfset revtab = "on">
				<cfset revimg = "arrowclear">
				<cfset invtab = "on">
				<cfset invimg = "arrowpink">
				<cfset frtab = "off">
				<cfset frimg = "arrowpink">
				<cfset clotab = "off">
				<cfset cloimg = "arrowwhite">
			<cfelseif listfindnocase("Approved,IRP Pending",getinvstat.status) gt 0>
				<cfset irtab = "on">
				<cfset irimg = "arrowclear">
				<cfset revtab = "on">
				<cfset revimg = "arrowclear">
				<cfset invtab = "on">
				<cfset invimg = "arrowclear">
				<cfset frtab = "on">
				<cfset frimg = "arrowpink">
				<cfset clotab = "off">
				<cfset cloimg = "arrowwhite">
				<cfset whypendlist = "">
									<cfquery name="getopenca" datasource="#request.dsn#">
										SELECT        status
										FROM            oneAIMCAPA
										WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (CAPAtype = 'corrective') AND (status = 'Open')
									</cfquery>
									<cfif getopenca.recordcount gt 0>
										<cfset whypendlist = listappend(whypendlist,"CA")>
									</cfif>
									<cfif listfind("1,5",getincidentdetails.PrimaryType) gt 0 or listfind("1,5",getincidentdetails.SecondaryType) gt 0>
										<cfquery name="getoshacat" datasource="#request.dsn#">
											SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
											FROM            oneAIMInjuryOI
											where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
										</cfquery>
										<cfif getoshacat.OSHAclass eq 2>
											<cfif trim(getoshacat.LTIDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"LTI")>
											</cfif>
										<cfelseif getoshacat.OSHAclass eq 4> 
											<cfif trim(getoshacat.RWCDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"RWC")>
											</cfif>
										</cfif> 
									</cfif>
					
									<cfset needpendsdp = "Pending Closure">
									<cfif trim(whypendlist) neq ''><cfset needpendsdp = needpendsdp & " - #whypendlist#"></cfif>
			
			
			</cfif>
		</cfif>
			
			</cfif>
		
			
	</cfif>
	
</cfif>
<tr bgcolor="ffffff">
	<Td colspan="2" align="center">
		<cfif isfat or not iswrrec>
		<cfset usecolwide = 6>
		<cfset needpendspimg = "arrowwhite">
	<cfif isdefined("getincidentdetails.recordcount")>
			<cfif getincidentdetails.withdrawnstatus eq 1>
				<cfset clotab = "off">
				<cfset cloimg = "arrowpurple">
				<cfset usecolwide = 8>
			</cfif>
			<cfif getincidentdetails.status eq "Cancel">
				<cfset clotab = "off">
				<cfset cloimg = "arrowpurple">
				<cfset usecolwide = 8>
			</cfif>
	</cfif>
	<cfif trim(needpendsdp) neq ''>
		<cfset clotab = "off">
		<cfset cloimg = "arrowpurple">
		<cfset usecolwide = 8>
			<cfif getincidentdetails.withdrawnstatus eq 1>
				<cfset clotab = "off">
				<cfset cloimg = "arrowpurple">
				<cfset usecolwide = 10>
				<cfset needpendspimg = "arrowclear">
			</cfif>
	</cfif>
		<table cellpadding="0" border="0" cellspacing="0" width="100%">
			<tr>
				<td height="1" bgcolor="6b84b3" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
			<tr>
			
			<td width="1" bgcolor="6b84b3"><img src="images/spacer.gif" height="1" border="0"></td>
			<td class="tubemap#irtab#" width="10%" align="center">Incident&nbsp;Raised</td>
			<td class="tubemap#irtab#"><img src="images/#irimg#.png" border="0"></td>
			
			<td class="tubemap#revtab#" width="10%" align="center">Initial&nbsp;Review</td>
			<td class="tubemap#revtab#"><img src="images/#revimg#.png" border="0"></td>
			<!--- <td class="tubemap#invtab#" width="10%" align="center">Investigation</td> --->
			<!--- <td class="tubemap#invtab#"><img src="images/#invimg#.png" border="0"></td> --->
			<!--- <td class="tubemap#frtab#" width="10%" align="center">Final&nbsp;Review</td>
			<td class="tubemap#frtab#"><img src="images/#frimg#.png" border="0"></td> --->
			<td class="tubemap#clotab#" width="10%" align="center">Closed</td>
			<td class="tubemap#clotab#"><img src="images/#cloimg#.png" border="0"></td>
			<cfif trim(needpendsdp) neq ''>
					<td class="tubemapon" width="10%" align="center">#needpendsdp#</td>
					<td class="tubemapon"><img src="images/#needpendspimg#.png" border="0"></td>
			</cfif>
			<cfif isdefined("getincidentdetails.recordcount")>
			<cfif getincidentdetails.status eq "Cancel">
				<td class="tubemapon" width="10%" align="center">Cancelled</td>
				<td class="tubemapon"><img src="images/arrowwhite.png" border="0"></td>
			<cfelse>
				<cfif getincidentdetails.withdrawnstatus eq 1>
					<td class="tubemapon" width="10%" align="center">Withdrawn</td>
					<td class="tubemapon"><img src="images/arrowwhite.png" border="0"></td>
				</cfif>
			</cfif>
			</cfif>
			<td width="70%">&nbsp;</td>
			<tr>
			<tr>
				<td height="1" bgcolor="6b84b3" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
		</table>
	<cfelse>
	<cfset usecolwide = 10>
	<cfset needpendspimg = "arrowwhite">
	<cfif isdefined("getincidentdetails.recordcount")>
			<cfif getincidentdetails.withdrawnstatus eq 1>
				<cfset clotab = "off">
				<cfset cloimg = "arrowpurple">
				<cfset usecolwide = 12>
			</cfif>
			<cfif getincidentdetails.status eq "Cancel">
				<cfset clotab = "off">
				<cfset cloimg = "arrowpurple">
				<cfset usecolwide = 12>
			</cfif>
	</cfif>
	<cfif trim(needpendsdp) neq ''>
		<cfset clotab = "off">
		<cfset cloimg = "arrowpurple">
		<cfset usecolwide = 12>
			<cfif getincidentdetails.withdrawnstatus eq 1>
				<cfset clotab = "off">
				<cfset cloimg = "arrowpurple">
				<cfset usecolwide = 14>
				<cfset needpendspimg = "arrowclear">
			</cfif>
	</cfif>
		<table cellpadding="0" border="0" cellspacing="0" width="100%">
			<tr>
				<td height="1" bgcolor="6b84b3" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
			<tr>
			
			<td width="1" bgcolor="6b84b3"><img src="images/spacer.gif" height="1" border="0"></td>
			<td class="tubemap#irtab#" width="10%" align="center">Incident&nbsp;Raised</td>
			<td class="tubemap#irtab#"><img src="images/#irimg#.png" border="0"></td>
			
			<td class="tubemap#revtab#" width="10%" align="center">Initial&nbsp;Review</td>
			<td class="tubemap#revtab#"><img src="images/#revimg#.png" border="0"></td>
			<td class="tubemap#invtab#" width="10%" align="center">Investigation</td>
			<td class="tubemap#invtab#"><img src="images/#invimg#.png" border="0"></td>
			<td class="tubemap#frtab#" width="10%" align="center">Investigation&nbsp;Review</td>
			<td class="tubemap#frtab#"><img src="images/#frimg#.png" border="0"></td>
			<td class="tubemap#clotab#" width="10%" align="center">Closed</td>
			<td class="tubemap#clotab#"><img src="images/#cloimg#.png" border="0"></td>
			<cfif trim(needpendsdp) neq ''>
					<td class="tubemapon" width="10%" align="center">#needpendsdp#</td>
					<td class="tubemapon"><img src="images/#needpendspimg#.png" border="0"></td>
			</cfif>
			<cfif isdefined("getincidentdetails.recordcount")>
			<cfif getincidentdetails.status eq "Cancel">
				<td class="tubemapon" width="10%" align="center">Cancelled</td>
				<td class="tubemapon"><img src="images/arrowwhite.png" border="0"></td>
			<cfelse>
				<cfif getincidentdetails.withdrawnstatus eq 1>
					<td class="tubemapon" width="10%" align="center">Withdrawn</td>
					<td class="tubemapon"><img src="images/arrowwhite.png" border="0"></td>
				</cfif>
			</cfif>
			</cfif>
			<td width="50%">&nbsp;</td>
			<tr>
			<tr>
				<td height="1" bgcolor="6b84b3" colspan="#usecolwide#"><img src="images/spacer.gif" height="1" border="0"></td>
			</tr>
		</table>
	</cfif>
	</td>
</tr>
</cfif>
<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>
</table>
</div>
<cfset canaccessincident = "no">
<cfset showleftnav = "yes">
<cfif listfindnocase(request.userlevel,"Senior Executive View") gt 0>
	<cfset canaccessincident = "yes">
	<cfset showleftnav = "no">
</cfif>
<cfif isdefined("getincidentdetails.recordcount")>
<cfif trim(getincidentdetails.status) eq ''>
	<cfset canaccessincident = "yes">
	
</cfif>

<cfif getincidentdetails.createdbyEmail eq request.userlogin>
	<cfset canaccessincident = "yes">
	<cfset showleftnav = "yes">
</cfif>

<cfif listfindnocase(request.userlevel,"Reviewer") gt 0>
	<cfif trim(getincidentdetails.status) neq '' and trim(getincidentdetails.status) neq "Initiated" and trim(getincidentdetails.status) neq "Started">
	<cfif listfind(request.userOUs,getincidentdetails.ouid) gt 0>
		<cfset canaccessincident = "yes">
		<cfset showleftnav = "yes">
	</cfif>
	</cfif>
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
	<cfif listfind(request.userOUs,getincidentdetails.ouid) gt 0>
		<cfset canaccessincident = "yes">
		<cfset showleftnav = "yes">
	</cfif>
</cfif>
<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0><!--- and trim(getincidentdetails.status) neq "Sent for Review" --->
	<cfif trim(getincidentdetails.status) neq '' and trim(getincidentdetails.status) neq "Initiated" and trim(getincidentdetails.status) neq "Started" >
		<cfif trim(getincidentdetails.status) eq "More Info Requested">
			<cfif getincidentdetails.investigationlevel eq 2 or getincidentdetails.needfa eq 1>
				<cfset canaccessincident = "yes">
				<cfset showleftnav = "yes">
			</cfif>
		<cfelse>
			<cfif listfind(request.userBUs,getincidentdetails.Business_Line_ID) gt 0>
				<cfset canaccessincident = "yes">
				<cfset showleftnav = "yes">
			</cfif>
		</cfif>
	</cfif>
</cfif>
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	<cfif listfind(request.userBUs,getincidentdetails.Business_Line_ID) gt 0>
		<cfset canaccessincident = "yes">
		<cfset showleftnav = "yes">
	</cfif>
</cfif>
</cfif>

<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
	<cfset canaccessincident = "yes">
	<cfset showleftnav = "yes">
</cfif>

<cfif listfindnocase("drawdropchart,popgroupinfo,incidentmanament,createsublists,manageinvestigation,manageCAPA,manageIRP,reviewirn,managereview,famanagement,reviewinvestigation,raiseCAPA,adddateval,fieldaudit,cancel,getconnames,addcontractor",fusebox.fuseaction) gt 0>
<cfset canaccessincident = "yes">
</cfif>

<cfif canaccessincident>
<cfset irbgcol = "e8bdf9">
<cfset irstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<cfset irvalign = "middle">
<cfset irtabs = "sm">

<cfset fabgcol = "e8bdf9">
<cfset fastyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<!--- <cfset favalign = "baseline"> --->
<cfset fatabs = "sm">

<cfset invbgcol = "e8bdf9">
<cfset invstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<cfset invvalign = "middle">
<cfset invtabs = "sm">

<cfset irpbgcol = "e8bdf9">
<cfset irpstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<cfset irpvalign = "middle">
<cfset irptabs = "sm">

<cfset capabgcol = "e8bdf9">
<cfset capastyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<cfset capavalign = "middle">
<cfset capatabs = "sm">

<cfset atbgcol = "e8bdf9">
<cfset atstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;">
<cfset atvalign = "middle">
<cfset attabs = "sm">

<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="incidentreport,fatalityreport">
		<cfset irbgcol = "5f2468">
		<cfset irstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">
		<cfset irvalign = "middle">
		<cfset irtabs = "">
	</cfcase>
	<cfcase value="firstalert">
		<cfset fabgcol = "5f2468">
		<cfset fastyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">
		<cfset favalign = "middle">
		<cfset fatabs = "">
	</cfcase>
	<cfcase value="investigation">
		<cfset invbgcol = "5f2468">
		<cfset invstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">
		<cfset invvalign = "middle">
		<cfset invtabs = "">
	</cfcase>
	<cfcase value="IRP">
		<cfset irpbgcol = "5f2468">
		<cfset irpstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">
		<cfset irpvalign = "middle">
		<cfset irptabs = "">
	</cfcase>
	<cfcase value="CAPA">
		<cfset capabgcol = "5f2468">
		<cfset capastyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">
		<cfset capavalign = "middle">
		<cfset capatabs = "">
	</cfcase>
	<cfcase value="audit">
		<cfset atbgcol = "5f2468">
		<cfset atstyle = "color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-15px;">
		<cfset atvalign = "middle">
		<cfset attabs = "">
	</cfcase>
</cfswitch>
<cfif isdefined("getincidentdetails.recordcount")>
	<cfif getincidentdetails.isfatality eq "yes">
		<cfset gotoaction = "fatalityreport">
	<cfelse>
		<cfif fusebox.fuseaction eq "fatalityreport">
			<cfset gotoaction = "fatalityreport">
		<cfelse>
			<cfset gotoaction = "incidentreport">
		</cfif>
	</cfif>
<cfelse>
	<cfif fusebox.fuseaction eq "fatalityreport">
		<cfset gotoaction = "fatalityreport">
	<cfelse>
		<cfset gotoaction = "incidentreport">
	</cfif>
</cfif>
<cfset needltidate = "no">
<cfset needrwcdate = "no">
<cfif isdefined("getincidentdetails.recordcount") and  isdefined("getincidentoi.recordcount")>
	<cfif getincidentdetails.recordcount gt 0 and getincidentoi.recordcount gt 0>
		
		<cfif getincidentoi.oshaclass eq 2>
			<cfif trim(getincidentoi.LTIDateReturned) eq ''>
				<cfset needltidate = "yes">
			</cfif>
		<cfelseif getincidentoi.oshaclass eq 4>
			<cfif trim(getincidentoi.RWCDateReturned) eq ''>
				<cfset needrwcdate = "yes">
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cfparam name="irn" default="0">

	<table cellpadding="0" border="0" cellspacing="0" width="85%" id="mytab">
	<tr>
		<td coslpan="2"><img src="images/spacer.gif" height="60" border="0"></td>
	</tr>
	<tr>
	<td width="7%" align="center">&nbsp;</td>
	<td width="93%">
		<table  cellpadding="0" cellspacing="0" border="0">
		<cfoutput>
			<tr>
				<cfif listfindnocase("reviewinvestigation,reviewirn,raiseCAPA,adddateval,fieldaudit,cancel,addcontractor",fusebox.fuseaction) eq 0>
				<td bgcolor="e4c9ff"><img src="images/tableft#irtabs#.png" border="0"></td>
				<td bgcolor="e4c9ff" style="#irstyle#" <cfif fusebox.fuseaction neq gotoaction>background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=incidents.#gotoaction#&irn=#irn#" style="#irstyle#"><cfif gotoaction eq "fatalityreport">Fatality<cfelse>Incident</cfif>&nbsp;Report<cfif isdefined("getincidentdetails.recordcount")><cfif getincidentdetails.recordcount gt 0>&nbsp;-&nbsp;#getincidentdetails.trackingnum#</cfif></cfif></a></td>
				<td bgcolor="e4c9ff"><img src="images/tabright#irtabs#.png" border="0"></td>
				</cfif>
				<cfif isdefined("getincidentdetails.recordcount")>
					<cfif getincidentdetails.recordcount gt 0>
					 <cfif getincidentdetails.isfatality neq "yes">
						<cfif getincidentdetails.status neq "Cancel"><!---  getincidentdetails.InvestigationLevel eq 2 or  --->
							<cfif getincidentdetails.needFA eq 1>
								<cfif listfindnocase("Review Completed,Closed,Initiated,More Info Requested,Sent for Review,Started",getincidentdetails.status) gt 0>
									<td bgcolor="e5f49a" id="fatabl"><img src="images/tableft#fatabs#.png" border="0"></td>
									<td bgcolor="e5f49a" style="#fastyle#"  id="fatabmid" <cfif fusebox.fuseaction neq "firstalert">background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=incidents.firstalert&irn=#irn#" style="#fastyle#">First&nbsp;Alert</a></td>
									<td bgcolor="e5f49a" id="fatabr"><img src="images/tabright#fatabs#.png" border="0"></td>
								</cfif>
							</cfif>
							<cfif listfindnocase("Closed,Review Completed",getincidentdetails.status) gt 0 and getincidentdetails.isworkrelated neq "No">
								<td bgcolor="cdf5ff"><img src="images/tableft#invtabs#.png" border="0"></td>
								<td bgcolor="cdf5ff" style="#invstyle#"  <cfif fusebox.fuseaction neq "investigation">background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=incidents.investigation&irn=#irn#" style="#invstyle#">Investigation</a></td>
								<td bgcolor="cdf5ff"><img src="images/tabright#invtabs#.png" border="0"></td>
							</cfif>
							<cfset showcapatab = "yes">
								<cfif listfindnocase("Closed,Review Completed",getincidentdetails.status) gt 0>
								<cfquery name="getneedcapatab" datasource="#request.dsn#">
										SELECT        status
										FROM            oneAIMCAPA
										WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
									</cfquery>
								<cfif getneedcapatab.recordcount eq 0>
									<cfset showcapatab = "no">
								</cfif>
								</cfif>
							<cfif showcapatab>
							<td bgcolor="ecd9ff"><img src="images/tableft#capatabs#.png" border="0"></td>
							<td bgcolor="ecd9ff" style="#capastyle#"  <cfif fusebox.fuseaction neq "CAPA">background="images/smtabbg.png"</cfif> ><a href="index.cfm?fuseaction=incidents.CAPA&irn=#irn#" style="#capastyle#">CA/PA</a></td>
							<td bgcolor="ecd9ff"><img src="images/tabright#capatabs#.png" border="0"></td>
							</cfif>
							<cfif getincidentdetails.needIRP eq 1>
								<td bgcolor="eff8c0"><img src="images/tableft#irptabs#.png" border="0"></td>
								<td bgcolor="eff8c0" style="#irpstyle#"  <cfif fusebox.fuseaction neq "IRP">background="images/smtabbg.png"</cfif> ><a href="index.cfm?fuseaction=incidents.IRP&irn=#irn#" style="#irpstyle#">IRP&nbsp;Learnings</a></td>
								<td bgcolor="eff8c0"><img src="images/tabright#irptabs#.png" border="0"></td>
							</cfif>
				
				</cfif>
				</cfif>
				<td bgcolor="ddf9ff"><img src="images/tableft#attabs#.png" border="0"></td>
				<td bgcolor="ddf9ff" style="#atstyle#"  <cfif fusebox.fuseaction neq "audit">background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=incidents.audit&irn=#irn#" style="#atstyle#">Audit&nbsp;Trail</a></td>
				<td bgcolor="ddf9ff"><img src="images/tabright#attabs#.png" border="0"></td>
				
				<cfif getincidentdetails.status eq "closed" and getincidentdetails.isfatality eq "no">
					<td ><a href="index.cfm?fuseaction=incidents.exportincpdf&irn=#irn#" target="_blank"><img src="images/pdfsm.png" border="0" height="24" style="vertical-align:0px;padding-left:4px;"></a></td>
				</cfif>
					<!--- <cfelse>
				
				<td bgcolor="e5f49a" id="fatabl"><img src="images/tableft#fatabs#.png" border="0"></td>
				<td bgcolor="e5f49a" id="fatabmid" style="#fastyle#" <cfif fusebox.fuseaction neq "firstalert">background="images/smtabbg.png"</cfif>><a href="index.cfm?fuseaction=incidents.firstalert&irn=#irn#" style="#fastyle#">First&nbsp;Alert</a></td>
				<td bgcolor="e5f49a" id="fatabr"><img src="images/tabright#fatabs#.png" border="0"></td>
				
				
				<script type="text/javascript">
				document.getElementById("fatabl").style.display='none';
				document.getElementById("fatabmid").style.display='none';
				document.getElementById("fatabr").style.display='none';
				</script> --->
				</cfif>
			</cfif>
				<cfif listfindnocase("reviewinvestigation,reviewirn,raiseCAPA,adddateval,fieldaudit,cancel,addcontractor",fusebox.fuseaction) eq 0>
			<!--- 	<td bgcolor="ffffff"><img src="images/tableftsm.png" border="0"></td> --->
				
				<td width="90%"></td>
				
				<!--- <cfif listfindnocase("reviewinvestigation,reviewirn,raiseCAPA,adddateval,fieldaudit,cancel,addcontractor",fusebox.fuseaction) eq 0> --->
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="/oneAIM" class="bodytextsmpurple" ><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;</td>
				</cfif>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
<cfset lbgcol = "a2df1b">
<tr>
<cfif listfindnocase("reviewinvestigation,reviewirn,raiseCAPA,adddateval,fieldaudit,cancel,addcontractor",fusebox.fuseaction) eq 0>
<cfif listfindnocase("capa,audit",fusebox.fuseaction) gt 0>
	<cfset lbgcol = "ffffff">
</cfif>
<cfif listfindnocase("fatalityreport",fusebox.fuseaction) gt 0>
	<cfif getincidentdetails.status eq "Sent for Review" and request.userlogin eq getincidentdetails.createdbyemail>
		<cfset lbgcol = "ffffff">
	</cfif>
</cfif>
<cfif isdefined("getincidentdetails.status")>
<cfif getincidentdetails.status eq "Closed">
	<cfset lbgcol = "ffffff">
	<cfif listfindnocase("CAPA",fusebox.fuseaction) gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
			<cfset lbgcol = "a2df1b">
		</cfif>
	</cfif>
</cfif>
</cfif>



<cfif showleftnav>



<td valign="top"  bgcolor="ffffff" id="mainlefttdcell">
	
	<!--- 		incident form left nav --->
	<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="incidentreport">
	
		<cfif isdefined("getincidentdetails.recordcount")>
			<cfif getincidentdetails.recordcount eq 0>
			<script type="text/javascript">	
				document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
			</script>
			<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
			<tr>
				<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
				
				<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
				<a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
				
				</td>
			</tr>
			</table>
			<cfelse>
<!--- <cfif getincidentdetails.InvestigationLevel eq 2> --->
			<cfset statlist = "Cancel,Closed,Pending Closure,Review Completed">
			 <cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
					 	<cfif request.userid eq getincidentdetails.withdrawnto>
							<cfset statlist = "Cancel,Closed,Pending Closure">
						<cfelse>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
									<cfset statlist = "Cancel,Closed,Pending Closure">
							</cfif>
						</cfif>
			</cfif>
				<cfif listfindnocase(statlist,getincidentdetails.status) eq 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident form:</strong> <br><br>
				
				
					<cfif trim(getincidentdetails.status) eq '' or listfindnocase("Started,Initiated,More Info Requested",getincidentdetails.status) gt 0>
					<cfset showinitlinks = "yes">
					 <cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
					 	<cfif request.userid neq getincidentdetails.withdrawnto>
								
						<cfelse>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
								<cfset showinitlinks = "no">
							</cfif>
						</cfif>
					</cfif>
					<cfif showinitlinks>
					
				<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
				<a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
						<strong class="bodytextsmpurple"><br>You can take the following actions on this record:</strong> <br><br>
						<cfif getincidentdetails.status eq "Review Completed">
							<a href="index.cfm?fuseaction=incidents.investigation&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Complete&nbsp;Investigation</a><br>
						</cfif>
						<!--- <a href="index.cfm?fuseaction=incidents.capa&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;CAPA</a><br>
						 --->
						 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
						 <cfif getincidentdetails.investigationlevel eq 2>
						<a href="index.cfm?fuseaction=incidents.firstalert&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Complete&nbsp;First&nbsp;Alert</a><br>
						</cfif>
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
								<cfif listfind("3,4,11",getincidentdetails.incassignedto) gt 0 and getincidentdetails.Contractor eq 0>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.addcontractor&irn=#irn#&action=review','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Assign&nbsp;Contractor</a><br>
								</cfif>
							</cfif>
						<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
					
					<!--- <cfif getincidentdetails.InvestigationLevel eq 2>
						<a href="index.cfm?fuseaction=incidents.firstalert&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;"><cfif listfindnocase("Started,Initiated,Withdrawn to Initiator,More Info Requested",getincidentdetails.status) gt 0>Complete&nbsp;</cfif>First&nbsp;Alert</a><br>
					</cfif> --->
					
					<cfelse>
						&nbsp;&nbsp;&nbsp;Withdrawn
					</cfif>
				<cfelse>
						</cfif>
						
						
					<cfif listfindnocase("Sent for Review",getincidentdetails.status) gt 0 and listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=review','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
						<!--- 	<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=moreinfo&irn=#getincidentdetails.irn#"> ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfo','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
						<!--- <strong class="bodytextsmpurple"><br>You can take the following actions on this record:</strong> <br><br>
							<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br> --->
							<cfif listfindnocase("1,2",getincidentdetails.InvestigationLevel) gt 0 and getincidentdetails.status eq "Review Completed">
								<a href="index.cfm?fuseaction=incidents.investigation&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Complete&nbsp;Investigation</a><br>
							</cfif>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
								<cfif listfind("3,4,11",getincidentdetails.incassignedto) gt 0 and getincidentdetails.Contractor eq 0>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.addcontractor&irn=#irn#&action=review','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Assign&nbsp;Contractor</a><br>
								</cfif>
							</cfif>
						<cfelseif listfindnocase(request.userlevel,"User") gt 0>
							<cfif getincidentdetails.status eq "Review Completed">
								<a href="index.cfm?fuseaction=incidents.investigation&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Complete&nbsp;Investigation</a><br>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
					<!--- 		<a href="index.cfm?fuseaction=incidents.capa&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;CAPA</a><br>
						 ---></cfif>
					<cfelseif listfindnocase("Sent for Review",getincidentdetails.status) gt 0 and listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
						<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0>
						<cfif request.userid neq getincidentdetails.withdrawnto>
							&nbsp;&nbsp;&nbsp;Withdrawn
						</cfif>
						</cfif>
					</cfif>
	
					
					<cfif listfindnocase("Started,Initiated,Closed,Cancel,More Info Requested",getincidentdetails.status) eq 0 and listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
						<cfif getincidentdetails.status eq "Review Completed" or getincidentdetails.status eq "Sent for Review">
							<cfif needltidate>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.adddateval&irn=#irn#&action=lti','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Add&nbsp;LTI&nbsp;Return&nbsp;Date</a><br>
							</cfif>
							<cfif needrwcdate>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.adddateval&irn=#irn#&action=rwc','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Add&nbsp;RWC&nbsp;Return&nbsp;Date</a><br>
							</cfif>
						</cfif>
						<cfif getincidentdetails.status neq "Closed">
						<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<br>
				<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						<!--- </cfif> --->
						</cfif>
						</cfif>
					</cfif>
					<cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
						<cfif request.userid eq getincidentdetails.withdrawnto>
						<!--- <a href="javascript:void(0);" onclick="return submitsfrm('savewith');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br> --->
						
						<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=returnwithdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"> --->
						<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
						<a href="javascript:void(0);" onclick="return submitsfrm('yes');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Return&nbsp;from&nbsp;Withdrawal</a><br>
						<cfelse>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
							<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
							<a href="javascript:void(0);" onclick="return submitsfrm('yes');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Return&nbsp;from&nbsp;Withdrawal</a><br>
							</cfif>
						</cfif>
					</cfif>
					</td>
				</tr>
			</table>
				<cfelse>
				<cfif listfind("Cancel,Closed,Pending Closure",getincidentdetails.status) gt 0>
					<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<cfif canaccessincident>
							<script type="text/javascript">	
								document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
								</script>
								<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td class="bodyTextPurple">
								What&nbsp;Next...<br><br>
								<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a>
							<br>
								<cfif listfind("Closed,Pending Closure",getincidentdetails.status) gt 0>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								</cfif>
									</td>
									</tr>
									</table>
							</cfif>
					<cfelseif listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
				
					<cfif canaccessincident and listfindnocase("Closed,Cancel",getincidentdetails.status) eq 0>
							<script type="text/javascript">	
								document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
								</script>
								<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td class="bodyTextPurple">
								What&nbsp;Next...<br><br>
								<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br><br><br>
									</td>
									</tr>
									</table>
							</cfif>
						
					</cfif>
				<cfelse>
					<cfif getincidentdetails.status eq "Review Completed">
						<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<!--- <cfif needltidate or needrwcdate> --->
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
						
						
						<cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
							<cfif listfindnocase(request.userlevel,"Global Admin") and 0 or listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0>
								<cfif request.userid neq getincidentdetails.withdrawnto>
									&nbsp;&nbsp;&nbsp;Withdrawn
								</cfif>
							</cfif>
						<cfelse>
							<cfif needltidate>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.adddateval&irn=#irn#&action=lti','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Add&nbsp;LTI&nbsp;Return&nbsp;Date</a><br>
							</cfif>
							<cfif needrwcdate>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.adddateval&irn=#irn#&action=rwc','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Add&nbsp;RWC&nbsp;Return&nbsp;Date</a><br>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
				<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
								<cfif listfind("3,4,11",getincidentdetails.incassignedto) gt 0 and getincidentdetails.Contractor eq 0>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.addcontractor&irn=#irn#&action=review','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Assign&nbsp;Contractor</a><br>
								</cfif>
							</cfif>
							<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<cfif getincidentdetails.status neq "Closed">
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						</cfif>
						</cfif>
					</td>
					</tr>
					</table>
						</cfif>
							
				</cfif>
				</cfif>
				
				
</cfif>

</cfif>	
	</cfcase>
	<cfcase value="fatalityreport">
		<cfif isdefined("getincidentdetails.recordcount")>
			<cfif getincidentdetails.recordcount eq 0>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
			<table bgcolor="a2df1b" cellpadding="5" cellspacing="0" height="100%">
			<tr>
				<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>

				<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
				<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
				
				</td>
			</tr>
			</table>
				</cfif>
			<cfelse>
				<cfif trim(getincidentdetails.status) eq '' or listfindnocase("Started,Initiated,More Info Requested",getincidentdetails.status) gt 0>
					<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				<table bgcolor="a2df1b" cellpadding="5" cellspacing="0" height="100%">
			<tr>
				<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>

				<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
				<a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
				
				</td>
			</tr>
			</table>
					</cfif>
				</cfif>
				
				<cfif listfindnocase("Sent for Review",getincidentdetails.status) gt 0>
						<!--- <cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0> --->
					<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"Global Admin") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					
					<table bgcolor="a2df1b" cellpadding="5" cellspacing="0" height="100%">
			<tr>
				<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>

				<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewfat','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Approve&nbsp;Report</a><br>
				<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfofat','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
				
				</td>
			</tr>
			</table>
						<!--- <table bgcolor="a2df1b" cellpadding="3" cellspacing="0">
			<tr>
				<td class="bodytextsm">
				What&nbsp;Next?<br>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewfat','Info','500','420','no');">Approve&nbsp;Report</a><br>
						<!--- 	<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=moreinfo&irn=#getincidentdetails.irn#"> ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfofat','Info','500','420','no');">Request&nbsp;More&nbsp;Info</a><br>
						<!--- 	<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#">Cancel</a><br> --->
							</td>
							</tr>
							</table> --->
						
						</cfif>
					</cfif>
				
			</cfif>
		</cfif>
	</cfcase>
	<cfcase value="firstalert"><!--- getincidentdetails.investigationlevel eq 2 or  --->
	<cfif getincidentdetails.needFA eq 1>
		<cfif isdefined("getFAdetail.status")>
			<cfif trim(getFAdetail.status) eq '' or  listfindnocase("Started,More Info Requested,SR More Info",trim(getFAdetail.status)) gt 0>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
				What&nbsp;Next...
				<cfif listfindnocase(request.userlevel,"OU Admin") eq 0><br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this First Alert</strong> <br><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a>
						<!--- <a href="javascript:void(0);" onclick="return chkprj();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a> --->
						<cfif getincidentdetails.investigationlevel neq 2>
						<br><a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a> 
						<cfelse>
							<cfif listfindnocase("Started,Initiated,More Info Requested,Sent for Review",getincidentdetails.status) eq 0>
							<br><a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a> 
							</cfif>
						</cfif>
					</cfif>
						<br><br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
							
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								
							</cfif>
							<cfelse>
								<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
								What&nbsp;Next...<cfif listfindnocase(request.userlevel,"OU Admin") eq 0><br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this First Alert</strong> <br><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a>
						<!--- <a href="javascript:void(0);" onclick="return chkprj();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a> --->
						<cfif getincidentdetails.investigationlevel neq 2>
						<br><a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a> 
						<cfelse>
							<cfif listfindnocase("Started,Initiated,More Info Requested,Sent for Review",getincidentdetails.status) eq 0>
							<br><a href="javascript:void(0);" onclick="return submitsfrm();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a> 
							</cfif>
						</cfif>
						</cfif>
						<br><br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
							
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
						
							
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								
							</cfif>
								<cfelse>
								What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this First Alert</strong> <br><br>
								&nbsp;&nbsp;&nbsp;Withdrawn
								</cfif>
							</cfif>
						<!--- 
						<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br> --->
						
							</td>
						</tr>
						
					</table>
				<cfelseif  listfindnocase(request.userlevel,"Reviewer") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
				
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
							<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						</cfif>
							<cfelse>
							&nbsp;&nbsp;&nbsp;Withdrawn
							</cfif>
							</td>
						</tr>
					</table>
				</cfif>
			<cfelseif trim(getFAdetail.status) eq "Sent for Review">
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
					 <cfif request.userid eq getincidentdetails.withdrawnto>
					 	<script type="text/javascript">	
							document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
						</script>
					 <table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
							What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this First Alert:</strong> <br><br>
						<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a>
						<!--- <a href="javascript:void(0);" onclick="return chkprj();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a> <br> --->
							</td>
						</tr>
					</table>
					 <cfelse>
					 <cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
					 	<script type="text/javascript">	
							document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
						</script>
					 <table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
							What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this First Alert:</strong> <br><br>
						<a href="javascript:void(0);" onclick="return chkprj('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return chkprj('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a>
						<!--- <a href="javascript:void(0);" onclick="return chkprj();" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a> <br> --->
							</td>
						</tr>
					</table>
					 <cfelse>
					 <script type="text/javascript">	
							document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
						</script>
					 <table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
							What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this First Alert:</strong> <br><br>
						&nbsp;&nbsp;&nbsp;Withdrawn</td>
						</tr>
					</table>
					</cfif>
					</cfif>
				<cfelse>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
				What&nbsp;Next...<br><br><cfif listfindnocase(request.userlevel,"OU Admin") eq 0>
				<strong class="bodytextsmpurple">You can take the following actions on this First Alert:</strong> <br><br>
				<cfif getincidentdetails.investigationlevel neq 2> 
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
				<cfelse>
					<cfif getincidentdetails.status eq "Sent For Review">
					<span class="bodytextsmpurple">Please use the review link on the Incident Report tab to advance this record</span><br>
					<cfelse>
					<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
					</cfif>
				</cfif>
				
					<br>
						</cfif>
						<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
						<cfif getincidentdetails.status neq "Closed">
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						
						</cfif>
							</td>
						</tr>
					</table>
				<cfelse>
					<cfif getincidentdetails.createdbyEmail eq request.userlogin>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
						<cfif getincidentdetails.status neq "Closed">
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						
							</cfif>
							</td>
						</tr>
					</table>
					</cfif>
				</cfif>
				</cfif>
				</cfif>
			<cfelseif trim(getFAdetail.status) eq "Approved" or trim(getFAdetail.status) eq "Declined" >
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin  or listfindnocase(request.userlevel,"Reviewer") gt 0>
						<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
						<cfif getincidentdetails.status neq "Closed">
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						
						</cfif>
							</td>
						</tr>
					</table>
					
			
				</cfif>
			<cfelseif trim(getFAdetail.status) eq "Sent for Approval">
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin  or listfindnocase(request.userlevel,"Reviewer") gt 0>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
				What&nbsp;Next...<br><br><cfif listfindnocase(request.userlevel,"OU Admin") eq 0>
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0><strong class="bodytextsmpurple">You can take the following actions on this First Alert:</strong> <br><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=SRreviewFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Review</a><br>
<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
						<br>
						</cfif>
						</cfif>
						<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
						<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
					<cfif getincidentdetails.status neq "Closed">
						<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
						</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						</cfif>
						<cfelse>
							<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
					<cfif getincidentdetails.status neq "Closed">
						
						
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						</cfif>
							<cfelse>
							&nbsp;&nbsp;&nbsp;Withdrawn
							</cfif>
						</cfif>
							</td>
						</tr>
					</table>
				<cfelse>
					<cfif getincidentdetails.createdbyEmail eq request.userlogin or listfindnocase(request.userlevel,"Reviewer") gt 0>
					<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
						<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
						<!--- <cfif getincidentdetails.createdbyEmail eq request.userlogin> --->
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
						<cfif getincidentdetails.status neq "Closed">
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
							</cfif>
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						</cfif>
				<cfelse>
					<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
						<cfif getincidentdetails.status neq "Closed">
							
							
							<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
						</cfif>
					<cfelse>
					&nbsp;&nbsp;&nbsp;Withdrawn
					</cfif>
				</cfif>
							</td>
						</tr>
					</table>
					
					</cfif>
				
				</cfif>
				</cfif>
			</cfif>
			
		</cfif>
	</cfif>
	</cfcase>
	<cfcase value="investigation">
		<cfif getincidentdetails.isworkrelated eq "yes">
			<cfif isdefined("getinvdetail.status")>
				<cfif getinvdetail.status eq "Started" or getinvdetail.status eq "More Info Requested">
					<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this investigation:</strong> <br><br>
								<!--- <a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
								 --->
								 <a href="javascript:void(0);" onclick="return submitsfrm('no','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
							<a href="javascript:void(0);" onclick="return submitsfrm('no','saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
						<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0><a href="javascript:void(0);" onclick="return submitsfrm('yes','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br></cfif>
								
								<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								
								</td>
							</tr>
						</table>
					<cfelseif  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
					<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
								
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
							</cfif>
								<cfelse>&nbsp;&nbsp;&nbsp;Withdrawn</cfif>
								</td>
							</tr>
						</table>
					</cfif>
				<cfelseif getinvdetail.status eq "IRP Pending" or getinvdetail.status eq "Approved">
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
							 <cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
							 <cfif getincidentdetails.status eq "Closed">
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
							<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
							</cfif>
							</cfif>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
								
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
							</cfif>
							<cfelse>
								<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>	
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
							</cfif>
								<cfelse>
								&nbsp;&nbsp;&nbsp;Withdrawn
								</cfif>
							
							</cfif>
								
								</td>
							</tr>
						</table>
					</cfif>
				<cfelseif getinvdetail.status eq "Sent for Review">
					 <cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
							<cfif request.userid eq getincidentdetails.withdrawnto>
																<script type="text/javascript">	
																	document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
																</script>
															<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
																<tr>
																	<td class="bodyTextPurple">
													What&nbsp;Next...<br><br>
													<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
																	<!--- <a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br> --->
																	<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
																	<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
																	<!--- 
																	<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br> --->
																	
																	</td>
																</tr>
															</table>
							
							<cfelse>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
							<script type="text/javascript">	
																	document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
																</script>
															<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
																<tr>
																	<td class="bodyTextPurple">
													What&nbsp;Next...<br><br>
													<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
																	<!--- <a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br> --->
																	<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
																	<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
																	<!--- 
																	<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br> --->
																	
																	</td>
																</tr>
															</table>
							<cfelse>
							<script type="text/javascript">	
								document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
							</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
							&nbsp;&nbsp;&nbsp;Withdrawn
								</td>
							</tr>
						</table>
						</cfif>
							</cfif>
	
	
						<cfelse>
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0>
								<script type="text/javascript">	
									document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
								</script>
										<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
											<tr>
												<td class="bodyTextPurple">
								What&nbsp;Next...<br><br>
								<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewInv','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Approve</a><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=moreinfoInv','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
												<!--- <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
												<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
												<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw&nbsp;Incident</a><br>
												</cfif> --->
												<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
							<cfif getincidentdetails.status neq "Closed">
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
												</td>
											</tr>
										</table>
							<cfelseif listfindnocase(request.userlevel,"Senior Reviewer") gt 0 and getincidentdetails.InvestigationLevel eq 2>
								<script type="text/javascript">	
									document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
								</script>
										<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
											<tr>
												<td class="bodyTextPurple">
								What&nbsp;Next...<br><br>
								<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewInv','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Approve</a><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=moreinfoInv','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br>
												<!--- <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
												<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
													<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
												<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw&nbsp;Incident</a><br>
												</cfif> --->
												<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<cfif getincidentdetails.status neq "Closed">
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>

												</td>
											</tr>
										</table>
							<cfelseif getincidentdetails.createdbyEmail eq request.userlogin>
							<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> --->
								 <a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<cfif getincidentdetails.status neq "Closed">
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								
								</td>
							</tr>
						</table>
							
									</cfif>
					
					</cfif>
					
					
					
					
					
					
				<!--- <cfelseif getinvdetail.status eq "More Info Requested">
					<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin or listfindnocase(request.userlevel,"Reviewer") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
								<!--- <a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
								 ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('yes','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw&nbsp;Incident</a><br>
								</cfif>
								</td>
							</tr>
						</table>
					<cfelseif listfindnocase(request.userlevel,"Senior Reviewer") gt 0 and getincidentdetails.InvestigationLevel eq 2>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
								<!--- <a href="javascript:void(0);" onclick="return submitsfrm('no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
								 ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('yes','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
								<!--- <a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=cancel&irn=#getincidentdetails.irn#" class="bodytextsmpurple"> ---><a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw&nbsp;Incident</a><br>
								</cfif>
								</td>
							</tr>
						</table>
					</cfif> --->
				</cfif>
			</cfif>
		</cfif>
	</cfcase>
	<cfcase value="IRP">
		<cfif listfindnocase("Complete,Approved,Sent for Review",getirpdetail.status) eq 0>
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
					<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this IRP:</strong> <br><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('no','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('no','saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('yes','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br>
								<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
									<cfif getincidentdetails.status neq "Closed">
				
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								<cfelse>
									<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
									What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this IRP:</strong> <br><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('no','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
								<a href="javascript:void(0);" onclick="return submitsfrm('no','saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
								<!--- <a href="javascript:void(0);" onclick="return submitsfrm('yes','saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Send&nbsp;for&nbsp;Review</a><br> --->
								<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
									<cfif getincidentdetails.status neq "Closed">
				
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
									<cfelse>
									What&nbsp;Next...<br><br>
									<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
										&nbsp;&nbsp;&nbsp;Withdrawn
									</cfif>
								
								</cfif>
								</td>
							</tr>
						</table>
			</cfif>
		<cfelseif listfindnocase("Complete,Approved",getirpdetail.status) gt 0>
			<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
			<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
				<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
									<cfif getincidentdetails.status neq "Closed">
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								</td>
							</tr>
						</table>
			
			
		<cfelseif getirpdetail.status eq "Sent for Review">
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
				<strong class="bodytextsmpurple">You can take the following actions on this incident:</strong> <br><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewIRP','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Approve</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoIRP','Info','500','420','no');"  class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Request&nbsp;More&nbsp;Info</a><br><br>
						
					
					</cfif>
								<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<cfif getincidentdetails.status neq "Closed">
						
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								<cfelse>
									<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
				<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<cfif getincidentdetails.status neq "Closed">
						
								
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
									<cfelse>
									&nbsp;&nbsp;&nbsp;Withdrawn
									</cfif>
									
								</cfif>
								</td>
							</tr>
						</table>
			<cfelseif getincidentdetails.createdbyEmail eq request.userlogin>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
						<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<cfif getincidentdetails.status neq "Closed">
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								</td>
							</tr>
						</table>
			</cfif>
		</cfif>
		
	</cfcase>
	<cfcase value="CAPA">
		<cfif getincidentdetails.status eq "Closed">
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin>
				<script type="text/javascript">	
					document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
				</script>
					<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
			<tr>
				<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
						<strong class="bodytextsmpurple">You can take the following actions on this CA/PA:</strong> <br><br>
						<cfif getincidentdetails.status eq "Closed">
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=reopen&irn=#getincidentdetails.irn#" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Re-open</a><br>
						</cfif>
						</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=corr&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Corrective&nbsp;Action</a><br>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.raiseCAPA&irn=#irn#&action=raise&type=prev&capafromlnk=CAPA','Info','800','500','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Raise&nbsp;Preventive&nbsp;Action</a><br>
								<cfif getincidentdetails.status neq "Closed"><br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
								
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
								</cfif>
								</td>
							</tr>
						</table>
			</cfif>
		<cfelse>
			<script type="text/javascript">	
				document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
			</script>
			<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
			<tr>
				<td class="bodyTextPurple">
				What&nbsp;Next...<br><br>
				<strong class="bodytextsmpurple">You can take the following actions on this CA/PA:</strong> <br><br>
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
				<a href="javascript:void(0);" onclick="return submitsfrm('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return submitsfrm('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
				
				<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
				<cfif getincidentdetails.status neq "Closed">
							<cfif listfindnocase("Started,Initiated",getincidentdetails.status) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
				</cfif>
				<cfelse>
					<cfif request.userid eq getincidentdetails.withdrawnto or  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
					<a href="javascript:void(0);" onclick="return submitsfrm('saveandclose');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save&nbsp;&&nbsp;Close</a><br>
				<a href="javascript:void(0);" onclick="return submitsfrm('saveonly');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Save</a><br>
				
				<br><strong class="bodytextsmpurple">You can take the following actions on this record:</strong> <br><br>
				<cfif getincidentdetails.status neq "Closed">
							<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
								<a href="index.cfm?fuseaction=incidents.incidentmanament&submittype=withdraw&irn=#getincidentdetails.irn#&type=initiator" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Withdraw</a><br>
								</cfif>
								<a href="javascript:void(0);" onclick="NewWindow('#self#?fuseaction=incidents.cancel&irn=#getincidentdetails.irn#','Info','500','420','no');" class="bodytextsmpurple"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">Cancel</a><br>
				</cfif>
					<cfelse>
					&nbsp;&nbsp;&nbsp;Withdrawn
					</cfif>
				
				</cfif>
				</td>
			</tr>
			</table>
		</cfif>
	</cfcase>
	<cfcase value="audit">
		<!--- audit --->
	</cfcase>
</cfswitch>
	<cfif listfindnocase("reviewinvestigation,reviewirn,raiseCAPA,adddateval,fieldaudit,cancel,addcontractor",fusebox.fuseaction) eq 0>
	
	<script type="text/javascript">	
				document.getElementById("mainlefttdcell").style.backgroundColor='a2df1b';
			</script>
			<table bgcolor="a2df1b" cellpadding="3" cellspacing="0" width="100%">
			<tr>
				<td class="bodyTextPurple"><br>
				<strong class="bodytextsmpurple">HSSE Incident Reporting Procedure</strong><br>
	<a href="https://conexlibrary.amecfw.com/Lists/ConexLibrary/HSE-PRO-100001.pdf" target="_blank" style="color:5f2468;font-size:10pt;font-family:Segoe UI;text-decoration:none;vertical-align:-21px;"><img src="images/navarrow.jpg" border="0" style="vertical-align:-1;padding-right:3px;">HSE&##8209;PRO&##8209;100001</a>
				</td>
			</tr>
		</table></cfif>
		
</td>
<cfelse>
<td>&nbsp;</td>
</cfif>
</cfif>
<td valign="top" width="100%">#Fusebox.layout#</td></tr></table>
</cfif>
</cfif>
</cfoutput>


