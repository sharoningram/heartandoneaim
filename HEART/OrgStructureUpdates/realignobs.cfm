<cfquery name="getobs" datasource="heart_ds">
SELECT top 500 ObservationNumber, ProjectNumber, ObservationBU, ObservationOU, BusinessStream, BUID, OUID
FROM            Observations
where wasrealigned = 0
</cfquery>

<cfoutput query="getobs">
	<cfquery name="getalign" datasource="oneaim">
		SELECT       Groups.Business_Line_ID, Groups.OUid, NewDials_1.Name AS buname, NewDials.Name AS ouname, NewDials_2.Name as bsname
		FROM            NewDials AS NewDials_1 INNER JOIN
                         Groups ON NewDials_1.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
		where Groups.group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ProjectNumber#">
	</cfquery>
	WAS = #ProjectNumber# #ObservationBU#, #ObservationOU#, #BusinessStream#, #BUID#, #OUID#<br>
	IS = #getalign.buname# #getalign.ouname# #getalign.bsname# #getalign.business_line_id# #getalign.ouid#<br><Br>
	<cfif getalign.recordcount gt 0>
	
	<cfquery name="updateobs" datasource="heart_ds">
		update Observations
		set  ObservationBU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getalign.buname#">, 
		ObservationOU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getalign.ouname#">, 
		BusinessStream = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getalign.bsname#">, 
		BUID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getalign.business_line_id#">, 
		OUID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getalign.ouid#">,
		wasrealigned =  1
		where ObservationNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ObservationNumber#">
	</cfquery>
	</cfif>
</cfoutput>