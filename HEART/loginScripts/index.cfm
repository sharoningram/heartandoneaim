<cfparam name="app" default="">
<cfparam name="qs" default="">
<cfparam name="msg" default="">
<cfcookie name="nonmyfwemail" expires="now"> 
<cftry>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 

"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

 <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta http-equiv="cache-control" content="no-cache,no-store"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="expires" content="-1"/>
        <meta name='mswebdialog-title' content='Connecting to AMEC Single Sign On'/>
		<link rel="apple-touch-icon" sizes="114x114" href="HeartMobile.png"/>
		<link rel="apple-touch-icon" sizes="72�72" href="HeartMobile72.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="HeartMobile57.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="HeartMobile144.png" />
        <title>Heart</title>

<style>
* {
	margin:0px;
	padding:0px;
}
html, body
{
    height:100%;
    width:100%;
    background-color:#ffffff;
	
    color:#000000;
    font-weight:normal;
    font-family:"Segoe UI" , "Segoe" , "SegoeUI-Regular-final", Tahoma, Helvetica, Arial, sans-serif;
    min-width:500px;
    -ms-overflow-style:-ms-autohiding-scrollbar;
}

body
{
    font-size:0.9em;
}

#noScript { margin:16px; color:Black; }

:lang(en-GB){quotes:'\2018' '\2019' '\201C' '\201D';}
:lang(zh){font-family:微软雅黑;}

@-ms-viewport { width: device-width; }
@-moz-viewport { width: device-width; }
@-o-viewport { width: device-width; }
@-webkit-viewport { width: device-width;  }
@viewport { width: device-width; }

/* Theme layout styles */

#fullPage, #brandingWrapper
{
    width:100%;
    height:100%;
    background-color:inherit;
}
#brandingWrapper
{
    background-color:#ffffff;
}
#branding
{       
    /* A background image will be added to the #branding element at run-time once the illustration image is configured in the theme. 
       Recommended image dimensions: 1420x1200 pixels, JPG or PNG, 200 kB average, 500 kB maximum. */
    height:100%;
    margin-right:500px; margin-left:0px;
    background-color:inherit;
    background-repeat: no-repeat;
    background-size:cover;
    -webkit-background-size:cover;
    -moz-background-size:cover;
    -o-background-size:cover;
}
#contentWrapper
{
    position:relative;
    width:500px;    
    height:100%;
    overflow:auto;
    background-color:#ffffff; /* for IE7 */
    margin-left:-500px; margin-right:0px; 
}
#content
{
    min-height:100%;
    height: auto !important;
    margin:0 auto -55px auto;
    padding:0px 150px 0px 50px;
}
#header
{
    font-size:2em;
    font-weight:lighter;
    font-family:"Segoe UI Light" , "Segoe" , "SegoeUI-Light-final", Tahoma, Helvetica, Arial, sans-serif;
    padding-top: 90px;
    margin-bottom:60px;
    min-height:100px;
    overflow:hidden;
}
#header img
{
    /* Logo image recommended dimension: 60x60 (square) or 350X35 (elongated), 4 kB average, 10 kB maximum. Transparent PNG strongly recommended. */
    width:auto;
    height:auto;
}
#workArea, #header
{
    word-wrap:break-word;
    width:350px;
}
#workArea
{
    margin-bottom:90px;
}
#footerPlaceholder
{
    height:40px;
}
#footer
{
    height:40px;
    padding:10px 50px 0px 50px;
    position:relative;
    color:#666666;
    font-size:0.78em;
}
#footerLinks
{
    float:none;
    padding-top:10px;
}
#copyright {color:#696969;}
.pageLink { color:#000000; padding-left:16px; }

/* Common content styles */

.clear {clear:both;}
.float { float:left; } 
.floatReverse { float:right; } 
.indent { margin-left:16px; } 
.indentNonCollapsible { padding-left:16px; }
.hidden {display:none;}
.notHidden {display:inherit;}
.error { color:#c85305; }
.actionLink { margin-bottom:8px; display:block; }
a
{
    color:#2672ec;
    text-decoration:none;
    background-color:transparent;
}
ul { list-style-type: disc; }
h1,h2,h3,h4,h5,label { margin-bottom: 8px; }
.submitMargin { margin-top:38px; margin-bottom:30px; }
.topFieldMargin { margin-top:8px; }
.fieldMargin { margin-bottom:8px; }
.groupMargin { margin-bottom:30px; } 
.sectionMargin { margin-bottom:64px; }
.block { display: block; }
.autoWidth { width:auto; }
.fullWidth { width:342px; }
.fullWidthIndent { width:326px; }
input
{
    max-width:100%; 
    font-family:inherit;
    margin-bottom:8px;
}
input[type="radio"], input[type="checkbox"] {
    vertical-align:middle;
    margin-bottom: 0px;
}
span.submit, input[type="submit"]
{
    border:none;
    background-color:rgb(38, 114, 236);
    min-width:80px;
    width:auto;
    height:30px;
    padding:4px 20px 6px 20px;
    border-style:solid;
    border-width:1px;
    transition:background 0s;
    color:rgb(255, 255, 255);
    cursor:pointer;
    margin-bottom:8px;
    
    -ms-user-select:none;
    -moz-transition:background 0s;
    -webkit-transition:background 0s;
    -o-transition:background 0s;
    -webkit-touch-callout:none;
    -webkit-user-select:none;
    -khtml-user-select:none;
    -moz-user-select: none;
    -o-user-select: none;
    user-select:none;
}
input[type="submit"]:hover,span.submit:hover
{
    background: rgb(212, 227, 251);
}
input.text{
    height:28px;
    padding:0px 3px 0px 3px ;
    border:solid 1px #BABABA;
}
input.text:focus
{
  border: 1px solid #6B6B6B;
}
select
{
    height:28px;
    min-width:60px;
    max-width:100%; 
    margin-bottom:8px;

    white-space:nowrap;
    overflow:hidden;
    box-shadow:none;
    padding:2px;
    font-family:inherit;
}
h1, .giantText
{
   font-size:2.0em; 
   font-weight:lighter;
}          
h2, .bigText
{
   font-size:1.33em;
   font-weight:lighter;
}
h3, .normalText
{
    font-size:1.0em;
    font-weight:normal;
}
h4, .smallText
{
    font-size:0.9em;
    font-weight:normal;
}
h5, .tinyText
{
    font-size:0.8em;
    font-weight:normal;
}
.hint
{
    color:#999999;
}
.emphasis
{
    font-weight:700;
    color:#2F2F2F;
} 
.smallIcon
{
    height:20px;
    padding-right:12px;
    vertical-align:middle;
}
.largeIcon
{
    height:48px;
    /* width:48px; */
    vertical-align:middle;
}
.largeTextNoWrap
{
    height:48px;
    display:table-cell; /* needed when in float*/
    vertical-align:middle;
    white-space:nowrap;
    font-size:1.2em;
}
.idp
{
    height:48px;
    clear:both;
    padding:8px;
    overflow:hidden;
}
.idp:hover 
{
    background-color:#cccccc;
}
.idpDescription
{
    width:80%;
}

/* Form factor: intermediate height layout. Reduce space of the header. */
@media only screen and (max-height: 820px) {
    #header {
        padding-top: 40px;
        min-height:0px;
        overflow: hidden;
    }

    #workArea
    {
        margin-bottom:60px; 
    }
}

/* Form factor: intermediate height layout. Reduce space of the header. */
@media only screen and (max-height: 500px) {
    #header {
        padding-top: 30px;
        margin-bottom: 30px;
    }
    #workArea{
        margin-bottom:40px; 
    }
}

/* Form factor: intermediate layout (WAB in non-snapped view falls in here) */
@media only screen and (max-width: 600px) {
    html, body {
        min-width: 260px;
    }

    #brandingWrapper {
        display: none;
    }

    #contentWrapper {
        float: none;
        width: 100%;
        margin: 0px auto;
    }

    #content, #footer, #header {
        width: 400px;
        padding-left: 0px;
        padding-right: 0px;
        margin-left: auto;
        margin-right: auto;
    }

    #workArea {
        width: 100%;
    }

    .fullWidth {
        width: 392px;
    }

    .fullWidthIndent {
        width: 376px;
    }
}

@media only screen and (max-width: 450px) {
    body {
        font-size: 0.8em;
    }

    #content, #footer {
        width:auto;
        margin-right:33px;
        margin-left:25px;
    }

    #header {
        width: auto;
    }

    span.submit, input[type="submit"] {
        font-size: 0.9em;
    }

    .fullWidth
    {
        width:100%;
        margin-left:auto;
        margin-right:auto;
    }

    .fullWidthIndent {
        width: 85%;
    }

    .idpDescription
    {
        width:70%;
    }
}

/* Form factor: snapped WAB (for WAB to work in snapped view, the content wrapper width has to be set to 260px) */
@media only screen and (max-width:280px)
{
    #contentWrapper
    {
        width:260px;
    }
    .idpDescription
    {
        max-width:160px;
        min-width:100px;
    }
}
</style>
<!--- <link rel="stylesheet" type="text/css" href="https://federation.amec.com/adfs/portal/css/style.css?id=D74D4D6943F32AE6F7F11D14D601DBB0E1A58919176EE512150366B6279AAF99" /> --->
<!--- <style>.illustrationClass {background-image:url("/logintest1/illustration.jpg");}</style> --->
</head>

 <body dir="ltr" class="body">
    <div id="noScript" style="position:static; width:100%; height:100%; z-index:100">
        <h1>JavaScript required</h1>
        <p>JavaScript is required. This web browser does not support JavaScript or JavaScript in this web browser is not enabled.</p>
        <p>To find out if your web browser supports JavaScript or to enable JavaScript, see web browser help.</p>
    </div>
    <script type="text/javascript" language="JavaScript">
         document.getElementById("noScript").style.display = "none";
    </script>
    <div id="fullPage">
        <div id="brandingWrapper" class="float">
            <div id="branding"></div>
        </div>
        <div id="contentWrapper" class="float">
            <div id="content">
                <div id="header">
                      <img class="logoImage" src="../images/logoLong.jpg" border="0" alt="Single Sign On"/>
                </div>
                <div id="workArea">
                    
    <div id="authArea" class="groupMargin">
        
        
    <div id="loginArea">        
        <div id="loginMessage" class="groupMargin">
		<cfif trim(msg) neq ''>
		<span style="color:red;">Login Failed, Please Try Again</span><br>
		</cfif>
		
		Sign in with your organizational account</div>
		<cfif not isdefined("cookie.useremail")>
<cfform name="loginappaccess" method="post" action="login.cfm">
	<cfoutput><input type="hidden" name="qs" value="#qs#"></cfoutput>
    <div id="userNameArea">
	<cfinput type="email" name="username" required="Yes" message="Please enter your email address"  value="" tabindex="1" class="text fullWidth" 
                        spellcheck="false" placeholder="someone@example.com" autocomplete="off"/>
	</div>
	 <div id="passwordArea">
	<cfinput type="password" name="password" required="Yes" message="Please enter your password" tabindex="2" class="text fullWidth" 
                        placeholder="Password" autocomplete="off"/>
	
	</div>
	<!--- <input type="submit" value="Log in"> --->
	  <div id="submissionArea" class="submitMargin">
	  	<input type="submit" id="submitButton" class="submit" value="Sign in">
                
                </div>
	</cfform>
<cfelseif trim(cookie.useremail) eq "">
<cfform name="loginappaccess" method="post" action="login.cfm">
	<cfoutput><input type="hidden" name="qs" value="#qs#"></cfoutput>
 <div id="userNameArea">
	<cfinput type="email" name="username" required="Yes" message="Please enter your email address"  value="" tabindex="1" class="text fullWidth" 
                        spellcheck="false" placeholder="someone@example.com" autocomplete="off"/>
	</div>
	 <div id="passwordArea">
	<cfinput type="password" name="password" required="Yes" message="Please enter your password" tabindex="2" class="text fullWidth" 
                        placeholder="Password" autocomplete="off"/>
	
	</div>
	
	  <div id="submissionArea" class="submitMargin">
	  	<input type="submit" id="submitButton" class="submit" value="Sign in">
                
                </div>
	
	</cfform>
<cfelse>
	<cflocation url="login.cfm" addtoken="No">
</cfif>

             <div id="authOptions">
  
      </div>

        <div id="introduction" class="groupMargin">
            <p><h5>You have accessed a Private and Restricted computing system. Continuation of login implies you have read and understood the Company's computer usage guidelines.</h5></p>                     
        </div>
<cfcatch type="Any">
<cfinclude template="../appconfig.cfm">
<cfmail to="#erroremail#" from="#HeartEmail#" subject="HEART error" type="html">
	Errors occurred during upload:<br><br>
	#cfcatch.type#<br>
	#cfcatch.message#<br>#cfcatch.detail#<br><br>
	
	<cfdump var="#cfcatch.tagcontext#">
</cfmail>	

<table width="100%" cellpadding="2" cellspacing="0" border="0">


<tr>
	<td colspan="2" align="center" style="font-family:Segoe UI;color:0076cd;font-size:12pt;"><br><br>
We're sorry, there seems to an error accessing the page you have requested. <br>
A site administrator has been notified and will be working to correct the problem.<br><br>
<cfoutput><a href="/#heartroot#" style="font-family:Segoe UI;font-size:12pt;">Return to HEART</a></cfoutput>
	</td>
</tr>
</table>

</cfcatch>
</cftry>