Application Name: oneAIM
Written using Fusebox Methodology.
Fusebox circuits are aligned with main navigation options in the header. They are listed in the fxb_circuits.cfm file.
The Shared folder contains code that is available throughout the application (header, footer, specially used modules, css and javascript). It contains a subfolder named Info. This folder has an index.cfm that contains a case statement directing the application to open the selected information file for the information pop ups throughtout the application.
The TransferScripts folder contains scripts that were used one time to transfer legacy data from the old Safety (PRS) application to the new suite.
The ScheduledTasks folder contains scripts that are run on a scheduled basis in CFAdmin.
The Uploads folder houses files that are uploaded and processed by the application.
The main css file /Shared/css/epr_local.css contains all styles and handling for responsive design.