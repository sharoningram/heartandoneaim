<p style="font-family:Segoe UI;font-size:11pt;">If you work for Amec Foster Wheeler plc you will need to select an Project Group (PG) with whom you share an office/building. This will be the PG that you are physically closest to. 
<br><br>The exception to this will be for those working at either Knutsford or Old Change House. The options for these offices will appear at the top of this selection list. </p>
<p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p>
