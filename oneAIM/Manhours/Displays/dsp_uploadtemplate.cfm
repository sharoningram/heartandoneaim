<div class="content1of1">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="mytab">
<tr>
	<td colspan="2"> <span class="bodyTextWhite"><strong>Man Hour Upload Template</strong></span></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">	
				<tr>
					<td class="bodytext"><strong>For best results download a new copy of the template prior to your upload of data. Make sure to read all instructions listed below.<br><br></strong>
Please use this link to download the Upload Man Hours template: <cfoutput><a href="/#getappconfig.oneAIMpath#/uploads/manhours/templates/manhourtemplate.csv">Download Template</a></cfoutput><br><br>
<strong>Instructions:</strong><br><br>
The template is a .CSV file and can be used to update Man Hour information. DO NOT make any changes to this template. Begin entering your data on the second row. DO NOT add any tabs to the workbook, use only the one with pre-defined column headers. The file will be processed and man hours will be adjusted for the month/year listed within the template. If the project group selected uses an ERP system for man hour collection only the manually adjustable man hour will be updated. For example, if the project group selected uses the JDE ERP system for man hour collection only the sub contractor and non tabulated man hours will adjusted, the other man hour fields will be ignored. This upload will update man hours for the selected project group only, individual contractor or joint venture company man hours must update separately.<br><br></td>
				</tr>
				<tr>
					<td>
						<table cellpadding="3" cellspacing="1" border="0" class="selected_tab">
							<tr>
								<Td align="center" width="18%" class="bodytext"><strong>Template&nbsp;Fields<br></strong>(fields&nbsp;in&nbsp;RED&nbsp;are&nbsp;required)</td>
								<td align="center" class="bodytext"><strong>Field&nbsp;Description</strong></td>
							</tr>
							<tr bgcolor="ffffff">
								<Td><strong class="bodytext" style="color:red;">Month</strong></td>
								<td class="bodytext" >Month for which man hours are being updated. May be either the month number (1,2,3,4,5,6,7,8,9,10,11,12) or month fully spelled out (January, February, March, April, etc.)</td>
							</tr>
							<tr >
								<Td class="bodytext" ><strong style="color:red;">Year</strong></td>
								<td class="bodytext" >Year for which man hours are being updated. Must be a 4-digit year (2013,2014,2015,etc.)</td>
							</tr>
							<tr bgcolor="ffffff">
								<Td class="bodytext" ><strong >Overhead</strong></td>
								<td class="bodytext" >The number of overhead hours being updated. This number will replace the existing overhead hours for the month/year selected. Blank values will be ignored, zeros (0) will replace the existing overhead hours with zero hours. This field may be ignored depending on the ERP system used by the selected project group.</td>
							</tr>
							<tr >
								<Td class="bodytext" ><strong>HomeOffice</strong></td>
								<td class="bodytext" >The number of home office hours being updated. This number will replace the existing home office hours for the month/year selected. Blank values will be ignored, zeros (0) will replace the existing home office hours with zero hours. This field may be ignored depending on the ERP system used by the selected project group.</td>
							</tr>
							<tr bgcolor="ffffff">
								<Td class="bodytext" ><strong>Field</strong></td>
								<td class="bodytext" >The number of field hours being updated. This number will replace the existing field hours for the month/year selected. Blank values will be ignored, zeros (0) will replace the existing field hours with zero hours. This field may be ignored depending on the ERP system used by the selected project group.</td>
							</tr>
							<tr >
								<Td class="bodytext" ><strong>Craft</strong></td>
								<td class="bodytext" >The number of craft hours being updated. This number will replace the existing craft hours for the month/year selected. Blank values will be ignored, zeros (0) will replace the existing craft hours with zero hours. This field may be ignored depending on the ERP system used by the selected project group.</td>
							</tr>
							<tr bgcolor="ffffff">
								<Td class="bodytext" ><strong>Subcontractor</strong></td>
								<td class="bodytext" >The number of subcontractor hours being updated. This number will replace the existing subcontractor hours for the month/year selected. Blank values will be ignored, zeros (0) will replace the existing subcontractor hours with zero hours. This field may be ignored depending on the ERP system used by the selected project group.</td>
							</tr>
							<tr >
								<Td class="bodytext" ><strong>NonTabulated</strong></td>
								<td class="bodytext" >The number of non-tabulated hours being updated. This number will replace the existing non-tabulated hours for the month/year selected. Blank values will be ignored, zeros (0) will replace the existing non-tabulated hours with zero hours. This field may be ignored depending on the ERP system used by the selected project group.</td>
							</tr>
							
						</table>
				</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>