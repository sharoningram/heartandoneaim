<cfparam name="lookupmonth" default="0">

<cfif lookupmonth neq 0>
	<cfquery name="inactold" datasource="#request.dsn#">
			update Population
			set isactive = 0
			WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">) AND (LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">) 
			 AND (YEAR(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">)
	</cfquery>
	<cfquery name="inactold" datasource="#request.dsn#">
			update ContractorPopulation
			set isactive = 0
			WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">) AND (LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">) 
			 AND (YEAR(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">)
	</cfquery>
	<cfloop list="#lookupmonth#" index="i">
		<cfif i eq listgetat(lookupmonth,listlast(lookupmonth)) and lookupyear eq year(now())>
			<cfset curractive = 1>
		<cfelse>
			<cfset curractive = 0>
		</cfif>
		<cfset oktorun = "No">
		
		<!--- <cfif lookupyear eq year(now())>
			<cfif i gte month(now())-1>
				<cfset oktorun = "yes">
			</cfif>
		<cfelseif lookupyear eq year(now())-1>
			<cfif i eq 12>
				<cfset oktorun = "yes">
			</cfif>
		</cfif>
		<cfif oktorun> --->
			<cfquery name="getcurrpoprec" datasource="#request.dsn#">
				SELECT       PopRecID
				FROM            Population
				WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">) AND (LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">) AND (YEAR(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) AND (MONTH(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
			</cfquery>
			
				<cfif trim(form["afwpop_#i#"]) eq ''>
					<cfset useafw = 0>
				<cfelse>
					<cfset useafw = form["afwpop_#i#"]>
				</cfif>
				<cfif trim(form["mcpop_#i#"]) eq ''>
					<cfset usemc = 0>
				<cfelse>
					<cfset usemc = form["mcpop_#i#"]>
				</cfif>
				<cfif trim(form["subpop_#i#"]) eq ''>
					<cfset usesub = 0>
				<cfelse>
					<cfset usesub = form["subpop_#i#"]>
				</cfif>
				<cfif trim(form["jvpop_#i#"]) eq ''>
					<cfset usejv = 0>
				<cfelse>
					<cfset usejv = form["jvpop_#i#"]>
				</cfif>
				
			
			
			<cfset recdate = createdate(lookupyear,i,1)>
			
			<cfif getcurrpoprec.recordcount eq 0>
				<cfquery name="addpop" datasource="#request.dsn#">
					insert into Population (GroupNumber, NumEmployedAFW, NumEmployedMC, NumEmployedSub, NumEmployedJV, PopDate, UpdateBy, UpdateDate, LocationID, isactive)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">,<cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#useafw#">,<cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usemc#">,<cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usesub#">,<cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usejv#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#recdate#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">, <cfqueryparam cfsqltype="CF_SQL_BIT" value="#curractive#">)
				</cfquery>
			<cfelse>
				<cfquery name="updatepop" datasource="#request.dsn#">
					update Population 
					set  NumEmployedAFW = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#useafw#">,
					 NumEmployedMC = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usemc#">, 
					 NumEmployedSub = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usesub#">, 
					 NumEmployedJV = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usejv#">,
					  UpdateBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">, 
					  UpdateDate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">,
					  isactive =  <cfqueryparam cfsqltype="CF_SQL_BIT" value="#curractive#">
					 where PopRecID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurrpoprec.PopRecID#">
				</cfquery>
			</cfif>
			
			
			
			<cfloop list="#form.fieldnames#" index="o">
			<cfif o contains "SUBCOPOP_#i#">
				<cfif listlen(o,"_") eq 3>
					<cfset submon = listgetat(o,2,"_")>
					<cfset coid = listgetat(o,3,"_")>
					
					<cfquery name="getconcurrpop" datasource="#request.dsn#">
						SELECT        ContractorPopID
						FROM            ContractorPopulation
						WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">) AND (LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">) AND (YEAR(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) AND (MONTH(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">) and ContractorID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#coid#">		
					</cfquery>
					<cfif trim(form[o]) eq ''>
						<cfset usecoval = 0>
					<cfelse>
						<cfset usecoval = form[o]>
					</cfif>
					<cfif getconcurrpop.recordcount eq 0>
						<cfquery name="addpop" datasource="#request.dsn#">
							insert into ContractorPopulation (ContractorID, NumEmployedCon, DateUpdated, UpdateBy, GroupNumber, PopDate, LocationID, isActive)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#coid#">, <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usecoval#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#recdate#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">, <cfqueryparam cfsqltype="CF_SQL_BIT" value="#curractive#">)
						</cfquery>
					<cfelse>
						<cfquery name="updatepop" datasource="#request.dsn#">
							update ContractorPopulation 
							set  NumEmployedCon = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#usecoval#">, 
								DateUpdated = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">, 
								UpdateBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">, 
								isActive = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#curractive#">
							where ContractorPopID =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getconcurrpop.ContractorPopID#">
						</cfquery>
					</cfif>
				
				</cfif>
			</cfif>
			</cfloop>
		
		<!--- </cfif> --->
	</cfloop>
	

</cfif>
<cfoutput>
		<form action="#self#?fuseaction=#attributes.xfa.population#" method="post" name="returnfrm">
				<input type="hidden" name="groupnum" value="#groupnumber#">
				<input type="hidden" name="site" value="#site#">
				<input type="hidden" name="lookupyear" value="#lookupyear#">
				<input type="hidden" name="lookupmonth" value="#lookupmonth#">
			</form>
			<script type="text/javascript">
				document.returnfrm.submit();
			</script>
	</cfoutput> 