<cfif isdefined("getgroupinfo.recordcount")>
	<cfset conidlist = "0">
	<cfquery name="getsubandjv" datasource="#request.dsn#">
		SELECT     ContractorID, ContractorName, CoType
		FROM         Contractors
		WHERE   status = 1 and  (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">)
		ORDER BY CoType desc, ContractorName
	</cfquery>
	<cfset mgdstruct = structnew()>
	<cfset jvhrsstruct = structnew()>
	
	<cfset hostruct = structnew()>
	<cfset fieldstruct = structnew()>
	<cfset craftstruct = structnew()>
	<cfset overheadstruct = structnew()>
	<cfset substruct = structnew()>
	<cfset nontabstruct = structnew()>

	<cfif getsubandjv.recordcount gt 0>
		<cfset conidlist = valuelist(getsubandjv.ContractorID)>
	</cfif>
	<cfif listlen(conidlist) gt 0>
		<cfloop list="#conidlist#" index="o">
			<cfset evaluate("jvsubstruct#o# = structnew()")>
		</cfloop>
	</cfif>
	<cfset thisYear = Year(now())>
<!--- Finds the first friday after today's date. --->
	<cfset today = CreateDate(thisYear,Month(now()), Day(now()))>
	<cfloop condition="#DayofWeekAsString(DayOfWeek(today))# NEQ 'Friday'">
		<cfset today = DateFormat(DateAdd('d', 1, today), "mm/dd/yy")>
	</cfloop>

 	<cfset thisWeek = DateFormat(today, "mm/dd/yy")> 
	<cfset DateBegin = CreateDate(2005,1,1)>
	<cfset WK5=dateformat(thisWeek,"mm/dd/yy")>
	<cfset WK1=dateformat(dateAdd('WW',  -4,  WK5),"mm/dd/yy")>
	<cfset WK2=dateformat(dateAdd('WW',  -3,  WK5),"mm/dd/yy")>
	<cfset WK3=dateformat(dateAdd('WW',  -2,  WK5),"mm/dd/yy")>
	<cfset WK4=dateformat(dateAdd('WW',  -1,  WK5),"mm/dd/yy")>
	<cfset fivewklist = "#wk1#,#wk2#,#wk3#,#wk4#,#wk5#">
	<cfset needprevyear = "no">
	<cfif month(wk1) eq 12 and year(wk1) eq year(now())-1>
		<cfset needprevyear = "yes">
	</cfif>

</cfif>