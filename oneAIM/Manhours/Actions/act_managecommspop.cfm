<cfif trim(submittype) neq ''>
	<cfswitch expression="#submittype#">
		<cfcase value="add">
			<cfquery name="addcomm" datasource="#request.dsn#">
				insert into PopulationComments (MHtype, MHdate, EnteredBy, dateEntered, GroupNumber, MHrow, ContractorID, MHcomment, LocationID,EnteredbyName)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mtype#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#mhd#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnum#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mrow#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractor#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mhcomment#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#loc#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.lname#, #request.fname#">)
			</cfquery>
			<cfoutput>
			<cfif mtype eq "month">
				<script type="text/javascript">
					window.opener.document.getElementById('img_#mrow#_#gnum#_#loc#_#month(mhd)#_#contractor#').src = "images/noteEdit.png";
				</script>
			<cfelseif mtype eq "weekly">
				<script type="text/javascript">
					window.opener.document.getElementById('img_#mrow#_#gnum#_#loc#_#month(mhd)#_#day(mhd)#_#contractor#').src = "images/noteEdit.png";
				</script>
			</cfif>
			</cfoutput>
			<script type="text/javascript">
				window.close();
			</script>
		</cfcase>
		<cfcase value="edit">
			<cfparam name="commid" default="0">
			<cfif commid neq 0>
				<cfquery name="editcomm" datasource="#request.dsn#">
					update PopulationComments
					set 
					dateEntered = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, 
					MHcomment = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mhcomment#">
					where commid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#commid#">
				
				</cfquery>
			</cfif>
			<cfoutput>
			<cfif mtype eq "month">
				<script type="text/javascript">
					window.opener.document.getElementById('img_#mrow#_#gnum#_#loc#_#month(mhd)#_#contractor#').src = "images/noteEdit.png";
				</script>
			<cfelseif mtype eq "weekly">
				<script type="text/javascript">
					window.opener.document.getElementById('img_#mrow#_#gnum#_#loc#_#month(mhd)#_#day(mhd)#_#contractor#').src = "images/noteEdit.png";
				</script>
			</cfif>
			</cfoutput>
			<script type="text/javascript">
				window.close();
			</script> 
		</cfcase>
	</cfswitch>
</cfif>
