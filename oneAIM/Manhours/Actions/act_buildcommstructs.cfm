<cfset commstruct = {}>

<cfif isdefined("getgroupinfo.recordcount")>

<cfquery name="getindivcomments" datasource="#request.dsn#">
SELECT DISTINCT MONTH(MHdate) AS commmonth, Day(MHdate) AS commday,MHrow, ContractorID
FROM            MHcomments
WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnum#">) AND (LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">)

<cfif fusebox.fuseaction eq "enter">
 AND (MHtype = 'weekly') 
		<cfif needprevyear eq "yes">
 			AND (YEAR(MHdate)		IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 				<cfelse>
  			AND (YEAR(MHdate)	= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 				</cfif>
<cfelse>
 AND (MHtype = 'month') 
AND (YEAR(MHdate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">)
</cfif>
</cfquery>

<cfloop query="getindivcomments">
<cfif fusebox.fuseaction eq "enter">
	<cfif not structkeyexists(commstruct,"#commmonth#_#commday#_#mhrow#_#ContractorID#")>
		<cfset commstruct["#commmonth#_#commday#_#mhrow#_#ContractorID#"] = "c">
	</cfif>
<cfelse>
	<cfif not structkeyexists(commstruct,"#commmonth#_#mhrow#_#ContractorID#")>
		<cfset commstruct["#commmonth#_#mhrow#_#ContractorID#"] = "c">
	</cfif>
</cfif>
</cfloop>



</cfif>