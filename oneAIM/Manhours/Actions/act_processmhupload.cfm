<cfparam name="processupload" default="no">
<cfparam name="groupnum" default="0">
<cfset recsinerr = "">
<cfset errfound = "no">
<cfset procerrlist = "">
<cfif processupload>
<cfset monstruct = {}>
	<cfloop from="1" to="12" index="o">
		<cfset monstruct[monthasstring(o)] = o>
	</cfloop>
	
	<cfquery name="geterp" datasource="#request.dsn#">
		select erpsys
		from groups
		where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnum#">
	</cfquery>
	<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "manhours\actions\act_processmhupload.cfm", "uploads\manhours"))>
	<cfif isdefined("mhfile")>
		<cfif trim(mhfile) neq "">
			<cfset uploadok = "yes">
			<!--- Try upload of file, if any error occurs display a message and send an email --->
			<!--- <cftry> --->
				<cffile action="upload" filefield="mhfile" destination="#FileDir#" result="file_result" nameconflict="makeunique" accept="application/vnd.ms-excel,text/plain" >
				
			<cfif uploadok>
				<!--- After upload, read the file and process. First conver csv file to array using regex to allow for commas within fields --->
				<cffile action="read" file="#filedir#\#file_result.serverfile#" variable="uploadedusers">
		 		<cfset uploadedusers = Trim(uploadedusers)>
				<cfset strRegEx = ("(""(?:[^""]|"""")*""|[^"",\r\n]*)" & "(,|\r\n?|\n)?")>
		 		<cfset objPattern = CreateObject("java","java.util.regex.Pattern").Compile(JavaCast( "string", strRegEx ))>
				<cfset objMatcher = objPattern.Matcher(JavaCast( "string", uploadedusers ))>
				<cfset arrData = ArrayNew(1)>
		 		<cfset ArrayAppend( arrData, ArrayNew(1))>
				<cfloop condition="objMatcher.Find()">
		 			<cfset REQUEST.Value = objMatcher.Group(JavaCast( "int", 1 ))>
		 			<cfset REQUEST.Value = REQUEST.Value.ReplaceAll(JavaCast( "string", "^""|""$" ),JavaCast( "string", "" ))>
		 			<cfset REQUEST.Value = REQUEST.Value.ReplaceAll(JavaCast( "string", "(""){2}" ),JavaCast( "string", "$1" ))>
		 			<cfset ArrayAppend(arrData[ ArrayLen( arrData ) ],REQUEST.Value)>
					<cfset REQUEST.Delimiter = objMatcher.Group(JavaCast( "int", 2 ))>
		 			<cfif StructKeyExists( REQUEST, "Delimiter" )>
						<cfif (REQUEST.Delimiter NEQ ",")>
		 					<cfset ArrayAppend(arrData,ArrayNew( 1 ))>
		 				</cfif>
		 			<cfelse>
						<cfbreak>
		 			</cfif>
				</cfloop>
			
				<cfif arraylen(arrData) lte 1>
					<!--- If no data is in the file alert user --->
					<cfset errfound = "yes">
					<cfset recsinerr = "<span style='color=red;'><strong>Please make sure you have uploaded a completed template with the proper column headings.</strong></span><br>">
				<cfelse>
					<cfif arrData[1][1] neq "Month" or arrData[1][2] neq "Year" or arrData[1][3] neq "Overhead" or arrData[1][4] neq "Homeoffice" or arrData[1][5] neq "Field"  or arrData[1][6] neq "Craft"  or arrData[1][7] neq "Subcontractor" or arrData[1][8] neq "NonTabulated">
						<cfset errfound = "yes">
						<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>This does not appear to be the proper template for this upload Expecting (Month) (Year) (Overhead) (Homeoffice) (Field) (Craft) (Subcontractor) (NonTabulated): </strong> ">
						<cfloop from="1" to="8" index="d">
							<cfset recsinerr = recsinerr & " (#arrData[1][d]#)">
						</cfloop>
						<cfset recsinerr = recsinerr & "</span><br>">
					<cfelse>
						<cfloop from="1" to="#arraylen(arrData)#" index="ad">
							<cfif arraylen(arrData[ad])  neq 8>
							<!--- Upload is expecting 13 columns of data, if there is more alert the user --->
								<cfset errfound = "yes">
								<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Unexpected data in row, please make sure values are entered only for the listed columns and that the proper template is being uploaded:</strong> ">
								<cfloop from="1" to="8" index="d">
									<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
								</cfloop>
								<cfset recsinerr = recsinerr & "</span><br>">
								<cfbreak>
							<cfelse>
								<!--- Ignore the first row of column headers and any completely empty rows --->
								<cfif arrData[ad][1] neq "Month" and len(arraytolist(arrData[ad])) gt 0>
									<cfif trim(arrData[ad][1]) eq '' or trim(arrData[ad][2]) eq ''>
									<!--- Check to see if required fields are filled, if not display a message to the user --->
										<cfset errfound = "yes">
										<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Required fields are blank:</strong> ">
										<cfif trim(arrData[ad][1]) eq ''><cfset recsinerr = recsinerr & "(Month) "></cfif>
										<cfif trim(arrData[ad][2]) eq ''><cfset recsinerr = recsinerr & "(Year) "></cfif>
										<cfset recsinerr = recsinerr & "</span><br>">
									<cfelse>
										<cfset acceptmonlist = "1,2,3,4,5,6,7,8,9,10,11,12,January,February,March,April,May,June,July,August,September,October,November,December">
										<cfif trim(arrData[ad][1]) neq ''>
											<cfif listfindnocase(acceptmonlist,trim(arrData[ad][1])) eq 0>
											<!--- Check to see if month is entered correctly --->
												<cfset errfound = "yes">
												<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Month must be a valid month number or month full name:</strong></span> ">
												<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
													<cfif d eq 1>
														<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
													</cfif>
													<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
													<cfif d eq 1>
														<cfset recsinerr = recsinerr & "</strong>">
													</cfif>
												</cfloop>
												<cfset recsinerr = recsinerr & "<br>">
											</cfif>
										</cfif>
										<cfif trim(arrData[ad][2]) neq ''>
											<cfif len(trim(arrData[ad][2])) neq 4 or not isNumeric(trim(arrData[ad][2]))>
											<!--- Check to see if year is entered correctly --->
												<cfset errfound = "yes">
												<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Year must be a 4-digit numeric year:</strong></span> ">
												<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
													<cfif d eq 2>
														<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
													</cfif>
													<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
													<cfif d eq 2>
														<cfset recsinerr = recsinerr & "</strong>">
													</cfif>
												</cfloop>
												<cfset recsinerr = recsinerr & "<br>">
											</cfif>
										</cfif>
										<!--- Check to see if Overhead is entered correctly, isnumeric flags commas in numbers as not numeric so they must be replaced for the validation  --->
										<cfif trim(arrData[ad][3]) neq ''>
											<cfswitch expression="#geterp.erpsys#">
												<cfcase value="JDE">
												
												</cfcase>
												<cfcase value="none">
													<cfif arrData[ad][3] contains "." or not isNumeric(replace(trim(arrData[ad][3]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Overhead hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 3>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 3>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
											</cfswitch>
										</cfif>
										<!--- Check to see if Homeoffice is entered correctly --->
										<cfif trim(arrData[ad][4]) neq ''>
											<cfswitch expression="#geterp.erpsys#">
												<cfcase value="JDE">
												
												</cfcase>
												<cfcase value="none">
													<cfif arrData[ad][4] contains "." or not isNumeric(replace(trim(arrData[ad][4]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Home Office hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 4>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 4>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
											</cfswitch>
										</cfif>
										<!--- Check to see if Field is entered correctly --->
										<cfif trim(arrData[ad][5]) neq ''>
											<cfswitch expression="#geterp.erpsys#">
												<cfcase value="JDE">
												
												</cfcase>
												<cfcase value="none">
													<cfif arrData[ad][5] contains "." or not isNumeric(replace(trim(arrData[ad][5]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Field hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 5>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 5>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
											</cfswitch>
										</cfif>
										<!--- Check to see if Craft is entered correctly --->
										<cfif trim(arrData[ad][6]) neq ''>
											<cfswitch expression="#geterp.erpsys#">
												<cfcase value="JDE">
												
												</cfcase>
												<cfcase value="none">
													<cfif arrData[ad][6] contains "." or not isNumeric(replace(trim(arrData[ad][6]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Craft hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 6>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 6>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
											</cfswitch>
										</cfif>
										<!--- Check to see if Subcontractor is entered correctly --->
										<cfif trim(arrData[ad][7]) neq ''>
											<cfswitch expression="#geterp.erpsys#">
												<cfcase value="JDE">
													<cfif arrData[ad][7] contains "." or not isNumeric(replace(trim(arrData[ad][7]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Subcontractor hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 7>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 7>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
												<cfcase value="none">
													<cfif arrData[ad][7] contains "." or not isNumeric(replace(trim(arrData[ad][7]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Subcontractor hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 7>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 7>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
											</cfswitch>
										</cfif>
										<!--- Check to see if NonTabulated is entered correctly --->
										<cfif trim(arrData[ad][8]) neq ''>
											<cfswitch expression="#geterp.erpsys#">
												<cfcase value="JDE">
													<cfif arrData[ad][8] contains "." or not isNumeric(replace(trim(arrData[ad][8]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Non-Tabulated hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 8>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 8>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
												<cfcase value="none">
													<cfif arrData[ad][8] contains "." or not isNumeric(replace(trim(arrData[ad][8]),",","","all"))>
														<cfset errfound = "yes">
														<cfset recsinerr = recsinerr & "<span style='color=red;'><strong>Row #ad# - Non-Tabulated hours must be numeric whole number with no decimal places:</strong></span> ">
														<cfloop from="1" to="#arraylen(arrData[ad])#" index="d">
															<cfif d eq 8>
																<cfset recsinerr = recsinerr & "<strong style='color=red;'>">
															</cfif>
															<cfset recsinerr = recsinerr & " (#arrData[ad][d]#)">
															<cfif d eq 8>
																<cfset recsinerr = recsinerr & "</strong>">
															</cfif>
														</cfloop>
														<cfset recsinerr = recsinerr & "<br>">
													</cfif>
												</cfcase>
											</cfswitch>
										</cfif>
									</cfif> 
								</cfif>
							</cfif>
						</cfloop>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	
	<cfif errfound eq "no">
	<!--- If no errors in file proceed to processing --->
		<cfset arrctr = 0>
		<cfset issuect = 0>
		<cfset errorcnt = 0>
		<cfset newempctr = 0>
		<cfset currempctr = 0>
		
		
		<cfloop from="1" to="#arraylen(arrData)#" index="empdata">
		<!--- Loop over array of data and try to process --->
			<cfif arrData[empdata][1] neq "Month" and len(arraytolist(arrData[empdata])) gt 0>
				<cfset usemon = ''>
					<cfif listfindnocase("January,February,March,April,May,June,July,August,September,October,November,December",arrData[empdata][1]) gt 0>
						<cfset usemon = monstruct[arrData[empdata][1]]>
					<cfelseif listfind("1,2,3,4,5,6,7,8,9,10,11,12",arrData[empdata][1]) gt 0>
						<cfset usemon = arrData[empdata][1]>
					</cfif>
				<cfset procmh = "yes">
					<cfif arrData[empdata][2] gt year(now())>
						<cfset procmh = "no">
						<cfset procerrlist = listappend(procerrlist,"Could not update data for row #empdata#: #arrData[empdata][2]# is greater than #year(now())#")>
					<cfelseif arrData[empdata][2] eq year(now()) and month(now()) lte usemon>
						<cfset procmh = "no">
						<cfset procerrlist = listappend(procerrlist,"Could not update data for row #empdata#: Month (#arrData[empdata][1]#) was out of range of month's that have passed")>
					</cfif>
				<cfif procmh> 
			
					<cfmodule template="/#getappconfig.oneAIMpath#/shared/weekenddate.cfm" numberweeks="1" startday="Friday" checkyear="#arrData[empdata][2]#">
						<cfset useweeks = "">
							<cfif trim(usemon) neq ''>
								<cfloop list="#weekendingdates#" index="i">
									<cfif listfind(usemon,month(i)) gt 0>
										<cfset useweeks = listappend(useweeks,i)>
									</cfif>
								</cfloop>
							<cfswitch expression="#geterp.erpsys#">
								<cfcase value="JDE">
									<cfset divby = listlen(useweeks)>
									<cfset divsub = 0>
									<cfset divsubt = 0>
									<cfset divnontab = 0>
									<cfset divnontabt = 0>
									<cfset ctr = 0>
									<!--- check to see if there is a value in the upload file, set to 0 if not --->
									<cfloop list="#useweeks#" index="c">
										<cfif trim(arrData[empdata][7]) neq ''>
											<cfset usesub = replace(arrData[empdata][7],",","","all")>
										<cfelse>
											<cfset usesub = 0>
										</cfif>
										<cfif trim(arrData[empdata][8]) neq ''>
											<cfset usent = replace(arrData[empdata][8],",","","all")>
										<cfelse>
											<cfset usent = 0>
										</cfif>
										<cfset ctr = ctr + 1>
										<!--- divide uploaded value by number of weeks in month and carry remainder to last week --->
										<cfif ctr neq listlen(useweeks)>
											<cfset divnontab = int(usent/divby)>
											<cfset divsub = int(usesub/divby)>
											<cfset divnontabt = divnontabt + divnontab>
											<cfset divsubt = divsubt + divsub>					
										<cfelse>
											<cfset divnontab = usent-divnontabt>
											<cfset divsub = usesub-divsubt>
										</cfif> 
										<!--- check to see if there is a record already, if yes update, if not add a new one for the week ending date --->
										<cfquery name="getsubrec" datasource="#request.dsn#">
											select laborhoursid, HomeOfficeHrs, OverheadHrs, FieldHrs, CraftHrs, nontabhrs
											from LaborHoursTotal
											where groupnumber = <cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#groupnum#"> and weekendingdate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#c#">
										</cfquery>	
										<cfif getsubrec.recordcount eq 0>
											<cfquery name="addrec" datasource="#request.dsn#">
												insert into LaborHoursTotal (GroupNumber, <cfif trim(arrData[empdata][7]) neq ''>SubHrs,</cfif> WeekEndingDate, UpdateBy, UpdateDate<cfif trim(arrData[empdata][8]) neq ''>,nontabhrs</cfif>)
												values (<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#groupnum#">,<cfif trim(arrData[empdata][7]) neq ''><cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divsub#">,</cfif><cfqueryparam cfsqltype="CF_SQL_DATE" value="#c#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#"><cfif trim(arrData[empdata][8]) neq ''>,<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divnontab#"></cfif>)
											</cfquery> 
										<cfelse> 
											<cfquery name="update" datasource="#request.dsn#">
												update LaborHoursTotal
												set 
												<cfif trim(arrData[empdata][7]) neq ''>SubHrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divsub#">,</cfif>
												UpdateBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,
												<cfif trim(arrData[empdata][8]) neq ''>nontabhrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divnontab#">,</cfif>
												updatedate = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">
												where laborhoursid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getsubrec.laborhoursid#">
											</cfquery>			
										</cfif>
									</cfloop>
								</cfcase>
								<cfcase value="none">
									<!--- Set base valuse for totals --->
									<cfset divby = listlen(useweeks)>
									<cfset divoh = 0>
									<cfset divho = 0>
									<cfset divfld = 0>
									<cfset divcrft = 0>
									<cfset divsub = 0>
									<cfset divoht = 0>
									<cfset divhot = 0>
									<cfset divfldt = 0>
									<cfset divcrftt = 0>
									<cfset divsubt = 0>
									<cfset divnontab = 0>
									<cfset divnontabt = 0>
									<cfset ctr = 0>
									<!--- check to see if there is a value in the upload file, set to 0 if not --->
									<cfloop list="#useweeks#" index="c">
										<cfif trim(arrData[empdata][7]) neq ''>
											<cfset usesub = replace(arrData[empdata][7],",","","all")>
										<cfelse>
											<cfset usesub = 0>
										</cfif>
										<cfif trim(arrData[empdata][8]) neq ''>
											<cfset usent = replace(arrData[empdata][8],",","","all")>
										<cfelse>
											<cfset usent = 0>
										</cfif>
										<cfif trim(arrData[empdata][4]) neq ''>
											<cfset useho = replace(arrData[empdata][4],",","","all")>
										<cfelse>
											<cfset useho = 0>
										</cfif>
										<cfif trim(arrData[empdata][3]) neq ''>
											<cfset useoh = replace(arrData[empdata][3],",","","all")>
										<cfelse>
											<cfset useoh = 0>
										</cfif>
										<cfif trim(arrData[empdata][5]) neq ''>
											<cfset usefld = replace(arrData[empdata][5],",","","all")>
										<cfelse>
											<cfset usefld = 0>
										</cfif>
										<cfif trim(arrData[empdata][6]) neq ''>
											<cfset usecrft = replace(arrData[empdata][6],",","","all")>
										<cfelse>
											<cfset usecrft = 0>
										</cfif>
										<cfset ctr = ctr + 1>
										<!--- divide uploaded value by number of weeks in month and carry remainder to last week --->
										<cfif ctr neq listlen(useweeks)>
											<cfset divoh = int(useoh/divby)>
											<cfset divnontab = int(usent/divby)>
											<cfset divho = int(useho/divby)>
											<cfset divfld = int(usefld/divby)>
											<cfset divcrft = int(usecrft/divby)>
											<cfset divsub = int(usesub/divby)>
											<cfset divnontabt = divnontabt + divnontab>
											<cfset divoht = divoht + divoh>
											<cfset divhot = divhot + divho>
											<cfset divfldt = divfldt + divfld>
											<cfset divcrftt = divcrftt + divcrft>
											<cfset divsubt = divsubt + divsub>					
										<cfelse>
											<cfset divnontab = usent-divnontabt>
											<cfset divoh = useoh-divoht>
											<cfset divho = useho-divhot>
											<cfset divfld = usefld-divfldt>
											<cfset divcrft = usecrft-divcrftt>
											<cfset divsub = usesub-divsubt>
										</cfif> 
										<!--- check to see if there is a record already, if yes update, if not add a new one for the week ending date --->
										<cfquery name="getsubrec" datasource="#request.dsn#">
											select laborhoursid, HomeOfficeHrs, OverheadHrs, FieldHrs, CraftHrs, nontabhrs
											from LaborHoursTotal
											where groupnumber = <cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#groupnum#"> and weekendingdate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#c#">
										</cfquery>	
										<cfif getsubrec.recordcount eq 0>
											<cfquery name="addrec" datasource="#request.dsn#">
												insert into LaborHoursTotal (GroupNumber, <cfif trim(arrData[empdata][4]) neq ''>HomeOfficeHrs,</cfif> <cfif trim(arrData[empdata][3]) neq ''>OverheadHrs,</cfif> <cfif trim(arrData[empdata][5]) neq ''>FieldHrs, </cfif><cfif trim(arrData[empdata][6]) neq ''>CraftHrs,</cfif> <cfif trim(arrData[empdata][7]) neq ''>SubHrs,</cfif> WeekEndingDate, UpdateBy, UpdateDate<cfif trim(arrData[empdata][8]) neq ''>,nontabhrs</cfif>)
												values (<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#groupnum#">,<cfif trim(arrData[empdata][4]) neq ''><cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divho#">,</cfif><cfif trim(arrData[empdata][3]) neq ''><cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divoh#">,</cfif><cfif trim(arrData[empdata][5]) neq ''><cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divfld#">,</cfif><cfif trim(arrData[empdata][6]) neq ''><cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divcrft#">,</cfif><cfif trim(arrData[empdata][7]) neq ''><cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divsub#">,</cfif><cfqueryparam cfsqltype="CF_SQL_DATE" value="#c#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#"><cfif trim(arrData[empdata][8]) neq ''>,<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divnontab#"></cfif>)
											</cfquery> 
										<cfelse> 
											<cfquery name="update" datasource="#request.dsn#">
												update LaborHoursTotal
												set 
												<cfif trim(arrData[empdata][4]) neq ''>HomeOfficeHrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divho#">, </cfif>
												<cfif trim(arrData[empdata][3]) neq ''>OverheadHrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divoh#">, </cfif>
												<cfif trim(arrData[empdata][5]) neq ''>FieldHrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divfld#">, </cfif>
												<cfif trim(arrData[empdata][6]) neq ''>CraftHrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divcrft#">,</cfif>
												<cfif trim(arrData[empdata][7]) neq ''>SubHrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divsub#">,</cfif>
												UpdateBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,
												<cfif trim(arrData[empdata][8]) neq ''>nontabhrs = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#divnontab#">,</cfif>
												updatedate = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">
												where laborhoursid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getsubrec.laborhoursid#">
											</cfquery>			
										</cfif>
									</cfloop>
								</cfcase>
							</cfswitch>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
		</cfif> 
	</cfif>
