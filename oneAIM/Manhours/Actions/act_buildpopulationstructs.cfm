
	<cfloop query="getgrouppopulation">
		<cfif not structkeyexists(afwpopstruct,month(PopDate))>
			<cfset afwpopstruct[month(popdate)] = NumEmployedAFW>
		</cfif>
		<cfif not structkeyexists(subpopstruct,month(PopDate))>
			<cfset subpopstruct[month(popdate)] = NumEmployedSub>
		</cfif>
		<cfif not structkeyexists(mcpopstruct,month(PopDate))>
			<cfset mcpopstruct[month(popdate)] = NumEmployedMC>
		</cfif>
		<cfif not structkeyexists(jvpopstruct,month(PopDate))>
			<cfset jvpopstruct[month(popdate)] = NumEmployedJV>
		</cfif>
	</cfloop>
	
	
	<cfloop query="getconpopulation">
		<cfif not structkeyexists(subjvstruct,"#month(popdate)#_#ContractorID#")>
			<cfset subjvstruct["#month(popdate)#_#ContractorID#"] = NumEmployedCon>
		</cfif>
	</cfloop>