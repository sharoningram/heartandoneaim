<!--- <cfset end_date = DateFormat(CreateDate(Year(now()),Month(now()), Day(now())), "mm/dd/yy")>
<cfset begin_date = DateFormat(DateAdd('m', -1, end_date), "mm/dd/yy")>

<cfoutput>#begin_date# #end_date#</cfoutput>
<cfquery name="gethrs" datasource="labor_safety">
SELECT    top 50    Project_Num, Project_Desc, Week_Ending_Date, HO, Field, Craft, COMPANY_NUM, DIVISION
FROM            vw_safety_labor_1
WHERE        (Week_Ending_Date BETWEEN '2/17/2015' AND '3/17/2015')
</cfquery>
<cfoutput query="gethrs">
#Project_Num#, #Project_Desc#, #Week_Ending_Date#, #HO#, #Field#, #Craft#, #COMPANY_NUM#, #DIVISION# <br>
</cfoutput> --->
<a href="#bottom">Bottom of page</a>
<a name="top"></a>
<cfquery name="getgroups" datasource="#request.dsn#">
select group_number, group_name
from groups
where business_line_id in (1,2,3,4,5,6)
<!--- where business_line_id = 1 --->
</cfquery>
<cfset thegnums = valuelist(getgroups.group_number)>
<cfset yrstruct = {}>
<cfset hostruct = {}>
<cfset fldstruct = {}>
<cfset crftstruct = {}><!---2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,  --->
<cfparam name="yrlist" default = "2015">

<style>
td {
font-family:verdana;
font-size:10px;
}
strong {
font-family:verdana;
font-size:11px;
}
</style>


<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT    Groups.Group_Number, Groups.Group_Description, YEAR(LaborHours.WEEK_ENDING_DATE) AS hryr, SUM(LaborHours.HO_HRS) AS ho, 
                      SUM(LaborHours.FIELD_HRS) AS fld, SUM(LaborHours.CRAFT_HRS) AS crft
FROM         Groups INNER JOIN
                      ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                      LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER
where year(week_ending_date) in (#yrlist#) and groups.group_number in (#thegnums# ) 
GROUP BY Groups.Group_Number, Groups.Group_Description, YEAR(LaborHours.WEEK_ENDING_DATE)
ORDER BY Groups.Group_Number, hryr
</cfquery>

<cfloop query="getjdehrs">
	
	<cfif not structkeyexists(hostruct,"#Group_Number#_#hryr#")>
		<cfset hostruct["#Group_Number#_#hryr#"] = ho>
	</cfif>
	<cfif not structkeyexists(fldstruct,"#Group_Number#_#hryr#")>
		<cfset fldstruct["#Group_Number#_#hryr#"] = fld>
	</cfif>
	<cfif not structkeyexists(crftstruct,"#Group_Number#_#hryr#")>
		<cfset crftstruct["#Group_Number#_#hryr#"] = crft>
	</cfif>
</cfloop> 
<cfquery name="getOH" datasource="#request.dsn#">
SELECT     officeLabormap.Group_Number, YEAR(laborhoursoffice.WEEK_ENDING_DATE) AS ohyr, SUM(laborhoursoffice.OH_HOURS) AS oh
FROM         officeLabormap INNER JOIN
                      laborhoursoffice ON officeLabormap.Dept_Home = laborhoursoffice.Dept_home
where year(week_ending_date) in (#yrlist#) and officeLabormap.group_number in (#thegnums#) 
GROUP BY officeLabormap.Group_Number, YEAR(laborhoursoffice.WEEK_ENDING_DATE)
ORDER BY officeLabormap.Group_Number, ohyr
</cfquery>
<cfset ohstruct = {}>
<cfloop query="getOH">
	<cfif not structkeyexists(ohstruct,"#Group_Number#_#ohyr#")>
		<cfset ohstruct["#Group_Number#_#ohyr#"] = oh>
	</cfif>
</cfloop>


<cfquery name="getsub1" datasource="#request.dsn#">
SELECT     GroupNumber, YEAR(WeekEndingDate) AS subyr, SUM(SubHrs) AS subhrs1, SUM(HomeOfficeHrs) AS ho,  SUM(FieldHrs) AS fld, SUM(CraftHrs) AS crt, SUM(nontabHrs) AS nont, sum(OverheadHrs) as oh
FROM         LaborHoursTotal
where year(WeekEndingDate) in (#yrlist#) and GroupNumber in (#thegnums#) 
GROUP BY GroupNumber, YEAR(WeekEndingDate)
ORDER BY GroupNumber, subyr
</cfquery>
<cfset substruct1 = {}>
<cfset ntstruct1 = {}>
<cfloop query="getsub1">
	<cfif not structkeyexists(substruct1,"#GroupNumber#_#subyr#")>
		<cfset substruct1["#GroupNumber#_#subyr#"] = subhrs1>
	</cfif>
	<cfif not structkeyexists(ntstruct1,"#GroupNumber#_#subyr#")>
		<cfset ntstruct1["#GroupNumber#_#subyr#"] = nont>
	</cfif>
	<cfif not structkeyexists(hostruct,"#GroupNumber#_#subyr#")>
		<cfset hostruct["#GroupNumber#_#subyr#"] = ho>
	</cfif>
	<cfif not structkeyexists(fldstruct,"#GroupNumber#_#subyr#")>
		<cfset fldstruct["#GroupNumber#_#subyr#"] = fld>
	</cfif>
	<cfif not structkeyexists(crftstruct,"#GroupNumber#_#subyr#")>
		<cfset crftstruct["#GroupNumber#_#subyr#"] = crt>
	</cfif>
	<cfif not structkeyexists(ohstruct,"#GroupNumber#_#subyr#")>
		<cfset ohstruct["#GroupNumber#_#subyr#"] = oh>
	</cfif>
</cfloop>
	
	



<cfquery name="getconjv" datasource="#request.dsn#">
SELECT     Contractors.GroupNumber, Contractors.CoType, YEAR(ContractorHours.WeekEndingDate) AS conyr, SUM(ContractorHours.ContractorHrs) AS conhrs
FROM         Contractors INNER JOIN
                      ContractorHours ON Contractors.ContractorID = ContractorHours.ContractorID
where year(WeekEndingDate) in (#yrlist#) and Contractors.GroupNumber in (#thegnums#) 
GROUP BY Contractors.GroupNumber, Contractors.CoType, YEAR(ContractorHours.WeekEndingDate)
ORDER BY Contractors.GroupNumber, conyr, Contractors.CoType
</cfquery>
<cfset jvstruct1 = {}>
<cfloop query="getconjv">
	<cfif CoType eq "sub">
		<cfif not structkeyexists(substruct1,"#GroupNumber#_#conyr#")>
			<cfset substruct1["#GroupNumber#_#conyr#"] = conhrs>
		<cfelse>
			<cfset substruct1["#GroupNumber#_#conyr#"] = substruct1["#GroupNumber#_#conyr#"]+conhrs>
		</cfif>
	<cfelseif CoType eq "jv">
		<cfif not structkeyexists(jvstruct1,"#GroupNumber#_#conyr#")>
			<cfset jvstruct1["#GroupNumber#_#conyr#"] = conhrs>
		</cfif>
	</cfif>
</cfloop>

<!--- <cfquery name="getincs" datasource="#request.dsn#">
SELECT     COUNT(First_Incident_Notice.Incident_Report_Number) AS incct, First_Incident_Notice.Group_Number, YEAR(First_Incident_Notice.Date_Incident_Occured) AS incyr, 
                      IncidentCategoryJoin.IncidentCatID
FROM         First_Incident_Notice INNER JOIN
                      IncidentCategoryJoin ON First_Incident_Notice.Incident_Report_Number = IncidentCategoryJoin.IRN
WHERE     (First_Incident_Notice.Injury_Illness IN ('C1', 'I2', 'I1', 'M1'))
GROUP BY First_Incident_Notice.Group_Number, YEAR(First_Incident_Notice.Date_Incident_Occured), IncidentCategoryJoin.IncidentCatID
ORDER BY First_Incident_Notice.Group_Number, incyr, IncidentCategoryJoin.IncidentCatID
</cfquery> --->
<cfset recincstruct = {}>
<cfset nontabstruct = {}>
<!--- 
<cfoutput query="getincs">
	<cfif IncidentCatID eq 4>
		<cfif not structkeyexists(nontabstruct,"#Group_Number#_#incyr#")>
			<cfset nontabstruct["#Group_Number#_#incyr#"] = incct>
		</cfif>
	<cfelse>
		<cfif not structkeyexists(recincstruct,"#Group_Number#_#incyr#")>
			<cfset recincstruct["#Group_Number#_#incyr#"] = incct>
		<cfelse>
			<cfset recincstruct["#Group_Number#_#incyr#"] = recincstruct["#Group_Number#_#incyr#"]+incct>
		</cfif>
	</cfif>
</cfoutput> --->

<cfset gtot1 = 0>
<cfset gtot2 = 0>
<cfset gtot3 = 0>
<cfset gtot4 = 0>
<cfset gtot5 = 0>
<cfset gtot6 = 0>
<cfset gtot7 = 0>
<cfset gtot8 = 0>
<cfset gtot9 = 0>
<table cellpadding="4" cellspacing="0" border="0" width="100%">

<cfoutput query="getgroups">
<tr <cfif currentrow mod 2 is 1>bgcolor="efefef"</cfif>>
	<td colspan="12"><br></td>
</tr>
<tr <cfif currentrow mod 2 is 1>bgcolor="efefef"</cfif>>
	<td colspan="12"><strong>#group_number# - #group_name#</strong></td>
</tr>


<tr <cfif currentrow mod 2 is 1>bgcolor="efefef"</cfif>>
	<td align="center"><strong>Year</strong></td>
	<td align="center"><strong>Home Office</strong></td>
	<td align="center"><strong>Field</strong></td>
	<td align="center"><strong>Craft</strong></td>
	<td align="center"><strong>Overhead</strong></td>
	<td align="center"><strong>Subcontractor</strong></td>
	<!--- <td align="center"><strong>Incidents</strong></td> --->
	<td align="center"><strong>JV</strong></td>
	<!--- <td align="center"><strong>TRIR</strong></td> --->
	<td align="center"><strong>Non Tabulated</strong></td>
	<!--- <td align="center"><strong>Non Tab Incidents</strong></td>
	<td align="center"><strong>Non Tab TRIR</strong></td> --->
</tr>
<cfset tot1 = 0>
<cfset tot2 = 0>
<cfset tot3 = 0>
<cfset tot4 = 0>
<cfset tot5 = 0>
<cfset tot6 = 0>
<cfset tot7 = 0>
<cfset tot8 = 0>
<cfset tot9 = 0>
<cfloop list="#yrlist#" index="i">
<cfset rowhrs = 0>
<tr <cfif currentrow mod 2 is 1>bgcolor="efefef"</cfif>>
	<td align="center">#i#</td>
	<td align="center"><cfif structkeyexists(hostruct,"#group_number#_#i#")>#numberformat(hostruct["#group_number#_#i#"])#<cfset tot1 = tot1+hostruct["#group_number#_#i#"]><cfset rowhrs = rowhrs+hostruct["#group_number#_#i#"]><cfelse>0</cfif></td>
	<td align="center"><cfif structkeyexists(fldstruct,"#group_number#_#i#")>#numberformat(fldstruct["#group_number#_#i#"])#<cfset tot2 = tot2+fldstruct["#group_number#_#i#"]><cfset rowhrs = rowhrs+fldstruct["#group_number#_#i#"]><cfelse>0</cfif></td>
	<td align="center"><cfif structkeyexists(crftstruct,"#group_number#_#i#")>#numberformat(crftstruct["#group_number#_#i#"])#<cfset tot3 = tot3+crftstruct["#group_number#_#i#"]><cfset rowhrs = rowhrs+crftstruct["#group_number#_#i#"]><cfelse>0</cfif></td>
	<td align="center"><cfif structkeyexists(ohstruct,"#group_number#_#i#")>#numberformat(ohstruct["#group_number#_#i#"])#<cfset tot4 = tot4+ohstruct["#group_number#_#i#"]><cfset rowhrs = rowhrs+ohstruct["#group_number#_#i#"]><cfelse>0</cfif></td>
	<td align="center"><cfif structkeyexists(substruct1,"#group_number#_#i#")>#numberformat(substruct1["#group_number#_#i#"])#<cfset tot5 = tot5+substruct1["#group_number#_#i#"]><cfset rowhrs = rowhrs+substruct1["#group_number#_#i#"]><cfelse>0</cfif></td>
	<!--- <td align="center"><cfif structkeyexists(recincstruct,"#group_number#_#i#")>#numberformat(recincstruct["#group_number#_#i#"])#<cfset tot8 = tot8+recincstruct["#group_number#_#i#"]><cfelse>0</cfif></td> --->
	<td align="center"><cfif structkeyexists(jvstruct1,"#group_number#_#i#")>#numberformat(jvstruct1["#group_number#_#i#"])#<cfset tot6 = tot6+jvstruct1["#group_number#_#i#"]><cfset rowhrs = rowhrs+jvstruct1["#group_number#_#i#"]><cfelse>0</cfif></td>
	<!--- <td align="center"><cfif structkeyexists(recincstruct,"#group_number#_#i#") and rowhrs gt 0>#numberformat((recincstruct["#group_number#_#i#"]*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td> --->
	<td align="center"><cfif structkeyexists(ntstruct1,"#group_number#_#i#")>#numberformat(ntstruct1["#group_number#_#i#"])#<cfset tot7 = tot7+ntstruct1["#group_number#_#i#"]><cfelse>0</cfif></td>
	<!--- <td align="center"><cfif structkeyexists(nontabstruct,"#group_number#_#i#")>#numberformat(nontabstruct["#group_number#_#i#"])#<cfset tot9 = tot9+nontabstruct["#group_number#_#i#"]><cfelse>0</cfif></td>
	<td align="center"><cfif structkeyexists(ntstruct1,"#group_number#_#i#") and structkeyexists(nontabstruct,"#group_number#_#i#")><cfif nontabstruct["#group_number#_#i#"] gt 0 and ntstruct1["#group_number#_#i#"]> #numberformat((nontabstruct["#group_number#_#i#"]*200000)/ntstruct1["#group_number#_#i#"],"0.00")#<cfelse>0</cfif><cfelse>0</cfif></td> --->
</tr> 
</cfloop>
<cfset subtothrs = 0>
<tr <cfif currentrow mod 2 is 1>bgcolor="efefef"</cfif>>
	<td><strong>Group Total</strong></td>
	<td align="center"><strong>#numberformat(tot1)#<cfset gtot1 = gtot1+tot1><cfset subtothrs = subtothrs+tot1></strong></td>
	<td align="center"><strong>#numberformat(tot2)#<cfset gtot2 = gtot2+tot2><cfset subtothrs = subtothrs+tot2></strong></td>
	<td align="center"><strong>#numberformat(tot3)#<cfset gtot3 = gtot3+tot3><cfset subtothrs = subtothrs+tot3></strong></td>
	<td align="center"><strong>#numberformat(tot4)#<cfset gtot4 = gtot4+tot4><cfset subtothrs = subtothrs+tot4></strong></td>
	<td align="center"><strong>#numberformat(tot5)#<cfset gtot5 = gtot5+tot5><cfset subtothrs = subtothrs+tot5></strong></td>
<!--- 	<td align="center"><strong>#numberformat(tot8)#<cfset gtot8 = gtot8+tot8></strong></td> --->
	<td align="center"><strong>#numberformat(tot6)#<cfset gtot6 = gtot6+tot6><cfset subtothrs = subtothrs+tot6></strong></td>
	<!--- <td align="center"><strong><cfif subtothrs gt 0 and tot8 gt 0>#numberformat((tot8*200000)/subtothrs,"0.00")#<cfelse>0</cfif></strong></td> --->
	<td align="center"><strong>#numberformat(tot7)#<cfset gtot7 = gtot7+tot7></strong></td>
	<!--- <td align="center"><strong>#numberformat(tot9)#<cfset gtot9 = gtot9+tot9></strong></td>
	<td align="center"><strong><cfif tot7 gt 0 and tot9 gt 0>#numberformat((tot9*200000)/tot7,"0.00")#<cfelse>0</cfif></strong></td> --->
</tr>
<tr <cfif currentrow mod 2 is 1>bgcolor="efefef"</cfif>>
	<td colspan="12"><br></td>
</tr>
</cfoutput>
<cfoutput>
<cfset gtothrs = 0>
<tr bgcolor="FAC77A">
	<td><strong>GRANT TOTAL</strong></td>
	<td align="center"><strong>#numberformat(gtot1)#</strong><cfset gtothrs = gtothrs+gtot1></td>
	<td align="center"><strong>#numberformat(gtot2)#</strong><cfset gtothrs = gtothrs+gtot2></td>
	<td align="center"><strong>#numberformat(gtot3)#</strong><cfset gtothrs = gtothrs+gtot3></td>
	<td align="center"><strong>#numberformat(gtot4)#</strong><cfset gtothrs = gtothrs+gtot4></td>
	<td align="center"><strong>#numberformat(gtot5)#</strong><cfset gtothrs = gtothrs+gtot5></td>
	<!--- <td align="center"><strong>#numberformat(gtot8)#</strong></td>
	 ---><td align="center"><strong>#numberformat(gtot6)#</strong><cfset gtothrs = gtothrs+gtot6></td>
	<!--- <td align="center"><strong><cfif gtothrs gt 0 and gtot8 gt 0>#numberformat((gtot8*200000)/gtothrs,"0.00")#<cfelse>0</cfif></strong></td>
	 ---><td align="center"><strong>#numberformat(gtot7)#</strong></td>
	<!--- <td align="center"><strong>#numberformat(gtot9)#</strong></td>
	<td align="center"><strong><cfif gtot7 gt 0 and gtot9 gt 0>#numberformat((gtot9*200000)/gtot7,"0.00")#<cfelse>0</cfif></strong></td> --->
</tr>
</cfoutput>
</table>
<a href="#top">Top of page</a>
<a name="bottom"></a>