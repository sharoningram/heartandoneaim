<div class="content1of1" align="center">

<cfform action="#self#?fuseaction=#attributes.xfa.upload#" method="post" name="uploadmhfrm" enctype="multipart/form-data">
<input type="hidden" name="processupload" value="yes">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="toptablefrm">
<tr><td colspan="2"> <span class="bodyTextWhite"><strong>Select Project Group</strong></span></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">
			<cfif processupload>
					<cfif errfound eq "no">
					<tr>
						<td class="bodytext">
							<strong style="color:green;">Upload complete, Man Hours have been updated</strong>
							<cfif trim(procerrlist) neq ''>
							<br><strong style="color:red;">The following issues arose with some records:</strong><br>
							<cfoutput><cfloop list="#procerrlist#" index="c">
								#c#<br>
							</cfloop></cfoutput>
							</cfif>
						</td>
					</tr>
					<cfelseif errfound eq "yes">
					<tr>
						<td class="bodytext">
						<strong style="color:red;">Issues have been identified in this upload file. Please correct these issues and upload the file again:</strong><br><br>
						<cfoutput>#recsinerr#</cfoutput>
						</td>
					</tr>
					</cfif>
				</cfif>	
				<tr>
					<td align="center"><strong class="bodytext">Select project group:</strong></td>
				</tr>
				<tr>
					<td align="center"><cfselect class="selectgen" size="1" name="groupnum" required="Yes" message="Please select a project group" id="lrgselect">
						<option value="">-- Select One --</option>
						<cfoutput query="getgrouplist" group="name">
						<option value="" disabled>#name#</option>
						<cfoutput>
							<option value="#group_number#" <cfif groupnum eq group_number>selected</cfif>>&nbsp;&nbsp;#group_name#</option>
						</cfoutput>
						</cfoutput>
						</cfselect></td>
				</tr>
				<tr>
					<td align="center"><strong class="bodytext">Select a file to upload:</strong></td>
				</tr>
				<tr>
					<td align="center"><cfinput type="file" class="selectgen" size="20" name="mhfile" required="Yes" message="Please select a file"></td>
				</tr>
				<tr>
					<td align="center"><input type="submit" value="Submit" class="selectGenBTN"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
</cfform>