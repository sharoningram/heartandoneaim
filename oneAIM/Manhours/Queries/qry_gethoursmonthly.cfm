<cfparam name="lookupyear" default="#year(now())#">
<cfparam name="lookupmonth" default="1,2,3,4,5,6,7,8,9,10,11,12">
<cfif isdefined("getgroupinfo.recordcount")>
	<cfswitch expression="#getgroupinfo.erpsys#">
		<cfcase value="JDE">	
			<cfset wedlist = "">
			<cfquery name="getjde" datasource="#request.dsn#">
				SELECT        month(LaborHours.WEEK_ENDING_DATE) as jdemon, SUM(LaborHours.HO_HRS) AS ho, SUM(LaborHours.FIELD_HRS) AS fld, SUM(LaborHours.CRAFT_HRS) AS crft
				FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER
				WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) and ProjectsByGroup.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">
				AND (YEAR(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) <!--- and month(LaborHours.WEEK_ENDING_DATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupmonth#" list="Yes">) --->
				GROUP BY month(LaborHours.WEEK_ENDING_DATE)
				ORDER BY jdemon		
			</cfquery>
			<cfset hostruct = {}>
			<cfset fldstruct = {}>
			<cfset crftstruct = {}>
			<cfset hoytd = 0>
			<cfset fldytd = 0>
			<cfset crftytd = 0>
			<cfset amecfwytd = 0>
			<cfloop query="getjde">
				<cfset hoytd = hoytd+ho>
				<cfset fldytd = fldytd+fld>
				<cfset crftytd = crftytd+crft>
				<cfif not structkeyexists(hostruct,jdemon)>
					<cfset hostruct[jdemon] = ho>
				</cfif>
				<cfif not structkeyexists(fldstruct,jdemon)>
					<cfset fldstruct[jdemon] = fld>
				</cfif>
				<cfif not structkeyexists(crftstruct,jdemon)>
					<cfset crftstruct[jdemon] = crft>
				</cfif>
			</cfloop>
			<cfquery name="getrelatedhrs" datasource="#request.dsn#">
				SELECT     sum(SubHrs) as subhrs, month(WeekEndingDate) as hrsmon, sum(nontabHrs) as nontabhrs, sum(JVhours) as jvhrs,sum(ManagedContractorHours) as mgdhrs
				FROM         LaborHoursTotal
				WHERE     (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#"> AND (YEAR(WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) <!--- and month(WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupmonth#" list="Yes">) --->
				group by month(weekendingdate)
				ORDER BY hrsmon DESC
			</cfquery>
			
			<cfset subytd = 0>
			<cfset mgdytd = 0>
			<cfset jvytd = 0>
			
			<cfset gsubstruct = {}>
			<cfset gnontstruct = {}>
			
			<cfset gjvstruct = {}>
			<cfset gmanagedstruct = {}>
			
			
			<cfloop query="getrelatedhrs">
				<cfif not structkeyexists(gjvstruct,hrsmon)>
					<cfset gjvstruct[hrsmon] = jvhrs>
				</cfif>
				<cfif not structkeyexists(gmanagedstruct,hrsmon)>
					<cfset gmanagedstruct[hrsmon] = mgdhrs>
				</cfif>
			
				<cfif not structkeyexists(gsubstruct,hrsmon)>
					<cfset gsubstruct[hrsmon] = SubHrs>
				</cfif>
				<cfif not structkeyexists(gnontstruct,hrsmon)>
					<cfset gnontstruct[hrsmon] = nontabHrs>
				</cfif>
				<cfset subytd =subytd+SubHrs>
			<cfset mgdytd = mgdytd+mgdhrs>
			<cfset jvytd = jvytd+jvhrs>
			</cfloop>
			<cfquery name="getoh" datasource="#request.dsn#">
				SELECT        month(LaborHoursOffice.WEEK_ENDING_DATE) as ohmonth, SUM(LaborHoursOffice.OH_HOURS) AS oh
				FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home
				WHERE    (OfficeLaborMap.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) and OfficeLaborMap.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">  AND (YEAR(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) <!--- and month(LaborHoursOffice.WEEK_ENDING_DATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupmonth#" list="Yes">) --->
				GROUP BY  month(LaborHoursOffice.WEEK_ENDING_DATE)
				ORDER BY ohmonth
			</cfquery>
			<Cfset ohytd = 0>
			<cfset ohstruct = {}>
			<cfloop query="getoh">
				<cfif not structkeyexists(ohstruct,ohmonth)>
					<cfset ohstruct[ohmonth] = oh>
				</cfif>
				<Cfset ohytd = ohytd+oh>
			</cfloop>
			<cfset subjvytdstruct = {}>
			<cfset subjvstruct = {}>
			<cfquery name="getsubandjvhrs" datasource="#request.dsn#">
				SELECT     CASE WHEN SUM(ContractorHrs) IS NULL THEN 0 ELSE SUM(ContractorHrs) END AS contractorhrs, ContractorID, month(WeekEndingDate) as conmon
				FROM         ContractorHours
				WHERE     (ContractorID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#conidlist#" list="Yes">)) and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#"> AND (YEAR(WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) <!--- and month(WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupmonth#" list="Yes">) --->
				GROUP BY ContractorID, month(WeekEndingDate)
				ORDER BY ContractorID, conmon
			</cfquery>
			<cfloop query="getsubandjvhrs">
				<cfif not structkeyexists(subjvstruct,"#conmon#_#ContractorID#")>
					<cfset subjvstruct["#conmon#_#ContractorID#"] = contractorhrs>
				</cfif>
				<cfif not structkeyexists(subjvytdstruct,"#ContractorID#")>
					<cfset subjvytdstruct["#ContractorID#"] = contractorhrs>
				<cfelse>
					<cfset subjvytdstruct["#ContractorID#"] = subjvytdstruct["#ContractorID#"]+contractorhrs>
				</cfif>
			</cfloop>
			<cfset amecfwytd = ohytd+hoytd+fldytd+crftytd>
		</cfcase>
		<cfcase value="none,mdax">
			<cfset amecfwytd = 0>
			<cfset subytd = 0>
			<cfset mgdytd = 0>
			<cfset jvytd = 0>
			
			<cfset hostruct = {}>
			<cfset fldstruct = {}>
			<cfset crftstruct = {}>
			<cfset gsubstruct = {}>
			<cfset gnontstruct = {}>
			<cfset ohstruct = {}>
			<cfset gjvstruct = {}>
			<cfset gmanagedstruct = {}>
			<cfquery name="gethours" datasource="#request.dsn#">
				SELECT    sum(HomeOfficeHrs) as ho, sum(OverheadHrs) as oh, sum(FieldHrs) as fld, sum(CraftHrs) as crft, sum(SubHrs) as subtot, month(WeekEndingDate) as fullmon,sum(nontabHrs) as nttot, sum(JVhours) as jvhrs,sum(ManagedContractorHours) as mgdhrs
				FROM         LaborHoursTotal
				WHERE     (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) and LaborHoursTotal.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#"> AND (YEAR(WeekEndingDate)= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) <!--- and month(WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupmonth#" list="Yes">) --->
				GROUP BY month(WeekEndingDate)
				ORDER BY fullmon DESC
			</cfquery>
			<cfloop query="gethours">
				<cfset amecfwytd = amecfwytd+ho+fld+crft+oh>
				<cfif not structkeyexists(hostruct,fullmon)>
					<cfset hostruct[fullmon] = ho>
				</cfif>
				<cfif not structkeyexists(fldstruct,fullmon)>
					<cfset fldstruct[fullmon] = fld>
				</cfif>
				<cfif not structkeyexists(crftstruct,fullmon)>
					<cfset crftstruct[fullmon] = crft>
				</cfif>
				<cfif not structkeyexists(ohstruct,fullmon)>
					<cfset ohstruct[fullmon] = oh>
				</cfif>
				<cfif not structkeyexists(gsubstruct,fullmon)>
					<cfset gsubstruct[fullmon] = subtot>
				</cfif>
				<cfif not structkeyexists(gnontstruct,fullmon)>
					<cfset gnontstruct[fullmon] = nttot>
				</cfif>
				<cfif not structkeyexists(gjvstruct,fullmon)>
					<cfset gjvstruct[fullmon] = jvhrs>
				</cfif>
				<cfif not structkeyexists(gmanagedstruct,fullmon)>
					<cfset gmanagedstruct[fullmon] = mgdhrs>
				</cfif>
				<cfset subytd = subytd+subtot>
				<cfset mgdytd = mgdytd+mgdhrs>
				<cfset jvytd = jvytd+jvhrs>
			</cfloop>
			<cfset subjvstruct = {}>
			<cfset subjvytdstruct = {}>
			<cfquery name="getsubandjvhrs" datasource="#request.dsn#">
				SELECT     CASE WHEN SUM(ContractorHrs) IS NULL THEN 0 ELSE SUM(ContractorHrs) END AS contractorhrs, ContractorID, month(WeekEndingDate) as conmon
				FROM         ContractorHours
				WHERE     (ContractorID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#conidlist#" list="Yes">))  and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">AND (YEAR(WeekEndingDate)= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) <!--- and month(WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupmonth#" list="Yes">) --->
				GROUP BY ContractorID, month(WeekEndingDate)
				ORDER BY ContractorID, conmon
			</cfquery>
			<cfloop query="getsubandjvhrs">
				<cfif not structkeyexists(subjvstruct,"#conmon#_#ContractorID#")>
					<cfset subjvstruct["#conmon#_#ContractorID#"] = contractorhrs>
				</cfif>
				<cfif not structkeyexists(subjvytdstruct,"#ContractorID#")>
					<cfset subjvytdstruct["#ContractorID#"] = contractorhrs>
				<cfelse>
					<cfset subjvytdstruct["#ContractorID#"] = subjvytdstruct["#ContractorID#"]+contractorhrs>
				</cfif>
			</cfloop>
		</cfcase>
	</cfswitch>
</cfif>