<cfif isdefined("getgroupinfo.recordcount")>

<cfquery name="gethrcomments" datasource="#request.dsn#">
SELECT        CommentID, Year, GroupNumber, LocationID, EnteredBy, HrComment, DateEntered
FROM            ManHourComments

WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnum#">) AND (LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">) AND (Year = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">)
ORDER BY DateEntered DESC
</cfquery>
</cfif>