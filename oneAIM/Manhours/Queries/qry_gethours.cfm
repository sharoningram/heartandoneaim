<cfif isdefined("getgroupinfo.recordcount")>
	<cfswitch expression="#getgroupinfo.erpsys#">
		<cfcase value="JDE">	
			<cfset wedlist = "">
			<cfquery name="getjde" datasource="#request.dsn#">
				SELECT        LaborHours.WEEK_ENDING_DATE, SUM(LaborHours.HO_HRS) AS ho, SUM(LaborHours.FIELD_HRS) AS fld, SUM(LaborHours.CRAFT_HRS) AS crft
				FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER
				WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">)
				and ProjectsByGroup.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">
				AND (YEAR(LaborHours.WEEK_ENDING_DATE) 
				<cfif needprevyear eq "yes">
 				IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 				<cfelse>
  				= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 				</cfif>
				GROUP BY LaborHours.WEEK_ENDING_DATE
				ORDER BY LaborHours.WEEK_ENDING_DATE		
			</cfquery>
			<cfset hostruct = {}>
			<cfset fldstruct = {}>
			<cfset crftstruct = {}>
			
			<cfloop query="getjde">
				<cfif listfindnocase(wedlist,dateformat(WEEK_ENDING_DATE,"mm/dd/yy")) eq 0>
					<cfset wedlist = listappend(wedlist,dateformat(WEEK_ENDING_DATE,"mm/dd/yy"))>
				</cfif>
				<cfif not structkeyexists(hostruct,dateformat(WEEK_ENDING_DATE,"mm/dd/yy"))>
					<cfset hostruct[dateformat(WEEK_ENDING_DATE,"mm/dd/yy")] = ho>
				</cfif>
				<cfif not structkeyexists(fldstruct,dateformat(WEEK_ENDING_DATE,"mm/dd/yy"))>
					<cfset fldstruct[dateformat(WEEK_ENDING_DATE,"mm/dd/yy")] = fld>
				</cfif>
				<cfif not structkeyexists(crftstruct,dateformat(WEEK_ENDING_DATE,"mm/dd/yy"))>
					<cfset crftstruct[dateformat(WEEK_ENDING_DATE,"mm/dd/yy")] = crft>
				</cfif>
			</cfloop>
			<cfquery name="getrelatedhrs" datasource="#request.dsn#">
				SELECT     LaborHoursID, GroupNumber, SubHrs, WeekEndingDate, UpdateBy, UpdateDate,nontabHrs,JVhours,ManagedContractorHours
				FROM         LaborHoursTotal
				WHERE     (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) 
				and LaborHoursTotal.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#"> AND (YEAR(WeekEndingDate)
			<cfif needprevyear eq "yes">
 				IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 			<cfelse>
  				= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 			</cfif>
				ORDER BY WeekEndingDate DESC
			</cfquery>
			
			<cfset gsubstruct = {}>
			<cfset gnontstruct = {}>
			<cfset gjvstruct = {}>
			<cfset gmanagedstruct = {}>
			
			<cfloop query="getrelatedhrs">
				<cfif listfindnocase(wedlist,dateformat(WeekEndingDate,"mm/dd/yy")) eq 0>
					<cfset wedlist = listappend(wedlist,dateformat(WeekEndingDate,"mm/dd/yy"))>
				</cfif>
				<cfif not structkeyexists(gsubstruct,dateformat(WEEKENDINGDATE,"mm/dd/yy"))>
					<cfset gsubstruct[dateformat(WEEKENDINGDATE,"mm/dd/yy")] = SubHrs>
				</cfif>
				<cfif not structkeyexists(gnontstruct,dateformat(WEEKENDINGDATE,"mm/dd/yy"))>
					<cfset gnontstruct[dateformat(WEEKENDINGDATE,"mm/dd/yy")] = nontabHrs>
				</cfif>
				<cfif not structkeyexists(gjvstruct,dateformat(WEEKENDINGDATE,"mm/dd/yy"))>
					<cfset gjvstruct[dateformat(WEEKENDINGDATE,"mm/dd/yy")] = JVhours>
				</cfif>
				<cfif not structkeyexists(gmanagedstruct,dateformat(WEEKENDINGDATE,"mm/dd/yy"))>
					<cfset gmanagedstruct[dateformat(WEEKENDINGDATE,"mm/dd/yy")] = ManagedContractorHours>
				</cfif>
			</cfloop>
			<cfquery name="getoh" datasource="#request.dsn#">
				SELECT        LaborHoursOffice.WEEK_ENDING_DATE, SUM(LaborHoursOffice.OH_HOURS) AS oh
				FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home
				WHERE    (OfficeLaborMap.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) 
				and OfficeLaborMap.LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">  AND (YEAR(LaborHoursOffice.WEEK_ENDING_DATE)
				<cfif needprevyear eq "yes">
 				IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 				<cfelse>
  				= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 				</cfif>
				GROUP BY LaborHoursOffice.WEEK_ENDING_DATE
				ORDER BY LaborHoursOffice.WEEK_ENDING_DATE
			</cfquery>
			<cfset ohstruct = {}>
			<cfloop query="getoh">
				<cfif listfindnocase(wedlist,dateformat(WEEK_ENDING_DATE,"mm/dd/yy")) eq 0>
					<cfset wedlist = listappend(wedlist,dateformat(WEEK_ENDING_DATE,"mm/dd/yy"))>
				</cfif>
				<cfif not structkeyexists(ohstruct,dateformat(WEEK_ENDING_DATE,"mm/dd/yy"))>
					<cfset ohstruct[dateformat(WEEK_ENDING_DATE,"mm/dd/yy")] = oh>
				</cfif>
			</cfloop>
			
			<cfset gethours = QueryNew("GroupNumber, HomeOfficeHrs, OverheadHrs, FieldHrs, CraftHrs, SubHrs, nontabHrs, WeekEndingDate,JVhours,ManagedContractorHours")>
			<cfset loopctr = 0>
			<cfloop list="#wedlist#" index="i">
			<cfset loopctr = loopctr+1>
				<cfset QueryAddRow( gethours ) />
				<cfset gethours["GroupNumber"][loopctr] = getgroupinfo.group_number />
				<cfset gethours["WeekEndingDate"][loopctr] = i />
				<cfif structkeyexists(hostruct,i)>
					<cfset gethours["HomeOfficeHrs"][loopctr] = hostruct[i] />
				<cfelse>
					<cfset gethours["HomeOfficeHrs"][loopctr] = 0 />
				</cfif>
				<cfif structkeyexists(fldstruct,i)>
					<cfset gethours["FieldHrs"][loopctr] = fldstruct[i] />
				<cfelse>
					<cfset gethours["FieldHrs"][loopctr] = 0 />
				</cfif>
				<cfif structkeyexists(crftstruct,i)>
					<cfset gethours["CraftHrs"][loopctr] = crftstruct[i] />
				<cfelse>
					<cfset gethours["CraftHrs"][loopctr] = 0 />
				</cfif>
				<cfif structkeyexists(gsubstruct,i)>
					<cfset gethours["SubHrs"][loopctr] = gsubstruct[i] />
				<cfelse>
					<cfset gethours["SubHrs"][loopctr] = 0 />
				</cfif>
				<cfif structkeyexists(gnontstruct,i)>
					<cfset gethours["nontabHrs"][loopctr] = gnontstruct[i] />
				<cfelse>
					<cfset gethours["nontabHrs"][loopctr] = 0 />
				</cfif>
				<cfif structkeyexists(ohstruct,i)>
					<cfset gethours["OverheadHrs"][loopctr] = ohstruct[i] />
				<cfelse>
					<cfset gethours["OverheadHrs"][loopctr] = 0 />
				</cfif> 
				<cfif structkeyexists(gjvstruct,i)>
					<cfset gethours["JVhours"][loopctr] = gjvstruct[i] />
				<cfelse>
					<cfset gethours["JVhours"][loopctr] = 0 />
				</cfif>
					<cfif structkeyexists(gmanagedstruct,i)>
					<cfset gethours["ManagedContractorHours"][loopctr] = gmanagedstruct[i] />
				<cfelse>
					<cfset gethours["ManagedContractorHours"][loopctr] = 0 />
				</cfif>
			</cfloop>
			<cfquery name="getsubandjvhrs" datasource="#request.dsn#">
				SELECT     CASE WHEN SUM(ContractorHrs) IS NULL THEN 0 ELSE SUM(ContractorHrs) END AS contractorhrs, ContractorID, WeekEndingDate
				FROM         ContractorHours
				WHERE     (ContractorID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#conidlist#" list="Yes">)) 
				and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#"> AND (YEAR(WeekEndingDate)
				<cfif needprevyear eq "yes">
 					IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 				<cfelse>
  				= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 				</cfif>
				GROUP BY ContractorID, WeekEndingDate
				ORDER BY ContractorID, WeekEndingDate
			</cfquery>
		</cfcase>
		<cfcase value="none">
			<cfquery name="gethours" datasource="#request.dsn#">
				SELECT     LaborHoursID, GroupNumber, HomeOfficeHrs, OverheadHrs, FieldHrs, CraftHrs, SubHrs, WeekEndingDate, UpdateBy, UpdateDate,nontabHrs,JVhours,ManagedContractorHours
				FROM         LaborHoursTotal
				WHERE     (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">
				 AND (YEAR(WeekEndingDate)
			<cfif needprevyear eq "yes">
 				IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 			<cfelse>
  				= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 			</cfif>
				ORDER BY WeekEndingDate DESC
			</cfquery>
			
			<cfquery name="getsubandjvhrs" datasource="#request.dsn#">
				SELECT     CASE WHEN SUM(ContractorHrs) IS NULL THEN 0 ELSE SUM(ContractorHrs) END AS contractorhrs, ContractorID, WeekEndingDate
				FROM         ContractorHours
				WHERE     (ContractorID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#conidlist#" list="Yes">)) 
				and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">
				AND (YEAR(WeekEndingDate)
				<cfif needprevyear eq "yes">
 					IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())-1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">))
 				<cfelse>
  				= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(Now())#">)
 				</cfif>
				GROUP BY ContractorID, WeekEndingDate
				ORDER BY ContractorID, WeekEndingDate
			</cfquery>
		</cfcase>
	</cfswitch>
</cfif>