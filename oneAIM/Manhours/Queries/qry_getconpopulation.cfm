<cfquery name="getconpopulation" datasource="#request.dsn#">
SELECT         ContractorPopID, ContractorID, NumEmployedCon, DateUpdated, UpdateBy, GroupNumber, PopDate, LocationID, isActive
FROM            ContractorPopulation
WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.group_number#">) AND (LocationID =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#">) AND (YEAR(PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">)
</cfquery>