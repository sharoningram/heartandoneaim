<cfparam name="groupnum" default="">
<cfparam name="site" default="0">
<cfif isnumeric(trim(groupnum))>
<!--- <cfquery name="getgroupinfo" datasource="#request.dsn#">
SELECT        Group_Number, Group_Name, ERPsys, Business_Line_ID
FROM            Groups
WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnum#">)
ORDER BY Group_Name
</cfquery> --->
<cfquery name="getgroupinfo" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, Groups.ERPsys, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, Countries.CountryName, 
                         NewDials_1.Name AS BU, NewDials.Name AS OU, NewDials_2.Name AS BS, GroupLocations.GroupLocID, GroupLocations.SiteName
FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         Groups INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON GroupLocations.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnum#">)
</cfquery>

</cfif>