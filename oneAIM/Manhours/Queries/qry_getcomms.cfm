

<cfif trim(gnum) gt 0 and trim(loc) gt 0 and trim(mhd) neq '' and trim(mrow) neq '' and trim(mtype) neq ''>
	<cfquery name="getrowcomms" datasource="#request.dsn#">
		SELECT        commID, MHtype, MHdate, EnteredBy, dateEntered, GroupNumber, MHrow, ContractorID, MHcomment, LocationID,EnteredbyName
		FROM            MHcomments
		where MHtype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mtype#">
		and MHdate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#mhd#">
		and GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnum#">
		and MHrow = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mrow#">
		and LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#loc#">
		<cfif trim(contractor) gt 0>
			and ContractorID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractor#">
		</cfif>
		ORDER BY dateEntered DESC
	</cfquery>
</cfif>


