


<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="main">
	<cfinclude template="actions/act_manhrtest.cfm">
</cfcase>
<cfcase value="enter">
	<cfset attributes.xfa.enter = "manhours.enter">
	<cfset attributes.xfa.popgroupinfo = "manhours.popgroupinfo">
	<cfset attributes.xfa.managemanhours = "manhours.managemanhours">
	<cfset attributes.xfa.addmhcomments = "manhours.addmhcomments">
	<cfinclude template="queries/qry_getgroupinfo.cfm">
	<cfinclude template="actions/act_sethrparams.cfm">
	<cfinclude template="queries/qry_gethours.cfm">
	<cfinclude template="actions/act_buildstructs.cfm">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfinclude template="actions/act_buildcommstructs.cfm">
	<cfinclude template="forms/frm_addmanhours.cfm">
</cfcase>
<cfcase value="managemanhours">
	<cfset attributes.xfa.enter = "manhours.enter">
	<cfset attributes.xfa.entermonthly = "manhours.entermonthly">
	<cfinclude template="actions/act_managemanhours.cfm">
</cfcase>
<cfcase value="addmhcomments">
	<cfset attributes.xfa.addmhcomments = "manhours.addmhcomments">
	<cfinclude template="queries/qry_commdetail.cfm">
	<cfinclude template="actions/act_setcommsparams.cfm">
	<cfinclude template="queries/qry_getcomms.cfm">
	<cfinclude template="actions/act_managecomms.cfm">
	<cfinclude template="forms/frm_managecomms.cfm">
</cfcase>
<cfcase value="entermonthly">
	<cfset attributes.xfa.managemanhours = "manhours.managemanhours">
	<cfset attributes.xfa.entermonthly = "manhours.entermonthly">
	<cfset attributes.xfa.popgroupinfo = "manhours.popgroupinfo">
	<cfset attributes.xfa.addmhcomments = "manhours.addmhcomments">
	<cfinclude template="queries/qry_getgroupinfo.cfm">
	<cfinclude template="actions/act_sethrparamsmonthly.cfm">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfinclude template="queries/qry_gethoursmonthly.cfm">
	<cfmodule template="/#getappconfig.oneAIMpath#/shared/weekenddate.cfm" numberweeks="1" startday="Friday" checkyear="#lookupyear#">
	<cfinclude template="queries/qry_gethrcomments.cfm">
	<cfinclude template="actions/act_buildcommstructs.cfm">
	<cfinclude template="forms/frm_addmanhoursmonthly.cfm">
</cfcase>

<cfcase value="population">
	<cfset attributes.xfa.managepopulation = "manhours.managepopulation">
	<cfset attributes.xfa.population = "manhours.population">
	<cfset attributes.xfa.popgroupinfo = "manhours.popgroupinfo">
	<cfset attributes.xfa.addpopcomments = "manhours.addpopcomments">
	<cfinclude template="queries/qry_getgroupinfo.cfm">
	<cfinclude template="actions/act_setpopparams.cfm">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfparam name="lookupyear" default="#year(now())#">
	<cfparam name="lookupmonth" default="1,2,3,4,5,6,7,8,9,10,11,12">
	<cfmodule template="/#getappconfig.oneAIMpath#/shared/weekenddate.cfm" numberweeks="1" startday="Friday" checkyear="#lookupyear#">
	<cfif isdefined("getgroupinfo.recordcount")>
		<cfinclude template="queries/qry_getgrouppopulation.cfm">
		<cfinclude template="queries/qry_getconpopulation.cfm">
		<cfinclude template="actions/act_buildpopulationstructs.cfm">
		<cfinclude template="actions/act_buildpopcommstructs.cfm">
	</cfif>
	<cfinclude template="forms/frm_addpopulation.cfm">
</cfcase>
<cfcase value="managepopulation">
	<cfset attributes.xfa.population = "manhours.population">
	<cfinclude template="actions/act_managepopulation.cfm">
</cfcase>
<cfcase value="addpopcomments">
	<cfset attributes.xfa.addpopcomments = "manhours.addpopcomments">
	<cfinclude template="queries/qry_commdetailpop.cfm">
	<cfinclude template="actions/act_setcommsparamspop.cfm">
	<cfinclude template="queries/qry_getcommspop.cfm">
	<cfinclude template="actions/act_managecommspop.cfm">
	<cfinclude template="forms/frm_managecommspop.cfm">
</cfcase>
<cfcase value="popgroupinfo">
	<cfinclude template="actions/act_popgroupinfo.cfm">
</cfcase>
<cfcase value="upload">
	<cfset attributes.xfa.upload = "manhours.upload">
	<cfinclude template="actions/act_processmhupload.cfm">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfinclude template="displays/dsp_uploadtemplate.cfm">
	<cfinclude template="forms/frm_uploadmanhours.cfm">
</cfcase>
<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
