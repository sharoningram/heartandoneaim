<body leftmargin="0" topmargin="0" rightmargin="0">
<cfoutput>
<link href="/#getappconfig.oneAIMpath#/shared/css/ers_local.css" rel="stylesheet" type="text/css"> 
</cfoutput>
<cfinclude template="appconfig.cfm">
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr bgcolor="ffffff">
	<td align="right" colspan="2"><img src="images/logoR.jpg" border="0" class="img"></td>
</tr>
<tr class="purplebg"><td class="bodytextwhitelg">&nbsp;oneAIM</td><td align="right" ></td></tr>

<tr>
	<td colspan="2" align="center"  class="bodyTextPurple"><br><br>
We're sorry, there seems to an error accessing the page you have requested. <br>
A site administrator has been notified and will be working to correct the problem.<br><br>
<cfoutput><a href="/#getappconfig.oneAIMpath#" class="bodyTextPurple">Return to oneAIM</a></cfoutput>
	</td>
</tr>
</table>


<cfset newdiag = #replace(error.diagnostics, "#chr(34)#", "'", "all")#>
<cfmail to="#error.mailto#" Subject="oneAIM error" From="#fromemail#" type="html">
Attention an Error has Occurred:<br><br>
		
Error created by: #cookie.useremail#<br>
Client Remote Address: #error.remoteaddress#<br>	
Client Browser: #error.browser#	<br>
Date/Time: #dateformat(error.datetime, "MM/DD/YYYY")# at #timeformat(error.datetime, "h:mm tt")#<br>
HTTP Referer: #error.HTTPReferer#<br>	
Query String: #error.querystring#<br>
Template: #error.template#<br>
Diagnostics: #newdiag#<br><br>
	
 <cfdump var="#error#">	

<cfif isdefined("form.fieldnames")>	
<cfloop list="#form.fieldnames#" index="i">
#i#: #form[i]#<br>

</cfloop><br>
</cfif>
</cfmail>
<br><br>
<div  class="stripeb">Amec Foster Wheeler oneAIM&nbsp;&nbsp;&nbsp;</div>
</body>