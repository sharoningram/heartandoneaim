<div align="center" class="midbox">
	<div align="center" class="midboxtitle">Which search would you like to run?</div>
	<table cellpadding="8" cellspacing="0" border="0" align="center" width="100%">
		<cfif getuserprjsearches.recordcount gt 0>
		<tr>
			<td class="formlable" colspan="2">Project Group Searches</td>
		</tr>
		<cfoutput query="getuserprjsearches">
		<tr>
																
			<td class="bodyTextGrey" width="95%"><a href="#self#?fuseaction=#attributes.xfa.prjsearch#&uprjsearch=#PrjSearchID#">#PrjSearchName#<cfif trim(SaveCtr) gt 1> (#SaveCtr#)</cfif></a></td>
			<td class="bodyTextGrey"><a href="#self#?fuseaction=#attributes.xfa.deleteprjsearch#&prjsearch=#PrjSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');" title="Delete"><img src="images/trash.gif" border="0" alt="Delete"></a></td>
		</tr>
		</cfoutput>
		</cfif>
		<cfif getincsearches.recordcount gt 0>
			<cfoutput query="getincsearches" group="incsearchtype">
			<tr>
			<td class="formlable" colspan="2">
			<cfswitch expression="#incsearchtype#">
				<cfcase value="fa">
					First Alert Searches
				</cfcase>
				<cfcase value="incident">
					Incident Searches
				</cfcase>
				<cfcase value="irp">
					IRP Searches
				</cfcase>
			</cfswitch>
			</td>
		</tr>
		<cfoutput>
		
		<cfswitch expression="#incsearchtype#">
				<cfcase value="fa">
					<tr>
									
										
		<td class="bodyTextGrey" width="95%"><a href="#self#?fuseaction=#attributes.xfa.fasearch#&uincsearch=#UserIncSearchID#">#SearchName#<cfif trim(SaveCtr) gt 1> (#SaveCtr#)</cfif></a></td>
		<td class="bodyTextGrey"><a href="#self#?fuseaction=#attributes.xfa.deleteincsearch#&prjsearch=#UserIncSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');" title="Delete"><img src="images/trash.gif" border="0" alt="Delete" ></a></td>
		</tr>
				</cfcase>
				<cfcase value="incident">
					<tr>
									
										
		<td class="bodyTextGrey" width="95%"><a href="#self#?fuseaction=#attributes.xfa.incidentsearch#&uincsearch=#UserIncSearchID#">#SearchName#<cfif trim(SaveCtr) gt 1> (#SaveCtr#)</cfif></a></td>
		<td class="bodyTextGrey"><a href="#self#?fuseaction=#attributes.xfa.deleteincsearch#&prjsearch=#UserIncSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');" title="Delete"><img src="images/trash.gif" border="0" alt="Delete" ></a></td>
		</tr>
				</cfcase>
				<cfcase value="irp">
					<tr>
									
										
		<td class="bodyTextGrey" width="95%"><a href="#self#?fuseaction=#attributes.xfa.irpsearch#&uincsearch=#UserIncSearchID#">#SearchName#<cfif trim(SaveCtr) gt 1> (#SaveCtr#)</cfif></a></td>
		<td class="bodyTextGrey"><a href="#self#?fuseaction=#attributes.xfa.deleteincsearch#&prjsearch=#UserIncSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');" title="Delete"><img src="images/trash.gif" border="0" alt="Delete" ></a></td>
		</tr>
				</cfcase>
			</cfswitch>
		
		
		</cfoutput>
		</cfoutput>
		</cfif>
	</table>
	</div>
</div>
<!--- <table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="65%" align="center">
	<tr><td colspan="2" class="bodyTextWhite">Saved Searches</td>
	</tr>

		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="4" border="0" bgcolor="ffffff" width="100%">
						<tr>
							<td class="formlable" align="center" width="50%"><strong>Project Group Searches</strong></td>
							<td class="formlable" align="center" width="50%"><strong>Incident Searches</strong></td>
						</tr>
						<tr>
							<td valign="top">
								<table cellpadding="4" cellspacing="0" border="0" width="100%">
								<cfoutput query="getuserprjsearches">
									<tr>
																
									<td class="bodyTextGrey" width="90%"><a href="#self#?fuseaction=#attributes.xfa.prjsearch#&uprjsearch=#PrjSearchID#">#PrjSearchName#<cfif trim(SaveCtr) gt 1> (#SaveCtr#)</cfif></a></td>
									<td class="bodyTextGrey"><a href="#self#?fuseaction=#attributes.xfa.deleteprjsearch#&prjsearch=#PrjSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');" title="Delete"><img src="images/trash.gif" border="0" alt="Delete"></a></td>
									</tr>
								</cfoutput>
								</table>							
							</td>
							<td valign="top">
								<table cellpadding="4" cellspacing="0" border="0" width="100%">
								<cfoutput query="getincsearches">
									<tr>
									
										
									<td class="bodyTextGrey" width="90%"><a href="#self#?fuseaction=#attributes.xfa.incidentsearch#&uincsearch=#UserIncSearchID#">#SearchName#<cfif trim(SaveCtr) gt 1> (#SaveCtr#)</cfif></a></td>
									<td class="bodyTextGrey"><a href="#self#?fuseaction=#attributes.xfa.deleteincsearch#&prjsearch=#UserIncSearchID#" onclick="return confirm('Are you sure you want to delete this saved search?');" title="Delete"><img src="images/trash.gif" border="0" alt="Delete" ></a></td>
									</tr>
								</cfoutput>
								</table>			
							</td>
						</tr>
				</table>
			</td>
		</tr>
	</table> --->