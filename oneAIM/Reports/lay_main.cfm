<cfif listlen(request.userlevel eq 1)>
	<cfif request.userlevel eq "Global IT">
		<cflocation url="/#getappconfig.oneAIMpath#/index.cfm?fuseaction=adminfunctions.main" addtoken="No">
	</cfif>
</cfif>
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="main">
		<cfset titleaddon = " - Main Page">
	</cfcase>
</cfswitch>
<cfparam name="frompg" default="srchfrm">
<cfoutput>
<cfif fusebox.fuseaction contains "print" or fusebox.fuseaction eq "exportincwdesc" or fusebox.fuseaction eq "exportreasonlate">
#Fusebox.layout#
<cfelse>
<table width="100%" cellpadding="2" cellspacing="0" border="0" >

<tr class="purplebg"><td  class="bodytextwhitelg"  width="90%">&nbsp;<cfif listfindnocase("potrating,printpotrating,ratetriangle,home,incwdesc,daysaway,oshachart,incbytype,monthly,incidentman,configdash,frb,mht,myreports,rwc,directmyreport,clientperf,contractorperf,incbytypechart,bird,spills,incclasschart,capalog,ouincclasschart,prjincclasschart,daysopen,daystoclose,frbdd,immcausechart,printimmcausechart,ouimmcausechart,printouimmcausechart,prjimmcausechart,printprjimmcausechart,injcausechart,printinjcausechart,ouinjcausechart,printouinjcausechart,prjinjcausechart,printprjinjcausechart,injcausechart,printinjcausechart,ouinjcausechart,printouinjcausechart,prjinjcausechart,printprjinjcausechart,hazardsourcechart,printhazardsourcechart,ouhazardsourcechart,printouhazardsourcechart,prjhazardsourcechart,printprjhazardsourcechart,rootcausechart,printrootcausechart,ourootcausechart,printourootcausechart,prjrootcausechart,prjprintrootcausechart,rootcausesubchart,printrootcausesubchart,ourootcausesubchart,printourootcausesubchart,prjrootcausesubchart,prjprintrootcausesubchart,immcausesubchart,printimmcausesubchart,ouimmcausesubchart,printouimmcausesubchart,prjimmcausesubchart,prjprintimmcausesubchart,reasonlate,numemployed,empbymonth,outstandingmh,safetyessentials,ousafetyessentials,prjsafetyessentials,safetyrules,ousafetyrules,prjsafetyrules,TRIrolling,ALLrolling,LTIrolling",fusebox.fuseaction) gt 0>Reports&nbsp;Home
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="myreports">-&nbsp;My&nbsp;Saved&nbsp;Reports</cfcase>
	<cfcase value="potrating">-&nbsp;Incident&nbsp;Potential&nbsp;Rating&nbsp;Matrix</cfcase>
	<cfcase value="ratetriangle">-&nbsp;Incident&nbsp;Potential&nbsp;Rating&nbsp;Triangle</cfcase>
	<cfcase value="incwdesc">-&nbsp;Incidents&nbsp;with&nbsp;Description</cfcase>
	<cfcase value="daysaway">-&nbsp;Days&nbsp;Away&nbsp;from&nbsp;Work</cfcase>
	<cfcase value="oshachart">-&nbsp;Injury&nbsp;Type&nbsp;by&nbsp;OSHA&nbsp;Rating&nbsp;Chart</cfcase>
	<cfcase value="incbytype">-&nbsp;Incidents&nbsp;by&nbsp;Type</cfcase>
	<cfcase value="monthly">-&nbsp;Monthly&nbsp;Statistic&nbsp;Table/Monthly&nbsp;Cumulative</cfcase>
	<cfcase value="incidentman">-&nbsp;Incident&nbsp;Man</cfcase>
	<cfcase value="configdash">-&nbsp;Configurable&nbsp;Dashboard</cfcase>
	<cfcase value="frb,frbdd">-&nbsp;Frequency&nbsp;Rate&nbsp;Breakdown&nbsp;Table</cfcase>
	<cfcase value="mht">-&nbsp;Man-Hours&nbsp;Total&nbsp;Report</cfcase>
	<cfcase value="outstandingmh">-&nbsp;Outstanding&nbsp;Manhours</cfcase>
	<cfcase value="rwc">-&nbsp;Restricted&nbsp;Work&nbsp;Cases</cfcase>
	<cfcase value="clientperf">-&nbsp;Client&nbsp;Performance</cfcase>
	<cfcase value="contractorperf">-&nbsp;Contractor&nbsp;Performance</cfcase>
	<cfcase value="incbytypechart">-&nbsp;Incidents&nbsp;by&nbsp;Type&nbsp;Chart</cfcase>
	<cfcase value="bird">-&nbsp;BIRD&nbsp;Triangle</cfcase>
	<cfcase value="spills">-&nbsp;Environmental&nbsp;Spills</cfcase>
	<cfcase value="incclasschart,ouincclasschart,prjincclasschart">-&nbsp;Incidents&nbsp;Classification&nbsp;Chart</cfcase>
	<cfcase value="capalog">-&nbsp;CA/PA&nbsp;Log</cfcase>
	<cfcase value="daysopen">-&nbsp;Incident&nbsp;Days&nbsp;Open</cfcase>
	<cfcase value="daystoclose">-&nbsp;Incident&nbsp;Days&nbsp;to&nbsp;Close</cfcase>
	<cfcase value="immcausechart,ouimmcausechart,prjimmcausechart">-&nbsp;Immediate&nbsp;Causes</cfcase>
	<cfcase value="injcausechart,ouinjcausechart,prjinjcausechart">-&nbsp;Cause&nbsp;of&nbsp;Injury&nbsp;Incident</cfcase>
	<cfcase value="hazardsourcechart,ouhazardsourcechart,prjhazardsourcechart">-&nbsp;Source&nbsp;of&nbsp;Hazard</cfcase>
	<cfcase value="rootcausechart,ourootcausechart,prjrootcausechart">-&nbsp;Root&nbsp;Cause</cfcase>
	<cfcase value="rootcausesubchart,ourootcausesubchart,prjrootcausesubchart">-&nbsp;Root&nbsp;Cause&nbsp;Sub-Type</cfcase>
	<cfcase value="immcausesubchart,ouimmcausesubchart,prjimmcausesubchart">-&nbsp;Immediate&nbsp;Cause&nbsp;Sub-Type</cfcase>
	<cfcase value="reasonlate">-&nbsp;Reason&nbsp;for&nbsp;Late&nbsp;Recording</cfcase>
	<cfcase value="numemployed,empbymonth">-&nbsp;Numbers&nbsp;Employed</cfcase>
	<cfcase value="safetyessentials,ousafetyessentials,prjsafetyessentials">-&nbsp;Safety&nbsp;Essentials</cfcase>
	<cfcase value="safetyrules,ousafetyrules,prjsafetyrules">-&nbsp;Global&nbsp;Safety&nbsp;Rules</cfcase>
	<cfcase value="TRIrolling">-&nbsp;Total&nbsp;Recordable&nbsp;Incidents&nbsp;Rolling&nbsp;Chart</cfcase>
	<cfcase value="ALLrolling">-&nbsp;All&nbsp;Injury&nbsp;Incidents&nbsp;Rolling&nbsp;Chart</cfcase>
	<cfcase value="LTIrolling">-&nbsp;Lost&nbsp;Time&nbsp;Injuries&nbsp;Rolling&nbsp;Chart</cfcase>
</cfswitch>
<cfelseif listfindnocase("diamond,rundiamond,diamonddetail",fusebox.fuseaction) gt 0>Reports&nbsp;Home&nbsp;-&nbsp;Mining&nbsp;the&nbsp;Diamond<cfelseif listfindnocase("doprjsearchadmin,prjsearchadmin",fusebox.fuseaction) gt 0>&nbsp;Admin&nbsp;Functions&nbsp;-&nbsp;Manage&nbsp;Project/Office&nbsp;Groups<cfelseif listfindnocase("mysearches",fusebox.fuseaction) gt 0>Search&nbsp;Functions&nbsp;-&nbsp;My&nbsp;Saved&nbsp;Searches<cfelse>Search&nbsp;Functions</cfif></td>
<td align="right"  class="bodytextwhite" ><cfif trim(request.fname) neq ''>Welcome&nbsp;<cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td><td align="right" class="helptext">&nbsp;<a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.oneAIMpath#/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');" class="helptext">Help</a>&nbsp;</span><td align="right">&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<table width="85%" cellpadding="0" cellspacing="0" border="0"  id="mytab">

<cfif listfindnocase("doprjsearchadmin,prjsearchadmin",fusebox.fuseaction) gt 0>
<tr>
		<td><img src="images/spacer.gif" height="6" border="0"></td>
	</tr>
<tr>
	<td>
		<table  cellpadding="0" cellspacing="0">
		<cfoutput>
			<!---  --->
			<tr>
				<td class="tab1bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab1bkg"  background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.main" class="bodyTextPurple">Add&nbsp;Project/Office</a></td>
				<td class="tab1bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td class="tab2bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab2bkg" background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.grouplist" class="bodyTextPurple">View&nbsp;Project/Office&nbsp;List</a></td>
				<td class="tab2bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td class="tab4bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab4bkg" ><span class="bodyTextPurple">Search&nbsp;for&nbsp;Project/Office</span></td>
				<td class="tab4bkg"><img src="images/tabright.png" border="0"></td>
				<td width="90%"></td>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=adminfunctions.main" class="bodytextsmpurple"><<&nbsp;Admin&nbsp;Home</a>&nbsp;&nbsp;</td>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple"><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;</td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
<cfelse>
<tr>
	<td align="right">
		<table cellpadding="0" cellspacing="0" border="0">
	
		<cfif fusebox.fuseaction eq "doprjsearch">
			<cfif frompg eq "mysearch">
				<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.mysearches" class="bodytextsmpurple"><<&nbsp;My&nbsp;Searches</a></td><!--- <cfelse>&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.cfm?fuseaction=reports.prjsearch" style="font-family:Segoe UI;font-size:10px;text-decoration:none;color:5f2468;"><<&nbsp;Search&nbsp;Again</a> --->
			</cfif>
		</cfif>
		<cfif fusebox.fuseaction eq "doincidentsearch">
			<cfif frompg eq "mysearch">
			<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.mysearches" class="bodytextsmpurple"><<&nbsp;My&nbsp;Searches</a></td><!--- <cfelse>&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.cfm?fuseaction=reports.incidentsearch" style="font-family:Segoe UI;font-size:10px;text-decoration:none;color:5f2468;"><<&nbsp;Search&nbsp;Again</a> --->
			</cfif>
		</cfif>
	
		<cfif listfindnocase("potrating,printpotrating,ratetriangle,incwdesc,daysaway,oshachart,incbytype,monthly,incidentman,configdash,frb,mht,outstandingmh,myreports,diamond,rundiamond,diamonddetail,rwc,clientperf,contractorperf,incbytypechart,bird,spills,incclasschart,capalog,ouincclasschart,prjincclasschart,daysopen,daystoclose,frbdd,immcausechart,printimmcausechart,ouimmcausechart,printouimmcausechart,prjimmcausechart,printprjimmcausechart,injcausechart,printinjcausechart,ouinjcausechart,printouinjcausechart,prjinjcausechart,printprjinjcausechart,hazardsourcechart,printhazardsourcechart,ouhazardsourcechart,printouhazardsourcechart,prjhazardsourcechart,printprjhazardsourcechart,rootcausechart,printrootcausechart,ourootcausechart,printourootcausechart,prjrootcausechart,prjprintrootcausechart,rootcausesubchart,printrootcausesubchart,ourootcausesubchart,printourootcausesubchart,prjrootcausesubchart,prjprintrootcausesubchart,immcausesubchart,printimmcausesubchart,ouimmcausesubchart,printouimmcausesubchart,prjimmcausesubchart,prjprintimmcausesubchart,reasonlate,numemployed,empbymonth,safetyessentials,ousafetyessentials,prjsafetyessentials,safetyrules,ousafetyrules,prjsafetyrules,TRIrolling,ALLrolling,LTIrolling",fusebox.fuseaction) gt 0>
			<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.home" class="bodytextsmpurple"><<&nbsp;Reports&nbsp;Home&nbsp;&nbsp;</a></td>
			<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.myreports" class="bodytextsmpurple"><<&nbsp;My&nbsp;Saved&nbsp;Reports&nbsp;&nbsp;</a></td>
		</cfif>
		<cfif listfindnocase("incidentsearch,doincidentsearch,fasearch,dofasearch,irpsearch,doirpsearch,prjsearch,doprjsearch,mysearches",fusebox.fuseaction) gt 0>
			<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.incidentsearch" class="bodytextsmpurple"><<&nbsp;Search&nbsp;Incidents&nbsp;&nbsp;</a></td>
			<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.prjsearch" class="bodytextsmpurple"><<&nbsp;Search&nbsp;for&nbsp;Project/Office&nbsp;&nbsp;</a></td>
			<td align="right"><img src="images/navlft3.png" border="0"></td>
			<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=reports.mysearches" class="bodytextsmpurple"><<&nbsp;My&nbsp;Saved&nbsp;Searches&nbsp;&nbsp;</a></td>
		</cfif>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple"><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;</td>
		<!--- <a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple"><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;&nbsp;&nbsp; --->
		</table>
	</td>
</tr>
</cfif>
<tr>
	<td valign="top" colspan="2" align="center">
		#Fusebox.layout#
	</td>
</tr>
</table>
</cfif>
</cfoutput>