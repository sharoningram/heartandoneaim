<cfparam name="dosearch" default="">



<cfset allinjcausesstruct = {}>
<cfloop query="getrootcauses">
	<cfif not structkeyexists(allinjcausesstruct,CategoryID)>
		<cfset allinjcausesstruct[CategoryID] = rootname>
	</cfif>
</cfloop>

<cfif  listfindnocase("rootcausesubchart,printrootcausesubchart",fusebox.fuseaction) gt 0>

<cfset buildurl = "">


<cfif isdefined("form.fieldnames")>
<cfloop list="#form.fieldnames#" index="i">
	<cfif i neq "fieldnames">
		<cfif listfindnocase("bu,ou",i) eq 0>
			<cfset buildurl = listappend(buildurl,"#i#=#evaluate(i)#","&")>
		</cfif>
	</cfif>
</cfloop>
</cfif>



		<cfset injcausestruct = {}>
		<cfset getinjchrt = QueryNew("buid, buname, injcauseid, noinjcause")>
		<cfloop query="getrootcausechart">
			<cfloop list="#AZCatID#" index="i">
				<cfif not structkeyexists(injcausestruct,"#buid#_#i#")>
					<cfset injcausestruct["#buid#_#i#"] = 1>
				<cfelse>
					<cfset injcausestruct["#buid#_#i#"] = injcausestruct["#buid#_#i#"]+1>
				</cfif> 
			</cfloop>
		</cfloop>
		
		<cfset ctr = 0>
			
			<cfloop list="#structkeylist(injcausestruct)#" index="i">
			<cfset QueryAddRow( getinjchrt ) />
			<cfset ctr = ctr+1>
				<cfloop query="getrootcausechart">
					<cfif buid eq listgetat(i,1,"_")>
						<cfset getinjchrt["buid"][ctr] = buid />
						<cfset getinjchrt["buname"][ctr] = buname>
					</cfif>
				</cfloop>
				<cfset getinjchrt["injcauseid"][ctr] = listgetat(i,2,"_")>
				<cfset getinjchrt["noinjcause"][ctr] = injcausestruct["#i#"]>
			</cfloop>
			
		<cfquery name="gettop5" dbtype="query">
			select buid, buname, injcauseid, noinjcause
			from getinjchrt
			order by buid, noinjcause desc
		</cfquery>
		<!--- <cfoutput query="gettop5">
		#buid#, #buname#, #injcauseid#, #noinjcause#<br>
		</cfoutput> --->
		<!--- <cfdump var="#allinjcausesstruct#"> --->
		<cfset dataforchart = QueryNew("buid, buname, injcauseid, noinjcause, injcausename")>
		<cfset totctr = 0>
		<cfset maxNum = 0>
		<cfoutput query="gettop5" group="buid">
			<cfset buctr = 0>
			<cfoutput>
				
				<cfset buctr = buctr + 1>
				<cfif buctr lte 5>
				<cfset totctr = totctr+1>
					<cfif structkeyexists(allinjcausesstruct,injcauseid)>
						<cfif noinjcause gt maxNum>
							<cfset maxNum = noinjcause>
						</cfif>
						<cfset QueryAddRow( dataforchart ) />
						<cfset dataforchart["buid"][totctr] = buid />
						<cfset dataforchart["buname"][totctr] = buname />
						<cfset dataforchart["injcauseid"][totctr] = injcauseid />
						<cfset dataforchart["noinjcause"][totctr] = noinjcause />
						<cfset dataforchart["injcausename"][totctr] = allinjcausesstruct[injcauseid] />
					</cfif>
				</cfif>
			</cfoutput>
		</cfoutput>
		
		
		<cfset charttop = 10>
		<cfif maxnum lt 10>
			<cfset charttop = maxnum + (10-maxnum)>
		<cfelseif maxnum eq 10>
			<cfset charttop = maxnum+10>
		<cfelse>
			<cfif maxnum mod 10 eq 0>
				<cfset charttop = maxnum+10>
			<cfelse>
				<cfset chknum = right(maxnum,1)>
				<cfloop from="1" to="10" index="i">
					<cfset thenumval = chknum+i>
					
					<cfif thenumval mod 10 eq 0>
						<cfset charttop = maxnum+i>
						<cfbreak>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>
		
		<cfif  listfindnocase("printrootcausesubchart",fusebox.fuseaction) eq 0>
		
		<cfoutput>
		
		<form action="#self#?fuseaction=#attributes.xfa.printrootcausesubchart#" name="printfrm" method="post" target="_blank">
			<input type="hidden" name="incyear" value="#incyear#">
		<input type="hidden" name="invlevel" value="#invlevel#">
		<input type="hidden" name="bu" value="#bu#">
		<input type="hidden" name="ou" value="#ou#">
		<input type="hidden" name="INCNM" value="#INCNM#">
		<input type="hidden" name="businessstream" value="#businessstream#">
		<input type="hidden" name="projectoffice" value="#projectoffice#">
		<input type="hidden" name="site" value="#site#">
		<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
		<input type="hidden" name="frmjv" value="#frmjv#">
		<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
		<input type="hidden" name="frmclient" value="#frmclient#">
		<input type="hidden" name="locdetail" value="#locdetail#">
		<input type="hidden" name="frmcountryid" value="#frmcountryid#">
		<input type="hidden" name="frmincassignto" value="#frmincassignto#">
		<input type="hidden" name="wronly" value="#wronly#">
		<input type="hidden" name="primarytype" value="#primarytype#">
		<input type="hidden" name="secondtype" value="#secondtype#">
		<input type="hidden" name="hipoonly" value="#hipoonly#">
		<input type="hidden" name="oshaclass" value="#oshaclass#">
		<input type="hidden" name="oidef" value="#oidef#">
		<input type="hidden" name="seriousinj" value="#seriousinj#">
		<input type="hidden" name="secinccats" value="#secinccats#">
		<input type="hidden" name="eic" value="#eic#">
		<input type="hidden" name="damagesrc" value="#damagesrc#">
		<input type="hidden" name="startdate" value="#startdate#">
		<input type="hidden" name="enddate" value="#enddate#">
		<input type="hidden" name="dosearch" value="#dosearch#">
		<input type="hidden" name="assocg" value="#assocg#">
		<input type="hidden" name="potentialrating" value="#potentialrating#">	
		<input type="hidden" name="occillcat" value="#occillcat#">	
			</form>
			</cfoutput>
		
		</cfif>

			<cfset howmanytds = 0>
			<cfoutput query="dataforchart" group="buid">
				<cfset howmanytds = howmanytds+1>
			</cfoutput>

			<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
			<tr>
				<td class="bodyTexthd" colspan="2">Root cause sub-type - top five</td>
			</tr><cfoutput>
			<tr>
				<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
				<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printrootcausesubchart"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
			</tr></cfoutput>
			</table>
			<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
				<tr>
					<td class="bodyTextWhite">&nbsp;</td>
				</tr>
				<tr>
					<td bgcolor="ffffff" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								
								<cfoutput query="dataforchart" group="buid">
									<cfset buname = replace(buname,"<","&lt;","all")>
									<cfset buname = replace(buname,">","&gt;","all")>
									<cfset buname = replace(buname,"'","","all")>
									<cfset chartxml="<chart caption='#buname# - Root cause sub-type' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='0' canvasBorderColor='##dfdfdf' canvasBorderThickness='0' showPlotBorder='0' plotFillRatio='100'  legendBorderThickness='0' legendShadow='0' legendPosition='bottom' legendNumColumns='3' alignLegendWithCanvas='1' chartLeftMargin='0' chartRightMargin='0'><categories>">
									<cfset chartxml= chartxml & "<category label='#buname#' />">
									<cfset chartxml= chartxml & "</categories>">
									<cfoutput>
									<cfset dspinjc = replace(injcausename,"/","","all")>
									<cfset chartxml= chartxml & "<dataset seriesname='#trim(dspinjc)#'>">
									<cfif trim(buildurl) neq ''>
										<cfset chartxml= chartxml & "<set value='#noinjcause#'  link='index.cfm?fuseaction=reports.ourootcausesubchart&bu=#buid#&#buildurl#' />">
									<cfelse>
										<cfset chartxml= chartxml & "<set value='#noinjcause#'  link='index.cfm?fuseaction=reports.ourootcausesubchart&bu=#buid#' />">
									</cfif>
									<cfset chartxml= chartxml & "</dataset>">
									</cfoutput>
									<cfset chartxml= chartxml &  "</chart>">
									<cfset tdwide = 100/howmanytds>
									<td valign="top" width="#tdwide#%">
									<div id="chartdiv#buid#"></div>
									</td>
									
									 <script type="text/javascript">
										FusionCharts.ready(function () {
										    var thisChart = new FusionCharts({
										        "type": "mscolumn2d",
										        "renderAt": "chartdiv#buid#",
										        "width": "100%",
										        "height": "450",
										        "dataFormat": "xml",
										        "dataSource":  "#chartxml#"
										    }
											);
										    thisChart.render();
										});
									</script>	
								</cfoutput>
							</tr>
						</table>
					
					</td>
				</tr>
			</table>

<cfelseif  listfindnocase("ourootcausesubchart,printourootcausesubchart",fusebox.fuseaction) gt 0>

<cfset buildurl = "">


<cfif isdefined("form.fieldnames")>
<cfloop list="#form.fieldnames#" index="i">
	<cfif i neq "fieldnames">
		<cfif listfindnocase("bu,ou",i) eq 0>
			<cfset buildurl = listappend(buildurl,"#i#=#evaluate(i)#","&")>
		</cfif>
	</cfif>
</cfloop>
<cfelse>
	<cfset buildurl = cgi.query_string>
	<cfset buildurl = replacenocase(buildurl,"fuseaction=reports.ourootcausesubchart","","all")>
	<cfset buildurl = rereplacenocase(buildurl,"&bu=[0-9]*","","all")>
</cfif>


	<cfset injcausestruct = {}>
		<cfset getinjchrt = QueryNew("ouid, ouname, injcauseid, noinjcause")>
		<cfloop query="getrootcausechart">
			<cfloop list="#AZCatID#" index="i">
				<cfif not structkeyexists(injcausestruct,"#ouid#_#i#")>
					<cfset injcausestruct["#ouid#_#i#"] = 1>
				<cfelse>
					<cfset injcausestruct["#ouid#_#i#"] = injcausestruct["#ouid#_#i#"]+1>
				</cfif> 
			</cfloop>
		</cfloop>
		
		<cfset ctr = 0>
			
			<cfloop list="#structkeylist(injcausestruct)#" index="i">
			<cfset QueryAddRow( getinjchrt ) />
			<cfset ctr = ctr+1>
				<cfloop query="getrootcausechart">
					<cfif ouid eq listgetat(i,1,"_")>
						<cfset getinjchrt["ouid"][ctr] = ouid />
						<cfset getinjchrt["ouname"][ctr] = ouname>
					</cfif>
				</cfloop>
				<cfset getinjchrt["injcauseid"][ctr] = listgetat(i,2,"_")>
				<cfset getinjchrt["noinjcause"][ctr] = injcausestruct["#i#"]>
			</cfloop>
			
		<cfquery name="gettop5" dbtype="query">
			select ouid, ouname, injcauseid, noinjcause
			from getinjchrt
			order by ouid, noinjcause desc
		</cfquery>
		
		<cfset dataforchart = QueryNew("ouid, ouname, injcauseid, noinjcause, injcausename")>
		<cfset totctr = 0>
		<cfset maxNum = 0>
		<cfoutput query="gettop5" group="ouid">
			<cfset ouctr = 0>
			<cfoutput>
				
				<cfset ouctr = ouctr + 1>
				<cfif ouctr lte 5>
					<cfset totctr = totctr+1>
					<cfif structkeyexists(allinjcausesstruct,injcauseid)>
						<cfif noinjcause gt maxNum>
							<cfset maxNum = noinjcause>
						</cfif>
						<cfset QueryAddRow( dataforchart ) />
						<cfset dataforchart["ouid"][totctr] = ouid />
						<cfset dataforchart["ouname"][totctr] = ouname />
						<cfset dataforchart["injcauseid"][totctr] = injcauseid />
						<cfset dataforchart["noinjcause"][totctr] = noinjcause />
						<cfset dataforchart["injcausename"][totctr] = allinjcausesstruct[injcauseid] />
					</cfif>
				</cfif>
			</cfoutput>
		</cfoutput>
		
		
		<cfset charttop = 10>
		<cfif maxnum lt 10>
			<cfset charttop = maxnum + (10-maxnum)>
		<cfelseif maxnum eq 10>
			<cfset charttop = maxnum+10>
		<cfelse>
			<cfif maxnum mod 10 eq 0>
				<cfset charttop = maxnum+10>
			<cfelse>
				<cfset chknum = right(maxnum,1)>
				<cfloop from="1" to="10" index="i">
					<cfset thenumval = chknum+i>
					
					<cfif thenumval mod 10 eq 0>
						<cfset charttop = maxnum+i>
						<cfbreak>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>
		
		<cfif  listfindnocase("printourootcausesubchart",fusebox.fuseaction) eq 0>
		
		<cfoutput>
		
		<form action="#self#?fuseaction=#attributes.xfa.printourootcausesubchart#" name="printfrm" method="post" target="_blank">
			<input type="hidden" name="incyear" value="#incyear#">
		<input type="hidden" name="invlevel" value="#invlevel#">
		<input type="hidden" name="bu" value="#bu#">
		<input type="hidden" name="ou" value="#ou#">
		<input type="hidden" name="INCNM" value="#INCNM#">
		<input type="hidden" name="businessstream" value="#businessstream#">
		<input type="hidden" name="projectoffice" value="#projectoffice#">
		<input type="hidden" name="site" value="#site#">
		<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
		<input type="hidden" name="frmjv" value="#frmjv#">
		<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
		<input type="hidden" name="frmclient" value="#frmclient#">
		<input type="hidden" name="locdetail" value="#locdetail#">
		<input type="hidden" name="frmcountryid" value="#frmcountryid#">
		<input type="hidden" name="frmincassignto" value="#frmincassignto#">
		<input type="hidden" name="wronly" value="#wronly#">
		<input type="hidden" name="primarytype" value="#primarytype#">
		<input type="hidden" name="secondtype" value="#secondtype#">
		<input type="hidden" name="hipoonly" value="#hipoonly#">
		<input type="hidden" name="oshaclass" value="#oshaclass#">
		<input type="hidden" name="oidef" value="#oidef#">
		<input type="hidden" name="seriousinj" value="#seriousinj#">
		<input type="hidden" name="secinccats" value="#secinccats#">
		<input type="hidden" name="eic" value="#eic#">
		<input type="hidden" name="damagesrc" value="#damagesrc#">
		<input type="hidden" name="startdate" value="#startdate#">
		<input type="hidden" name="enddate" value="#enddate#">
		<input type="hidden" name="dosearch" value="#dosearch#">
		<input type="hidden" name="assocg" value="#assocg#">
		<input type="hidden" name="potentialrating" value="#potentialrating#">	
		<input type="hidden" name="occillcat" value="#occillcat#">	
			</form>
			</cfoutput>
		
		</cfif>

			<cfset howmanytds = 0>
			<cfoutput query="dataforchart" group="ouid">
				<cfset howmanytds = howmanytds+1>
			</cfoutput>


			<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
			<tr>
				<td class="bodyTexthd" colspan="2">Root cause sub-type - top five<cfif fusebox.fuseaction neq "printourootcausesubchart">&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Reset" onclick="window.location('index.cfm?fuseaction=reports.rootcausesubchart');"></cfif></td>
			</tr><cfoutput>
			<tr>
				<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
				<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printourootcausesubchart"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
			</tr></cfoutput>
			</table>
			<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
				<tr>
					<td class="bodyTextWhite">&nbsp;</td>
				</tr>
				<tr>
					<td bgcolor="ffffff" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								
								<cfoutput query="dataforchart" group="ouid">
									<cfset ouname = replace(ouname,"<","&lt;","all")>
									<cfset ouname = replace(ouname,">","&gt;","all")>
									<cfset ouname = replace(ouname,"'","","all")>
									<cfset chartxml="<chart caption='#ouname# - Root cause sub-type' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='0' canvasBorderColor='##dfdfdf' canvasBorderThickness='0' showPlotBorder='0' plotFillRatio='100'  legendBorderThickness='0' legendShadow='0' legendPosition='bottom' legendNumColumns='2' alignLegendWithCanvas='1'  chartLeftMargin='0' chartRightMargin='0'><categories>">
									<cfset chartxml= chartxml & "<category label='#ouname#' />">
									<cfset chartxml= chartxml & "</categories>">
									<cfoutput>
									<cfset dspinjc = replace(injcausename,"/","","all")>
									<cfset chartxml= chartxml & "<dataset seriesname='#trim(dspinjc)#'>">
									<cfif trim(buildurl) neq ''>
										<cfset chartxml= chartxml & "<set value='#noinjcause#'  link='index.cfm?fuseaction=reports.prjrootcausesubchart&ou=#ouid#&#buildurl#' />">
									<cfelse>
										<cfset chartxml= chartxml & "<set value='#noinjcause#'  link='index.cfm?fuseaction=reports.prjrootcausesubchart&ou=#ouid#' />">
									</cfif>
									<cfset chartxml= chartxml & "</dataset>">
									</cfoutput>
									<cfset chartxml= chartxml &  "</chart>">
									<cfset tdwide = 100/howmanytds>
									<td valign="top" width="#tdwide#%">
									<div id="chartdiv#ouid#"></div>
									</td>
									
									 <script type="text/javascript">
										FusionCharts.ready(function () {
										    var thisChart = new FusionCharts({
										        "type": "mscolumn2d",
										        "renderAt": "chartdiv#ouid#",
										        "width": "100%",
										        "height": "450",
										        "dataFormat": "xml",
										        "dataSource":  "#chartxml#"
										    }
											);
										    thisChart.render();
										});
									</script>	
								</cfoutput>
							</tr>
						</table>
					
					</td>
				</tr>
			</table>
		

<cfelseif  listfindnocase("prjrootcausesubchart,prjprintrootcausesubchart",fusebox.fuseaction) gt 0>
<cfset injcausestruct = {}>
		<cfset getinjchrt = QueryNew("prjid, prjname, injcauseid, noinjcause")>
		<cfloop query="getrootcausechart">
			<cfloop list="#AZCatID#" index="i">
				<cfif not structkeyexists(injcausestruct,"#prjid#_#i#")>
					<cfset injcausestruct["#prjid#_#i#"] = 1>
				<cfelse>
					<cfset injcausestruct["#prjid#_#i#"] = injcausestruct["#prjid#_#i#"]+1>
				</cfif> 
			</cfloop>
		</cfloop>
		
		<cfset ctr = 0>
			<cfoutput>
			<cfloop list="#structkeylist(injcausestruct)#" index="i">
			
			<cfset QueryAddRow( getinjchrt ) />
			<cfset ctr = ctr+1>
				<cfloop query="getrootcausechart">
					<cfif prjid eq listgetat(i,1,"_")>
						<cfset getinjchrt["prjid"][ctr] = prjid />
						<cfset getinjchrt["prjname"][ctr] = prjname>
					</cfif>
				</cfloop>
				<cfset getinjchrt["injcauseid"][ctr] = listgetat(i,2,"_")>
				<cfset getinjchrt["noinjcause"][ctr] = injcausestruct["#i#"]>
			</cfloop>
			</cfoutput>
		<cfquery name="gettop5" dbtype="query">
			select prjid, prjname, injcauseid, noinjcause
			from getinjchrt
			order by prjid, noinjcause desc
		</cfquery>
		
		
		<cfset dataforchart = QueryNew("prjid, prjname, injcauseid, noinjcause, injcausename")>
		<cfset totctr = 0>
		<cfset maxNum = 0>
		<cfoutput query="gettop5" group="prjid">
			<cfset prjctr = 0>
			<cfoutput>
				
				<cfset prjctr = prjctr + 1>
				
				<cfif prjctr lte 5>
					<cfset totctr = totctr+1>
					<cfif structkeyexists(allinjcausesstruct,injcauseid)>
						<cfif noinjcause gt maxNum>
							<cfset maxNum = noinjcause>
						</cfif>
						<cfset QueryAddRow( dataforchart ) />
						<cfset dataforchart["prjid"][totctr] = prjid />
						<cfset dataforchart["prjname"][totctr] = prjname />
						<cfset dataforchart["injcauseid"][totctr] = injcauseid />
						<cfset dataforchart["noinjcause"][totctr] = noinjcause />
						<cfset dataforchart["injcausename"][totctr] = allinjcausesstruct[injcauseid] />
					</cfif>
				</cfif>
			</cfoutput>
		</cfoutput>
		
		
		<cfset charttop = 10>
		<cfif maxnum lt 10>
			<cfset charttop = maxnum + (10-maxnum)>
		<cfelseif maxnum eq 10>
			<cfset charttop = maxnum+10>
		<cfelse>
			<cfif maxnum mod 10 eq 0>
				<cfset charttop = maxnum+10>
			<cfelse>
				<cfset chknum = right(maxnum,1)>
				<cfloop from="1" to="10" index="i">
					<cfset thenumval = chknum+i>
					
					<cfif thenumval mod 10 eq 0>
						<cfset charttop = maxnum+i>
						<cfbreak>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>
		
		<cfif  listfindnocase("prjprintrootcausesubchart",fusebox.fuseaction) eq 0>
		
		<cfoutput>
		
		<form action="#self#?fuseaction=#attributes.xfa.prjprintrootcausesubchart#" name="printfrm" method="post" target="_blank">
			<input type="hidden" name="incyear" value="#incyear#">
		<input type="hidden" name="invlevel" value="#invlevel#">
		<input type="hidden" name="bu" value="#bu#">
		<input type="hidden" name="ou" value="#ou#">
		<input type="hidden" name="INCNM" value="#INCNM#">
		<input type="hidden" name="businessstream" value="#businessstream#">
		<input type="hidden" name="projectoffice" value="#projectoffice#">
		<input type="hidden" name="site" value="#site#">
		<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
		<input type="hidden" name="frmjv" value="#frmjv#">
		<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
		<input type="hidden" name="frmclient" value="#frmclient#">
		<input type="hidden" name="locdetail" value="#locdetail#">
		<input type="hidden" name="frmcountryid" value="#frmcountryid#">
		<input type="hidden" name="frmincassignto" value="#frmincassignto#">
		<input type="hidden" name="wronly" value="#wronly#">
		<input type="hidden" name="primarytype" value="#primarytype#">
		<input type="hidden" name="secondtype" value="#secondtype#">
		<input type="hidden" name="hipoonly" value="#hipoonly#">
		<input type="hidden" name="oshaclass" value="#oshaclass#">
		<input type="hidden" name="oidef" value="#oidef#">
		<input type="hidden" name="seriousinj" value="#seriousinj#">
		<input type="hidden" name="secinccats" value="#secinccats#">
		<input type="hidden" name="eic" value="#eic#">
		<input type="hidden" name="damagesrc" value="#damagesrc#">
		<input type="hidden" name="startdate" value="#startdate#">
		<input type="hidden" name="enddate" value="#enddate#">
		<input type="hidden" name="dosearch" value="#dosearch#">
		<input type="hidden" name="assocg" value="#assocg#">
		<input type="hidden" name="potentialrating" value="#potentialrating#">	
		<input type="hidden" name="occillcat" value="#occillcat#">	
			</form>
			</cfoutput>
		
		</cfif>

			<cfset howmanytds = 0>
			<cfoutput query="dataforchart" group="prjid">
				<cfset howmanytds = howmanytds+1>
			</cfoutput>
		
			<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
			<tr>
				<td class="bodyTexthd" colspan="2">Root cause sub-type - top five<cfif fusebox.fuseaction neq "prjprintrootcausesubchart">&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Reset" onclick="window.location('index.cfm?fuseaction=reports.rootcausesubchart');"></cfif></td>
			</tr><cfoutput>
			<tr>
				<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
				<td align="right" valign="middle"><cfif fusebox.fuseaction neq "prjprintrootcausesubchart"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
			</tr></cfoutput>
			</table>
			<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
				<tr>
					<td class="bodyTextWhite">&nbsp;</td>
				</tr>
				<tr>
					<td bgcolor="ffffff" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								
								<cfset rctr = 0>
								<cfoutput query="dataforchart" group="prjid">
								<cfset rctr = rctr+1>
									<cfset prjname = replace(prjname,"<","&lt;","all")>
									<cfset prjname = replace(prjname,">","&gt;","all")>
									<cfset prjname = replace(prjname,"'","","all")>
									<cfset chartxml="<chart caption='#prjname# - Root cause sub-type' subcaption='' xaxisname='' yaxisname='' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='4' yAxisMinValue='0' yAxisMaxValue='#charttop#' bgColor='##ffffff,##ffffff' canvasBgColor='##ffffff,##ffffff' bgAlpha='0' canvasBgAlpha='0' showBorder='0' canvasBorderColor='##dfdfdf' canvasBorderThickness='0' showPlotBorder='0' plotFillRatio='100'  legendBorderThickness='0' legendShadow='0' legendPosition='bottom' legendNumColumns='2' alignLegendWithCanvas='1' chartLeftMargin='0' chartRightMargin='0'><categories>">
									<cfset chartxml= chartxml & "<category label='#prjname#' />">
									<cfset chartxml= chartxml & "</categories>">
									<cfoutput>
									<cfset dspinjc = replace(injcausename,"/","","all")>
									<cfset chartxml= chartxml & "<dataset seriesname='#trim(dspinjc)#'>">
					
									<cfset chartxml= chartxml & "<set value='#noinjcause#' />">
									<cfset chartxml= chartxml & "</dataset>">
									</cfoutput>
									<cfset chartxml= chartxml &  "</chart>">
									<cfset tdwide = 100/howmanytds>
									<td valign="top" width="#tdwide#%">
									<div id="chartdiv#prjid#"></div>
									<cfif rctr mod 5 is 0>
									</td></tr>
									<cfelse>
									</td>
									</cfif>
									
									 <script type="text/javascript">
										FusionCharts.ready(function () {
										    var thisChart = new FusionCharts({
										        "type": "mscolumn2d",
										        "renderAt": "chartdiv#prjid#",
										        "width": "100%",
										        "height": "450",
										        "dataFormat": "xml",
										        "dataSource":  "#chartxml#"
										    }
											);
										    thisChart.render();
										});
									</script>	
								</cfoutput>
							</tr>
						</table>
					
					</td>
				</tr>
			</table>

</cfif>