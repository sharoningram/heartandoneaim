<cfparam name="dosearch" default="">
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_incbytypepdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "IncidentsByType_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape">
	
<cfset iostruct = {}>
<cfloop query="getincoi">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		
		<cfif PrimaryType eq 5>
			<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
				</cfif>
			<cfelse>
				<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"]+1>
				</cfif>
			</cfif>
			<cfif OIdefinition eq 2 and isnearmiss eq "no">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_OIDEF_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_OIDEF_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_OIDEF_#IncAssignedTo#"] =  iostruct["#PrimaryType#_OIDEF_#IncAssignedTo#"]+1>
				</cfif>
			
			</cfif>
		
		<cfelseif primarytype eq 1>
		
		<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
				</cfif>
		<cfelse>
		
			<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_#IncAssignedTo#")>
				<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] = 1>
			<cfelse>
				<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"]+1>
			</cfif>
		
		</cfif>
		</cfif>
	<cfelse>
		<cfif PrimaryType eq 5>
		<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
				</cfif>
		<cfelse>
		
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_oth")>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] =  iostruct["#PrimaryType#_#catid#_oth"]+1>
		</cfif>
		</cfif>
		<cfif OIdefinition eq 2>
				<cfif not structkeyexists(iostruct,"#PrimaryType#_OIDEF_oth")>
					<cfset iostruct["#PrimaryType#_OIDEF_oth"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_OIDEF_oth"] =  iostruct["#PrimaryType#_OIDEF_oth"]+1>
				</cfif>
			
			</cfif>
		<cfelseif primarytype eq 1>
		<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
				</cfif>
		<cfelse>
		
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_oth")>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] =  iostruct["#PrimaryType#_#catid#_oth"]+1>
		</cfif>
		
		</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="incasset">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#DamageSource#_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_#DamageSource#_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#DamageSource#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#DamageSource#_#IncAssignedTo#"]+1>
		</cfif>
		</cfif>
	<cfelse>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#DamageSource#_oth")>
			<cfset iostruct["#PrimaryType#_#DamageSource#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#DamageSource#_oth"] =  iostruct["#PrimaryType#_#DamageSource#_oth"]+1>
		</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="getenv">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#EnvIncCat#_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#EnvIncCat#_#IncAssignedTo#"]+1>
		</cfif>
		</cfif>
	<cfelse>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#EnvIncCat#_oth")>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_oth"] =  iostruct["#PrimaryType#_#EnvIncCat#_oth"]+1>
		</cfif>
		</cfif>
	</cfif>
</cfloop>
<cfloop query="getsec">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#SecIncCat#_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#SecIncCat#_#IncAssignedTo#"]+1>
		</cfif>
		</cfif>
	<cfelse>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#SecIncCat#_oth")>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_oth"] =  iostruct["#PrimaryType#_#SecIncCat#_oth"]+1>
		</cfif>
		</cfif>
	</cfif>
</cfloop>

	<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Incidents by Type</td>
</tr>
</table>
<!--- <table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Incidents</td>
	</tr>
	<tr>
		<td bgcolor="ffffff"> --->
<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%" bgcolor="5f2468" >
	<tr>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Type</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>&nbsp;</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Amec Foster Wheeler</strong></td>
		<!--- <td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Another Amec-FW Co (working as Contractor)</strong></td> --->
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Sub-Contractor</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Managed Contractor</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Joint Venture</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Other (Includes: Member of the Public, Visitor, and Other)</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" align="center" ><strong>Count</strong></td>
		
	</tr>
	<cfset col1tot = 0><cfset col2tot = 0><cfset col3tot = 0><cfset col4tot= 0><cfset col5tot = 0><cfset col6tot = 0><cfset col7tot = 0>
	<cfoutput query="getsortedincs" group="inctypeid">
	<cfset col1ctr = 0><cfset col2ctr = 0><cfset col3ctr = 0><cfset col4ctr = 0><cfset col5ctr = 0><cfset col6ctr = 0><cfset col7ctr = 0>
	<cfset gctr = 0>
	<cfset needrwcnt = 0>
	<cfoutput>
	<cfset needrwcnt = needrwcnt+1>
	</cfoutput>
	
	<cfoutput>
	<cfset gctr = gctr + 1>
	<cfset rctr = 0>
	
	<tr>
		<cfif gctr eq 1><td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" <cfif inctypeid eq 5>rowspan="#needrwcnt+3#"<cfelse>rowspan="#needrwcnt+2#"</cfif>  valign="top"><cfif gctr eq 1>#IncType#</cfif></td></cfif>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">#incsubtype#</td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_1")>#iostruct["#inctypeid#_#incsubtypeid#_1"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_#incsubtypeid#_1"]><cfelse>0</cfif></td>
		<!--- <td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_2")>#iostruct["#inctypeid#_#incsubtypeid#_2"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_#incsubtypeid#_2"]><cfelse>0</cfif></td> --->
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_3")>#iostruct["#inctypeid#_#incsubtypeid#_3"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_#incsubtypeid#_3"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_4")>#iostruct["#inctypeid#_#incsubtypeid#_4"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_#incsubtypeid#_4"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_11")>#iostruct["#inctypeid#_#incsubtypeid#_11"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_#incsubtypeid#_11"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_oth")>#iostruct["#inctypeid#_#incsubtypeid#_oth"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfelse>0</cfif></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#rctr#<cfset col7ctr = col7ctr+rctr></td>
	</tr>
	
	</cfoutput>
	<cfif inctypeid eq 5>
	<cfset rctr = 0>
	<tr>
		
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Non-Discrete</td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_1")>#iostruct["#inctypeid#_OIDEF_1"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_OIDEF_1"]><cfelse>0</cfif></td>
		<!--- <td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_2")>#iostruct["#inctypeid#_OIDEF_2"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_OIDEF_2"]><cfelse>0</cfif></td> --->
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_3")>#iostruct["#inctypeid#_OIDEF_3"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_OIDEF_3"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_4")>#iostruct["#inctypeid#_OIDEF_4"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_OIDEF_4"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_11")>#iostruct["#inctypeid#_OIDEF_11"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_OIDEF_11"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_oth")>#iostruct["#inctypeid#_OIDEF_oth"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_OIDEF_oth"]><cfelse>0</cfif></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#rctr#<cfset col7ctr = col7ctr+rctr></td>
	</tr>
	
	</cfif>
	<cfif inctypeid neq 4>
	<cfset rctr = 0>
	<tr>
		
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Near Miss</td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_1")>#iostruct["#inctypeid#_NearMiss_1"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_NearMiss_1"]><cfelse>0</cfif></td>
		<!--- <td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_2")>#iostruct["#inctypeid#_NearMiss_2"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_NearMiss_2"]><cfelse>0</cfif></td> --->
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_3")>#iostruct["#inctypeid#_NearMiss_3"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_NearMiss_3"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_4")>#iostruct["#inctypeid#_NearMiss_4"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_NearMiss_4"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_11")>#iostruct["#inctypeid#_NearMiss_11"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_NearMiss_11"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_oth")>#iostruct["#inctypeid#_NearMiss_oth"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_NearMiss_oth"]><cfelse>0</cfif></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#rctr#<cfset col7ctr = col7ctr+rctr></td>
	</tr>
	</cfif>
	<tr>
		
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Sub-Total</td>
		<td align="center" class="bodyTextGrey"  bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col1ctr#<cfset col1tot = col1tot + col1ctr></td>
		<!--- <td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col2ctr#<cfset col2tot = col2tot + col2ctr></td> --->
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col3ctr#<cfset col3tot = col3tot + col3ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col4ctr#<cfset col4tot = col4tot + col4ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col5ctr#<cfset col5tot = col5tot + col5ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col6ctr#<cfset col6tot = col6tot + col6ctr></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#col7ctr#<cfset col7tot = col7tot + col7ctr></td>
	</tr>
	<tr>
		<td colspan="8" class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
	</tr>
	</cfoutput>
	<cfoutput>
	<tr>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Total</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col1tot#</strong></td>
		<!--- <td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col2tot#</strong></td> --->
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col3tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col4tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col5tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col6tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><strong>#col7tot#</strong></td>
	</tr>
	</cfoutput>
</table>
	</cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_incbytypepdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">