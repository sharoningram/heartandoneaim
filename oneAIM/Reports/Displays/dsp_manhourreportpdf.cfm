
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_manhourreportpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "ManHourReport_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">

<cfoutput>
<cfparam name="dosearch" default="">

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Man-Hours Total Report</td>
</tr>

</table>
</cfoutput>

<table cellpadding="2" cellspacing="1" bgcolor="000000" width="100%" border="0">
	<tr>
		
		<cfoutput><td class="purplebg" align="center" width="25%" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">#request.bulabellong#</strong></td></cfoutput>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">AFW Employees</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468" ><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Managed Contractor</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468" ><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Subcontractor</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468" ><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Joint Venture</strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Total</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfoutput query="getOU" group="buname">
	<cfset rowtot = 0>
	<cfif structkeyexists(manhrstruct,qrybuid)><cfset jdehrs = manhrstruct[qrybuid]><cfelse><cfset jdehrs = 0></cfif>
	<cfif structkeyexists(subhrsstruct,qrybuid)><cfset subhours = subhrsstruct[qrybuid]><cfelse><cfset subhours = 0></cfif>
	<cfif structkeyexists(jvhrsstruct,qrybuid)><cfset jvhours = jvhrsstruct[qrybuid]><cfelse><cfset jvhours = 0></cfif>
	<cfif structkeyexists(mdghrsstruct,qrybuid)><cfset mdghours = mdghrsstruct[qrybuid]><cfelse><cfset mdghours = 0></cfif>
	<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	<cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#buname#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jdehrs)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mdghours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
	<cfoutput>
	<cfset rowtot = 0>
	<cfif structkeyexists(manhrstruct,id)><cfset jdehrs = manhrstruct[id]><cfelse><cfset jdehrs = 0></cfif>
	<cfif structkeyexists(subhrsstruct,id)><cfset subhours = subhrsstruct[id]><cfelse><cfset subhours = 0></cfif>
	<cfif structkeyexists(jvhrsstruct,id)><cfset jvhours = jvhrsstruct[id]><cfelse><cfset jvhours = 0></cfif>
	<cfif structkeyexists(mdghrsstruct,id)><cfset mdghours = mdghrsstruct[id]><cfelse><cfset mdghours = 0></cfif>
	<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	<cfif listfind(openbu,qrybuid) gt 0>
	<tr <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>  id="ourow#id#">
		<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;#name#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jdehrs)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mdghours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
	</cfif>
	<cfloop query="getgroups">
	<cfset rowtot = 0>
	<cfif listfind(openou,ouid) gt 0>
		<cfif getgroups.ouid eq getOU.ID>
		<cfif structkeyexists(manhrstructgrp,getgroups.group_number)><cfset jdehrs = manhrstructgrp[getgroups.group_number]><cfelse><cfset jdehrs = 0></cfif>
		<cfif structkeyexists(subhrsstructgrp,getgroups.group_number)><cfset subhours = subhrsstructgrp[getgroups.group_number]><cfelse><cfset subhours = 0></cfif>
		<cfif structkeyexists(jvhrsstructgrp,getgroups.group_number)><cfset jvhours = jvhrsstructgrp[getgroups.group_number]><cfelse><cfset jvhours = 0></cfif>
		<cfif structkeyexists(mdghrsstructgrp,getgroups.group_number)><cfset mdghours = mdghrsstructgrp[getgroups.group_number]><cfelse><cfset mdghours = 0></cfif>
		<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
		<tr <cfif ctr mod 2 is 0>class="formlable"  style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;<cfelse>bgcolor="ffffff"</cfif> id="grouprow#group_number#">
		<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;&nbsp;&nbsp;#getgroups.group_name#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jdehrs)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mdghours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
	
	
	<cfloop query="getgroupsites">
		<cfset rowtot = 0>
		<cfif listfind(opengnum,GroupNumber) gt 0>
			<cfif getgroupsites.GroupNumber eq getgroups.Group_Number>
	
			<cfif structkeyexists(manhrstructloc,getgroupsites.GroupLocID)><cfset jdehrs = manhrstructloc[getgroupsites.GroupLocID]><cfelse><cfset jdehrs = 0></cfif>
		<cfif structkeyexists(subhrsstructloc,getgroupsites.GroupLocID)><cfset subhours = subhrsstructloc[getgroupsites.GroupLocID]><cfelse><cfset subhours = 0></cfif>
		<cfif structkeyexists(jvhrsstructloc,getgroupsites.GroupLocID)><cfset jvhours = jvhrsstructloc[getgroupsites.GroupLocID]><cfelse><cfset jvhours = 0></cfif>
		<cfif structkeyexists(mdghrsstructloc,getgroupsites.GroupLocID)><cfset mdghours = mdghrsstructloc[getgroupsites.GroupLocID]><cfelse><cfset mdghours = 0></cfif>
		<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	<tr <cfif ctr mod 2 is 0>class="formlable"  style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;<cfelse>bgcolor="ffffff"</cfif> id="siterow#getgroupsites.GroupLocID#">
		
		<td class="bodytext"  nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#getgroupsites.SiteName#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jdehrs)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mdghours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvhours)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
			
			</cfif>
			</cfif>
		
		
		</cfloop>
		
	
		</cfif>
	</cfif>
	</cfloop> 
	</cfoutput>
	</cfoutput>
	<cfset rowtot = 0>
	<cfif structkeyexists(manhrstruct,"Global")><cfset jdehrs = manhrstruct["Global"]><cfelse><cfset jdehrs = 0></cfif>
	<cfif structkeyexists(subhrsstruct,"Global")><cfset subhours = subhrsstruct["Global"]><cfelse><cfset subhours = 0></cfif>
	<cfif structkeyexists(jvhrsstruct,"Global")><cfset jvhours = jvhrsstruct["Global"]><cfelse><cfset jvhours = 0></cfif>
	<cfif structkeyexists(mdghrsstruct,"Global")><cfset mdghours = mdghrsstruct["Global"]><cfelse><cfset mdghours = 0></cfif>
	<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	<cfoutput>
	<tr class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;">
		<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>Amec Foster Wheeler Global</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(jdehrs)#</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(mdghours)#</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(subhours)#</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(jvhours)#</strong></td>
		<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;" ><strong>#numberformat(rowtot)#</strong></td>
	</tr>
	</cfoutput>
</table>
<!--- <script type="text/javascript">

<cfoutput query="getOU" group="qrybuid">

<cfoutput>
<cfif listfind(openbu,qrybuid) eq 0>
document.getElementById("ourow#id#").style.display='none';
</cfif>

</cfoutput>
</cfoutput>
<cfoutput query="getgroups">
<cfif listfind(openou,ouid) eq 0>
document.getElementById("grouprow#group_number#").style.display='none';
</cfif>
</cfoutput>

</script> --->
</cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_manhourreportpdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">