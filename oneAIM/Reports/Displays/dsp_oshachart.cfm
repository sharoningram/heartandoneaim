<cfparam name="dosearch" default="">
<cfif fusebox.fuseaction neq "printoshachart">
<cfoutput>

<form action="#self#?fuseaction=#attributes.xfa.printoshachart#" name="printfrm" method="post" target="_blank">
	<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">	
<input type="hidden" name="frmbodypart" value="#frmbodypart#">	
	</form>
	</cfoutput>
</cfif>

<cfloop query="getoshaclassrpt">
	<cfloop query="getinjurytypes">
		<cfset "ctr_#getoshaclassrpt.catid#_#InjTypeID#" = 0>
	</cfloop>
</cfloop>

<cfoutput query="getoshachart" group="OSHAclass">
	<cfoutput>
		<cfloop list="#injuryType#" index="i">
			<cfset "ctr_#oshaclass#_#i#" = evaluate("ctr_#oshaclass#_#i#")+1>
			
		</cfloop>
	</cfoutput>
	
</cfoutput>


<cfset chartxml = "<chart caption='Injury Type by OSHA Rating' subcaption='' xaxisname='' yaxisname='' showsum='0' numberprefix='' showvalues='0'  legendPosition='right' drawCustomLegendIcon='1' legendIconSides='5' palettecolors='92bae3,a47734,f1c283,3381cc,daa4c8,e3d63c,5d079c,c82759,447711,da7062,27758b,fece60,a22d62,dd581f,00906a,efa252,3a6598' useRoundEdges='0' exportenabled='1' exportAtClientSide='1'><categories>">
<cfloop query="getoshaclassrpt">
<cfset chartxml = chartxml & "<category label='#Category#' />">
</cfloop>



<cfset chartxml = chartxml & "</categories>">

<cfloop query="getinjurytypes">
<cfset chartxml = chartxml & "<dataset seriesname='#InjuryType#'>">
<cfloop query="getoshaclassrpt">
	<cfset chartxml = chartxml & "<set value='#evaluate("ctr_#getoshaclassrpt.catid#_#getinjurytypes.InjTypeID#")#' />">
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
</cfloop>
<cfset chartxml = chartxml & "</chart>">
<cfoutput>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2">Injury Type by OSHA Rating Chart</td>
</tr><cfoutput>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printoshachart"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
</tr></cfoutput>
</table>
<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Injury Type by OSHA Rating Chart</td>
	</tr>
	<tr>
		<td bgcolor="ffffff" align="center">
<div id="chartContainer"></div>
		</td>
	</tr>
</table>
<script type="text/javascript">

FusionCharts.ready(function () {
    var stackedChart = new FusionCharts({
        "type": "stackedcolumn2d",
        "renderAt": "chartContainer",
        "width": "750",
        "height": "550",
        "dataFormat": "xml",
        "dataSource":  "#chartxml#"
		
    }
	
	);

    stackedChart.render();


});
</script>
</cfoutput>
