<script type="text/javascript">
function submitsfrm(){
// alert(document.userlookupfrm.searchitem.value);
var oktosubmit = "yes"

if(document.savesearchfrm.searchname.value==""){
	var oktosubmit = "no";
	 document.savesearchfrm.searchname.style.border = "2px solid F00000";
	
}
else{
document.savesearchfrm.searchname.style.border = "1px solid 5f2468";
}



if (oktosubmit=="no"){
	document.getElementById("reqflds").style.display='';
	return false;
}  
// alert(oktosubmit);
}

function submitsrtfrm(srtcol){
document.sortresfrm.sortordby.value=srtcol;
document.sortresfrm.submit();

}

</script>	
<cfparam name="newsave" default="1">
<cfparam name="frompg" default="srchfrm">
<cfform action="#self#?fuseaction=reports.prjsearch" method="post" name="backtosearch">
				<cfoutput>
				<!--- <input type="hidden" name="frompg" value="#frompg#"> --->
				<input type="hidden" name="fromedit" value="yes">		
				<input type="hidden" name="dosearch" value="redir">
				<input type="hidden" name="bu" value="#bu#">
				<input type="hidden" name="projectoffice" value="#projectoffice#">
				<input type="hidden" name="ou" value="#ou#">
				<input type="hidden" name="site" value="#site#">
				<input type="hidden" name="businessstream" value="#businessstream#">
				<input type="hidden" name="locdetail" value="#locdetail#">
				<input type="hidden" name="frmclient" value="#frmclient#">
				<input type="hidden" name="frmcountryid" value="#frmcountryid#">
				<input type="hidden" name="mhetype" value="#mhetype#">
				<input type="hidden" name="groupactive" value="#groupactive#">
				<input type="hidden" name="siteactive" value="#siteactive#">
				<input type="hidden" name="mhentrytype" value="#mhentrytype#">
				<input type="hidden" name="prjnumber" value="#prjnumber#">
				</cfoutput>
</cfform>
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td colspan="2" class="bodyTextWhite">Criteria</td>
	</tr>

		<tr>
			<td align="center" bgcolor="ffffff" colspan="2">
			<cfform action="#self#?fuseaction=#attributes.xfa.doprjsearch#" method="post" name="savesearchfrm" enctype="multipart/form-data" onsubmit="return submitsfrm();">
				<cfoutput>
				<input type="hidden" name="frompg" value="#frompg#">
				<input type="hidden" name="savefrm" value="yes">
				<input type="hidden" name="bu" value="#bu#">
				<input type="hidden" name="projectoffice" value="#projectoffice#">
				<input type="hidden" name="ou" value="#ou#">
				<input type="hidden" name="site" value="#site#">
				<input type="hidden" name="businessstream" value="#businessstream#">
				<input type="hidden" name="locdetail" value="#locdetail#">
				<input type="hidden" name="frmclient" value="#frmclient#">
				<input type="hidden" name="frmcountryid" value="#frmcountryid#">
				<input type="hidden" name="mhetype" value="#mhetype#">
				<input type="hidden" name="groupactive" value="#groupactive#">
				<input type="hidden" name="siteactive" value="#siteactive#">
				<input type="hidden" name="mhentrytype" value="#mhentrytype#">
				<input type="hidden" name="prjnumber" value="#prjnumber#">
				
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<tr id="reqflds">
							<td colspan="4" class="redText" align="center">Please fill in all required fields</td>
						</tr>
						<tr>
							<td align="center" colspan="8" class="formlable"><strong>Location Detail Parameters</strong></td>
						</tr>
						<tr>
							<td class="formlable" width="12%"><strong>#request.bulabellong#</strong></td>
							<td class="bodyTextGrey" width="12%"><cfif listfind(bu,"all") gt 0>All<br></cfif>
								<cfloop query="getbus">
										<cfif listfind(bu,id) gt 0>#Name#<br></cfif>
										</cfloop></td>
							<td class="formlable"  width="12%"><strong>#request.oulabellong#</strong></td>
							<td class="bodyTextGrey" width="12%"><cfif listfind(ou,"all") gt 0>All<br></cfif>
								<cfloop query="getOU">
										<cfif listfind(ou,id) gt 0>#Name#<br></cfif>
										</cfloop></td>
							<td class="formlable"  width="12%"><strong>Business Stream</strong></td>
							<td class="bodyTextGrey" width="12%"><cfif listfind(businessstream,"all") gt 0>All<br></cfif>
								<cfloop query="getBS">
										<cfif listfind(businessstream,id) gt 0>#Name#<br></cfif>
										</cfloop></td>
							<td class="formlable"   width="12%"><strong>Project/Office</strong></td>
							<td class="bodyTextGrey"  width="12%"><cfif listfind(projectoffice,"all") gt 0>All<br></cfif>
								<cfloop query="getgroups">
										<cfif listfind(projectoffice,group_number) gt 0>#group_name#<br></cfif>
										</cfloop></td>
						</tr>
						<tr>
							
							<td class="formlable" ><strong>Site/Office Name</strong></td>
							<td class="bodyTextGrey"><cfif listfind(site,"all") gt 0>All<br></cfif>
								<cfloop query="getsites">
										<cfif listfind(site,GroupLocID) gt 0>#sitename#<br></cfif>
										</cfloop></td>
							<td class="formlable" ><strong>Client</strong></td>
							<td class="bodyTextGrey"><cfif listfind(frmclient,"all") gt 0>All<br></cfif>
								<cfloop query="getclients">
										<cfif listfind(frmclient,ClientID) gt 0>#clientName#<br></cfif>
										</cfloop></td>
							<td class="formlable" ><strong>Location Details</strong></td>
							<td class="bodyTextGrey"><cfif listfind(locdetail,"all") gt 0>All<br></cfif>
								<cfloop query="getlocdetails">
										<cfif listfind(locdetail,LocationDetailID) gt 0>#LocationDetail#<br></cfif>
										</cfloop></td>
							<td class="formlable" ><strong>Country</strong></td>
							<td class="bodyTextGrey"><cfif listfind(frmcountryid,"all") gt 0>All<br></cfif>
								<cfloop query="getcountries">
										<cfif listfind(frmcountryid,CountryID) gt 0>#CountryName#<br></cfif>
										</cfloop></td>
						</tr>
						
						<tr>
						<td class="formlable" align="left" width="12%"><strong>Man Hour Entry:</strong></td>
						<td class="bodyTextGrey" width="12%" >#replace(mhetype,",",", ","all")#</td>  
						<td class="formlable" align="left" width="12%"><strong>Active Project/Office:</strong></td>
						<td class="bodyTextGrey" width="12%" >#groupactive#</td>  
						<td class="formlable" align="left" width="12%"><strong>Active Site/Office:</strong></td>
						<td class="bodyTextGrey" width="12%" >#siteactive#</td>  
						<td class="formlable" align="left" width="12%"><strong>Will man hours be recorded for this location?</strong></td>
						<td class="bodyTextGrey" width="12%" >#mhentrytype#</td>  
					</tr>
					<tr>
						<td class="formlable" align="left" width="12%"><strong>Project/Office Number:</strong></td>
						<td class="bodyTextGrey" width="12%">#replace(prjnumber,",",", ","all")#</td>
					
					</td>
						<!--- <cfif trim(searchname) eq ''> --->
						<tr><!--- onkeypress="this.style.border='1px solid 5f2468';" --->
						<td colspan="4"  class="formlable"><strong>Assign Name to Search Criteria:</strong><br><em>Enter a name ONLY if you wish to save this search on your "My Searches" page.</em></td>
						<td colspan="4" ><input type="text" name="searchname" value="#searchname#" class="selectgen" size="60" maxlength="75" ></td>
					</tr>
					
					<tr>
						<td colspan="8" align="center" class="bodyTextGrey">
						<input type="submit" class="selectGenBTN" value="Save Search Criteria">&nbsp;&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Edit Criteria" onclick="document.backtosearch.submit();"></td>
					</tr>
					<!--- <cfelse>
					<tr>
						<td  class="formlable"><strong>Searched saved as:</strong></td>
						<td colspan="3" class="bodyTextGrey">#searchname#<cfif newsave gt 1> (#newsave#)</cfif></td>
					</tr>
					</cfif> --->
				</table>
				</cfoutput>
				</cfform>
			</td>
		</tr>
	<tr >
	<td class="bodyTextWhite">Search Results</td>
	<td align="right"><a href="javascript:void(0);" onclick="document.exportprjfrm.submit();"><img src="images/excel.png" border="0"  style="padding-right:8px;padding-top:0px;"></a></td>
	</tr>

		<tr>
			<td align="center" bgcolor="ffffff" colspan="2">
				
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('prjnum');">Number</a></strong></td>
							<cfoutput>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('bu');">#request.bulabellong#</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('ou');">#request.oulabellong#</a></strong></td>
							</cfoutput>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('bs');">Business Stream</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('cl');">Client</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('prj');">Project/Office</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('prjstat');">Active Project/Office</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('so');">Site/Office Name</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('sostat');">Active Site/Office</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('ld');">Location Details</a></strong></td>
							<td class="formlable" ><strong><a href="javascript:void(0);" onclick="submitsrtfrm('cnty');">Country</a></strong></td>
						</tr>
						<cfoutput query="getprjsearchresults" >
						<tr <cfif currentrow mod 2 is 0>class="formlable"</cfif>>
							<td class="bodyTextGrey"><cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0><a href="index.cfm?fuseaction=#attributes.xfa.updategroup#&groupnumber=#group_number#">P#trackingdate##numberformat(tracking_num,"0000")#</a><cfelse>P#trackingdate##numberformat(tracking_num,"0000")#</cfif></td>
							<td class="bodyTextGrey">#buname#</td>
							<td class="bodyTextGrey">#ouname#</td>
							<td class="bodyTextGrey">#bsname#</td>
							<td class="bodyTextGrey">#clientname#</td>
							<td class="bodyTextGrey">#group_name#</td>
							<td class="bodyTextGrey"><cfif active_group eq 0>No<cfelse>Yes</cfif></td>
							<td class="bodyTextGrey">#sitename#</td>
							<td class="bodyTextGrey"><cfif isactive eq 0>No<cfelse>Yes</cfif></td>
							<td class="bodyTextGrey">#LocationDetail#</td>
							<td class="bodyTextGrey">#CountryName#</td>
						</tr>
						</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	
	<cfform action="#self#?fuseaction=#attributes.xfa.exportprjsearch#" method="post" name="exportprjfrm" target="_blank">
				<cfoutput>
				<input type="hidden" name="frompg" value="#frompg#">
					
				<input type="hidden" name="sortordby" value="#sortordby#">
				<input type="hidden" name="bu" value="#bu#">
				<input type="hidden" name="projectoffice" value="#projectoffice#">
				<input type="hidden" name="ou" value="#ou#">
				<input type="hidden" name="site" value="#site#">
				<input type="hidden" name="businessstream" value="#businessstream#">
				<input type="hidden" name="locdetail" value="#locdetail#">
				<input type="hidden" name="frmclient" value="#frmclient#">
				<input type="hidden" name="frmcountryid" value="#frmcountryid#">
				<input type="hidden" name="mhetype" value="#mhetype#">
				<input type="hidden" name="groupactive" value="#groupactive#">
				<input type="hidden" name="siteactive" value="#siteactive#">
				<input type="hidden" name="mhentrytype" value="#mhentrytype#">
				<input type="hidden" name="prjnumber" value="#prjnumber#">
				</cfoutput>
		</cfform>
	<cfform action="#self#?fuseaction=#attributes.xfa.doprjsearch#" method="post" name="sortresfrm">
				<cfoutput>
				<input type="hidden" name="frompg" value="#frompg#">
					
				<input type="hidden" name="sortordby" value="">
				<input type="hidden" name="bu" value="#bu#">
				<input type="hidden" name="projectoffice" value="#projectoffice#">
				<input type="hidden" name="ou" value="#ou#">
				<input type="hidden" name="site" value="#site#">
				<input type="hidden" name="businessstream" value="#businessstream#">
				<input type="hidden" name="locdetail" value="#locdetail#">
				<input type="hidden" name="frmclient" value="#frmclient#">
				<input type="hidden" name="frmcountryid" value="#frmcountryid#">
				<input type="hidden" name="mhetype" value="#mhetype#">
				<input type="hidden" name="groupactive" value="#groupactive#">
				<input type="hidden" name="siteactive" value="#siteactive#">
				<input type="hidden" name="mhentrytype" value="#mhentrytype#">
				<input type="hidden" name="prjnumber" value="#prjnumber#">
				</cfoutput>
		</cfform>
	<script type="text/javascript">
	document.getElementById("reqflds").style.display='none';
</script>