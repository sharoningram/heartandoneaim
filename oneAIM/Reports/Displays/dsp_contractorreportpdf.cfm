
<cfparam name="openou" default="0">
<cfparam name="openbu" default="0">
<cfparam name="dosearch" default="">
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_contractorreportpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "ContractorReport_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">
<cfoutput>
<style>
.purplebg {
background: ##5f2468;

}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	color: ##000000;
	font-style: normal;
}
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: ##FFFFFF;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10pt;
	color: ##5f5f5f;
	font-style: normal;
	}
.formlablelt{
background-color:##fdf8ff;
font-family: Segoe UI;
	font-size: 10pt;
	color: ##5f5f5f;
	font-style: normal;
}
.formlable{
background-color:##f7f1f9;
font-family: Segoe UI;
	font-size: 10pt;
	color: ##5f5f5f;
	font-style: normal;
}
</style>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="95%">
<tr>
	<td class="bodyTexthd" colspan="2">Contractor Performance Report</td>
</tr>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#<cfelse>For #incyear#</cfif></td>
	
</tr>
</table>

</cfoutput>
<table cellpadding="3" cellspacing="0"  width="100%" border="0">
<cfoutput query="getcontractorreplist" group="cotype">
<tr>
		
		<td class="purplebg" colspan="11"><strong class="bodytextwhite"><cfif cotype eq "jv">Joint Venture<cfelseif cotype eq "sub">Subcontractor<cfelseif cotype eq "mgd">Managed Contractor</cfif> Performance</strong></td>
		
		
	</tr>
	<cfoutput group="ContractorNameID">
	<cfset contothrs = 0>
	<cfset contotFAT = 0>
	<cfset contotLTI = 0>
	<cfset contotRWC = 0>
	<cfset contotMTC = 0>
	<cfset contotOID = 0>
	<cfset contotFAid = 0>
	<tr>
		
		<td class="formlable" width="15%"><strong class="formlable">#ContractorName#</strong></td>
		<td class="formlable" align="center"><strong class="formlable">Hours</strong></td>
		<td class="formlable" align="center"><strong class="formlable">Fatality</strong></td>
		<td class="formlable" align="center"><strong class="formlable">LTI</strong></td>
		<td class="formlable" align="center"><strong class="formlable">RWC</strong></td>
		<td class="formlable" align="center"><strong class="formlable">MTC</strong></td>
		<td class="formlable" align="center"><strong class="formlable">OI-D</strong></td>
		<td class="formlable" align="center"><strong class="formlable">LTIR</strong></td>
		<td class="formlable" align="center"><strong class="formlable">TRIR</strong></td>
		<td class="formlable" align="center"><strong class="formlable">First Aid</strong></td>
		<td class="formlable" align="center"><strong class="formlable">AIR</strong></td>
		
	</tr>
	<cfoutput>
	<cfset thisrwhrs = 0>
	<cfset thisrwlti = 0>
	<cfset thisrwtotrec = 0>
	<cfset thisrwallinc = 0>
		<tr>
			<td class="bodyTextGrey">&nbsp;&nbsp;&nbsp;#group_name#</td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(conhrsstruct,contractorid)>#numberformat(conhrsstruct[contractorid])#<cfset contothrs = contothrs+conhrsstruct[contractorid]><cfset thisrwhrs = conhrsstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(conFATstruct,contractorid)>#numberformat(conFATstruct[contractorid])#<cfset contotFAT = contotFAT+conFATstruct[contractorid]><cfset thisrwtotrec = thisrwtotrec+conFATstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(LTIstruct,contractorid)>#numberformat(LTIstruct[contractorid])#<cfset contotLTI = contotLTI+LTIstruct[contractorid]><cfset thisrwlti = LTIstruct[contractorid]><cfset thisrwtotrec = thisrwtotrec+LTIstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(RWCstruct,contractorid)>#numberformat(RWCstruct[contractorid])#<cfset contotRWC = contotRWC+RWCstruct[contractorid]><cfset thisrwtotrec = thisrwtotrec+RWCstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(MTCstruct,contractorid)>#numberformat(MTCstruct[contractorid])#<cfset contotMTC = contotMTC+MTCstruct[contractorid]><cfset thisrwtotrec = thisrwtotrec+MTCstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(OIDstruct,contractorid)>#numberformat(OIDstruct[contractorid])#<cfset contotOID = contotOID+OIDstruct[contractorid]><cfset thisrwtotrec = thisrwtotrec+OIDstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif thisrwhrs gt 0 and thisrwlti gt 0>#numberformat((thisrwlti*200000)/thisrwhrs,"0.00")#<cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif thisrwhrs gt 0 and thisrwtotrec gt 0>#numberformat((thisrwtotrec*200000)/thisrwhrs,"0.00")#<cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif structkeyexists(FAstruct,contractorid)>#numberformat(FAstruct[contractorid])#<cfset contotFAid = contotFAid+FAstruct[contractorid]><cfset thisrwallinc = thisrwtotrec+FAstruct[contractorid]><cfelse>0</cfif></td>
			<td class="bodyTextGrey" align="center"><cfif thisrwhrs gt 0 and thisrwallinc gt 0>#numberformat((thisrwallinc*200000)/thisrwhrs,"0.00")#<cfelse>0</cfif></td>
		</tr>
		</cfoutput>
		<cfset contotrec = contotFAT+contotLTI+contotRWC+contotMTC+contotOID>
		<cfset contotar = contotrec+contotFAid>
	<tr>
			<td class="formlablelt"><strong>#ContractorName# Total</strong></td>
			<td class="formlablelt" align="center">#numberformat(contothrs)#</td>
			<td class="formlablelt" align="center">#numberformat(contotFAT)#</td>
			<td class="formlablelt" align="center">#numberformat(contotLTI)#</td>
			<td class="formlablelt" align="center">#numberformat(contotRWC)#</td>
			<td class="formlablelt" align="center">#numberformat(contotMTC)#</td>
			<td class="formlablelt" align="center">#numberformat(contotOID)#</td>
			<td class="formlablelt" align="center"><cfif contothrs gt 0 and contotLTI gt 0>#numberformat((contotLTI*200000)/contothrs,"0.00")#<cfelse>0</cfif></td>
			<td class="formlablelt" align="center"><cfif contothrs gt 0 and contotrec gt 0>#numberformat((contotrec*200000)/contothrs,"0.00")#<cfelse>0</cfif></td>
			<td class="formlablelt" align="center">#numberformat(contotFAid)#</td>
			<td class="formlablelt" align="center"><cfif contothrs gt 0 and contotar gt 0>#numberformat((contotar*200000)/contothrs,"0.00")#<cfelse>0</cfif></td>
		</tr>
	<tr>
		<td colspan="11"><br></td>
	</tr> 
</cfoutput>
</cfoutput>
</table>
	</cfdocument>
<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">