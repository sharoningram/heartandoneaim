
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_configdashboardpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "ConfigDashboard_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25"> 
<cfoutput>
<cfparam name="dosearch" default="">
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Configurable Dashboard Report</td>
</tr>

</table>



<table cellpadding="3" cellspacing="0"  width="100%" border="0">
	<tr>
		
		<td class="purplebg" width="15%" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">BU</strong></td>
		<td class="purplebg"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Incident Assigned To</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Hours</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Fatality</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">LTI</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">RWC</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">MTC</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">LTIR</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">TRIR</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">First Aid</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">AIR</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Occ. Illness<br>Discrete</strong></td>
	</tr>
	<!--- <tr>
		<td colspan="14" class="formlable" ><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2468;" width="100%"></td>
	</tr> --->
	<cfset amfwctr = 0>
	<cfset subctr = 0>
	<cfset jvctr = 0>
	<cfset mgdctr = 0>
	<cfset globaltot = 0>
	<cfset buctr = 0>
	
	<cfset ltitotctrafw = 0>
	<cfset fattotctrafw = 0>
	<cfset rwctotctrafw = 0>
	<cfset mtctotctrafw = 0>
	<cfset fatotctrafw = 0>
	<cfset oitotctrafw = 0>
	
	<cfset ltitotctrsub = 0>
	<cfset fattotctrsub = 0>
	<cfset rwctotctrsub = 0>
	<cfset mtctotctrsub = 0>
	<cfset fatotctrsub = 0>
	<cfset oitotctrsub = 0>
	
	<cfset ltitotctrjv = 0>
	<cfset fattotctrjv = 0>
	<cfset rwctotctrjv = 0>
	<cfset mtctotctrjv = 0>
	<cfset fatotctrjv = 0>
	<cfset oitotctrjv = 0>
	
	<cfset ltitotctrmgd = 0>
	<cfset fattotctrmgd = 0>
	<cfset rwctotctrmgd = 0>
	<cfset mtctotctrmgd = 0>
	<cfset fatotctrmgd = 0>
	<cfset oitotctrmgd = 0>
	
	<cfset ltitotctrglobal = 0>
	<cfset fattotctrglobal= 0>
	<cfset rwctotctrglobal = 0>
	<cfset mtctotctrglobal = 0>
	<cfset fatotctrglobal = 0>
	<cfset oitotctrglobal = 0>
	
	
	
	
	<cfloop query="getBUs">
	<cfset buhrsdsp = 0>
		<cfset bufattot = 0>
		<cfset bultitot = 0>
		<cfset burwctot = 0>
		<cfset bumtctot = 0>
		<cfset bufatot = 0>
		<cfset buoitot = 0>
		
	<cfset buctr = buctr+1>
		<cfset ctr = 0>
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset ctr = ctr+1><!--- <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> --->
	<tr >
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif ctr eq 1>#getBUs.Name#</cfif></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#IncAssignedTo#</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getBUs.ID)>#numberformat(manhrstruct[getBUs.id])#<cfset buhrsdsp = buhrsdsp+manhrstruct[getBUs.id]><cfset amfwctr = amfwctr+manhrstruct[getBUs.id]><cfset rowhrs = rowhrs+manhrstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getBUs.ID)>#numberformat(subhrsstruct[getBUs.id])#<cfset buhrsdsp = buhrsdsp+subhrsstruct[getBUs.id]><cfset subctr = subctr+subhrsstruct[getBUs.id]><cfset rowhrs = rowhrs+subhrsstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getBUs.ID)>#numberformat(mdgconstruct[getBUs.id])#<cfset buhrsdsp = buhrsdsp+mdgconstruct[getBUs.id]><cfset mgdctr = mgdctr+mdgconstruct[getBUs.id]><cfset rowhrs = rowhrs+mdgconstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getBUs.ID)>#numberformat(jvhrsstruct[getBUs.id])#<cfset buhrsdsp = buhrsdsp+jvhrsstruct[getBUs.id]><cfset jvctr = jvctr+jvhrsstruct[getBUs.id]><cfset rowhrs = rowhrs+jvhrsstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">
		
		<cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")>#FATstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset bufattot = bufattot + FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+FATstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrmgd = fattotctrmgd + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")>#LTIstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset bultitot = bultitot + LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowlti = rowlti+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")>#RWCstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset burwctot = burwctot + RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+RWCstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrmgd = rwctotctrmgd + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")>#MTCstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset bumtctot = bumtctot + MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+MTCstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrmgd = mtctotctrmgd + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")>#FAstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset bufatot = bufatot + FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowair = rowair+FAstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
	<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">
		<cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")>#OIstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset buoitot = buoitot + OIstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrafw = oitotctrafw + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrsub = oitotctrsub + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrmgd = oitotctrmgd + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrjv = oitotctrjv + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		</td>
	</tr>
		</cfif>
		
		</cfloop>
		
		
		<!--- 	added 3/1 --->
	<tr>
		<td class="bodyTextGrey"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"></td>
		<td class="formlablelt" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><strong>Total</strong></td>
		<td class="bodyTextGrey" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><strong>#numberformat(buhrsdsp)#</strong></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(bufattot)#</td>
		<td class="bodyTextGrey" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(bultitot)#</td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(burwctot)#</td>
		<td class="bodyTextGrey" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(bumtctot)#</td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif buhrsdsp gt 0 and bultitot gt 0>#numberformat((bultitot*200000)/buhrsdsp,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfset butrir = bufattot+bultitot+burwctot+bumtctot><cfif buhrsdsp gt 0 and butrir gt 0>#numberformat((butrir*200000)/buhrsdsp,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(bufatot)#</td>
		<td class="bodyTextGrey" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfset buair = bufatot+butrir><cfif buhrsdsp gt 0 and buair gt 0>#numberformat((buair*200000)/buhrsdsp,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(buoitot)#</td>
	</tr>
		
		<cfloop query="getou">
		
		
		<cfset ouhrsdsp = 0>
		<cfset oufattot = 0>
		<cfset oultitot = 0>
		<cfset ourwctot = 0>
		<cfset oumtctot = 0>
		<cfset oufatot = 0>
		<cfset ouoitot = 0>
			
		<cfif getou.qrybuid eq getbus.id and listfind(openbu,getou.qrybuid) gt 0>
		<cfset ctr = 0>
		
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset ctr = ctr+1><!--- <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> --->
	<tr >
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif ctr eq 1>#getou.Name#</cfif></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#IncAssignedTo#</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getou.id)>#numberformat(manhrstruct[getou.id])#<cfset ouhrsdsp = ouhrsdsp+manhrstruct[getou.id]><cfset rowhrs = rowhrs+manhrstruct[getou.id]><cfelse>0</cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getou.id)>#numberformat(subhrsstruct[getou.id])#<cfset ouhrsdsp = ouhrsdsp+subhrsstruct[getou.id]><cfset rowhrs = rowhrs+subhrsstruct[getou.id]><cfelse>0</cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getou.id)>#numberformat(mdgconstruct[getou.id])#<cfset ouhrsdsp = ouhrsdsp+mdgconstruct[getou.id]><cfset rowhrs = rowhrs+mdgconstruct[getou.id]><cfelse>0</cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getou.id)>#numberformat(jvhrsstruct[getou.id])#<cfset ouhrsdsp = ouhrsdsp+jvhrsstruct[getou.id]><cfset rowhrs = rowhrs+jvhrsstruct[getou.id]><cfelse>0</cfif></cfcase>
		
		</cfswitch> 
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">
		
		<cfif structkeyexists(FATstruct,"#getou.id#_#IncAssignedID#")>#FATstruct["#getou.id#_#IncAssignedID#"]#<cfset oufattot = oufattot + FATstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+FATstruct["#getou.id#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getou.id#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getou.id#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getou.id#_#IncAssignedID#")><cfset fattotctrmdg = fattotctrmdg + FATstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getou.id#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(LTIstruct,"#getou.id#_#IncAssignedID#")>#LTIstruct["#getou.id#_#IncAssignedID#"]#<cfset oultitot = oultitot + LTIstruct["#getou.ID#_#IncAssignedID#"]><cfset rowlti = rowlti+LTIstruct["#getou.id#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstruct["#getou.id#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getou.id#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getou.id#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getou.id#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getou.id#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>  --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif structkeyexists(RWCstruct,"#getou.id#_#IncAssignedID#")>#RWCstruct["#getou.id#_#IncAssignedID#"]#<cfset ourwctot = ourwctot + RWCstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+RWCstruct["#getou.id#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getou.id#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getou.id#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getou.id#_#IncAssignedID#")><cfset rwctotctrmdg = rwctotctrmdg + RWCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getou.id#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>  --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(MTCstruct,"#getou.id#_#IncAssignedID#")>#MTCstruct["#getou.id#_#IncAssignedID#"]#<cfset oumtctot = oumtctot + MTCstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+MTCstruct["#getou.id#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getou.id#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getou.id#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getou.id#_#IncAssignedID#")><cfset mtctotctrmdg = mtctotctrmdg + MTCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getou.id#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>  --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif structkeyexists(FAstruct,"#getou.id#_#IncAssignedID#")>#FAstruct["#getou.id#_#IncAssignedID#"]#<cfset oufatot = oufatot + FAstruct["#getou.ID#_#IncAssignedID#"]><cfset rowair = rowair+FAstruct["#getou.id#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getou.id#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getou.id#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getou.id#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getou.id#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getou.id#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">
		<cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")>#OIstruct["#getou.ID#_#IncAssignedID#"]#<cfset ouoitot = ouoitot + OIstruct["#getou.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
	<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		
		</cfswitch> 
		 --->
		</td>
	</tr>
		</cfif>
		
		</cfloop>
		
	
		<tr>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"></td>
		<td class="formlablelt" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><strong>Total</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><strong>#numberformat(ouhrsdsp)#</strong></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(oufattot)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(oultitot)#</td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(ourwctot)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(oumtctot)#</td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif ouhrsdsp gt 0 and oultitot gt 0>#numberformat((oultitot*200000)/ouhrsdsp,"0.000")#<cfelse>0</cfif></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfset outrir = oufattot+oultitot+ourwctot+oumtctot><cfif ouhrsdsp gt 0 and outrir gt 0>#numberformat((outrir*200000)/ouhrsdsp,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(oufatot)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfset ouair = oufatot+outrir><cfif ouhrsdsp gt 0 and ouair gt 0>#numberformat((ouair*200000)/ouhrsdsp,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(ouoitot)#</td>
	</tr>
		
		
		
		<cfloop query="getgroups">
		
		<cfset grphrsdsp = 0>
		<cfset grpfattot = 0>
		<cfset grpltitot = 0>
		<cfset grprwctot = 0>
		<cfset grpmtctot = 0>
		<cfset grpfatot = 0>
		<cfset grpoitot = 0>
		
		<cfif getgroups.ouid eq getou.id and listfind(openou,getou.id) gt 0>
		<cfset ctr = 0>
		
		
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset ctr = ctr+1><!--- <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> --->
	<tr >
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif ctr eq 1>#getgroups.group_Name#</cfif></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#IncAssignedTo#</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstructgrp,getgroups.group_number)>#numberformat(manhrstructgrp[getgroups.group_number])#<cfset grphrsdsp = grphrsdsp+manhrstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+manhrstructgrp[getgroups.group_number]><cfelse>0</cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstructgrp,getgroups.group_number)>#numberformat(subhrsstructgrp[getgroups.group_number])#<cfset grphrsdsp = grphrsdsp+subhrsstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+subhrsstructgrp[getgroups.group_number]><cfelse>0</cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstructgrp,getgroups.group_number)>#numberformat(mdgconstructgrp[getgroups.group_number])#<cfset grphrsdsp = grphrsdsp+mdgconstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+mdgconstructgrp[getgroups.group_number]><cfelse>0</cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstructgrp,getgroups.group_number)>#numberformat(jvhrsstructgrp[getgroups.group_number])#<cfset grphrsdsp = grphrsdsp+jvhrsstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+jvhrsstructgrp[getgroups.group_number]><cfelse>0</cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">
		
		<cfif structkeyexists(FATstructgrp,"#getgroups.group_number#_#IncAssignedID#")>#FATstructgrp["#getgroups.group_number#_#IncAssignedID#"]#<cfset grpfattot = grpfattot + FATstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+FATstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		 <!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrmdg = fattotctrmdg + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>  --->
		
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(LTIstructgrp,"#getgroups.group_number#_#IncAssignedID#")>#LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]#<cfset grpltitot = grpltitot + LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowlti = rowlti+LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>   --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif structkeyexists(RWCstructgrp,"#getgroups.group_number#_#IncAssignedID#")>#RWCstructgrp["#getgroups.group_number#_#IncAssignedID#"]#<cfset grprwctot = grprwctot + RWCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+RWCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrmdg = rwctotctrmdg + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif structkeyexists(MTCstructgrp,"#getgroups.group_number#_#IncAssignedID#")>#MTCstructgrp["#getgroups.group_number#_#IncAssignedID#"]#<cfset grpmtctot = grpmtctot + MTCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+MTCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrmdg = mtctotctrmdg + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif structkeyexists(FAstructgrp,"#getgroups.group_number#_#IncAssignedID#")>#FAstructgrp["#getgroups.group_number#_#IncAssignedID#"]#<cfset grpfatot = grpfatot + FAstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowair = rowair+FAstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		</td>
		<td class="bodyTextGrey" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
	<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">
		<cfif structkeyexists(OIstructgrp,"#getgroups.group_number#_#IncAssignedID#")>#OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]#<cfset grpoitot = grpoitot + OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		<!---  <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(OIstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset oitotctrafw = oitotctrafw + OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(OIstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset oitotctrsub = oitotctrsub + OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(OIstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset oitotctrmdg = oitotctrmdg + OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(OIstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset oitotctrjv = oitotctrjv + OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>  --->
		</td>
	</tr>
		</cfif>
		
		</cfloop>
		
		<tr>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"></td>
		<td class="formlablelt" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><strong>Total</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><strong>#numberformat(grphrsdsp)#</strong></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(grpfattot)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(grpltitot)#</td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(grprwctot)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(grpmtctot)#</td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff"><cfif grphrsdsp gt 0 and grpltitot gt 0>#numberformat((grpltitot*200000)/grphrsdsp,"0.000")#<cfelse>0</cfif></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfset grptrir = grpfattot+grpltitot+grprwctot+grpmtctot><cfif grphrsdsp gt 0 and grptrir gt 0>#numberformat((grptrir*200000)/grphrsdsp,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(grpfatot)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfset grpair = grpfatot+grptrir><cfif grphrsdsp gt 0 and grpair gt 0>#numberformat((grpair*200000)/grphrsdsp,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;" bgcolor="fdf8ff">#numberformat(grpoitot)#</td>
	</tr>
		
		
		
		
		
		</cfif>
		</cfloop>
		
		<!--- end ou loop --->
		</cfif>
		</cfloop>
		
		<!--- end bu loop --->
		<tr>
		<td colspan="14" bgcolor="5f2468"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2468;" width="100%"></td>
	</tr>
	</cfloop>
	<cfset ctr = 0>
	<cfset buctr = buctr + 1>
	<cfif not showouonly>
	<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset ctr = ctr+1>
	<tr <cfif buctr mod 2 eq 0>class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif>>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfif ctr eq 1>Amec Foster Wheeler Global</cfif></td>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#IncAssignedTo#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(amfwctr)#<cfset globaltot = globaltot+amfwctr><cfset rowhrs = rowhrs+amfwctr></cfcase>
		<cfcase value="3">#numberformat(subctr)#<cfset globaltot = globaltot+subctr><cfset rowhrs = rowhrs+subctr></cfcase>
		<cfcase value="4">#numberformat(mgdctr)#<cfset globaltot = globaltot+mgdctr><cfset rowhrs = rowhrs+mgdctr></cfcase>
		<cfcase value="11">#numberformat(jvctr)#<cfset globaltot = globaltot+jvctr><cfset rowhrs = rowhrs+jvctr></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(fattotctrafw)#<cfset fattotctrglobal = fattotctrglobal+fattotctrafw><cfset rowtrir = rowtrir+fattotctrafw></cfcase>
		<cfcase value="3">#numberformat(fattotctrsub)#<cfset fattotctrglobal = fattotctrglobal+fattotctrsub><cfset rowtrir = rowtrir+fattotctrsub></cfcase>
		<cfcase value="4">#numberformat(fattotctrmgd)#<cfset fattotctrglobal = fattotctrglobal+fattotctrmgd><cfset rowtrir = rowtrir+fattotctrmgd></cfcase>
		<cfcase value="11">#numberformat(fattotctrjv)#<cfset fattotctrglobal = fattotctrglobal+fattotctrjv><cfset rowtrir = rowtrir+fattotctrjv></cfcase>
		
		</cfswitch>
		
		</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(ltitotctrafw)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrafw><cfset rowlti =rowlti+ltitotctrafw><cfset rowtrir = rowtrir+ltitotctrafw></cfcase>
		<cfcase value="3">#numberformat(ltitotctrsub)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrsub><cfset rowlti =rowlti+ltitotctrsub><cfset rowtrir = rowtrir+ltitotctrsub></cfcase>
		<cfcase value="4">#numberformat(ltitotctrmgd)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrmgd><cfset rowlti =rowlti+ltitotctrmgd><cfset rowtrir = rowtrir+ltitotctrmgd></cfcase>
		<cfcase value="11">#numberformat(ltitotctrjv)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrjv><cfset rowlti =rowlti+ltitotctrjv><cfset rowtrir = rowtrir+ltitotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(rwctotctrafw)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrafw><cfset rowtrir = rowtrir+rwctotctrafw></cfcase>
		<cfcase value="3">#numberformat(rwctotctrsub)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrsub><cfset rowtrir = rowtrir+rwctotctrsub></cfcase>
		<cfcase value="4">#numberformat(rwctotctrmgd)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrmgd><cfset rowtrir = rowtrir+rwctotctrmgd></cfcase>
		<cfcase value="11">#numberformat(rwctotctrjv)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrjv><cfset rowtrir = rowtrir+rwctotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(mtctotctrafw)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrafw><cfset rowtrir = rowtrir+mtctotctrafw></cfcase>
		<cfcase value="3">#numberformat(mtctotctrsub)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrsub><cfset rowtrir = rowtrir+mtctotctrsub></cfcase>
		<cfcase value="4">#numberformat(mtctotctrmgd)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrmgd><cfset rowtrir = rowtrir+mtctotctrmgd></cfcase>
		<cfcase value="11">#numberformat(mtctotctrjv)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrjv><cfset rowtrir = rowtrir+mtctotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(fatotctrafw)#<cfset fatotctrglobal = fatotctrglobal+fatotctrafw><cfset rowair = rowair+fatotctrafw></cfcase>
		<cfcase value="3">#numberformat(fatotctrsub)#<cfset fatotctrglobal = fatotctrglobal+fatotctrsub><cfset rowair = rowair+fatotctrsub></cfcase>
		<cfcase value="4">#numberformat(fatotctrmgd)#<cfset fatotctrglobal = fatotctrglobal+fatotctrmgd><cfset rowair = rowair+fatotctrmgd></cfcase>
		<cfcase value="11">#numberformat(fatotctrjv)#<cfset fatotctrglobal = fatotctrglobal+fatotctrjv><cfset rowair = rowair+fatotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(oitotctrafw)#<cfset oitotctrglobal = oitotctrglobal+oitotctrafw><cfset rowair = rowair+fatotctrafw></cfcase>
		<cfcase value="3">#numberformat(oitotctrsub)#<cfset oitotctrglobal = oitotctrglobal+oitotctrsub><cfset rowair = rowair+fatotctrsub></cfcase>
		<cfcase value="4">#numberformat(oitotctrmgd)#<cfset oitotctrglobal = oitotctrglobal+oitotctrmgd><cfset rowair = rowair+fatotctrmgd></cfcase>
		<cfcase value="11">#numberformat(oitotctrjv)#<cfset oitotctrglobal = oitotctrglobal+oitotctrjv><cfset rowair = rowair+fatotctrjv></cfcase>
		
		</cfswitch>
		</td>
	</tr>
	</cfif>
	</cfloop>
	
	<tr>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"></td>
		<td class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><strong>Total</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#numberformat(globaltot)#</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(fattotctrglobal)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">#numberformat(ltitotctrglobal)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(rwctotctrglobal)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">#numberformat(mtctotctrglobal)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff"><cfif globaltot gt 0 and ltitotctrglobal gt 0>#numberformat((ltitotctrglobal*200000)/globaltot,"0.000")#<cfelse>0</cfif></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfset glbtrir = fattotctrglobal+ltitotctrglobal+rwctotctrglobal+mtctotctrglobal><cfif globaltot gt 0 and glbtrir gt 0>#numberformat((glbtrir*200000)/globaltot,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(fatotctrglobal)#</td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><cfset glbair = fatotctrglobal+glbtrir><cfif globaltot gt 0 and glbair gt 0>#numberformat((glbair*200000)/globaltot,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" bgcolor="fdf8ff">#numberformat(oitotctrglobal)#</td>
	</tr>
	</cfif>
</table>


</cfoutput>
 </cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_configdashboardpdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE">
			 --->
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">