
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_monthlypdf.cfm", "pdfgen"))>
<cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "IncidentsByType_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25">
<cfoutput>
<cfparam name="dosearch" default="">

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Monthly Statistic Table/Monthly Cumulative</td>
</tr>

</table>




<table cellpadding="3" cellspacing="1" bgcolor="000000" width="90%" border="0">
	<tr>
		<td class="purplebg" bgcolor="5f2468" rowspan="2"></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Hours Worked</strong></td>
		<!--- <td><img src="images/spacer.gif" width="1" style="padding-left:0;padding-right:0;"></td> --->
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Lost Time Injuries</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Total Recordable Cases</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">First Aid</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">High Potential Incidents</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">AIR</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">TRIR</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">LTIR</strong></td>
		<td class="purplebg" bgcolor="5f2468" align="center" colspan="3"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">HiPoR</strong></td>
	</tr>
	<tr>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		<!--- <td><img src="images/spacer.gif" width="1" style="padding-left:0;padding-right:0;"></td> --->
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<!--- <td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Target</strong></td> --->
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<!--- <td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Target</strong></td> --->
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<!--- <td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Target</strong></td> --->
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
		
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Month</strong></td>
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">YTD</strong></td>
		<!--- <td class="formlable" align="center"><strong class="formlable">Target</strong></td> --->
		<td class="formlable" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"><strong class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">Rolling</strong></td>
	</tr>
	<cfset yrctr = 0>
	<cfset ltictr = 0>
	<cfset trictr = 0>
	<cfset factr = 0>
	<cfset hpctr = 0>
	<cfset ytdallhrs = 0>
	
	<cfset FATctr = 0>
	
	<cfloop from="1" to="12" index="i">
	
		<cfset monthhrs = 0>
		<!--- <cfif structkeyexists(manhrstructoffshore,"#lookupyear#_#i#")>
			<cfset monthhrs = monthhrs + manhrstructoffshore["#lookupyear#_#i#"]>
			<cfset ytdallhrs = ytdallhrs + manhrstructoffshore["#lookupyear#_#i#"]>
		</cfif> --->
	<cfset rollosctr = 0>
	<cfset rollctr = 0>
	<cfset rollltictr = 0>
	<cfset rolltrictr = 0>
	<cfset rollfactr = 0>
	<cfset rollhpctr = 0>
	
	<cfset rollFATctr = 0>
	
	
	
	<cfif structkeyexists(manhrstruct,"#lookupyear#_#i#")>
		<cfset yrctr = yrctr + manhrstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(LTIstruct,"#lookupyear#_#i#")>
		<cfset ltictr = ltictr + LTIstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(TRIstruct,"#lookupyear#_#i#")>
		<cfset trictr = trictr + TRIstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(Fatalitystruct,"#lookupyear#_#i#")>
		<cfset trictr = trictr + Fatalitystruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(FAstruct,"#lookupyear#_#i#")>
		<cfset factr = factr + FAstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(HiPostruct,"#lookupyear#_#i#")>
		<cfset hpctr = hpctr + HiPostruct["#lookupyear#_#i#"]>
	</cfif>
		<cfset rolloslist = "">
		<cfset rollhrlist = "">
		<cfset rollLTIlist = "">
		<cfset rollTRIlist = "">
		<cfset rollFAlist = "">
		<cfset rollHPlist = "">
		<cfset rollFATlist = "">
		<cfloop from="#i+1#" to="12" index="o">
			<cfset rollhrlist = listappend(rollhrlist,"#lookupyear-1#_#o#")>
			<cfset rollLTIlist = listappend(rollLTIlist,"#lookupyear-1#_#o#")>
			<cfset rollTRIlist = listappend(rollTRIlist,"#lookupyear-1#_#o#")>
			<cfset rollFAlist = listappend(rollFAlist,"#lookupyear-1#_#o#")>
			<cfset rollHPlist = listappend(rollHPlist,"#lookupyear-1#_#o#")>
			<cfset rolloslist = listappend(rolloslist,"#lookupyear-1#_#o#")>
			<cfset rollFATlist = listappend(rollFATlist,"#lookupyear-1#_#o#")>
		</cfloop>
		<cfloop from="1" to="#12-listlen(rollhrlist)#" index="u">
			<cfset rollhrlist = listappend(rollhrlist,"#lookupyear#_#u#")>
			<cfset rollLTIlist = listappend(rollLTIlist,"#lookupyear#_#u#")>
			<cfset rollTRIlist = listappend(rollTRIlist,"#lookupyear#_#u#")>
			<cfset rollFAlist = listappend(rollFAlist,"#lookupyear#_#u#")>
			<cfset rollHPlist = listappend(rollHPlist,"#lookupyear#_#u#")>
			<cfset rolloslist = listappend(rolloslist,"#lookupyear#_#u#")>
			<cfset rollFATlist = listappend(rollFATlist,"#lookupyear#_#u#")>
		</cfloop>
		
		<cfloop list="#structkeylist(Fatalitystruct)#" index="j">
			<cfif listfind(rollFATlist,j) gt 0>
				<cfset rolltrictr = rolltrictr+Fatalitystruct[j]>
			</cfif>
		</cfloop>
		
		<!--- <cfloop list="#structkeylist(manhrstructoffshore)#" index="j">
			<cfif listfind(rolloslist,j) gt 0>
				<cfset rollosctr = rollosctr+manhrstructoffshore[j]>
			</cfif>
		</cfloop> --->
		<cfloop list="#structkeylist(manhrstruct)#" index="j">
			<cfif listfind(rollhrlist,j) gt 0>
				<cfset rollctr = rollctr+manhrstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(LTIstruct)#" index="j">
			<cfif listfind(rollLTIlist,j) gt 0>
				<cfset rollltictr = rollltictr+LTIstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(TRIstruct)#" index="j">
			<cfif listfind(rollTRIlist,j) gt 0>
				<cfset rolltrictr = rolltrictr+TRIstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(FAstruct)#" index="j">
			<cfif listfind(rollFAlist,j) gt 0>
				<cfset rollfactr = rollfactr+FAstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(HiPostruct)#" index="j">
			<cfif listfind(rollHPlist,j) gt 0>
				<cfset rollhpctr = rollhpctr+HiPostruct[j]>
			</cfif>
		</cfloop>
		
		<cfset airctr = 0>
		<cfset airytd = 0>
		<cfset airroll = 0>
		<cfset dspytdall = 0>
		<cfset dsprollos = 0>
		<cfset trirctr = 0>
		<cfset trirctrytd = 0>
		<cfset trirctrroll = 0>
		<cfset ltirctr = 0>
		<cfset ltirctrytd = 0>
		<cfset ltirctrroll = 0>
		<cfset hipoctr = 0>
		<cfset fatalityctr = 0>
		
		<cfset monthlytri = 0>
		<cfif structkeyexists(TRIstruct,"#lookupyear#_#i#")>
			<cfset monthlytri = monthlytri + TRIstruct["#lookupyear#_#i#"]>
		</cfif>
		<cfif structkeyexists(Fatalitystruct,"#lookupyear#_#i#")>
			<cfset monthlytri = monthlytri + Fatalitystruct["#lookupyear#_#i#"]>
		</cfif>
		
		
	<tr>
		<td class="purplebg" bgcolor="5f2468" align="center"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">#left(monthasstring(i),3)#</strong></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center">
			<cfif lookupyear eq year(now())>
				<cfif i lte month(now())>
					<cfif structkeyexists(manhrstruct,"#lookupyear#_#i#")>#numberformat(manhrstruct["#lookupyear#_#i#"])#<cfset monthhrs = monthhrs+manhrstruct["#lookupyear#_#i#"]>
					<cfelse>0
					</cfif>
				</cfif>
			<cfelse><cfif structkeyexists(manhrstruct,"#lookupyear#_#i#")>#numberformat(manhrstruct["#lookupyear#_#i#"])#<cfset monthhrs = monthhrs+manhrstruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dspytdall = ytdallhrs+yrctr>#numberformat(yrctr)#</cfif><cfelse><cfset dspytdall = ytdallhrs+yrctr>#numberformat(yrctr)#</cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dsprollos = rollosctr+rollctr>#numberformat(rollctr)#</cfif><cfelse><cfset dsprollos = rollosctr+rollctr>#numberformat(rollctr)#</cfif></td>
		<!--- <td><img src="images/spacer.gif" width="1" style="padding-left:0;padding-right:0;"></td> --->
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif structkeyexists(LTIstruct,"#lookupyear#_#i#")>#numberformat(LTIstruct["#lookupyear#_#i#"])#<cfset ltirctr = ltirctr+LTIstruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif><cfelse><cfif structkeyexists(LTIstruct,"#lookupyear#_#i#")>#numberformat(LTIstruct["#lookupyear#_#i#"])#<cfset ltirctr = ltirctr+LTIstruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(ltictr)#<cfset ltirctrytd = ltirctrytd+ltictr></cfif><cfelse>#numberformat(ltictr)#<cfset ltirctrytd = ltirctrytd+ltictr></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(rollltictr)#<cfset ltirctrroll = ltirctrroll+rollltictr></cfif><cfelse>#numberformat(rollltictr)#<cfset ltirctrroll = ltirctrroll+rollltictr></cfif></td>
		
		
		
		
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#monthlytri#<cfset trirctr = trirctr+monthlytri><cfset airctr = airctr+monthlytri></cfif><cfelse>#monthlytri#<cfset trirctr = trirctr+monthlytri><cfset airctr = airctr+monthlytri></cfif></td>

		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(trictr)#<cfset airytd = airytd+trictr><cfset trirctrytd = trirctrytd+trictr></cfif><cfelse>#numberformat(trictr)#<cfset airytd = airytd+trictr><cfset trirctrytd = trirctrytd+trictr></cfif></td>

		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(rolltrictr)#<cfset airroll = airroll+rolltrictr><cfset trirctrroll = trirctrroll+rolltrictr></cfif><cfelse>#numberformat(rolltrictr)#<cfset airroll = airroll+rolltrictr><cfset trirctrroll = trirctrroll+rolltrictr></cfif></td>
		
		
		
		
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif structkeyexists(FAstruct,"#lookupyear#_#i#")>#numberformat(FAstruct["#lookupyear#_#i#"])#<cfset airctr = airctr+FAstruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif><cfelse><cfif structkeyexists(FAstruct,"#lookupyear#_#i#")>#numberformat(FAstruct["#lookupyear#_#i#"])#<cfset airctr = airctr+FAstruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(factr)#<cfset airytd = airytd+factr></cfif><cfelse>#numberformat(factr)#<cfset airytd = airytd+factr></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(rollfactr)#<cfset airroll = airroll+rollfactr></cfif><cfelse>#numberformat(rollfactr)#<cfset airroll = airroll+rollfactr></cfif></td>
		
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif structkeyexists(HiPostruct,"#lookupyear#_#i#")>#numberformat(HiPostruct["#lookupyear#_#i#"])#<cfset hipoctr = hipoctr+HiPostruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif><cfelse><cfif structkeyexists(HiPostruct,"#lookupyear#_#i#")>#numberformat(HiPostruct["#lookupyear#_#i#"])#<cfset hipoctr = hipoctr+HiPostruct["#lookupyear#_#i#"]><cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(hpctr)#</cfif><cfelse>#numberformat(hpctr)#</cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#numberformat(rollhpctr)#</cfif><cfelse>#numberformat(rollhpctr)#</cfif></td>
				
		
				
				
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif airctr gt 0 and monthhrs gt 0>#numberformat((airctr*200000)/monthhrs,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif airctr gt 0 and monthhrs gt 0>#numberformat((airctr*200000)/monthhrs,"0.00")#<cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif airytd gt 0 and dspytdall gt 0>#numberformat((airytd*200000)/dspytdall,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif airytd gt 0 and dspytdall gt 0>#numberformat((airytd*200000)/dspytdall,"0.00")#<cfelse>0</cfif></cfif></td>
		<!--- <td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#gettargets.airgoal#</cfif><cfelse>#gettargets.airgoal#</cfif></td> --->
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif airroll gt 0 and dsprollos gt 0>#numberformat((airroll*200000)/dsprollos,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif airroll gt 0 and dsprollos gt 0>#numberformat((airroll*200000)/dsprollos,"0.00")#<cfelse>0</cfif></cfif></td>
		
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctr gt 0 and monthhrs gt 0>#numberformat((trirctr*200000)/monthhrs,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif trirctr gt 0 and monthhrs gt 0>#numberformat((trirctr*200000)/monthhrs,"0.00")#<cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctrytd gt 0 and dspytdall gt 0>#numberformat((trirctrytd*200000)/dspytdall,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif trirctrytd gt 0 and dspytdall gt 0>#numberformat((trirctrytd*200000)/dspytdall,"0.00")#<cfelse>0</cfif></cfif></td>
		<!--- <td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#gettargets.TRIRgoal#</cfif><cfelse>#gettargets.TRIRgoal#</cfif></td> --->
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctrroll gt 0 and dsprollos gt 0>#numberformat((trirctrroll*200000)/dsprollos,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif trirctrroll gt 0 and dsprollos gt 0>#numberformat((trirctrroll*200000)/dsprollos,"0.00")#<cfelse>0</cfif></cfif></td>
		
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif ltirctr gt 0 and monthhrs gt 0>#numberformat((ltirctr*200000)/monthhrs,"0.000")#<cfelse>0</cfif></cfif><cfelse><cfif ltirctr gt 0 and monthhrs gt 0>#numberformat((ltirctr*200000)/monthhrs,"0.000")#<cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif ltirctrytd gt 0 and dspytdall gt 0>#numberformat((ltirctrytd*200000)/dspytdall,"0.000")#<cfelse>0</cfif></cfif><cfelse><cfif ltirctrytd gt 0 and dspytdall gt 0>#numberformat((ltirctrytd*200000)/dspytdall,"0.000")#<cfelse>0</cfif></cfif></td>
		<!--- <td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())>#gettargets.LTIRgoal#</cfif><cfelse>#gettargets.LTIRgoal#</cfif></td> --->
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctrroll gt 0 and dsprollos gt 0>#numberformat((ltirctrroll*200000)/dsprollos,"0.000")#<cfelse>0</cfif></cfif><cfelse><cfif trirctrroll gt 0 and dsprollos gt 0>#numberformat((ltirctrroll*200000)/dsprollos,"0.000")#<cfelse>0</cfif></cfif></td>
		
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif hipoctr gt 0 and monthhrs gt 0>#numberformat((hipoctr*200000)/monthhrs,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif hipoctr gt 0 and monthhrs gt 0>#numberformat((hipoctr*200000)/monthhrs,"0.00")#<cfelse>0</cfif></cfif></td>
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif hpctr gt 0 and dspytdall gt 0>#numberformat((hpctr*200000)/dspytdall,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif hpctr gt 0 and dspytdall gt 0>#numberformat((hpctr*200000)/dspytdall,"0.00")#<cfelse>0</cfif></cfif></td>
	<!--- 	<td <cfif i mod 2 is 0>class="formlable"<cfelse>bgcolor="ffffff" class="bodyTextGrey"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())></cfif><cfelse></cfif></td> --->
		<td <cfif i mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"</cfif> align="center"><cfif lookupyear eq year(now())><cfif i lte month(now())><cfif rollhpctr gt 0 and dsprollos gt 0>#numberformat((rollhpctr*200000)/dsprollos,"0.00")#<cfelse>0</cfif></cfif><cfelse><cfif rollhpctr gt 0 and dsprollos gt 0>#numberformat((rollhpctr*200000)/dsprollos,"0.00")#<cfelse>0</cfif></cfif></td>
	</tr>
	</cfloop>
</table>
<table cellpadding="2" cellspacing="0" border="0" align="left">
	<tr>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>LTIR</strong></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">Number of lost time incidents X 200,000 &divide; hours worked</td>
	</tr>
	<tr>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>TRIR</strong></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">Number of recordable cases X 200,000 &divide; hours worked</td>
	</tr>
	<tr>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>AIR</strong></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">Number of recordable cases + first aid cases X 200,000 &divide; hours worked</td>
	</tr>
	<tr>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>Rolling</strong></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">12 months total, ie Jan = Feb #year(now())-1# to Jan #year(now())#</td>
	</tr>
</table>
</cfoutput>
</cfdocument>
			 
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_monthlypdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">