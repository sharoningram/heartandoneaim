<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_capalogpdf.cfm", "pdfgen"))>
<cfset fnamepdf = "CAPAlog_#timeformat(now(),'hhnnssl')#.pdf">
<cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfparam name="dosearch" default="">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">
<style>
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #FFFFFF;
	font-style: normal;
}
.purplebg {
background: #5f2468;

}
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f5f5f;
	font-style: normal;
	}
.formlable{
background-color:#f7f1f9;
font-family: Segoe UI;
	font-size: 10pt;

	color: #5f5f5f;
	font-style: normal;
}
</style>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="900">
<tr>
	<td class="bodyTexthd" colspan="2">CA/PA Log</td>
</tr>
</table>

<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="formlable" colspan="6" align="center"><strong>Incident Details</strong></td>
		<td class="formlable" colspan="6" align="center"><strong>Action Details</strong></td>
	</tr>
	<tr>
		<td class="formlable"   width="4%" align="center"><strong>Incident Number</strong></td>
		<td class="formlable"  width="10%" align="center"><strong><cfoutput>#request.oulabellong#</cfoutput></strong></td>
		<td class="formlable"  align="center"  width="10%"><strong>Project/Office</strong></td>
		<td class="formlable"  align="center"  width="10%"><strong>Site/Office Name</strong></td>
		<td class="formlable"  align="center"  width="16%"><strong>Short Description</strong></td>
		<td class="formlable" align="center"><strong>Record Status</strong></td>
		<td class="formlable"  align="center"  width="3%"><strong>Action Type</strong></td>
		<td class="formlable"  align="center"   width="3%"><strong>Due Date</strong></td>
		<td class="formlable"  align="center"   width="10%"><strong>Person Responsible</strong></td>
		<td class="formlable"  align="center"  width="16%"><strong>Details of Action Req'd</strong></td>
		<td class="formlable"  align="center"  width="10%"><strong>CA/PA Assignee Email</strong></td>
		<td class="formlable"  align="center"  width="8%"><strong>Date Completed</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfoutput query="capalog">
	<cfif status neq "Review Completed">
	<cfset ctr = ctr+1>
	<tr <cfif ctr mod 2 is 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodyTextGrey">#trackingnum#</td>
		<td class="bodyTextGrey">#ouname#</td>
		<td class="bodyTextGrey">#group_name#</td>
		<td class="bodyTextGrey">#sitename#</td>
		<td class="bodyTextGrey">#shortdesc#</td>
		<td class="bodyTextGrey" align="center">#status#</td>
		<td class="bodyTextGrey">#capatype#</td>
		<td class="bodyTextGrey" align="center">#dateformat(duedate,"dd mmmm yyyy")#</td>
		<td class="bodyTextGrey">#AssignedTo#</td>
		<td class="bodyTextGrey">#ActionDetail#</td>
		<td class="bodyTextGrey">#AssigneeEmail#</td>
		<td class="bodyTextGrey" align="center">#dateformat(DateComplete,"dd mmmm yyyy")#</td>
		
	</tr>
	</cfif>
	</cfoutput>
</table>
</cfdocument>
<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">