<!--- <cfset userrepstruct = {}>
<cfloop query="getuserreps">
	<cfset repname = SearchName>
	<cfif trim(SaveCtr) gt 1><cfset repname = repname & " (#SaveCtr#)"></cfif>
	<cfif not structkeyexists(userrepstruct,reportType)>
		<cfset userrepstruct[reportType] = repname>
	<cfelse>
		<cfset userrepstruct[reportType] = listappend(userrepstruct[reportType],repname,"~")>
	</cfif>
</cfloop> --->
<div align="center" class="midbox">
	<div align="center" class="midboxtitle">What report would you like to view?</div>
	<table cellpadding="8" cellspacing="0" border="0" align="center" width="100%">
		<cfif getuserreps.recordcount gt 0>
		<cfoutput query="getuserreps" group="reportType">
		<cfswitch expression="#reportType#">
		<cfcase value="bird">
			<tr>
			<td class="formlable" colspan="2">BIRD Triangle</td>
			</tr>
		</cfcase>
		<cfcase value="spills">
			<tr>
			<td class="formlable" colspan="2">Environmental Spills</td>
			</tr>
		</cfcase>
		<cfcase value="contractorperf">
			<tr>
			<td class="formlable" colspan="2">Contractor Performance Report</td>
			</tr>
		</cfcase>
		<cfcase value="clientperf">
			<tr>
			<td class="formlable" colspan="2">Client Performance Report</td>
			</tr>
		</cfcase>
		<cfcase value="rwc">
			<tr>
			<td class="formlable" colspan="2">Restricted Work Cases</td>
			</tr>
		</cfcase>
		<cfcase value="mht">
			<tr>
			<td class="formlable" colspan="2">Man-Hours Total Report</td>
			</tr>
		</cfcase>
		<cfcase value="frb">
			<tr>
			<td class="formlable" colspan="2">Frequency Rate Breakdown Table</td>
			</tr>
		</cfcase>
		<cfcase value="configdash">
			<tr>
			<td class="formlable" colspan="2">Configurable Dashboard Report</td>
			</tr>
		</cfcase>
		<cfcase value="monthly">
			<tr>
			<td class="formlable" colspan="2">Monthly Statistic Table/Monthly Cumulative</td>
			</tr>
		</cfcase>
		<cfcase value="numemployed">
			<tr>
			<td class="formlable" colspan="2">Numbers Employed</td>
			</tr>
		</cfcase>
		<cfcase value="empbymonth">
			<tr>
			<td class="formlable" colspan="2">Numbers Employed - By Month</td>
			</tr>
		</cfcase>
		<cfcase value="potrating">
			<tr>
			<td class="formlable" colspan="2">Incident Potential Rating Matrix</td>
			</tr>
		</cfcase>
		<cfcase value="ratetriangle">
			<tr>
			<td class="formlable" colspan="2">Incident Potential Rating Triangle</td>
			</tr>
		</cfcase>
		<cfcase value="incwdesc">
			<tr>
			<td class="formlable" colspan="2">Incidents with Description</td>
			</tr>
		</cfcase>
		<cfcase value="daysaway">
			<tr>
			<td class="formlable" colspan="2">Days Away from Work</td>
			</tr>
		</cfcase>
		<cfcase value="oshachart">
			<tr>
			<td class="formlable" colspan="2">Injury Type by OSHA Chart</td>
			</tr>
		</cfcase>
		<cfcase value="incbytype">
			<tr>
			<td class="formlable" colspan="2">Incidents by Type</td>
			</tr>
		</cfcase>
		<cfcase value="incbytypechart">
			<tr>
			<td class="formlable" colspan="2">Incidents by Type Chart</td>
			</tr>
		</cfcase>
		<cfcase value="Diamond">
			<tr>
			<td class="formlable" colspan="2">Mining the Diamond</td>
			</tr>
		</cfcase>
		<cfcase value="incidentman">
			<tr>
			<td class="formlable" colspan="2">Incident Man</td>
			</tr>
		</cfcase>
		<cfcase value="incclasschart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Incident Classification Chart</td>
			</tr>
		</cfcase>
		<cfcase value="ouincclasschart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Incident Classification Chart</td>
			</tr>
		</cfcase>
		<cfcase value="prjincclasschart">
			<tr>
			<td class="formlable" colspan="2">Project Incident Classification Chart</td>
			</tr>
		</cfcase>
		
		<cfcase value="safetyessentials">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Safety Essentials</td>
			</tr>
		</cfcase>
		<cfcase value="ousafetyessentials">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Safety Essentials</td>
			</tr>
		</cfcase>
		<cfcase value="prjsafetyessentials">
			<tr>
			<td class="formlable" colspan="2">Project Safety Essentials</td>
			</tr>
		</cfcase>
	
		<cfcase value="immcausechart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Immediate Causes Chart</td>
			</tr>
		</cfcase>
		<cfcase value="ouimmcausechart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Immediate Causes Chart</td>
			</tr>
		</cfcase>
		<cfcase value="prjimmcausechart">
			<tr>
			<td class="formlable" colspan="2">Project Immediate Causes Chart</td>
			</tr>
		</cfcase>
		
		<cfcase value="injcausechart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Cause of Injury Chart</td>
			</tr>
		</cfcase>
		<cfcase value="ouinjcausechart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Cause of Injury Chart</td>
			</tr>
		</cfcase>
		<cfcase value="prjinjcausechart">
			<tr>
			<td class="formlable" colspan="2">Project Cause of Injury Chart</td>
			</tr>
		</cfcase>
		
		<cfcase value="hazardsourcechart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Source of Hazard</td>
			</tr>
		</cfcase>
		<cfcase value="ouhazardsourcechart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Source of Hazard</td>
			</tr>
		</cfcase>
		<cfcase value="prjhazardsourcechart">
			<tr>
			<td class="formlable" colspan="2">Project Source of Hazard</td>
			</tr>
		</cfcase>
		
		
		<cfcase value="safetyrules">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Global Safety Rules</td>
			</tr>
		</cfcase>
		<cfcase value="ousafetyrules">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Global Safety Rules</td>
			</tr>
		</cfcase>
		<cfcase value="prjsafetyrules">
			<tr>
			<td class="formlable" colspan="2">Project Global Safety Rules</td>
			</tr>
		</cfcase>
		<cfcase value="rootcausechart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Root Cause</td>
			</tr>
		</cfcase>
		<cfcase value="ourootcausechart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Root Cause</td>
			</tr>
		</cfcase>
		<cfcase value="prjrootcausechart">
			<tr>
			<td class="formlable" colspan="2">Project Root Cause</td>
			</tr>
		</cfcase>
		
		<cfcase value="rootcausesubchart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Root Cause Sub-Type</td>
			</tr>
		</cfcase>
		<cfcase value="ourootcausesubchart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Root Cause Sub-Type</td>
			</tr>
		</cfcase>
		<cfcase value="prjrootcausesubchart">
			<tr>
			<td class="formlable" colspan="2">Project Root Cause Sub-Type</td>
			</tr>
		</cfcase>
		<cfcase value="reasonlate">
			<tr>
			<td class="formlable" colspan="2">Reason for Late Recording</td>
			</tr>
		</cfcase>
		<cfcase value="immcausesubchart">
			<tr>
			<td class="formlable" colspan="2">#request.bulabellong# Immediate Cause Sub-Type</td>
			</tr>
		</cfcase>
		<cfcase value="ouimmcausesubchart">
			<tr>
			<td class="formlable" colspan="2">#request.oulabellong# Immediate Cause Sub-Type</td>
			</tr>
		</cfcase>
		<cfcase value="prjimmcausesubchart">
			<tr>
			<td class="formlable" colspan="2">Project Immediate Cause Sub-Type</td>
			</tr>
		</cfcase>
		
		<cfcase value="capalog">
			<tr>
			<td class="formlable" colspan="2">CA/PA Log</td>
			</tr>
		</cfcase>
		<cfcase value="daysopen">
			<tr>
			<td class="formlable" colspan="2">Incident Days Open</td>
			</tr>
		</cfcase>
		<cfcase value="daystoclose">
			<tr>
			<td class="formlable" colspan="2">Incident Days to Close</td>
			</tr>
		</cfcase>
		<cfcase value="TRIrolling">
			<tr>
			<td class="formlable" colspan="2">Total Recordable Incidents Rolling Chart</td>
			</tr>
		</cfcase>
		<cfcase value="ALLrolling">
			<tr>
			<td class="formlable" colspan="2">All Injury Incidents Rolling Chart</td>
			</tr>
		</cfcase>
		<cfcase value="LTIrolling">
			<tr>
			<td class="formlable" colspan="2">Lost Time Injuries Rolling Chart</td>
			</tr>
		</cfcase>
		</cfswitch>
			<cfoutput>
			<tr>
			<td class="midboxtitletxt" width="95%"><a href="index.cfm?fuseaction=reports.directmyreport&rep=#UserReportID#" class="midboxtitletxt">#SearchName#</a></td>
			<td><a href="#self#?fuseaction=#attributes.xfa.deletemyreports#&prjsearch=#UserReportID#" title="Delete"><img src="images/trash.gif" border="0" alt="Delete"></a></td><!--- <td><a href="index.cfm?fuseaction=reports.directmyreport&rep=#UserReportID#"><img src="images/smbutton.png" border="0"></a></td> --->
			</tr>
		</cfoutput>
		
	</cfoutput>
	<cfelse>
		<tr>
			<td align="center" class="bodyTextGrey" colspan="2">You have no saved reports</td>
		</tr>
		</cfif>
		
		
	
	</table>



</div>