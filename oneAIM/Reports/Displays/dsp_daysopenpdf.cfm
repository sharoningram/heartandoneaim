<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_daysopenpdf.cfm", "pdfgen"))>
<cfset fnamepdf = "DaysOpen_#timeformat(now(),'hhnnssl')#.pdf">
<cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfparam name="dosearch" default="">




<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">
<style>
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #FFFFFF;
	font-style: normal;
}
.purplebg {
background: #5f2468;

}
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f5f5f;
	font-style: normal;
	}
.formlable{
background-color:#f7f1f9;
font-family: Segoe UI;
	font-size: 10pt;

	color: #5f5f5f;
	font-style: normal;
}
</style>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="900">
<tr>
	<td class="bodyTexthd" colspan="2">Incident Days Open</td>
</tr>
</table>

<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%">
	
	<tr>
		<td class="formlable" ><strong>Incident Number</strong></td>
		<td class="formlable" ><strong><cfoutput>#request.oulabellong#</cfoutput></strong></td>
		<td class="formlable"  align="center" ><strong>Business Stream</strong></td>
		<td class="formlable"  align="center" ><strong>Project/Office</strong></td>
		<td class="formlable"  align="center" ><strong>Site/Office Name</strong></td>
		<td class="formlable"  align="center" ><strong>Incident Type</strong></td>
		<td class="formlable"  align="center" ><strong>Near Miss?</strong></td>
		<td class="formlable"  align="center" ><strong>OSHA Classification</strong></td>
		<td class="formlable"  align="center" ><strong>Potential Rating</strong></td>
		<td class="formlable"  align="center" ><strong>Incident Date</strong></td>
		<td class="formlable"  align="center" ><strong>Work Related?</strong></td>
		<td class="formlable"  align="center" ><strong>Incident Assigned To</strong></td>
		<td class="formlable"  align="center" ><strong>Client</strong></td>
		<td class="formlable"  align="center" ><strong>Short Description</strong></td>
		<td class="formlable"  align="center" ><strong>Days Since Opened</strong></td>
		<td class="formlable"  align="center" ><strong>Record Status</strong></td>
		<td class="formlable"  align="center" ><strong>Days Awaiting Action</strong></td>
		<td class="formlable"  align="center" ><strong>Pending Action By</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfoutput query="getdaysopen">
	
	<cfif status neq "Review Completed">
	<cfset ctr = ctr+1>
	<tr <cfif ctr mod 2 is 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodyTextGrey">#TrackingNum#</td>
		<td class="bodyTextGrey">#ouname#</td>
		<td class="bodyTextGrey">#bsname#</td>
		<td class="bodyTextGrey">#group_name#</td>
		<td class="bodyTextGrey">#sitename#</td>
		<td class="bodyTextGrey"><cfif isfatality eq "yes">Fatality<cfelse>#IncType#</cfif></td>
		<td class="bodyTextGrey" align="center" >#isnearmiss#</td>
		<td class="bodyTextGrey">#category#</td>
		<td class="bodyTextGrey" align="center" >#PotentialRating#</td>
		<td class="bodyTextGrey">#dateformat(incidentDate,sysdateformat)#</td>
		<td class="bodyTextGrey" align="center" >#isworkrelated#</td>
		<td class="bodyTextGrey">#IncAssignedTo#</td>
		<td class="bodyTextGrey">#clientname#</td>
		<td class="bodyTextGrey">#shortdesc#</td>
		<td class="bodyTextGrey" align="center" >#datediff('d',dateCreated,now())#</td>
		<td class="bodyTextGrey">#status#</td>
		<td class="bodyTextGrey" align="center" >
		<cfswitch expression="#status#">
			<cfcase value="Initiated">#datediff('d',dateCreated,now())#</cfcase>
			<cfcase value="withdrawn"><cfif structkeyexists(withdrawnstruct,irn)>#datediff('d',withdrawnstruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			<cfcase value="Awaiting Investigation"><cfif structkeyexists(investigationstruct,irn)>#datediff('d',investigationstruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			<cfcase value="Sent for Review"><cfif structkeyexists(sentforreviewstruct,irn)>#datediff('d',sentforreviewstruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			<cfcase value="Pending Closure"><cfif structkeyexists(pendclosestruct,irn)>#datediff('d',pendclosestruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			<cfcase value="More Info Requested"><cfif structkeyexists(moreinfostruct,irn)>#datediff('d',moreinfostruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			<cfcase value="Investigation Review"><cfif structkeyexists(investreviewstruct,irn)>#datediff('d',investreviewstruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			<cfcase value="Investigation Needs More Info"><cfif structkeyexists(investmistruct,irn)>#datediff('d',investmistruct[irn],now())#<cfelse>#datediff('d',dateCreated,now())#</cfif></cfcase>
			
		</cfswitch>
				
		
		
		</td>
		<td class="bodyTextGrey">
		<cfswitch expression="#status#">
			<cfcase value="Initiated">#createdBy#</cfcase>
			<cfcase value="withdrawn"><cfif structkeyexists(withdrawnuserstruct,irn)>#withdrawnuserstruct[irn]#</cfif></cfcase>
			<cfcase value="Awaiting Investigation">#createdBy#</cfcase>
			<cfcase value="Sent for Review"><cfif structkeyexists(sentforreviewuserstruct,irn)>#sentforreviewuserstruct[irn]#</cfif></cfcase>
			<cfcase value="Pending Closure">#createdBy#</cfcase>
			<cfcase value="More Info Requested">#createdBy#</cfcase>
			<cfcase value="Investigation Review"><cfif structkeyexists(investreviewuserstruct,irn)>#investreviewuserstruct[irn]#</cfif></cfcase>
			<cfcase value="Investigation Needs More Info"><cfif structkeyexists(investmiuserstruct,irn)>#investmiuserstruct[irn]#</cfif></cfcase>
			
		</cfswitch>
		
		</td>
	</tr>
	</cfif>
	</cfoutput> 
</table>
</cfdocument>
<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">