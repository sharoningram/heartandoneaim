
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_frbpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "FrequencyRateBreakdown_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">
<cfoutput>
<cfparam name="dosearch" default="">

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Frequency Rate Breakdown Table</td>
</tr>

</table>
</cfoutput>

<table cellpadding="2" cellspacing="1" bgcolor="000000" width="100%" border="0">
	<tr>
		
		<td class="purplebg" align="center" rowspan="2" width="25%" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Company</strong></td>
		<td class="purplebg" align="center" rowspan="2" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Hours Worked</strong></td>
		<td class="purplebg" align="center" colspan="2" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">All Injury Rate</strong></td>
		<td class="purplebg" align="center" colspan="2" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Total Recordable Incident Rate</strong></td>
		<td class="purplebg" align="center" colspan="2" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Lost Time Incident Rate</strong></td>
	</tr>
	<tr>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Target</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">YTD Performance</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Target</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">YTD Performance</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Target</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">YTD Performance</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfoutput query="getOU" group="buname">
	<cfif structkeyexists(manhrstructprev,qrybuid)><cfset prevrowhr = manhrstructprev[qrybuid]><cfelse><cfset prevrowhr = 0></cfif>
	<cfif structkeyexists(manhrstruct,qrybuid)><cfset rowhr = manhrstruct[qrybuid]><cfelse><cfset rowhr = 0></cfif>
	
	<cfif structkeyexists(targetstructair,qrybuid)><cfset airgoal = targetstructair[qrybuid]><cfelse><cfset airgoal = "Not Set"></cfif>
	<cfif structkeyexists(AIRstructprev,qrybuid)><cfset useairprev = AIRstructprev[qrybuid]><cfelse><cfset useairprev = 0></cfif>
	<cfif structkeyexists(AIRstruct,qrybuid)><cfset useair = AIRstruct[qrybuid]><cfelse><cfset useair = 0></cfif>
	<cfif prevrowhr gt 0 and useairprev gt 0>
		<cfset prevairrate = (useairprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevairrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useair gt 0>
		<cfset airrate = (useair*200000)/rowhr>
	<cfelse>
		<cfset airrate = 0>
	</cfif>
	<cfset airtxt = "000000">
	<cfset airbg = "">
	<cfif airgoal neq "Not Set">
		<cfif airrate lt airgoal>
			<cfset airbg = "00b050">
			<cfset airtxt = "ffffff">
		<cfelseif airrate gte airgoal>
			<cfif airrate lt prevairrate>
				<cfset airbg = "ffc000">
			<cfelse>
				<cfif airrate eq 0 and airgoal eq 0>
					<cfset airbg = "00b050">
					<cfset airtxt = "ffffff">
				<cfelse>
					<cfset airbg = "ff0000">
					<cfset airtxt = "ffffff">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	
	<cfif structkeyexists(targetstructtrir,qrybuid)><cfset TRIRgoal = targetstructtrir[qrybuid]><cfelse><cfset TRIRgoal = "Not Set"></cfif>
	<cfif structkeyexists(TRIRstructprev,qrybuid)><cfset useTRIRprev = TRIRstructprev[qrybuid]><cfelse><cfset useTRIRprev = 0></cfif>
	<cfif structkeyexists(TRIRstruct,qrybuid)><cfset useTRIR = TRIRstruct[qrybuid]><cfelse><cfset useTRIR = 0></cfif>
	<cfif prevrowhr gt 0 and useTRIRprev gt 0>
		<cfset prevTRIRrate = (useTRIRprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevTRIRrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useTRIR gt 0>
		<cfset TRIRrate = (useTRIR*200000)/rowhr>
	<cfelse>
		<cfset TRIRrate = 0>
	</cfif>
	<cfset TRIRtxt = "000000">
	<cfset TRIRbg = "">
	<cfif TRIRgoal neq "Not Set">
		<cfif TRIRrate lt TRIRgoal>
			<cfset TRIRbg = "00b050">
			<cfset TRIRtxt = "ffffff">
		<cfelseif TRIRrate gte TRIRgoal>
			<cfif TRIRrate lt prevTRIRrate>
				<cfset TRIRbg = "ffc000">
			<cfelse>
				<cfif TRIRrate eq 0 and TRIRgoal eq 0>
					<cfset TRIRbg = "00b050">
					<cfset TRIRtxt = "ffffff">
				<cfelse>
					<cfset TRIRbg = "ff0000">
					<cfset TRIRtxt = "ffffff">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	
	<cfif structkeyexists(targetstructlti,qrybuid)><cfset LTIgoal = targetstructlti[qrybuid]><cfelse><cfset LTIgoal = "Not Set"></cfif>
	<cfif structkeyexists(LTIstructprev,qrybuid)><cfset useLTIprev = LTIstructprev[qrybuid]><cfelse><cfset useLTIprev = 0></cfif>
	<cfif structkeyexists(LTIstruct,qrybuid)><cfset useLTI = LTIstruct[qrybuid]><cfelse><cfset useLTI = 0></cfif>
	<cfif prevrowhr gt 0 and useLTIprev gt 0>
		<cfset prevLTIrate = (useLTIprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevLTIrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useLTI gt 0>
		<cfset LTIrate = (useLTI*200000)/rowhr>
	<cfelse>
		<cfset LTIrate = 0>
	</cfif>
	<cfset LTItxt = "000000">
	<cfset LTIbg = "">
	<cfif LTIgoal neq "Not Set">
		<cfif LTIrate lt LTIgoal>
			<cfset LTIbg = "00b050">
			<cfset LTItxt = "ffffff">
		<cfelseif LTIrate gte LTIgoal>
			<cfif LTIrate lt prevLTIrate>
				<cfset LTIbg = "ffc000">
			<cfelse>
				<cfif LTIrate eq 0 and LTIgoal eq 0>
					<cfset LTIbg = "00b050">
					<cfset LTItxt = "ffffff">
				<cfelse>
					<cfset LTIbg = "ff0000">
					<cfset LTItxt = "ffffff">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	
	<cfset ctr = ctr+1>
		<cfif buname neq "plc">
		<tr <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
			<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#buname#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowhr)#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#airgoal#</td>
			<td class="bodytext" align="center" <cfif trim(airbg) neq ''>bgcolor="#airbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#airtxt#;">#numberformat(airrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#TRIRgoal#</td>
			<td class="bodytext" align="center" <cfif trim(TRIRbg) neq ''>bgcolor="#TRIRbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#trirtxt#;">#numberformat(TRIRrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#LTIgoal#</td>
			<td class="bodytext" align="center" <cfif trim(LTIbg) neq ''>bgcolor="#LTIbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#ltitxt#;">#numberformat(LTIrate,"0.000")#</span></td>
		</tr>
		</cfif>
		<cfoutput>
		<cfif structkeyexists(manhrstructprev,id)><cfset prevrowhr = manhrstructprev[id]><cfelse><cfset prevrowhr = 0></cfif>
		<cfif structkeyexists(manhrstruct,id)><cfset rowhr = manhrstruct[id]><cfelse><cfset rowhr = 0></cfif>
		
		<cfif structkeyexists(targetstructair,id)><cfset airgoal = targetstructair[id]><cfelse><cfset airgoal = "Not Set"></cfif>
		<cfif structkeyexists(AIRstructprev,id)><cfset useairprev = AIRstructprev[id]><cfelse><cfset useairprev = 0></cfif>
		<cfif structkeyexists(AIRstruct,id)><cfset useair = AIRstruct[id]><cfelse><cfset useair = 0></cfif>
		<cfif prevrowhr gt 0 and useairprev gt 0>
			<cfset prevairrate = (useairprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevairrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useair gt 0>
			<cfset airrate = (useair*200000)/rowhr>
		<cfelse>
			<cfset airrate = 0>
		</cfif>
		<cfset airtxt = "000000">
		<cfset airbg = "">
		<cfif airgoal neq "Not Set">
			<cfif airrate lt airgoal>
				<cfset airbg = "00b050">
				<cfset airtxt = "ffffff">
			<cfelseif airrate gte airgoal>
				<cfif airrate lt prevairrate>
					<cfset airbg = "ffc000">
				<cfelse>
					<cfif airrate eq 0 and airgoal eq 0>
					<cfset airbg = "00b050">
					<cfset airtxt = "ffffff">
				<cfelse>
					<cfset airbg = "ff0000">
					<cfset airtxt = "ffffff">
				</cfif>
				</cfif>
			</cfif>
		</cfif>
		
		<cfif structkeyexists(targetstructtrir,id)><cfset TRIRgoal = targetstructtrir[id]><cfelse><cfset TRIRgoal = "Not Set"></cfif>
		<cfif structkeyexists(TRIRstructprev,id)><cfset useTRIRprev = TRIRstructprev[id]><cfelse><cfset useTRIRprev = 0></cfif>
		<cfif structkeyexists(TRIRstruct,id)><cfset useTRIR = TRIRstruct[id]><cfelse><cfset useTRIR = 0></cfif>
		<cfif prevrowhr gt 0 and useTRIRprev gt 0>
			<cfset prevTRIRrate = (useTRIRprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevTRIRrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useTRIR gt 0>
			<cfset TRIRrate = (useTRIR*200000)/rowhr>
		<cfelse>
			<cfset TRIRrate = 0>
		</cfif>
		<cfset TRIRtxt = "000000">
		<cfset TRIRbg = "">
		<cfif TRIRgoal neq "Not Set">
			<cfif TRIRrate lt TRIRgoal>
				<cfset TRIRbg = "00b050">
				<cfset TRIRtxt = "ffffff">
			<cfelseif TRIRrate gte TRIRgoal>
				<cfif TRIRrate lt prevTRIRrate>
					<cfset TRIRbg = "ffc000">
				<cfelse>
					<cfif TRIRrate eq 0 and TRIRgoal eq 0>
					<cfset TRIRbg = "00b050">
					<cfset TRIRtxt = "ffffff">
				<cfelse>
					<cfset TRIRbg = "ff0000">
					<cfset TRIRtxt = "ffffff">
				</cfif>
				</cfif>
			</cfif>
		</cfif>
		
	<cfif structkeyexists(targetstructlti,id)><cfset LTIgoal = targetstructlti[id]><cfelse><cfset LTIgoal = "Not Set"></cfif>
	<cfif structkeyexists(LTIstructprev,id)><cfset useLTIprev = LTIstructprev[id]><cfelse><cfset useLTIprev = 0></cfif>
	<cfif structkeyexists(LTIstruct,id)><cfset useLTI = LTIstruct[id]><cfelse><cfset useLTI = 0></cfif>
	<cfif prevrowhr gt 0 and useLTIprev gt 0>
		<cfset prevLTIrate = (useLTIprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevLTIrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useLTI gt 0>
		<cfset LTIrate = (useLTI*200000)/rowhr>
	<cfelse>
		<cfset LTIrate = 0>
	</cfif>
	<cfset LTItxt = "000000">
	<cfset LTIbg = "">
	<cfif LTIgoal neq "Not Set">
		<cfif LTIrate lt LTIgoal>
			<cfset LTIbg = "00b050">
			<cfset LTItxt = "ffffff">
		<cfelseif LTIrate gte LTIgoal>
			<cfif LTIrate lt prevLTIrate>
				<cfset LTIbg = "ffc000">
			<cfelse>
				<cfif LTIrate eq 0 and LTIgoal eq 0>
					<cfset LTIbg = "00b050">
					<cfset LTItxt = "ffffff">
				<cfelse>
					<cfset LTIbg = "ff0000">
					<cfset LTItxt = "ffffff">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
		<!--- <cfif buname neq "plc"> --->
		<cfif qrybuid eq 3173>
			<cfif id neq 3240>
		<tr <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
			<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;&nbsp;#name#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowhr)#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#airgoal#</td>
			<td class="bodytext" align="center" <cfif trim(airbg) neq ''>bgcolor="#airbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#airtxt#;">#numberformat(airrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#TRIRgoal#</td>
			<td class="bodytext" align="center" <cfif trim(TRIRbg) neq ''>bgcolor="#TRIRbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#trirtxt#;">#numberformat(TRIRrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#LTIgoal#</td>
			<td class="bodytext" align="center" <cfif trim(LTIbg) neq ''>bgcolor="#LTIbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#ltitxt#;">#numberformat(LTIrate,"0.000")#</span></td>
		</tr>
		</cfif>
		<cfelse>
		<tr <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
			<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;&nbsp;#name#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowhr)#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#airgoal#</td>
			<td class="bodytext" align="center" <cfif trim(airbg) neq ''>bgcolor="#airbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#airtxt#;">#numberformat(airrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#TRIRgoal#</td>
			<td class="bodytext" align="center" <cfif trim(TRIRbg) neq ''>bgcolor="#TRIRbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#trirtxt#;">#numberformat(TRIRrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#LTIgoal#</td>
			<td class="bodytext" align="center" <cfif trim(LTIbg) neq ''>bgcolor="#LTIbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#ltitxt#;">#numberformat(LTIrate,"0.000")#</span></td>
		</tr>
		</cfif>
		<!--- </cfif> --->
		</cfoutput>
	</cfoutput>
<cfoutput>
		<cfif structkeyexists(manhrstructprev,"Global")><cfset prevrowhr = manhrstructprev["Global"]><cfelse><cfset prevrowhr = 0></cfif>
		<cfif structkeyexists(manhrstruct,"Global")><cfset rowhr = manhrstruct["Global"]><cfelse><cfset rowhr = 0></cfif>
		
		<cfif structkeyexists(targetstructair,"Global")><cfset airgoal = targetstructair["Global"]><cfelse><cfset airgoal = "Not Set"></cfif>
		<cfif structkeyexists(AIRstructprev,"Global")><cfset useairprev = AIRstructprev["Global"]><cfelse><cfset useairprev = 0></cfif>
		<cfif structkeyexists(AIRstruct,"Global")><cfset useair = AIRstruct["Global"]><cfelse><cfset useair = 0></cfif>
		<cfif prevrowhr gt 0 and useairprev gt 0>
			<cfset prevairrate = (useairprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevairrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useair gt 0>
			<cfset airrate = (useair*200000)/rowhr>
		<cfelse>
			<cfset airrate = 0>
		</cfif>
		<cfset airtxt = "000000">
		<cfset airbg = "">
		<cfif airgoal neq "Not Set">
			<cfif airrate lt airgoal>
				<cfset airbg = "00b050">
				<cfset airtxt = "ffffff">
			<cfelseif airrate gte airgoal>
				<cfif airrate lt prevairrate>
					<cfset airbg = "ffc000">
				<cfelse>
					<cfif airrate eq 0 and airgoal eq 0>
					<cfset airbg = "00b050">
					<cfset airtxt = "ffffff">
				<cfelse>
					<cfset airbg = "ff0000">
					<cfset airtxt = "ffffff">
				</cfif>
				</cfif>
			</cfif>
		</cfif>
		
		<cfif structkeyexists(targetstructtrir,"Global")><cfset TRIRgoal = targetstructtrir["Global"]><cfelse><cfset TRIRgoal = "Not Set"></cfif>
		<cfif structkeyexists(TRIRstructprev,"Global")><cfset useTRIRprev = TRIRstructprev["Global"]><cfelse><cfset useTRIRprev = 0></cfif>
		<cfif structkeyexists(TRIRstruct,"Global")><cfset useTRIR = TRIRstruct["Global"]><cfelse><cfset useTRIR = 0></cfif>
		<cfif prevrowhr gt 0 and useTRIRprev gt 0>
			<cfset prevTRIRrate = (useTRIRprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevTRIRrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useTRIR gt 0>
			<cfset TRIRrate = (useTRIR*200000)/rowhr>
		<cfelse>
			<cfset TRIRrate = 0>
		</cfif>
		<cfset TRIRtxt = "000000">
		<cfset TRIRbg = "">
		<cfif TRIRgoal neq "Not Set">
			<cfif TRIRrate lt TRIRgoal>
				<cfset TRIRbg = "00b050">
				<cfset TRIRtxt = "ffffff">
			<cfelseif TRIRrate gte TRIRgoal>
				<cfif TRIRrate lt prevTRIRrate>
					<cfset TRIRbg = "ffc000">
				<cfelse>
					<cfif TRIRrate eq 0 and TRIRgoal eq 0>
					<cfset TRIRbg = "00b050">
					<cfset TRIRtxt = "ffffff">
				<cfelse>
					<cfset TRIRbg = "ff0000">
					<cfset TRIRtxt = "ffffff">
				</cfif>
				</cfif>
			</cfif>
		</cfif>
		
	<cfif structkeyexists(targetstructlti,"Global")><cfset LTIgoal = targetstructlti["Global"]><cfelse><cfset LTIgoal = "Not Set"></cfif>
	<cfif structkeyexists(LTIstructprev,"Global")><cfset useLTIprev = LTIstructprev["Global"]><cfelse><cfset useLTIprev = 0></cfif>
	<cfif structkeyexists(LTIstruct,"Global")><cfset useLTI = LTIstruct["Global"]><cfelse><cfset useLTI = 0></cfif>
	<cfif prevrowhr gt 0 and useLTIprev gt 0>
		<cfset prevLTIrate = (useLTIprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevLTIrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useLTI gt 0>
		<cfset LTIrate = (useLTI*200000)/rowhr>
	<cfelse>
		<cfset LTIrate = 0>
	</cfif>
	<cfset LTItxt = "000000">
	<cfset LTIbg = "">
	<cfif LTIgoal neq "Not Set">
		<cfif LTIrate lt LTIgoal>
			<cfset LTIbg = "00b050">
			<cfset LTItxt = "ffffff">
		<cfelseif LTIrate gte LTIgoal>
			<cfif LTIrate lt prevLTIrate>
				<cfset LTIbg = "ffc000">
			<cfelse>
				<cfif LTIrate eq 0 and LTIgoal eq 0>
					<cfset LTIbg = "00b050">
					<cfset LTItxt = "ffffff">
				<cfelse>
					<cfset LTIbg = "ff0000">
					<cfset LTItxt = "ffffff">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	<tr class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;">
			<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Amec Foster Wheeler Overall</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowhr)#</td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#airgoal#</td>
			<td class="bodytext" align="center" <cfif trim(airbg) neq ''>bgcolor="#airbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#airtxt#;">#numberformat(airrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#TRIRgoal#</td>
			<td class="bodytext" align="center" <cfif trim(TRIRbg) neq ''>bgcolor="#TRIRbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#trirtxt#;">#numberformat(TRIRrate,"0.00")#</span></td>
			<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#LTIgoal#</td>
			<td class="bodytext" align="center" <cfif trim(LTIbg) neq ''>bgcolor="#LTIbg#"</cfif>><span style="font-family: Segoe UI;font-size: 8pt;color:#ltitxt#;">#numberformat(LTIrate,"0.000")#</span></td>
		</tr>
</table>
<br>
<table cellpadding="3" cellspacing="1" border="0" bgcolor="000000" align="left"> 
	<tr>
		<Td class="bodytext" bgcolor="ffffff"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>Key:</strong></td>
		<td bgcolor="00b050" class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;"><span style="color:ffffff;">Target Achieved</span></td>
		<td bgcolor="ffc000" class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Better than <cfif listfindnocase(incyear,"All") eq 0>#incyear-1#<cfelse>#year(now())-1#</cfif>, Target Not Achieved</td>
		<td bgcolor="ff0000" class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;"><span style="color:ffffff;">Target Not Achieved</span></td>
	</tr>
</table>
</cfoutput>
</cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_frbpdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">