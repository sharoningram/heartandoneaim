<cfparam name="dosearch" default="">
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_incidentlistpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "IncidentWithDescription_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginTop="1" marginLeft=".5" marginRight=".5" unit="cm">
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Incidents with Description</td>
</tr>
</table>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" bgcolor="5f2468" align="center">
	<tr>
		<td class="bodyTextWhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">&nbsp;Incidents</td>
	</tr>
	<tr>
		<td bgcolor="ffffff" align="center" width="100%">
<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%"  bgcolor="5f2468" align="center">
	<tr>
		<td class="formlable" width="5%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Number</strong></td>
		<td class="formlable" width="5%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Date</strong></td>
		<cfoutput>
		<td class="formlable" width="12%" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#request.bulabellong#</strong></td>
		<td class="formlable" width="12%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>#request.oulabellong#</strong></td>
		</cfoutput>
		<td class="formlable" width="12%"   style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Project/Office</strong></td>
		<td class="formlable" width="12%"   style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Site/Office Name</strong></td>
		<td class="formlable" width="12%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Type</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;" align="center"><strong>Near Miss?</strong></td>
		<td class="formlable" width="12%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>OSHA Classification</strong></td>
		<td class="formlable" width="12%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Short Description</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Potential Rating</strong></td>
		<td class="formlable" width="12%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Assigned To</strong></td>
		<td class="formlable" width="12%"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Status</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;" align="center"><strong>Need IRP?</strong></td>
	</tr>
	<cfset statstruct = {}>
	<cfset totctr = 0>
	<cfset alreadyshown = "">
	<cfloop query="getincidentlist">
	<cfif listfindnocase(alreadyshown,irn) eq 0>
	<cfset alreadyshown = listappend(alreadyshown,irn)>
		<cfset totctr = totctr+1>
		<cfif not structkeyexists(statstruct,status)>
			<cfset statstruct[status] = 1>
		<cfelse>
			<cfset statstruct[status] = statstruct[status]+1>
		</cfif>
	</cfif>
	</cfloop>
	
	<cfset ctrr = 0>
	<cfset alreadyshown = "">
	<cfoutput query="getincidentlist">
	<cfif listfindnocase(alreadyshown,irn) eq 0>
		<cfset showrw = "yes">
		<cfset alreadyshown = listappend(alreadyshown,irn)>
	<cfelse>
		<cfset showrw = "no">
	</cfif>
	
		
	<cfif showrw eq "yes">
	
	
	
	<cfset ctrr = ctrr+1>
	<tr <cfif ctrr mod 2 is 0>bgcolor="f7f1f9"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#TrackingNum#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#dateformat(incidentDate,sysdateformat)#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#buname#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#ouname#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#Group_Name#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#SiteName#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#IncType#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" align="center">#isnearmiss#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;"><cfif isnearmiss eq "Yes"><cfelse>#Category#</cfif></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#shortdesc#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#PotentialRating#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#IncAssignedTo#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;"><cfif listfindnocase("started,initiated",status) gt 0>Raised<cfelse>#status#</cfif></td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" align="center"><cfif needirp eq 1>Yes<cfelseif needirp eq 0>No</cfif></td>
	</tr>
	</cfif>
	</cfoutput>
	
</table>
	</td></tr>
	<tr bgcolor="ffffff">
		<td  class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Count of Incidents: <cfoutput>#totctr#</cfoutput></td>
	</tr>
	</table>
	
	
	<br>
	<table cellpadding="4" cellspacing="1" border="0"  align="left">
		<cfoutput>
		<cfloop list="#structkeylist(statstruct)#" index="i">
		<tr>
		<td bgcolor="ffffff" class="bodytext" align="right" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#i#:</td>
		<td bgcolor="ffffff" class="bodytext"  style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#statstruct[i]#</td>
		</tr>
		</cfloop>
		</cfoutput>
	</table>
	</cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_incidentlistpdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">