<cfset linkurldetail = "diamondtype=&dosearch=#dosearch#&reptype=#fusebox.fuseaction#&savefrm=yes&incyear=#incyear#&invlevel=#invlevel#&bu=#bu#&ou=#ou#&INCNM=#INCNM#&businessstream=#businessstream#&projectoffice=#projectoffice#&site=#site#&frmmgdcontractor=#frmmgdcontractor#&frmjv=#frmjv#&frmsubcontractor=#frmsubcontractor#&frmclient=#frmclient#&locdetail=#locdetail#&frmcountryid=#frmcountryid#&frmincassignto=#frmincassignto#&wronly=#wronly#&primarytype=#primarytype#&secondtype=#secondtype#&hipoonly=#hipoonly#&oshaclass=#oshaclass#&oidef=#oidef#&seriousinj=#seriousinj#&secinccats=#secinccats#&eic=#eic#&damagesrc=#damagesrc#&startdate=#startdate#&enddate=#enddate#&assocg=#assocg#&charttype=#charttype#&chartvalue=#chartvalue#&potentialrating=#potentialrating#&incandor=#incandor#&droppedobj=#droppedobj#">

<cfset chrttitle = "Source of Security Threat">
<cfset qryname = "getmdsecthreat">
<cfset containername = "chartContainersecthreat">

<cfset cntlist = valuelist(getmdsecthreat.cnt)>
<cfset maxNum = ArrayMax(ListToArray(cntlist))>

<cfset charttop = 10>
<cfif maxnum lt 10>
	<cfset charttop = maxnum + (10-maxnum)>
<cfelseif maxnum eq 10>
	<cfset charttop = maxnum+10>
<cfelse>
	<cfif maxnum mod 10 eq 0>
		<cfset charttop = maxnum+10>
	<cfelse>
		<cfset chknum = right(maxnum,1)>
		<cfloop from="1" to="10" index="i">
			<cfset thenumval = chknum+i>
			
			<cfif thenumval mod 10 eq 0>
				<cfset charttop = maxnum+i>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>
</cfif>
<cfset chartlines = 4>



<table cellpadding="0" cellspacing="0"  width="50%">
	
	<tr>
		<td>		
			<cfif evaluate("#qryname#").recordcount gt 0>
			<cfswitch expression="#charttype#">
				<cfcase value="table">
				<script type="text/javascript">
function expmdsecthreatjs(exptype){

document.expmdsecthreat.diamondtype.value=exptype;
document.expmdsecthreat.submit();

}
</script>
<cfoutput>
<cfform action="#self#?fuseaction=#attributes.xfa.exportrundiamond#" method="post" name="expmdsecthreat" target="_blank">
<input type="hidden" name="diamondtype" value="">	
<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="charttype" value="#charttype#">
<input type="hidden" name="chartvalue" value="secthreat">			
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="incandor" value="#incandor#">
<input type="hidden" name="droppedobj" value="#droppedobj#">
</cfform>		
</cfoutput>		
					<table cellpadding="4" cellspacing="1"  width="100%">
						<tr>
							<td align="right"><a href="javascript:void(0);" onclick="expmdsecthreatjs('pdf');"><img src="images/pdfsm.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="expmdsecthreatjs('xcl');"><img src="images/excel.png" border="0"></a></td>
						</tr>
					</table>
					<table cellpadding="4" cellspacing="1"  width="100%" class="purplebg">
						<tr>
							<td class="purplebg" colspan="2"  align="center"><strong class="bodytextwhite"><cfoutput>#chrttitle#</cfoutput></strong></td>
						</tr>
						<!--- <tr>
							<td class="formlable" align="center"><strong >Weather</strong></td>
							<td class="formlable" align="center" align="center"><strong >Number of Incidents</strong></td>
						</tr> --->
						<cfoutput query="#qryname#">
							<tr>
								<td bgcolor="ffffff" class="bodytext"  width="80%" ><a href="index.cfm?fuseaction=#attributes.xfa.diamonddetail#&diadettype=secthreat&#linkurldetail#">#lbl#</a></td>
								<Td bgcolor="ffffff" class="bodytext" align="center">#cnt#</td>
							</tr>
						</cfoutput>
					</table>
				</cfcase>
				<cfcase value="bar">
					<cfset chartxml="<chart caption='#chrttitle#' subcaption='' xaxisname='' yaxisname='Number of Incidents' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='#chartlines#' yAxisMinValue='0' yAxisMaxValue='#charttop#'>">
					<cfoutput query="#qryname#">
						<cfset uselable = trim(lbl)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable#' value='#cnt#'  link='index.cfm?fuseaction=#attributes.xfa.diamonddetail#&diadettype=secthreat&#linkurldetail#' />">
					</cfoutput>
					<cfset chartxml= chartxml & "</chart>">
					<cfoutput>
					<div id="#containername#"></div>
					
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "bar3d",
						        "renderAt": "#containername#",
						        "width": "750",
						        "height": "500",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>
				</cfcase>
				<cfcase value="column">
					<cfset chartxml="<chart caption='#chrttitle#' subcaption='' xaxisname='' yaxisname='Number of Incidents' numberprefix=''  showsum='0' showValues='0'  exportenabled='1' exportAtClientSide='1' adjustDiv='0' numDivLines='#chartlines#' yAxisMinValue='0' yAxisMaxValue='#charttop#'>">
					<cfoutput query="#qryname#">
						<cfset uselable = trim(lbl)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable#' value='#cnt#'  link='index.cfm?fuseaction=#attributes.xfa.diamonddetail#&diadettype=secthreat&#linkurldetail#'  />">
					</cfoutput>
					<cfset chartxml= chartxml & "</chart>">
					<cfoutput>
					<div id="#containername#"></div>
					
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "column3d",
						        "renderAt": "#containername#",
						        "width": "750",
						        "height": "550",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>
				</cfcase>
				
				<cfcase value="Doughnut">
					<cfset chartxml="<chart caption='#chrttitle#' subcaption='' numberprefix=''  bgcolor='ffffff' showborder='0' use3dlighting='0' showshadow='0' enablesmartlabels='1' startingangle='310' showlabels='0' showpercentvalues='0' showlegend='1' legendshadow='0' legendborderalpha='0' decimals='0' captionfontsize='14' subcaptionfontsize='14' subcaptionfontbold='0' tooltipcolor='ffffff' tooltipborderthickness='0' tooltipbgcolor='000000' tooltipbgalpha='80' tooltipborderradius='2' tooltippadding='5' usedataplotcolorforlabels='1'  exportenabled='1' exportAtClientSide='1'>">
					<cfoutput query="#qryname#">
						<cfset uselable = trim(lbl)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable#' value='#cnt#'  link='index.cfm?fuseaction=#attributes.xfa.diamonddetail#&diadettype=secthreat&#linkurldetail#'  />">
					</cfoutput>
 					<cfset chartxml= chartxml & "</chart>">
					<cfoutput>
					<div id="#containername#"></div>
					
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "doughnut3d",
						        "renderAt": "#containername#",
						        "width": "750",
						        "height": "550",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>
				
				</cfcase>
			</cfswitch>
			<cfelse>
				<table cellpadding="4" cellspacing="1"  width="100%" class="purplebg">
					<tr>
						<td class="purplebg" colspan="2"  align="center"><strong class="bodytextwhite"><cfoutput>#chrttitle#</cfoutput></strong></td>
					</tr>
					<tr>
						<td bgcolor="ffffff" class="bodytext" align="center" width="100%" >This is no data available to run this report</td>
					</tr>
				</table>
			</cfif>
		</td>
	</tr>
</table>