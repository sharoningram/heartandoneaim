<cfparam name="dosearch" default="">
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_potratingpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset ratestruct = {}>
	<cfset greenpotratings = "A1,B1,C1,D1,A2,B2,C2">
	<cfset yellowpotratings = "E1,D2,E2,A3,B3,C3,D3">
	<cfset redpotratings = "E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
<cfset totctr = 0>
<cfset greenctr = 0>
<cfset yellowctr = 0>
<cfset redctr = 0>
<cfset needratectr = 0>
<cfloop query="getratings">
<cfset totctr = totctr+1>
	<cfif trim(PotentialRating) eq ''>
		<cfset needratectr = needratectr+1>
	<cfelseif listfindnocase(greenpotratings,PotentialRating) gt 0>
		<cfset greenctr = greenctr+1>
	<cfelseif listfindnocase(yellowpotratings,PotentialRating) gt 0>
		<cfset yellowctr = yellowctr+1>
	<cfelseif listfindnocase(redpotratings,PotentialRating) gt 0>
		<cfset redctr = redctr+1>
	</cfif>
	<cfif not structkeyexists(ratestruct,PotentialRating)>
		<cfset ratestruct[PotentialRating] = 1>
	<cfelse>
		<cfset ratestruct[PotentialRating] = ratestruct[PotentialRating] +1>
	</cfif>
</cfloop>
<!--- <cfdump var="#ratestruct#"> --->
<cfset fnamepdf = "PotentialRating_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">
<cfoutput>

		
	
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2"  style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Incident Potential Rating Matrix</td>
</tr>

</table>
<table cellpadding="4" cellspacing="1" bgcolor="##000000" width="100%" id="potentialtable" align="center">
					
					<tr style="height:37px;">
						<td rowspan="8" width="1%" align="center" bgcolor="000000"><img src="images/consequences.png" border="0"></td>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf"><strong class="bodyText"  style="font-family: Segoe UI;font-size: 9pt;color: 000000;">When using this matrix consider the potential outcome of the incident not the actual outcome<br>*IP = intellectual property</strong></td>
						<td width="25%" align="center"  colspan="5" bgcolor="ffffff"><strong class="bodyText"  style="font-family: Segoe UI;font-size: 9pt;color: 000000;">No. of People</strong></td>
						
					</tr>
					<tr style="height:37px;" bgcolor="ffffff">
						
						<td  align="center"  ></td>
						<td   align="center"  ><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Injury/Health</strong></td>
						<td  align="center"  ><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Environmental</strong></td>
						<td  align="center"  ><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Damage</strong></td>
						<td   align="center"  ><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Security</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">0</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">1</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">1-2</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">2-10</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">10+</strong></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center" bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">1</strong></td>
						<td align="center" class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">First aid/health effect</td>
						<td align="center" class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Minimal reversible environmental impact</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Minor loss/damage/business impact (<10K)</td>
						<td align="center" class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Minor crime, no impact on business operations or reputation</td>
					<td align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"A1")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">A1<br>#ratestruct["A1"]#</strong></cfif></td>
					<td align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"B1")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">B1<br>#ratestruct["B1"]#</strong></cfif></td>
					<td  align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"C1")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">C1<br>#ratestruct["C1"]#</strong></cfif></td>
					<td align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"D1")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">D1<br>#ratestruct["D1"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"E1")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">E1<br>#ratestruct["E1"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td  align="center" bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">2</strong></td>
						<td align="center" class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Medical treatment/ restricted work/ moderate health effect</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Minor pollution with short term impact (1 month)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Moderate loss/ damage/ business impact (10-100K)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Theft/vandalism/loss of non-IP* information. No lasting impact on business operations or reputation</td>
					<td  align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"A2")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">A2<br>#ratestruct["A2"]#</strong></cfif></td>
					<td  align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"B2")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">B2<br>#ratestruct["B2"]#</strong></cfif></td>
					<td align="center"  bgcolor="##00b050"><cfif structkeyexists(ratestruct,"C2")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">C2<br>#ratestruct["C2"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"D2")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">D2<br>#ratestruct["D2"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"E2")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">E2<br>#ratestruct["E2"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">3</strong></td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Lost time injury/ significant health effect</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Moderate pollution with medium term localised impact (1 year)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Significant loss/damage/ business impact reportable event within local legislation (100K-1M)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Crime (threat, intimidation, sabotage) with impact on people or loss of confidential IP containing material</td>
					<td  align="center" bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"A3")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">A3<br>#ratestruct["A3"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"B3")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">B3<br>#ratestruct["B3"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"C3")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">C3<br>#ratestruct["C3"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"D3")><strong style="font-size:8pt;color:000000;font-family:Segoe UI;">D3<br>#ratestruct["D3"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"E3")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">E3<br>#ratestruct["E3"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">4</strong></td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Serious injury/ severe health effect/ long term disability</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Severe pollution with long term localised impact (+1 year)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Severe loss/damage/ business impact reportable event within local legislation (1-10M)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Serious and deliberate criminal attack against people, disruptive natural event or loss of sensitive IP material</td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"A4")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">A4<br>#ratestruct["A4"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"B4")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">B4<br>#ratestruct["B4"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"C4")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">C4<br>#ratestruct["C4"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"D4")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">D4<br>#ratestruct["D4"]#</strong></cfif></td>
					<td align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"E4")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">E4<br>#ratestruct["E4"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">5</strong></td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Fatality</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Major pollution with long term environmental change</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Major loss/damage/ reportable event within local legislation business impact (10M+)</td>
						<td align="center"  class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">Sustained, serious attack/loss of confidential IP or natural disaster requiring formal emergency response</td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"A5")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">A5<br>#ratestruct["A5"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"B5")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">B5<br>#ratestruct["B5"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"C5")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">C5<br>#ratestruct["C5"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"D5")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">D5<br>#ratestruct["D5"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"E5")><strong style="font-size:8pt;color:ffffff;font-family:Segoe UI;">E5<br>#ratestruct["E5"]#</strong></cfif></td>
					</tr>
					<tr>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf" class="bodyTextsm" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">High Potential Incidents (Red) = LEVEL 2; Medium Potential Incidents (Amber) = LEVEL 1 or 2 (at HSSE Manager's discretion); Low Potential Incidents (Green) = LEVEL 1</td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">A</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">B</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">C</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">D</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">E</strong></td>
					</tr>
				</table>	
				<br>
<table cellpadding="4" cellspacing="1" bgcolor="##000000" align="center">

	<tr bgcolor="ffffff">
		<Td></td>
		<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Number of Incidents</td>
		<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Percentage</td>
	</tr>
	<tr bgcolor="ffffff">
		<Td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Total number of incidents</td>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;" align="center">#totctr#</td>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;" align="center"></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">High Potential</td>
		<td class="bodytext" bgcolor="##ff0000" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#redctr#</td>
		<td class="bodytext" bgcolor="##ff0000" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;"><cfif redctr gt 0 and totctr gt 0>#numberformat(redctr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Medium Potential</td>
		<td class="bodytext" bgcolor="##ffc000" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#yellowctr#</td>
		<td class="bodytext" bgcolor="##ffc000" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;"><cfif yellowctr gt 0 and totctr gt 0>#numberformat(yellowctr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Low Potential</td>
		<td class="bodytext" bgcolor="##00b050" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#greenctr#</td>
		<td class="bodytext" bgcolor="##00b050" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;"><cfif greenctr gt 0 and totctr gt 0>#numberformat(greenctr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Potential rating to be determined</td>
		<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#needratectr#</td>
		<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;"><cfif needratectr gt 0 and totctr gt 0>#numberformat(needratectr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
</table>
</cfoutput>

<cfif isdefined("getincidentratelist.recordcount")><br><br>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" bgcolor="5f2468"  width="100%" align="center">
	<tr>
		<td class="bodyTextWhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;"><cfif ratelev eq "TBD">Potential Rating to be Determined<cfelse><cfoutput>#ratelev#</cfoutput> Potential Incidents</cfif></td>
	</tr>
	<tr>
		<td>
<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
	<tr>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Number</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Date</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong><cfoutput>#request.bulabellong#</cfoutput></strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong><cfoutput>#request.oulabellong#</cfoutput></strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Project/Office</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Incident Type</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>OSHA Classification</strong></td>
		<td class="formlable"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"><strong>Potential Rating</strong></td>
		
	</tr>
	<cfoutput query="getincidentratelist">
	<cfset oktolink = "no">
	<cfif accessglist eq "all">
		<cfset oktolink = "yes">
	<cfelse>
		<cfif listfind(accessglist,group_number) gt 0>
			<cfset oktolink = "yes">
		<cfelse>
			<cfif createdbyEmail eq request.userlogin>
				<cfset oktolink = "yes">
			</cfif>
		</cfif>
	</cfif>
	<tr <cfif currentrow mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;background-color:f7f1f9;"</cfif>>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#TrackingNum#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#dateformat(incidentDate,sysdateformat)#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#buname#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#ouname#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#Group_Name#</td>
		
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#IncType#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#Category#</td>
		<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;">#PotentialRating#</td>
		
	</tr>
	</cfoutput>
</table>
	</td></tr></table>
	
</cfif>
	</cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_potratingpdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">