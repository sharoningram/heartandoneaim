
<cfparam name="dosearch" default="">
<cfset ratestruct = {}>
	<cfset greenpotratings = "A1,B1,C1,D1,A2,B2,C2">
	<cfset yellowpotratings = "E1,D2,E2,A3,B3,C3,D3">
	<cfset redpotratings = "E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
<cfset totctr = 0>
<cfset greenctr = 0>
<cfset yellowctr = 0>
<cfset redctr = 0>
<cfset needratectr = 0>
<cfloop query="getratings">
<cfset totctr = totctr+1>
	<cfif trim(PotentialRating) eq ''>
		<cfset needratectr = needratectr+1>
	<cfelseif listfindnocase(greenpotratings,PotentialRating) gt 0>
		<cfset greenctr = greenctr+1>
	<cfelseif listfindnocase(yellowpotratings,PotentialRating) gt 0>
		<cfset yellowctr = yellowctr+1>
	<cfelseif listfindnocase(redpotratings,PotentialRating) gt 0>
		<cfset redctr = redctr+1>
	</cfif>
	<cfif not structkeyexists(ratestruct,PotentialRating)>
		<cfset ratestruct[PotentialRating] = 1>
	<cfelse>
		<cfset ratestruct[PotentialRating] = ratestruct[PotentialRating] +1>
	</cfif>
</cfloop>
<!--- <cfdump var="#ratestruct#"> --->
<cfoutput>
<cfif fusebox.fuseaction neq "printratetriangle">
<form action="#self#?fuseaction=#attributes.xfa.printratetriangle#" name="printfrm" method="post" target="_blank">
		
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.printratetrianglepdf#" name="printratetrianglepdf" method="post" target="_blank">
		
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	</cfif>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Incident Potential Rating Triangle</td>
</tr>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right"><cfif fusebox.fuseaction neq "printratetriangle"><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="document.printratetrianglepdf.submit();"><img src="images/pdfsm.png" border="0"></a></cfif></td>
</tr>
</table>

<cfswitch expression="#len(redctr)#">
	<cfcase value="1">
		<cfset rstart = 210>
		<cfset rstarth = 70>
	</cfcase>
	<cfcase value="2">
		<cfset rstart = 202>
		<cfset rstarth = 70>
	</cfcase>
	<cfcase value="3">
		<cfset rstart = 196>
		<cfset rstarth = 70>
	</cfcase>
	<cfcase value="4">
		<cfset rstart = 191>
		<cfset rstarth = 83>
	</cfcase>
	<cfcase value="5">
		<cfset rstart = 184>
		<cfset rstarth = 83>
	</cfcase>
</cfswitch>
<cfswitch expression="#len(yellowctr)#">
	<cfcase value="1">
		<cfset ystart = 210>
	</cfcase>
	<cfcase value="2">
		<cfset ystart = 202>
	</cfcase>
	<cfcase value="3">
		<cfset ystart = 196>
	</cfcase>
	<cfcase value="4">
		<cfset ystart = 191>
	</cfcase>
	<cfcase value="5">
		<cfset ystart = 184>
	</cfcase>
</cfswitch>
<cfswitch expression="#len(greenctr)#">
	<cfcase value="1">
		<cfset gstart = 210>
	</cfcase>
	<cfcase value="2">
		<cfset gstart = 202>
	</cfcase>
	<cfcase value="3">
		<cfset gstart = 196>
	</cfcase>
	<cfcase value="4">
		<cfset gstart = 191>
	</cfcase>
	<cfcase value="5">
		<cfset gstart = 184>
	</cfcase>
</cfswitch>
<cfset filedirectory= trim(replacenocase(getcurrenttemplatepath(), 
						"reports\displays\dsp_ratetriangle.cfm", "images\"))>
<cfimage action="read" source="#filedirectory#pyramid.jpg" name="chartimg">
<cfset ImageSetAntialiasing(chartimg,"on")>
<cfset ImageSetDrawingColor(chartimg,"white")> 
<cfset attr = StructNew()> 
<!--- <cfset attr.style="bold"> --->
<cfset attr.size=22> 
<cfset attr.font="Segoe UI"> 

<cfset attr2 = StructNew()> 
<!--- <cfset attr2.style="bold"> --->
<cfset attr2.size=14> 
<cfset attr2.font="Segoe UI"> 
<cfset ImageDrawText(chartimg,redctr,rstart,rstarth,attr)>
<cfif redctr gt 0 and totctr gt 0>
	<cfset redperc = redctr/totctr>
<cfelse>
	<cfset redperc = 0>
</cfif>
<cfset ImageDrawText(chartimg,"#numberformat(redperc,"0.0000")*100#%",248,55,attr2)> 
<cfset ImageSetDrawingColor(chartimg,"black")>
<cfif yellowctr gt 0 and totctr gt 0>
	<cfset yellperc = yellowctr/totctr>
<cfelse>
	<cfset yellperc = 0>
</cfif>
<cfif greenctr gt 0 and totctr gt 0>
	<cfset greenperc = greenctr/totctr>
<cfelse>
	<cfset greenperc = 0>
</cfif>


<cfset ImageDrawText(chartimg,yellowctr,ystart,150,attr)>
<cfset ImageDrawText(chartimg,"#numberformat(yellperc,"0.0000")*100#%",308,137,attr2)> 

<cfset ImageDrawText(chartimg,greenctr,gstart,233,attr)>
<cfset ImageDrawText(chartimg,"#numberformat(greenperc,"0.0000")*100#%",369,228,attr2)> 



<!--- <cfset ImageSetDrawingStroke(chartimg,attr)>
<cfset ImageDrawPoint(chartimg,210,45)>
 --->
<!---
<cfset innerattr = StructNew()>
<cfset innerattr.width = 6>
<cfset innerattr.endcaps = "round">
<cfset ImageSetDrawingStroke(chartimg,innerattr)>
<cfset ImageDrawPoint(chartimg,275,300)> --->



<table cellpadding="4" cellspacing="1" bgcolor="##ffffff">
	<tr>
		<td><cfimage source="#chartimg#" action="writeToBrowser"></td>
	</tr>
</table>
				<br>
<table cellpadding="4" cellspacing="1" bgcolor="##000000">

	<tr bgcolor="ffffff">
		<Td class="bodytext">Total number of incidents</td>
		<td class="bodytext" align="center">#totctr#</td>
		
	</tr>	
	<!--- <tr bgcolor="ffffff">
		<Td class="bodytext">High Potential</td>
		<td class="bodytext" bgcolor="##ff0000" align="center">#redctr#</td>
		<td class="bodytext" bgcolor="##ff0000" align="center">#numberformat(redctr/totctr,"0.00")*100#%</td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext">Medium Potential</td>
		<td class="bodytext" bgcolor="##ffc000" align="center">#yellowctr#</td>
		<td class="bodytext" bgcolor="##ffc000" align="center">#numberformat(yellowctr/totctr,"0.00")*100#%</td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext">Low Potential</td>
		<td class="bodytext" bgcolor="##00b050" align="center">#greenctr#</td>
		<td class="bodytext" bgcolor="##00b050" align="center">#numberformat(greenctr/totctr,"0.00")*100#%</td>
	</tr>	 --->
	<tr bgcolor="ffffff">
		<Td class="bodytext">Incidents with Potential Rating to be determined</td>
		<td class="bodytext" align="center">#needratectr#</td>
		<!--- <td class="bodytext" align="center">#numberformat(needratectr/totctr,"0.00")*100#%</td> --->
	</tr>	
</table>
</cfoutput>