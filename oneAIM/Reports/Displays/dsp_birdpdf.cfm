<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_birdpdf.cfm", "pdfgen"))>
<cfset fnamepdf = "BIRD_#timeformat(now(),'hhnnssl')#.pdf">
<cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfparam name="dosearch" default="">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes">
<cfoutput>


<cfset FATctr = 0>
<cfset LTIctr = 0>
<cfset RWCctr = 0>
<cfset MTCctr = 0>
<cfset FActr = 0>
<cfset NTctr = 0>
<cfset NMctr = 0>

<cfset envctr = getenviro.recordcount>
<cfset envnmctr = 0>
<cfloop query="getenviro">
	<cfif isnearmiss eq "yes">
		<cfset envnmctr = envnmctr+1>
	</cfif>
</cfloop>
<cfset adctr = getad.recordcount>
<cfset adnmctr = 0>
<cfloop query="getad">
	<cfif isnearmiss eq "yes">
		<cfset adnmctr = adnmctr+1>
	</cfif>
</cfloop>
<cfset secctr = getsec.recordcount>

<cfset FATctr = getfatal.recordcount>
<cfloop query="getincoi">
	<cfif CatID eq 1>
		<cfset FActr = FActr + 1>
	</cfif>
	<cfif CatID eq 2>
		<cfif isnearmiss neq "yes">
			<cfset LTIctr = LTIctr + 1>
		</cfif>
	</cfif>
	<cfif CatID eq 3>
		<cfif isnearmiss neq "yes">
			<cfset MTCctr = MTCctr + 1>
		</cfif>
	</cfif>
	<cfif CatID eq 4>
		<cfif isnearmiss neq "yes">
			<cfset RWCctr = RWCctr + 1>
		</cfif>
	</cfif>
	<cfif CatID eq 5>
		<cfif isnearmiss neq "yes">
			<cfset NTctr = NTctr + 1>
		</cfif>
	</cfif>
	<cfif isnearmiss eq "yes">
		<cfset NMctr = NMctr+1>
	</cfif>
</cfloop>
<cfset filedirectory= trim(replacenocase(getcurrenttemplatepath(), 
						"reports\displays\dsp_birdpdf.cfm", "images\"))>
<cfimage action="read" source="#filedirectory#birdtriangle.png" name="chartimg">
<cfset ImageSetAntialiasing(chartimg,"on")>
<cfset ImageSetDrawingColor(chartimg,"Black")> 
<cfset attr = StructNew()> 

<cfset attr.size=16> 
<cfset attr.font="Segoe UI"> 


<cfswitch expression="#len(NMctr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,NMctr,nmstart,385,attr)>

<cfset ImageSetDrawingColor(chartimg,"dfdfdf")>
<cfswitch expression="#len(NTctr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,NTctr,nmstart,327,attr)>

<cfset ImageSetDrawingColor(chartimg,"black")>
<cfswitch expression="#len(FActr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,FActr,nmstart,269,attr)>

<cfset ImageSetDrawingColor(chartimg,"black")>
<cfswitch expression="#len(MTCctr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,MTCctr,nmstart,211,attr)>


<cfset ImageSetDrawingColor(chartimg,"black")>
<cfswitch expression="#len(RWCctr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,RWCctr,nmstart,153,attr)>

<cfset ImageSetDrawingColor(chartimg,"black")>
<cfswitch expression="#len(LTIctr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,LTIctr,nmstart,95,attr)>

<cfset ImageSetDrawingColor(chartimg,"black")>
<cfswitch expression="#len(FATctr)#">
	<cfcase value="1">
		<cfset nmstart = 321>
	</cfcase>
	<cfcase value="2">
		<cfset nmstart = 316>
	</cfcase>
	<cfcase value="3">
		<cfset nmstart = 312>
	</cfcase>
	<cfcase value="4">
		<cfset nmstart = 307>
	</cfcase>
	<cfcase value="5">
		<cfset nmstart = 303>
	</cfcase>
	<cfdefaultcase>
		<cfset nmstart = 298>
	</cfdefaultcase>
</cfswitch>
<cfset ImageDrawText(chartimg,FATctr,nmstart,40,attr)>
<cfset attr2 = StructNew()> 

<cfset attr2.size=15> 
<cfset attr2.style="bold"> 
<cfset attr2.font="Segoe UI"> 
<cfset ImageDrawLine(chartimg,347,32,357,32)>
<cfset ImageDrawText(chartimg,"Fatality",361,37,attr2)>

<cfset ImageDrawLine(chartimg,380,88,390,88)>
<cfset ImageDrawText(chartimg,"LTI",394,93,attr2)>

<cfset ImageDrawLine(chartimg,411,144,421,144)>
<cfset ImageDrawText(chartimg,"RWC",425,149,attr2)>

<cfset ImageDrawLine(chartimg,447,205,457,205)>
<cfset ImageDrawText(chartimg,"MTC",461,211,attr2)>

<cfset ImageDrawLine(chartimg,480,263,490,263)>
<cfset ImageDrawText(chartimg,"First Aid",494,268,attr2)>

<cfset ImageDrawLine(chartimg,513,321,523,321)>
<cfset ImageDrawText(chartimg,"No Treatment",527,327,attr2)>

<cfset ImageDrawLine(chartimg,547,380,557,380)>
<cfset ImageDrawText(chartimg,"Injury/OI",564,378,attr2)>
<cfset ImageDrawText(chartimg,"Near Miss",561,393,attr2)>
<style>
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: ##FFFFFF;
	font-style: normal;
}
</style>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">BIRD Triangle</td>
</tr>
</table>
<table cellpadding="4" cellspacing="1" bgcolor="##ffffff">
	<tr>
		<td><cfimage source="#chartimg#" action="writeToBrowser"></td>
	</tr>
	<tr>
		<td align="center">
			<cfoutput>
			<table width="474" cellpadding="0" bgcolor="00ac58" cellpadding="2">
				<tr>
					<td class="bodytextwhite" width="95%">Environmental Incidents</td>
					<td class="bodytextwhite" align="center"><cfif envctr gt 0>#envctr-envnmctr#<cfelse>#envctr#</cfif></td>
				</tr>
				<tr>
					<td class="bodytextwhite" width="95%">Environmental Near Miss</td>
					<td class="bodytextwhite" align="center">#envnmctr#</td>
				</tr>
			</table>
			</cfoutput>
		</td>
	</tr>
	<tr>
		<td align="center">
			<cfoutput>
			<table width="474" cellpadding="0" bgcolor="00b3bd" cellpadding="2">
				<tr>
					<td class="bodytextwhite" width="95%">Asset Damage Incidents</td>
					<td class="bodytextwhite" align="center"><cfif envctr gt 0>#adctr-adnmctr#<cfelse>#adctr#</cfif></td>
				</tr>
				<tr>
					<td class="bodytextwhite" width="95%">Asset Damage Near Miss</td>
					<td class="bodytextwhite" align="center">#adnmctr#</td>
				</tr>
			</table>
			</cfoutput>
		</td>
	</tr>
	<tr>
		<td align="center">
			<cfoutput>
			<table width="474" cellpadding="0" bgcolor="50388b" cellpadding="2">
				<tr>
					<td class="bodytextwhite" width="95%">Security Incidents</td>
					<td class="bodytextwhite" align="center">#secctr#</td>
				</tr>
				
			</table>
			</cfoutput>
		</td>
	</tr>
</table>

</cfoutput>





</cfdocument>
<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">