
<cfparam name="dosearch" default="">
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_daysawaypdf.cfm", "pdfgen"))>
<cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "DaysAway_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes">
<cfoutput>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Days Away From Work</td>
</tr>

</table>
<cfset injlti = 0>
<cfset inj4week = 0>
<cfset inj180 = 0>
<cfset injnumdays = 0>
<cfset oilti = 0>
<cfset oi4week = 0>
<cfset oi180 = 0>
<cfset oinumdays = 0>

<cfloop query="getdaysaway">
	<cfif primarytype eq 1>
		<cfset injlti = injlti+1>
			<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset injnumdays = injnumdays + 180>
				</cfif>
				
			</cfif>
	<cfelseif PrimaryType eq 5 or SecondaryType eq 5>
		<cfset oilti = oilti+1>
		<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				
				
				
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset oinumdays = oinumdays + 180>
				</cfif>
			</cfif>
	<cfelseif SecondaryType eq 1>
		<cfset injlti = injlti+1>
			<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset injnumdays = injnumdays + 180>
				</cfif>
				
			</cfif>
	<cfelseif SecondaryType eq 5>
		<cfset oilti = oilti+1>
		<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				
				
				
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset oinumdays = oinumdays + 180>
				</cfif>
			</cfif>
	</cfif>

</cfloop>




<table cellpadding="4" cellspacing="1" bgcolor="000000" width="99%" align="center">
	<tr>
		<td class="purplebg"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Incident Type</strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Number of LTIs</strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Cases Over Four Weeks</strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Cases at 180 Days</strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Number of Days Lost</strong></td>
	</tr>
	<tr>
		<td class="purplebg"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Injury</strong></td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#injlti#</td>
		<Td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#inj4week#</td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#inj180#</td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#injnumdays#</td>
	</tr>
	<tr>
		<td class="purplebg"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Occupational Illness</strong></td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#oilti#</td>
		<Td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#oi4week#</td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#oi180#</td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#oinumdays#</td>
	</tr>
	<tr>
		<td class="purplebg"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Total</strong></td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#injlti+oilti#</td>
		<Td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#inj4week+oi4week#</td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#inj180+oi180#</td>
		<td bgcolor="ffffff" class="bodytext" align="center" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#injnumdays+oinumdays#</td>
	</tr>
</table>

</cfoutput>
</cfdocument>
			 
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_daysawaypdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">