<cfparam name="dosearch" default="">

<cfoutput>
<cfif fusebox.fuseaction neq "printincidentman">
<form action="#self#?fuseaction=#attributes.xfa.printincidentman#" name="printfrm" method="post" target="_blank">
	<input type="hidden" name="searchname" value="#searchname#">	
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.printincidentmanpdf#" name="printincidentmanpdf" method="post" target="_blank">
	<input type="hidden" name="searchname" value="#searchname#">	
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	</cfif>
	

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Incident Man</td>
</tr>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right"><cfif fusebox.fuseaction neq "printincidentman"><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="document.printincidentmanpdf.submit();"><img src="images/pdfsm.png" border="0"></a></cfif></td>
</tr>
</table>

<cfset filedirectory= trim(replacenocase(getcurrenttemplatepath(), 
						"reports\displays\dsp_incman.cfm", "images\"))>
<cfimage action="read" source="#filedirectory#incidentMan.jpg" name="chartimg">

<cfset ImageSetAntialiasing(chartimg,"on")>
<cfset attr = StructNew()> 

<cfset attr.size=16> 
<cfset attr.style="bold"> 
<cfset attr.font="Segoe UI"> 

	<cfloop query="getincman">
		<cfswitch expression="#lbl#">
			<cfcase value="ankle">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",130,570,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="arm/wrist">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",320,274,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="back">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",85,140,attr)>
				<cfset ImageDrawLine(chartimg,110,144,165,160)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="chest">
				<cfif cnt gt 0>
					
					<cfset stp = 215>
					<cfif len(cnt) eq 1>
						<cfset stp = 215>
					<cfelseif len(cnt) eq 2>
						<cfset stp = 213>
					<cfelseif len(cnt) eq 3>
						<cfset stp = 210>
					<cfelseif len(cnt) eq 4>
						<cfset stp = 206>
					<cfelseif len(cnt) eq 5>
						<cfset stp = 200>
					<cfelseif len(cnt) eq 6>
						<cfset stp = 197>
					<cfelseif len(cnt) gte 7>
						<cfset stp = 193>
					</cfif>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",stp,170,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
				
			</cfcase>
			<cfcase value="eye">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",290,50,attr)>
					<cfset ImageDrawLine(chartimg,260,70,288,45)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
				
			</cfcase>
			<cfcase value="face">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",140,50,attr)>
				<cfset ImageDrawLine(chartimg,160,55,244,80)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="fingers/thumb">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",40,396,attr)>
				<!--- 	<cfset ImageDrawLine(chartimg,79,395,150,360)> --->
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="foot/toes">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",285,624,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="hand">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",375,326,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="head">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",218,28,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="leg">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")>
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",305,444,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="neck">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",161,117,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="pelvis">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",65,250,attr)>
					<cfset ImageDrawLine(chartimg,90,255,208,320)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="shoulder">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",320,145,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="abdomen">
				<cfif cnt gt 0>
					<cfset stp = 242>
					<cfif len(cnt) eq 1>
						<cfset stp = 242>
					<cfelseif len(cnt) eq 2>
						<cfset stp = 237>
					<cfelseif len(cnt) eq 3>
						<cfset stp = 232>
					<cfelseif len(cnt) eq 4>
						<cfset stp = 227>
					<cfelseif len(cnt) eq 5>
						<cfset stp = 223>
					<cfelseif len(cnt) eq 6>
						<cfset stp = 219>
					<cfelseif len(cnt) gte 7>
						<cfset stp = 214>
					</cfif>
				
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl#",215,286,attr)>
					<cfset ImageDrawText(chartimg,"(#cnt#)",stp,301,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="Internal Organs">
				<cfif cnt gt 0><!--- #cnt# --->
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"Internal",222,220,attr)>
					<cfset ImageDrawText(chartimg,"Organs",224,235,attr)>
					<cfset stp = 242>
					<cfif len(cnt) eq 1>
						<cfset stp = 242>
					<cfelseif len(cnt) eq 2>
						<cfset stp = 237>
					<cfelseif len(cnt) eq 3>
						<cfset stp = 232>
					<cfelseif len(cnt) eq 4>
						<cfset stp = 227>
					<cfelseif len(cnt) eq 5>
						<cfset stp = 223>
					<cfelseif len(cnt) eq 6>
						<cfset stp = 219>
					<cfelseif len(cnt) gte 7>
						<cfset stp = 214>
					</cfif>
					<cfset ImageDrawText(chartimg,"(#cnt#)", stp,252,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="Groin">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",70,310,attr)>
					<cfset ImageDrawLine(chartimg,100,315,235,365)>
					
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="Mouth/Teeth">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",307,95,attr)>
					<cfset ImageDrawLine(chartimg,258,100,303,92)>
					
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			
		</cfswitch>
	</cfloop>



<!--- <cfset ImageSetAntialiasing(chartimg,"on")>
<cfset ImageSetDrawingColor(chartimg,"white")> 
<cfset attr = StructNew()> 
<!--- <cfset attr.style="bold"> --->
<cfset attr.size=22> 
<cfset attr.font="Segoe UI"> 

<cfset attr2 = StructNew()> 
<!--- <cfset attr2.style="bold"> --->
<cfset attr2.size=14> 
<cfset attr2.font="Segoe UI"> 
<cfset ImageDrawText(chartimg,redctr,rstart,rstarth,attr)>
<cfif redctr gt 0 and totctr gt 0>
	<cfset redperc = redctr/totctr>
<cfelse>
	<cfset redperc = 0>
</cfif>
<cfset ImageDrawText(chartimg,"#numberformat(redperc,"0.0000")*100#%",248,55,attr2)> 
<cfset ImageSetDrawingColor(chartimg,"black")>
<cfif yellowctr gt 0 and totctr gt 0>
	<cfset yellperc = yellowctr/totctr>
<cfelse>
	<cfset yellperc = 0>
</cfif>
<cfif greenctr gt 0 and totctr gt 0>
	<cfset greenperc = greenctr/totctr>
<cfelse>
	<cfset greenperc = 0>
</cfif>


<cfset ImageDrawText(chartimg,yellowctr,ystart,150,attr)>
<cfset ImageDrawText(chartimg,"#numberformat(yellperc,"0.0000")*100#%",308,137,attr2)> 

<cfset ImageDrawText(chartimg,greenctr,gstart,233,attr)>
<cfset ImageDrawText(chartimg,"#numberformat(greenperc,"0.0000")*100#%",369,228,attr2)>  --->




<table cellpadding="4" cellspacing="1" bgcolor="##ffffff">
	<tr>
		<td><cfimage source="#chartimg#" action="writeToBrowser"></td>
	</tr>
</table>
				
</cfoutput>