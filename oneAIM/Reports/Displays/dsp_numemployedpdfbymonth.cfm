
<cfparam name="lookupyear" default="#year(now())#">
<cfparam name="openbu" default="">
<cfparam name="openou" default="">
<cfparam name="dosearch" default="">

<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_numemployedpdfbymonth.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfset fnamepdf = "NumberEmployed_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="portrait" marginright="0.5" marginleft="0.5" >

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Numbers Employed Report - By Month</td>
</tr>


</table>


<table cellpadding="2" cellspacing="1" bgcolor="000000" width="100%" border="0">
	<tr>
		
		<td class="purplebg" align="center" width="25%" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">&nbsp;</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">AFW Employees</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Managed Contractor</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Subcontractor</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Joint Venture</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Total</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfset globalAFW = 0>
	<cfset globalSUB = 0>
	<cfset globalMC = 0>
	<cfset globalJV = 0>
	
	<cfoutput query="getnumemplyed" group="yr">
	<cfset yrctr = 0>
	<cfset yrAFW = 0>
	<cfset yrSUB = 0>
	<cfset yrMC = 0>
	<cfset yrJV = 0>
	
	<cfoutput group="mon">
	<cfset rowtot = 0>
		
	<cfif trim(afw) neq ''>
		<cfset afwnum = afw>
	<cfelse>
		<cfset afwnum = 0>
	</cfif>
	<cfif trim(sub) neq ''>
		<cfset subnum = sub>
	<cfelse>
		<cfset subnum = 0>
	</cfif>
	<cfif trim(jv) neq ''>
		<cfset jvnum = jv>
	<cfelse>
		<cfset jvnum = 0>
	</cfif>
	<cfif trim(mc) neq ''>
		<cfset mcnum = mc>
	<cfelse>
		<cfset mcnum = 0>
	</cfif>
	
	<cfset globalAFW = globalAFW+afwnum>
	<cfset globalSUB = globalSUB+subnum>
	<cfset globalMC = globalMC+mcnum>
	<cfset globalJV = globalJV+jvnum>
	
	<cfset yrAFW = yrAFW+afwnum>
	<cfset yrSUB = yrSUB+subnum>
	<cfset yrMC = yrMC+mcnum>
	<cfset yrJV = yrJV+jvnum>
	
	<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
	<cfset ctr = ctr+1>
	<cfset yrctr = yrctr+1>
	<tr  <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
		
		<td class="bodytext" width="25%" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#monthasstring(mon)#</td>
		
		<td class="bodytext" align="center" width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(afwnum)#</td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mcnum)#</td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subnum)#</td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvnum)#</td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
	
	
	
	</cfoutput>
	<cfif incyear eq "All">
	<cfset yrtothrs = yrafw+yrmc+yrsub+yrjv>
	<tr  <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
		
		<td class="bodytext" width="25%" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#yr# Average</strong></td>
		
		<td class="bodytext" align="center" width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif yrafw gt 0 and yrctr gt 0>#numberformat(yrafw/yrctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif yrmc gt 0 and yrctr gt 0>#numberformat(yrmc/yrctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif yrsub gt 0 and yrctr gt 0>#numberformat(yrsub/yrctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif yrjv gt 0 and yrctr gt 0>#numberformat(yrjv/yrctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  width="15%" style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif yrtothrs gt 0 and yrctr gt 0>#numberformat((yrtothrs)/yrctr)#<cfelse>0</cfif></strong></td>
	</tr>
	</cfif>
	</cfoutput>
	<cfset rowtot = 0>
	
	<cfoutput>
	<cfset tothrs = globalAFW+globalMC+globalSUB+globalJV>
	<tr class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;">
		<td class="bodytext"   nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>Average Number Employed</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif globalAFW gt 0 and ctr gt 0>#numberformat(globalAFW/ctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif globalMC gt 0 and ctr gt 0>#numberformat(globalMC/ctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif globalSUB gt 0 and ctr gt 0>#numberformat(globalSUB/ctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif globalJV gt 0 and ctr gt 0>#numberformat(globalJV/ctr)#<cfelse>0</cfif></strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong><cfif tothrs gt 0 and ctr gt 0>#numberformat((tothrs)/ctr)#<cfelse>0</cfif></strong></td>
	</tr>
	</cfoutput>
</table>
</cfdocument>
			
			
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">