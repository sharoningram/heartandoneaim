<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_spillspdf.cfm", "pdfgen"))>
<cfset fnamepdf = "EnvSpills_#timeformat(now(),'hhnnssl')#.pdf">
<cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfparam name="dosearch" default="">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes">
<style>
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #FFFFFF;
	font-style: normal;
}
.purplebg {
background: #5f2468;

}
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f5f5f;
	font-style: normal;
	}
.formlable{
background-color:#f7f1f9;
font-family: Segoe UI;
	font-size: 10pt;

	color: #5f5f5f;
	font-style: normal;
}
</style>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Environmental Spills</td>
</tr>
</table>


<cfset spillchrtstruct = {}>
<cfloop query="getspills">
	<cfif not structkeyexists(spillchrtstruct,SubstanceType)>
		<cfset spillchrtstruct[SubstanceType] = 1>
	<cfelse>
		<cfset spillchrtstruct[SubstanceType] = spillchrtstruct[SubstanceType]+1>
	</cfif>
</cfloop>

<cfset chartxml="<chart caption='Environmental Spills'  showborder='0' use3dlighting='0' enablesmartlabels='1' startingangle='310' showlabels='1' showvalues='1' showpercentvalues='1' showlegend='1' showtooltip='0' decimals='0' usedataplotcolorforlabels='1'  labelDistance='2' smartLabelClearance='3'   doughnutRadius='70' exportenabled='1' exportAtClientSide='1' pieYScale='90'>">
					<cfloop list="#structkeylist(spillchrtstruct)#" index="i">
						<cfset uselable = trim(i)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable# #spillchrtstruct[i]#' value='#spillchrtstruct[i]#' />">
					</cfloop>
 					<cfset chartxml= chartxml & "</chart>">
					<cfoutput>
					<div id="Spillchrt"></div>
					
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "doughnut3d",
						        "renderAt": "Spillchrt",
						        "width": "510",
						        "height": "430",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>






<table cellpadding="5" cellspacing="0" bgcolor="000000" width="80%">
	<tr>
		<td class="purplebg"></td>
		<td class="purplebg"><strong class="bodytextwhite">Incident No.</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Type of Substance</strong></td>
		<td class="purplebg" align="center"><strong class="bodytextwhite">Qty of Release</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Unit</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Source</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Duration</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Receiving Environment</strong></td>
	</tr>
	<cfset spilltotstruct = {}>
	<cfset spilltot = 0>
	<cfoutput query="getspills" group="SubstanceType">
		<cfset rspanctr = 0>
		<cfoutput>
			<cfset rspanctr = rspanctr + 1>
		</cfoutput>
		<cfset rctr = 0>
		<cfoutput>
			<cfset rctr = rctr + 1>
				<tr bgcolor="ffffff">
					<cfif rctr eq 1><td class="bodyTextGrey" rowspan="#rspanctr#" valign="top">#SubstanceType#</td></cfif>
					<td class="bodyTextGrey" valign="top">#trackingnum#</td>
					<td class="bodyTextGrey" valign="top">#SubstanceType#</td>
					<td class="bodyTextGrey" valign="top" align="center">#Quantity#</td>
					<td class="bodyTextGrey" valign="top">#ReleaseUnit#</td>
					<td class="bodyTextGrey" valign="top">#ReleaseSource#</td>
					<td class="bodyTextGrey" valign="top">#ReleaseDuration#</td>
					<td class="bodyTextGrey" valign="top">#ReceivingEnv#</td>
				</tr>
				<cfif not structkeyexists(spilltotstruct,ReleaseUnit)>
					<cfset spilltotstruct[ReleaseUnit] = Quantity>
					<cfset spilltot = spilltot+Quantity>
				<cfelse>
					<cfset spilltotstruct[ReleaseUnit] = spilltotstruct[ReleaseUnit]+Quantity>
					<cfset spilltot = spilltot+Quantity>
				</cfif>
			</cfoutput>
			<tr>
				<td class="formlable" valign="top"><strong>Total #SubstanceType# Spills:</strong></td>
				 <td class="formlable" valign="top" colspan="7"><strong>#rctr#</strong></td>
			</tr>
		</cfoutput>
		<tr bgcolor="ffffff">
		<td colspan="8" class="bodyTextGrey"><strong>Grand Total Spills</strong></td>
	</tr>
	<tr bgcolor="ffffff">
		<td colspan="8">
		<cfoutput>
		<table cellpadding="5" cellspacing="0" bgcolor="000000" border="1" align="left">
			<tr>
				<td bgcolor="ffffff" class="bodyTextGrey"></td>
				<td bgcolor="ffffff" class="bodyTextGrey" align="center"><strong>Total</strong></td>
			</tr>
			<tr>
				<td bgcolor="ffffff" class="formlable"><strong>Total</strong></td>
				<td bgcolor="ffffff" class="formlable" align="center">#numberformat(spilltot,"0.00")#</td>
			</tr>
			<cfif listlen(structkeylist(spilltotstruct)) gt 0>
				<cfloop list="#structkeylist(spilltotstruct)#" index="i">
					<tr>
						<td bgcolor="ffffff" class="bodyTextGrey"><strong>#i#</strong></td>
						<td bgcolor="ffffff" class="bodyTextGrey" align="center">#numberformat(spilltotstruct[i],"0.00")#</td>
					</tr>
				</cfloop>
			</cfif>
		</table>
		</cfoutput>
		</td>
	</tr>
</table>
</cfdocument>
<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">
