<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_daystoclosepdf.cfm", "pdfgen"))>
<cfset fnamepdf = "DaystoClose_#timeformat(now(),'hhnnssl')#.pdf">
<cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfparam name="dosearch" default="">




<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" marginright="0.25" marginleft="0.25" margintop="0.25" marginbottom="0.25">
<style>
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #FFFFFF;
	font-style: normal;
}
.purplebg {
background: #5f2468;

}
.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	color: ##000000;
	font-style: normal;
}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f5f5f;
	font-style: normal;
	}
.formlable{
background-color:#f7f1f9;
font-family: Segoe UI;
	font-size: 10pt;

	color: #5f5f5f;
	font-style: normal;
}
</style>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="900">
<tr>
	<td class="bodyTexthd" colspan="2">Incident Days to Close</td>
</tr>
</table>

<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%">
	
	<tr>
		<td class="formlable" ><strong>Incident Number</strong></td>
		<td class="formlable" ><strong><cfoutput>#request.oulabellong#</cfoutput></strong></td>
		<td class="formlable"  align="center" ><strong>Business Stream</strong></td>
		<td class="formlable"  align="center" ><strong>Project/Office</strong></td>
		<td class="formlable"  align="center" ><strong>Site/Office Name</strong></td>
		<td class="formlable"  align="center" ><strong>Incident Type</strong></td>
		<td class="formlable"  align="center" ><strong>Near Miss?</strong></td>
		<td class="formlable"  align="center" ><strong>OSHA Classification</strong></td>
		<td class="formlable"  align="center" ><strong>Potential Rating</strong></td>
		<td class="formlable"  align="center" ><strong>Incident Date</strong></td>
		<td class="formlable"  align="center" ><strong>Incident Assigned To</strong></td>
		<td class="formlable"  align="center" ><strong>Short Description</strong></td>
		<td class="formlable"  align="center" ><strong>Date Opened</strong></td>
		<td class="formlable"  align="center" ><strong>Date Closed</strong></td>
		<td class="formlable"  align="center" ><strong>Num. Days Open</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfoutput query="getdaystoclose" group="trackingnum">
	
	
	<cfset ctr = ctr+1>
	<tr <cfif ctr mod 2 is 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodyTextGrey">#TrackingNum#</td>
		<td class="bodyTextGrey">#ouname#</td>
		<td class="bodyTextGrey">#bsname#</td>
		<td class="bodyTextGrey">#group_name#</td>
		<td class="bodyTextGrey">#sitename#</td>
		<td class="bodyTextGrey"><cfif isfatality eq "yes">Fatality<cfelse>#IncType#</cfif></td>
		<td class="bodyTextGrey" align="center" ><cfif isfatality eq "yes">No<cfelse>#isnearmiss#</cfif></td>
		<td class="bodyTextGrey">#category#</td>
		<td class="bodyTextGrey" align="center" >#PotentialRating#</td>
		<td class="bodyTextGrey">#dateformat(incidentDate,sysdateformat)#</td>
		<td class="bodyTextGrey">#IncAssignedTo#</td>
		<td class="bodyTextGrey">#shortdesc#</td>
		<td class="bodyTextGrey">#dateformat(dateCreated,sysdateformat)#</td>
		<td class="bodyTextGrey">#dateformat(CompletedDate,sysdateformat)#</td>
		<td class="bodyTextGrey"><cfif trim(CompletedDate) neq '' and trim(dateCreated) neq ''>#datediff("d",dateCreated,CompletedDate)#</cfif></td>
		
		
		
	</tr>
	
	</cfoutput> 
</table>
</cfdocument>
<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">