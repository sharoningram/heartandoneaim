<cfparam name="dosearch" default="">

<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_incmanpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "IncidentMan_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="portrait">
<cfoutput>


<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Incident Man</td>
</tr>

</table>

<cfset filedirectory= trim(replacenocase(getcurrenttemplatepath(), 
						"reports\displays\dsp_incmanpdf.cfm", "images\"))>
<cfimage action="read" source="#filedirectory#incidentMan.jpg" name="chartimg">

<cfset ImageSetAntialiasing(chartimg,"on")>
<cfset attr = StructNew()> 

<cfset attr.size=16> 
<cfset attr.style="bold"> 
<cfset attr.font="Segoe UI"> 

	<cfloop query="getincman">
		<cfswitch expression="#lbl#">
			<cfcase value="ankle">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",130,570,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="arm/wrist">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",320,274,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="back">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",85,140,attr)>
				<cfset ImageDrawLine(chartimg,110,144,165,160)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="chest">
				<cfif cnt gt 0>
					
					<cfset stp = 215>
					<cfif len(cnt) eq 1>
						<cfset stp = 215>
					<cfelseif len(cnt) eq 2>
						<cfset stp = 213>
					<cfelseif len(cnt) eq 3>
						<cfset stp = 210>
					<cfelseif len(cnt) eq 4>
						<cfset stp = 206>
					<cfelseif len(cnt) eq 5>
						<cfset stp = 200>
					<cfelseif len(cnt) eq 6>
						<cfset stp = 197>
					<cfelseif len(cnt) gte 7>
						<cfset stp = 193>
					</cfif>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",stp,170,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
				
			</cfcase>
			<cfcase value="eye">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",290,50,attr)>
					<cfset ImageDrawLine(chartimg,260,70,288,45)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
				
			</cfcase>
			<cfcase value="face">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",140,50,attr)>
				<cfset ImageDrawLine(chartimg,160,55,244,80)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="fingers/thumb">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",40,396,attr)>
				<!--- 	<cfset ImageDrawLine(chartimg,79,395,150,360)> --->
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="foot/toes">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",285,624,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="hand">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",375,326,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="head">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",218,28,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="leg">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")>
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",305,444,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="neck">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",161,117,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="pelvis">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",65,250,attr)>
					<cfset ImageDrawLine(chartimg,90,255,208,320)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="shoulder">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",320,145,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="abdomen">
				<cfif cnt gt 0>
					<cfset stp = 242>
					<cfif len(cnt) eq 1>
						<cfset stp = 242>
					<cfelseif len(cnt) eq 2>
						<cfset stp = 237>
					<cfelseif len(cnt) eq 3>
						<cfset stp = 232>
					<cfelseif len(cnt) eq 4>
						<cfset stp = 227>
					<cfelseif len(cnt) eq 5>
						<cfset stp = 223>
					<cfelseif len(cnt) eq 6>
						<cfset stp = 219>
					<cfelseif len(cnt) gte 7>
						<cfset stp = 214>
					</cfif>
				
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl#",215,286,attr)>
					<cfset ImageDrawText(chartimg,"(#cnt#)",stp,301,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="Internal Organs">
				<cfif cnt gt 0><!--- #cnt# --->
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"Internal",222,220,attr)>
					<cfset ImageDrawText(chartimg,"Organs",224,235,attr)>
					<cfset stp = 242>
					<cfif len(cnt) eq 1>
						<cfset stp = 242>
					<cfelseif len(cnt) eq 2>
						<cfset stp = 237>
					<cfelseif len(cnt) eq 3>
						<cfset stp = 232>
					<cfelseif len(cnt) eq 4>
						<cfset stp = 227>
					<cfelseif len(cnt) eq 5>
						<cfset stp = 223>
					<cfelseif len(cnt) eq 6>
						<cfset stp = 219>
					<cfelseif len(cnt) gte 7>
						<cfset stp = 214>
					</cfif>
					<cfset ImageDrawText(chartimg,"(#cnt#)", stp,252,attr)>
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="Groin">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",70,310,attr)>
					<cfset ImageDrawLine(chartimg,100,315,235,365)>
					
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			<cfcase value="Mouth/Teeth">
				<cfif cnt gt 0>
					<cfset ImageSetDrawingColor(chartimg,"5f2468")> 
					<cfset ImageDrawText(chartimg,"#lbl# (#cnt#)",307,95,attr)>
					<cfset ImageDrawLine(chartimg,258,100,303,92)>
					
				<!--- <cfelse>
					<cfset ImageSetDrawingColor(chartimg,"6f6f6f")>  --->
				</cfif>
				
			</cfcase>
			
		</cfswitch>
	</cfloop>




<table cellpadding="4" cellspacing="1" bgcolor="##ffffff" align="center">
	<tr>
		<td><cfimage source="#chartimg#" action="writeToBrowser"></td>
	</tr>
</table>
				
</cfoutput>
</cfdocument>
			
			<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_incmanpdf.cfm", ""))>	
			<cffile action="MOVE" source="#deldir##fnamepdf#" destination="#PDFFileDir#" nameconflict="OVERWRITE"> --->
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">