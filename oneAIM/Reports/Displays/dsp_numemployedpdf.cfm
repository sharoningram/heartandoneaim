
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "dsp_numemployedpdf.cfm", "pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "NumberEmployed_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="portrait" marginright="0.5" marginleft="0.5" >

<cfoutput>
<cfparam name="dosearch" default="">

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Numbers Employed Report (#monthasstring(month(qrymonth))# #year(qrymonth)#)</td>
</tr>

</table>
</cfoutput>

<table cellpadding="2" cellspacing="1" bgcolor="000000" width="100%" border="0">
	<tr>
		
		<td class="purplebg" align="center" width="25%" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;"><cfoutput>#request.bulabellong#</cfoutput></strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">AFW Employees</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468" ><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Managed Contractor</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468" ><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Subcontractor</strong></td>
		<td class="purplebg" align="center" bgcolor="5f2468" ><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Joint Venture</strong></td>
		<td class="purplebg" align="center"  bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">Total</strong></td>
	</tr>
	<cfset ctr = 0>
	<cfset globalAFW = 0>
	<cfset globalSUB = 0>
	<cfset globalMC = 0>
	<cfset globalJV = 0>
	
	<cfoutput query="getnumemplyed" group="buname">
	<cfset rowtot = 0>
	<cfif structkeyexists(numempstruct,"#buid#_AFW")><cfset afwnum = numempstruct["#buid#_AFW"]><cfelse><cfset afwnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#buid#_SUB")><cfset subnum = numempstruct["#buid#_SUB"]><cfelse><cfset subnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#buid#_JV")><cfset jvnum = numempstruct["#buid#_JV"]><cfelse><cfset jvnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#buid#_MC")><cfset mcnum = numempstruct["#buid#_MC"]><cfelse><cfset mcnum = 0></cfif>
	
	<cfset globalAFW = globalAFW+afwnum>
	<cfset globalSUB = globalSUB+subnum>
	<cfset globalMC = globalMC+mcnum>
	<cfset globalJV = globalJV+jvnum>
	
	<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
	<cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodytext" style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#buname#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(afwnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mcnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
	<cfoutput group="ouid">
	<cfif listfind(openbu,buid) gt 0>
	<cfset rowtot = 0>
	<cfif structkeyexists(numempstruct,"#ouid#_AFW")><cfset afwnum = numempstruct["#ouid#_AFW"]><cfelse><cfset afwnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#ouid#_SUB")><cfset subnum = numempstruct["#ouid#_SUB"]><cfelse><cfset subnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#ouid#_JV")><cfset jvnum = numempstruct["#ouid#_JV"]><cfelse><cfset jvnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#ouid#_MC")><cfset mcnum = numempstruct["#ouid#_MC"]><cfelse><cfset mcnum = 0></cfif>
	<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
	<cfif listfind(openbu,buid) gt 0>
	<tr <cfif ctr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;"<cfelse>bgcolor="ffffff"</cfif>  id="ourow#ouid#">
		<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;#ouname#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(afwnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mcnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
	</tr>
	</cfif>
	<cfoutput group="group_number">
	<cfset rowtot = 0>
	<cfif listfind(openou,ouid) gt 0>
		<cfif structkeyexists(grpnumempstruct,"#group_number#_AFW")><cfset afwnum = grpnumempstruct["#group_number#_AFW"]><cfelse><cfset afwnum = 0></cfif>
		<cfif structkeyexists(grpnumempstruct,"#group_number#_SUB")><cfset subnum = grpnumempstruct["#group_number#_SUB"]><cfelse><cfset subnum = 0></cfif>
		<cfif structkeyexists(grpnumempstruct,"#group_number#_JV")><cfset jvnum = grpnumempstruct["#group_number#_JV"]><cfelse><cfset jvnum = 0></cfif>
		<cfif structkeyexists(grpnumempstruct,"#group_number#_MC")><cfset mcnum = grpnumempstruct["#group_number#_MC"]><cfelse><cfset mcnum = 0></cfif>
		<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
		<tr <cfif ctr mod 2 is 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> id="grouprow#group_number#">
		<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;">&nbsp;&nbsp;&nbsp;&nbsp;#group_name#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(afwnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(mcnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(subnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(jvnum)#</td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;">#numberformat(rowtot)#</td>
		</tr>
		
	</cfif>
	</cfoutput> 
	</cfif>
	</cfoutput>
	</cfoutput>
	<cfset rowtot = 0>
	
	<cfoutput>
	<tr class="formlable" style="font-family: Segoe UI;font-size: 8pt;background-color:f7f1f9;">
		<td class="bodytext" nowrap style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>Amec Foster Wheeler Global</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(globalAFW)#</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(globalMC)#</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(globalSUB)#</strong></td>
		<td class="bodytext" align="center"  style="font-family: Segoe UI;font-size: 8pt;color: 000000;"><strong>#numberformat(globalJV)#</strong></td>
		<td class="bodytext" align="center" style="font-family: Segoe UI;font-size: 8pt;color: 000000;" ><strong>#numberformat(globalAFW+globalMC+globalSUB+globalJV)#</strong></td>
	</tr>
	</cfoutput>
</table>

 </cfdocument>
			
			
			
			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No"> 