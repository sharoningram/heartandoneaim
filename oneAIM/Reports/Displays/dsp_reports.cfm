
<div align="center" class="midbox">
	<div align="center" class="midboxtitle">What report would you like to view?</div>
	<table cellpadding="8" cellspacing="0" border="0" align="center" width="100%">
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.ALLrolling" class="midboxtitletxt">All Injury Incidents Rolling Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.bird" class="midboxtitletxt">BIRD Triangle</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.capalog" class="midboxtitletxt">CA/PA Log</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.injcausechart" class="midboxtitletxt">Cause of Injury Incident Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.configdash" class="midboxtitletxt">Configurable Dashboard Report</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.contractorperf" class="midboxtitletxt">Contractor Performance Report</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.clientperf" class="midboxtitletxt">Client Performance Report</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.daysaway" class="midboxtitletxt">Days Away from Work</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.spills" class="midboxtitletxt">Environmental Spills</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.frb" class="midboxtitletxt">Frequency Rate Break Down Table</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.safetyrules" class="midboxtitletxt">Global Safety Rules</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.immcausechart" class="midboxtitletxt">Immediate Cause Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.immcausesubchart" class="midboxtitletxt">Immediate Cause Sub-Type Chart</a></td>
		</tr>
		
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.incclasschart" class="midboxtitletxt">Incident Classification Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.daysopen" class="midboxtitletxt">Incident Days Open</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.daystoclose" class="midboxtitletxt">Incident Days to Close</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.incbytype" class="midboxtitletxt">Incidents by Type</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.incbytypechart" class="midboxtitletxt">Incidents by Type Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.incidentman" class="midboxtitletxt">Incident Man</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.potrating" class="midboxtitletxt">Incident Potential Rating Matrix</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.ratetriangle" class="midboxtitletxt">Incident Potential Rating Triangle</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.incwdesc" class="midboxtitletxt">Incidents with Description</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.oshachart" class="midboxtitletxt">Injury Type by OSHA Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.LTIrolling" class="midboxtitletxt">Lost Time Injuries Rolling Chart</a></td>
		</tr>
		 
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.mht" class="midboxtitletxt">Man-hours Total Report</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.monthly" class="midboxtitletxt">Monthly Statistic Table/Monthly Cumulative</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.numemployed" class="midboxtitletxt">Numbers Employed</a></td>
		</tr>
		
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.reasonlate" class="midboxtitletxt">Reason for Late Recording</a></td>
		</tr>
		
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.rwc" class="midboxtitletxt">Restricted Work Cases</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.rootcausechart" class="midboxtitletxt">Root Cause Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.rootcausesubchart" class="midboxtitletxt">Root Cause Sub-Type Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.safetyessentials" class="midboxtitletxt">Safety Essentials</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.hazardsourcechart" class="midboxtitletxt">Source of Hazard Chart</a></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.TRIrolling" class="midboxtitletxt">Total Recordable Incidents Rolling Chart</a></td>
		</tr>
		
		<tr>
			<td class="formlable"><img src="images/spacer.gif" height="3"></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.diamond" class="midboxtitletxt">Mining the Diamond</a></td>
		</tr>
		<tr>
			<td class="formlable"><img src="images/spacer.gif" height="3"></td>
		</tr>
		<tr>
			<td class="midboxtitletxt"><a href="index.cfm?fuseaction=reports.myreports" class="midboxtitletxt">My Saved Reports</a></td>	
		</tr>
	</table>
</div>