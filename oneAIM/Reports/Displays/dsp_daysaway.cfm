<cfoutput>
<cfparam name="dosearch" default="">
<cfif fusebox.fuseaction neq "printdaysaway">
<form action="#self#?fuseaction=#attributes.xfa.printdaysaway#" name="printfrm" method="post" target="_blank">
		
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.printdaysawaypdf#" name="printdaysawaypdf" method="post" target="_blank">
		
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	</cfif>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Days Away From Work</td>
</tr>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right"><cfif fusebox.fuseaction neq "printdaysaway"><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="document.printdaysawaypdf.submit();"><img src="images/pdfsm.png" border="0"></a></cfif></td>
</tr>
</table>
<cfset injlti = 0>
<cfset inj4week = 0>
<cfset inj180 = 0>
<cfset injnumdays = 0>
<cfset oilti = 0>
<cfset oi4week = 0>
<cfset oi180 = 0>
<cfset oinumdays = 0>

<cfloop query="getdaysaway">
	<cfif primarytype eq 1>
		<cfset injlti = injlti+1>
			<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset injnumdays = injnumdays + 180>
				</cfif>
				
			</cfif>
	<cfelseif PrimaryType eq 5 or SecondaryType eq 5>
		<cfset oilti = oilti+1>
		<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				
				
				
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset oinumdays = oinumdays + 180>
				</cfif>
			</cfif>
	<cfelseif SecondaryType eq 1>
		<cfset injlti = injlti+1>
			<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset inj4week = inj4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset inj180 = inj180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset injnumdays = injnumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset injnumdays = injnumdays + 180>
				</cfif>
				
			</cfif>
	<cfelseif SecondaryType eq 5>
		<cfset oilti = oilti+1>
		<cfif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) neq ''>
				<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,LTIDateReturned)>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,LTIDateReturned) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				
				
				
			<cfelseif trim(LTIFirstDate) neq '' and trim(LTIDateReturned) eq ''>
				<cfif datediff("d",LTIFirstDate,now()) gte 28>
					<cfset oi4week = oi4week+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) gte 180>
					<cfset oi180 = oi180+1>
				</cfif>
				<cfif datediff("d",LTIFirstDate,now()) lte 180>
					<cfset oinumdays = oinumdays + datediff("d",LTIFirstDate,now())>
				<cfelse>
					<cfset oinumdays = oinumdays + 180>
				</cfif>
			</cfif>
	</cfif>

</cfloop>




<table cellpadding="4" cellspacing="1" bgcolor="000000" width="80%">
	<tr>
		<td class="purplebg"><strong class="bodytextwhite">Incident Type</strong></td>
		<td class="purplebg" align="center"><strong class="bodytextwhite">Number of LTIs</strong></td>
		<td class="purplebg" align="center"><strong class="bodytextwhite">Cases Over Four Weeks</strong></td>
		<td class="purplebg" align="center"><strong class="bodytextwhite">Cases at 180 Days</strong></td>
		<td class="purplebg" align="center"><strong class="bodytextwhite">Number of Days Lost</strong></td>
	</tr>
	<tr>
		<td class="purplebg"><strong class="bodytextwhite">Injury</strong></td>
		<td bgcolor="ffffff" class="bodytext" align="center">#injlti#</td>
		<Td bgcolor="ffffff" class="bodytext" align="center">#inj4week#</td>
		<td bgcolor="ffffff" class="bodytext" align="center">#inj180#</td>
		<td bgcolor="ffffff" class="bodytext" align="center">#injnumdays#</td>
	</tr>
	<tr>
		<td class="purplebg"><strong class="bodytextwhite">Occupational Illness</strong></td>
		<td bgcolor="ffffff" class="bodytext" align="center">#oilti#</td>
		<Td bgcolor="ffffff" class="bodytext" align="center">#oi4week#</td>
		<td bgcolor="ffffff" class="bodytext" align="center">#oi180#</td>
		<td bgcolor="ffffff" class="bodytext" align="center">#oinumdays#</td>
	</tr>
	<tr>
		<td class="purplebg"><strong class="bodytextwhite">Total</strong></td>
		<td bgcolor="ffffff" class="bodytext" align="center">#injlti+oilti#</td>
		<Td bgcolor="ffffff" class="bodytext" align="center">#inj4week+oi4week#</td>
		<td bgcolor="ffffff" class="bodytext" align="center">#inj180+oi180#</td>
		<td bgcolor="ffffff" class="bodytext" align="center">#injnumdays+oinumdays#</td>
	</tr>
</table>
	
</cfoutput>