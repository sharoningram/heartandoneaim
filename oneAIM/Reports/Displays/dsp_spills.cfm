
<cfparam name="dosearch" default="">

<cfoutput>
<cfif fusebox.fuseaction neq "printbird">
<form action="#self#?fuseaction=#attributes.xfa.printspills#" name="printfrm" method="post" target="_blank">
		
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">	
<input type="hidden" name="irnumber" value="#irnumber#">	
<input type="hidden" name="keywords" value="#keywords#">	
<input type="hidden" name="incstatus" value="#incstatus#">	
	</form>
	<form action="#self#?fuseaction=#attributes.xfa.printspillspdf#" name="printspillspdf" method="post" target="_blank">
		
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">	
<input type="hidden" name="irnumber" value="#irnumber#">	
<input type="hidden" name="keywords" value="#keywords#">	
<input type="hidden" name="incstatus" value="#incstatus#">	
	</form>
	</cfif>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Environmental Spills</td>
</tr>

</table>
</cfoutput>

<cfset spillchrtstruct = {}>
<cfloop query="getspills">
	<cfif not structkeyexists(spillchrtstruct,SubstanceType)>
		<cfset spillchrtstruct[SubstanceType] = 1>
	<cfelse>
		<cfset spillchrtstruct[SubstanceType] = spillchrtstruct[SubstanceType]+1>
	</cfif>
</cfloop>

<cfset chartxml="<chart caption='Environmental Spills'  showborder='0' use3dlighting='0' enablesmartlabels='1' startingangle='310' showlabels='1' showvalues='1' showpercentvalues='1' showlegend='1' showtooltip='0' decimals='0' usedataplotcolorforlabels='1'  labelDistance='2' smartLabelClearance='3'  doughnutRadius='60' exportenabled='1' exportAtClientSide='1' pieYScale='90'>">
					<cfloop list="#structkeylist(spillchrtstruct)#" index="i">
						<cfset uselable = trim(i)>
						<cfset uselable = replace(uselable,"<","&lt;","all")>
						<cfset uselable = replace(uselable,">","&gt;","all")>
						<cfset chartxml= chartxml & "<set label='#uselable# #spillchrtstruct[i]#' value='#spillchrtstruct[i]#' />">
					</cfloop>
 					<cfset chartxml= chartxml & "</chart>">
					<cfoutput>
					<div id="Spillchrt"></div>
					
					<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "doughnut3d",
						        "renderAt": "Spillchrt",
						        "width": "510",
						        "height": "430",
						        "dataFormat": "xml",
						        "dataSource":  "#chartxml#"
								
						    }
							
							);
						
						    thisChart.render();
						
						
						});
						</script>		
					</cfoutput>




<cfoutput>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">

<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right"><cfif fusebox.fuseaction neq "printspills"><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="document.printspillspdf.submit();"><img src="images/pdfsm.png" border="0"></a></cfif></td>
</tr>
</table>
</cfoutput>
<cfset spilltotstruct = {}>
<cfset spilltot = 0>
<table cellpadding="5" cellspacing="0" bgcolor="000000" width="80%">
	<tr>
		<td class="purplebg"></td>
		<td class="purplebg"><strong class="bodytextwhite">Incident No.</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Type of Substance</strong></td>
		<td class="purplebg" align="center"><strong class="bodytextwhite">Qty of Release</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Unit</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Source</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Duration</strong></td>
		<td class="purplebg"><strong class="bodytextwhite">Receiving Environment</strong></td>
	</tr>
	<cfoutput query="getspills" group="SubstanceType">
		<cfset rspanctr = 0>
		<cfoutput>
			<cfset rspanctr = rspanctr + 1>
		</cfoutput>
		<cfset rctr = 0>
		<cfoutput>
			<cfset rctr = rctr + 1>
				<tr bgcolor="ffffff">
					<cfif rctr eq 1><td class="bodyTextGrey" rowspan="#rspanctr#" valign="top">#SubstanceType#</td></cfif>
					<td class="bodyTextGrey" valign="top">#trackingnum#</td>
					<td class="bodyTextGrey" valign="top">#SubstanceType#</td>
					<td class="bodyTextGrey" valign="top" align="center">#Quantity#</td>
					<td class="bodyTextGrey" valign="top">#ReleaseUnit#</td>
					<td class="bodyTextGrey" valign="top">#ReleaseSource#</td>
					<td class="bodyTextGrey" valign="top">#ReleaseDuration#</td>
					<td class="bodyTextGrey" valign="top">#ReceivingEnv#</td>
				</tr>
				<cfif not structkeyexists(spilltotstruct,ReleaseUnit)>
					<cfset spilltotstruct[ReleaseUnit] = Quantity>
					<cfset spilltot = spilltot+Quantity>
				<cfelse>
					<cfset spilltotstruct[ReleaseUnit] = spilltotstruct[ReleaseUnit]+Quantity>
					<cfset spilltot = spilltot+Quantity>
				</cfif>
			</cfoutput>
			<tr>
				<td class="formlable" valign="top"><strong>Total #SubstanceType# Spills:</strong></td>
				 <td class="formlable" valign="top" colspan="7"><strong>#rctr#</strong></td>
			</tr>
		</cfoutput>
	<tr bgcolor="ffffff">
		<td colspan="8" class="bodyTextGrey"><strong>Grand Total Spills</strong></td>
	</tr>
	<tr bgcolor="ffffff">
		<td colspan="8">
		<cfoutput>
		<table cellpadding="5" cellspacing="0" bgcolor="000000" border="1" align="left">
			<tr>
				<td bgcolor="ffffff" class="bodyTextGrey"></td>
				<td bgcolor="ffffff" class="bodyTextGrey" align="center"><strong>Total</strong></td>
			</tr>
			<tr>
				<td bgcolor="ffffff" class="formlable"><strong>Total</strong></td>
				<td bgcolor="ffffff" class="formlable" align="center">#numberformat(spilltot,"0.00")#</td>
			</tr>
			<cfif listlen(structkeylist(spilltotstruct)) gt 0>
				<cfloop list="#structkeylist(spilltotstruct)#" index="i">
					<tr>
						<td bgcolor="ffffff" class="bodyTextGrey"><strong>#i#</strong></td>
						<td bgcolor="ffffff" class="bodyTextGrey" align="center">#numberformat(spilltotstruct[i],"0.00")#</td>
					</tr>
				</cfloop>
			</cfif>
		</table>
		</cfoutput>
		</td>
	</tr>
</table>

	


