<cfparam name="dosearch" default="">
<cfset ratestruct = {}>
	<cfset greenpotratings = "A1,B1,C1,D1,A2,B2,C2">
	<cfset yellowpotratings = "E1,D2,E2,A3,B3,C3,D3">
	<cfset redpotratings = "E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
<cfset totctr = 0>
<cfset greenctr = 0>
<cfset yellowctr = 0>
<cfset redctr = 0>
<cfset needratectr = 0>
<cfloop query="getratings">
<cfset totctr = totctr+1>
	<cfif trim(PotentialRating) eq ''>
		<cfset needratectr = needratectr+1>
	<cfelseif listfindnocase(greenpotratings,PotentialRating) gt 0>
		<cfset greenctr = greenctr+1>
	<cfelseif listfindnocase(yellowpotratings,PotentialRating) gt 0>
		<cfset yellowctr = yellowctr+1>
	<cfelseif listfindnocase(redpotratings,PotentialRating) gt 0>
		<cfset redctr = redctr+1>
	</cfif>
	<cfif not structkeyexists(ratestruct,PotentialRating)>
		<cfset ratestruct[PotentialRating] = 1>
	<cfelse>
		<cfset ratestruct[PotentialRating] = ratestruct[PotentialRating] +1>
	</cfif>
</cfloop>
<!--- <cfdump var="#ratestruct#"> --->
<cfoutput>
<cfif fusebox.fuseaction neq "printpotrating">
	<form action="#self#?fuseaction=#attributes.xfa.printpotrating#" name="printfrm" method="post" target="_blank">
		
	<input type="hidden" name="ratelev" value="#ratelev#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	
	<form action="#self#?fuseaction=#attributes.xfa.printpotratingpdf#" name="printpotratingpdf" method="post" target="_blank">
		
	<input type="hidden" name="ratelev" value="#ratelev#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	
	
	
</cfif>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="88%">
<tr>
	<td class="bodyTexthd" colspan="2">Incident Potential Rating Matrix</td>
</tr>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right"><cfif fusebox.fuseaction neq "printpotrating"><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="document.printpotratingpdf.submit();"><img src="images/pdfsm.png" border="0"></a></cfif></td>
</tr>
</table>
<table cellpadding="4" cellspacing="1" bgcolor="##000000" width="88%" id="potentialtable">
					
					<tr style="height:37px;">
						<td rowspan="8" width="1%" align="center" bgcolor="000000"><img src="images/consequences.png" border="0"></td>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf"><strong class="bodyText">When using this matrix consider the potential outcome of the incident not the actual outcome<br>*IP = intellectual property</strong></td>
						<td width="25%" align="center"  colspan="5" bgcolor="ffffff"><strong class="bodyText">No. of People</strong></td>
						
					</tr>
					<tr style="height:37px;" bgcolor="ffffff">
						
						<td  align="center"  ></td>
						<td   align="center"  ><strong class="bodyTextsm">Injury/Health</strong></td>
						<td  align="center"  ><strong class="bodyTextsm">Environmental</strong></td>
						<td  align="center"  ><strong class="bodyTextsm">Damage</strong></td>
						<td   align="center"  ><strong class="bodyTextsm">Security</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm">0</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">1</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm">1-2</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">2-10</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">10+</strong></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center" bgcolor="##DDEEFF"><strong class="bodyText">1</strong></td>
						<td align="center" class="bodyTextsm">First aid/health effect</td>
						<td align="center" class="bodyTextsm">Minimal reversible environmental impact</td>
						<td align="center"  class="bodyTextsm">Minor loss/damage/business impact (<10K)</td>
						<td align="center" class="bodyTextsm">Minor crime, no impact on business operations or reputation</td>
					<td align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"A1")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A1<br>#ratestruct["A1"]#</strong></cfif></td>
					<td align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"B1")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B1<br>#ratestruct["B1"]#</strong></cfif></td>
					<td  align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"C1")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C1<br>#ratestruct["C1"]#</strong></cfif></td>
					<td align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"D1")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D1<br>#ratestruct["D1"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"E1")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">E1<br>#ratestruct["E1"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td  align="center" bgcolor="##DDEEFF"><strong class="bodyText">2</strong></td>
						<td align="center" class="bodyTextsm">Medical treatment/ restricted work/ moderate health effect</td>
						<td align="center"  class="bodyTextsm">Minor pollution with short term impact (1 month)</td>
						<td align="center"  class="bodyTextsm">Moderate loss/ damage/ business impact (10-100K)</td>
						<td align="center"  class="bodyTextsm">Theft/vandalism/loss of non-IP* information. No lasting impact on business operations or reputation</td>
					<td  align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"A2")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A2<br>#ratestruct["A2"]#</strong></cfif></td>
					<td  align="center" bgcolor="##00b050"><cfif structkeyexists(ratestruct,"B2")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B2<br>#ratestruct["B2"]#</strong></cfif></td>
					<td align="center"  bgcolor="##00b050"><cfif structkeyexists(ratestruct,"C2")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C2<br>#ratestruct["C2"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"D2")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">D2<br>#ratestruct["D2"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"E2")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">E2<br>#ratestruct["E2"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">3</strong></td>
						<td align="center"  class="bodyTextsm">Lost time injury/ significant health effect</td>
						<td align="center"  class="bodyTextsm">Moderate pollution with medium term localised impact (1 year)</td>
						<td align="center"  class="bodyTextsm">Significant loss/damage/ business impact reportable event within local legislation (100K-1M)</td>
						<td align="center"  class="bodyTextsm">Crime (threat, intimidation, sabotage) with impact on people or loss of confidential IP containing material</td>
					<td  align="center" bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"A3")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">A3<br>#ratestruct["A3"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"B3")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">B3<br>#ratestruct["B3"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"C3")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">C3<br>#ratestruct["C3"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif structkeyexists(ratestruct,"D3")><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">D3<br>#ratestruct["D3"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"E3")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E3<br>#ratestruct["E3"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">4</strong></td>
						<td align="center"  class="bodyTextsm">Serious injury/ severe health effect/ long term disability</td>
						<td align="center"  class="bodyTextsm">Severe pollution with long term localised impact (+1 year)</td>
						<td align="center"  class="bodyTextsm">Severe loss/damage/ business impact reportable event within local legislation (1-10M)</td>
						<td align="center"  class="bodyTextsm">Serious and deliberate criminal attack against people, disruptive natural event or loss of sensitive IP material</td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"A4")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A4<br>#ratestruct["A4"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"B4")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B4<br>#ratestruct["B4"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"C4")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C4<br>#ratestruct["C4"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"D4")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D4<br>#ratestruct["D4"]#</strong></cfif></td>
					<td align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"E4")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E4<br>#ratestruct["E4"]#</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">5</strong></td>
						<td align="center"  class="bodyTextsm">Fatality</td>
						<td align="center"  class="bodyTextsm">Major pollution with long term environmental change</td>
						<td align="center"  class="bodyTextsm">Major loss/damage/ reportable event within local legislation business impact (10M+)</td>
						<td align="center"  class="bodyTextsm">Sustained, serious attack/loss of confidential IP or natural disaster requiring formal emergency response</td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"A5")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A5<br>#ratestruct["A5"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"B5")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B5<br>#ratestruct["B5"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"C5")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C5<br>#ratestruct["C5"]#</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"D5")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D5<br>#ratestruct["D5"]#</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif structkeyexists(ratestruct,"E5")><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E5<br>#ratestruct["E5"]#</strong></cfif></td>
					</tr>
					<tr>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf" class="bodyTextsm">High Potential Incidents (Red) = LEVEL 2; Medium Potential Incidents (Amber) = LEVEL 1 or 2 (at HSSE Manager's discretion); Low Potential Incidents (Green) = LEVEL 1</td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">A</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">B</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">C</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">D</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodyText">E</strong></td>
					</tr>
				</table>	
				<br>
<table cellpadding="4" cellspacing="1" bgcolor="##000000">

	<tr bgcolor="ffffff">
		<Td></td>
		<td class="bodytext" align="center">Number of Incidents</td>
		<td class="bodytext" align="center">Percentage</td>
	</tr>
	<tr bgcolor="ffffff">
		<Td class="bodytext">Total number of incidents</td>
		<td class="bodytext" align="center">#totctr#</td>
		<td class="bodytext" align="center"></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext">High Potential</td>
		<td class="bodytext" bgcolor="##ff0000" align="center"><cfif redctr gt 0><cfif fusebox.fuseaction neq "printpotrating"><a href="javascript:void(0);" onclick="submtlevfrm('High');">#redctr#</a><cfelse>#redctr#</cfif><cfelse>#redctr#</cfif></td>
		<td class="bodytext" bgcolor="##ff0000" align="center"><cfif redctr gt 0 and totctr gt 0>#numberformat(redctr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext">Medium Potential</td>
		<td class="bodytext" bgcolor="##ffc000" align="center"><cfif yellowctr gt 0><cfif fusebox.fuseaction neq "printpotrating"><a href="javascript:void(0);" onclick="submtlevfrm('Medium');">#yellowctr#</a><cfelse>#yellowctr#</cfif><cfelse>#yellowctr#</cfif></td>
		<td class="bodytext" bgcolor="##ffc000" align="center"><cfif yellowctr gt 0 and totctr gt 0>#numberformat(yellowctr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext">Low Potential</td>
		<td class="bodytext" bgcolor="##00b050" align="center"><cfif greenctr gt 0><cfif fusebox.fuseaction neq "printpotrating"><a href="javascript:void(0);" onclick="submtlevfrm('Low');">#greenctr#</a><cfelse>#greenctr#</cfif><cfelse>#greenctr#</cfif></td>
		<td class="bodytext" bgcolor="##00b050" align="center"><cfif greenctr gt 0 and totctr gt 0>#numberformat(greenctr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
	<tr bgcolor="ffffff">
		<Td class="bodytext">Potential rating to be determined</td>
		<td class="bodytext" align="center"><cfif needratectr gt 0><cfif fusebox.fuseaction neq "printpotrating"><a href="javascript:void(0);" onclick="submtlevfrm('TBD');">#needratectr#</a><cfelse>#needratectr#</cfif><cfelse>#needratectr#</cfif></td>
		<td class="bodytext" align="center"><cfif needratectr gt 0 and totctr gt 0>#numberformat(needratectr/totctr,"0.0000")*100#%</cfif></td>
	</tr>	
</table>
</cfoutput>
<cfif fusebox.fuseaction neq "printpotrating">
<script type="text/javascript">
	function submtlevfrm(levtype){
	document.showlevlist.ratelev.value=levtype;
	document.showlevlist.submit();
	}
	</script>
	<cfoutput>
	<form action="#self#?fuseaction=#attributes.xfa.potrating#" name="showlevlist" method="post">
		
	<input type="hidden" name="ratelev" value="">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
	<input type="hidden" name="dosearch" value="#dosearch#">
	<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="occillcat" value="#occillcat#">		
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>
	</cfoutput>
	</cfif>
<cfif isdefined("getincidentratelist.recordcount")><br>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite"><cfif ratelev eq "TBD">Potential Rating to be Determined<cfelse><cfoutput>#ratelev#</cfoutput> Potential Incidents</cfif></td>
	</tr>
	<tr>
		<td>
<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
	<tr>
		<td class="formlable" ><strong>Incident Number</strong></td>
		<td class="formlable" ><strong>Incident Date</strong></td>
		<td class="formlable" ><strong><cfoutput>#request.bulabellong#</cfoutput></strong></td>
		<td class="formlable" ><strong><cfoutput>#request.oulabellong#</cfoutput></strong></td>
		<td class="formlable" ><strong>Project/Office</strong></td>
		<td class="formlable" ><strong>Incident Type</strong></td>
		<td class="formlable" ><strong>OSHA Classification</strong></td>
		<td class="formlable" ><strong>Potential Rating</strong></td>
		
	</tr>
	<cfoutput query="getincidentratelist">
	<cfset oktolink = "no">
	<cfif accessglist eq "all">
		<cfset oktolink = "yes">
	<cfelse>
		<cfif listfind(accessglist,group_number) gt 0>
			<cfset oktolink = "yes">
		<cfelse>
			<cfif createdbyEmail eq request.userlogin>
				<cfset oktolink = "yes">
			</cfif>
		</cfif>
	</cfif>
	<tr <cfif currentrow mod 2 is 0>class="formlable"</cfif>>
		<td class="bodyTextGrey"><cfif fusebox.fuseaction neq "printpotrating" and oktolink><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#">#TrackingNum#</a><cfelse>#TrackingNum#</cfif></td>
		<td class="bodyTextGrey">#dateformat(incidentDate,sysdateformat)#</td>
		<td class="bodyTextGrey">#buname#</td>
		<td class="bodyTextGrey">#ouname#</td>
		<td class="bodyTextGrey">#Group_Name#</td>
		
		<td class="bodyTextGrey">#IncType#</td>
		<td class="bodyTextGrey">#Category#</td>
		<td class="bodyTextGrey">#PotentialRating#</td>
		
	</tr>
	</cfoutput>
</table>
	</td></tr></table>
	
</cfif>