<cfif  listfindnocase("printLTIrolling",fusebox.fuseaction) eq 0>
		
		<cfoutput>
		
		<form action="#self#?fuseaction=#attributes.xfa.printLTIrolling#" name="printfrm" method="post" target="_blank">
			<input type="hidden" name="incyear" value="#incyear#">
		<input type="hidden" name="invlevel" value="#invlevel#">
		<input type="hidden" name="bu" value="#bu#">
		<input type="hidden" name="ou" value="#ou#">
		<input type="hidden" name="INCNM" value="#INCNM#">
		<input type="hidden" name="businessstream" value="#businessstream#">
		<input type="hidden" name="projectoffice" value="#projectoffice#">
		<input type="hidden" name="site" value="#site#">
		<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
		<input type="hidden" name="frmjv" value="#frmjv#">
		<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
		<input type="hidden" name="frmclient" value="#frmclient#">
		<input type="hidden" name="locdetail" value="#locdetail#">
		<input type="hidden" name="frmcountryid" value="#frmcountryid#">
		<input type="hidden" name="frmincassignto" value="#frmincassignto#">
		<input type="hidden" name="wronly" value="#wronly#">
		<input type="hidden" name="primarytype" value="#primarytype#">
		<input type="hidden" name="secondtype" value="#secondtype#">
		<input type="hidden" name="hipoonly" value="#hipoonly#">
		<input type="hidden" name="oshaclass" value="#oshaclass#">
		<input type="hidden" name="oidef" value="#oidef#">
		<input type="hidden" name="seriousinj" value="#seriousinj#">
		<input type="hidden" name="secinccats" value="#secinccats#">
		<input type="hidden" name="eic" value="#eic#">
		<input type="hidden" name="damagesrc" value="#damagesrc#">
		<input type="hidden" name="startdate" value="#startdate#">
		<input type="hidden" name="enddate" value="#enddate#">
		<input type="hidden" name="dosearch" value="#dosearch#">
		<input type="hidden" name="assocg" value="#assocg#">
		<input type="hidden" name="potentialrating" value="#potentialrating#">	
		<input type="hidden" name="occillcat" value="#occillcat#">	
		<input type="hidden" name="irnumber" value="#irnumber#">	
		<input type="hidden" name="keywords" value="#keywords#">	
		<input type="hidden" name="incstatus" value="#incstatus#">
			</form>
			</cfoutput>
		
		</cfif>

			


			<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
			<tr>
				<td class="bodyTexthd" colspan="2">Lost Time Incidents Rolling Chart</td>
			</tr><cfoutput>
			<tr>
				<td class="bodyText"><cfif trim(dosearch) neq ''><cfif trim(startdate) neq ''>Start Date: #dateformat(startdate,sysdateformat)#<cfelse><cfif trim(incyear) neq ''>For #incyear#</cfif></cfif></cfif><!--- <cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif> ---></td>
				<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printLTIrolling"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
			</tr></cfoutput>
			</table>
			<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
				<tr>
					<td class="bodyTextWhite">&nbsp;</td>
				</tr>
				<tr>
					<td bgcolor="ffffff" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top" align="center" id="chartContainer3" width="100%"></td>
							</tr>
						</table>
					
					</td>
				</tr>
			</table>
			
			<cfoutput>	
<script type="text/javascript">

FusionCharts.ready(function () {
    var multiseriesChart = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer3",
        "width": "90%",
        "height": "300",
        "dataFormat": "xml",
        "dataSource":  "#chart3xml#"
		
    }
	
	);

    multiseriesChart.render();

	
});
</script>
	</cfoutput>
