<cfparam name="dosearch" default="">
<cfif fusebox.fuseaction neq "printincbytypechart">
<cfoutput>

<form action="#self#?fuseaction=#attributes.xfa.printincbytypechart#" name="printfrm" method="post" target="_blank">
	<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="dosearch" value="#dosearch#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="potentialrating" value="#potentialrating#">
<input type="hidden" name="irnumber" value="#irnumber#">
<input type="hidden" name="keywords" value="#keywords#">
<input type="hidden" name="incstatus" value="#incstatus#">
	</form>

	</cfoutput>
	</cfif>
	
<cfoutput query="getsortedincs" group="inctypeid">

<cfset "NM_#inctypeid#" = 0>
<cfset "FA_#inctypeid#" = 0>
<cfset "LTI_#inctypeid#" = 0>
<cfset "MTC_#inctypeid#" = 0>
<cfset "RWC_#inctypeid#" = 0>
<cfset "NT_#inctypeid#" = 0>
<cfset "OID_#inctypeid#" = 0>
<cfset "POLL_#inctypeid#" = 0>
<cfset "NONPOLL_#inctypeid#" = 0>
<cfset "NAT_#inctypeid#" = 0>
<cfset "UNI_#inctypeid#" = 0>
<cfset "INT_#inctypeid#" = 0>
<cfset "PER_#inctypeid#" = 0>
<cfset "BA_#inctypeid#" = 0>
<cfset "IS_#inctypeid#" = 0>
<cfset "SPB_#inctypeid#" = 0>

</cfoutput>


<cfloop query="getincoi">
	<cfif isnearmiss eq 1>
		<cfset "NM_#PrimaryType#" = evaluate("NM_#PrimaryType#")+1>
	</cfif>
	<cfif CatID eq 1>
		<cfset "FA_#PrimaryType#" = evaluate("FA_#PrimaryType#")+1>
	</cfif>
	<cfif CatID eq 2>
		<cfset "LTI_#PrimaryType#" = evaluate("LTI_#PrimaryType#")+1>
	</cfif>
	<cfif CatID eq 3>
		<cfset "MTC_#PrimaryType#" = evaluate("MTC_#PrimaryType#")+1>
	</cfif>
	<cfif CatID eq 4>
		<cfset "RWC_#PrimaryType#" = evaluate("RWC_#PrimaryType#")+1>
	</cfif>
	<cfif CatID eq 5>
		<cfset "NT_#PrimaryType#" = evaluate("NT_#PrimaryType#")+1>
	</cfif>
	<cfif primarytype eq 5>
		<cfif OIdefinition eq 2>
			<cfset "OID_#PrimaryType#" = evaluate("OID_#PrimaryType#")+1>
		</cfif>
	</cfif>
</cfloop>
<cfloop query="getenv">
	<cfif isnearmiss eq 1>
		<cfset "NM_#PrimaryType#" = evaluate("NM_#PrimaryType#")+1>
	</cfif>
	<cfif EnvIncCat eq 1>
		<cfset "POLL_#PrimaryType#" = evaluate("POLL_#PrimaryType#")+1>
	</cfif>
	<cfif EnvIncCat eq 2>
		<cfset "NONPOLL_#PrimaryType#" = evaluate("NONPOLL_#PrimaryType#")+1>
	</cfif>
</cfloop>
<cfloop query="incasset">
	<cfif isnearmiss eq 1>
		<cfset "NM_#PrimaryType#" = evaluate("NM_#PrimaryType#")+1>
	</cfif>
	<cfif DamageSource eq 1>
		<cfset "NAT_#PrimaryType#" = evaluate("NAT_#PrimaryType#")+1>
	</cfif>
	<cfif DamageSource eq 2>
		<cfset "INT_#PrimaryType#" = evaluate("INT_#PrimaryType#")+1>
	</cfif>
	<cfif DamageSource eq 3>
		<cfset "UNI_#PrimaryType#" = evaluate("UNI_#PrimaryType#")+1>
	</cfif>
</cfloop>

<cfloop query="getsec">
	<cfif isnearmiss eq 1>
		<cfset "NM_#PrimaryType#" = evaluate("NM_#PrimaryType#")+1>
	</cfif>
	<cfif SecIncCat eq 1>
		<cfset "PER_#PrimaryType#" = evaluate("PER_#PrimaryType#")+1>
	</cfif>
	<cfif SecIncCat eq 2>
		<cfset "BA_#PrimaryType#" = evaluate("BA_#PrimaryType#")+1>
	</cfif>
	<cfif SecIncCat eq 3>
		<cfset "IS_#PrimaryType#" = evaluate("IS_#PrimaryType#")+1>
	</cfif>
	<cfif SecIncCat eq 4>
		<cfset "SPB_#PrimaryType#" = evaluate("SPB_#PrimaryType#")+1>
	</cfif>
</cfloop>


<cfset chartxml = "<chart caption='Incidents by Type Chart' subcaption='' xaxisname='' yaxisname='' showsum='0' numberprefix='' showvalues='0'  legendPosition='right' drawCustomLegendIcon='1' legendIconSides='5' palettecolors='92bae3,a47734,f1c283,3381cc,daa4c8,e3d63c,5d079c,c82759,447711,da7062,27758b,fece60,a22d62,dd581f,00906a,efa252,3a6598' useRoundEdges='0' exportenabled='1' exportAtClientSide='1'><categories>">
<cfoutput query="getsortedincs" group="inctypeid">
<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset chartxml = chartxml & "<category label='#inctype#' />">
</cfif>
</cfoutput>
<cfset chartxml = chartxml & "</categories>">

<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,1) gt 0 or listfind(primarytype,5) gt 0>
<cfset chartxml = chartxml & "<dataset seriesname='Near Miss'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset nmval = evaluate("NM_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#nmval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='First Aid'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset faval = evaluate("FA_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#faval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='LTI'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset ltival = evaluate("LTI_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#ltival#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='MTC'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset mtcval = evaluate("MTC_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#mtcval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='RWC'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset rwcval = evaluate("RWC_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#rwcval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='No Treatment'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset ntval = evaluate("NT_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#ntval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
</cfif>
<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,5) gt 0>
<cfset chartxml = chartxml & "<dataset seriesname='OIND'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset oidval = evaluate("OID_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#oidval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
</cfif>

<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,2) gt 0>
<cfset chartxml = chartxml & "<dataset seriesname='Pollution Event'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset pval = evaluate("POLL_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#pval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='Non-Pollution Event'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset npval = evaluate("NONPOLL_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#npval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
</cfif>

<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,3) gt 0>
<cfset chartxml = chartxml & "<dataset seriesname='Natural Event'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset natval = evaluate("NAT_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#natval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='Intentional'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset inteval = evaluate("INT_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#inteval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='Unintentional'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset unintval = evaluate("UNI_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#unintval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
</cfif>

<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,4) gt 0>
<cfset chartxml = chartxml & "<dataset seriesname='Person'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset perval = evaluate("PER_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#perval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='Business Assets'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset baval = evaluate("BA_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#baval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='Information Security'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset isval = evaluate("IS_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#isval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
<cfset chartxml = chartxml & "<dataset seriesname='Security Practice Breach'>">
<cfloop query="getsortedincs" group="inctypeid">
	<cfif listfindnocase(primarytype,"all") gt 0 or listfind(primarytype,inctypeid) gt 0>
	<cfset spbval = evaluate("SPB_#inctypeid#")>
	<cfset chartxml = chartxml & "<set value='#spbval#' />">
	</cfif>
</cfloop>
<cfset chartxml = chartxml & "</dataset>">
</cfif>
<cfset chartxml = chartxml & "</chart>">
<cfoutput>
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2">Incidents by Type Chart</td>
</tr><cfoutput>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#</cfif></td>
	<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printincbytypechart"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
</tr></cfoutput>
</table>
<table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Incidents by Type Chart</td>
	</tr>
	<tr>
		<td bgcolor="ffffff" align="center">
<div id="chartContainer"></div>
		</td>
	</tr>
</table>
<script type="text/javascript">

FusionCharts.ready(function () {
    var stackedChart = new FusionCharts({
        "type": "stackedcolumn2d",
        "renderAt": "chartContainer",
        "width": "750",
        "height": "550",
        "dataFormat": "xml",
        "dataSource":  "#chartxml#"
		
    }
	
	);

    stackedChart.render();


});
</script>
</cfoutput>

	<!--- <table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="100%">
<tr>
	<td class="bodyTexthd" colspan="2">Incidents by Type</td>
</tr><cfoutput>
<tr>
	<td class="bodyText"><cfif trim(dosearch) eq ''>From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents</cfif></td>
	<td align="right" valign="middle"><cfif fusebox.fuseaction neq "printincbytypechart"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td></tr></table></cfif></td>
</tr></cfoutput>
</table>
<!--- <table cellpadding="4" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="bodyTextWhite">Incidents</td>
	</tr>
	<tr>
		<td bgcolor="ffffff"> --->
<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td class="formlable" ><strong>Incident Type</strong></td>
		<td class="formlable" ><strong>&nbsp;</strong></td>
		<td class="formlable"  align="center" ><strong>Amec Foster Wheeler</strong></td>
		<td class="formlable"  align="center" ><strong>Another Amec-FW Co (working as Contractor)</strong></td>
		<td class="formlable"  align="center" ><strong>Sub-Contractor</strong></td>
		<td class="formlable"  align="center" ><strong>Managed Contractor</strong></td>
		<td class="formlable"  align="center" ><strong>Joint Venture</strong></td>
		<td class="formlable"  align="center" ><strong>Other (Includes: Member of the Public, Visitor, and Other)</strong></td>
		<td class="formlable"  align="center" ><strong>Count</strong></td>
		
	</tr>
	<cfset col1tot = 0><cfset col2tot = 0><cfset col3tot = 0><cfset col4tot= 0><cfset col5tot = 0><cfset col6tot = 0><cfset col7tot = 0>
	<cfoutput query="getsortedincs" group="inctypeid">
	<cfset col1ctr = 0><cfset col2ctr = 0><cfset col3ctr = 0><cfset col4ctr = 0><cfset col5ctr = 0><cfset col6ctr = 0><cfset col7ctr = 0>
	<cfset gctr = 0>
	<cfset needrwcnt = 0>
	<cfoutput>
	<cfset needrwcnt = needrwcnt+1>
	</cfoutput>
	<cfoutput>
	<cfset gctr = gctr + 1>
	<cfset rctr = 0>
	
	<tr>
		<cfif gctr eq 1><td class="formlable" <cfif inctypeid eq 5>rowspan="#needrwcnt+3#"<cfelse>rowspan="#needrwcnt+2#"</cfif> valign="top"><cfif gctr eq 1>#IncType#</cfif></td></cfif>
		<td class="formlable">#incsubtype#</td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_1")>#iostruct["#inctypeid#_#incsubtypeid#_1"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_#incsubtypeid#_1"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_2")>#iostruct["#inctypeid#_#incsubtypeid#_2"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_#incsubtypeid#_2"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_3")>#iostruct["#inctypeid#_#incsubtypeid#_3"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_#incsubtypeid#_3"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_4")>#iostruct["#inctypeid#_#incsubtypeid#_4"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_#incsubtypeid#_4"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_11")>#iostruct["#inctypeid#_#incsubtypeid#_11"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_#incsubtypeid#_11"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_oth")>#iostruct["#inctypeid#_#incsubtypeid#_oth"]#<cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfelse>0</cfif></td>
		<td align="center" class="formlable" bgcolor="ffffff">#rctr#<cfset col7ctr = col7ctr+rctr></td>
	</tr>
	
	</cfoutput>
	<cfif inctypeid eq 5>
	<cfset rctr = 0>
	<tr>
		
		<td class="formlable">Non-Discrete</td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_1")>#iostruct["#inctypeid#_OIDEF_1"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_OIDEF_1"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_2")>#iostruct["#inctypeid#_OIDEF_2"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_OIDEF_2"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_3")>#iostruct["#inctypeid#_OIDEF_3"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_OIDEF_3"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_4")>#iostruct["#inctypeid#_OIDEF_4"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_OIDEF_4"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_11")>#iostruct["#inctypeid#_OIDEF_11"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_OIDEF_11"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_oth")>#iostruct["#inctypeid#_OIDEF_oth"]#<cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_OIDEF_oth"]><cfelse>0</cfif></td>
		<td align="center" class="formlable" bgcolor="ffffff">#rctr#<cfset col7ctr = col7ctr+rctr></td>
	</tr>
	
	</cfif>
	<cfset rctr = 0>
	<tr>
		
		<td class="formlable">Near Miss</td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_1")>#iostruct["#inctypeid#_NearMiss_1"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_NearMiss_1"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_2")>#iostruct["#inctypeid#_NearMiss_2"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_NearMiss_2"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_3")>#iostruct["#inctypeid#_NearMiss_3"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_NearMiss_3"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_4")>#iostruct["#inctypeid#_NearMiss_4"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_NearMiss_4"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_11")>#iostruct["#inctypeid#_NearMiss_11"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_NearMiss_11"]><cfelse>0</cfif></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff"><cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_oth")>#iostruct["#inctypeid#_NearMiss_oth"]#<cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_NearMiss_oth"]><cfelse>0</cfif></td>
		<td align="center" class="formlable" bgcolor="ffffff">#rctr#</td>
	</tr>
	<tr>
		
		<td class="formlable">Sub-Total</td>
		<td align="center" class="bodyTextGrey"  bgcolor="ffffff">#col1ctr#<cfset col1tot = col1tot + col1ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff">#col2ctr#<cfset col2tot = col2tot + col2ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff">#col3ctr#<cfset col3tot = col3tot + col3ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff">#col4ctr#<cfset col4tot = col4tot + col4ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff">#col5ctr#<cfset col5tot = col5tot + col5ctr></td>
		<td align="center" class="bodyTextGrey" bgcolor="ffffff">#col6ctr#<cfset col6tot = col6tot + col6ctr></td>
		<td align="center" class="formlable" bgcolor="ffffff">#col7ctr#<cfset col7tot = col7tot + col7ctr></td>
	</tr>
	<tr>
		<td colspan="9" class="formlable"></td>
	</tr>
	</cfoutput>
	<cfoutput>
	<tr>
		<td class="formlable"></td>
		<td class="formlable"><strong>Total</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col1tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col2tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col3tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col4tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col5tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col6tot#</strong></td>
		<td align="center" class="formlable" bgcolor="ffffff"><strong>#col7tot#</strong></td>
	</tr>
	</cfoutput>
</table> --->
<!--- </td>
</tr>
</table> --->