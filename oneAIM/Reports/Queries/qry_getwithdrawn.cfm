<cfquery name="getwithdrawn" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.withdrawndate, oneAIMincidents.withdrawnto, Users.Firstname, Users.Lastname
FROM            Users INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON Users.UserId = oneAIMincidents.withdrawnto LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.SecondaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.withdrawnto is not null 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif listfindnocase(occillcat,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#occillcat#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
		and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>
</cfif>

ORDER BY oneAIMincidents.TrackingNum
</cfquery>