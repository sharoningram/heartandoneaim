<cfquery name="getnonjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) 
                         + SUM(LaborHoursTotal.SubHrs) + SUM(LaborHoursTotal.JVHours) + SUM(LaborHoursTotal.ManagedContractorHours) AS manhrs, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID, YEAR(LaborHoursTotal.WeekEndingDate) AS manyr, Groups.OUid
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID AND LaborHoursTotal.GroupNumber = GroupLocations.GroupNumber

<cfif trim(startdate) neq ''>
	WHERE LaborHoursTotal.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHoursTotal.WeekEndingDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
WHERE      year(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
</cfif>
</cfif>
<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY Groups.Business_Line_ID, GroupLocations.LocationDetailID, YEAR(LaborHoursTotal.WeekEndingDate), Groups.OUid
ORDER BY manyr, Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>