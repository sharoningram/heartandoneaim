<cfif listfindnocase("empbymonth,printempbymonth,printempbymonthpdf,exportnumemployedbymonth",fusebox.fuseaction) gt 0>
<cfquery name="getincidentyears" datasource="#request.dsn#">
SELECT DISTINCT YEAR(PopDate) AS incyear
FROM            Population
WHERE        (PopDate IS NOT NULL)
ORDER BY incyear
</cfquery>
<cfelse>

<cfquery name="getincidentyears" datasource="#request.dsn#">
SELECT DISTINCT YEAR(incidentDate) AS incyear
FROM            oneAIMincidents
WHERE        (incidentDate IS NOT NULL)
ORDER BY incyear
</cfquery>

</cfif>