<cfparam name="uincsearch" default="0">
<cfquery name="getusersearchdetails" datasource="#request.dsn#">
SELECT       UserIncSearchID, SearchName, UserID, incyear, invlevel, bu, ou, bs, project, siteoffice, mgdcontractor, jv, subcon, clientid, locdetail, countryid, 
                         incassigned, wronly, primarytype, secondtype, hipoony, oshaclass, oidef, seriousinj, secinccats, eic, damagesrc, startdate, enddate, DateSaved, SaveCtr, 
                         incnm, irnumber, incandor, searchtype,potentialrating
FROM            UserIncidentSearches
WHERE UserIncSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#uincsearch#">
</cfquery>

<cfif getusersearchdetails.recordcount gt 0>
		<cfset bu = getusersearchdetails.bu >
		<cfset ou =  getusersearchdetails.ou >
		<cfset businessstream =  getusersearchdetails.bs >
		<cfset frmclient  = getusersearchdetails.clientid >
		<cfset projectoffice =  getusersearchdetails.project >
		<cfset site  = getusersearchdetails.siteoffice >
		<cfset frmcountryid =  getusersearchdetails.countryid >
		<cfset locdetail  = getusersearchdetails.locdetail >
		<cfset frmincassignto  = getusersearchdetails.incassigned >
		<cfset eic =  getusersearchdetails.eic >
		<cfset damagesrc  = getusersearchdetails.damagesrc >
		<cfset frmmgdcontractor  = getusersearchdetails.mgdcontractor >
		<cfset frmjv  = getusersearchdetails.jv >
		<cfset frmsubcontractor =  getusersearchdetails.subcon >
		<cfset wronly = getusersearchdetails.wronly > 
		<cfset primarytype  = getusersearchdetails.primarytype >
		<cfset secondtype =  getusersearchdetails.secondtype >
		<cfset hipoonly = getusersearchdetails.hipoony >
		<cfset oshaclass =  getusersearchdetails.oshaclass >
		<cfset oidef =  getusersearchdetails.oidef >
		<cfset seriousinj  = getusersearchdetails.seriousinj >
		<cfset secinccats =  getusersearchdetails.secinccats >
		<cfset invlevel  = getusersearchdetails.invlevel >
		<cfset form.incyear =  getusersearchdetails.incyear >
		<cfset startdate = getusersearchdetails.startdate >
		<cfset enddate = getusersearchdetails.enddate >
		<cfset searchname = getusersearchdetails.SearchName >
		<cfset incnm = getusersearchdetails.incnm >
		<cfset irnumber = getusersearchdetails.irnumber >
		<cfset incandor = getusersearchdetails.incandor >
		<cfset searchtype = getusersearchdetails.searchtype >
		<cfset potentialrating = getusersearchdetails.potentialrating >
</cfif>
