<cfparam name="incyear" default="#year(now())#">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="Yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="Yes">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="chartvalue" default="">
<cfparam name="charttype" default="No">
<cfparam name="searchname" default="">
<cfparam name="potentialrating" default="All">
<cfparam name="occillcat" default="All">
<cfparam name="manhrtype" default="All">
<cfparam name="irnumber" default="">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="beentoirp" default="No">

<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>

<cfquery name="getdeadlines" datasource="#request.dsn#">
select DLdate
from ManHourDeadLines
where  year(DLdate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#"> and month(DLdate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#month(now())#">
order by DLdate
</cfquery>

 <cfif getdeadlines.recordcount gt 0>
<cfoutput query="getdeadlines">
	<cfif month(now()) eq month(dldate)>
		<cfif now() lt dateadd("ww",1,dldate)>
			<cfset chkmonth = month(dateadd("m",-1,dldate))>
			<cfset chkyear = year(dateadd("m",-1,dldate))>
		<cfelse>
			<cfset chkmonth = month(dldate)>
			<cfset chkyear = year(dldate)>
		</cfif>


	</cfif>
</cfoutput>

<cfelse>
	<cfmodule template="/#getappconfig.oneAIMpath#/shared/weekenddate.cfm" numberweeks="1" startday="Thursday" checkyear="#year(now())#">
	<cfset monthwks = "">
	<cfset chkmonth = month(now())>
	<cfset chkyear = year(now())>
	<cfloop list="#weekendingdates#" index="x">
		<cfif month(x) eq month(now())>
			<cfset monthwks = listappend(monthwks,x)>
		</cfif>
	</cfloop>
	
	<cfif now() lt listgetat(monthwks,2)>
		<cfif month(now()) eq 1>
			<cfset chkmonth = 12>
			<cfset chkyear = year(now())-1>
		<cfelse>
			<cfset chkmonth = chkmonth-1>
		</cfif>
	</cfif>
</cfif>

<cfset gothrslist = "">
<cfquery name="getgrouplistc" datasource="#request.dsn#">
SELECT    distinct   GroupLocations.GroupLocID as Group_Number
FROM            Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber
WHERE        (Groups.Active_Group = 1) AND (NewDials.status <> 99) AND (NewDials.Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (GroupLocations.entryType IN ('both', 'MHOnly')) and groupLocations.isactive = 1
</cfquery>
<cfset allgnumlist = valuelist(getgrouplistc.Group_Number)>

<cfquery name="getjdehrsc" datasource="#request.dsn#">
SELECT       distinct GroupLocations.GroupLocID as group_number
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE        YEAR(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">
</cfquery>
<cfloop query="getjdehrsc">
	<cfif listfind(allgnumlist,group_number) gt 0>
		<cfset allgnumlist = listdeleteat(allgnumlist, listfind(allgnumlist,group_number))>
	</cfif>
	<!--- <cfif listfind(gothrslist,group_number) eq 0>
		<cfset gothrslist = listappend(gothrslist,group_number)>
	</cfif> --->
</cfloop>
<cfquery name="getjdeohc" datasource="#request.dsn#">
SELECT       distinct GroupLocations.GroupLocID as group_number
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE        YEAR(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">
</cfquery>
<cfloop query="getjdeohc">
	<cfif listfind(allgnumlist,group_number) gt 0>
		<cfset allgnumlist = listdeleteat(allgnumlist, listfind(allgnumlist,group_number))>
	</cfif>
	<!--- <cfif listfind(gothrslist,group_number) eq 0>
		<cfset gothrslist = listappend(gothrslist,group_number)>
	</cfif> --->
</cfloop>
<cfquery name="getnonjdehrsc" datasource="#request.dsn#">
SELECT  distinct  locationid as GroupNumber
FROM            LaborHoursTotal 
WHERE        YEAR(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">
</cfquery>
<cfloop query="getnonjdehrsc">
	<cfif listfind(allgnumlist,GroupNumber) gt 0>
		<cfset allgnumlist = listdeleteat(allgnumlist, listfind(allgnumlist,GroupNumber))>
	</cfif>
	<!--- <cfif listfind(gothrslist,GroupNumber) eq 0>
		<cfset gothrslist = listappend(gothrslist,GroupNumber)>
	</cfif> --->
</cfloop>
<cfquery name="getcontractorhrsc" datasource="#request.dsn#">
SELECT     distinct   locationid as GroupNumber
FROM            ContractorHours 
WHERE        YEAR(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">
</cfquery>
<cfloop query="getcontractorhrsc">
	<cfif listfind(allgnumlist,GroupNumber) gt 0>
		<cfset allgnumlist = listdeleteat(allgnumlist, listfind(allgnumlist,GroupNumber))>
	</cfif>
	<!--- <cfif listfind(gothrslist,GroupNumber) eq 0>
	
	
		<cfset gothrslist = listappend(gothrslist,GroupNumber)>
	</cfif> --->
</cfloop>






<cfquery name="getoutstandingmh" datasource="#request.dsn#">
SELECT    distinct    Groups.Group_Number, Groups.Group_Name, NewDials.ID AS ouid, NewDials.Name AS ouname, NewDials_1.ID as buid, NewDials_1.Name as buname
FROM            Groups INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber
<cfif trim(allgnumlist) neq ''>
where GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#allgnumlist#" list="yes">)
<cfelse>
where GroupLocations.grouplocid in (0)
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
order by buname, ouname, group_name
</cfquery>