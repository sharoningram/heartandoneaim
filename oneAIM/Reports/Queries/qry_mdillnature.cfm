<cfquery name="getmdillnature" datasource="#request.dsn#">
SELECT        oneAIMInjuryOI.IllnessNature
FROM            oneAIMInjuryOI INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON oneAIMInjuryOI.IRN = oneAIMincidents.IRN INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         oneAIMassetDamage ON oneAIMincidents.IRN = oneAIMassetDamage.IRN LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         oneAIMSecurity ON oneAIMincidents.IRN = oneAIMSecurity.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMInjuryOI.IllnessNature IS NOT NULL) AND (LEN(oneAIMInjuryOI.IllnessNature) > 0)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)    
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oshaclass,"All") eq 0>
	and oneAIMInjuryOI.OSHAclass in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oshaclass#" list="Yes">)
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>
<cfif listfindnocase(seriousinj,"All") eq 0>
	and oneAIMInjuryOI.isSerious in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#" list="Yes">)
</cfif>
<cfif listfindnocase(secinccats,"All") eq 0>
	and oneAIMSecurity.SecIncCat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#secinccats#" list="Yes">)
</cfif>
<cfif listfindnocase(eic,"All") eq 0>
	and oneAIMEnvironmental.EnvIncCat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#eic#" list="Yes">)
</cfif>
<cfif listfindnocase(damagesrc,"All") eq 0>
	and oneAIMassetDamage.DamageSource in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#damagesrc#" list="Yes">)
</cfif>



</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

</cfquery>

<cfquery name="getillnatlist" datasource="#request.dsn#">
SELECT        IllNatureID, IllnessNature, Status
FROM            IllnessNature
</cfquery>
<cfset illnatstruct = {}>

<cfoutput query="getmdillnature">
	<cfloop list="#IllnessNature#" index="i">
		<cfif not structkeyexists(illnatstruct,i)>
			<cfset illnatstruct[i] = 1>
		<cfelse>
			<cfset illnatstruct[i] = illnatstruct[i]+1>
		</cfif>
	
	</cfloop>
</cfoutput>
<cfset getillnaturemd = QueryNew("cnt,lbl")>
<Cfset ctr = 0>
<cfloop query="getillnatlist">
	<cfif  structkeyexists(illnatstruct,IllNatureID)>
		<Cfset ctr = ctr+1>
		<cfset QueryAddRow( getillnaturemd ) />
		<cfset getillnaturemd["cnt"][ctr] = illnatstruct[IllNatureID] />
		<cfset getillnaturemd["lbl"][ctr] = IllnessNature>
	</cfif>
</cfloop> 



