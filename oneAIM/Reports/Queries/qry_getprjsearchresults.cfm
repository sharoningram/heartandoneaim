<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="sortordby" default="prjnum">
<cfparam name="mhetype" default="all">
<cfparam name="groupactive" default="yes">
<cfparam name="siteactive" default="yes">
<cfparam name="mhentrytype" default="all">
<cfparam name="prjnumber" default="">
<cfquery name="getprjsearchresults" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.TrackingDate, Groups.Tracking_Num, NewDials.Name AS buname, NewDials_1.Name AS ouname, NewDials_2.Name AS bsname, 
                         GroupLocations.SiteName, Clients.ClientName, Groups.Group_Name, LocationDetails.LocationDetail, Countries.CountryName,Groups.active_group,GroupLocations.isActive
FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID ON GroupLocations.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         LocationDetails ON GroupLocations.LocationDetailID = LocationDetails.LocationDetailID LEFT OUTER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE 1 = 1
<cfif trim(prjnumber) neq ''>
and   (('P' + CONVERT(varchar, TrackingDate) + RIGHT('0000' + CAST(Tracking_Num AS VARCHAR(4)), 4)) IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#replace(prjnumber,' ','','all')#" list="Yes">))
</cfif>
<cfif listfindnocase(mhetype,"All") eq 0>
	and Groups.erpsys in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#replacenocase(mhetype,'Manual','none')#" list="Yes">)
</cfif>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>

<cfif listfindnocase(groupactive,"All") eq 0>
	<cfif groupactive eq "yes">
		and Groups.active_group = 1
	<cfelseif groupactive eq "no">
		and Groups.active_group = 0
	</cfif>
</cfif>


<cfif listfindnocase(siteactive,"All") eq 0>
	<cfif siteactive eq "yes">
		and GroupLocations.isactive = 1
	<cfelseif siteactive eq "no">
		and GroupLocations.isactive = 0
	</cfif>
</cfif>

<cfif listfindnocase(mhentrytype,"All") eq 0>
	<cfif mhentrytype eq "yes">
		and GroupLocations.entryType = 'Both'
	<cfelseif mhentrytype eq "no">
		and GroupLocations.entryType = 'IncidentOnly'
	<cfelseif mhentrytype eq "Man hours only">
		and GroupLocations.entryType = 'MHOnly'
	</cfif>
</cfif>

<cfif  listfindnocase(request.userlevel,"Global Admin") eq 0>
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfif  listfindnocase(request.userlevel,"Senior Executive View") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0>
	<cfif  listfindnocase(request.userlevel,"Senior Executive View") eq 0>
			and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
	</cfif>
</cfif>
<cfif  listfindnocase(request.userlevel,"User") gt 0>
		and Groups.Group_Number in (SELECT DISTINCT Groups.Group_Number
		FROM            GroupUserAssignment INNER JOIN
		                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
		WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1) AND (Groups.OUid IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)))
</cfif>
</cfif>
<cfswitch expression="#sortordby#">
<cfcase value="prjnum">
ORDER BY Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="bu">
ORDER BY buname, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="ou">
ORDER BY ouname, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="bs">
ORDER BY bsname, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="cl">
ORDER BY Clients.ClientName, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="prj">
ORDER BY Groups.Group_Name, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="so">
ORDER BY GroupLocations.SiteName, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="ld">
ORDER BY LocationDetails.LocationDetail, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="cnty">
ORDER BY Countries.CountryName, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="prjstat">
ORDER BY Groups.Active_Group desc, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
<cfcase value="sostat">
ORDER BY GroupLocations.IsActive desc, Groups.TrackingDate, Groups.Tracking_num
</cfcase>
</cfswitch>
</cfquery>
