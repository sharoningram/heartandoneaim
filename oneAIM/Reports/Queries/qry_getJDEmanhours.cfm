<cfif listfindnocase(incyear,"All") eq 0>
<cfset rollstart = "1/1/#incyear-1#">

<cfif incyear eq year(now())>
	<cfset rollend = "#month(now())#/#daysinmonth(now())#/#incyear#">
<cfelse>
	<cfset rollend = "12/31/#incyear#">
</cfif>
<cfelse>
<cfset rollstart = "1/1/#year(now())-1#">

<cfif year(now()) eq year(now())>
	<cfset rollend = "#month(now())#/#daysinmonth(now())#/#year(now())#">
<cfelse>
	<cfset rollend = "12/31/#year(now())#">
</cfif>
</cfif>
<cfparam name="sortordby" default="incnum">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="Yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="potentialrating" default="All">
<cfparam name="occillcat" default="All">

<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, YEAR(LaborHours.WEEK_ENDING_DATE) AS hryr, MONTH(LaborHours.WEEK_ENDING_DATE) AS hrmon, 
                         SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE laborhours.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>

<cfelse>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>

<cfif listfindnocase(frmincassignto,"All") eq 0>
	<cfset needqryjde = "no">
	<cfloop list="#frmincassignto#" index="i">
		<cfif listfind("3,4,11",i) eq 0>
			<cfset needqryjde = "yes">
			<cfbreak>
		</cfif>
	</cfloop>
	<cfif needqryjde eq "no">
		and 1 = 2
	</cfif>
</cfif>
<cfif trim(contractlist) neq ''>
	and 1 = 2
</cfif>
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif> --->

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif> --->
	 
	<cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Groups.Group_Number, Groups.Group_Name, YEAR(LaborHours.WEEK_ENDING_DATE), MONTH(LaborHours.WEEK_ENDING_DATE), 
                         GroupLocations.LocationDetailID
ORDER BY hryr, hrmon, Groups.Group_Number
</cfquery>

<cfset manhrstruct = {}>
<cfset manhrstructoffshore = {}>
<cfloop query="getjdehrs">
	<cfif not structkeyexists(manhrstruct,"#hryr#_#hrmon#")>
		<cfset manhrstruct["#hryr#_#hrmon#"] = jdehrs>
	<cfelse>
		<cfset manhrstruct["#hryr#_#hrmon#"] = manhrstruct["#hryr#_#hrmon#"] + jdehrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif not structkeyexists(manhrstruct,"#hryr#_#hrmon#")>
			<cfset manhrstruct["#hryr#_#hrmon#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#hryr#_#hrmon#"] = manhrstruct["#hryr#_#hrmon#"] + jdehrs>
		</cfif>
		<!--- <cfif not structkeyexists(manhrstructoffshore,"#hryr#_#hrmon#")>
			<cfset manhrstructoffshore["#hryr#_#hrmon#"] = jdehrs>
		<cfelse>
			<cfset manhrstructoffshore["#hryr#_#hrmon#"] = manhrstructoffshore["#hryr#_#hrmon#"] + jdehrs>
		</cfif> --->
	</cfif>
</cfloop>

<cfquery name="getjdeoh" datasource="#request.dsn#">
SELECT        OfficeLaborMap.Group_Number, YEAR(LaborHoursOffice.WEEK_ENDING_DATE) AS ohyr, MONTH(LaborHoursOffice.WEEK_ENDING_DATE) AS ohmon, 
                         SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE LaborHoursOffice.WEEK_ENDING_DATE  between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#">    
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>

<cfelse>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>

<cfif listfindnocase(frmincassignto,"All") eq 0>
	<cfset needqryjde = "no">
	<cfloop list="#frmincassignto#" index="i">
		<cfif listfind("3,4,11",i) eq 0>
			<cfset needqryjde = "yes">
			<cfbreak>
		</cfif>
	</cfloop>
	<cfif needqryjde eq "no">
		and 1 = 2
	</cfif>
</cfif>

<cfif trim(contractlist) neq ''>
	and 1 = 2
</cfif>
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif> --->

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif> --->

				 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY OfficeLaborMap.Group_Number, YEAR(LaborHoursOffice.WEEK_ENDING_DATE), MONTH(LaborHoursOffice.WEEK_ENDING_DATE), 
                         GroupLocations.LocationDetailID
ORDER BY ohyr, ohmon
</cfquery>

<cfloop query="getjdeoh">
	<cfif not structkeyexists(manhrstruct,"#ohyr#_#ohmon#")>
		<cfset manhrstruct["#ohyr#_#ohmon#"] = oh>
	<cfelse>
		<cfset manhrstruct["#ohyr#_#ohmon#"] = manhrstruct["#ohyr#_#ohmon#"] + oh>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif not structkeyexists(manhrstruct,"#ohyr#_#ohmon#")>
			<cfset manhrstruct["#ohyr#_#ohmon#"] = oh>
		<cfelse>
			<cfset manhrstruct["#ohyr#_#ohmon#"] = manhrstruct["#ohyr#_#ohmon#"] + oh>
		</cfif>
		<!--- <cfif not structkeyexists(manhrstructoffshore,"#ohyr#_#ohmon#")>
			<cfset manhrstructoffshore["#ohyr#_#ohmon#"] = oh>
		<cfelse>
			<cfset manhrstructoffshore["#ohyr#_#ohmon#"] = manhrstructoffshore["#ohyr#_#ohmon#"] + oh>
		</cfif> --->
	</cfif>
</cfloop>