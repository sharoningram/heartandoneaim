<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        YEAR(LaborHours.WEEK_ENDING_DATE) AS hryr, SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, 
                         Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID


<cfif trim(startdate) neq ''>
	WHERE laborhours.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and laborhours.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
WHERE      year(laborhours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
</cfif>
</cfif>
<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY YEAR(LaborHours.WEEK_ENDING_DATE), Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
ORDER BY hryr, Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>

<cfquery name="getjdeoh" datasource="#request.dsn#">
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, 
                         YEAR(LaborHoursOffice.WEEK_ENDING_DATE) AS ohyr
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID

<cfif trim(startdate) neq ''>
	WHERE LaborHoursOffice.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHoursOffice.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
WHERE      year(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
</cfif>
</cfif>
<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, YEAR(LaborHoursOffice.WEEK_ENDING_DATE)
ORDER BY ohyr, Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>