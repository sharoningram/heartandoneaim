<cfparam name="qrygou" default="">
<cfif isdefined("getOU.recordcount")>
<cfset qrygou = valuelist(getOU.id)>
</cfif>
<cfif isdefined("ou")>
<cfif listfindnocase(ou,"All") eq 0>
	<cfset qrygou = ou>
</cfif>
</cfif>

<cfquery name="getgroups" datasource="#request.dsn#">
SELECT        Group_Number, Group_Name, OUID, Business_line_id
FROM            Groups
WHERE      1 = 1 <cfif trim(qrygou) neq ''> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qrygou#" list="yes">)</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0>
<!--- <cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
	<!---  and groups.group_number in (SELECT        Groups.Group_Number
FROM            UserRoles INNER JOIN
                         Groups ON UserRoles.AssignedLocs = Groups.Business_Line_ID
WHERE        (UserRoles.UserID = 353) AND (UserRoles.UserRole = 'BU Admin')) --->
</cfif> --->
<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer')
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
	 and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
</cfif>
<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and  listfindnocase(request.userlevel,"OU View Only") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
	 and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Reviewer')
</cfif>
<cfif  listfindnocase(request.userlevel,"User") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Reviewer") eq 0 and listfindnocase(request.userlevel,"OU View Only") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"BU View Only") eq 0>
		and Group_Number in (SELECT        Group_Number
from groups 
WHERE Active_Group = 1
and  groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserOUs#" list="Yes">)
		<!--- and group_number in (SELECT        GroupNumber
FROM            GroupUserAssignment
WHERE        (status = 1) AND (GroupRole = 'dataentry') AND (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">)) --->)
</cfif>
</cfif>
ORDER BY Group_Name
</cfquery>