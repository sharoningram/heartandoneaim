<cfparam name="qryclou" default="">
<cfif isdefined("getOU.recordcount")>
<cfset qryclou = valuelist(getOU.id)>
</cfif>
<cfif isdefined("ou")>
<cfif listfindnocase(ou,"All") eq 0>
	<cfset qryclou = ou>
</cfif>
</cfif>
<cfquery name="getclients" datasource="#request.dsn#">
SELECT DISTINCT Clients.ClientID, Clients.ClientName, Clients.status
FROM            Groups INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber RIGHT OUTER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID
WHERE   <cfif fusebox.fuseaction eq "clientperf">1 = 1 <cfelse>(Clients.status = 1) </cfif>

<cfif trim(qryclou) neq ''> and  groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryclou#" list="yes">)</cfif>
<!--- <cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
</cfif> --->
<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer')
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
	 and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
</cfif>
<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"OU View Only") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
	 and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Reviewer')
</cfif>
<cfif  listfindnocase(request.userlevel,"User") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Reviewer") eq 0 and listfindnocase(request.userlevel,"OU View Only") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and listfindnocase(request.userlevel,"BU View Only") eq 0>
		and groups.Group_Number in (SELECT        Group_Number
from groups 
WHERE Active_Group = 1
and  groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserOUs#" list="Yes">)
		<!--- and group_number in (SELECT        GroupNumber
FROM            GroupUserAssignment
WHERE        (status = 1) AND (GroupRole = 'dataentry') AND (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">)) --->)
</cfif>
ORDER BY Clients.ClientName
</cfquery>