<cfquery name="getincidenttypes" datasource="#request.dsn#">
SELECT        IncTypeID, IncType, Status, SortOrd
FROM            IncidentTypes
WHERE        (Status = 1)
ORDER BY SortOrd
</cfquery>
<cfquery name="getoshacatlists" datasource="#request.dsn#">
SELECT        CatID, Category
FROM            OSHACategories
WHERE        (Status = 1)
</cfquery>
<cfquery name="getad" datasource="#request.dsn#">
SELECT        DamageSourceID, DamageSource, status
FROM            DamageSources
WHERE        (status = 1)
</cfquery>
<cfquery name="getenv" datasource="#request.dsn#">
SELECT        EnvIncCateID, EnvIncCategory, status
FROM            EnvironmentIncCategories
WHERE        (status = 1)
</cfquery>
<cfquery name="getsec" datasource="#request.dsn#">
SELECT        SecIncCatID, SecIncCategory, Status
FROM            SecurityIncCategory
WHERE        (Status = 1)
</cfquery>
<cfset ctr = 0>

<cfset getinctypelist = QueryNew("inctypeid, inctype, incsubtypeid, incsubtype, sortord")>
<cfoutput query="getincidenttypes">
	<cfif IncTypeID eq 1 or IncTypeID eq 5>
		
		<cfloop query="getoshacatlists">
		<cfset ctr = ctr+1>
			<cfset QueryAddRow( getinctypelist ) />
			<cfset getinctypelist["inctypeid"][ctr] = getincidenttypes.IncTypeID />
			<cfset getinctypelist["inctype"][ctr] = getincidenttypes.IncType>
			<cfset getinctypelist["incsubtypeid"][ctr] = getoshacatlists.CatID>
			<cfset getinctypelist["incsubtype"][ctr] = getoshacatlists.Category>
			<cfif getincidenttypes.IncTypeID eq 1><cfset getinctypelist["sortord"][ctr] = 1><cfelse><cfset getinctypelist["sortord"][ctr] = 2></cfif>
		</cfloop>
	<cfelseif inctypeid eq 2>
		<cfloop query="getenv">
		<cfset ctr = ctr+1>
			<cfset QueryAddRow( getinctypelist ) />
			<cfset getinctypelist["inctypeid"][ctr] = getincidenttypes.IncTypeID />
			<cfset getinctypelist["inctype"][ctr] = getincidenttypes.IncType>
			<cfset getinctypelist["incsubtypeid"][ctr] = getenv.EnvIncCateID>
			<cfset getinctypelist["incsubtype"][ctr] = getenv.EnvIncCategory>
			<cfset getinctypelist["sortord"][ctr] = 3>
		</cfloop>
	<cfelseif inctypeid eq 3>
		<cfloop query="getad">
		<cfset ctr = ctr+1>
			<cfset QueryAddRow( getinctypelist ) />
			<cfset getinctypelist["inctypeid"][ctr] = getincidenttypes.IncTypeID />
			<cfset getinctypelist["inctype"][ctr] = getincidenttypes.IncType>
			<cfset getinctypelist["incsubtypeid"][ctr] = getad.DamageSourceID>
			<cfset getinctypelist["incsubtype"][ctr] = getad.DamageSource>
			<cfset getinctypelist["sortord"][ctr] = 4>
		</cfloop>
	
	<cfelseif inctypeid eq 4>
		<cfloop query="getsec">
		<cfset ctr = ctr+1>
			<cfset QueryAddRow( getinctypelist ) />
			<cfset getinctypelist["inctypeid"][ctr] = getincidenttypes.IncTypeID />
			<cfset getinctypelist["inctype"][ctr] = getincidenttypes.IncType>
			<cfset getinctypelist["incsubtypeid"][ctr] = getsec.SecIncCatID>
			<cfset getinctypelist["incsubtype"][ctr] = getsec.SecIncCategory>
			<cfset getinctypelist["sortord"][ctr] = 5>
		</cfloop>
	</cfif>

</cfoutput>
<cfquery name="getsortedincs" dbtype="query">
select inctypeid,inctype,incsubtypeid,incsubtype
from getinctypelist
order by sortord
</cfquery>