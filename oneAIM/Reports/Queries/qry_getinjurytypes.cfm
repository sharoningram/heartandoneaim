<cfset injlist = "">
<cfloop query="getoshachart">
	<cfloop list="#injuryType#" index="i">
		<cfif listfind(injlist,i) eq 0>
			<cfset injlist = listappend(injlist,i)>
		</cfif>
	</cfloop>
</cfloop>
<cfif trim(injlist) eq ''>
	<cfset injlist = 0>
</cfif>

<cfquery name="getinjurytypes" datasource="#request.dsn#">
SELECT        InjTypeID, InjuryType, Status
FROM            InjuryTypes
WHERE        (Status = 1) and InjTypeID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#injlist#" list="Yes">)
</cfquery>