<cfparam name="sortordby" default="incnum">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="all">
<cfparam name="frmjv" default="all">
<cfparam name="frmsubcontractor" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="irnumber" default="">
<cfparam name="incandor" default="And">
<cfparam name="searchtype" default="basic">
<cfparam name="potentialrating" default="All">
<cfparam name="INCNM" default="yes">

<cfset contractlist = "">
<cfif listfindnocase(frmmgdcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmmgdcontractor)>
</cfif>
<cfif listfindnocase(frmjv,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmjv)>
</cfif>
<cfif listfindnocase(frmsubcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmsubcontractor)>
	</cfif>
<cfset tnumbers = "">
		<cfloop list="#irnumber#" index="i">
			<cfif isnumeric(i)>
				<cfset tnumbers = listappend(tnumbers,i)>
			</cfif>
		</cfloop>
<cfset irnumber = tnumbers>
<cfquery name="getincidents" datasource="#request.dsn#">
SELECT        oneAIMincidents.isFatality, oneAIMincidents.IRN, oneAIMincidents.TrackingNum, Groups.Group_Name, NewDials_1.Name AS ouname, oneAIMincidents.incidentDate,
                          IncidentTypes.IncType, OSHACategories.Category, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.Status as incstat, oneAIMincidents.createdbyEmail,
                         oneAIMincidents.isNearMiss, NewDials.Name AS buname,CASE WHEN oneAimFirstAlerts.Status IS NULL 
                         THEN 'FA Prepare' WHEN oneAimFirstAlerts.Status = 'Started' THEN 'FA Prepare' ELSE oneAimFirstAlerts.Status END AS status
FROM            oneAIMEnvironmental RIGHT OUTER JOIN
                         IncidentTypes RIGHT OUTER JOIN
                         oneAimFirstAlerts RIGHT OUTER JOIN
                         oneAIMincidents ON oneAimFirstAlerts.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID ON IncidentTypes.IncTypeID = oneAIMincidents.PrimaryType LEFT OUTER JOIN
                         oneAIMassetDamage ON oneAIMincidents.IRN = oneAIMassetDamage.IRN ON oneAIMEnvironmental.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         NewDials INNER JOIN
                         NewDials AS NewDials_1 INNER JOIN
                         Groups ON NewDials_1.ID = Groups.OUid ON NewDials.ID = Groups.Business_Line_ID ON 
                         oneAIMincidents.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         oneAIMSecurity ON oneAIMincidents.IRN = oneAIMSecurity.IRN LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE  isworkrelated = 'yes'  AND (oneAIMincidents.needFA = 1)

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif listfindnocase(oshaclass,"All") eq 0>
	<cfif listfindnocase(oshaclass,"All Recordable") gt 0>
		<cfset ocl = "">
		<cfloop list="#oshaclass#" index="x">
			<cfif listfindnocase("All,All Recordable",x) eq 0>
				<cfset ocl = listappend(ocl,x)>
			</cfif>
		</cfloop>
		<cfif trim(ocl) neq ''>
		and oneAIMInjuryOI.OSHAclass in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ocl#" list="Yes">) and (oneAIMincidents.primarytype in (1,5) or oneAIMincidents.secondarytype in (1,5))
		<cfelse>
		AND	( ((oneAIMincidents.PrimaryType = 1) AND (oneAIMInjuryOI.OSHAclass IN (2, 3, 4)) OR 
				 (oneAIMincidents.PrimaryType = 5) AND oneaimincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (2, 3, 4))) or oneAIMincidents.isFatality = 'Yes')
		</cfif>
	<cfelse>
		and oneAIMInjuryOI.OSHAclass in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oshaclass#" list="Yes">) and (oneAIMincidents.primarytype in (1,5) or oneAIMincidents.secondarytype in (1,5))
	</cfif>
</cfif>

<!--- <cfif incandor eq "And"> --->
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">) 
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>
	)
</cfif>
	




<cfif searchtype eq "advanced">





<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>


<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<!--- <cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif INCNM eq "no">
	<cfif listfindnocase(oshaclass,"All Recordable") gt 0>
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
	<cfelse>
	and oneAIMincidents.isnearmiss = 'no'
	</cfif>
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<!--- <cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif> --->

<!--- <cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif> --->

 <cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif listfindnocase(seriousinj,"All") eq 0>
	and oneAIMInjuryOI.isserious in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#seriousinj#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(secinccats,"All") eq 0>
	and oneAIMSecurity.secinccat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#secinccats#" list="Yes">)
</cfif>
<cfif listfindnocase(eic,"All") eq 0>
	and oneAIMEnvironmental.envinccat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#eic#" list="Yes">)
</cfif>
<cfif listfindnocase(damagesrc,"All") eq 0>
	and oneAIMassetDamage.DamageSource in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#damagesrc#" list="Yes">)
</cfif> --->

 </cfif>

<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
<cfelseif  listfindnocase(request.userlevel,"Senior Executive View") gt 0>
<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0  or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0>
		and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
			and  oneAIMincidents.isFatality = 'no'
<cfelseif listfindnocase(request.userlevel,"User") gt 0>
	<!--- and oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#request.userlogin#"> --->
	and oneAIMincidents.groupnumber in (SELECT DISTINCT GroupNumber
FROM            GroupUserAssignment
WHERE     (status = 1) AND (GroupRole = 'dataentry') and    (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (Groups.OUid IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)))
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="incnum">
ORDER BY oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="bu">
ORDER BY buname, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="ou">
ORDER BY ouname, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="incdate">
ORDER BY oneAIMincidents.incidentdate,oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="inctype">
ORDER BY IncidentTypes.IncType, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="prj">
ORDER BY Groups.Group_Name, oneAIMincidents.TrackingNum desc
</cfcase>

<cfcase value="osha">
ORDER BY OSHACategories.Category, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="potrate">
ORDER BY oneAIMincidents.PotentialRating, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="shortdesc">
ORDER BY oneAIMincidents.ShortDesc, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="recstat">
ORDER BY Status, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="nearmiss">
ORDER BY oneAIMincidents.isnearmiss, oneAIMincidents.TrackingNum desc
</cfcase>
</cfswitch>

</cfquery>
