<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="Yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="Yes">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="chartvalue" default="">
<cfparam name="charttype" default="No">
<cfparam name="searchname" default="">
<cfparam name="potentialrating" default="All">
<cfparam name="occillcat" default="All">

<cfparam name="irnumber" default="">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="beentoirp" default="No">

<cfset showouonly = "yes">
<cfset qrymonth = dateadd("m",-1,now())>

<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>

<cfquery name="getnumemplyed" datasource="#request.dsn#">
SELECT         Population.PopRecID, Population.GroupNumber, Population.NumEmployedAFW, Population.NumEmployedMC, Population.NumEmployedSub, 
                         Population.NumEmployedJV, Population.LocationID, Population.isActive, Groups.Group_Number, Groups.Group_Name, Groups.OUid, NewDials.Name AS ouname, 
                         NewDials_1.Name AS buname, Groups.Business_Line_ID AS buid
FROM            Population INNER JOIN
                         GroupLocations ON Population.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Groups ON GroupLocations.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID
<!--- WHERE        (Population.isActive = 1) AND (YEAR(Population.PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">) --->
WHERE month(Population.PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#month(qrymonth)#"> AND (YEAR(Population.PopDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(qrymonth)#">) 
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
order by buname, ouname, group_name
</cfquery>