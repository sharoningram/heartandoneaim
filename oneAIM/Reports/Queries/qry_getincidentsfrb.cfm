<cfquery name="getincs" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt,YEAR(oneAIMincidents.incidentDate) AS incyr, oneAIMInjuryOI.OSHAclass, Groups.Business_Line_ID,Groups.OUid
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.isnearmiss = 'No' and   oneAIMincidents.isworkrelated = 'yes' and    (oneAIMincidents.Status <> 'Cancel')  
                        
						 
	<cfif trim(startdate) neq ''>
	AND oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
 AND YEAR(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
 </cfif>
</cfif>
						 
						 AND 
((oneAIMincidents.PrimaryType = 1)  AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4))
OR (oneAIMincidents.PrimaryType = 5) AND oneAIMincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))						 
<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY YEAR(oneAIMincidents.incidentDate) ,Groups.Business_Line_ID, Groups.OUid, oneAIMInjuryOI.OSHAclass
ORDER BY  incyr,Groups.Business_Line_ID, Groups.OUid, oneAIMInjuryOI.OSHAclass
</cfquery>

<cfquery name="getfatalities" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, YEAR(oneAIMincidents.incidentDate) AS incyr, Groups.Business_Line_ID, Groups.OUid
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE     oneAIMincidents.isworkrelated = 'yes' and     (oneAIMincidents.Status <> 'Cancel') 

<cfif trim(startdate) neq ''>
	AND oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
 AND YEAR(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
 </cfif>
</cfif>

AND (oneAIMincidents.isFatality = 'yes')
<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY YEAR(oneAIMincidents.incidentDate), Groups.Business_Line_ID,Groups.OUid
ORDER BY incyr, Groups.Business_Line_ID,Groups.OUid
</cfquery>