
<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, YEAR(ContractorHours.WeekEndingDate) AS conyr, MONTH(ContractorHours.WeekEndingDate) AS conmon, 
                         GroupLocations.LocationDetailID
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE  ContractorHours.WeekEndingDate  between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#">
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
<cfelse>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif> --->


<cfif trim(contractlist) neq ''>
	and Contractors.ContractorID in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>

<cfif listfindnocase(frmincassignto,"All") eq 0>
	<cfset cotypelist = "">
		
	<cfif listfindnocase(frmincassignto,3) gt 0>
		<cfset cotypelist = listappend(cotypelist,"sub")>
	</cfif>
	<cfif listfindnocase(frmincassignto,4) gt 0>
		<cfset cotypelist = listappend(cotypelist,"mgd")>
	</cfif>
	<cfif listfindnocase(frmincassignto,11) gt 0>
		<cfset cotypelist = listappend(cotypelist,"jv")>
	</cfif>
	<cfif trim(cotypelist) neq ''>
		and Contractors.cotype in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cotypelist#" list="Yes">)
	<cfelse>
		and 1 = 2
	</cfif>
	
</cfif>
</cfif>
<!--- <cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif> --->
				 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(ContractorHours.WeekEndingDate), MONTH(ContractorHours.WeekEndingDate), GroupLocations.LocationDetailID
ORDER BY conyr, conmon
</cfquery>

<cfloop query="getcontractorhrs">
	<cfif not structkeyexists(manhrstruct,"#conyr#_#conmon#")>
		<cfset manhrstruct["#conyr#_#conmon#"] = conhrs>
	<cfelse>
		<cfset manhrstruct["#conyr#_#conmon#"] = manhrstruct["#conyr#_#conmon#"] + conhrs>
	</cfif>
	<cfif LocationDetailID eq 4>
	<cfif not structkeyexists(manhrstruct,"#conyr#_#conmon#")>
		<cfset manhrstruct["#conyr#_#conmon#"] = conhrs>
	<cfelse>
		<cfset manhrstruct["#conyr#_#conmon#"] = manhrstruct["#conyr#_#conmon#"] + conhrs>
	</cfif>
		<!--- <cfif not structkeyexists(manhrstructoffshore,"#conyr#_#conmon#")>
			<cfset manhrstructoffshore["#conyr#_#conmon#"] = conhrs>
		<cfelse>
			<cfset manhrstructoffshore["#conyr#_#conmon#"] = manhrstructoffshore["#conyr#_#conmon#"] + conhrs>
		</cfif> --->
	</cfif>
</cfloop>