
<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Contractors.CoType, Groups.Group_Number,GroupLocations.GroupLocID
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE 1 = 1

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ContractorHours.WEEKENDINGDATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and ContractorHours.WEEKENDINGDATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and ContractorHours.WEEKENDINGDATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
	<cfif incyear neq "All">
	and      year(ContractorHours.WEEKENDINGDATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
	</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>			 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

GROUP BY Contractors.CoType, GroupLocations.LocationDetailID, Groups.Group_Number,GroupLocations.GroupLocID
ORDER BY Contractors.CoType, Groups.Group_Number
</cfquery>
