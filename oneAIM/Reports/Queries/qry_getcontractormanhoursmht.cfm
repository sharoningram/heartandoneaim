
<cfparam name="incyear" default="#year(now())#">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, Contractors.CoType,  ContractorHours.GroupNumber as group_number, GroupLocations.GroupLocID
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
<!--- WHERE        (YEAR(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">) --->
<cfif trim(startdate) neq ''>
	WHERE ContractorHours.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and ContractorHours.WeekEndingDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		WHERE (YEAR(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">)
	</cfif>
</cfif>
<cfif trim(locdetail) neq ''>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
</cfif>
<cfif manhrtype eq "JDE only">
and groups.erpsys = 'JDE'
<cfelseif manhrtype eq "Manual only">
and groups.erpsys = 'none' 
</cfif>

<!--- <cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Contractors.CoType, ContractorHours.GroupNumber, GroupLocations.GroupLocID
ORDER BY Contractors.CoType, Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>