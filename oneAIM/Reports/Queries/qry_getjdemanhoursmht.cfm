
<cfparam name="incyear" default="#year(now())#">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="manhrtype" default="All">
<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, Groups.Business_Line_ID, Groups.OUid, 
                         GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
<!--- WHERE        (YEAR(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">) --->
<cfif trim(startdate) neq ''>
	WHERE LaborHours.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHours.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		WHERE (YEAR(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">)
	</cfif>
</cfif>
<cfif trim(locdetail) neq ''>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
</cfif>
<cfif manhrtype eq "JDE only">
and groups.erpsys = 'JDE'
<cfelseif manhrtype eq "Manual only">
and groups.erpsys = 'none' 
</cfif>

<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID
ORDER BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID
</cfquery>

<cfquery name="getjdeoh" datasource="#request.dsn#">
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
<cfif trim(startdate) neq ''>
	WHERE LaborHoursOffice.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHoursOffice.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		WHERE (YEAR(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">)
	</cfif>
</cfif>

<cfif trim(locdetail) neq ''>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
</cfif>

<cfif manhrtype eq "JDE only">
and groups.erpsys = 'JDE'
<cfelseif manhrtype eq "Manual only">
and groups.erpsys = 'none'  
</cfif>

<!--- WHERE        (YEAR(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">) --->
<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID
ORDER BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID
</cfquery>