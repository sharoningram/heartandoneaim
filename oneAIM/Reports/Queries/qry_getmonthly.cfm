<cfquery name="getmonthlyincs" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) AS incmon, 
                         oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.isnearmiss = 'No' and   oneAIMincidents.isworkrelated = 'yes' and    (oneAIMincidents.Status <> 'Cancel') AND oneAIMincidents.incidentDate between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> AND 
((oneAIMincidents.PrimaryType = 1)  AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4))
OR (oneAIMincidents.PrimaryType = 5) AND oneAIMincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
<cfelse>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif> --->

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
</cfif>



<cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating
ORDER BY incyr, incmon
</cfquery>

<cfset LTIstruct = {}>
<cfset FAstruct = {}>
<cfset TRIstruct = {}>


<cfloop query="getmonthlyincs">
	<cfif OSHAclass eq 1>
		<cfif not structkeyexists(FAstruct,"#incyr#_#incmon#")>
			<cfset FAstruct["#incyr#_#incmon#"] = inccnt>
		<cfelse>
			<cfset FAstruct["#incyr#_#incmon#"] = FAstruct["#incyr#_#incmon#"]+inccnt>
		</cfif>
	</cfif>
	<cfif OSHAclass eq 2>
		<cfif not structkeyexists(LTIstruct,"#incyr#_#incmon#")>
			<cfset LTIstruct["#incyr#_#incmon#"] = inccnt>
		<cfelse>
			<cfset LTIstruct["#incyr#_#incmon#"] = LTIstruct["#incyr#_#incmon#"]+inccnt>
		</cfif>
	</cfif>
	<cfif listfind("2,3,4",OSHAclass) gt 0>
		<cfif not structkeyexists(TRIstruct,"#incyr#_#incmon#")>
			<cfset TRIstruct["#incyr#_#incmon#"] = inccnt>
		<cfelse>
			<cfset TRIstruct["#incyr#_#incmon#"] = TRIstruct["#incyr#_#incmon#"]+inccnt>
		</cfif>
	</cfif>
<!--- 	<cfif listfindnocase("E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5",PotentialRating) gt 0>
		<cfif not structkeyexists(HiPostruct,"#incyr#_#incmon#")>
			<cfset HiPostruct["#incyr#_#incmon#"] = inccnt>
		<cfelse>
			<cfset HiPostruct["#incyr#_#incmon#"] = HiPostruct["#incyr#_#incmon#"]+inccnt>
		</cfif>
	</cfif> --->
</cfloop>

<cfquery name="getfatalities" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) AS incmon, 
                         oneAIMincidents.PotentialRating
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE     oneAIMincidents.isworkrelated = 'yes' and   (oneAIMincidents.Status <> 'Cancel') AND oneAIMincidents.incidentDate between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> AND (oneAIMincidents.isFatality = 'Yes')

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
<cfelse>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>  --->

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

</cfif>
<cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), oneAIMincidents.PotentialRating
ORDER BY incyr, incmon
</cfquery>

<cfset Fatalitystruct = {}>
<cfloop query="getfatalities">
	
		<cfif not structkeyexists(Fatalitystruct,"#incyr#_#incmon#")>
			<cfset Fatalitystruct["#incyr#_#incmon#"] = inccnt>
		<cfelse>
			<cfset Fatalitystruct["#incyr#_#incmon#"] = Fatalitystruct["#incyr#_#incmon#"]+inccnt>
		</cfif>
	
</cfloop>





<cfquery name="getHiPo" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) AS incmon, 
                         oneAIMincidents.PotentialRating
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE     oneAIMincidents.isworkrelated = 'yes' and   (oneAIMincidents.Status <> 'Cancel') AND oneAIMincidents.incidentDate between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> AND 
                         (oneAIMincidents.PotentialRating IN ('E3', 'A4', 'B4', 'C4', 'D4', 'E4', 'A5', 'B5', 'C5', 'D5', 'E5'))

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
<cfelse>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>  --->

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

</cfif>
<cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), oneAIMincidents.PotentialRating
ORDER BY incyr, incmon
</cfquery>

<cfset HiPostruct = {}>
<cfloop query="getHiPo">
	
		<cfif not structkeyexists(HiPostruct,"#incyr#_#incmon#")>
			<cfset HiPostruct["#incyr#_#incmon#"] = inccnt>
		<cfelse>
			<cfset HiPostruct["#incyr#_#incmon#"] = HiPostruct["#incyr#_#incmon#"]+inccnt>
		</cfif>
	
</cfloop>
