<cfparam name="rep" default="0">
<cfquery name="getreport" datasource="#request.dsn#">
SELECT        UserReportID, SearchName, UserID, incyear, invlevel, bu, ou, bs, project, siteoffice, mgdcontractor, jv, subcon, clientid, locdetail, countryid, incassigned, wronly, 
                         primarytype, secondtype, hipoony, oshaclass, oidef, seriousinj, secinccats, eic, damagesrc, startdate, enddate, DateSaved, SaveCtr, reportType, incnm, charttype, 
                         chartvalue, assocgroups, potentialrating,occillcat,irnumber,keywords,incstatus,manhrtype,beentoirp,apptype,capatype,capastatus,frminjtype,frmbodypart,frminjcause,droppedobj,weightedhours,incsub
FROM            UserSavedReports
WHERE        (UserReportID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#rep#">)
</cfquery>