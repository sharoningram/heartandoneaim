<cfparam name="projectoffice" default="all">
<cfparam name="qrypoou" default="">
<cfif isdefined("getOU.recordcount")>
<cfset qrypoou = valuelist(getOU.id)>
</cfif>
<cfif isdefined("ou")>
<cfif listfindnocase(ou,"All") eq 0>
	<cfset qrypoou = ou>
</cfif>
</cfif>
<cfquery name="getsites" datasource="#request.dsn#">
SELECT        GroupLocID, SiteName, isActive,GroupNumber
FROM            GroupLocations
WHERE      1 = 1 <cfif trim(qrypoou) neq ''>AND (GroupNumber IN
                             (SELECT        Group_Number
                               FROM            Groups
                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qrypoou#" list="Yes">))))</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	and GroupNumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#PROJECTOFFICE#" list="Yes">)
<cfelse>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0  and listfindnocase(request.userlevel,"Senior Reviewer") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0 >
<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	AND (GroupNumber IN
                             (SELECT        Group_Number
                               FROM            Groups
                               WHERE        (business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">))))
	
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0>
	AND (GroupNumber IN
                             (SELECT        Group_Number
                               FROM            Groups
                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
		
</cfif>
<cfif  listfindnocase(request.userlevel,"User") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Reviewer") eq 0 and listfindnocase(request.userlevel,"OU View Only") eq 0  and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"BU View Only") eq 0>
AND (GroupNumber IN
                             (SELECT        Group_Number
                               FROM            Groups
                               WHERE        (ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">))))
		
</cfif>
</cfif>
</cfif>
ORDER BY SiteName
</cfquery>

