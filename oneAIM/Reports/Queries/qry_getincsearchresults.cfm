<cfparam name="sortordby" default="incnum">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="all">
<cfparam name="frmjv" default="all">
<cfparam name="frmsubcontractor" default="all">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="irnumber" default="">
<cfparam name="incandor" default="And">
<cfparam name="searchtype" default="basic">
<cfparam name="potentialrating" default="All">
<cfparam name="INCNM" default="no">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="gsr" default="All">
<cfparam name="seb" default="All">

<cfparam name="frminjtype" default="all">
<cfparam name="frmbodypart" default="all">
<cfparam name="frminjcause" default="all">
<cfparam name="droppedobj" default="All">


<cfset injtypeirnlist = "">
<cfset bpirnlist = "">
<cfset injcauseirnlist = "">

<cfset contractlist = "">
<cfif listfindnocase(frmmgdcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmmgdcontractor)>
</cfif>
<cfif listfindnocase(frmjv,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmjv)>
</cfif>
<cfif listfindnocase(frmsubcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmsubcontractor)>
	</cfif>
<cfset tnumbers = "">
		<cfloop list="#irnumber#" index="i">
			<cfif isnumeric(i)>
				<cfset tnumbers = listappend(tnumbers,i)>
			</cfif>
		</cfloop>
<cfset irnumber = tnumbers>
<cfset pendirnlist = "">

<!--- <cfif listfindnocase(incstatus,"All") gt 0 or  listfindnocase(incstatus,"Pending Closure") gt 0 > --->
<cfquery name="getpendingirns" datasource="#request.dsn#">
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE      (IncidentInvestigation.Status = 'IRP Pending') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5)) and oneAIMincidents.withdrawnto is null 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
						 UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE        <!--- (oneAIMincidents.isWorkRelated = 'yes') AND ---> (IncidentInvestigation.Status = 'IRP Pending') AND (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMInjuryOI.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5)) and oneAIMincidents.withdrawnto is null 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
						 UNION
                         SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE        <!--- (oneAIMincidents.isWorkRelated = 'yes') AND ---> (oneAIMCAPA.DateComplete IS NULL) AND (IncidentInvestigation.Status = 'IRP Pending') and oneAIMincidents.withdrawnto is null 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>

<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
 UNION
SELECT     oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    (oneAIMincidents.isWorkRelated = 'no') AND (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                        (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5)) and oneAIMincidents.withdrawnto is null 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
 
UNION
SELECT     oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE    (IncidentInvestigation.Status = 'Approved') AND (oneAIMincidents.isWorkRelated = 'yes') AND (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) and oneAIMincidents.withdrawnto is null AND 
                        (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5)) 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
 UNION
SELECT        oneAIMincidents_1.irn
FROM            oneAIMincidents AS oneAIMincidents_1 INNER JOIN
                         oneAIMInjuryOI AS oneAIMInjuryOI_1 ON oneAIMincidents_1.IRN = oneAIMInjuryOI_1.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents_1.IRN = IncidentInvestigation.IRN
WHERE      (IncidentInvestigation.Status = 'approved') and oneAIMincidents_1.withdrawnto is null AND (oneAIMincidents_1.isWorkRelated = 'yes')  and  (oneAIMincidents_1.Status = 'Review Completed') AND (oneAIMInjuryOI_1.OSHAclass = 2) AND (oneAIMInjuryOI_1.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents_1.PrimaryType IN (1, 5) or oneAIMincidents_1.SecondaryType in (1,5))
<!--- <cfif searchtype eq "basic">
and oneAIMincidents_1.isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents_1.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents_1.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents_1.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents_1.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents_1.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents_1.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents_1.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents_1.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents_1.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents_1.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents_1.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents_1.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents_1.isnearmiss = 'no' or oneAIMincidents_1.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents_1.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents_1.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents_1.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
 
 
UNION
SELECT        oneAIMincidents_1.irn
FROM            oneAIMincidents AS oneAIMincidents_1 INNER JOIN
                         oneAIMInjuryOI AS oneAIMInjuryOI_1 ON oneAIMincidents_1.IRN = oneAIMInjuryOI_1.IRN
WHERE      (oneAIMincidents_1.isWorkRelated = 'no') and oneAIMincidents_1.withdrawnto is null  and  (oneAIMincidents_1.Status = 'Review Completed') AND (oneAIMInjuryOI_1.OSHAclass = 2) AND (oneAIMInjuryOI_1.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents_1.PrimaryType IN (1, 5) or oneAIMincidents_1.SecondaryType in (1,5))
<!--- <cfif searchtype eq "basic">
and oneAIMincidents_1.isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents_1.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents_1.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents_1.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents_1.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents_1.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents_1.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents_1.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents_1.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents_1.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents_1.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents_1.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents_1.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents_1.isnearmiss = 'no' or oneAIMincidents_1.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents_1.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents_1.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents_1.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
 
 
 
UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN
WHERE        (oneAIMincidents.Status = 'Review Completed') AND (oneAIMincidents.isWorkRelated = 'no')  AND (oneAIMCAPA.DateComplete IS NULL) and oneAIMincidents.withdrawnto is null 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
 
 
 
 
 
 
 
UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE        (oneAIMincidents.Status = 'Review Completed') AND (oneAIMincidents.isWorkRelated = 'yes')  AND (IncidentInvestigation.Status = 'Approved') AND (oneAIMCAPA.DateComplete IS NULL) and oneAIMincidents.withdrawnto is null 
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
UNION
SELECT        oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status IN ('closed', 'Review Completed'))  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.needIRP = 1) AND ((oneAIMIncidentIRP.Status = 'Started') OR (oneAIMIncidentIRP.Status IS NULL))  AND (IncidentAudits.Status = 'Investigation Approved')
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
UNION
SELECT        oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status IN ('closed', 'Review Completed'))  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.needIRP = 1) AND ((oneAIMIncidentIRP.Status = 'Sent for Review'))  AND (IncidentAudits.Status = 'Investigation Approved')
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif> --->

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>


<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>

<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>


 </cfif>
</cfquery>
<cfset pendirnlist = valuelist(getpendingirns.irn)>
<cfif trim(pendirnlist) eq ''>
	<cfset pendirnlist = 0>
</cfif>
<!--- 
</cfif> --->




<cfset statirnlist = ''>

 <cfif listfindnocase(incstatus,"All") eq 0>
	<cfif listfindnocase(incstatus,"Awaiting Review") gt 0>
		<cfquery name="getirnar" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN
			WHERE    oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and     (oneAIMincidents.withdrawnto IS NULL) AND (oneAIMincidents.Status IN ('sent for review')) AND (IncidentAudits.Status = 'Sent for Review')
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirnar">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>

	<cfif listfindnocase(incstatus,"More Info Requested") gt 0>
		<cfquery name="getirnmir" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN
			WHERE  oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and      (oneAIMincidents.Status in ('More Info Requested')) AND (IncidentAudits.Status = 'More Info Requested')  and oneAIMincidents.withdrawnto is null
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirnmir">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>

	<cfif listfindnocase(incstatus,"Awaiting Investigation") gt 0>
		<cfquery name="getirnai" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
			WHERE  oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and   oneAIMincidents.isworkrelated = 'yes' and   oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed')) AND ((IncidentInvestigation.Status = 'started') OR (IncidentInvestigation.Status IS NULL)) AND (IncidentAudits.ActionTaken like 'Review Complete%')
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirnai">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>

	<cfif listfindnocase(incstatus,"Investigation Review") gt 0>
		<cfquery name="getirnirev" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
			WHERE   oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and    oneAIMincidents.isworkrelated = 'yes' and   oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed')) AND (IncidentInvestigation.Status in ('Sent for Review','Sent for Senior Review')) AND (IncidentAudits.Status = 'Investigation Sent for Review')
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirnirev">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>

	<cfif listfindnocase(incstatus,"Investigation Needs More Info") gt 0>
		<cfquery name="getirninmf" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
			WHERE oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and   oneAIMincidents.isworkrelated = 'yes' and     oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 'Review Completed')) AND (IncidentInvestigation.Status in ('More Info Requested','Senior More Info Requested')) 
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
                         AND (IncidentAudits.Status = 'Investigation Sent for More Information')
		</cfquery>
		<cfloop query="getirninmf">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>

	<cfif listfindnocase(incstatus,"Pending Closure") gt 0>
		<cfquery name="getirnpc" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents
			WHERE    oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">)  and oneAIMincidents.withdrawnto is null
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirnpc">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>
	
	<cfif listfindnocase(incstatus,"Withdrawn") gt 0>
		<cfquery name="getirnwd" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents
			WHERE oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and   oneAIMincidents.withdrawnto is not null 
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirnwd">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>
	
	<cfif listfindnocase(incstatus,"Closed") gt 0>
		<cfquery name="getirncld" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents
			WHERE   oneAIMincidents.status = 'Closed'
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirncld">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>
	
		<cfif listfindnocase(incstatus,"Cancel") gt 0>
		<cfquery name="getirncan" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents
			WHERE   oneAIMincidents.status = 'Cancel'
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirncan">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>
	
	<cfif listfindnocase(incstatus,"Initiated") gt 0 or listfindnocase(incstatus,"All") gt 0>
		<cfquery name="getirninti" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN
			FROM            oneAIMincidents
			WHERE  oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and  oneAIMincidents.status in ('Initiated','Started')
			
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>
		</cfquery>
		<cfloop query="getirninti">
			<cfif listfind(statirnlist,irn) eq 0>
				<cfset statirnlist = listappend(statirnlist,irn)>
			</cfif>
		</cfloop>
	</cfif>
	
	
	 </cfif>
<cfset tblUID = replace(createUUID(),"-","","all")>
<cfif trim(statirnlist) neq ''>
	<cfquery name="newtbl" datasource="#request.dsn#"> 
		 Create Table ##incsrchTemp#tblUID# (IRN int null) 
	</cfquery>
	<cfloop list="#statirnlist#" index="i">
		<cfquery name="addtotemp" datasource="#request.dsn#">
			insert into ##incsrchTemp#tblUID# 
                values (#i# )
		</cfquery>
	</cfloop>
	
</cfif>



	 <cfif listfindnocase(frminjtype,"All") eq 0>
<cfquery name="getinjtypeincs" datasource="#request.dsn#">
SELECT DISTINCT oneAIMincidents.IRN, oneAIMInjuryOI.injuryType
FROM            OSHACategories RIGHT OUTER JOIN
                         IncidentAudits RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID
WHERE    ((oneAIMincidents.PrimaryType = 1) OR
                         (oneAIMincidents.SecondaryType = 1))

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>		 
	
<cfif listfindnocase(incstatus,"All") eq 0>
	<cfif trim(statirnlist) neq ''>
		 and oneAIMincidents.irn in (select irn from ##incsrchTemp#tblUID#)
	<cfelse>
	and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="Yes">)
	</cfif>
</cfif> 
</cfquery>

<cfif getinjtypeincs.recordcount gt 0>
	<cfloop query="getinjtypeincs">
		<cfloop list="#injuryType#" index="i">
			<cfif listfind(frminjtype,i) gt 0>
				<cfset injtypeirnlist = listappend(injtypeirnlist,irn)>
			</cfif>
		</cfloop>
	</cfloop>
	<cfif trim(injtypeirnlist) eq ''>
		<cfset injtypeirnlist = 0>
	</cfif>
<cfelse>
	<cfset injtypeirnlist = 0>
</cfif>

</cfif>

	 


<cfif listfindnocase(frmbodypart,"All") eq 0>
<cfquery name="getbpirns" datasource="#request.dsn#">
SELECT DISTINCT oneAIMincidents.IRN, oneAIMInjuryOI.bodyPart
FROM            OSHACategories RIGHT OUTER JOIN
                         IncidentAudits RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID
WHERE     ((oneAIMincidents.PrimaryType = 1) OR
                         (oneAIMincidents.SecondaryType = 1))

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>	


<cfif listfindnocase(incstatus,"All") eq 0>
	<cfif trim(statirnlist) neq ''>
	
	 and oneAIMincidents.irn in (select irn from ##incsrchTemp#tblUID#)
	<cfelse>
	and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="Yes">)
	</cfif>
</cfif> 
	
</cfquery>
  
<cfif getbpirns.recordcount gt 0>
	<cfloop query="getbpirns">
		<cfloop list="#bodyPart#" index="i">
			<cfif listfind(frmbodypart,i) gt 0>
				<cfset bpirnlist = listappend(bpirnlist,irn)>
			</cfif>
		</cfloop>
	</cfloop>
	<cfif trim(bpirnlist) eq ''>
		<cfset bpirnlist = 0>
	</cfif>
<cfelse>
	<cfset bpirnlist = 0>
</cfif>

</cfif>






  <cfif listfindnocase(frminjcause,"All") eq 0>
<cfquery name="getinjcauseirns" datasource="#request.dsn#">
SELECT DISTINCT oneAIMincidents.IRN, oneAIMInjuryOI.injuryCause
FROM            OSHACategories RIGHT OUTER JOIN
                         IncidentAudits RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID
WHERE        (oneAIMincidents.PrimaryType = 1) OR
                         (oneAIMincidents.SecondaryType = 1)

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif searchtype eq "advanced">
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	)
</cfif>
<cfif INCNM eq "no">
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>
</cfif>	


<cfif listfindnocase(incstatus,"All") eq 0>
	<cfif trim(statirnlist) neq ''>
	 and oneAIMincidents.irn in (select irn from ##incsrchTemp#tblUID#)
	<cfelse>
	and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="Yes">)
	</cfif>
</cfif> 
</cfquery>
     
<cfif getinjcauseirns.recordcount gt 0>
	<cfloop query="getinjcauseirns">
		<cfloop list="#injuryCause#" index="i">
			<cfif listfind(frminjcause,i) gt 0>
				<cfset injcauseirnlist = listappend(injcauseirnlist,irn)>
			</cfif>
		</cfloop>
	
	</cfloop>
	<cfif trim(injcauseirnlist) eq ''>
		<cfset injcauseirnlist = 0>
	</cfif>
<cfelse>
	<cfset injcauseirnlist = 0>
</cfif>

</cfif>


<cfquery name="getincidents" datasource="#request.dsn#">
SELECT DISTINCT 
                         oneAIMincidents.isFatality, oneAIMincidents.IRN, oneAIMincidents.TrackingNum, Groups.Group_Name, NewDials_1.Name AS ouname, oneAIMincidents.incidentDate, 
                         IncidentTypes.IncType, OSHACategories.Category, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.isNearMiss, oneAIMincidents.createdbyEmail,
                         NewDials.Name AS buname, oneAIMincidents.withdrawnto, CASE 
						 WHEN oneAIMincidents.withdrawnto IS NOT NULL THEN 'Withdrawn' 
						 WHEN oneAIMincidents.IRN IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) THEN 'Pending Closure' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         (IncidentInvestigation.Status IS NULL OR
                         IncidentInvestigation.Status = 'Started') AND LEFT(IncidentAudits.ActionTaken, 15) 
                         = 'Review Complete' and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Awaiting Investigation' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND IncidentInvestigation.Status = 'Sent for Review' AND 
                         IncidentAudits.Status = 'Investigation Sent for Review'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes'  THEN 'Investigation Review' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND IncidentInvestigation.Status = 'Sent for Senior Review' AND 
                         IncidentAudits.Status = 'Investigation Sent for Review'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes'  THEN 'Investigation Review' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'More Info Requested' AND 
                         IncidentAudits.Status = 'Investigation Sent for More Information'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info' 
						  WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'Senior More Info Requested' AND 
                         IncidentAudits.Status = 'Investigation Sent for More Information'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info' 
						 WHEN oneAIMincidents.status = 'Closed' AND 
                         (IncidentInvestigation.Status IS NULL OR
                         IncidentInvestigation.Status = 'Started') AND LEFT(IncidentAudits.ActionTaken, 15) 
                         = 'Review Complete'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes'  THEN 'Awaiting Investigation (Closed)' 
						 WHEN oneAIMincidents.status = 'Closed' AND IncidentInvestigation.Status = 'Sent for Review' AND 
                         IncidentAudits.Status = 'Investigation Sent for Review'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review (Closed)' 
						  WHEN oneAIMincidents.status = 'Closed' AND IncidentInvestigation.Status = 'Sent for Senior Review' AND 
                         IncidentAudits.Status = 'Investigation Sent for Review'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review (Closed)' 
						 WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'More Info Requested' AND 
                         IncidentAudits.Status = 'Investigation Sent for More Information'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info (Closed)' 
						  WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'Senior More Info Requested' AND 
                         IncidentAudits.Status = 'Investigation Sent for More Information'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info (Closed)' 
						 WHEN oneAIMincidents.status in ('Started','Initiated') THEN 'Initiated'
						 ELSE oneAIMincidents.Status END AS status, oneAIMincidents.isWorkRelated
FROM            oneAIMassetDamage RIGHT OUTER JOIN
                         GroupLocations RIGHT OUTER JOIN
                         IncidentInvestigation RIGHT OUTER JOIN
                         oneAIMincidents ON IncidentInvestigation.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN ON GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID ON oneAIMassetDamage.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         NewDials INNER JOIN
                         NewDials AS NewDials_1 INNER JOIN
                         Groups ON NewDials_1.ID = Groups.OUid ON NewDials.ID = Groups.Business_Line_ID ON 
                         oneAIMincidents.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         oneAIMSecurity ON oneAIMincidents.IRN = oneAIMSecurity.IRN LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE  1 = 1  
<!--- <cfif searchtype eq "basic">
and isworkrelated = 'yes'
</cfif>  --->
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif listfindnocase(oshaclass,"All") eq 0>
	<cfif listfindnocase(oshaclass,"All Recordable") gt 0>
		<cfset ocl = "2,3,4">
		<cfloop list="#oshaclass#" index="x">
			<cfif listfindnocase("All,All Recordable,fatality",x) eq 0>
				<cfset ocl = listappend(ocl,x)>
			</cfif>
		</cfloop>
		
		AND	( ((oneAIMincidents.PrimaryType = 1) AND (oneAIMInjuryOI.OSHAclass IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ocl#" list="Yes">)) OR 
				 (oneAIMincidents.PrimaryType = 5) AND oneaimincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ocl#" list="Yes">))) or oneAIMincidents.isFatality = 'Yes')
	
	<cfelseif listfindnocase(oshaclass,"Fatality") gt 0>
		<cfset ocl = "">
		<cfloop list="#oshaclass#" index="x">
			
			<cfif listfindnocase("All,All Recordable,fatality",x) eq 0>
				<cfset ocl = listappend(ocl,x)>
			</cfif>
		</cfloop>
		<cfif trim(ocl) eq ''>
			and oneAIMincidents.isFatality = 'Yes'
		<cfelse>
		AND	( ((oneAIMincidents.PrimaryType = 1) AND (oneAIMInjuryOI.OSHAclass IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ocl#" list="Yes">)) OR 
				 (oneAIMincidents.PrimaryType = 5) AND oneaimincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ocl#" list="Yes">))) or oneAIMincidents.isFatality = 'Yes')
		</cfif>
	<cfelse>
		
		and oneAIMInjuryOI.OSHAclass in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oshaclass#" list="Yes">)  and (oneAIMincidents.primarytype in (1,5) or oneAIMincidents.secondarytype in (1,5)) 
	</cfif>
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	<cfset ptyplist = "">
		<cfloop list="#primarytype#" index="i">
			<cfif i neq "fatality">
				<cfset ptyplist = listappend(ptyplist,i)>
			</cfif>
		</cfloop>
		<cfif trim(ptyplist) neq ''>
			<cfif  listfindnocase(primarytype,"fatality") eq 0>
				and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ptyplist#" list="Yes">) 
				<cfif secondtype eq "yes">
					or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ptyplist#" list="Yes">)
				</cfif>
				)
			<cfelse>
				and (( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ptyplist#" list="Yes">) 
				<cfif secondtype eq "yes">
					or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ptyplist#" list="Yes">)
				</cfif>
				) or oneAIMincidents.isFatality = 'Yes')
			</cfif>
		<cfelse>
			<cfif  listfindnocase(primarytype,"fatality") gt 0>
			 and oneAIMincidents.isFatality = 'Yes'
			</cfif>
		</cfif>
</cfif>

<cfif trim(irnumber) neq ''>
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif searchtype eq "advanced">



<cfif listfindnocase(frminjcause,"All") eq 0>
	<cfif trim(injcauseirnlist) neq ''>
		and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#injcauseirnlist#" list="yes">)
	<cfelse>
		and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="yes">)
	</cfif>
</cfif>
<cfif listfindnocase(frmbodypart,"All") eq 0>
	<cfif trim(bpirnlist) neq ''>
		and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bpirnlist#" list="yes">)
	<cfelse>
		and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="yes">)
	</cfif>
</cfif>
<cfif listfindnocase(frminjtype,"All") eq 0>
	<cfif trim(injtypeirnlist) neq ''>
		and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#injtypeirnlist#" list="yes">)
	<cfelse>
		and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>



<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(projectoffice,"All") eq 0>
	 and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#projectoffice#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<cfif listfindnocase(gsr,"All") eq 0>
	and IncidentInvestigation.rulebreach in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gsr#" list="Yes">)
</cfif>

<cfif listfindnocase(seb,"All") eq 0>
	and IncidentInvestigation.essentialbreach in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#seb#" list="Yes">)
</cfif>


<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>

<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>

<cfif listfindnocase(incstatus,"All") eq 0>
	<cfif trim(statirnlist) neq ''>
		and oneAIMincidents.irn in (select irn from ##incsrchTemp#tblUID#)
	<cfelse>
	and oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0" list="Yes">)
	</cfif>
</cfif> 

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

<cfif INCNM eq "no">
	
	and (oneAIMincidents.isnearmiss = 'no' or oneAIMincidents.isfatality = 'yes')
	
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



<cfif listfindnocase(seriousinj,"All") eq 0>
	and oneAIMInjuryOI.isserious in (<cfqueryparam cfsqltype="CF_SQL_varchar" value="#seriousinj#" list="Yes">)
</cfif>
<cfif listfindnocase(secinccats,"All") eq 0>
	and oneAIMSecurity.secinccat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#secinccats#" list="Yes">)
</cfif>
<cfif listfindnocase(eic,"All") eq 0>
	and oneAIMEnvironmental.envinccat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#eic#" list="Yes">)
</cfif>
<cfif listfindnocase(damagesrc,"All") eq 0>
	and oneAIMassetDamage.DamageSource in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#damagesrc#" list="Yes">)
</cfif>

 </cfif>

<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>

<cfelseif  listfindnocase(request.userlevel,"Senior Executive View") gt 0>

<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0  or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0>
		and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
			and  oneAIMincidents.isFatality = 'no'
<cfelseif listfindnocase(request.userlevel,"User") gt 0>
	<!--- and oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_varchar" value="#request.userlogin#"> --->
	
	and oneAIMincidents.groupnumber in (SELECT DISTINCT GroupNumber
FROM            GroupUserAssignment
WHERE     (status = 1) AND (GroupRole = 'dataentry') and    (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (Groups.OUid IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)))
</cfif>

<cfswitch expression="#sortordby#">
<cfcase value="incnum">
ORDER BY oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="bu">
ORDER BY buname, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="ou">
ORDER BY ouname, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="incdate">
ORDER BY oneAIMincidents.incidentdate,oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="inctype">
ORDER BY IncidentTypes.IncType, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="prj">
ORDER BY Groups.Group_Name, oneAIMincidents.TrackingNum desc
</cfcase>

<cfcase value="osha">
ORDER BY OSHACategories.Category, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="potrate">
ORDER BY oneAIMincidents.PotentialRating, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="shortdesc">
ORDER BY oneAIMincidents.ShortDesc, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="recstat">
ORDER BY Status, TrackingNum desc
</cfcase>
<cfcase value="nearmiss">
ORDER BY oneAIMincidents.isnearmiss, oneAIMincidents.TrackingNum desc
</cfcase>
<cfcase value="workrel">
ORDER BY oneAIMincidents.isWorkRelated, oneAIMincidents.TrackingNum desc
</cfcase>
</cfswitch>

</cfquery>
