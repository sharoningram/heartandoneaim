<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="Yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="Yes">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="chartvalue" default="">
<cfparam name="charttype" default="No">
<cfparam name="searchname" default="">
<cfparam name="potentialrating" default="All">
<cfparam name="occillcat" default="All">

<cfparam name="irnumber" default="">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="beentoirp" default="All">
<cfparam name="capatype" default="All">
<cfparam name="capastatus" default="All">

<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>

<cfset qryproject = projectoffice>

<cfif listfindnocase(projectoffice,"All") eq 0>
	<cfif assocg eq "Yes">
		<cfset gtoview = "">
			<cfloop list="#projectoffice#" index="i">
				<cfif listfind(gtoview,i) eq 0>
					<cfset gtoview = listappend(gtoview,i)>
				</cfif>
				
				<cfquery name="getassocg" datasource="#request.dsn#">
					SELECT        GroupNumber1, GroupNumber2
					FROM            GroupAssociations
					WHERE        (GroupNumber1 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">) OR
                         (GroupNumber2 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
				</cfquery>
				<cfloop query="getassocg">
					<cfif listfind(gtoview,GroupNumber1) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber1)>
					</cfif>
					<cfif listfind(gtoview,GroupNumber2) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber2)>
					</cfif>
				</cfloop>
			</cfloop>
		
		<cfset qryproject = gtoview>
	</cfif>
</cfif>

<cfset tnumbers = "">
		<cfloop list="#irnumber#" index="i">
			<cfif isnumeric(i)>
				<cfset tnumbers = listappend(tnumbers,i)>
			</cfif>
		</cfloop>
<cfset irnumber = tnumbers>
<cfset pendirnlist = "">
<!--- <cfif listfindnocase(incstatus,"All") gt 0 or  listfindnocase(incstatus,"Pending Closure") gt 0> --->

<cfquery name="getpendingirns" datasource="#request.dsn#">
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE      (IncidentInvestigation.Status = 'IRP Pending') and oneAIMincidents.withdrawnto is null AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
						 UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE        (IncidentInvestigation.Status = 'IRP Pending') and oneAIMincidents.withdrawnto is null AND (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMInjuryOI.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
 UNION
                         SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE      (oneAIMCAPA.DateComplete IS NULL) and oneAIMincidents.withdrawnto is null AND (IncidentInvestigation.Status = 'IRP Pending')
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>





UNION
SELECT     oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE    (IncidentInvestigation.Status = 'Approved') and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'yes') and  (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                        (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>

UNION
SELECT     oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    (oneAIMincidents.isWorkRelated = 'no') and oneAIMincidents.withdrawnto is null and  (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                        (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>

UNION
SELECT        oneAIMincidents_1.irn
FROM            oneAIMincidents AS oneAIMincidents_1 INNER JOIN
                         oneAIMInjuryOI AS oneAIMInjuryOI_1 ON oneAIMincidents_1.IRN = oneAIMInjuryOI_1.IRN
WHERE      (oneAIMincidents_1.Status = 'Review Completed') and oneAIMincidents_1.withdrawnto is null AND (oneAIMInjuryOI_1.OSHAclass = 2) AND (oneAIMInjuryOI_1.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents_1.PrimaryType IN (1, 5) or oneAIMincidents_1.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents_1.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents_1.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents_1.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents_1.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents_1.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents_1.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents_1.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>





UNION
SELECT        oneAIMincidents_1.irn
FROM            oneAIMincidents AS oneAIMincidents_1 INNER JOIN
                         oneAIMInjuryOI AS oneAIMInjuryOI_1 ON oneAIMincidents_1.IRN = oneAIMInjuryOI_1.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents_1.IRN = IncidentInvestigation.IRN
WHERE     (IncidentInvestigation.Status = 'approved') and oneAIMincidents_1.withdrawnto is null AND (oneAIMincidents_1.isWorkRelated = 'yes') and  (oneAIMincidents_1.Status = 'Review Completed') AND (oneAIMInjuryOI_1.OSHAclass = 2) AND (oneAIMInjuryOI_1.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents_1.PrimaryType IN (1, 5) or oneAIMincidents_1.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents_1.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents_1.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents_1.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents_1.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents_1.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents_1.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents_1.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>



UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN
WHERE        (oneAIMincidents.Status = 'Review Completed')  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'no') AND (oneAIMCAPA.DateComplete IS NULL)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>



UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE        (oneAIMincidents.Status = 'Review Completed')  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'yes')  AND (IncidentInvestigation.Status = 'Approved') AND (oneAIMCAPA.DateComplete IS NULL)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>



UNION
SELECT        oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status IN ('closed', 'Review Completed'))  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.needIRP = 1) AND ((oneAIMIncidentIRP.Status = 'Started') OR (oneAIMIncidentIRP.Status IS NULL))  AND (IncidentAudits.Status = 'Investigation Approved')
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
UNION
SELECT        oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status IN ('closed', 'Review Completed'))  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.needIRP = 1) AND ((oneAIMIncidentIRP.Status = 'Sent for Review'))  AND (IncidentAudits.Status = 'Investigation Approved')
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
</cfquery>
<cfset pendirnlist = valuelist(getpendingirns.irn)>
<cfif trim(pendirnlist) eq ''>
	<cfset pendirnlist = 0>
</cfif>
<!--- 
</cfif> --->

<cfset incstatirns = 0>

<cfif listfindnocase(incstatus,"All") eq 0>
	<cfif listfindnocase(incstatus,"Awaiting Review") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN
		WHERE       oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and    (oneAIMincidents.withdrawnto IS NULL) AND (oneAIMincidents.Status IN ('sent for review')) AND (IncidentAudits.Status = 'Sent for Review')
		<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"More Info Requested") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN
		WHERE    oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and      (oneAIMincidents.withdrawnto IS NULL) AND (oneAIMincidents.Status IN ('More Info Requested')) AND (IncidentAudits.Status = 'More Info Requested')
		<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"Awaiting Investigation") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
		WHERE     oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and   oneAIMincidents.isworkrelated = 'yes' and      (oneAIMincidents.withdrawnto IS NULL) AND (oneAIMincidents.Status IN ('Review Completed')) AND (IncidentInvestigation.Status = 'started' OR
                         IncidentInvestigation.Status IS NULL) AND (IncidentAudits.ActionTaken LIKE 'Review Complete%') 
						 <cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"Investigation Review") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
		WHERE    oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and    oneAIMincidents.isworkrelated = 'yes' and     oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed')) AND (IncidentInvestigation.Status in ('Sent for Review', 'Sent for Senior Review')) AND (IncidentAudits.Status = 'Investigation Sent for Review') 
						 <cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"Investigation Needs More Info") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
		WHERE    oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and   oneAIMincidents.isworkrelated = 'yes' and      oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 'Review Completed')) AND (IncidentInvestigation.Status in ('More Info Requested','Senior More Info Requested')) 
                         AND (IncidentAudits.Status = 'Investigation Sent for More Information')
						 <cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"Pending Closure") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents 
		WHERE       oneAIMincidents.irn in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">)  and oneAIMincidents.withdrawnto is null
		<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"Withdrawn") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents 
		WHERE     oneAIMincidents.irn not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) and     oneAIMincidents.withdrawnto is not null
		<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	<cfif listfindnocase(incstatus,"Closed") gt 0>
		<cfquery name="getirnlist" datasource="#request.dsn#">
		SELECT DISTINCT oneAIMincidents.IRN
		FROM            oneAIMincidents 
		WHERE      oneAIMincidents.status = 'Closed'
		<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">


<cfelse>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
		</cfquery>
		<cfset incstatirns = listappend(incstatirns,valuelist(getirnlist.irn))>
	</cfif>
	
</cfif>


<cfset tblUID = replace(createUUID(),"-","","all")>
<cfif trim(incstatirns) neq ''>
	<cfquery name="newtbl" datasource="#request.dsn#"> 
		 Create Table ##inccapaTemp#tblUID# (IRN int null) 
	</cfquery>
	<cfloop list="#incstatirns#" index="i">
		<cfquery name="addtotemp" datasource="#request.dsn#">
			insert into ##inccapaTemp#tblUID# 
                values (#i# )
		</cfquery>
	</cfloop>
	
</cfif>

<cfquery name="capalog" datasource="#request.dsn#">
SELECT DISTINCT 
                         oneAIMincidents.IRN, oneAIMincidents.TrackingNum, Groups.Group_Name, NewDials_1.Name AS ouname, oneAIMincidents.ShortDesc, oneAIMincidents.isNearMiss,
                          NewDials.Name AS buname, oneAIMincidents.withdrawnto, CASE WHEN oneAIMincidents.withdrawnto IS NOT NULL 
                         THEN 'Withdrawn' WHEN oneAIMincidents.IRN IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) THEN 'Pending Closure' WHEN oneAIMincidents.status = 'Review Completed' AND 
                         (IncidentInvestigation.Status IS NULL OR
                         IncidentInvestigation.Status = 'Started') AND LEFT(IncidentAudits.ActionTaken, 15) = 'Review Complete' AND oneAIMincidents.withdrawnto IS NULL AND 
                         oneAIMincidents.isworkrelated = 'yes' THEN 'Awaiting Investigation' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'Sent for Review' AND IncidentAudits.Status = 'Investigation Sent for Review' AND oneAIMincidents.withdrawnto IS NULL AND 
                         oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'Sent for Senior Review' AND IncidentAudits.Status = 'Investigation Sent for Review' AND oneAIMincidents.withdrawnto IS NULL AND 
                         oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'More Info Requested' AND IncidentAudits.Status = 'Investigation Sent for More Information' AND oneAIMincidents.withdrawnto IS NULL 
                         AND oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info'  WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'Senior More Info Requested' AND IncidentAudits.Status = 'Investigation Sent for More Information' AND oneAIMincidents.withdrawnto IS NULL 
                         AND oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info' 
						 WHEN oneAIMincidents.status = 'Closed' AND 
                         (IncidentInvestigation.Status IS NULL OR
                         IncidentInvestigation.Status = 'Started') AND LEFT(IncidentAudits.ActionTaken, 15) = 'Review Complete' AND oneAIMincidents.withdrawnto IS NULL AND 
                         oneAIMincidents.isworkrelated = 'yes' THEN 'Awaiting Investigation (Closed)' WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'Sent for Review' AND IncidentAudits.Status = 'Investigation Sent for Review' AND oneAIMincidents.withdrawnto IS NULL AND 
                         oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review (Closed)' WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'Sent for Senior Review' AND IncidentAudits.Status = 'Investigation Sent for Review' AND oneAIMincidents.withdrawnto IS NULL AND 
                         oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review (Closed)' WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'More Info Requested' AND IncidentAudits.Status = 'Investigation Sent for More Information' AND oneAIMincidents.withdrawnto IS NULL 
                         AND oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info (Closed)'  WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'Senior More Info Requested' AND IncidentAudits.Status = 'Investigation Sent for More Information' AND oneAIMincidents.withdrawnto IS NULL 
                         AND oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info (Closed)' WHEN oneAIMincidents.status IN ('Started', 'Initiated') 
                         THEN 'Initiated' ELSE oneAIMincidents.Status END AS status, oneAIMCAPA.CAPAtype, oneAIMCAPA.DueDate, oneAIMCAPA.AssignedTo, oneAIMCAPA.AssigneeEmail, 
                         oneAIMCAPA.ActionDetail, oneAIMCAPA.DateComplete, GroupLocations.SiteName
FROM            IncidentTypes RIGHT OUTER JOIN
                         IncidentAudits RIGHT OUTER JOIN
                         oneAIMCAPA INNER JOIN
                         oneAIMincidents ON oneAIMCAPA.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID ON IncidentTypes.IncTypeID = oneAIMincidents.PrimaryType LEFT OUTER JOIN
                         oneAIMassetDamage ON oneAIMincidents.IRN = oneAIMassetDamage.IRN LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         NewDials INNER JOIN
                         NewDials AS NewDials_1 INNER JOIN
                         Groups ON NewDials_1.ID = Groups.OUid ON NewDials.ID = Groups.Business_Line_ID ON 
                         oneAIMincidents.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         oneAIMSecurity ON oneAIMincidents.IRN = oneAIMSecurity.IRN LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
		
WHERE oneAIMincidents.status <> 'cancel' 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">

<cfif beentoirp eq "Yes">
	and oneAIMincidents.needIRP = 1
<cfelseif beentoirp  eq "No">
	and oneAIMincidents.needIRP = 0
</cfif>

<cfelse>

<cfif capatype neq "All">
	and oneAIMCAPA.capatype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#capatype#">
</cfif>

<cfif capastatus neq "All">
	and oneAIMCAPA.status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#capastatus#">
</cfif>

<cfif beentoirp eq "Yes">
	and oneAIMincidents.needIRP = 1
<cfelseif beentoirp  eq "No">
	and oneAIMincidents.needIRP = 0
</cfif>

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>

<cfif trim(irnumber) neq ''>
	
	and oneAIMincidents.trackingnum in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irnumber#" list="Yes">)
</cfif>

<cfif listfindnocase(incstatus,"All") eq 0>
	 and oneAIMincidents.irn in (select irn from ##inccapaTemp#tblUID#)
</cfif>


<cfif trim(keywords) neq ''>
<cfset kctr = 0>
	and (
		<cfloop list="#keywords#" index="k">
			<cfset kctr = kctr + 1>
			(oneAIMincidents.shortdesc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(k)#%"> or oneAIMincidents.description like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#Trim(k)#%">) 
			<cfif kctr lt listlen(keywords)>
				or
			</cfif>
		</cfloop>
	
	
	)

</cfif>

</cfif>
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
</cfquery>
