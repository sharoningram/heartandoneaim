<cfquery name="getimmcauselist" datasource="#request.dsn#">
SELECT        AZcategories.CategoryID, AZcategories.CategoryLetter + '-' + AZcategories.CategoryName AS immcause
FROM            AZcategories INNER JOIN
                         AZSubCauses ON AZcategories.SubCauseID = AZSubCauses.SubCauseID CROSS JOIN
                         IncidentAZ
WHERE        (AZSubCauses.CauseID = 1)
</cfquery>