<cfparam name="uprjsearch" default="0">
<cfquery name="getuserprjsearch" datasource="#request.dsn#">
SELECT        PrjSearchID, PrjSearchName, UserID, BU, OU, BS, ClientID, Project, SiteOffice, LocDetail, CountryID, DateSaved, SaveCtr, ErpSys, groupactive, siteactive, mhentrytype,prjnumber
FROM            UserProjectSearches
WHERE        (PrjSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#uprjsearch#">)
</cfquery>