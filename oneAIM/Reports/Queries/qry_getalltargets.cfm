<cfquery name="getalltargets" datasource="#request.dsn#">
SELECT        TRIRgoal, LTIRgoal, AIRgoal, DialID, allocatedto, ObjYear
FROM            Objectives

<cfif trim(startdate) neq ''>
WHERE ObjYear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(startdate)#">
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
 WHERE ObjYear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
 <cfelse>
  WHERE ObjYear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">
 </cfif>
</cfif>


</cfquery>