<cfset rollstart = "1/1/#year(now())#">
<cfset rollend = "#now()#">
<!---  SUM(LaborHours.HO_HRS) AS ho, SUM(LaborHours.FIELD_HRS) AS fld, SUM(LaborHours.CRAFT_HRS) AS crft SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID--->
<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
<cfif trim(startdate) neq ''>
	WHERE laborhours.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and laborhours.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
WHERE      year(laborhours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
</cfif>
</cfif>
<!--- 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND laborhours.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfelse>
<!--- <cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse> --->
	<!--- <cfif listfindnocase(incyear,"All") eq 0>
		and year(laborhours.WEEK_ENDING_DATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif> --->
<!--- </cfif> --->
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>


</cfif>		 --->			 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
<!--- GROUP BY Groups.Group_Number, Groups.Group_Name, YEAR(LaborHours.WEEK_ENDING_DATE), MONTH(LaborHours.WEEK_ENDING_DATE), 
                         GroupLocations.LocationDetailID, Groups.Business_Line_ID
ORDER BY Groups.Business_Line_ID --->
GROUP BY Groups.Group_Number, Groups.Group_Name, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid
ORDER BY Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number
</cfquery>


<cfquery name="getjdeoh" datasource="#request.dsn#">
<!--- SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, Groups.Business_Line_ID
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID --->
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
<cfif listfindnocase(incyear,"All") eq 0>
WHERE      year(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
</cfif>
<!--- 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND LaborHoursOffice.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfelse>
<!--- <cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse> --->
	<!--- <cfif listfindnocase(incyear,"All") eq 0>
		and year(laborhours.WEEK_ENDING_DATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif> --->
<!--- </cfif> --->
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>


</cfif>		 --->			 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Groups.Business_Line_ID, GroupLocations.LocationDetailID, Groups.OUid, Groups.Group_Number
ORDER BY Groups.Business_Line_ID, GroupLocations.LocationDetailID, Groups.OUid, Groups.Group_Number
</cfquery>
