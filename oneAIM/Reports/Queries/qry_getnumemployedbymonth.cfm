<cfparam name="incyear" default="#year(now())#">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="Yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="Yes">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="chartvalue" default="">
<cfparam name="charttype" default="No">
<cfparam name="searchname" default="">
<cfparam name="potentialrating" default="All">
<cfparam name="occillcat" default="All">

<cfparam name="irnumber" default="">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="beentoirp" default="No">

<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>

<cfquery name="getnumemplyed" datasource="#request.dsn#">
SELECT        YEAR(Population.PopDate) AS yr, MONTH(Population.PopDate) AS mon, SUM(Population.NumEmployedAFW) AS afw, SUM(Population.NumEmployedMC) AS mc, 
                         SUM(Population.NumEmployedSub) AS sub, SUM(Population.NumEmployedJV) AS jv
FROM            Population INNER JOIN
                         GroupLocations ON Population.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Groups ON GroupLocations.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID
where 1 = 1
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(incyear,"All") eq 0>
	and year(Population.PopDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(Population.PopDate), MONTH(Population.PopDate)
ORDER BY yr DESC, mon
</cfquery>