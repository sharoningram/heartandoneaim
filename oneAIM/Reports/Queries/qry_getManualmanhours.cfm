<cfset neednonqryjde = "yes">
<cfif listfindnocase(frmincassignto,"All") eq 0>
	<cfset neednonqryjde = "no">
	<cfloop list="#frmincassignto#" index="i">
		<cfif listfind("3,4,11",i) eq 0>
			<cfset neednonqryjde = "yes">
			<cfbreak>
		</cfif>
	</cfloop>
	
</cfif>

<cfquery name="getnonjdehrs" datasource="#request.dsn#">
<cfif neednonqryjde eq "yes">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) 
                        <cfif listfind(frmincassignto,3) gt 0 or listfindnocase(frmincassignto,"All") gt 0> + SUM(LaborHoursTotal.SubHrs)</cfif> <cfif listfind(frmincassignto,11) gt 0 or listfindnocase(frmincassignto,"All") gt 0>+ SUM(LaborHoursTotal.JVHours)</cfif><cfif listfind(frmincassignto,4) gt 0 or listfindnocase(frmincassignto,"All") gt 0> + SUM(LaborHoursTotal.ManagedContractorHours)</cfif> AS nonjdehrs, 
                         MONTH(LaborHoursTotal.WeekEndingDate) AS njmon, YEAR(LaborHoursTotal.WeekEndingDate) AS njyr, GroupLocations.LocationDetailID
<cfelse>
SELECT    0 <cfif listfind(frmincassignto,3) gt 0> +  SUM(LaborHoursTotal.SubHrs)</cfif> <cfif listfind(frmincassignto,11) gt 0> + SUM(LaborHoursTotal.JVHours) </cfif><cfif listfind(frmincassignto,4) gt 0> + SUM(LaborHoursTotal.ManagedContractorHours)</cfif> AS nonjdehrs, 
                         MONTH(LaborHoursTotal.WeekEndingDate) AS njmon, YEAR(LaborHoursTotal.WeekEndingDate) AS njyr, GroupLocations.LocationDetailID

</cfif>
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID AND LaborHoursTotal.GroupNumber = GroupLocations.GroupNumber

   WHERE LaborHoursTotal.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
<cfelse>
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<!--- <cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif> --->

<cfif trim(contractlist) neq ''>
	and 1 = 2
</cfif>
<!--- <cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif> --->
</cfif>
			 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(LaborHoursTotal.WeekEndingDate), MONTH(LaborHoursTotal.WeekEndingDate), GroupLocations.LocationDetailID
ORDER BY njyr, njmon
</cfquery>

<cfloop query="getnonjdehrs">
	<cfif not structkeyexists(manhrstruct,"#njyr#_#njmon#")>
		<cfset manhrstruct["#njyr#_#njmon#"] = nonjdehrs>
	<cfelse>
		<cfset manhrstruct["#njyr#_#njmon#"] = manhrstruct["#njyr#_#njmon#"] + nonjdehrs>
	</cfif>
	<cfif LocationDetailID eq 4>
	<cfif not structkeyexists(manhrstruct,"#njyr#_#njmon#")>
		<cfset manhrstruct["#njyr#_#njmon#"] = nonjdehrs>
	<cfelse>
		<cfset manhrstruct["#njyr#_#njmon#"] = manhrstruct["#njyr#_#njmon#"] + nonjdehrs>
	</cfif>
		<!--- <cfif not structkeyexists(manhrstructoffshore,"#njyr#_#njmon#")>
			<cfset manhrstructoffshore["#njyr#_#njmon#"] = nonjdehrs>
		<cfelse>
			<cfset manhrstructoffshore["#njyr#_#njmon#"] = manhrstructoffshore["#njyr#_#njmon#"] + nonjdehrs>
		</cfif> --->
	</cfif>
</cfloop>


