<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        YEAR(ContractorHours.WeekEndingDate) AS conyr, SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Groups.Business_Line_ID, 
                         Groups.OUid
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID

<cfif trim(startdate) neq ''>
	WHERE ContractorHours.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and ContractorHours.WeekEndingDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
WHERE      year(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
</cfif>
</cfif>
<!--- <cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY YEAR(ContractorHours.WeekEndingDate), Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
ORDER BY conyr, Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>