<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>
<cfparam name="sortordby" default="incnum">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfset contractlist = "">
<cfif trim(frmmgdcontractor) neq ''>
<cfif listfindnocase(frmmgdcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmmgdcontractor)>
</cfif>
</cfif>
<cfif trim(frmjv) neq ''>
<cfif listfindnocase(frmjv,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmjv)>
</cfif>
</cfif>
<cfif trim(frmsubcontractor) neq ''>
<cfif listfindnocase(frmsubcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmsubcontractor)>
</cfif>
</cfif>