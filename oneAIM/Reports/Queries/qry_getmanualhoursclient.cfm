<cfquery name="getnonjdehrs" datasource="#request.dsn#">

SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) AS jdehrs, 
                         SUM(LaborHoursTotal.SubHrs) AS sub, SUM(LaborHoursTotal.JVHours) AS jv, SUM(LaborHoursTotal.ManagedContractorHours) AS mc, 
                         GroupLocations.LocationDetailID, Groups.Group_Number,GroupLocations.GroupLocID
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID AND LaborHoursTotal.GroupNumber = GroupLocations.GroupNumber

WHERE 1 = 1

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND LaborHoursTotal.WEEKENDINGDATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and LaborHoursTotal.WEEKENDINGDATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHoursTotal.WEEKENDINGDATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
	<cfif incyear neq "All">
	and      year(LaborHoursTotal.WEEKENDINGDATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
	</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>		 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

GROUP BY  GroupLocations.LocationDetailID, Groups.Group_Number,GroupLocations.GroupLocID
ORDER BY Groups.Group_Number
</cfquery>


