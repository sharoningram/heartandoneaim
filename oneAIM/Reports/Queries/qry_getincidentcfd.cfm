<cfquery name="getcfdincs" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, 
                         Groups.Group_Number
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.isnearmiss = 'No' and   oneAIMincidents.isworkrelated = 'yes' and    (oneAIMincidents.Status <> 'Cancel') 

<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
AND (YEAR(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">) 
</cfif>
</cfif>
AND ((oneAIMincidents.PrimaryType = 1)  AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4))
OR (oneAIMincidents.PrimaryType = 5) AND oneAIMincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))		
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

GROUP BY Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass, Groups.OUid, Groups.Group_Number
ORDER BY Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass
</cfquery>

<cfquery name="getoi" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, 
                         Groups.Group_Number
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.isNearMiss = 'No') AND (oneAIMincidents.isWorkRelated = 'yes') AND (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType = 5) 
                         AND (oneAIMincidents.OIdefinition = 1)
	<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
AND (YEAR(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">) 
</cfif>
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass, Groups.OUid, Groups.Group_Number
ORDER BY Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass
</cfquery>



<cfquery name="getfatalities" datasource="#request.dsn#">

SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, Groups.Group_Number
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE         oneAIMincidents.isworkrelated = 'yes' and  (oneAIMincidents.Status <> 'Cancel') 
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
AND (YEAR(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">) 
</cfif>
</cfif>

AND (oneAIMincidents.isFatality = 'yes')
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, Groups.Group_Number
ORDER BY Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, oneAIMincidents.IncAssignedTo

</cfquery>
