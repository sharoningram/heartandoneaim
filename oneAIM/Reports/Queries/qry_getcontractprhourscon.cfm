
<cfquery name="getsubcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, Contractors.CoType, ContractorHours.ContractorID
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID INNER JOIN
                         ContractorNames ON Contractors.ContractorNameID = ContractorNames.ContractorID

<cfif listfindnocase(cotypelist,"sub") gt 0>
	Where Contractors.CoType = 'sub'
<cfelse>
	WHERE 1 = 2
</cfif>
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ContractorHours.WEEKENDINGDATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#"> 

<cfelse>
<cfif listfindnocase(frmsubcontractor,"all") eq 0 and  listfindnocase(frmsubcontractor,"none") eq 0 >
	and ContractorNames.ContractorID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmsubcontractor#" list="Yes">)
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>

<cfif trim(startdate) neq ''>
	and ContractorHours.WEEKENDINGDATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and ContractorHours.WEEKENDINGDATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(ContractorHours.WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


</cfif>
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Contractors.CoType, ContractorHours.ContractorID
ORDER BY Contractors.CoType, ContractorHours.ContractorID
</cfquery>

<cfquery name="getjvcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, Contractors.CoType, ContractorHours.ContractorID
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID INNER JOIN
                         ContractorNames ON Contractors.ContractorNameID = ContractorNames.ContractorID
<cfif listfindnocase(cotypelist,"jv") gt 0>
	Where Contractors.CoType = 'jv'
<cfelse>
	WHERE 1 = 2
</cfif>
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ContractorHours.WEEKENDINGDATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#"> 

<cfelse>
<cfif listfindnocase(frmjv,"all") eq 0 and  listfindnocase(frmjv,"none") eq 0 >
	and ContractorNames.ContractorID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmjv#" list="Yes">)
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>


<cfif trim(startdate) neq ''>
	and ContractorHours.WEEKENDINGDATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and ContractorHours.WEEKENDINGDATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(ContractorHours.WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>


</cfif>
	 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Contractors.CoType, ContractorHours.ContractorID
ORDER BY Contractors.CoType, ContractorHours.ContractorID
</cfquery>

<cfquery name="getmccontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, Contractors.CoType, ContractorHours.ContractorID
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID INNER JOIN
                         ContractorNames ON Contractors.ContractorNameID = ContractorNames.ContractorID
<cfif listfindnocase(cotypelist,"mgd") gt 0>
	Where Contractors.CoType = 'mgd'
<cfelse>
	WHERE 1 = 2
</cfif>

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ContractorHours.WEEKENDINGDATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#"> 

<cfelse>
<cfif listfindnocase(frmmgdcontractor,"all") eq 0 and  listfindnocase(frmmgdcontractor,"none") eq 0 >
	and ContractorNames.ContractorID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmmgdcontractor#" list="Yes">)
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>

<cfif trim(startdate) neq ''>
	and ContractorHours.WEEKENDINGDATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and ContractorHours.WEEKENDINGDATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(ContractorHours.WEEKENDINGDATE) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

</cfif>
	 
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY Contractors.CoType, ContractorHours.ContractorID
ORDER BY Contractors.CoType, ContractorHours.ContractorID
</cfquery>

<cfquery name="getreporthours" dbtype="query">
  Select ContractorID, conhrs, cotype
  from getsubcontractorhrs
  union 
  Select ContractorID, conhrs, cotype
  from getjvcontractorhrs
   union 
  Select ContractorID, conhrs, cotype
  from getmccontractorhrs
ORDER BY ContractorID, conhrs, cotype
</cfquery>

