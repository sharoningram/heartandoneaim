<cfset greenpotratings = "A1,B1,C1,D1,A2,B2,C2">
<cfset yellowpotratings = "E1,D2,E2,A3,B3,C3,D3">
<cfset redpotratings = "E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="Yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="Yes">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="chartvalue" default="">
<cfparam name="charttype" default="No">
<cfparam name="searchname" default="">
<cfparam name="potentialrating" default="All">
<cfparam name="occillcat" default="All">
<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>
<cfquery name="getclientreportprojects" datasource="#request.dsn#">
SELECT        Clients.ClientID, Clients.ClientName, Clients.status, Groups.Group_Name, GroupLocations.GroupLocID, Groups.Group_Number
FROM            Clients INNER JOIN
                         GroupLocations ON Clients.ClientID = GroupLocations.ClientID INNER JOIN
                         Groups ON GroupLocations.GroupNumber = Groups.Group_Number
where 1 = 1
<cfif listfindnocase(frmclient,"all") eq 0>
and Clients.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="yes">)
</cfif>
	<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
ORDER BY Clients.ClientName, Clients.ClientID, Groups.Group_Name, Groups.Group_Number
</cfquery>