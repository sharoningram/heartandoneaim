
<cfparam name="incyear" default="#year(now())#">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">


<cfquery name="getnonjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) AS jdehrs, 
                         SUM(LaborHoursTotal.SubHrs) AS subhrs, SUM(LaborHoursTotal.JVHours) AS jvhrs, SUM(LaborHoursTotal.ManagedContractorHours) AS mdghrs, 
                         GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, Groups.Group_Name, GroupLocations.GroupLocID

FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID AND Groups.Group_Number = GroupLocations.GroupNumber
<cfif trim(startdate) neq ''>
	WHERE LaborHoursTotal.WeekEndingDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHoursTotal.WeekEndingDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		WHERE (YEAR(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">)
	</cfif>
</cfif>
<cfif trim(locdetail) neq ''>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
</cfif>
<cfif manhrtype eq "JDE only">
and groups.erpsys = 'JDE'  
<cfelseif manhrtype eq "Manual only">
and groups.erpsys = 'none'
</cfif>

<!--- <cfif showouonly> and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> --->
GROUP BY Groups.Business_Line_ID, GroupLocations.LocationDetailID, Groups.OUid,Groups.Group_Number,  Groups.Group_Name, GroupLocations.GroupLocID
ORDER BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID,Groups.Group_Number,  Groups.Group_Name, GroupLocations.GroupLocID
</cfquery>