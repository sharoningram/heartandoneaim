<cfquery name="getfatalities" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt,  oneAIMincidents.IncAssignedTo,  Groups.Group_Number,GroupLocations.GroupLocID
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE         oneAIMincidents.isworkrelated = 'yes' and  (oneAIMincidents.Status <> 'Cancel') 
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
	<cfif incyear neq "All">
	and      year(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
	</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>		 
AND (oneAIMincidents.isFatality = 'yes')
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY oneAIMincidents.IncAssignedTo,  Groups.Group_Number,GroupLocations.GroupLocID
ORDER BY  Groups.Group_Number

</cfquery>


<cfquery name="getcfdincs" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass,  oneAIMincidents.IncAssignedTo,
                         Groups.Group_Number,GroupLocations.GroupLocID
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.isnearmiss = 'No' and   oneAIMincidents.isworkrelated = 'yes' and    (oneAIMincidents.Status <> 'Cancel') 

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
	<cfif incyear neq "All">
	and      year(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
	</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>		 
AND ((oneAIMincidents.PrimaryType = 1)  AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4))
OR (oneAIMincidents.PrimaryType = 5) AND oneAIMincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))		
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>

GROUP BY  oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass, Groups.Group_Number,GroupLocations.GroupLocID
ORDER BY Groups.Group_Number
</cfquery>

<cfquery name="getoi" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass,  oneAIMincidents.IncAssignedTo, 
                         Groups.Group_Number,GroupLocations.GroupLocID
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.isNearMiss = 'No') AND (oneAIMincidents.isWorkRelated = 'yes') AND (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType = 5) 
                         AND (oneAIMincidents.OIdefinition = 1)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
	<cfif incyear neq "All">
	and      year(oneAIMincidents.incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
	</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>		
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass,Groups.Group_Number,GroupLocations.GroupLocID
ORDER BY  Groups.Group_Number
</cfquery>