<cfset rollstart = "1/1/#year(now())#">
<cfset rollend = "#now()#">

<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID, 
                         Groups.Group_Number, GroupLocations.GroupLocID
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE 1 = 1

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND laborhours.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and laborhours.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and laborhours.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
		<cfif incyear neq "All">
			and      year(laborhours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
		</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>
 
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif> 
GROUP BY Groups.Group_Number, GroupLocations.LocationDetailID, GroupLocations.GroupLocID
ORDER BY Groups.Group_Number
</cfquery>


<cfquery name="getjdeoh" datasource="#request.dsn#">

SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID,  Groups.Group_Number, GroupLocations.GroupLocID
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE 1 = 1

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND LaborHoursOffice.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollstart#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#rollend#"> 
<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
<cfelse>
	<cfif trim(startdate) neq ''>
	and LaborHoursOffice.WEEK_ENDING_DATE >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and LaborHoursOffice.WEEK_ENDING_DATE <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
	<cfelse>
	<cfif incyear neq "All">
	and      year(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#">
	</cfif>
	</cfif>
	<cfif listfindnocase(frmclient,"All") eq 0>
		and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
	</cfif>
</cfif>		 
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY GroupLocations.LocationDetailID,  Groups.Group_Number, GroupLocations.GroupLocID
ORDER BY  Groups.Group_Number
</cfquery>

