<cfquery name="getrootcauses" datasource="#request.dsn#">
SELECT        AZfactors.FactorID AS CategoryID, AZfactors.CategoryLetter + CONVERT(varchar, AZfactors.FactorNumber) + '-' + AZfactors.FactorName AS rootname
FROM            AZSubCauses INNER JOIN
                         AZcategories ON AZSubCauses.SubCauseID = AZcategories.SubCauseID INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter
WHERE        (AZSubCauses.CauseID = 2)
</cfquery>