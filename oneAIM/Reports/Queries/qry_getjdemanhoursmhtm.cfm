<cfquery name="getjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, Groups.Business_Line_ID, Groups.OUid, 
                         GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE     month(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#monnum#"> and   (YEAR(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2015">)

GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name
ORDER BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Groups.Group_Number, Groups.Group_Name
</cfquery>

<cfquery name="getjdeoh" datasource="#request.dsn#">
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE     month(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#monnum#"> and      (YEAR(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2015">)
GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
ORDER BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>

<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, Contractors.CoType
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE      month(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#monnum#"> and   (YEAR(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2015">)
GROUP BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID, Contractors.CoType
ORDER BY Contractors.CoType, Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID
</cfquery>

<cfquery name="getnonjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) AS jdehrs, 
                         SUM(LaborHoursTotal.SubHrs) AS subhrs, SUM(LaborHoursTotal.JVHours) AS jvhrs, SUM(LaborHoursTotal.ManagedContractorHours) AS mdghrs, 
                         GroupLocations.LocationDetailID, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, Groups.Group_Name
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
WHERE        month(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#monnum#"> and    (YEAR(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2015">)

GROUP BY Groups.Business_Line_ID, GroupLocations.LocationDetailID, Groups.OUid,Groups.Group_Number,  Groups.Group_Name
ORDER BY Groups.Business_Line_ID, Groups.OUid, GroupLocations.LocationDetailID,Groups.Group_Number,  Groups.Group_Name
</cfquery>


