<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "DiamondDetails\Actions\act_export#diadettype#Details.cfm", "exports"))>
<cfset thefilename = "#replace(diamondrep,' ','','all')#_#timeformat(now(),'hhnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("#diamondrep#","true")>

<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"#diamondrep#",1,1);
	SpreadsheetSetCellValue(theSheet,"Incident Number",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,4);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,5);
	SpreadsheetSetCellValue(theSheet,"Near Miss?",1,6);
	SpreadsheetSetCellValue(theSheet,"Incident Date",1,7);
	SpreadsheetSetCellValue(theSheet,"Incident Type",1,8);
	SpreadsheetSetCellValue(theSheet,"OSHA Classification",1,9);
	SpreadsheetSetCellValue(theSheet,"Potential Rating",1,10);
	SpreadsheetSetCellValue(theSheet,"Short Description",1,11);
	SpreadsheetSetCellValue(theSheet,"Record Status",1,12);
</cfscript>

<cfset rctr = 1>

<cfoutput query="getincidents" group="lbl">
	<cfset ctrr = 0>
	<cfset alreadyshown = "">
	
	<cfoutput>
	<!--- <cfif listfindnocase(alreadyshown,irn) eq 0> --->
		<cfset showrw = "yes">
		<!--- <cfset alreadyshown = listappend(alreadyshown,irn)>
	<cfelse>
		<cfset showrw = "no">
	</cfif> --->
	
	<cfif showrw eq "yes">
		<cfset rctr = rctr+1>
		<cfset ctrr = ctrr+1>
		<cfif isfatality eq "Yes"><cfset dspinctype = "Fatality"><cfelse><cfset dspinctype = IncType></cfif>
		
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#lbl#",#rctr#,1);
			SpreadsheetSetCellValue(theSheet,"#TrackingNum#",#rctr#,2);
			SpreadsheetSetCellValue(theSheet,"#buname#",#rctr#,3);
			SpreadsheetSetCellValue(theSheet,"#ouname#",#rctr#,4);
			SpreadsheetSetCellValue(theSheet,"#Group_Name#",#rctr#,5);
			SpreadsheetSetCellValue(theSheet,"#isnearmiss#",#rctr#,6);
			SpreadsheetSetCellValue(theSheet,"#dateformat(incidentDate,sysdateformat)#",#rctr#,7);
			SpreadsheetSetCellValue(theSheet,"#dspinctype#",#rctr#,8);
			SpreadsheetSetCellValue(theSheet,"#Category#",#rctr#,9);
			SpreadsheetSetCellValue(theSheet,"#PotentialRating#",#rctr#,10);
			SpreadsheetSetCellValue(theSheet,"#ShortDesc#",#rctr#,11);
			SpreadsheetSetCellValue(theSheet,"#status#",#rctr#,12);
		</cfscript>
	</cfif>
</cfoutput>
</cfoutput>
<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">