<cfquery name="getrecirns" datasource="#request.dsn#">
SELECT        oneAIMincidents.irn, SecurityIncidentNature.IncidentNature as lbl
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID  INNER JOIN
                         oneAIMSecurity ON oneAIMincidents.IRN = oneAIMSecurity.IRN INNER JOIN
                         SecurityIncidentNature ON oneAIMSecurity.PersonNature = SecurityIncidentNature.SecIncNatureID LEFT OUTER JOIN
                         oneAIMassetDamage ON oneAIMincidents.IRN = oneAIMassetDamage.IRN LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMSecurity.SecIncCat = 1)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oshaclass,"All") eq 0>
	and oneAIMInjuryOI.OSHAclass in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oshaclass#" list="Yes">)
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>
<cfif listfindnocase(seriousinj,"All") eq 0>
	and oneAIMInjuryOI.isSerious in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#" list="Yes">)
</cfif>
<cfif listfindnocase(secinccats,"All") eq 0>
	and oneAIMSecurity.SecIncCat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#secinccats#" list="Yes">)
</cfif>
<cfif listfindnocase(eic,"All") eq 0>
	and oneAIMEnvironmental.EnvIncCat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#eic#" list="Yes">)
</cfif>
<cfif listfindnocase(damagesrc,"All") eq 0>
	and oneAIMassetDamage.DamageSource in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#damagesrc#" list="Yes">)
</cfif>



</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
order by lbl
</cfquery>


<cfset irnlistq = valuelist(getrecirns.irn)>



<cfquery name="getpendingirns" datasource="#request.dsn#">
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE      (IncidentInvestigation.Status = 'IRP Pending') and oneAIMincidents.withdrawnto is null AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE       (IncidentInvestigation.Status = 'IRP Pending') and oneAIMincidents.withdrawnto is null AND (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMInjuryOI.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
						 UNION
                         SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE         (oneAIMCAPA.DateComplete IS NULL) and oneAIMincidents.withdrawnto is null AND (IncidentInvestigation.Status = 'IRP Pending')
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
 UNION
SELECT     oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    (oneAIMincidents.isWorkRelated = 'no') and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                        (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
 
UNION
SELECT     oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE    (IncidentInvestigation.Status = 'Approved') and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'yes') AND (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 4) AND (oneAIMInjuryOI.RWCDateReturned IS NULL) AND 
                        (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
 UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents  INNER JOIN
                         oneAIMInjuryOI  ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE      (IncidentInvestigation.Status = 'approved') and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'yes')  and  (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMInjuryOI.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
 
UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents  INNER JOIN
                         oneAIMInjuryOI  ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE      (oneAIMincidents.isWorkRelated = 'no')  and oneAIMincidents.withdrawnto is null and  (oneAIMincidents.Status = 'Review Completed') AND (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMInjuryOI.LTIDateReturned IS NULL) AND 
                         (oneAIMincidents.PrimaryType IN (1, 5) or oneAIMincidents.SecondaryType in (1,5))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
 
UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN
WHERE        (oneAIMincidents.Status = 'Review Completed') and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'no')  AND (oneAIMCAPA.DateComplete IS NULL)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
 <cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
 
 
UNION
SELECT        oneAIMincidents.irn
FROM            oneAIMincidents INNER JOIN
                         oneAIMCAPA ON oneAIMincidents.IRN = oneAIMCAPA.IRN INNER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN
WHERE        (oneAIMincidents.Status = 'Review Completed') and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.isWorkRelated = 'yes')  AND (IncidentInvestigation.Status = 'Approved') AND (oneAIMCAPA.DateComplete IS NULL)
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
UNION
SELECT        oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status IN ('closed', 'Review Completed'))  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.needIRP = 1) AND ((oneAIMIncidentIRP.Status = 'Started') OR (oneAIMIncidentIRP.Status IS NULL))  AND (IncidentAudits.Status = 'Investigation Approved')
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
UNION
SELECT        oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status IN ('closed', 'Review Completed'))  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.needIRP = 1) AND ((oneAIMIncidentIRP.Status = 'Sent for Review'))  AND (IncidentAudits.Status = 'Investigation Approved')
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<!--- <cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif> --->
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>



</cfif>
</cfquery>
<cfset pendirnlist = valuelist(getpendingirns.irn)>
<cfif trim(pendirnlist) eq ''>
	<cfset pendirnlist = 0>
</cfif>




<cfquery name="getincdata" datasource="#request.dsn#">
SELECT DISTINCT 
                         oneAIMincidents.isFatality, oneAIMincidents.IRN, oneAIMincidents.TrackingNum, Groups.Group_Name, NewDials_1.Name AS ouname, oneAIMincidents.incidentDate, 
                         IncidentTypes.IncType, OSHACategories.Category, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.isNearMiss, oneAIMincidents.createdbyEmail,
                         NewDials.Name AS buname, oneAIMincidents.withdrawnto, CASE 
						 WHEN oneAIMincidents.withdrawnto IS NOT NULL THEN 'Withdrawn' 
						 WHEN oneAIMincidents.IRN IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#pendirnlist#" list="Yes">) THEN 'Pending Closure' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         (IncidentInvestigation.Status IS NULL OR
                         IncidentInvestigation.Status = 'Started') AND LEFT(IncidentAudits.ActionTaken, 15) 
                         = 'Review Complete' and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Awaiting Investigation' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND IncidentInvestigation.Status = 'Sent for Review' AND 
                         IncidentAudits.Status = 'Investigation Sent for Review'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes'  THEN 'Investigation Review' 
						 WHEN oneAIMincidents.status = 'Review Completed' AND 
                         IncidentInvestigation.Status = 'More Info Requested' AND 
                         IncidentAudits.Status = 'Investigation Sent for More Information'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info' 
						 WHEN oneAIMincidents.status = 'Closed' AND 
                         (IncidentInvestigation.Status IS NULL OR
                         IncidentInvestigation.Status = 'Started') AND LEFT(IncidentAudits.ActionTaken, 15) 
                         = 'Review Complete'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes'  THEN 'Awaiting Investigation (Closed)' 
						 WHEN oneAIMincidents.status = 'Closed' AND IncidentInvestigation.Status = 'Sent for Review' AND 
                         IncidentAudits.Status = 'Investigation Sent for Review'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Review (Closed)' 
						 WHEN oneAIMincidents.status = 'Closed' AND 
                         IncidentInvestigation.Status = 'More Info Requested' AND 
                         IncidentAudits.Status = 'Investigation Sent for More Information'  and oneAIMincidents.withdrawnto is null and oneAIMincidents.isworkrelated = 'yes' THEN 'Investigation Needs More Info (Closed)' 
						 WHEN oneAIMincidents.status in ('Started','Initiated') THEN 'Initiated'
						 ELSE oneAIMincidents.Status END AS status, oneAIMincidents.isWorkRelated
FROM            oneAIMassetDamage RIGHT OUTER JOIN
                         GroupLocations RIGHT OUTER JOIN
                         IncidentInvestigation RIGHT OUTER JOIN
                         oneAIMincidents ON IncidentInvestigation.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN ON GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID ON oneAIMassetDamage.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         NewDials INNER JOIN
                         NewDials AS NewDials_1 INNER JOIN
                         Groups ON NewDials_1.ID = Groups.OUid ON NewDials.ID = Groups.Business_Line_ID ON 
                         oneAIMincidents.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         oneAIMSecurity ON oneAIMincidents.IRN = oneAIMSecurity.IRN LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE  1 = 1
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND ((oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="1/1/#year(now())#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) or (oneAIMincidents.incidentdate is null))
<cfelse>
<cfif trim(startdate) neq ''>
	and oneAIMincidents.incidentDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">
	<cfif trim(enddate) neq ''>
		 and oneAIMincidents.incidentDate <= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#"> 
	</cfif>
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		and year(oneAIMincidents.incidentDate) in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#incyear#" list="Yes">)
	</cfif>
</cfif>

<cfif droppedobj eq "No">	
	and (oneAIMincidents.droppedObject != 'Yes' or oneAIMincidents.droppedObject is null)
<cfelseif droppedobj eq "yes">
	and oneAIMincidents.droppedObject = 'Yes'
</cfif>

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>

<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>
<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(primarytype,"All") eq 0>
	and ( oneAIMincidents.primarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)     
	<cfif secondtype eq "yes">
		or oneAIMincidents.secondarytype in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#primarytype#" list="Yes">)
	</cfif>)
</cfif>

<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif hipoonly eq "yes">
	and oneAIMincidents.potentialrating in ('E3','A4','B4','C4','D4','E4','A5','B5','C5','D5','E5')
</cfif>
<cfif INCNM eq "no">
	and oneAIMincidents.isnearmiss = 'no'
<cfelseif incnm eq "nmonly">
	and oneAIMincidents.isnearmiss = 'yes'
</cfif>
<cfif listfindnocase(oshaclass,"All") eq 0>
	and oneAIMInjuryOI.OSHAclass in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oshaclass#" list="Yes">)
</cfif>


<cfif listfindnocase(oidef,"All") eq 0>
	and oneAIMincidents.OIdefinition in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#oidef#" list="Yes">)
</cfif>
<cfif listfindnocase(seriousinj,"All") eq 0>
	and oneAIMInjuryOI.isSerious in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#" list="Yes">)
</cfif>
<cfif listfindnocase(secinccats,"All") eq 0>
	and oneAIMSecurity.SecIncCat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#secinccats#" list="Yes">)
</cfif>
<cfif listfindnocase(eic,"All") eq 0>
	and oneAIMEnvironmental.EnvIncCat in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#eic#" list="Yes">)
</cfif>
<cfif listfindnocase(damagesrc,"All") eq 0>
	and oneAIMassetDamage.DamageSource in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#damagesrc#" list="Yes">)
</cfif>



</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
ORDER BY oneAIMincidents.TrackingNum desc
</cfquery>

<cfquery name="getincidents" dbtype="query">
	select getincdata.isfatality, getincdata.irn, getincdata.trackingnum, getincdata.group_name, getincdata.ouname, getincdata.incidentdate, getincdata.inctype, getincdata.category, getincdata.PotentialRating, getincdata.shortdesc, getincdata.isNearMiss, getincdata.createdbyEmail, getincdata.buname, getincdata.withdrawnto, getincdata.status, getincdata.isWorkRelated, getrecirns.lbl
	from getincdata , getrecirns
	where getincdata.irn  = getrecirns.irn
	order by getrecirns.lbl , getincdata.trackingnum
</cfquery>