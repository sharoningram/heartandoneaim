<cfset recliststruct = {}>

<cfloop query="getrecirns">
	<cfif not structkeyexists(recliststruct,lbl)>
		<cfset recliststruct[lbl] = irn>
	<cfelse>
		<cfset recliststruct[lbl] = listappend(recliststruct[lbl],irn)>
	</cfif>
</cfloop>
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "DiamondDetails\Displays\dsp_#diadettype#DetailsPDF.cfm", "displays\pdfgen"))>
 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
<cfset fnamepdf = "#replace(diamondrep,' ','','all')#_#timeformat(now(),'hhnnssl')#.pdf">
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes" orientation="landscape" > 

<table cellpadding="0" cellspacing="1" border="0"  width="100%">
	
	<tr >
	<td class="bodyText" width="99%" style="font-family: Segoe UI;font-size: 10pt;color: 000000;"><strong><cfoutput>#diamondrep#</cfoutput></strong></td>
	<td class="bodyText" >&nbsp;&nbsp;</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="1" border="0"  bgcolor="5f2468" width="100%">
		<tr>
			<td align="center" bgcolor="ffffff" colspan="2">
				
				<table cellpadding="4" cellspacing="0" border="0" bgcolor="ffffff" width="100%">
					<cfoutput query="getincidents" group="lbl">
						<tr>
							<td class="purplebg" colspan="11" bgcolor="5f2468"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 8pt;color: ffffff;">#lbl#</strong></td>
						</tr>
						<tr>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Incident Number</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>#request.bulabellong#</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>#request.oulabellong#</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Project/Office</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Near Miss?</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Incident Date</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Incident Type</strong></td>
							
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>OSHA Classification</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Potential Rating</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Short Description</strong></td>
							<td class="formlable"  style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" ><strong>Record Status</strong></td>
						</tr>
						
						<cfset ctrr = 0>
						<cfset alreadyshown = "">
						
						<cfoutput >
						
						<cfif listfindnocase(alreadyshown,IncAZid) eq 0>
							<cfset showrw = "yes">
							<cfset alreadyshown = listappend(alreadyshown,IncAZid)>
						<cfelse>
							<cfset showrw = "no">
						</cfif>
						
							
						<cfif showrw eq "yes">
						
						
						
						<cfset ctrr = ctrr+1>
						<tr <cfif ctrr mod 2 is 0>class="formlable" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;background-color:f7f1f9;" </cfif>>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#TrackingNum#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#buname#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#ouname#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#Group_Name#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#isnearmiss#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#dateformat(incidentDate,sysdateformat)#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;"><cfif isfatality eq "Yes">Fatality<cfelse>#IncType#</cfif></td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#Category#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#PotentialRating#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#ShortDesc#</td>
							<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 8pt;color: 5f5f5f;">#status#
							
							</td>
						</tr>
						</cfif>
						</cfoutput>
						</cfoutput>
				</table>
			</td>
		</tr>
	</table> 
	</td></tr>
	</table>
	 </cfdocument>

			<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">