<cfset recliststruct = {}>

<cfloop query="getrecirns">
	<cfif not structkeyexists(recliststruct,lbl)>
		<cfset recliststruct[lbl] = irn>
	<cfelse>
		<cfset recliststruct[lbl] = listappend(recliststruct[lbl],irn)>
	</cfif>
</cfloop>


<table cellpadding="0" cellspacing="1" border="0"  width="100%">
	
	<tr >
	<td class="bodyText" width="99%"><strong><cfoutput>#diamondrep#</cfoutput></strong></td>
	<td class="bodyText" ><cfif fusebox.fuseaction neq "printdiamonddetail"><table cellpadding="0" cellspacing="0" border="0"><tr><td><input type="button" class="selectGenBTNgreen" value="Return" onclick="document.rundiamond.submit();">&nbsp;&nbsp;</td><td><a href="javascript:void(0);" onclick="document.printfrm.submit();"><img src="images/print.png" border="0"></a></td><td>&nbsp;&nbsp;</td><td><a href="javascript:void(0);" onclick="document.exportfrm.submit();"><img src="images/excel.png" border="0"  style="padding-right:8px;padding-top:0px;"></a></td><td><a href="javascript:void(0);" onclick="document.printtopdf.submit();"><img src="images/pdfsm.png" border="0"></a></td></tr></table></cfif></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
		<tr>
			<td align="center" bgcolor="ffffff" colspan="2">
				
				<table cellpadding="4" cellspacing="0" border="0" bgcolor="ffffff" width="100%">
					<cfoutput query="getincidents" group="lbl">
						<tr>
							<td class="purplebg" colspan="11"><strong class="bodytextwhite">#lbl#</strong></td>
						</tr>
						<tr>
							<td class="formlable" ><strong>Incident Number</strong></td>
							<td class="formlable" ><strong>#request.bulabellong#</strong></td>
							<td class="formlable" ><strong>#request.oulabellong#</strong></td>
							<td class="formlable" ><strong>Project/Office</strong></td>
							<td class="formlable" ><strong>Near Miss?</strong></td>
							<td class="formlable" ><strong>Incident Date</strong></td>
							<td class="formlable" ><strong>Incident Type</strong></td>
							
							<td class="formlable" ><strong>OSHA Classification</strong></td>
							<td class="formlable" ><strong>Potential Rating</strong></td>
							<td class="formlable" ><strong>Short Description</strong></td>
							<td class="formlable" ><strong>Record Status</strong></td>
						</tr>
						
						<cfset ctrr = 0>
						<cfset alreadyshown = "">
						
						<cfoutput >
						
						<cfif listfindnocase(alreadyshown,irn) eq 0>
							<cfset showrw = "yes">
							<cfset alreadyshown = listappend(alreadyshown,irn)>
						<cfelse>
							<cfset showrw = "no">
						</cfif>
						
							
						<cfif showrw eq "yes">
						
						
						
						<cfset ctrr = ctrr+1>
						<tr <cfif ctrr mod 2 is 0>class="formlable"</cfif>>
							<td class="bodyTextGrey">
							<cfif fusebox.fuseaction eq "printdiamonddetail">
							#TrackingNum#
							<cfelse>
							<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0  or  listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
								<cfif isfatality eq "Yes">
									<a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#IRN#">#TrackingNum#</a>
								<cfelse>
									<a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#IRN#">#TrackingNum#</a>
								</cfif>
							<cfelseif  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"User") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0>
								<cfif isfatality eq "Yes">
									#TrackingNum#
								<cfelse>
									<cfif  listfindnocase(request.userlevel,"User") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0 and  listfindnocase(request.userlevel,"Senior Reviewer") eq 0  and  listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"BU View Only") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and  listfindnocase(request.userlevel,"Reviewer") eq 0 and listfindnocase(request.userlevel,"OU View Only") eq 0>
										<a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#IRN#">#TrackingNum#</a>
										
									<cfelse>
									<a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#IRN#">#TrackingNum#</a>
									</cfif>
								</cfif>
							<cfelse>
								#TrackingNum#
							</cfif>
							</cfif></td>
							<td class="bodyTextGrey">#buname#</td>
							<td class="bodyTextGrey">#ouname#</td>
							<td class="bodyTextGrey">#Group_Name#</td>
							<td class="bodyTextGrey">#isnearmiss#</td>
							<td class="bodyTextGrey">#dateformat(incidentDate,sysdateformat)#</td>
							<td class="bodyTextGrey"><cfif isfatality eq "Yes">Fatality<cfelse>#IncType#</cfif></td>
							<td class="bodyTextGrey">#Category#</td>
							<td class="bodyTextGrey">#PotentialRating#</td>
							<!--- <td class="bodyTextGrey">#isnearmiss#</td> --->
							<td class="bodyTextGrey">#ShortDesc#</td>
							<td class="bodyTextGrey">#status#
							
							</td>
						</tr>
						</cfif>
						</cfoutput>
						</cfoutput>
				</table>
			</td>
		</tr>
	</table> 
	</td></tr>
	</table>