<cfparam name="searchname" default="">
<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.dsn#">
		select PrjSearchID
		from UserProjectSearches
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and PrjSearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
	</cfquery>

	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.dsn#">
			delete from UserProjectSearches
			where PrjSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.PrjSearchID#">
		</cfquery>
	</cfif>
	
	<!--- <cfif getnamedsearch.recordcount gt 0>
		<cfquery name="getmax" datasource="#request.dsn#">
			select max(savectr) as savectr
			from UserProjectSearches
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and PrjSearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
		</cfquery>
		<cfset newsave = getmax.savectr+1>
	<cfelse>
		<cfset newsave = 1>
	</cfif>
	 --->
	
		<cfquery name="addsearch" datasource="#request.dsn#">
			insert into UserProjectSearches (PrjSearchName, UserID, BU, OU, BS, ClientID, Project, SiteOffice, LocDetail, CountryID,savectr,ErpSys, groupactive, siteactive, mhentrytype,prjnumber)
			values(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclient#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#locdetail#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmcountryid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#newsave#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mhetype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#groupactive#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#siteactive#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#mhentrytype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prjnumber#">)
		</cfquery>
	<!--- <cfelse> --->
		<!--- <cfquery name="updatesearch" datasource="#request.dsn#">
			update UserProjectSearches
			set  BU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">,
			 OU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">,
			 BS = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">,
			 ClientID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclient#">, 
			 Project = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, 
			 SiteOffice = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">, 
			 LocDetail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#locdetail#">, 
			 CountryID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmcountryid#">
		where PrjSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.PrjSearchID#">
		</cfquery> --->
	<!--- </cfif> --->

</cfif>
</cfif>