<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportprjsearch.cfm", "exports"))>
<cfset thefilename = "ProjectSearch_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Project Search Results","true")>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Number",1,1);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"Business Stream",1,4);
	SpreadsheetSetCellValue(theSheet,"Client",1,5);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,6);
	SpreadsheetSetCellValue(theSheet,"Active Project/Office",1,7);
	SpreadsheetSetCellValue(theSheet,"Site/Office Name",1,8);
	SpreadsheetSetCellValue(theSheet,"Active Site/Office",1,9);
	SpreadsheetSetCellValue(theSheet,"Location Details",1,10);
	SpreadsheetSetCellValue(theSheet,"Country",1,11);
</cfscript>

						<cfset ctr = 1>
						<cfoutput query="getprjsearchresults">
						<cfif active_group eq 0><cfset grpstatus = "No"><cfelse><cfset grpstatus = "Yes"></cfif>
						<cfif isactive eq 0><cfset grplocstatus = "No"><cfelse><cfset grplocstatus = "Yes"></cfif>
						<cfset ctr = ctr+1>
						<cfscript> 
							SpreadsheetSetCellValue(theSheet,"P#trackingdate##numberformat(tracking_num,'0000')#",#ctr#,1);
							SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,2);
							SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,3);
							SpreadsheetSetCellValue(theSheet,"#bsname#",#ctr#,4);
							SpreadsheetSetCellValue(theSheet,"#clientname#",#ctr#,5);
							SpreadsheetSetCellValue(theSheet,"#group_name#",#ctr#,6);
							SpreadsheetSetCellValue(theSheet,"#grpstatus#",#ctr#,7);
							SpreadsheetSetCellValue(theSheet,"#sitename#",#ctr#,8);
							SpreadsheetSetCellValue(theSheet,"#grplocstatus#",#ctr#,9);
							SpreadsheetSetCellValue(theSheet,"#LocationDetail#",#ctr#,10);
							SpreadsheetSetCellValue(theSheet,"#CountryName#",#ctr#,11);
						</cfscript>
						
						</cfoutput>
		
				
		 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">