<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportcapalog.cfm", "exports"))>
<cfset thefilename = "CAPAlog_#timeformat(now(),'hhnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("CAPA Log","true")>

<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"CA/PA Log",1,1);
	 SpreadsheetMergeCells(theSheet,2,2,1,6); 
	 SpreadsheetSetCellValue(theSheet,"Incident Details",2,1);
	 SpreadsheetMergeCells(theSheet,2,2,7,12); 
	 SpreadsheetSetCellValue(theSheet,"Action Details",2,7);
	 SpreadsheetSetCellValue(theSheet,"Incident Number",3,1);
	 SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",3,2);
	 SpreadsheetSetCellValue(theSheet,"Project/Office",3,3);
	 SpreadsheetSetCellValue(theSheet,"Site/Office Name",3,4);
	 SpreadsheetSetCellValue(theSheet,"Short Description",3,5);
	  SpreadsheetSetCellValue(theSheet,"Record Status",3,6);
	 SpreadsheetSetCellValue(theSheet,"Action Type",3,7);
	 SpreadsheetSetCellValue(theSheet,"Due Date",3,8);
	 SpreadsheetSetCellValue(theSheet,"Person Responsible",3,9);
	 SpreadsheetSetCellValue(theSheet,"Details of Action Req'd",3,10);
	 SpreadsheetSetCellValue(theSheet,"CA/PA Assignee Email",3,11);
	 SpreadsheetSetCellValue(theSheet,"Date Completed",3,12);
	
</cfscript>
<cfset rctr = 3>
	<cfoutput query="capalog">
	<cfif status neq "Review Completed">
		<cfset rctr = rctr+1>
		<cfscript> 
			 SpreadsheetSetCellValue(theSheet,"#trackingnum#",#rctr#,1);
			 SpreadsheetSetCellValue(theSheet,"#ouname#",#rctr#,2);
			 SpreadsheetSetCellValue(theSheet,"#group_name#",#rctr#,3);
			 SpreadsheetSetCellValue(theSheet,"#sitename#",#rctr#,4);
			 SpreadsheetSetCellValue(theSheet,"#shortdesc#",#rctr#,5);
			 SpreadsheetSetCellValue(theSheet,"#status#",#rctr#,6);
			 SpreadsheetSetCellValue(theSheet,"#capatype#",#rctr#,7);
			 SpreadsheetSetCellValue(theSheet,"#dateformat(duedate,sysdateformatxcel)#",#rctr#,8);
			 SpreadsheetSetCellValue(theSheet,"#AssignedTo#",#rctr#,9);
			 SpreadsheetSetCellValue(theSheet,"#ActionDetail#",#rctr#,10);
			 SpreadsheetSetCellValue(theSheet,"#AssigneeEmail#",#rctr#,11);
			 SpreadsheetSetCellValue(theSheet,"#dateformat(DateComplete,sysdateformatxcel)#",#rctr#,12);
			 
		</cfscript>
	</cfif>
	</cfoutput>

 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">