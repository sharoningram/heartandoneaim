<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportreasonlate.cfm", "exports"))>
<cfset thefilename = "ReasonForLateRecording_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Reason for late recording","true")>



<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Incident Number",1,1);
	SpreadsheetSetCellValue(theSheet,"Incident Date",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,4);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,5);
	SpreadsheetSetCellValue(theSheet,"Incident Type",1,6);
	SpreadsheetSetCellValue(theSheet,"Near Miss?",1,7);
	
	SpreadsheetSetCellValue(theSheet,"OSHA Classification",1,8);
	SpreadsheetSetCellValue(theSheet,"Short Description",1,9);
	SpreadsheetSetCellValue(theSheet,"Potential Rating",1,10);
	
	SpreadsheetSetCellValue(theSheet,"Status",1,11);
	SpreadsheetSetCellValue(theSheet,"Reason for late recording",1,12);
	SpreadsheetSetCellValue(theSheet,"Date opened",1,13);
	SpreadsheetSetCellValue(theSheet,"No. days to enter",1,14);
</cfscript>

						<cfset ctr = 1>
	<cfset statstruct = {}>
	<cfset totctr = 0>
	<cfset alreadyshown = "">
	<cfloop query="getincidentlist">
	<cfif listfindnocase(alreadyshown,irn) eq 0>
	<cfset alreadyshown = listappend(alreadyshown,irn)>
		<cfset totctr = totctr+1>
		<cfif not structkeyexists(statstruct,status)>
			<cfset statstruct[status] = 1>
		<cfelse>
			<cfset statstruct[status] = statstruct[status]+1>
		</cfif>
	</cfif>
	</cfloop>
						
						<cfset alreadyshown = "">
						
						<cfoutput query="getincidentlist">
						
						<cfif listfindnocase(alreadyshown,irn) eq 0>
		<cfset showrw = "yes">
		<cfset alreadyshown = listappend(alreadyshown,irn)>
	<cfelse>
		<cfset showrw = "no">
	</cfif>
	
		
	<cfif showrw eq "yes">
						
						
						<cfset dspirp = "">
						<cfif needirp eq 1><cfset dspirp = "Yes"><cfelseif needirp eq 0><cfset dspirp = "No"></cfif>
						<cfset ctr = ctr+1>
						<cfscript> 
							SpreadsheetSetCellValue(theSheet,"#TrackingNum#",#ctr#,1);
							SpreadsheetSetCellValue(theSheet,"#dateformat(incidentDate,sysdateformatxcel)#",#ctr#,2);
							SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,3);
							SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,4);
							SpreadsheetSetCellValue(theSheet,"#Group_Name#",#ctr#,5);
							SpreadsheetSetCellValue(theSheet,"#IncType#",#ctr#,6);
							SpreadsheetSetCellValue(theSheet,"#isnearmiss#",#ctr#,7);
							
							SpreadsheetSetCellValue(theSheet,"#Category#",#ctr#,8);
							SpreadsheetSetCellValue(theSheet,"#ShortDesc#",#ctr#,9);
							
							SpreadsheetSetCellValue(theSheet,"#PotentialRating#",#ctr#,10);
							
							SpreadsheetSetCellValue(theSheet,"#status#",#ctr#,11);
							SpreadsheetSetCellValue(theSheet,"#ReasonLate#",#ctr#,12);
							SpreadsheetSetCellValue(theSheet,"#dateformat(datecreated,sysdateformatxcel)#",#ctr#,13);
							SpreadsheetSetCellValue(theSheet,"#incdiffdays#",#ctr#,14);
						</cfscript>
						
						</cfif>
						</cfoutput>
		
					<cfset ctr = ctr + 1>
		<cfoutput>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"Count of Incidents:",#ctr#,1);
			SpreadsheetSetCellValue(theSheet,"#totctr#",#ctr#,2);
		</cfscript>
		 </cfoutput>	
		 	<cfset ctr = ctr + 1>
		<cfoutput>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"",#ctr#,1);
			SpreadsheetSetCellValue(theSheet,"",#ctr#,2);
		</cfscript>
		 </cfoutput>	
		 
		 
		
	

		<cfoutput>
		<cfloop list="#structkeylist(statstruct)#" index="i">
		<cfset ctr = ctr + 1>
		<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#i#:",#ctr#,1);
			SpreadsheetSetCellValue(theSheet,"#statstruct[i]#",#ctr#,2);
		</cfscript>
		
		</cfloop>
		</cfoutput>
	
		 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">