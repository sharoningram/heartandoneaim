<cfparam name="incsub" default="S,MC,JV">
<cfparam name="incyear" default="">
<cfparam name="invlevel" default="">
<cfparam name="bu" default="All">
<cfparam name="ou" default="All">
<cfparam name="businessstream" default="All">
<cfparam name="projectoffice" default="All">
<cfparam name="site" default="All">
<cfif listfindnocase("contractorperf",fusebox.fuseaction) gt 0>
	<cfparam name="frmmgdcontractor" default="All">
	<cfparam name="frmjv" default="All">
	<cfparam name="frmsubcontractor" default="All">
<cfelse>
	<cfparam name="frmmgdcontractor" default="">
	<cfparam name="frmjv" default="">
	<cfparam name="frmsubcontractor" default="">
</cfif>
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="">
<cfparam name="frmcountryid" default="All">
<cfparam name="frmincassignto" default="All">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="All">
<cfparam name="secondtype" default="All">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="All">
<cfparam name="oidef" default="All">
<cfparam name="seriousinj" default="All">
<cfparam name="secinccats" default="All">
<cfparam name="eic" default="All">
<cfparam name="damagesrc" default="All">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="incnm" default="">
<cfparam name="assocg" default="">
<cfparam name="searchname" default="">
<cfparam name="potentialrating" default="all">
<cfparam name="occillcat" default="All">
<cfparam name="irnumber" default="">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="manhrtype" default="All">
<cfif listfindnocase("capalog",fusebox.fuseaction) gt 0>
	<cfparam name="beentoirp" default="All">
<cfelse>
	<cfparam name="beentoirp" default="No">
</cfif>
<cfparam name="apptype" default="oneAIM">
<cfparam name="capatype" default="All">
<cfparam name="capastatus" default="All">
<cfparam name="frminjtype" default="all">
<cfparam name="frmbodypart" default="all">
<cfparam name="frminjcause" default="all">
<cfparam name="droppedobj" default="All">

<cfparam name="weightedhours" default="Yes">


<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.dsn#">
		select UserReportID
		from UserSavedReports
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#"> and reportType = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#reptype#">
	</cfquery>
	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.dsn#">
			delete from UserSavedReports
			where UserReportID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.UserReportID#">
		</cfquery>
	</cfif>
	<!--- <cfif getnamedsearch.recordcount gt 0>
		<cfquery name="getmax" datasource="#request.dsn#">
			select max(savectr) as savectr
			from UserSavedReports
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
		</cfquery>
		<cfset newsave = getmax.savectr+1>
	<cfelse>
		<cfset newsave = 1>
	</cfif> --->
	
		

		<cfquery name="addsearch" datasource="#request.dsn#">
			insert into UserSavedReports (SearchName, UserID, incyear, invlevel, bu, ou, bs, project, siteoffice, mgdcontractor, jv, subcon, clientid, locdetail, countryid, 
                         incassigned, wronly, primarytype, secondtype, hipoony, oshaclass, oidef, seriousinj, secinccats, eic, damagesrc, startdate, enddate, SaveCtr,incnm,reportType,assocgroups,potentialrating,occillcat,irnumber,keywords,incstatus,manhrtype,beentoirp,apptype,capatype,capastatus,frminjtype,frmbodypart,frminjcause,droppedobj,weightedhours,incsub)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incyear#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmmgdcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmjv#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmsubcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclient#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#locdetail#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmcountryid#">, 
                         <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmincassignto#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wronly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#primarytype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secondtype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#hipoonly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oshaclass#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oidef#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secinccats#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#eic#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#damagesrc#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#newsave#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incnm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#reptype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#assocg#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#occillcat#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#irnumber#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#keywords#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incstatus#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#manhrtype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#beentoirp#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#apptype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#capatype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#capastatus#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frminjtype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmbodypart#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frminjcause#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#droppedobj#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#weightedhours#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incsub#">)
		</cfquery>
	
</cfif>
</cfif>