<cfparam name="diadettype" default="">
<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>
<cfset qryproject = projectoffice>

<cfif listfindnocase(projectoffice,"All") eq 0>
	<cfif assocg eq "Yes">
		<cfset gtoview = "">
			<cfloop list="#projectoffice#" index="i">
				<cfif listfind(gtoview,i) eq 0>
					<cfset gtoview = listappend(gtoview,i)>
				</cfif>
				
				<cfquery name="getassocg" datasource="#request.dsn#">
					SELECT        GroupNumber1, GroupNumber2
					FROM            GroupAssociations
					WHERE        (GroupNumber1 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">) OR
                         (GroupNumber2 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
				</cfquery>
				<cfloop query="getassocg">
					<cfif listfind(gtoview,GroupNumber1) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber1)>
					</cfif>
					<cfif listfind(gtoview,GroupNumber2) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber2)>
					</cfif>
				</cfloop>
			</cfloop>
		
		<cfset qryproject = gtoview>
	</cfif>
</cfif>


<cfset contractlist = "">
<cfif trim(frmmgdcontractor) neq ''>
<cfif listfindnocase(frmmgdcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmmgdcontractor)>
</cfif>
</cfif>
<cfif trim(frmjv) neq ''>
<cfif listfindnocase(frmjv,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmjv)>
</cfif>
</cfif>
<cfif trim(frmsubcontractor) neq ''>
<cfif listfindnocase(frmsubcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmsubcontractor)>
</cfif>
</cfif>

<cfoutput>
<cfform action="#self#?fuseaction=#attributes.xfa.rundiamond#" method="post" name="rundiamond">
<input type="hidden" name="diamondtype" value="">	
<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="charttype" value="#charttype#">
<input type="hidden" name="chartvalue" value="#chartvalue#">				
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="incandor" value="#incandor#">
<input type="hidden" name="droppedobj" value="#droppedobj#">
</cfform>		


<cfform action="#self#?fuseaction=#attributes.xfa.exportdiamonddetail#" method="post" name="exportfrm" target="_blank">
<input type="hidden" name="diamondtype" value="">	
<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="charttype" value="#charttype#">
<input type="hidden" name="chartvalue" value="#chartvalue#">				
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="incandor" value="#incandor#">
<input type="hidden" name="droppedobj" value="#droppedobj#">
<input type="hidden" name="diadettype" value="#diadettype#">
</cfform>	

<cfform action="#self#?fuseaction=#attributes.xfa.printdiamonddetail#" method="post" name="printfrm" target="_blank">
<input type="hidden" name="diamondtype" value="">	
<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="charttype" value="#charttype#">
<input type="hidden" name="chartvalue" value="#chartvalue#">				
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="incandor" value="#incandor#">
<input type="hidden" name="droppedobj" value="#droppedobj#">
<input type="hidden" name="diadettype" value="#diadettype#">
</cfform>		


<cfform action="#self#?fuseaction=#attributes.xfa.printdiamonddetailpdf#" method="post" name="printtopdf" target="_blank">
<input type="hidden" name="diamondtype" value="">	
<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
<input type="hidden" name="assocg" value="#assocg#">
<input type="hidden" name="charttype" value="#charttype#">
<input type="hidden" name="chartvalue" value="#chartvalue#">				
<input type="hidden" name="potentialrating" value="#potentialrating#">	
<input type="hidden" name="incandor" value="#incandor#">
<input type="hidden" name="droppedobj" value="#droppedobj#">
<input type="hidden" name="diadettype" value="#diadettype#">
</cfform>	


</cfoutput>		
<br>
<cfswitch expression="#diadettype#">
	<!--- ****** General Reports Start ****** --->
	<cfcase value="tio">
		<cfset diamondrep = "Time Incident Occurred">
		<cfinclude template="../diamonddetails/queries/qry_getTioDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportTioDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_TioDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_TioDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="weekday">
		<cfset diamondrep = "Day of Week">
		<cfinclude template="../diamonddetails/queries/qry_getWeekDayDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportWeekDayDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_WeekDayDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_WeekDayDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="weather">
		<cfset diamondrep = "Primary Weather Conditions">
		<cfinclude template="../diamonddetails/queries/qry_getWeatherDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportWeatherDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_WeatherDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_WeatherDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="statbody">
		<cfset diamondrep = "Reported to Statutory Body">
		<cfinclude template="../diamonddetails/queries/qry_getstatbodyDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportstatbodyDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_statbodyDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_statbodyDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="immcause">
		<cfset diamondrep = "Immediate Cause">
		<cfinclude template="../diamonddetails/queries/qry_getimmcauseDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportimmcauseDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_immcauseDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_immcauseDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="immcausesub">
		<cfset diamondrep = "Immediate Cause Sub-Type">
		<cfinclude template="../diamonddetails/queries/qry_getimmcausesubDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportimmcausesubDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_immcausesubDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_immcausesubDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="immcausesub10">
		<cfset diamondrep = "Immediate Cause Sub-Type Top 10">
		<cfinclude template="../diamonddetails/queries/qry_getimmcausesub10Detail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportimmcausesub10Details.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_immcausesub10DetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_immcausesub10Details.cfm">
		</cfif>
	</cfcase>
	<cfcase value="rootcause">
		<cfset diamondrep = "Root Cause">
		<cfinclude template="../diamonddetails/queries/qry_getrootcauseDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportrootcauseDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_rootcauseDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_rootcauseDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="rootcausesub">
		<cfset diamondrep = "Root Cause Sub-Type">
		<cfinclude template="../diamonddetails/queries/qry_getrootcausesubDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportrootcausesubDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_rootcausesubDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_rootcausesubDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="rootcausesub10">
		<cfset diamondrep = "Root Cause Sub-Type Top 10">
		<cfinclude template="../diamonddetails/queries/qry_getrootcausesub10Detail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportrootcausesub10Details.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_rootcausesub10DetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_rootcausesub10Details.cfm">
		</cfif>
	</cfcase>
	<cfcase value="seb">
		<cfset diamondrep = "Safety Essential Breach">
		<cfinclude template="../diamonddetails/queries/qry_getsebDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportsebDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_sebDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_sebDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="lsb">
		<cfset diamondrep = "Global Safety Rules">
		<cfinclude template="../diamonddetails/queries/qry_getlsbDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportlsbDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_lsbDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_lsbDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="bymonth">
		<cfset diamondrep = "Incidents by Month">
		<cfinclude template="../diamonddetails/queries/qry_getbymonthDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportbymonthDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_bymonthDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_bymonthDetails.cfm">
		</cfif>
	</cfcase>
	<!--- ****** General Reports End ****** --->
	<!--- ****** Injury Illness and Occupation Health Reports Start ****** --->
	<cfcase value="hazard">
		<cfset diamondrep = "Source of Hazard">
		<cfinclude template="../diamonddetails/queries/qry_gethazardDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exporthazardDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_hazardDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_hazardDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="injtype">
		<cfset diamondrep = "Injury Type">
		<cfinclude template="../diamonddetails/queries/qry_getinjtypeDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportinjtypeDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_injtypeDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_injtypeDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="illtype">
		<cfset diamondrep = "Illness Type">
		<cfinclude template="../diamonddetails/queries/qry_getilltypeDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportilltypeDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_illtypeDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_illtypeDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="bodypart">
		<cfset diamondrep = "Body Part">
		<cfinclude template="../diamonddetails/queries/qry_getbodypartDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportbodypartDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_bodypartDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_bodypartDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="injcause">
		<cfset diamondrep = "Cause of Injury Incident">
		<cfinclude template="../diamonddetails/queries/qry_getinjcauseDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportinjcauseDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_injcauseDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_injcauseDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="illnature">
		<cfset diamondrep = "Nature of Illness Incident">
		<cfinclude template="../diamonddetails/queries/qry_getillnatureDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportillnatureDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_illnatureDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_illnatureDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="age">
		<cfset diamondrep = "Age Profile">
		<cfinclude template="../diamonddetails/queries/qry_getageDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportageDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_ageDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_ageDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="occupation">
		<cfset diamondrep = "Injured Persons Occupation">
		<cfinclude template="../diamonddetails/queries/qry_getoccupationDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportoccupationDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_occupationDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_occupationDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="gender">
		<cfset diamondrep = "Gender">
		<cfinclude template="../diamonddetails/queries/qry_getgenderDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportgenderDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_genderDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_genderDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="shift">
		<cfset diamondrep = "Shift">
		<cfinclude template="../diamonddetails/queries/qry_getshiftDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportshiftDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_shiftDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_shiftDetails.cfm">
		</cfif>
	</cfcase>
	<!--- ****** Injury Illness and Occupation Health Reports End ****** --->
	<!--- ****** Asset Damage Reports Start ****** --->
	<cfcase value="damagenature">
		<cfset diamondrep = "Nature of Damage">
		<cfinclude template="../diamonddetails/queries/qry_getdamagenatureDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportdamagenatureDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_damagenatureDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_damagenatureDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="proptype">
		<cfset diamondrep = "Property Type">
		<cfinclude template="../diamonddetails/queries/qry_getproptypeDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportproptypeDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_proptypeDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_proptypeDetails.cfm">
		</cfif>
	</cfcase>
	<!--- ****** Asset Damage Reports End ****** --->
	<!--- ****** Environmental Incident Reports Start ****** --->
	<cfcase value="inbreach">
		<cfset diamondrep = "In Breach of an Environmental Permit or License or Legislation">
		<cfinclude template="../diamonddetails/queries/qry_getinbreachDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportinbreachDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_inbreachDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_inbreachDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="polltype">
		<cfset diamondrep = "Type of Pollution Event">
		<cfinclude template="../diamonddetails/queries/qry_getpolltypeDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportpolltypeDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_polltypeDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_polltypeDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="substance">
		<cfset diamondrep = "Type of Substance">
		<cfinclude template="../diamonddetails/queries/qry_getsubstanceDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportsubstanceDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_substanceDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_substanceDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="releasesrc">
		<cfset diamondrep = "Source of Release">
		<cfinclude template="../diamonddetails/queries/qry_getreleasesrcDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportreleasesrcDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_releasesrcDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_releasesrcDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="releasedur">
		<cfset diamondrep = "Duration of Release">
		<cfinclude template="../diamonddetails/queries/qry_getreleasedurDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportreleasedurDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_releasedurDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_releasedurDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="enviro">
		<cfset diamondrep = "Receiving Environment">
		<cfinclude template="../diamonddetails/queries/qry_getenviroDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportenviroDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_enviroDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_enviroDetails.cfm">
		</cfif>
	</cfcase>
	<!--- ****** Environmental Incident Reports End ****** --->
	<!--- ****** Security Incident Reports Start ****** --->
	<cfcase value="secthreat">
		<cfset diamondrep = "Source of Security Threat">
		<cfinclude template="../diamonddetails/queries/qry_getsecthreatDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportsecthreatDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_secthreatDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_secthreatDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="whileabroad">
		<cfset diamondrep = "Incident Whilst Travelling Abroad">
		<cfinclude template="../diamonddetails/queries/qry_getwhileabroadDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportwhileabroadDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_whileabroadDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_whileabroadDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="whichcountry">
		<cfset diamondrep = "Security Incident on Travel by Country">
		<cfinclude template="../diamonddetails/queries/qry_getwhichcountryDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportwhichcountryDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_whichcountryDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_whichcountryDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="intelpropcat">
		<cfset diamondrep = "Intellectual Property Incident Category">
		<cfinclude template="../diamonddetails/queries/qry_getintelpropcatDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportintelpropcatDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_intelpropcatDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_intelpropcatDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="weapontype">
		<cfset diamondrep = "Weapon Types">
		<cfinclude template="../diamonddetails/queries/qry_getweapontypeDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportweapontypeDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_weapontypeDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_weapontypeDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="allnat">
		<cfset diamondrep = "All Nature of Incident">
		<cfinclude template="../diamonddetails/queries/qry_getallnatDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportallnatDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_allnatDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_allnatDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="natperson">
		<cfset diamondrep = "Nature of Incident - Person">
		<cfinclude template="../diamonddetails/queries/qry_getnatpersonDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportnatpersonDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_natpersonDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_natpersonDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="natinfo">
		<cfset diamondrep = "Nature of Incident - Information Security">
		<cfinclude template="../diamonddetails/queries/qry_getnatinfoDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportnatinfoDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_natinfoDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_natinfoDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="natsecurity">
		<cfset diamondrep = "Nature of Incident - Security Practice Breach">
		<cfinclude template="../diamonddetails/queries/qry_getnatsecurityDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportnatsecurityDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_natsecurityDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_natsecurityDetails.cfm">
		</cfif>
	</cfcase>
	<cfcase value="natbiz">
		<cfset diamondrep = "Nature of Incident - Business Asset">
		<cfinclude template="../diamonddetails/queries/qry_getnatbizDetail.cfm">
		<cfif fusebox.fuseaction eq "exportdiamonddetail">
			<cfinclude template="../diamonddetails/actions/act_exportnatbizDetails.cfm">
		<cfelseif fusebox.fuseaction eq "printdiamonddetailpdf">
			<cfinclude template="../diamonddetails/displays/dsp_natbizDetailsPDF.cfm">
		<cfelse>
			<cfinclude template="../diamonddetails/displays/dsp_natbizDetails.cfm">
		</cfif>
	</cfcase>
	<!--- ****** Security Incident Reports End ****** --->
	
</cfswitch>