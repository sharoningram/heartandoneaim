<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportincbytype.cfm", "exports"))>
<cfset thefilename = "IncidentsByType_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Incidents By Type","true")>

<cfset iostruct = {}>
<cfloop query="getincoi">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		
		<cfif PrimaryType eq 5>
			<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
				</cfif>
			<cfelse>
				<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"]+1>
				</cfif>
			</cfif>
			<cfif OIdefinition eq 2 and isnearmiss eq "no">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_OIDEF_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_OIDEF_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_OIDEF_#IncAssignedTo#"] =  iostruct["#PrimaryType#_OIDEF_#IncAssignedTo#"]+1>
				</cfif>
			
			</cfif>
		
		<cfelseif primarytype eq 1>
		
		<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
				</cfif>
		<cfelse>
		
			<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_#IncAssignedTo#")>
				<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] = 1>
			<cfelse>
				<cfset iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#catid#_#IncAssignedTo#"]+1>
			</cfif>
		
		</cfif>
		</cfif>
	<cfelse>
		<cfif PrimaryType eq 5>
		<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
				</cfif>
		<cfelse>
		
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_oth")>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] =  iostruct["#PrimaryType#_#catid#_oth"]+1>
		</cfif>
		</cfif>
		<cfif OIdefinition eq 2>
				<cfif not structkeyexists(iostruct,"#PrimaryType#_OIDEF_oth")>
					<cfset iostruct["#PrimaryType#_OIDEF_oth"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_OIDEF_oth"] =  iostruct["#PrimaryType#_OIDEF_oth"]+1>
				</cfif>
			
			</cfif>
		<cfelseif primarytype eq 1>
		<cfif isnearmiss eq "yes">
				<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
				<cfelse>
					<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
				</cfif>
		<cfelse>
		
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#catid#_oth")>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#catid#_oth"] =  iostruct["#PrimaryType#_#catid#_oth"]+1>
		</cfif>
		
		</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="incasset">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#DamageSource#_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_#DamageSource#_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#DamageSource#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#DamageSource#_#IncAssignedTo#"]+1>
		</cfif>
		</cfif>
	<cfelse>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#DamageSource#_oth")>
			<cfset iostruct["#PrimaryType#_#DamageSource#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#DamageSource#_oth"] =  iostruct["#PrimaryType#_#DamageSource#_oth"]+1>
		</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="getenv">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#EnvIncCat#_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#EnvIncCat#_#IncAssignedTo#"]+1>
		</cfif>
		</cfif>
	<cfelse>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#EnvIncCat#_oth")>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#EnvIncCat#_oth"] =  iostruct["#PrimaryType#_#EnvIncCat#_oth"]+1>
		</cfif>
		</cfif>
	</cfif>
</cfloop>
<cfloop query="getsec">
	<cfif listfind("0,8,9,10",IncAssignedTo) eq 0>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"] =  iostruct["#PrimaryType#_NearMiss_#IncAssignedTo#"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#SecIncCat#_#IncAssignedTo#")>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_#IncAssignedTo#"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_#IncAssignedTo#"] =  iostruct["#PrimaryType#_#SecIncCat#_#IncAssignedTo#"]+1>
		</cfif>
		</cfif>
	<cfelse>
		<cfif isnearmiss eq "yes">
		<cfif not structkeyexists(iostruct,"#PrimaryType#_NearMiss_oth")>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_NearMiss_oth"] =  iostruct["#PrimaryType#_NearMiss_oth"]+1>
		</cfif>
		<cfelse>
		<cfif not structkeyexists(iostruct,"#PrimaryType#_#SecIncCat#_oth")>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_oth"] = 1>
		<cfelse>
			<cfset iostruct["#PrimaryType#_#SecIncCat#_oth"] =  iostruct["#PrimaryType#_#SecIncCat#_oth"]+1>
		</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Incident Type",1,1);
	SpreadsheetSetCellValue(theSheet,"",1,2);
	SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler",1,3);
	
	SpreadsheetSetCellValue(theSheet,"Sub-Contractor",1,4);
	SpreadsheetSetCellValue(theSheet,"Managed Contractor",1,5);
	SpreadsheetSetCellValue(theSheet,"Joint Venture",1,6);
	SpreadsheetSetCellValue(theSheet,"Other",1,7);
	SpreadsheetSetCellValue(theSheet,"Count",1,8);
	
</cfscript><!--- SpreadsheetSetCellValue(theSheet,"Another Amec-FW Co (working as Contractor)",1,4); --->
<cfset srowctr = 1>




<cfset col1tot = 0><cfset col2tot = 0><cfset col3tot = 0><cfset col4tot= 0><cfset col5tot = 0><cfset col6tot = 0><cfset col7tot = 0>
	<cfoutput query="getsortedincs" group="inctypeid">
	<cfset col1ctr = 0><cfset col2ctr = 0><cfset col3ctr = 0><cfset col4ctr = 0><cfset col5ctr = 0><cfset col6ctr = 0><cfset col7ctr = 0>
	<cfset gctr = 0>
	<cfoutput>
	<cfset srowctr = srowctr+1>
	<cfset gctr = gctr + 1>
	<cfset rctr = 0>
	
	
	
		<cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_1")><cfset dsp1 =  iostruct["#inctypeid#_#incsubtypeid#_1"]><cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_#incsubtypeid#_1"]><cfelse><cfset dsp1 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_2")><cfset dsp2 =  iostruct["#inctypeid#_#incsubtypeid#_2"]><cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_#incsubtypeid#_2"]><cfelse><cfset dsp2 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_3")><cfset dsp3 =  iostruct["#inctypeid#_#incsubtypeid#_3"]><cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_#incsubtypeid#_3"]><cfelse><cfset dsp3 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_4")><cfset dsp4 =  iostruct["#inctypeid#_#incsubtypeid#_4"]><cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_#incsubtypeid#_4"]><cfelse><cfset dsp4 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_11")><cfset dsp5 =  iostruct["#inctypeid#_#incsubtypeid#_11"]><cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_#incsubtypeid#_11"]><cfelse><cfset dsp5 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_#incsubtypeid#_oth")><cfset dsp6 =  iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfset rctr = rctr+iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_#incsubtypeid#_oth"]><cfelse><cfset dsp6 =  0></cfif>
		<cfset col7ctr = col7ctr+rctr>
	
	<cfscript> 
	SpreadsheetSetCellValue(theSheet,"#IncType#",#srowctr#,1);
	SpreadsheetSetCellValue(theSheet,"#incsubtype#",#srowctr#,2);
	SpreadsheetSetCellValue(theSheet,"#dsp1#",#srowctr#,3);
	
	SpreadsheetSetCellValue(theSheet,"#dsp3#",#srowctr#,4);
	SpreadsheetSetCellValue(theSheet,"#dsp4#",#srowctr#,5);
	SpreadsheetSetCellValue(theSheet,"#dsp5#",#srowctr#,6);
	SpreadsheetSetCellValue(theSheet,"#dsp6#",#srowctr#,7);
	SpreadsheetSetCellValue(theSheet,"#rctr#",#srowctr#,8);
	
</cfscript><!--- SpreadsheetSetCellValue(theSheet,"#dsp2#",#srowctr#,4); --->
	
	</cfoutput>
	<cfif inctypeid eq 5>
	<cfset rctr = 0>
	
	<cfset srowctr = srowctr+1>
	<cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_1")><cfset dsp1 =  iostruct["#inctypeid#_OIDEF_1"]><cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_OIDEF_1"]><cfelse><cfset dsp1 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_2")><cfset dsp2 =  iostruct["#inctypeid#_OIDEF_2"]><cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_OIDEF_2"]><cfelse><cfset dsp2 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_3")><cfset dsp3 =  iostruct["#inctypeid#_OIDEF_3"]><cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_OIDEF_3"]><cfelse><cfset dsp3 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_4")><cfset dsp4 =  iostruct["#inctypeid#_OIDEF_4"]><cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_OIDEF_4"]><cfelse><cfset dsp4 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_11")><cfset dsp5 =  iostruct["#inctypeid#_OIDEF_11"]><cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_OIDEF_11"]><cfelse><cfset dsp5 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_OIDEF_oth")><cfset dsp6 =  iostruct["#inctypeid#_OIDEF_oth"]><cfset rctr = rctr+iostruct["#inctypeid#_OIDEF_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_OIDEF_oth"]><cfelse><cfset dsp6 =  0></cfif>
		<cfset col7ctr = col7ctr+rctr>
	
	<cfscript> 
	SpreadsheetSetCellValue(theSheet,"#IncType#",#srowctr#,1);
	SpreadsheetSetCellValue(theSheet,"Non-Discrete",#srowctr#,2);
	SpreadsheetSetCellValue(theSheet,"#dsp1#",#srowctr#,3);
	
	SpreadsheetSetCellValue(theSheet,"#dsp3#",#srowctr#,4);
	SpreadsheetSetCellValue(theSheet,"#dsp4#",#srowctr#,5);
	SpreadsheetSetCellValue(theSheet,"#dsp5#",#srowctr#,6);
	SpreadsheetSetCellValue(theSheet,"#dsp6#",#srowctr#,7);
	SpreadsheetSetCellValue(theSheet,"#rctr#",#srowctr#,8);
	
</cfscript><!--- SpreadsheetSetCellValue(theSheet,"#dsp2#",#srowctr#,4); --->
	</cfif>
	<cfif inctypeid neq 4>
	<cfset rctr = 0>
	<cfset srowctr = srowctr+1>
		<cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_1")><cfset dsp1 =  iostruct["#inctypeid#_NearMiss_1"]><cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_1"]><cfset col1ctr = col1ctr+iostruct["#inctypeid#_NearMiss_1"]><cfelse><cfset dsp1 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_2")><cfset dsp2 =  iostruct["#inctypeid#_NearMiss_2"]><cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_2"]><cfset col2ctr = col2ctr+iostruct["#inctypeid#_NearMiss_2"]><cfelse><cfset dsp2 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_3")><cfset dsp3 =  iostruct["#inctypeid#_NearMiss_3"]><cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_3"]><cfset col3ctr = col3ctr+iostruct["#inctypeid#_NearMiss_3"]><cfelse><cfset dsp3 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_4")><cfset dsp4 =  iostruct["#inctypeid#_NearMiss_4"]><cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_4"]><cfset col4ctr = col4ctr+iostruct["#inctypeid#_NearMiss_4"]><cfelse><cfset dsp4 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_11")><cfset dsp5 =  iostruct["#inctypeid#_NearMiss_11"]><cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_11"]><cfset col5ctr = col5ctr+iostruct["#inctypeid#_NearMiss_11"]><cfelse><cfset dsp5 =  0></cfif>
		<cfif structkeyexists(iostruct,"#inctypeid#_NearMiss_oth")><cfset dsp6 =  iostruct["#inctypeid#_NearMiss_oth"]><cfset rctr = rctr+iostruct["#inctypeid#_NearMiss_oth"]><cfset col6ctr = col6ctr+iostruct["#inctypeid#_NearMiss_oth"]><cfelse><cfset dsp6 =  0></cfif>
		<cfset col7ctr = col7ctr+rctr>
	
	<cfscript> 
	SpreadsheetSetCellValue(theSheet,"#IncType#",#srowctr#,1);
	SpreadsheetSetCellValue(theSheet,"Near Miss",#srowctr#,2);
	SpreadsheetSetCellValue(theSheet,"#dsp1#",#srowctr#,3);
	
	SpreadsheetSetCellValue(theSheet,"#dsp3#",#srowctr#,4);
	SpreadsheetSetCellValue(theSheet,"#dsp4#",#srowctr#,5);
	SpreadsheetSetCellValue(theSheet,"#dsp5#",#srowctr#,6);
	SpreadsheetSetCellValue(theSheet,"#dsp6#",#srowctr#,7);
	SpreadsheetSetCellValue(theSheet,"#rctr#",#srowctr#,8);
	
</cfscript><!--- SpreadsheetSetCellValue(theSheet,"#dsp2#",#srowctr#,4); --->
	</cfif>
	<cfset srowctr = srowctr+1>
	
		<cfset col1tot = col1tot + col1ctr>
		<cfset col2tot = col2tot + col2ctr>
		<cfset col3tot = col3tot + col3ctr>
		<cfset col4tot = col4tot + col4ctr>
		<cfset col5tot = col5tot + col5ctr>
		<cfset col6tot = col6tot + col6ctr>
		<cfset col7tot = col7tot + col7ctr>
	<cfscript> 
	SpreadsheetSetCellValue(theSheet,"#IncType#",#srowctr#,1);
	SpreadsheetSetCellValue(theSheet,"Sub-Total",#srowctr#,2);
	SpreadsheetSetCellValue(theSheet,"#col1ctr#",#srowctr#,3);
	
	SpreadsheetSetCellValue(theSheet,"#col3ctr#",#srowctr#,4);
	SpreadsheetSetCellValue(theSheet,"#col4ctr#",#srowctr#,5);
	SpreadsheetSetCellValue(theSheet,"#col5ctr#",#srowctr#,6);
	SpreadsheetSetCellValue(theSheet,"#col6ctr#",#srowctr#,7);
	SpreadsheetSetCellValue(theSheet,"#col7ctr#",#srowctr#,8);
	
</cfscript><!--- SpreadsheetSetCellValue(theSheet,"#col2ctr#",#srowctr#,4); --->
	</cfoutput>
	<cfoutput>
	<cfset srowctr = srowctr+1>
		<cfscript> 
	SpreadsheetSetCellValue(theSheet,"",#srowctr#,1);
	SpreadsheetSetCellValue(theSheet,"Total",#srowctr#,2);
	SpreadsheetSetCellValue(theSheet,"#col1tot#",#srowctr#,3);
	
	SpreadsheetSetCellValue(theSheet,"#col3tot#",#srowctr#,4);
	SpreadsheetSetCellValue(theSheet,"#col4tot#",#srowctr#,5);
	SpreadsheetSetCellValue(theSheet,"#col5tot#",#srowctr#,6);
	SpreadsheetSetCellValue(theSheet,"#col6tot#",#srowctr#,7);
	SpreadsheetSetCellValue(theSheet,"#col7tot#",#srowctr#,8);
	
</cfscript><!--- SpreadsheetSetCellValue(theSheet,"#col2tot#",#srowctr#,4); --->
	
	</cfoutput>

	
	<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">