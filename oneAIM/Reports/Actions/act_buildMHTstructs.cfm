<cfparam name="weightedhours" default="yes">

<cfset manhrstruct = {}>
<cfset manhrstructloc = {}>
<cfset manhrstructgrp = {}>

<cfloop query="getjdehrs">
	
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + jdehrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + jdehrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + jdehrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstructgrp,group_number)>
			<cfset manhrstructgrp[group_number] = jdehrs>
		<cfelse>
			<cfset manhrstructgrp[group_number] = manhrstructgrp[group_number] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstructgrp,group_number)>
				<cfset manhrstructgrp[group_number] = jdehrs>
			<cfelse>
				<cfset manhrstructgrp[group_number] = manhrstructgrp[group_number] + jdehrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(manhrstructloc,GroupLocID)>
			<cfset manhrstructloc[GroupLocID] = jdehrs>
		<cfelse>
			<cfset manhrstructloc[GroupLocID] = manhrstructloc[GroupLocID] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstructloc,GroupLocID)>
				<cfset manhrstructloc[GroupLocID] = jdehrs>
			<cfelse>
				<cfset manhrstructloc[GroupLocID] = manhrstructloc[GroupLocID] + jdehrs>
			</cfif>
		</cfif>
		
</cfloop>
<cfloop query="getjdeoh">
	
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = oh>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = oh>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + oh>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = oh>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = oh>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + oh>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = oh>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = oh>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + oh>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstructgrp,group_number)>
			<cfset manhrstructgrp[group_number] = oh>
		<cfelse>
			<cfset manhrstructgrp[group_number] = manhrstructgrp[group_number] + oh>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstructgrp,group_number)>
				<cfset manhrstructgrp[group_number] = oh>
			<cfelse>
				<cfset manhrstructgrp[group_number] = manhrstructgrp[group_number] + oh>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstructloc,GroupLocID)>
			<cfset manhrstructloc[GroupLocID] = oh>
		<cfelse>
			<cfset manhrstructloc[GroupLocID] = manhrstructloc[GroupLocID] + oh>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstructloc,GroupLocID)>
				<cfset manhrstructloc[GroupLocID] = oh>
			<cfelse>
				<cfset manhrstructloc[GroupLocID] = manhrstructloc[GroupLocID] + oh>
			</cfif>
		</cfif>
		
</cfloop>

<cfset subhrsstruct = {}>
<cfset jvhrsstruct = {}>
<cfset mdghrsstruct = {}>

<cfset subhrsstructloc = {}>
<cfset jvhrsstructloc = {}>
<cfset mdghrsstructloc = {}>

<cfset subhrsstructgrp = {}>
<cfset jvhrsstructgrp = {}>
<cfset mdghrsstructgrp = {}>

<cfloop query="getnonjdehrs">
		
		<cfif not structkeyexists(manhrstructloc,GroupLocID)>
			<cfset manhrstructloc[GroupLocID] = jdehrs>
		<cfelse>
			<cfset manhrstructloc[GroupLocID] = manhrstructloc[GroupLocID] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstructloc,GroupLocID)>
				<cfset manhrstructloc[GroupLocID] = jdehrs>
			<cfelse>
				<cfset manhrstructloc[GroupLocID] = manhrstructloc[GroupLocID] + jdehrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(jvhrsstructloc,GroupLocID)>
			<cfset jvhrsstructloc[GroupLocID] = jvhrs>
		<cfelse>
			<cfset jvhrsstructloc[GroupLocID] = jvhrsstructloc[GroupLocID] + jvhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstructloc,GroupLocID)>
				<cfset jvhrsstructloc[GroupLocID] = jvhrs>
			<cfelse>
				<cfset jvhrsstructloc[GroupLocID] = jvhrsstructloc[GroupLocID] + jvhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(subhrsstructloc,GroupLocID)>
			<cfset subhrsstructloc[GroupLocID] = subhrs>
		<cfelse>
			<cfset subhrsstructloc[GroupLocID] = subhrsstructloc[GroupLocID] + subhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstructloc,GroupLocID)>
				<cfset subhrsstructloc[GroupLocID] = subhrs>
			<cfelse>
				<cfset subhrsstructloc[GroupLocID] = subhrsstructloc[GroupLocID] + subhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstructloc,GroupLocID)>
			<cfset mdghrsstructloc[GroupLocID] = mdghrs>
		<cfelse>
			<cfset mdghrsstructloc[GroupLocID] = mdghrsstructloc[GroupLocID] + mdghrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstructloc,GroupLocID)>
				<cfset mdghrsstructloc[GroupLocID] = mdghrs>
			<cfelse>
				<cfset mdghrsstructloc[GroupLocID] = mdghrsstructloc[GroupLocID] + mdghrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(mdghrsstruct,"Global")>
			<cfset mdghrsstruct["Global"] = mdghrs>
		<cfelse>
			<cfset mdghrsstruct["Global"] = mdghrsstruct["Global"] + mdghrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstruct,"Global")>
				<cfset mdghrsstruct["Global"] = mdghrs>
			<cfelse>
				<cfset mdghrsstruct["Global"] = mdghrsstruct["Global"] + mdghrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstruct,"#business_line_id#")>
			<cfset mdghrsstruct["#business_line_id#"] = mdghrs>
		<cfelse>
			<cfset mdghrsstruct["#business_line_id#"] = mdghrsstruct["#business_line_id#"] + mdghrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstruct,"#business_line_id#")>
				<cfset mdghrsstruct["#business_line_id#"] = mdghrs>
			<cfelse>
				<cfset mdghrsstruct["#business_line_id#"] = mdghrsstruct["#business_line_id#"] + mdghrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstruct,"#ouid#")>
			<cfset mdghrsstruct["#ouid#"] = mdghrs>
		<cfelse>
			<cfset mdghrsstruct["#ouid#"] = mdghrsstruct["#ouid#"] + mdghrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstruct,"#ouid#")>
				<cfset mdghrsstruct["#ouid#"] = mdghrs>
			<cfelse>
				<cfset mdghrsstruct["#ouid#"] = mdghrsstruct["#ouid#"] + mdghrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstructgrp,group_number)>
			<cfset mdghrsstructgrp[group_number] = mdghrs>
		<cfelse>
			<cfset mdghrsstructgrp[group_number] = mdghrsstructgrp[group_number] + mdghrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstructgrp,group_number)>
				<cfset mdghrsstructgrp[group_number] = mdghrs>
			<cfelse>
				<cfset mdghrsstructgrp[group_number] = mdghrsstructgrp[group_number] + mdghrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(jvhrsstruct,"Global")>
			<cfset jvhrsstruct["Global"] = jvhrs>
		<cfelse>
			<cfset jvhrsstruct["Global"] = jvhrsstruct["Global"] + jvhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstruct,"Global")>
				<cfset jvhrsstruct["Global"] = jvhrs>
			<cfelse>
				<cfset jvhrsstruct["Global"] = jvhrsstruct["Global"] + jvhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(jvhrsstruct,"#business_line_id#")>
			<cfset jvhrsstruct["#business_line_id#"] = jvhrs>
		<cfelse>
			<cfset jvhrsstruct["#business_line_id#"] = jvhrsstruct["#business_line_id#"] + jvhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstruct,"#business_line_id#")>
				<cfset jvhrsstruct["#business_line_id#"] = jvhrs>
			<cfelse>
				<cfset jvhrsstruct["#business_line_id#"] = jvhrsstruct["#business_line_id#"] + jvhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(jvhrsstruct,"#ouid#")>
			<cfset jvhrsstruct["#ouid#"] = jvhrs>
		<cfelse>
			<cfset jvhrsstruct["#ouid#"] = jvhrsstruct["#ouid#"] + jvhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstruct,"#ouid#")>
				<cfset jvhrsstruct["#ouid#"] = jvhrs>
			<cfelse>
				<cfset jvhrsstruct["#ouid#"] = jvhrsstruct["#ouid#"] + jvhrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(jvhrsstructgrp,group_number)>
			<cfset jvhrsstructgrp[group_number] = jvhrs>
		<cfelse>
			<cfset jvhrsstructgrp[group_number] = jvhrsstructgrp[group_number] + jvhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstructgrp,group_number)>
				<cfset jvhrsstructgrp[group_number] = jvhrs>
			<cfelse>
				<cfset jvhrsstructgrp[group_number] = jvhrsstructgrp[group_number] + jvhrs>
			</cfif>
		</cfif>
		
		
		
	
		<cfif not structkeyexists(subhrsstruct,"Global")>
			<cfset subhrsstruct["Global"] = subhrs>
		<cfelse>
			<cfset subhrsstruct["Global"] = subhrsstruct["Global"] + subhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstruct,"Global")>
				<cfset subhrsstruct["Global"] = subhrs>
			<cfelse>
				<cfset subhrsstruct["Global"] = subhrsstruct["Global"] + subhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(subhrsstruct,"#business_line_id#")>
			<cfset subhrsstruct["#business_line_id#"] = subhrs>
		<cfelse>
			<cfset subhrsstruct["#business_line_id#"] = subhrsstruct["#business_line_id#"] + subhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstruct,"#business_line_id#")>
				<cfset subhrsstruct["#business_line_id#"] = subhrs>
			<cfelse>
				<cfset subhrsstruct["#business_line_id#"] = subhrsstruct["#business_line_id#"] + subhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(subhrsstruct,"#ouid#")>
			<cfset subhrsstruct["#ouid#"] = subhrs>
		<cfelse>
			<cfset subhrsstruct["#ouid#"] = subhrsstruct["#ouid#"] + subhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstruct,"#ouid#")>
				<cfset subhrsstruct["#ouid#"] = subhrs>
			<cfelse>
				<cfset subhrsstruct["#ouid#"] = subhrsstruct["#ouid#"] + subhrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(subhrsstructgrp,group_number)>
			<cfset subhrsstructgrp[group_number] = subhrs>
		<cfelse>
			<cfset subhrsstructgrp[group_number] = subhrsstructgrp[group_number] + subhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstructgrp,group_number)>
				<cfset subhrsstructgrp[group_number] = subhrs>
			<cfelse>
				<cfset subhrsstructgrp[group_number] = subhrsstructgrp[group_number] + subhrs>
			</cfif>
		</cfif>
		
		
		
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + jdehrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + jdehrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + jdehrs>
			</cfif>
		</cfif>
				
		
		<cfif not structkeyexists(manhrstructgrp,group_number)>
			<cfset manhrstructgrp[group_number] = jdehrs>
		<cfelse>
			<cfset manhrstructgrp[group_number] = manhrstructgrp[group_number] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(manhrstructgrp,group_number)>
				<cfset manhrstructgrp[group_number] = jdehrs>
			<cfelse>
				<cfset manhrstructgrp[group_number] = manhrstructgrp[group_number] + jdehrs>
			</cfif>
		</cfif>
</cfloop>

<cfloop query="getcontractorhrs">
	<cfif cotype eq "sub">
	
		
		
		<cfif not structkeyexists(subhrsstructloc,GroupLocID)>
			<cfset subhrsstructloc[GroupLocID] = conhrs>
		<cfelse>
			<cfset subhrsstructloc[GroupLocID] = subhrsstructloc[GroupLocID] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstructloc,GroupLocID)>
				<cfset subhrsstructloc[GroupLocID] = conhrs>
			<cfelse>
				<cfset subhrsstructloc[GroupLocID] = subhrsstructloc[GroupLocID] + conhrs>
			</cfif>
		</cfif>
		
		
	
		<cfif not structkeyexists(subhrsstruct,"Global")>
			<cfset subhrsstruct["Global"] = conhrs>
		<cfelse>
			<cfset subhrsstruct["Global"] = subhrsstruct["Global"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstruct,"Global")>
				<cfset subhrsstruct["Global"] = conhrs>
			<cfelse>
				<cfset subhrsstruct["Global"] = subhrsstruct["Global"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(subhrsstruct,"#business_line_id#")>
			<cfset subhrsstruct["#business_line_id#"] = conhrs>
		<cfelse>
			<cfset subhrsstruct["#business_line_id#"] = subhrsstruct["#business_line_id#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstruct,"#business_line_id#")>
				<cfset subhrsstruct["#business_line_id#"] = conhrs>
			<cfelse>
				<cfset subhrsstruct["#business_line_id#"] = subhrsstruct["#business_line_id#"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(subhrsstruct,"#ouid#")>
			<cfset subhrsstruct["#ouid#"] = conhrs>
		<cfelse>
			<cfset subhrsstruct["#ouid#"] = subhrsstruct["#ouid#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstruct,"#ouid#")>
				<cfset subhrsstruct["#ouid#"] = conhrs>
			<cfelse>
				<cfset subhrsstruct["#ouid#"] = subhrsstruct["#ouid#"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(subhrsstructgrp,group_number)>
			<cfset subhrsstructgrp[group_number] = conhrs>
		<cfelse>
			<cfset subhrsstructgrp[group_number] = subhrsstructgrp[group_number] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(subhrsstructgrp,group_number)>
				<cfset subhrsstructgrp[group_number] = conhrs>
			<cfelse>
				<cfset subhrsstructgrp[group_number] = subhrsstructgrp[group_number] + conhrs>
			</cfif>
		</cfif>
		
	<cfelseif cotype eq "jv">
	
	
		<cfif not structkeyexists(jvhrsstructloc,GroupLocID)>
			<cfset jvhrsstructloc[GroupLocID] = conhrs>
		<cfelse>
			<cfset jvhrsstructloc[GroupLocID] = jvhrsstructloc[GroupLocID] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstructloc,GroupLocID)>
				<cfset jvhrsstructloc[GroupLocID] = conhrs>
			<cfelse>
				<cfset jvhrsstructloc[GroupLocID] = jvhrsstructloc[GroupLocID] + conhrs>
			</cfif>
		</cfif>
		
	<cfif not structkeyexists(jvhrsstruct,"Global")>
			<cfset jvhrsstruct["Global"] = conhrs>
		<cfelse>
			<cfset jvhrsstruct["Global"] = jvhrsstruct["Global"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstruct,"Global")>
				<cfset jvhrsstruct["Global"] = conhrs>
			<cfelse>
				<cfset jvhrsstruct["Global"] = jvhrsstruct["Global"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(jvhrsstruct,"#business_line_id#")>
			<cfset jvhrsstruct["#business_line_id#"] = conhrs>
		<cfelse>
			<cfset jvhrsstruct["#business_line_id#"] = jvhrsstruct["#business_line_id#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstruct,"#business_line_id#")>
				<cfset jvhrsstruct["#business_line_id#"] = conhrs>
			<cfelse>
				<cfset jvhrsstruct["#business_line_id#"] = jvhrsstruct["#business_line_id#"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(jvhrsstruct,"#ouid#")>
			<cfset jvhrsstruct["#ouid#"] = conhrs>
		<cfelse>
			<cfset jvhrsstruct["#ouid#"] = jvhrsstruct["#ouid#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstruct,"#ouid#")>
				<cfset jvhrsstruct["#ouid#"] = conhrs>
			<cfelse>
				<cfset jvhrsstruct["#ouid#"] = jvhrsstruct["#ouid#"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(jvhrsstructgrp,group_number)>
			<cfset jvhrsstructgrp[group_number] = conhrs>
		<cfelse>
			<cfset jvhrsstructgrp[group_number] = jvhrsstructgrp[group_number] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(jvhrsstructgrp,group_number)>
				<cfset jvhrsstructgrp[group_number] = conhrs>
			<cfelse>
				<cfset jvhrsstructgrp[group_number] = jvhrsstructgrp[group_number] + conhrs>
			</cfif>
		</cfif>
	<cfelseif cotype eq "mgd">
	
		<cfif not structkeyexists(mdghrsstructloc,GroupLocID)>
			<cfset mdghrsstructloc[GroupLocID] = conhrs>
		<cfelse>
			<cfset mdghrsstructloc[GroupLocID] = mdghrsstructloc[GroupLocID] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstructloc,GroupLocID)>
				<cfset mdghrsstructloc[GroupLocID] = conhrs>
			<cfelse>
				<cfset mdghrsstructloc[GroupLocID] = mdghrsstructloc[GroupLocID] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstruct,"Global")>
			<cfset mdghrsstruct["Global"] = conhrs>
		<cfelse>
			<cfset mdghrsstruct["Global"] = mdghrsstruct["Global"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstruct,"Global")>
				<cfset mdghrsstruct["Global"] = conhrs>
			<cfelse>
				<cfset mdghrsstruct["Global"] = mdghrsstruct["Global"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstruct,"#business_line_id#")>
			<cfset mdghrsstruct["#business_line_id#"] = conhrs>
		<cfelse>
			<cfset mdghrsstruct["#business_line_id#"] = mdghrsstruct["#business_line_id#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstruct,"#business_line_id#")>
				<cfset mdghrsstruct["#business_line_id#"] = conhrs>
			<cfelse>
				<cfset mdghrsstruct["#business_line_id#"] = mdghrsstruct["#business_line_id#"] + conhrs>
			</cfif>
		</cfif>
		
		<cfif not structkeyexists(mdghrsstruct,"#ouid#")>
			<cfset mdghrsstruct["#ouid#"] = conhrs>
		<cfelse>
			<cfset mdghrsstruct["#ouid#"] = mdghrsstruct["#ouid#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstruct,"#ouid#")>
				<cfset mdghrsstruct["#ouid#"] = conhrs>
			<cfelse>
				<cfset mdghrsstruct["#ouid#"] = mdghrsstruct["#ouid#"] + conhrs>
			</cfif>
		</cfif>
		
		
		<cfif not structkeyexists(mdghrsstructgrp,group_number)>
			<cfset mdghrsstructgrp[group_number] = conhrs>
		<cfelse>
			<cfset mdghrsstructgrp[group_number] = mdghrsstructgrp[group_number] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4 and weightedhours eq "yes">
			<cfif not structkeyexists(mdghrsstructgrp,group_number)>
				<cfset mdghrsstructgrp[group_number] = conhrs>
			<cfelse>
				<cfset mdghrsstructgrp[group_number] = mdghrsstructgrp[group_number] + conhrs>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

