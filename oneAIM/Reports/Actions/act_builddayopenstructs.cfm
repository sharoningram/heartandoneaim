<cfset withdrawnstruct = {}>
<cfset withdrawnuserstruct = {}>
<cfloop query="getwithdrawn">
	<cfif not structkeyexists(withdrawnstruct,irn)>
		<cfset withdrawnstruct[irn] = withdrawndate>
	</cfif>
	<cfif not structkeyexists(withdrawnuserstruct,irn)>
		<cfset withdrawnuserstruct[irn] = "#firstname# #lastname#">
	</cfif>
</cfloop>

<cfset investigationstruct = {}>
<cfloop query="needinvestigation">
	<cfif not structkeyexists(investigationstruct,irn)>
		<cfset investigationstruct[irn] = compdate>
	</cfif>
</cfloop>

<cfset sentforreviewstruct = {}>
<cfset sentforreviewuserstruct = {}>
<cfloop query="getsentforreview">
	<cfif not structkeyexists(sentforreviewstruct,irn)>
		<cfset sentforreviewstruct[irn] = compdate>
	</cfif>
	<cfif not structkeyexists(sentforreviewuserstruct,irn)>
		<cfif trim(firstname) eq '' and trim(lastname) eq ''>
			<cfquery name="getreviewer" datasource="#request.dsn#">
				SELECT       Users.Firstname + ' ' + Users.Lastname as revnames
				FROM            Users INNER JOIN
             		        UserRoles ON Users.UserId = UserRoles.UserID
				WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">)
			</cfquery>
			<cfset sentforreviewuserstruct[irn] = valuelist(getreviewer.revnames)>
		<cfelse>
			<cfset sentforreviewuserstruct[irn] =  "#firstname# #lastname#">
		</cfif>
	</cfif>
</cfloop>

<cfset pendclosestruct = {}>
<cfloop query="getpendingclosure">
	<cfif not structkeyexists(pendclosestruct,irn)>
		<cfset pendclosestruct[irn] = compdate>
	</cfif>
</cfloop>

<cfset moreinfostruct = {}>
<cfloop query="getmoreinforeq">
	<cfif not structkeyexists(moreinfostruct,irn)>
		<cfset moreinfostruct[irn] = compdate>
	</cfif>
</cfloop>

<cfset investreviewstruct = {}>
<cfset investreviewuserstruct = {}>
<cfloop query="getinvestreview">
	<cfif not structkeyexists(investreviewstruct,irn)>
		<cfset investreviewstruct[irn] = compdate>
	</cfif>
	<cfif not structkeyexists(investreviewuserstruct,irn)>
		<cfif InvestigationLevel eq 1>
			<cfif trim(revname) eq ''>
				<cfquery name="getreviewer" datasource="#request.dsn#">
				SELECT       Users.Firstname + ' ' + Users.Lastname as revnames
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">)
				</cfquery>
				<cfset investreviewuserstruct[irn] = valuelist(getreviewer.revnames)>
			<cfelse>
				<cfset investreviewuserstruct[irn] = revname>
			</cfif>
		<cfelseif InvestigationLevel eq 2>
			<cfif Status eq "Sent for Review">
				<cfif trim(revname) eq ''>
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT       Users.Firstname + ' ' + Users.Lastname as revnames
						FROM            Users INNER JOIN
               		        UserRoles ON Users.UserId = UserRoles.UserID
						WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">)
					</cfquery>
					<cfset investreviewuserstruct[irn] = valuelist(getreviewer.revnames)>
				<cfelse>
					<cfset investreviewuserstruct[irn] = revname>
				</cfif>
			<cfelse>
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT       Users.Firstname + ' ' + Users.Lastname as revnames
						FROM            Users INNER JOIN
               		        UserRoles ON Users.UserId = UserRoles.UserID
						WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#business_line_id#">)
					</cfquery>
					<cfset investreviewuserstruct[irn] = valuelist(getreviewer.revnames)>
			</cfif>
		</cfif>		
	</cfif>
</cfloop>

<cfset investmistruct = {}>
<cfset investmiuserstruct = {}>
<cfloop query="getinvestmoreinfo">
	<cfif not structkeyexists(investmistruct,irn)>
		<cfset investmistruct[irn] = compdate>
	</cfif>
	<cfif not structkeyexists(investmiuserstruct,irn)>
		<cfif status eq "More Info Requested">
			<cfset investmiuserstruct[irn] = createdby>
		<cfelse>
			<cfif trim(revname) eq ''>
				<cfquery name="getreviewer" datasource="#request.dsn#">
					SELECT       Users.Firstname + ' ' + Users.Lastname as revnames
					FROM            Users INNER JOIN
              		        UserRoles ON Users.UserId = UserRoles.UserID
					WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">)
				</cfquery>
				<cfset investmiuserstruct[irn] = valuelist(getreviewer.revnames)>
			<cfelse>
				<cfset investmiuserstruct[irn] = revname>
			</cfif>
		</cfif>
	</cfif>
</cfloop>