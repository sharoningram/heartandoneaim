<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportdaysopen.cfm", "exports"))>
<cfset thefilename = "DaysOpen_#timeformat(now(),'hhnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Incident Days Open","true")>

<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"Incident Number",1,1);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,2);
	SpreadsheetSetCellValue(theSheet,"Business Stream",1,3);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,4);
	SpreadsheetSetCellValue(theSheet,"Site/Office Name",1,5);
	SpreadsheetSetCellValue(theSheet,"Incident Type",1,6);
	SpreadsheetSetCellValue(theSheet,"Near Miss?",1,7);
	SpreadsheetSetCellValue(theSheet,"OSHA Classification",1,8);
	SpreadsheetSetCellValue(theSheet,"Potential Rating",1,9);
	SpreadsheetSetCellValue(theSheet,"Incident Date",1,10);
	SpreadsheetSetCellValue(theSheet,"Work Related?",1,11);
	SpreadsheetSetCellValue(theSheet,"Incident Assigned To",1,12);
	SpreadsheetSetCellValue(theSheet,"Client",1,13);
	SpreadsheetSetCellValue(theSheet,"Short Description",1,14);
	SpreadsheetSetCellValue(theSheet,"Days Since Opened",1,15);
	SpreadsheetSetCellValue(theSheet,"Record Status",1,16);
	SpreadsheetSetCellValue(theSheet,"Days Awaiting Action",1,17);
	SpreadsheetSetCellValue(theSheet,"Pending Action By",1,18);
</cfscript>
<cfset rctr = 1>
	<cfoutput query="getdaysopen">
	<cfif status neq "Review Completed">
		<cfset rctr = rctr+1>
		<cfset dspdayaop = datediff('d',dateCreated,now())>
		<cfswitch expression="#status#">
			<cfcase value="Initiated"><cfset dspdayaop = datediff('d',dateCreated,now())></cfcase>
			<cfcase value="withdrawn"><cfif structkeyexists(withdrawnstruct,irn)><cfset dspdayaop = datediff('d',withdrawnstruct[irn],now())></cfif></cfcase>
			<cfcase value="Awaiting Investigation"><cfif structkeyexists(investigationstruct,irn)><cfset dspdayaop = datediff('d',investigationstruct[irn],now())></cfif></cfcase>
			<cfcase value="Sent for Review"><cfif structkeyexists(sentforreviewstruct,irn)><cfset dspdayaop = datediff('d',sentforreviewstruct[irn],now())></cfif></cfcase>
			<cfcase value="Pending Closure"><cfif structkeyexists(pendclosestruct,irn)><cfset dspdayaop = datediff('d',pendclosestruct[irn],now())></cfif></cfcase>
			<cfcase value="More Info Requested"><cfif structkeyexists(moreinfostruct,irn)><cfset dspdayaop = datediff('d',moreinfostruct[irn],now())></cfif></cfcase>
			<cfcase value="Investigation Review"><cfif structkeyexists(investreviewstruct,irn)><cfset dspdayaop = datediff('d',investreviewstruct[irn],now())></cfif></cfcase>
			<cfcase value="Investigation Needs More Info"><cfif structkeyexists(investmistruct,irn)><cfset dspdayaop = datediff('d',investmistruct[irn],now())></cfif></cfcase>
			
		</cfswitch>
		<cfset dsppendby = "">
		<cfswitch expression="#status#">
			<cfcase value="Initiated"><cfset dsppendby = createdBy></cfcase>
			<cfcase value="withdrawn"><cfif structkeyexists(withdrawnuserstruct,irn)><cfset dsppendby = withdrawnuserstruct[irn]></cfif></cfcase>
			<cfcase value="Awaiting Investigation"><cfset dsppendby = createdBy></cfcase>
			<cfcase value="Sent for Review"><cfif structkeyexists(sentforreviewuserstruct,irn)><cfset dsppendby = sentforreviewuserstruct[irn]></cfif></cfcase>
			<cfcase value="Pending Closure"><cfset dsppendby = createdBy></cfcase>
			<cfcase value="More Info Requested"><cfset dsppendby = createdBy></cfcase>
			<cfcase value="Investigation Review"><cfif structkeyexists(investreviewuserstruct,irn)><cfset dsppendby = investreviewuserstruct[irn]></cfif></cfcase>
			<cfcase value="Investigation Needs More Info"><cfif structkeyexists(investmiuserstruct,irn)><cfset dsppendby = investmiuserstruct[irn]></cfif></cfcase>
			
		</cfswitch>
		
		
		<cfif isfatality eq "yes"><cfset dspinctype = "Fatality"><cfelse><cfset dspinctype = IncType></cfif>
		<cfscript> 
			 SpreadsheetSetCellValue(theSheet,"#trackingnum#",#rctr#,1);
			 SpreadsheetSetCellValue(theSheet,"#ouname#",#rctr#,2);
			 SpreadsheetSetCellValue(theSheet,"#bsname#",#rctr#,3);
			 SpreadsheetSetCellValue(theSheet,"#group_name#",#rctr#,4);
			 SpreadsheetSetCellValue(theSheet,"#sitename#",#rctr#,5);
			 SpreadsheetSetCellValue(theSheet,"#dspinctype#",#rctr#,6);
			 SpreadsheetSetCellValue(theSheet,"#isnearmiss#",#rctr#,7);
			 SpreadsheetSetCellValue(theSheet,"#category#",#rctr#,8);
			 SpreadsheetSetCellValue(theSheet,"#PotentialRating#",#rctr#,9);
			 SpreadsheetSetCellValue(theSheet,"#dateformat(incidentDate,sysdateformatxcel)#",#rctr#,10);
			 SpreadsheetSetCellValue(theSheet,"#isworkrelated#",#rctr#,11);
			 SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#rctr#,12);
			 SpreadsheetSetCellValue(theSheet,"#clientname#",#rctr#,13);
			 SpreadsheetSetCellValue(theSheet,"#shortdesc#",#rctr#,14);
			 SpreadsheetSetCellValue(theSheet,"#datediff('d',dateCreated,now())#",#rctr#,15);
			 SpreadsheetSetCellValue(theSheet,"#status#",#rctr#,16);
			 SpreadsheetSetCellValue(theSheet,"#dspdayaop#",#rctr#,17);
			 SpreadsheetSetCellValue(theSheet,"#dsppendby#",#rctr#,18);
		</cfscript>
	</cfif>
	</cfoutput>

	
 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">