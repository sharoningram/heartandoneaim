<cfparam name="incyear" default="">
<cfparam name="invlevel" default="">
<cfparam name="bu" default="All">
<cfparam name="ou" default="All">
<cfparam name="businessstream" default="All">
<cfparam name="projectoffice" default="">
<cfparam name="site" default="">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="All">
<cfparam name="locdetail" default="">
<cfparam name="frmcountryid" default="">
<cfparam name="frmincassignto" default="">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="All">
<cfparam name="secondtype" default="All">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="All">
<cfparam name="oidef" default="All">
<cfparam name="seriousinj" default="All">
<cfparam name="secinccats" default="All">
<cfparam name="eic" default="All">
<cfparam name="damagesrc" default="All">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="INCNM" default="no">
<cfparam name="searchname" default="">
<cfparam name="irnumber" default="">
<cfparam name="incandor" default="And">
<cfparam name="searchtype" default="basic">
<cfparam name="potentialrating" default="All">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="All">
<cfparam name="gsr" default="All">
<cfparam name="seb" default="All">
<cfparam name="frminjtype" default="All">
<cfparam name="frmbodypart" default="All">
<cfparam name="frminjcause" default="All">
<cfparam name="droppedobj" default="All"> 
  
<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.dsn#">
		select UserIncSearchID
		from UserIncidentSearches
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
	</cfquery>
	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.dsn#">
			delete from UserIncidentSearches
			where UserIncSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.UserIncSearchID#">
		</cfquery>
	</cfif>
	<!--- <cfif getnamedsearch.recordcount gt 0>
		<cfquery name="getmax" datasource="#request.dsn#">
			select max(savectr) as savectr
			from UserIncidentSearches
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
		</cfquery>
		<cfset newsave = getmax.savectr+1>
	<cfelse>
		<cfset newsave = 1>
	</cfif> --->
	
	
		<cfquery name="addsearch" datasource="#request.dsn#">
			insert into UserIncidentSearches (SearchName, UserID, incyear, invlevel, bu, ou, bs, project, siteoffice, mgdcontractor, jv, subcon, clientid, locdetail, countryid, 
                         incassigned, wronly, primarytype, secondtype, hipoony, oshaclass, oidef, seriousinj, secinccats, eic, damagesrc, startdate, enddate, SaveCtr,incnm,irnumber,incandor,searchtype,potentialrating,incsearchtype,keywords,incstatus,gsr,seb,frminjtype,frmbodypart,frminjcause,droppedobj)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incyear#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmmgdcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmjv#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmsubcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclient#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#locdetail#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmcountryid#">, 
                         <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmincassignto#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wronly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#primarytype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secondtype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#hipoonly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oshaclass#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oidef#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secinccats#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#eic#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#damagesrc#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#newsave#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incnm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#irnumber#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incandor#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchtype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">,'incident',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#keywords#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incstatus#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#gsr#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seb#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frminjtype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmbodypart#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frminjcause#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#droppedobj#">)
		</cfquery>
	
</cfif>
</cfif>