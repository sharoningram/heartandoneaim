<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportconfigdash.cfm", "exports"))>
<cfset thefilename = "ConfigurableDashboard_#incyear#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfparam name="openbu" default="0">
<cfparam name="openou" default="0">
<cfset theSheet = spreadsheetnew("Dashboard","true")>
<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"Configurable Dashboard",1,1);
	 SpreadsheetMergeCells(theSheet,2,2,1,11); 
	 if (dosearch IS "") {
	
		 SpreadsheetSetCellValue(theSheet,"#dspoulist# From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents",2,1);
	}
	else {
		 SpreadsheetSetCellValue(theSheet,"#dspoulist# For #incyear#; All Incidents",2,1);
	}
	SpreadsheetSetCellValue(theSheet,"BU",3,1);
	SpreadsheetSetCellValue(theSheet,"Incident Assigned To",3,2);
	SpreadsheetSetCellValue(theSheet,"Hours",3,3);
	SpreadsheetSetCellValue(theSheet,"Fatality",3,4);
	SpreadsheetSetCellValue(theSheet,"LTI",3,5);
	SpreadsheetSetCellValue(theSheet,"RWC",3,6);
	SpreadsheetSetCellValue(theSheet,"MTC",3,7);
	SpreadsheetSetCellValue(theSheet,"LTIR",3,8);
	SpreadsheetSetCellValue(theSheet,"TRIR",3,9);
	SpreadsheetSetCellValue(theSheet,"First Aid",3,10);
	SpreadsheetSetCellValue(theSheet,"AIR",3,11);
	SpreadsheetSetCellValue(theSheet,"Occ. Illness Discrete",3,12);
</cfscript>


	<cfset amfwctr = 0>
	<cfset subctr = 0>
	<cfset jvctr = 0>
	<cfset mgdctr = 0>
	<cfset globaltot = 0>
	<cfset buctr = 0>
	
	<cfset ltitotctrafw = 0>
	<cfset fattotctrafw = 0>
	<cfset rwctotctrafw = 0>
	<cfset mtctotctrafw = 0>
	<cfset fatotctrafw = 0>
	<cfset oitotctrafw = 0>
	
	<cfset ltitotctrsub = 0>
	<cfset fattotctrsub = 0>
	<cfset rwctotctrsub = 0>
	<cfset mtctotctrsub = 0>
	<cfset fatotctrsub = 0>
	<cfset oitotctrsub = 0>
	
	<cfset ltitotctrjv = 0>
	<cfset fattotctrjv = 0>
	<cfset rwctotctrjv = 0>
	<cfset mtctotctrjv = 0>
	<cfset fatotctrjv = 0>
	<cfset oitotctrjv = 0>
	
	<cfset ltitotctrmgd = 0>
	<cfset fattotctrmgd = 0>
	<cfset rwctotctrmgd = 0>
	<cfset mtctotctrmgd = 0>
	<cfset fatotctrmgd = 0>
	<cfset oitotctrmgd = 0>
	
	<cfset ltitotctrglobal = 0>
	<cfset fattotctrglobal= 0>
	<cfset rwctotctrglobal = 0>
	<cfset mtctotctrglobal = 0>
	<cfset fatotctrglobal = 0>
	<cfset oitotctrglobal = 0>
	
	
	<cfset exctr = 3>
	
	<cfloop query="getBUs">
	<cfset buhrsdsp = 0>
		<cfset bufattot = 0>
		<cfset bultitot = 0>
		<cfset burwctot = 0>
		<cfset bumtctot = 0>
		<cfset bufatot = 0>
		<cfset buoitot = 0>
		
	<cfset buctr = buctr+1>
		<cfset ctr = 0>
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset exctr = exctr+1>
		<cfset ctr = ctr+1>
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#getBUs.Name#",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#exctr#,2);
		</cfscript>
		
		
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getBUs.ID)><cfset amfwctr = amfwctr+manhrstruct[getBUs.id]><cfset buhrsdsp = buhrsdsp+manhrstruct[getBUs.id]><cfset rowhrs = rowhrs+manhrstruct[getBUs.id]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getBUs.ID)><cfset subctr = subctr+subhrsstruct[getBUs.id]><cfset buhrsdsp = buhrsdsp+subhrsstruct[getBUs.id]><cfset rowhrs = rowhrs+subhrsstruct[getBUs.id]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getBUs.ID)><cfset buhrsdsp = buhrsdsp+mdgconstruct[getBUs.id]><cfset mgdctr = mgdctr+mdgconstruct[getBUs.id]><cfset rowhrs = rowhrs+mdgconstruct[getBUs.id]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getBUs.ID)><cfset buhrsdsp = buhrsdsp+jvhrsstruct[getBUs.id]><cfset jvctr = jvctr+jvhrsstruct[getBUs.id]><cfset rowhrs = rowhrs+jvhrsstruct[getBUs.id]></cfif></cfcase>
		
		</cfswitch>
		
		
		
		
		<cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset bufattot = bufattot + FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowfatcount = FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfelse><cfset rowfatcount = 0></cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrmgd = fattotctrmgd + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		<cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset bultitot = bultitot + LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowlticount = LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowlti = rowlti+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse><cfset rowlticount = 0></cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		<cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset burwctot = burwctot + RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowrwccount = RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+RWCstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse><cfset rowrwccount = 0></cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrmgd = rwctotctrmgd + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		<cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset bumtctot = bumtctot + MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowmtccount = MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+MTCstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse><cfset rowmtccount = 0></cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrmgd = mtctotctrmgd + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		<cfif rowhrs gt 0 and rowlti gt 0><cfset rowltirate = numberformat((rowlti*200000)/rowhrs,"0.000")><cfelse><cfset rowltirate = 0></cfif>
		<cfif rowhrs gt 0 and rowtrir gt 0><cfset rowtrirrate = numberformat((rowtrir*200000)/rowhrs,"0.00")><cfelse><cfset rowtrirrate = 0></cfif>
		<cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset bufatot = bufatot + FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowfaidcount = FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowair = rowair+FAstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse><cfset rowfaidcount = 0></cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		<cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0><cfset rowairrate = numberformat((rowair*200000)/rowhrs,"0.00")><cfelse><cfset rowairrate = 0></cfif>
		<cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset buoitot = buoitot + OIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowoicount = OIstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse><cfset rowoicount = 0></cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrafw = oitotctrafw + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrsub = oitotctrsub + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrmgd = oitotctrmgd + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(OIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset oitotctrjv = oitotctrjv + OIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowhrs)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfatcount)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowlticount)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowrwccount)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowmtccount)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#rowltirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#rowtrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfaidcount)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#rowairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowoicount)#",#exctr#,12);
	</cfscript>
		</cfif>
		
		
		
		
		
		</cfloop>
		<cfif buhrsdsp gt 0 and bultitot gt 0><cfset bultirate = numberformat((bultitot*200000)/buhrsdsp,"0.000")><cfelse><cfset bultirate = 0></cfif>
		<cfset butrir = bufattot+bultitot+burwctot+bumtctot><cfif buhrsdsp gt 0 and butrir gt 0><cfset butrirrate = numberformat((butrir*200000)/buhrsdsp,"0.00")><cfelse><cfset butrirrate = 0></cfif>
		<cfset buair = bufatot+butrir><cfif buhrsdsp gt 0 and buair gt 0><cfset buairrate = numberformat((buair*200000)/buhrsdsp,"0.00")><cfelse><cfset buairrate = 0></cfif>
		<cfset exctr = exctr + 1>
			<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getBUs.Name#",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"Total",#exctr#,2);
		SpreadsheetSetCellValue(theSheet,"#numberformat(buhrsdsp)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(bufattot)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(bultitot)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(burwctot)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(bumtctot)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#bultirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#butrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(bufatot)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#buairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(buoitot)#",#exctr#,12);
	</cfscript>
		
		
		
		
		<cfloop query="getou">
		
		
		<cfset ouhrsdsp = 0>
		<cfset oufattot = 0>
		<cfset oultitot = 0>
		<cfset ourwctot = 0>
		<cfset oumtctot = 0>
		<cfset oufatot = 0>
		<cfset ouoitot = 0>
		
		<cfif getou.qrybuid eq getbus.id and listfind(openbu,getou.qrybuid) gt 0>
		<cfset ctr = 0>
		
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset exctr = exctr+1>
		<cfset ctr = ctr+1>
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#getou.Name#",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#exctr#,2);
		</cfscript>
		
		
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getou.ID)><cfset ouhrsdsp = ouhrsdsp+manhrstruct[getou.id]><cfset rowhrs = rowhrs+manhrstruct[getou.ID]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getou.ID)><cfset ouhrsdsp = ouhrsdsp+subhrsstruct[getou.id]><cfset rowhrs = rowhrs+subhrsstruct[getou.ID]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getou.ID)><cfset ouhrsdsp = ouhrsdsp+mdgconstruct[getou.id]><cfset rowhrs = rowhrs+mdgconstruct[getou.ID]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getou.ID)><cfset ouhrsdsp = ouhrsdsp+jvhrsstruct[getou.id]><cfset rowhrs = rowhrs+jvhrsstruct[getou.ID]></cfif></cfcase>
		
		</cfswitch>
		
		
		
		
		<cfif structkeyexists(FATstruct,"#getou.ID#_#IncAssignedID#")><cfset oufattot = oufattot + FATstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+FATstruct["#getou.ID#_#IncAssignedID#"]><cfset rowfatcount = FATstruct["#getou.ID#_#IncAssignedID#"]><cfelse><cfset rowfatcount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getou.ID#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getou.ID#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getou.ID#_#IncAssignedID#")><cfset fattotctrmdg = fattotctrmdg + FATstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getou.ID#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		
		<cfif structkeyexists(LTIstruct,"#getou.ID#_#IncAssignedID#")><cfset oultitot = oultitot + LTIstruct["#getou.ID#_#IncAssignedID#"]><cfset rowlticount = LTIstruct["#getou.ID#_#IncAssignedID#"]><cfset rowlti = rowlti+LTIstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstruct["#getou.ID#_#IncAssignedID#"]><Cfelse><cfset rowlticount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getou.ID#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getou.ID#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getou.ID#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getou.ID#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		<cfif structkeyexists(RWCstruct,"#getou.ID#_#IncAssignedID#")><cfset rowrwccount = RWCstruct["#getou.ID#_#IncAssignedID#"]><cfset ourwctot = ourwctot + RWCstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+RWCstruct["#getou.ID#_#IncAssignedID#"]><Cfelse><cfset rowrwccount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getou.ID#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getou.ID#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getou.ID#_#IncAssignedID#")><cfset rwctotctrmdg = rwctotctrmdg + RWCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getou.ID#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		
		<cfif structkeyexists(MTCstruct,"#getou.ID#_#IncAssignedID#")><cfset oumtctot = oumtctot + MTCstruct["#getou.ID#_#IncAssignedID#"]><cfset rowmtccount = MTCstruct["#getou.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+MTCstruct["#getou.ID#_#IncAssignedID#"]><Cfelse><cfset rowmtccount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getou.ID#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getou.ID#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getou.ID#_#IncAssignedID#")><cfset mtctotctrmdg = mtctotctrmdg + MTCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getou.ID#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		<cfif rowhrs gt 0 and rowlti gt 0><cfset rowltirate = numberformat((rowlti*200000)/rowhrs,"0.000")><cfelse><cfset rowltirate = 0></cfif>
		<cfif rowhrs gt 0 and rowtrir gt 0><cfset rowtrirrate = numberformat((rowtrir*200000)/rowhrs,"0.00")><cfelse><cfset rowtrirrate = 0></cfif>
		<cfif structkeyexists(FAstruct,"#getou.ID#_#IncAssignedID#")><cfset rowfaidcount = FAstruct["#getou.ID#_#IncAssignedID#"]><cfset oufatot = oufatot + FAstruct["#getou.ID#_#IncAssignedID#"]><cfset rowair = rowair+FAstruct["#getou.ID#_#IncAssignedID#"]><Cfelse><cfset rowfaidcount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getou.ID#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getou.ID#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getou.ID#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getou.ID#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getou.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		<cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0><cfset rowairrate = numberformat((rowair*200000)/rowhrs,"0.00")><cfelse><cfset rowairrate = 0></cfif>
		<cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")><cfset rowoicount = OIstruct["#getou.ID#_#IncAssignedID#"]><cfset ouoitot = ouoitot + OIstruct["#getou.ID#_#IncAssignedID#"]><Cfelse><cfset rowoicount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(OIstruct,"#getou.ID#_#IncAssignedID#")></cfif></cfcase>
		
		</cfswitch> --->
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowhrs)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfatcount)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowlticount)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowrwccount)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowmtccount)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#rowltirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#rowtrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfaidcount)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#rowairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowoicount)#",#exctr#,12);
	</cfscript>
		</cfif>
			
		
		</cfloop>
		
		
		<cfif ouhrsdsp gt 0 and oultitot gt 0><cfset oultirate = numberformat((oultitot*200000)/ouhrsdsp,"0.000")><cfelse><cfset oultirate = 0></cfif>
		<cfset outrir = oufattot+oultitot+ourwctot+oumtctot><cfif ouhrsdsp gt 0 and outrir gt 0><cfset outrirrate = numberformat((outrir*200000)/ouhrsdsp,"0.00")><cfelse><cfset outrirrate = 0></cfif>
		<cfset ouair = oufatot+outrir><cfif ouhrsdsp gt 0 and ouair gt 0><cfset ouairrate = numberformat((ouair*200000)/ouhrsdsp,"0.00")><cfelse><cfset ouairrate = 0></cfif>
		<cfset exctr = exctr + 1>
			<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getou.Name#",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"Total",#exctr#,2);
		SpreadsheetSetCellValue(theSheet,"#numberformat(ouhrsdsp)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(oufattot)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(oultitot)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(ourwctot)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(oumtctot)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#oultirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#outrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(oufatot)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#ouairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(ouoitot)#",#exctr#,12);
	</cfscript>
		
		
		
		<cfloop query="getgroups">
		
		<cfset grphrsdsp = 0>
		<cfset grpfattot = 0>
		<cfset grpltitot = 0>
		<cfset grprwctot = 0>
		<cfset grpmtctot = 0>
		<cfset grpfatot = 0>
		<cfset grpoitot = 0>
		
		<cfif getgroups.ouid eq getou.id and listfind(openou,getou.id) gt 0>
		<cfset ctr = 0>
		
		
			<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
		<cfset exctr = exctr+1>
		<cfset ctr = ctr+1>
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#getgroups.group_Name#",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#exctr#,2);
		</cfscript>
		
		
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstructgrp,getgroups.group_number)><cfset grphrsdsp = grphrsdsp+manhrstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+manhrstructgrp[getgroups.group_number]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstructgrp,getgroups.group_number)><cfset grphrsdsp = grphrsdsp+subhrsstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+subhrsstructgrp[getgroups.group_number]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstructgrp,getgroups.group_number)><cfset grphrsdsp = grphrsdsp+mdgconstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+mdgconstructgrp[getgroups.group_number]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstructgrp,getgroups.group_number)><cfset grphrsdsp = grphrsdsp+jvhrsstructgrp[getgroups.group_number]><cfset rowhrs = rowhrs+jvhrsstructgrp[getgroups.group_number]></cfif></cfcase>
		
		</cfswitch>
		
		
		
		
		<cfif structkeyexists(FATstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset grpfattot = grpfattot + FATstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+FATstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowfatcount = FATstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfelse><cfset rowfatcount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrmdg = fattotctrmdg + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		
		<cfif structkeyexists(LTIstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset grpltitot = grpltitot + LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowlticount = LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowlti = rowlti+LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse><cfset rowlticount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		<cfif structkeyexists(RWCstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset grprwctot = grprwctot + RWCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowrwccount = RWCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+RWCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse><cfset rowrwccount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrmdg = rwctotctrmdg + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		
		<cfif structkeyexists(MTCstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset grpmtctot = grpmtctot + MTCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowmtccount = MTCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+MTCstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse><cfset rowmtccount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrmdg = mtctotctrmdg + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		<cfif rowhrs gt 0 and rowlti gt 0><cfset rowltirate = numberformat((rowlti*200000)/rowhrs,"0.000")><cfelse><cfset rowltirate = 0></cfif>
		<cfif rowhrs gt 0 and rowtrir gt 0><cfset rowtrirrate = numberformat((rowtrir*200000)/rowhrs,"0.00")><cfelse><cfset rowtrirrate = 0></cfif>
		<cfif structkeyexists(FAstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset grpfatot = grpfatot + FAstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowfaidcount = FAstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowair = rowair+FAstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse><cfset rowfaidcount = 0></cfif>
		<!--- <cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getgroups.group_number#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getgroups.group_number#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch> --->
		<cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0><cfset rowairrate = numberformat((rowair*200000)/rowhrs,"0.00")><cfelse><cfset rowairrate = 0></cfif>
		<cfif structkeyexists(OIstructgrp,"#getgroups.group_number#_#IncAssignedID#")><cfset grpoitot = grpoitot + OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><cfset rowoicount = OIstructgrp["#getgroups.group_number#_#IncAssignedID#"]><Cfelse><cfset rowoicount = 0></cfif>
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowhrs)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfatcount)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowlticount)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowrwccount)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowmtccount)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#rowltirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#rowtrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfaidcount)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#rowairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowoicount)#",#exctr#,12);
	</cfscript>
		</cfif>
			
		
		</cfloop>
		
		
		<cfif grphrsdsp gt 0 and grpltitot gt 0><cfset grpltirate = numberformat((grpltitot*200000)/grphrsdsp,"0.000")><cfelse><cfset grpltirate = 0></cfif>
		<cfset grptrir = grpfattot+grpltitot+grprwctot+grpmtctot><cfif grphrsdsp gt 0 and grptrir gt 0><cfset grptrirrate = numberformat((grptrir*200000)/grphrsdsp,"0.00")><cfelse><cfset grptrirrate = 0></cfif>
		<cfset grpair = grpfatot+grptrir><cfif grphrsdsp gt 0 and grpair gt 0><cfset grpairrate = numberformat((grpair*200000)/grphrsdsp,"0.00")><cfelse><cfset grpairrate = 0></cfif>
		<cfset exctr = exctr + 1>
			<cfscript> 
			SpreadsheetSetCellValue(theSheet,"#getgroups.group_Name#",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"Total",#exctr#,2);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grphrsdsp)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grpfattot)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grpltitot)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grprwctot)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grpmtctot)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#grpltirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#grptrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grpfatot)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#grpairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(grpoitot)#",#exctr#,12);
	</cfscript>
		
		
		</cfif>
		</cfloop>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		</cfif>
		</cfloop>
		
		
		
		
		
		
		
		
		
		
		
	<!--- 	end bu --->
	</cfloop>
	<cfset ctr = 0>
	<cfset buctr = buctr + 1>
	<cfif not showouonly>
	<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("1,4,3,11",IncAssignedID) gt 0>
	<cfset rowhrsdsp = 0>
	<cfset exctr = exctr+1>
		<cfset ctr = ctr+1>
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler Global",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#exctr#,2);
		</cfscript>
	
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">
			<cfset rowhrsdsp = amfwctr><cfset globaltot = globaltot+amfwctr><cfset rowhrs = rowhrs+amfwctr>
			<cfset rowfatcount = numberformat(fattotctrafw)><cfset fattotctrglobal = fattotctrglobal+fattotctrafw><cfset rowtrir = rowtrir+fattotctrafw>
			<cfset rowlticount = numberformat(ltitotctrafw)><cfset ltitotctrglobal = ltitotctrglobal+ltitotctrafw><cfset rowlti =rowlti+ltitotctrafw><cfset rowtrir = rowtrir+ltitotctrafw>
			<cfset rowrwccount = numberformat(rwctotctrafw)><cfset rwctotctrglobal = rwctotctrglobal+rwctotctrafw><cfset rowtrir = rowtrir+rwctotctrafw>
			<cfset rowmtccount = numberformat(mtctotctrafw)><cfset mtctotctrglobal = mtctotctrglobal+mtctotctrafw><cfset rowtrir = rowtrir+mtctotctrafw>
			<cfset rowfaidcount = numberformat(fatotctrafw)><cfset fatotctrglobal = fatotctrglobal+fatotctrafw><cfset rowair = rowair+fatotctrafw>
			<cfset rowoicount = numberformat(oitotctrafw)><cfset oitotctrglobal = oitotctrglobal+oitotctrafw>
		</cfcase>
		<cfcase value="3">
			<cfset rowhrsdsp = subctr><cfset globaltot = globaltot+subctr><cfset rowhrs = rowhrs+subctr>
			<cfset rowfatcount = numberformat(fattotctrsub)><cfset fattotctrglobal = fattotctrglobal+fattotctrsub><cfset rowtrir = rowtrir+fattotctrsub>
			<cfset rowlticount = numberformat(ltitotctrsub)><cfset ltitotctrglobal = ltitotctrglobal+ltitotctrsub><cfset rowlti =rowlti+ltitotctrsub><cfset rowtrir = rowtrir+ltitotctrsub>
			<cfset rowrwccount = numberformat(rwctotctrsub)><cfset rwctotctrglobal = rwctotctrglobal+rwctotctrsub><cfset rowtrir = rowtrir+rwctotctrsub>
			<cfset rowmtccount = numberformat(mtctotctrsub)><cfset mtctotctrglobal = mtctotctrglobal+mtctotctrsub><cfset rowtrir = rowtrir+mtctotctrsub>
			<cfset rowfaidcount = numberformat(fatotctrsub)><cfset fatotctrglobal = fatotctrglobal+fatotctrsub><cfset rowair = rowair+fatotctrsub>
			<cfset rowoicount = numberformat(oitotctrsub)><cfset oitotctrglobal = oitotctrglobal+fatotctrsub>
		</cfcase>
		<cfcase value="4">
			<cfset rowhrsdsp = mgdctr><cfset globaltot = globaltot+mgdctr><cfset rowhrs = rowhrs+mgdctr>
			<cfset rowfatcount = numberformat(fattotctrmgd)><cfset fattotctrglobal = fattotctrglobal+fattotctrmgd><cfset rowtrir = rowtrir+fattotctrmgd>
			<cfset rowlticount = numberformat(ltitotctrmgd)><cfset ltitotctrglobal = ltitotctrglobal+ltitotctrmgd><cfset rowlti =rowlti+ltitotctrmgd><cfset rowtrir = rowtrir+ltitotctrmgd>
			<cfset rowrwccount = numberformat(rwctotctrmgd)><cfset rwctotctrglobal = rwctotctrglobal+rwctotctrmgd><cfset rowtrir = rowtrir+rwctotctrmgd>
			<cfset rowmtccount = numberformat(mtctotctrmgd)><cfset mtctotctrglobal = mtctotctrglobal+mtctotctrmgd><cfset rowtrir = rowtrir+mtctotctrmgd>
			<cfset rowfaidcount = numberformat(fatotctrmgd)><cfset fatotctrglobal = fatotctrglobal+fatotctrmgd><cfset rowair = rowair+fatotctrmgd>
			<cfset rowoicount = numberformat(oitotctrmgd)><cfset oitotctrglobal = oitotctrglobal+oitotctrmgd>
		</cfcase>
		<cfcase value="11">
			<cfset rowhrsdsp = jvctr><cfset globaltot = globaltot+jvctr><cfset rowhrs = rowhrs+jvctr>
			<cfset rowfatcount = numberformat(fattotctrjv)><cfset fattotctrglobal = fattotctrglobal+fattotctrjv><cfset rowtrir = rowtrir+fattotctrjv>
			<cfset rowlticount = numberformat(ltitotctrjv)><cfset ltitotctrglobal = ltitotctrglobal+ltitotctrjv><cfset rowlti =rowlti+ltitotctrjv><cfset rowtrir = rowtrir+ltitotctrjv>
			<cfset rowrwccount = numberformat(rwctotctrjv)><cfset rwctotctrglobal = rwctotctrglobal+rwctotctrjv><cfset rowtrir = rowtrir+rwctotctrjv>
			<cfset rowmtccount = numberformat(mtctotctrjv)><cfset mtctotctrglobal = mtctotctrglobal+mtctotctrjv><cfset rowtrir = rowtrir+mtctotctrjv>
			<cfset rowfaidcount = numberformat(fatotctrjv)><cfset fatotctrglobal = fatotctrglobal+fatotctrjv><cfset rowair = rowair+fatotctrjv>
			<cfset rowoicount = numberformat(oitotctrjv)><cfset oitotctrglobal = oitotctrglobal+oitotctrjv>
		</cfcase>
		
		</cfswitch>
		
		
		<cfif rowhrs gt 0 and rowlti gt 0><cfset rowltirate = numberformat((rowlti*200000)/rowhrs,"0.000")><cfelse><cfset rowltirate = 0></cfif>
		<cfif rowhrs gt 0 and rowtrir gt 0><cfset rowtrirrate = numberformat((rowtrir*200000)/rowhrs,"0.00")><cfelse><cfset rowtrirrate = 0></cfif>
		
		<cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0><cfset rowairrate = numberformat((rowair*200000)/rowhrs,"0.00")><cfelse><cfset rowairrate = 0></cfif>
	<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowhrsdsp)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfatcount)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowlticount)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowrwccount)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowmtccount)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#rowltirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#rowtrirrate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowfaidcount)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#rowairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rowoicount)#",#exctr#,12);
	</cfscript>
	</cfif>
	</cfloop>
	<cfset exctr = exctr+1>
	 
	<cfif globaltot gt 0 and ltitotctrglobal gt 0><cfset gltirate = numberformat((ltitotctrglobal*200000)/globaltot,"0.000")><cfelse><cfset gltirate = 0></cfif><cfset glbtrir = fattotctrglobal+ltitotctrglobal+rwctotctrglobal+mtctotctrglobal><cfif globaltot gt 0 and glbtrir gt 0><cfset gtrirate = numberformat((glbtrir*200000)/globaltot,"0.00")><cfelse><cfset gtrirate = 0></cfif><cfset glbair = fatotctrglobal+glbtrir><cfif globaltot gt 0 and glbair gt 0><cfset gairrate = numberformat((glbair*200000)/globaltot,"0.00")><cfelse><cfset gairrate = 0></cfif>
<cfscript> 
	SpreadsheetMergeCells(theSheet,#exctr#,#exctr#,1,2); 
	SpreadsheetSetCellValue(theSheet,"Total",#exctr#,1);
		SpreadsheetSetCellValue(theSheet,"#numberformat(globaltot)#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(fattotctrglobal)#",#exctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(ltitotctrglobal)#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(rwctotctrglobal)#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(mtctotctrglobal)#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#gltirate#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#gtrirate#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(fatotctrglobal)#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#gairrate#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#numberformat(oitotctrglobal)#",#exctr#,12);
</cfscript>

</cfif>



 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">