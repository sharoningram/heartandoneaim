<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportnumbersemployedbymonth.cfm", "exports"))>
<cfset thefilename = "NumberEmployed_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Numbers Emplyed by Month","true")>
<cfscript> 
	 SpreadsheetMergeCells(theSheet,1,1,1,2); 
	 SpreadsheetSetCellValue(theSheet,"Numbers Employed Report",1,1);
	
	 SpreadsheetSetCellValue(theSheet," ",2,1);
	 SpreadsheetSetCellValue(theSheet,"AFW Employees",2,2);
	 SpreadsheetSetCellValue(theSheet,"Managed Contractor",2,3);
	 SpreadsheetSetCellValue(theSheet,"Subcontractor",2,4);
	 SpreadsheetSetCellValue(theSheet,"Joint Venture",2,5);
	 SpreadsheetSetCellValue(theSheet,"Total",2,6);
	
</cfscript>
<cfset divmonctr = 0>
	<cfset ctr = 2>
	
	<cfset globalAFW = 0>
	<cfset globalSUB = 0>
	<cfset globalMC = 0>
	<cfset globalJV = 0>
	
	<cfoutput query="getnumemplyed" group="yr">
	<cfset yrctr = 0>
	<cfset yrAFW = 0>
	<cfset yrSUB = 0>
	<cfset yrMC = 0>
	<cfset yrJV = 0>
	
	<cfoutput group="mon">
	<cfset rowtot = 0>
		
	<cfif trim(afw) neq ''>
		<cfset afwnum = afw>
	<cfelse>
		<cfset afwnum = 0>
	</cfif>
	<cfif trim(sub) neq ''>
		<cfset subnum = sub>
	<cfelse>
		<cfset subnum = 0>
	</cfif>
	<cfif trim(jv) neq ''>
		<cfset jvnum = jv>
	<cfelse>
		<cfset jvnum = 0>
	</cfif>
	<cfif trim(mc) neq ''>
		<cfset mcnum = mc>
	<cfelse>
		<cfset mcnum = 0>
	</cfif>
	
	<cfset globalAFW = globalAFW+afwnum>
	<cfset globalSUB = globalSUB+subnum>
	<cfset globalMC = globalMC+mcnum>
	<cfset globalJV = globalJV+jvnum>
	
	<cfset yrAFW = yrAFW+afwnum>
	<cfset yrSUB = yrSUB+subnum>
	<cfset yrMC = yrMC+mcnum>
	<cfset yrJV = yrJV+jvnum>
	
	<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
	<cfset ctr = ctr+1>
	<cfset yrctr = yrctr+1>
	<cfset divmonctr = divmonctr+ 1>
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#monthasstring(mon)#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(afwnum)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mcnum)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subnum)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvnum)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	
	
	
	
	
	</cfoutput>
	<cfif incyear eq "All">
		<cfset ctr = ctr+1>
	<cfset yrtothrs = yrafw+yrmc+yrsub+yrjv>
	<cfif yrafw gt 0 and yrctr gt 0><cfset avafw = numberformat(yrafw/yrctr)><cfelse><cfset avafw = 0></cfif>
	<cfif yrmc gt 0 and yrctr gt 0><cfset avmc = numberformat(yrmc/yrctr)><cfelse><cfset avmc = 0></cfif>
	<cfif yrsub gt 0 and yrctr gt 0><cfset avsub = numberformat(yrsub/yrctr)><cfelse><cfset avsub = 0></cfif>
	<cfif yrjv gt 0 and yrctr gt 0><cfset avjv = numberformat(yrjv/yrctr)><cfelse><cfset avjv = 0></cfif>
	<cfif yrtothrs gt 0 and yrctr gt 0><cfset avtot = numberformat((yrtothrs)/yrctr)><cfelse><cfset avtot = 0></cfif>
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#yr# Average",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#avafw#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#avmc#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#avsub#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#avjv#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#avtot#",#ctr#,6);
	</cfscript>
	
	</cfif>
	</cfoutput>
	<cfset rowtot = 0>
	
	<cfoutput>
	<cfset ctr = ctr + 1>
	
	<cfset tothrs = globalAFW+globalMC+globalSUB+globalJV>
	
	
	
	<cfif globalAFW gt 0 and divmonctr gt 0><cfset gavfw = numberformat(globalAFW/divmonctr)><cfelse><cfset gavfw = 0></cfif>
	<cfif globalMC gt 0 and divmonctr gt 0><cfset gavmc = numberformat(globalMC/divmonctr)><cfelse><cfset gavmc = 0></cfif>
	<cfif globalSUB gt 0 and divmonctr gt 0><cfset gavsub = numberformat(globalSUB/divmonctr)><cfelse><cfset gavsub = 0></cfif>
	<cfif globalJV gt 0 and divmonctr gt 0><cfset gavjv = numberformat(globalJV/divmonctr)><cfelse><cfset gavjv = 0></cfif>
	<cfif tothrs gt 0 and divmonctr gt 0><cfset gavtot = numberformat((tothrs)/divmonctr)><cfelse><cfset gavtot = 0></cfif>
	
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"Average Number Employed",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#gavfw#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#gavmc#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#gavsub#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#gavjv#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#gavtot#",#ctr#,6);
	</cfscript>
	</cfoutput>
<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#"> 
