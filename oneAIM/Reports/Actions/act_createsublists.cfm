<cfparam name="type"  default="">
<cfparam name="BU"  default="">

<cfif trim(type) neq ''>
	<cfswitch expression="#type#">
		<cfcase value="OU">
			<cfif trim(BU) neq '' and isnumeric(BU)>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        ID, Name
					FROM            NewDials
					WHERE        (Parent = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (status <> 99) and isgroupdial = 0
					<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0>
						and id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
					</cfif>
					<cfif  listfindnocase(request.userlevel,"User") gt 0>
					and id in (SELECT DISTINCT Groups.ouid
					FROM            GroupUserAssignment INNER JOIN
					                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
					WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
					</cfif>
					ORDER BY Name
				</cfquery><!--- this.style.border='1px solid 5f2468'; --->
				{zz{<select name="ou" size="1" class="selectgen" id="lrgselect"  onchange="buildbslist(this.value);">
					<option value="">-- Select One --</option>
					<cfoutput query="getOU"><option value="#id#">#name#</option></cfoutput>
					</select>}zz}
					
			</cfif>
		</cfcase>
		<cfcase value="BS">
			<cfif trim(BU) neq '' and isnumeric(BU)>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        ID, Name
					FROM            NewDials
					WHERE        (Parent = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (status <> 99) and isgroupdial = 0
					<cfif  listfindnocase(request.userlevel,"User") gt 0>
					and id in (SELECT DISTINCT Groups.BusinessStreamID
					FROM            GroupUserAssignment INNER JOIN
					                         Groups ON GroupUserAssignment.GroupNumber = Groups.Group_Number
					WHERE        (GroupUserAssignment.UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) AND (GroupUserAssignment.status = 1))
					</cfif>
					ORDER BY Name
				</cfquery>
				<cfquery name="getuser" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
					FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
					WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (UserRoles.UserRole in ( 'user'))
					order by Users.Firstname, Users.Lastname
				</cfquery>
				{zz{<select name="businessstream" size="1" class="selectgen" id="lrgselect" <cfif getOU.recordcount eq 0>disabled</cfif>>
					<option value="">-- Select One --</option>
					<cfoutput query="getOU"><option value="#id#">#name#</option></cfoutput>
					</select>}zz}<!---  onchange="this.style.border='1px solid 5f2468';" --->
					{uu{<select name="assign_user_id" size="4" class="selectgen" id="lrgselect" multiple="Yes" >
										<option value="" selected>-- Select From List --</option>
										<cfoutput query="getuser">
										<cfif userrole eq "user">
										<option value="#userid#">#firstname# #lastname#</option>
										</cfif>
										</cfoutput> 
										</select>}uu}<!--- onchange="this.style.border='1px solid 5f2468';"  --->
					{mm{<select name="mhperson" size="4" class="selectgen" id="lrgselect" multiple="Yes" >
										<option value="" selected>-- Select From List --</option>
										<cfoutput query="getuser">
										
										<option value="#userid#">#firstname# #lastname#</option>
										
										</cfoutput> 
										</select>}mm}
			</cfif>
		</cfcase>
	</cfswitch>
</cfif>