<cfset numempstruct = {}>
<cfset grpnumempstruct = {}>

<cfloop query="getnumemplyed">

	<cfif not structkeyexists(numempstruct,"#buid#_AFW")>
		<cfset numempstruct["#buid#_AFW"] = NumEmployedAFW>
	<cfelse>
		<cfset numempstruct["#buid#_AFW"] = numempstruct["#buid#_AFW"] + NumEmployedAFW>
	</cfif>
	<cfif not structkeyexists(numempstruct,"#buid#_SUB")>
		<cfset numempstruct["#buid#_SUB"] = NumEmployedSub>
	<cfelse>
		<cfset numempstruct["#buid#_SUB"] = numempstruct["#buid#_SUB"] + NumEmployedSub>
	</cfif>
	<cfif not structkeyexists(numempstruct,"#buid#_MC")>
		<cfset numempstruct["#buid#_MC"] = NumEmployedMC>
	<cfelse>
		<cfset numempstruct["#buid#_MC"] = numempstruct["#buid#_MC"] + NumEmployedMC>
	</cfif>
	<cfif not structkeyexists(numempstruct,"#buid#_JV")>
		<cfset numempstruct["#buid#_JV"] = NumEmployedJV>
	<cfelse>
		<cfset numempstruct["#buid#_JV"] = numempstruct["#buid#_JV"] + NumEmployedJV>
	</cfif>
	
	<cfif not structkeyexists(numempstruct,"#ouid#_AFW")>
		<cfset numempstruct["#ouid#_AFW"] = NumEmployedAFW>
	<cfelse>
		<cfset numempstruct["#ouid#_AFW"] = numempstruct["#ouid#_AFW"] + NumEmployedAFW>
	</cfif>
	<cfif not structkeyexists(numempstruct,"#ouid#_SUB")>
		<cfset numempstruct["#ouid#_SUB"] = NumEmployedSub>
	<cfelse>
		<cfset numempstruct["#ouid#_SUB"] = numempstruct["#ouid#_SUB"] + NumEmployedSub>
	</cfif>
	<cfif not structkeyexists(numempstruct,"#ouid#_MC")>
		<cfset numempstruct["#ouid#_MC"] = NumEmployedMC>
	<cfelse>
		<cfset numempstruct["#ouid#_MC"] = numempstruct["#ouid#_MC"] + NumEmployedMC>
	</cfif>
	<cfif not structkeyexists(numempstruct,"#ouid#_JV")>
		<cfset numempstruct["#ouid#_JV"] = NumEmployedJV>
	<cfelse>
		<cfset numempstruct["#ouid#_JV"] = numempstruct["#ouid#_JV"] + NumEmployedJV>
	</cfif>
	
	<cfif not structkeyexists(grpnumempstruct,"#group_number#_AFW")>
		<cfset grpnumempstruct["#group_number#_AFW"] = NumEmployedAFW>
	<cfelse>
		<cfset grpnumempstruct["#group_number#_AFW"] = grpnumempstruct["#group_number#_AFW"] + NumEmployedAFW>
	</cfif>
	<cfif not structkeyexists(grpnumempstruct,"#group_number#_SUB")>
		<cfset grpnumempstruct["#group_number#_SUB"] = NumEmployedSub>
	<cfelse>
		<cfset grpnumempstruct["#group_number#_SUB"] = grpnumempstruct["#group_number#_SUB"] + NumEmployedSub>
	</cfif>
	<cfif not structkeyexists(grpnumempstruct,"#group_number#_MC")>
		<cfset grpnumempstruct["#group_number#_MC"] = NumEmployedMC>
	<cfelse>
		<cfset grpnumempstruct["#group_number#_MC"] = grpnumempstruct["#group_number#_MC"] + NumEmployedMC>
	</cfif>
	<cfif not structkeyexists(grpnumempstruct,"#group_number#_JV")>
		<cfset grpnumempstruct["#group_number#_JV"] = NumEmployedJV>
	<cfelse>
		<cfset grpnumempstruct["#group_number#_JV"] = grpnumempstruct["#group_number#_JV"] + NumEmployedJV>
	</cfif>
</cfloop>

