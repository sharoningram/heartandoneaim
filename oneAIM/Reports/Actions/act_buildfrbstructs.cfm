<cfset targetstructair = {}>
<cfset targetstructlti = {}>
<cfset targetstructtrir = {}>
<cfset targetstructairprev = {}>
<cfset targetstructltiprev = {}>
<cfset targetstructtrirprev = {}>

<cfif trim(startdate) neq ''>
<cfset useincyear = year(startdate)>
<cfelse>
<cfif listfindnocase(incyear,"All") eq 0>
	<cfset useincyear = incyear>
<cfelse>
	<cfset useincyear = year(now())>
</cfif>
</cfif>



<cfloop query="getalltargets">
	<cfif ObjYear eq useincyear>
		<cfif allocatedto eq "Global">
			<cfset targetstructair["Global"] = AIRgoal>
			<cfset targetstructlti["Global"] = LTIRgoal>
			<cfset targetstructtrir["Global"] = TRIRgoal>
		<cfelse>
			<cfset targetstructair[DialID] = AIRgoal>
			<cfset targetstructlti[DialID] = LTIRgoal>
			<cfset targetstructtrir[DialID] = TRIRgoal>
		</cfif>
	<cfelseif objyear eq useincyear-1>
		<cfif allocatedto eq "Global">
			<cfset targetstructairprev["Global"] = AIRgoal>
			<cfset targetstructltiprev["Global"] = LTIRgoal>
			<cfset targetstructtrirprev["Global"] = TRIRgoal>
		<cfelse>
			<cfset targetstructairprev[DialID] = AIRgoal>
			<cfset targetstructltiprev[DialID] = LTIRgoal>
			<cfset targetstructtrirprev[DialID] = TRIRgoal>
		</cfif>
	</cfif>
</cfloop>

<cfset manhrstruct = {}>
<cfset manhrstructprev = {}>
<cfloop query="getjdehrs">
	<cfif hryr eq useincyear>
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + jdehrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + jdehrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = jdehrs>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = jdehrs>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + jdehrs>
			</cfif>
		</cfif>
	<cfelseif hryr eq useincyear-1>
		<cfif not structkeyexists(manhrstructprev,"Global")>
			<cfset manhrstructprev["Global"] = jdehrs>
		<cfelse>
			<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"Global")>
				<cfset manhrstructprev["Global"] = jdehrs>
			<cfelse>
				<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + jdehrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
			<cfset manhrstructprev["#business_line_id#"] = jdehrs>
		<cfelse>
			<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
				<cfset manhrstructprev["#business_line_id#"] = jdehrs>
			<cfelse>
				<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + jdehrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#ouid#")>
			<cfset manhrstructprev["#ouid#"] = jdehrs>
		<cfelse>
			<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + jdehrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#ouid#")>
				<cfset manhrstructprev["#ouid#"] = jdehrs>
			<cfelse>
				<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + jdehrs>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="getjdeoh">
<cfif ohyr eq useincyear>
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = oh>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = oh>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + oh>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = oh>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = oh>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + oh>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = oh>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = oh>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + oh>
			</cfif>
		</cfif>
<cfelseif ohyr eq useincyear-1>
		<cfif not structkeyexists(manhrstructprev,"Global")>
			<cfset manhrstructprev["Global"] = oh>
		<cfelse>
			<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"Global")>
				<cfset manhrstructprev["Global"] = oh>
			<cfelse>
				<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + oh>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
			<cfset manhrstructprev["#business_line_id#"] = oh>
		<cfelse>
			<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
				<cfset manhrstructprev["#business_line_id#"] = oh>
			<cfelse>
				<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + oh>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#ouid#")>
			<cfset manhrstructprev["#ouid#"] = oh>
		<cfelse>
			<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + oh>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#ouid#")>
				<cfset manhrstructprev["#ouid#"] = oh>
			<cfelse>
				<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + oh>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="getnonjdehrs">
<cfif manyr eq useincyear>
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = manhrs>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + manhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = manhrs>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + manhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = manhrs>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + manhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = manhrs>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + manhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = manhrs>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + manhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = manhrs>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + manhrs>
			</cfif>
		</cfif>
<cfelseif manyr eq useincyear-1>
		<cfif not structkeyexists(manhrstructprev,"Global")>
			<cfset manhrstructprev["Global"] = manhrs>
		<cfelse>
			<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + manhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"Global")>
				<cfset manhrstructprev["Global"] = manhrs>
			<cfelse>
				<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + manhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
			<cfset manhrstructprev["#business_line_id#"] = manhrs>
		<cfelse>
			<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + manhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
				<cfset manhrstructprev["#business_line_id#"] = manhrs>
			<cfelse>
				<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + manhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#ouid#")>
			<cfset manhrstructprev["#ouid#"] = manhrs>
		<cfelse>
			<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + manhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#ouid#")>
				<cfset manhrstructprev["#ouid#"] = manhrs>
			<cfelse>
				<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + manhrs>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfloop query="getcontractorhrs">
<cfif conyr eq useincyear>
		<cfif not structkeyexists(manhrstruct,"Global")>
			<cfset manhrstruct["Global"] = conhrs>
		<cfelse>
			<cfset manhrstruct["Global"] = manhrstruct["Global"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"Global")>
				<cfset manhrstruct["Global"] = conhrs>
			<cfelse>
				<cfset manhrstruct["Global"] = manhrstruct["Global"] + conhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
			<cfset manhrstruct["#business_line_id#"] = conhrs>
		<cfelse>
			<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#business_line_id#")>
				<cfset manhrstruct["#business_line_id#"] = conhrs>
			<cfelse>
				<cfset manhrstruct["#business_line_id#"] = manhrstruct["#business_line_id#"] + conhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstruct,"#ouid#")>
			<cfset manhrstruct["#ouid#"] = conhrs>
		<cfelse>
			<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstruct,"#ouid#")>
				<cfset manhrstruct["#ouid#"] = conhrs>
			<cfelse>
				<cfset manhrstruct["#ouid#"] = manhrstruct["#ouid#"] + conhrs>
			</cfif>
		</cfif>
<cfelseif conyr eq useincyear-1>
		<cfif not structkeyexists(manhrstructprev,"Global")>
			<cfset manhrstructprev["Global"] = conhrs>
		<cfelse>
			<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"Global")>
				<cfset manhrstructprev["Global"] = conhrs>
			<cfelse>
				<cfset manhrstructprev["Global"] = manhrstructprev["Global"] + conhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
			<cfset manhrstructprev["#business_line_id#"] = conhrs>
		<cfelse>
			<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#business_line_id#")>
				<cfset manhrstructprev["#business_line_id#"] = conhrs>
			<cfelse>
				<cfset manhrstructprev["#business_line_id#"] = manhrstructprev["#business_line_id#"] + conhrs>
			</cfif>
		</cfif>
		<cfif not structkeyexists(manhrstructprev,"#ouid#")>
			<cfset manhrstructprev["#ouid#"] = conhrs>
		<cfelse>
			<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + conhrs>
		</cfif>
		<cfif LocationDetailID eq 4>
			<cfif not structkeyexists(manhrstructprev,"#ouid#")>
				<cfset manhrstructprev["#ouid#"] = conhrs>
			<cfelse>
				<cfset manhrstructprev["#ouid#"] = manhrstructprev["#ouid#"] + conhrs>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfset TRIRstruct = {}>
<cfset AIRstruct = {}>
<cfset LTIstruct = {}>

<cfset TRIRstructprev = {}>
<cfset AIRstructprev = {}>
<cfset LTIstructprev = {}>

<cfloop query="getincs">
	<cfif incyr eq useincyear>
	<cfswitch expression="#OSHAclass#">
		<cfcase value="1">
			<cfif not structkeyexists(AIRstruct,"Global")>
				<cfset AIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset AIRstruct["Global"] = AIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#business_line_id#")>
				<cfset AIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#business_line_id#"] = AIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#ouid#")>
				<cfset AIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#ouid#"] = AIRstruct["#ouid#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="2">
			<cfif not structkeyexists(LTIstruct,"Global")>
				<cfset LTIstruct["Global"] = inccnt>
			<cfelse>
				<cfset LTIstruct["Global"] = LTIstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(LTIstruct,"#business_line_id#")>
				<cfset LTIstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset LTIstruct["#business_line_id#"] = LTIstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(LTIstruct,"#ouid#")>
				<cfset LTIstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset LTIstruct["#ouid#"] = LTIstruct["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(AIRstruct,"Global")>
				<cfset AIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset AIRstruct["Global"] = AIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#business_line_id#")>
				<cfset AIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#business_line_id#"] = AIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#ouid#")>
				<cfset AIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#ouid#"] = AIRstruct["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(TRIRstruct,"Global")>
				<cfset TRIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["Global"] = TRIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstruct,"#business_line_id#")>
				<cfset TRIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["#business_line_id#"] = TRIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstruct,"#ouid#")>
				<cfset TRIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["#ouid#"] = TRIRstruct["#ouid#"]+inccnt>
			</cfif>
			
		</cfcase>
		<cfcase value="3">
			<cfif not structkeyexists(TRIRstruct,"Global")>
				<cfset TRIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["Global"] = TRIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstruct,"#business_line_id#")>
				<cfset TRIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["#business_line_id#"] = TRIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstruct,"#ouid#")>
				<cfset TRIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["#ouid#"] = TRIRstruct["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(AIRstruct,"Global")>
				<cfset AIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset AIRstruct["Global"] = AIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#business_line_id#")>
				<cfset AIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#business_line_id#"] = AIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#ouid#")>
				<cfset AIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#ouid#"] = AIRstruct["#ouid#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="4">
			<cfif not structkeyexists(TRIRstruct,"Global")>
				<cfset TRIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["Global"] = TRIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstruct,"#business_line_id#")>
				<cfset TRIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["#business_line_id#"] = TRIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstruct,"#ouid#")>
				<cfset TRIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset TRIRstruct["#ouid#"] = TRIRstruct["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(AIRstruct,"Global")>
				<cfset AIRstruct["Global"] = inccnt>
			<cfelse>
				<cfset AIRstruct["Global"] = AIRstruct["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#business_line_id#")>
				<cfset AIRstruct["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#business_line_id#"] = AIRstruct["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstruct,"#ouid#")>
				<cfset AIRstruct["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstruct["#ouid#"] = AIRstruct["#ouid#"]+inccnt>
			</cfif>
		</cfcase>
	</cfswitch>
<cfelseif incyr eq useincyear-1>
	<cfswitch expression="#OSHAclass#">
		<cfcase value="1">
			<cfif not structkeyexists(AIRstructprev,"Global")>
				<cfset AIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["Global"] = AIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#business_line_id#")>
				<cfset AIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#business_line_id#"] = AIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#ouid#")>
				<cfset AIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#ouid#"] = AIRstructprev["#ouid#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="2">
			<cfif not structkeyexists(LTIstructprev,"Global")>
				<cfset LTIstructprev["Global"] = inccnt>
			<cfelse>
				<cfset LTIstructprev["Global"] = LTIstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(LTIstructprev,"#business_line_id#")>
				<cfset LTIstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset LTIstructprev["#business_line_id#"] = LTIstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(LTIstructprev,"#ouid#")>
				<cfset LTIstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset LTIstructprev["#ouid#"] = LTIstructprev["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(AIRstructprev,"Global")>
				<cfset AIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["Global"] = AIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#business_line_id#")>
				<cfset AIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#business_line_id#"] = AIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#ouid#")>
				<cfset AIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#ouid#"] = AIRstructprev["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(TRIRstructprev,"Global")>
				<cfset TRIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["Global"] = TRIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstructprev,"#business_line_id#")>
				<cfset TRIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["#business_line_id#"] = TRIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstructprev,"#ouid#")>
				<cfset TRIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["#ouid#"] = TRIRstructprev["#ouid#"]+inccnt>
			</cfif>
			
		</cfcase>
		<cfcase value="3">
			<cfif not structkeyexists(TRIRstructprev,"Global")>
				<cfset TRIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["Global"] = TRIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstructprev,"#business_line_id#")>
				<cfset TRIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["#business_line_id#"] = TRIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstructprev,"#ouid#")>
				<cfset TRIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["#ouid#"] = TRIRstructprev["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(AIRstructprev,"Global")>
				<cfset AIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["Global"] = AIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#business_line_id#")>
				<cfset AIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#business_line_id#"] = AIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#ouid#")>
				<cfset AIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#ouid#"] = AIRstructprev["#ouid#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="4">
			<cfif not structkeyexists(TRIRstructprev,"Global")>
				<cfset TRIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["Global"] = TRIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstructprev,"#business_line_id#")>
				<cfset TRIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["#business_line_id#"] = TRIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(TRIRstructprev,"#ouid#")>
				<cfset TRIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset TRIRstructprev["#ouid#"] = TRIRstructprev["#ouid#"]+inccnt>
			</cfif>
			
			<cfif not structkeyexists(AIRstructprev,"Global")>
				<cfset AIRstructprev["Global"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["Global"] = AIRstructprev["Global"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#business_line_id#")>
				<cfset AIRstructprev["#business_line_id#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#business_line_id#"] = AIRstructprev["#business_line_id#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(AIRstructprev,"#ouid#")>
				<cfset AIRstructprev["#ouid#"] = inccnt>
			<cfelse>
				<cfset AIRstructprev["#ouid#"] = AIRstructprev["#ouid#"]+inccnt>
			</cfif>
		</cfcase>
	</cfswitch>
</cfif>
</cfloop>
<cfloop query="getfatalities">
<cfif incyr eq useincyear>
	<cfif not structkeyexists(AIRstruct,"Global")>
		<cfset AIRstruct["Global"] = inccnt>
	<cfelse>
		<cfset AIRstruct["Global"] = AIRstruct["Global"]+inccnt>
	</cfif>
	<cfif not structkeyexists(AIRstruct,"#business_line_id#")>
		<cfset AIRstruct["#business_line_id#"] = inccnt>
	<cfelse>
		<cfset AIRstruct["#business_line_id#"] = AIRstruct["#business_line_id#"]+inccnt>
	</cfif>
	<cfif not structkeyexists(AIRstruct,"#ouid#")>
		<cfset AIRstruct["#ouid#"] = inccnt>
	<cfelse>
		<cfset AIRstruct["#ouid#"] = AIRstruct["#ouid#"]+inccnt>
	</cfif>
	
	<cfif not structkeyexists(TRIRstruct,"Global")>
		<cfset TRIRstruct["Global"] = inccnt>
	<cfelse>
		<cfset TRIRstruct["Global"] = TRIRstruct["Global"]+inccnt>
	</cfif>
	<cfif not structkeyexists(TRIRstruct,"#business_line_id#")>
		<cfset TRIRstruct["#business_line_id#"] = inccnt>
	<cfelse>
		<cfset TRIRstruct["#business_line_id#"] = TRIRstruct["#business_line_id#"]+inccnt>
	</cfif>
	<cfif not structkeyexists(TRIRstruct,"#ouid#")>
		<cfset TRIRstruct["#ouid#"] = inccnt>
	<cfelse>
		<cfset TRIRstruct["#ouid#"] = TRIRstruct["#ouid#"]+inccnt>
	</cfif>
<cfelseif incyr eq useincyear-1>
	<cfif not structkeyexists(TRIRstructprev,"Global")>
		<cfset TRIRstructprev["Global"] = inccnt>
	<cfelse>
		<cfset TRIRstructprev["Global"] = TRIRstructprev["Global"]+inccnt>
	</cfif>
	<cfif not structkeyexists(TRIRstructprev,"#business_line_id#")>
		<cfset TRIRstructprev["#business_line_id#"] = inccnt>
	<cfelse>
		<cfset TRIRstructprev["#business_line_id#"] = TRIRstructprev["#business_line_id#"]+inccnt>
	</cfif>
	<cfif not structkeyexists(TRIRstructprev,"#ouid#")>
		<cfset TRIRstructprev["#ouid#"] = inccnt>
	<cfelse>
		<cfset TRIRstructprev["#ouid#"] = TRIRstructprev["#ouid#"]+inccnt>
	</cfif>
	
	<cfif not structkeyexists(AIRstructprev,"Global")>
		<cfset AIRstructprev["Global"] = inccnt>
	<cfelse>
		<cfset AIRstructprev["Global"] = AIRstructprev["Global"]+inccnt>
	</cfif>
	<cfif not structkeyexists(AIRstructprev,"#business_line_id#")>
		<cfset AIRstructprev["#business_line_id#"] = inccnt>
	<cfelse>
		<cfset AIRstructprev["#business_line_id#"] = AIRstructprev["#business_line_id#"]+inccnt>
	</cfif>
	<cfif not structkeyexists(AIRstructprev,"#ouid#")>
		<cfset AIRstructprev["#ouid#"] = inccnt>
	<cfelse>
		<cfset AIRstructprev["#ouid#"] = AIRstructprev["#ouid#"]+inccnt>
	</cfif>
</cfif>
</cfloop>
