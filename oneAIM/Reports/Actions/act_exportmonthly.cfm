<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportmonthly.cfm", "exports"))>
<cfset thefilename = "MonthlyStat_#incyear#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Monthly Statistic Table","true")>
<cfscript> 
	 SpreadsheetMergeCells(theSheet,1,1,1,30); 
	 SpreadsheetSetCellValue(theSheet,"#dspoulist# Monthly Statistic Table/Monthly Cumulative - #incyear#",1,1);
	SpreadsheetSetCellValue(theSheet,"",2,1);
	 SpreadsheetMergeCells(theSheet,2,2,2,4); 
	SpreadsheetSetCellValue(theSheet,"Hours Worked",2,2);
	 SpreadsheetMergeCells(theSheet,2,2,5,7);
	SpreadsheetSetCellValue(theSheet,"Lost Time Injuries",2,5);
	 SpreadsheetMergeCells(theSheet,2,2,8,10);
	SpreadsheetSetCellValue(theSheet,"Total Recordable Cases",2,8);
	 SpreadsheetMergeCells(theSheet,2,2,11,13);
	SpreadsheetSetCellValue(theSheet,"First Aid",2,11);
	 SpreadsheetMergeCells(theSheet,2,2,14,16);
	SpreadsheetSetCellValue(theSheet,"High Potential Incidents",2,14);
	 SpreadsheetMergeCells(theSheet,2,2,17,19);
	 SpreadsheetSetCellValue(theSheet,"AIR",2,17);
	 SpreadsheetMergeCells(theSheet,2,2,20,22);
	 SpreadsheetSetCellValue(theSheet,"TRIR",2,20);
	 SpreadsheetMergeCells(theSheet,2,2,23,25);
	 SpreadsheetSetCellValue(theSheet,"LTIR",2,23);
	 SpreadsheetMergeCells(theSheet,2,2,26,28);
	 SpreadsheetSetCellValue(theSheet,"HiPoR",2,26);
	 
	 	SpreadsheetSetCellValue(theSheet,"",3,1);
	 	SpreadsheetSetCellValue(theSheet,"Month",3,2);
		SpreadsheetSetCellValue(theSheet,"YTD",3,3);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,4);
	 	SpreadsheetSetCellValue(theSheet,"Month",3,5);
		SpreadsheetSetCellValue(theSheet,"YTD",3,6);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,7);
		SpreadsheetSetCellValue(theSheet,"Month",3,8);
		SpreadsheetSetCellValue(theSheet,"YTD",3,9);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,10);
		SpreadsheetSetCellValue(theSheet,"Month",3,11);
		SpreadsheetSetCellValue(theSheet,"YTD",3,12);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,13);
		SpreadsheetSetCellValue(theSheet,"Month",3,14);
		SpreadsheetSetCellValue(theSheet,"YTD",3,15);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,16);
		SpreadsheetSetCellValue(theSheet,"Month",3,17);
		SpreadsheetSetCellValue(theSheet,"YTD",3,18);
		// SpreadsheetSetCellValue(theSheet,"Target",3,19);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,19);
		SpreadsheetSetCellValue(theSheet,"Month",3,20);
		SpreadsheetSetCellValue(theSheet,"YTD",3,21);
		// SpreadsheetSetCellValue(theSheet,"Target",3,23);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,22);
		SpreadsheetSetCellValue(theSheet,"Month",3,23);
		SpreadsheetSetCellValue(theSheet,"YTD",3,24);
		// SpreadsheetSetCellValue(theSheet,"Target",3,27);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,25);
		SpreadsheetSetCellValue(theSheet,"Month",3,26);
		SpreadsheetSetCellValue(theSheet,"YTD",3,27);
		SpreadsheetSetCellValue(theSheet,"Rolling",3,28);
	
		
</cfscript>
<!--- 	SpreadsheetSetCellValue(theSheet,"Rolling",2,32); --->

<cfset yrctr = 0>
	<cfset ltictr = 0>
	<cfset trictr = 0>
	<cfset factr = 0>
	<cfset hpctr = 0>
	<cfset ytdallhrs = 0>
	<cfset FATctr = 0>
	
	<cfset exctr = 3>
	<cfloop from="1" to="12" index="i">
	<cfset exctr = exctr+1>
	
		<cfset monthhrs = 0>
		<!--- <cfif structkeyexists(manhrstructoffshore,"#lookupyear#_#i#")>
			<cfset monthhrs = monthhrs + manhrstructoffshore["#lookupyear#_#i#"]>
			<cfset ytdallhrs = ytdallhrs + manhrstructoffshore["#lookupyear#_#i#"]>
		</cfif> --->
	<cfset rollosctr = 0>
	<cfset rollctr = 0>
	<cfset rollltictr = 0>
	<cfset rolltrictr = 0>
	<cfset rollfactr = 0>
	<cfset rollhpctr = 0>
	<cfset rollFATctr = 0>
	
	<cfif structkeyexists(manhrstruct,"#lookupyear#_#i#")>
		<cfset yrctr = yrctr + manhrstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(LTIstruct,"#lookupyear#_#i#")>
		<cfset ltictr = ltictr + LTIstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(TRIstruct,"#lookupyear#_#i#")>
		<cfset trictr = trictr + TRIstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(Fatalitystruct,"#lookupyear#_#i#")>
		<cfset trictr = trictr + Fatalitystruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(FAstruct,"#lookupyear#_#i#")>
		<cfset factr = factr + FAstruct["#lookupyear#_#i#"]>
	</cfif>
	<cfif structkeyexists(HiPostruct,"#lookupyear#_#i#")>
		<cfset hpctr = hpctr + HiPostruct["#lookupyear#_#i#"]>
	</cfif>
		<cfset rolloslist = "">
		<cfset rollhrlist = "">
		<cfset rollLTIlist = "">
		<cfset rollTRIlist = "">
		<cfset rollFAlist = "">
		<cfset rollHPlist = "">
		<cfset rollFATlist = "">
		<cfloop from="#i+1#" to="12" index="o">
			<cfset rollhrlist = listappend(rollhrlist,"#lookupyear-1#_#o#")>
			<cfset rollLTIlist = listappend(rollLTIlist,"#lookupyear-1#_#o#")>
			<cfset rollTRIlist = listappend(rollTRIlist,"#lookupyear-1#_#o#")>
			<cfset rollFAlist = listappend(rollFAlist,"#lookupyear-1#_#o#")>
			<cfset rollHPlist = listappend(rollHPlist,"#lookupyear-1#_#o#")>
			<cfset rolloslist = listappend(rolloslist,"#lookupyear-1#_#o#")>
			<cfset rollFATlist = listappend(rollFATlist,"#lookupyear-1#_#o#")>
		</cfloop>
		<cfloop from="1" to="#12-listlen(rollhrlist)#" index="u">
			<cfset rollhrlist = listappend(rollhrlist,"#lookupyear#_#u#")>
			<cfset rollLTIlist = listappend(rollLTIlist,"#lookupyear#_#u#")>
			<cfset rollTRIlist = listappend(rollTRIlist,"#lookupyear#_#u#")>
			<cfset rollFAlist = listappend(rollFAlist,"#lookupyear#_#u#")>
			<cfset rollHPlist = listappend(rollHPlist,"#lookupyear#_#u#")>
			<cfset rolloslist = listappend(rolloslist,"#lookupyear#_#u#")>
			<cfset rollFATlist = listappend(rollFATlist,"#lookupyear#_#u#")>
		</cfloop>
		
		<cfloop list="#structkeylist(Fatalitystruct)#" index="j">
			<cfif listfind(rollFATlist,j) gt 0>
				<cfset rolltrictr = rolltrictr+Fatalitystruct[j]>
			</cfif>
		</cfloop>
		<!--- <cfloop list="#structkeylist(manhrstructoffshore)#" index="j">
			<cfif listfind(rolloslist,j) gt 0>
				<cfset rollosctr = rollosctr+manhrstructoffshore[j]>
			</cfif>
		</cfloop> --->
		<cfloop list="#structkeylist(manhrstruct)#" index="j">
			<cfif listfind(rollhrlist,j) gt 0>
				<cfset rollctr = rollctr+manhrstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(LTIstruct)#" index="j">
			<cfif listfind(rollLTIlist,j) gt 0>
				<cfset rollltictr = rollltictr+LTIstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(TRIstruct)#" index="j">
			<cfif listfind(rollTRIlist,j) gt 0>
				<cfset rolltrictr = rolltrictr+TRIstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(FAstruct)#" index="j">
			<cfif listfind(rollFAlist,j) gt 0>
				<cfset rollfactr = rollfactr+FAstruct[j]>
			</cfif>
		</cfloop>
		<cfloop list="#structkeylist(HiPostruct)#" index="j">
			<cfif listfind(rollHPlist,j) gt 0>
				<cfset rollhpctr = rollhpctr+HiPostruct[j]>
			</cfif>
		</cfloop>
		
		<cfset airctr = 0>
		<cfset airytd = 0>
		<cfset airroll = 0>
		<cfset dspytdall = 0>
		<cfset dsprollos = 0>
		<cfset trirctr = 0>
		<cfset trirctrytd = 0>
		<cfset trirctrroll = 0>
		<cfset ltirctr = 0>
		<cfset ltirctrytd = 0>
		<cfset ltirctrroll = 0>
		<cfset hipoctr = 0>
		<cfset fatalityctr = 0>
		
		
		<cfset monthlytri = 0>
		<cfif structkeyexists(TRIstruct,"#lookupyear#_#i#")>
			<cfset monthlytri = monthlytri + TRIstruct["#lookupyear#_#i#"]>
		</cfif>
		<cfif structkeyexists(Fatalitystruct,"#lookupyear#_#i#")>
			<cfset monthlytri = monthlytri + Fatalitystruct["#lookupyear#_#i#"]>
		</cfif>
		
		
		<cfset dsmonhrs = "">
		<cfif lookupyear eq year(now())>
				<cfif i lte month(now())>
					<cfif structkeyexists(manhrstruct,"#lookupyear#_#i#")><cfset dsmonhrs = numberformat(manhrstruct["#lookupyear#_#i#"])><cfset monthhrs = monthhrs+manhrstruct["#lookupyear#_#i#"]>
					<cfelse><cfset dsmonhrs = 0></cfif>
				</cfif>
			<cfelse><cfif structkeyexists(manhrstruct,"#lookupyear#_#i#")><cfset dsmonhrs = numberformat(manhrstruct["#lookupyear#_#i#"])><cfset monthhrs = monthhrs+manhrstruct["#lookupyear#_#i#"]><cfelse><cfset dsmonhrs = 0></cfif></cfif>
		<cfset dsytdhrs = ""> 
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dspytdall = ytdallhrs+yrctr><cfset dsytdhrs = numberformat(yrctr)></cfif><cfelse><cfset dspytdall = ytdallhrs+yrctr><cfset dsytdhrs = numberformat(yrctr)></cfif>
		<Cfset dsrollhrs = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dsprollos = rollosctr+rollctr><Cfset dsrollhrs = numberformat(rollctr)></cfif><cfelse><cfset dsprollos = rollosctr+rollctr><Cfset dsrollhrs = numberformat(rollctr)></cfif>
	<cfset dsltimon = "">

	<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif structkeyexists(LTIstruct,"#lookupyear#_#i#")><cfset dsltimon = numberformat(LTIstruct["#lookupyear#_#i#"])><cfset ltirctr = ltirctr+LTIstruct["#lookupyear#_#i#"]><cfelse><cfset dsltimon = 0></cfif></cfif><cfelse><cfif structkeyexists(LTIstruct,"#lookupyear#_#i#")><cfset dsltimon = numberformat(LTIstruct["#lookupyear#_#i#"])><cfset ltirctr = ltirctr+LTIstruct["#lookupyear#_#i#"]><cfelse><cfset dsltimon = 0></cfif></cfif>
	<cfset dsltiytd = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dsltiytd = numberformat(ltictr)><cfset ltirctrytd = ltirctrytd+ltictr></cfif><cfelse><cfset dsltiytd = numberformat(ltictr)><cfset ltirctrytd = ltirctrytd+ltictr></cfif>
	<cfset dsltiroll = "">
	<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dsltiroll =numberformat(rollltictr)><cfset ltirctrroll = ltirctrroll+rollltictr></cfif><cfelse><cfset dsltiroll =numberformat(rollltictr)><cfset ltirctrroll = ltirctrroll+rollltictr></cfif>

	<cfset dstrimon = "">
	<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dstrimon = monthlytri><cfset trirctr = trirctr+monthlytri><cfset airctr = airctr+monthlytri></cfif><cfelse><cfif structkeyexists(TRIstruct,"#lookupyear#_#i#")><cfset dstrimon = numberformat(TRIstruct["#lookupyear#_#i#"])><cfelse><cfset dstrimon = 0></cfif><cfset trirctr = trirctr+monthlytri><cfset airctr = airctr+monthlytri></cfif>

	<cfset dstriytd = "">
	<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dstriytd = numberformat(trictr)><cfset airytd = airytd+trictr><cfset trirctrytd = trirctrytd+trictr></cfif><cfelse><cfset dstriytd =numberformat(trictr)><cfset airytd = airytd+trictr><cfset trirctrytd = trirctrytd+trictr></cfif>
	<cfset dstriroll = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dstriroll = numberformat(rolltrictr)><cfset airroll = airroll+rolltrictr><cfset trirctrroll = trirctrroll+rolltrictr></cfif><cfelse><cfset dstriroll = numberformat(rolltrictr)><cfset airroll = airroll+rolltrictr><cfset trirctrroll = trirctrroll+rolltrictr></cfif>

<cfset dsfamon = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif structkeyexists(FAstruct,"#lookupyear#_#i#")><cfset dsfamon = numberformat(FAstruct["#lookupyear#_#i#"])><cfset airctr = airctr+FAstruct["#lookupyear#_#i#"]><cfelse><cfset dsfamon = 0></cfif></cfif><cfelse><cfif structkeyexists(FAstruct,"#lookupyear#_#i#")><cfset dsfamon = numberformat(FAstruct["#lookupyear#_#i#"])><cfset airctr = airctr+FAstruct["#lookupyear#_#i#"]><cfelse><cfset dsfamon = 0></cfif></cfif>

<cfset dsfaytd = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dsfaytd = numberformat(factr)><cfset airytd = airytd+factr></cfif><cfelse><cfset dsfaytd = numberformat(factr)><cfset airytd = airytd+factr></cfif>

<cfset dsfaroll = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dsfaroll = numberformat(rollfactr)><cfset airroll = airroll+rollfactr></cfif><cfelse><cfset dsfaroll = numberformat(rollfactr)><cfset airroll = airroll+rollfactr></cfif>
<cfset dshipomon = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif structkeyexists(HiPostruct,"#lookupyear#_#i#")><cfset dshipomon = numberformat(HiPostruct["#lookupyear#_#i#"])><cfset hipoctr = hipoctr+HiPostruct["#lookupyear#_#i#"]><cfelse><cfset dshipomon = 0></cfif></cfif><cfelse><cfif structkeyexists(HiPostruct,"#lookupyear#_#i#")><cfset dshipomon = numberformat(HiPostruct["#lookupyear#_#i#"])><cfset hipoctr = hipoctr+HiPostruct["#lookupyear#_#i#"]><cfelse><cfset dshipomon = 0></cfif></cfif>

<cfset dshipoytd = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dshipoytd = numberformat(hpctr)></cfif><cfelse><cfset dshipoytd = numberformat(hpctr)></cfif>

<cfset dshiporoll = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset dshiporoll = numberformat(rollhpctr)></cfif><cfelse><cfset dshiporoll = numberformat(rollhpctr)></cfif>
		
<cfset dsairmon = "">	
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif airctr gt 0 and monthhrs gt 0><cfset dsairmon = numberformat((airctr*200000)/monthhrs,"0.00")><cfelse><cfset dsairmon = 0></cfif></cfif><cfelse><cfif airctr gt 0 and monthhrs gt 0><cfset dsairmon = numberformat((airctr*200000)/monthhrs,"0.00")><cfelse><cfset dsairmon = 0></cfif></cfif>

<cfset dsairytd = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif airytd gt 0 and dspytdall gt 0><cfset dsairytd = numberformat((airytd*200000)/dspytdall,"0.00")><cfelse><cfset dsairytd = 0></cfif></cfif><cfelse><cfif airytd gt 0 and dspytdall gt 0><cfset dsairytd = numberformat((airytd*200000)/dspytdall,"0.00")><cfelse><cfset dsairytd = 0></cfif></cfif>
		
<cfset dsairroll = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif airroll gt 0 and dsprollos gt 0><cfset dsairroll = numberformat((airroll*200000)/dsprollos,"0.00")><cfelse><cfset dsairroll = 0></cfif></cfif><cfelse><cfif airroll gt 0 and dsprollos gt 0><cfset dsairroll = numberformat((airroll*200000)/dsprollos,"0.00")><cfelse><cfset dsairroll = 0></cfif></cfif>

<cfset dstriratemon = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctr gt 0 and monthhrs gt 0><cfset dstriratemon = numberformat((trirctr*200000)/monthhrs,"0.00")><cfelse><cfset dstriratemon = 0></cfif></cfif><cfelse><cfif trirctr gt 0 and monthhrs gt 0><cfset dstriratemon = numberformat((trirctr*200000)/monthhrs,"0.00")><cfelse><cfset dstriratemon = 0></cfif></cfif>
<cfset dstrirateytd = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctrytd gt 0 and dspytdall gt 0><cfset dstrirateytd = numberformat((trirctrytd*200000)/dspytdall,"0.00")><cfelse><cfset dstrirateytd = 0></cfif></cfif><cfelse><cfif trirctrytd gt 0 and dspytdall gt 0><cfset dstrirateytd = numberformat((trirctrytd*200000)/dspytdall,"0.00")><cfelse><cfset dstrirateytd = 0></cfif></cfif>

	<cfset dstrirateroll = "">
	<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctrroll gt 0 and dsprollos gt 0><cfset dstrirateroll = numberformat((trirctrroll*200000)/dsprollos,"0.00")><cfelse><cfset dstrirateroll = 0></cfif></cfif><cfelse><cfif trirctrroll gt 0 and dsprollos gt 0><cfset dstrirateroll = numberformat((trirctrroll*200000)/dsprollos,"0.00")><cfelse><cfset dstrirateroll = 0></cfif></cfif>



<cfset dsltiratemon = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif ltirctr gt 0 and monthhrs gt 0><cfset dsltiratemon = numberformat((ltirctr*200000)/monthhrs,"0.000")><cfelse><cfset dsltiratemon = 0></cfif></cfif><cfelse><cfif ltirctr gt 0 and monthhrs gt 0><cfset dsltiratemon = numberformat((ltirctr*200000)/monthhrs,"0.000")><cfelse><cfset dsltiratemon = 0></cfif></cfif>

<cfset dsltirateytd = "">
		<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif ltirctrytd gt 0 and dspytdall gt 0><cfset dsltirateytd = numberformat((ltirctrytd*200000)/dspytdall,"0.000")><cfelse><cfset dsltirateytd = 0></cfif></cfif><cfelse><cfif ltirctrytd gt 0 and dspytdall gt 0><cfset dsltirateytd = numberformat((ltirctrytd*200000)/dspytdall,"0.000")><cfelse><cfset dsltirateytd = 0></cfif></cfif>

<cfset dsltirateroll = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif trirctrroll gt 0 and dsprollos gt 0><cfset dsltirateroll = numberformat((ltirctrroll*200000)/dsprollos,"0.000")><cfelse><cfset dsltirateroll = 0></cfif></cfif><cfelse><cfif trirctrroll gt 0 and dsprollos gt 0><cfset dsltirateroll = numberformat((ltirctrroll*200000)/dsprollos,"0.000")><cfelse><cfset dsltirateroll = 0></cfif></cfif></td>


<cfset dshiporatemon = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif hipoctr gt 0 and monthhrs gt 0><cfset dshiporatemon = numberformat((hipoctr*200000)/monthhrs,"0.00")><cfelse><cfset dshiporatemon = 0></cfif></cfif><cfelse><cfif hipoctr gt 0 and monthhrs gt 0><cfset dshiporatemon = numberformat((hipoctr*200000)/monthhrs,"0.00")><cfelse><cfset dshiporatemon = 0></cfif></cfif>

<cfset dshiporateytd = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif hpctr gt 0 and dspytdall gt 0><cfset dshiporateytd = numberformat((hpctr*200000)/dspytdall,"0.00")><cfelse><cfset dshiporateytd = 0></cfif></cfif><cfelse><cfif hpctr gt 0 and dspytdall gt 0><cfset dshiporateytd = numberformat((hpctr*200000)/dspytdall,"0.00")><cfelse><cfset dshiporateytd = 0></cfif></cfif>

<cfset dshiporateroll = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfif rollhpctr gt 0 and dsprollos gt 0><cfset dshiporateroll = numberformat((rollhpctr*200000)/dsprollos,"0.00")><cfelse><cfset dshiporateroll = 0></cfif></cfif><cfelse><cfif rollhpctr gt 0 and dsprollos gt 0><cfset dshiporateroll = numberformat((rollhpctr*200000)/dsprollos,"0.00")><cfelse><cfset dshiporateroll = 0></cfif></cfif>
<cfset tritargdsr = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset tritargdsr = gettargets.TRIRgoal></cfif><cfelse><cfset tritargdsr = gettargets.TRIRgoal></cfif>
<cfset ltitargdsr = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset ltitargdsr = gettargets.LTIRgoal></cfif><cfelse><cfset ltitargdsr = gettargets.LTIRgoal></cfif>
<cfset airtargdsr = "">
<cfif lookupyear eq year(now())><cfif i lte month(now())><cfset airtargdsr = gettargets.airgoal></cfif><cfelse><cfset airtargdsr = gettargets.airgoal></cfif>
		<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"#left(monthasstring(i),3)#",#exctr#,1);
	 	SpreadsheetSetCellValue(theSheet,"#dsmonhrs#",#exctr#,2);
		SpreadsheetSetCellValue(theSheet,"#dsytdhrs#",#exctr#,3);
		SpreadsheetSetCellValue(theSheet,"#dsrollhrs#",#exctr#,4);
	 	SpreadsheetSetCellValue(theSheet,"#dsltimon#",#exctr#,5);
		SpreadsheetSetCellValue(theSheet,"#dsltiytd#",#exctr#,6);
		SpreadsheetSetCellValue(theSheet,"#dsltiroll#",#exctr#,7);
		SpreadsheetSetCellValue(theSheet,"#dstrimon#",#exctr#,8);
		SpreadsheetSetCellValue(theSheet,"#dstriytd#",#exctr#,9);
		SpreadsheetSetCellValue(theSheet,"#dstriroll#",#exctr#,10);
		SpreadsheetSetCellValue(theSheet,"#dsfamon#",#exctr#,11);
		SpreadsheetSetCellValue(theSheet,"#dsfaytd#",#exctr#,12);
		SpreadsheetSetCellValue(theSheet,"#dsfaroll#",#exctr#,13);
		SpreadsheetSetCellValue(theSheet,"#dshipomon#",#exctr#,14);
		SpreadsheetSetCellValue(theSheet,"#dshipoytd#",#exctr#,15);
		SpreadsheetSetCellValue(theSheet,"#dshiporoll#",#exctr#,16);
		SpreadsheetSetCellValue(theSheet,"#dsairmon#",#exctr#,17);
		SpreadsheetSetCellValue(theSheet,"#dsairytd#",#exctr#,18);
		// SpreadsheetSetCellValue(theSheet,"#airtargdsr#",#exctr#,19);
		SpreadsheetSetCellValue(theSheet,"#dsairroll#",#exctr#,19);
		SpreadsheetSetCellValue(theSheet,"#dstriratemon#",#exctr#,20);
		SpreadsheetSetCellValue(theSheet,"#dstrirateytd#",#exctr#,21);
		// SpreadsheetSetCellValue(theSheet,"#tritargdsr#",#exctr#,23);
		SpreadsheetSetCellValue(theSheet,"#dstrirateroll#",#exctr#,22);
		SpreadsheetSetCellValue(theSheet,"#dsltiratemon#",#exctr#,23);
		SpreadsheetSetCellValue(theSheet,"#dsltirateytd#",#exctr#,24);
		// SpreadsheetSetCellValue(theSheet,"#ltitargdsr#",#exctr#,27);
		SpreadsheetSetCellValue(theSheet,"#dsltirateroll#",#exctr#,25);
		SpreadsheetSetCellValue(theSheet,"#dshiporatemon#",#exctr#,26);
		SpreadsheetSetCellValue(theSheet,"#dshiporateytd#",#exctr#,27);
		
		SpreadsheetSetCellValue(theSheet,"#dshiporateroll#",#exctr#,28);
		
</cfscript>

			<!--- SpreadsheetSetCellValue(theSheet,"",#exctr#,31); --->
		
				
		
				
				

		

		

		
	
	</cfloop>

<cfset exctr = exctr + 1>
<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"",#exctr#,1);
</cfscript>
<cfset exctr = exctr + 1>
<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"LTIR",#exctr#,1);
		SpreadsheetMergeCells(theSheet,#exctr#,#exctr#,2,16); 
		SpreadsheetSetCellValue(theSheet,"Number of lost time incidents X 200,000 &divide; hours worked",#exctr#,2);
</cfscript>
<cfset exctr = exctr + 1>
<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"TRIR",#exctr#,1);
		SpreadsheetMergeCells(theSheet,#exctr#,#exctr#,2,16); 
		SpreadsheetSetCellValue(theSheet,"Number of recordable cases X 200,000 &divide; hours worked",#exctr#,2);
</cfscript>
<cfset exctr = exctr + 1>
<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"AIR",#exctr#,1);
		SpreadsheetMergeCells(theSheet,#exctr#,#exctr#,2,16); 
		SpreadsheetSetCellValue(theSheet,"Number of recordable cases + first aid cases X 200,000 &divide; hours worked",#exctr#,2);
</cfscript>
<cfset exctr = exctr + 1>
<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"Rolling",#exctr#,1);
		SpreadsheetMergeCells(theSheet,#exctr#,#exctr#,2,16); 
		SpreadsheetSetCellValue(theSheet,"12 months total, ie Jan = Feb #year(now())-1# to Jan #year(now())#",#exctr#,2);
</cfscript>





 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">
