<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportclientperf.cfm", "exports"))>
<cfset thefilename = "ClientPerformance_#incyear#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Dashboard","true")>
<cfset clictr = 1>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Client Performance",1,1);
</cfscript>
<cfoutput query="getclientreportprojects" group="clientid">
<cfset clictr = clictr+1>
<cfscript> 



	 SpreadsheetSetCellValue(theSheet,"#clientname#",#clictr#,1);
	SpreadsheetSetCellValue(theSheet,"Incident Assigned To",#clictr#,2);
	SpreadsheetSetCellValue(theSheet,"Hours",#clictr#,3);
	SpreadsheetSetCellValue(theSheet,"Fatality",#clictr#,4);
	SpreadsheetSetCellValue(theSheet,"LTI",#clictr#,5);
	SpreadsheetSetCellValue(theSheet,"RWC",#clictr#,6);
	SpreadsheetSetCellValue(theSheet,"MTC",#clictr#,7);
	SpreadsheetSetCellValue(theSheet,"OI-D",#clictr#,8);
	SpreadsheetSetCellValue(theSheet,"LTIR",#clictr#,9);
	SpreadsheetSetCellValue(theSheet,"TRIR",#clictr#,10);
	SpreadsheetSetCellValue(theSheet,"First Aid",#clictr#,11);
	SpreadsheetSetCellValue(theSheet,"AIR",#clictr#,12);
</cfscript>



	

	<cfset cliafwtot = 0>
	<cfset clijvtot = 0>
	<cfset clisubtot = 0>
	<cfset climctot = 0>
	<cfloop query="getincassignedto">
		<cfset "cliFATtot_#IncAssignedID#" = 0>
		<cfset "cliFAtot_#IncAssignedID#" = 0>
		<cfset "cliLTItot_#IncAssignedID#" = 0>
		<cfset "cliRWCtot_#IncAssignedID#" = 0>
		<cfset "cliMTCtot_#IncAssignedID#" = 0>
		<cfset "cliOIDtot_#IncAssignedID#" = 0>
	</cfloop>
	
	<cfset clienttothrs = 0>
	<cfset clienttotfat = 0>
	<cfset clienttotLTI = 0>
	<cfset clienttotRWC = 0>
	<cfset clienttotMTC = 0>
	<cfset clienttotOID = 0>
	<cfset clienttotFA = 0>
	
	<cfoutput group="group_number">
	
	<cfset projecttothrs = 0>
	<cfset projecttotfat = 0>
	<cfset projecttotLTI = 0>
	<cfset projecttotRWC = 0>
	<cfset projecttotMTC = 0>
	<cfset projecttotOID = 0>
	<cfset projecttotFA = 0>
	
	<cfset gdspctr = 0>
	
	<cfloop query="getincassignedto">
	<cfset clictr = clictr+1>
	<cfset thisrowhrs = 0>
	<cfset thisrowTR = 0>
	<cfset thisrowAR = 0>
	<cfset gdspctr = gdspctr+1>
	

		
		<cfswitch expression="#IncAssignedID#">
			<cfcase value="1">
				<cfif structkeyexists(amecfwmanhrstruct,"#getclientreportprojects.group_number#")>
					<!--- #numberformat(amecfwmanhrstruct["#getclientreportprojects.group_number#"])# --->
					<cfset cliafwtot = cliafwtot+amecfwmanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset thisrowhrs = amecfwmanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset projecttothrs = projecttothrs+amecfwmanhrstruct["#getclientreportprojects.group_number#"]>
				<!--- <cfelse>
					0 --->
				</cfif>
			</cfcase>
			<cfcase value="11">
				<cfif structkeyexists(jvmanhrstruct,"#getclientreportprojects.group_number#")>
					<!--- #numberformat(jvmanhrstruct["#getclientreportprojects.group_number#"])# --->
					<cfset clijvtot = clijvtot+jvmanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset thisrowhrs = jvmanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset projecttothrs = projecttothrs+jvmanhrstruct["#getclientreportprojects.group_number#"]>
				<!--- <cfelse>
					0 --->
				</cfif>
			</cfcase>
			<cfcase value="3">
				<cfif structkeyexists(submanhrstruct,"#getclientreportprojects.group_number#")>
					<!--- #numberformat(submanhrstruct["#getclientreportprojects.group_number#"])# --->
					<cfset clisubtot = clisubtot+submanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset thisrowhrs = submanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset projecttothrs = projecttothrs+submanhrstruct["#getclientreportprojects.group_number#"]>
				<!--- <cfelse>
					0 --->
				</cfif>
			</cfcase>
			<cfcase value="4">
				<cfif structkeyexists(mcmanhrstruct,"#getclientreportprojects.group_number#")>
					<!--- #numberformat(mcmanhrstruct["#getclientreportprojects.group_number#"])# --->
					<cfset climctot = climctot+mcmanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset thisrowhrs = mcmanhrstruct["#getclientreportprojects.group_number#"]>
					<cfset projecttothrs = projecttothrs+mcmanhrstruct["#getclientreportprojects.group_number#"]>
				<!--- <cfelse>
					0 --->
				</cfif>
			</cfcase>
		</cfswitch>
		
			<cfif structkeyexists(FATstruct,"#getclientreportprojects.group_number#_#IncAssignedID#")>
				<cfset rowFATval = numberformat(FATstruct["#getclientreportprojects.group_number#_#IncAssignedID#"])>
				<cfset "cliFATtot_#IncAssignedID#" = evaluate("cliFATtot_#IncAssignedID#")+FATstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset thisrowTR = thisrowTR+FATstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset projecttotfat = projecttotfat+FATstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
			<cfelse>
				<cfset rowFATval = 0>
			</cfif>
		
			<cfif structkeyexists(LTIstruct,"#getclientreportprojects.group_number#_#IncAssignedID#")>
				<cfset rowLTIval = numberformat(LTIstruct["#getclientreportprojects.group_number#_#IncAssignedID#"])>
				<cfset "cliLTItot_#IncAssignedID#" = evaluate("cliLTItot_#IncAssignedID#")+LTIstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset thisrowTR = thisrowTR+LTIstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfif thisrowhrs gt 0 and LTIstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
					<cfset rowLTIR = (LTIstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]*200000)/thisrowhrs>
				<cfelse>
					<cfset rowLTIR = 0>
				</cfif>
				<cfset projecttotLTI = projecttotLTI+LTIstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
			<cfelse>
				<cfset rowLTIval = 0>
				<cfset rowLTIR = 0>
			</cfif>
		
			<cfif structkeyexists(RWCstruct,"#getclientreportprojects.group_number#_#IncAssignedID#")>
				<cfset rowRWCval = numberformat(RWCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"])>
				<cfset "cliRWCtot_#IncAssignedID#" = evaluate("cliRWCtot_#IncAssignedID#")+RWCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset thisrowTR = thisrowTR+RWCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset projecttotRWC = projecttotRWC+RWCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
			<cfelse>
				<cfset rowRWCval = 0>
			</cfif>
		
			<cfif structkeyexists(MTCstruct,"#getclientreportprojects.group_number#_#IncAssignedID#")>
				<cfset rowMTCval = numberformat(MTCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"])>
				<cfset "cliMTCtot_#IncAssignedID#" = evaluate("cliMTCtot_#IncAssignedID#")+MTCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset thisrowTR = thisrowTR+MTCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset projecttotMTC = projecttotMTC+MTCstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
			<cfelse>
				<cfset rowMTCval = 0>
			</cfif>
		
			<cfif structkeyexists(OIDstruct,"#getclientreportprojects.group_number#_#IncAssignedID#")>
				<cfset rowOIDval = numberformat(OIDstruct["#getclientreportprojects.group_number#_#IncAssignedID#"])>
				<cfset "cliOIDtot_#IncAssignedID#" = evaluate("cliOIDtot_#IncAssignedID#")+OIDstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset thisrowTR = thisrowTR+OIDstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset projecttotOID = projecttotOID+OIDstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
			<cfelse>
				<cfset rowOIDval = 0>
			</cfif>
		
		<cfif rowLTIR gt 0><cfset rowLTIRnum = numberformat(rowLTIR,"0.000")><cfelse><cfset rowLTIRnum = 0></cfif>
		
		
		<cfif thisrowTR gt 0 and thisrowhrs gt 0><cfset rowTRIRnum = numberformat((thisrowTR*200000)/thisrowhrs,"0.00")><cfelse><cfset rowTRIRnum = 0></cfif>
		
		
		
		
			<cfif structkeyexists(FAstruct,"#getclientreportprojects.group_number#_#IncAssignedID#")>
				<cfset rowFAval = numberformat(FAstruct["#getclientreportprojects.group_number#_#IncAssignedID#"])>
				<cfset "cliFAtot_#IncAssignedID#" = evaluate("cliFAtot_#IncAssignedID#")+FAstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset thisrowAR = thisrowTR+FAstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
				<cfset projecttotFA = projecttotFA+FAstruct["#getclientreportprojects.group_number#_#IncAssignedID#"]>
			<cfelse>
				<cfset rowFAval = 0>
			</cfif>
		

		<cfif thisrowAR gt 0 and thisrowhrs gt 0><cfset rowAIRnum = numberformat((thisrowAR*200000)/thisrowhrs,"0.00")><cfelse><cfset rowAIRnum = 0></cfif>
		
	<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"#getclientreportprojects.group_name#",#clictr#,1);
	SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#clictr#,2);
	SpreadsheetSetCellValue(theSheet,"#numberformat(thisrowhrs)#",#clictr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowFATval)#",#clictr#,4);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowLTIval)#",#clictr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowRWCval)#",#clictr#,6);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowMTCval)#",#clictr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowOIDval)#",#clictr#,8);
	SpreadsheetSetCellValue(theSheet,"#rowLTIRnum#",#clictr#,9);
	SpreadsheetSetCellValue(theSheet,"#rowTRIRnum#",#clictr#,10);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowFAval)#",#clictr#,11);
	SpreadsheetSetCellValue(theSheet,"#rowAIRnum#",#clictr#,12);
	</cfscript>
	</cfloop>
	
	<cfset clictr = clictr+1>
	
	
	<cfset clienttothrs = clienttothrs+projecttothrs>
	<cfset clienttotfat = clienttotfat+projecttotfat>
	<cfset clienttotLTI =  clienttotLTI+projecttotLTI>
	<cfset clienttotRWC =  clienttotRWC+projecttotRWC>
	<cfset clienttotMTC =  clienttotMTC+projecttotMTC>
	<cfset clienttotOID =  clienttotOID+projecttotOID>
	<cfif projecttotLTI gt 0 and projecttothrs gt 0>
		<cfset projectLTIR = (projecttotLTI*200000)/projecttothrs>
	<cfelse>
		<cfset projectLTIR = 0>
	</cfif>
	<cfset projectTotRec = projecttotfat+projecttotLTI+projecttotMTC+projecttotOID+projecttotRWC>
	<cfif projectTotRec gt 0 and projecttothrs gt 0>
		<cfset projectTRIR = (projectTotRec*200000)/projecttothrs>
	<cfelse>
		<cfset projectTRIR = 0>
	</cfif>
	<cfset clienttotFA =  clienttotFA+projecttotFA>
	<cfset projAIRcnt = projectTotRec+projecttotFA>
	<cfif projAIRcnt gt 0 and projecttothrs gt 0>
		<cfset projectAIR = (projAIRcnt*200000)/projecttothrs>
	<cfelse>
		<cfset projectAIR = 0>
	</cfif>
	<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"",#clictr#,1);
	SpreadsheetSetCellValue(theSheet,"Project Total",#clictr#,2);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttothrs)#",#clictr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttotfat)#",#clictr#,4);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttotLTI)#",#clictr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttotRWC)#",#clictr#,6);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttotMTC)#",#clictr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttotOID)#",#clictr#,8);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projectLTIR,'0.000')#",#clictr#,9);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projectTRIR,'0.00')#",#clictr#,10);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projecttotFA)#",#clictr#,11);
	SpreadsheetSetCellValue(theSheet,"#numberformat(projectAIR,'0.00')#",#clictr#,12);
	</cfscript>
	
	
	</cfoutput>
	<cfset gdspctr = 0>
	<cfloop query="getincassignedto">
	<cfset clictr = clictr+1>
	<cfset thisrowhrs = 0>
	<cfset thisrowTR = 0>
	<cfset thisrowAR = 0>
	
	<cfset gdspctr = gdspctr+1>
	
	
	
	
	
	
	
		<cfswitch expression="#IncAssignedID#">
				<cfcase value="1">
					<cfset thisrowhrs = cliafwtot>
				</cfcase>
			<cfcase value="11">
				<cfset thisrowhrs = clijvtot>
			</cfcase>
			<cfcase value="3">
				<cfset thisrowhrs = clisubtot>
			</cfcase>
			<cfcase value="4">
				<cfset thisrowhrs = climctot>
			</cfcase>
		</cfswitch>
		<cfset thisrowTR = evaluate("cliFATtot_#IncAssignedID#")+evaluate("cliLTItot_#IncAssignedID#")+evaluate("cliRWCtot_#IncAssignedID#")+evaluate("cliMTCtot_#IncAssignedID#")+evaluate("cliOIDtot_#IncAssignedID#")>
		<cfset thisrowAR = thisrowTR+evaluate("cliFAtot_#IncAssignedID#")>
		
		<cfif thisrowhrs gt 0 and evaluate("cliLTItot_#IncAssignedID#") gt 0><cfset rowLTIRnum = numberformat((evaluate("cliLTItot_#IncAssignedID#")*200000)/thisrowhrs,"0.000")><cfelse><cfset rowLTIRnum = 0></cfif>
		<cfif thisrowTR gt 0 and thisrowhrs gt 0><cfset rowTRIRnum = numberformat((thisrowTR*200000)/thisrowhrs,"0.00")><cfelse><cfset rowTRIRnum = 0></cfif>
		
		<cfif thisrowAR gt 0 and thisrowhrs gt 0><cfset rowAIRnum = numberformat((thisrowAR*200000)/thisrowhrs,"0.00")><cfelse><cfset rowAIRnum = 0></cfif>
	
	<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"#getclientreportprojects.clientname# Total",#clictr#,1);
	SpreadsheetSetCellValue(theSheet,"#IncAssignedTo#",#clictr#,2);
	SpreadsheetSetCellValue(theSheet,"#numberformat(thisrowhrs)#",#clictr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(evaluate("cliFATtot_#IncAssignedID#"))#",#clictr#,4);
	SpreadsheetSetCellValue(theSheet,"#numberformat(evaluate("cliLTItot_#IncAssignedID#"))#",#clictr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(evaluate("cliRWCtot_#IncAssignedID#"))#",#clictr#,6);
	SpreadsheetSetCellValue(theSheet,"#numberformat(evaluate("cliMTCtot_#IncAssignedID#"))#",#clictr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(evaluate("cliOIDtot_#IncAssignedID#"))#",#clictr#,8);
	SpreadsheetSetCellValue(theSheet,"#rowLTIRnum#",#clictr#,9);
	SpreadsheetSetCellValue(theSheet,"#rowTRIRnum#",#clictr#,10);
	SpreadsheetSetCellValue(theSheet,"#numberformat(evaluate("cliFAtot_#IncAssignedID#"))#",#clictr#,11);
	SpreadsheetSetCellValue(theSheet,"#rowAIRnum#",#clictr#,12);
	</cfscript>
	
	</cfloop>

	<cfset clictr = clictr+1>
	<cfif clienttotLTI gt 0 and clienttothrs gt 0>
		<cfset clientLTIR = (clienttotLTI*200000)/clienttothrs>
	<cfelse>
		<cfset clientLTIR = 0>
	</cfif>
	<cfset ClientTotRec = clienttotfat+clienttotLTI+clienttotRWC+clienttotOID+clienttotMTC>
	<cfif ClientTotRec gt 0 and clienttothrs gt 0>
		<cfset ClientTRIR = (ClientTotRec*200000)/clienttothrs>
	<cfelse>
		<cfset ClientTRIR = 0>
	</cfif>
	<cfset clierntAIRcnt = ClientTotRec+clienttotFA>
	<cfif clierntAIRcnt gt 0 and clienttothrs gt 0>
		<cfset clientAIR = (clierntAIRcnt*200000)/clienttothrs>
	<cfelse>
		<cfset clientAIR = 0>
	</cfif>
	<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"",#clictr#,1);
	SpreadsheetSetCellValue(theSheet,"Client Total",#clictr#,2);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttothrs)#",#clictr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttotfat)#",#clictr#,4);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttotLTI)#",#clictr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttotRWC)#",#clictr#,6);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttotMTC)#",#clictr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttotOID)#",#clictr#,8);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clientLTIR,'0.000')#",#clictr#,9);
	SpreadsheetSetCellValue(theSheet,"#numberformat(ClientTRIR,'0.00')#",#clictr#,10);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clienttotFA)#",#clictr#,11);
	SpreadsheetSetCellValue(theSheet,"#numberformat(clientAIR,'0.00')#",#clictr#,12);
	</cfscript>
	



</cfoutput>

 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">