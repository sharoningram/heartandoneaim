<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportnumbersemployed.cfm", "exports"))>
<cfset thefilename = "NumberEmployed_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Numbers Employed","true")>
<cfscript> 
	 SpreadsheetMergeCells(theSheet,1,1,1,2); 
	 SpreadsheetSetCellValue(theSheet,"Numbers Employed Report (#monthasstring(month(qrymonth))# #year(qrymonth)#)",1,1);
	
	 SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",2,1);
	 SpreadsheetSetCellValue(theSheet,"AFW Employees",2,2);
	 SpreadsheetSetCellValue(theSheet,"Managed Contractor",2,3);
	 SpreadsheetSetCellValue(theSheet,"Subcontractor",2,4);
	 SpreadsheetSetCellValue(theSheet,"Joint Venture",2,5);
	 SpreadsheetSetCellValue(theSheet,"Total",2,6);
	
</cfscript>

	<cfset ctr = 2>
	<cfset globalAFW = 0>
	<cfset globalSUB = 0>
	<cfset globalMC = 0>
	<cfset globalJV = 0>
	<cfoutput query="getnumemplyed" group="buname">
	<cfset rowtot = 0>
	<cfif structkeyexists(numempstruct,"#buid#_AFW")><cfset afwnum = numempstruct["#buid#_AFW"]><cfelse><cfset afwnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#buid#_SUB")><cfset subnum = numempstruct["#buid#_SUB"]><cfelse><cfset subnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#buid#_JV")><cfset jvnum = numempstruct["#buid#_JV"]><cfelse><cfset jvnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#buid#_MC")><cfset mcnum = numempstruct["#buid#_MC"]><cfelse><cfset mcnum = 0></cfif>
	
	<cfset globalAFW = globalAFW+afwnum>
	<cfset globalSUB = globalSUB+subnum>
	<cfset globalMC = globalMC+mcnum>
	<cfset globalJV = globalJV+jvnum>
	<cfset ctr = ctr+1>
		<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(afwnum)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mcnum)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subnum)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvnum)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	
	<cfoutput group="ouid">
	<cfset rowtot = 0>
	<cfif structkeyexists(numempstruct,"#ouid#_AFW")><cfset afwnum = numempstruct["#ouid#_AFW"]><cfelse><cfset afwnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#ouid#_SUB")><cfset subnum = numempstruct["#ouid#_SUB"]><cfelse><cfset subnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#ouid#_JV")><cfset jvnum = numempstruct["#ouid#_JV"]><cfelse><cfset jvnum = 0></cfif>
	<cfif structkeyexists(numempstruct,"#ouid#_MC")><cfset mcnum = numempstruct["#ouid#_MC"]><cfelse><cfset mcnum = 0></cfif>
	<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
	<cfif listfind(openbu,buid) gt 0>
	<cfset ctr = ctr+1>
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(afwnum)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mcnum)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subnum)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvnum)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	</cfif>
	
	<cfoutput group="group_number">
		<cfset rowtot = 0>
		<cfif listfind(openou,ouid) gt 0>
		
		<cfset ctr = ctr + 1>
		<cfif structkeyexists(grpnumempstruct,"#group_number#_AFW")><cfset afwnum = grpnumempstruct["#group_number#_AFW"]><cfelse><cfset afwnum = 0></cfif>
	<cfif structkeyexists(grpnumempstruct,"#group_number#_SUB")><cfset subnum = grpnumempstruct["#group_number#_SUB"]><cfelse><cfset subnum = 0></cfif>
	<cfif structkeyexists(grpnumempstruct,"#group_number#_JV")><cfset jvnum = grpnumempstruct["#group_number#_JV"]><cfelse><cfset jvnum = 0></cfif>
	<cfif structkeyexists(grpnumempstruct,"#group_number#_MC")><cfset mcnum = grpnumempstruct["#group_number#_MC"]><cfelse><cfset mcnum = 0></cfif>
	<cfset rowtot = rowtot+afwnum+subnum+jvnum+mcnum>
		<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#group_name#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(afwnum)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mcnum)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subnum)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvnum)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
		
		</cfif>
	</cfoutput>
	
	
	</cfoutput>
	</cfoutput>
	
	
	
	
	
	
	<cfset rowtot = 0>
	
	
	<cfset ctr = ctr+1>
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler Global",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(globalAFW)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(globalMC)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(globalSUB)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(globalJV)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(globalAFW+globalMC+globalSUB+globalJV)#",#ctr#,6);
	</cfscript>

<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#"> 
