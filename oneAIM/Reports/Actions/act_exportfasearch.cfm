<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportfasearch.cfm", "exports"))>
<cfset thefilename = "FASearch_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("First Alert Search Results","true")>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Incident Number",1,1);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,4);
	SpreadsheetSetCellValue(theSheet,"Is Near Miss",1,5);
	SpreadsheetSetCellValue(theSheet,"Incident Date",1,6);
	SpreadsheetSetCellValue(theSheet,"Incident Type",1,7);
	SpreadsheetSetCellValue(theSheet,"OSHA Classification",1,8);
	SpreadsheetSetCellValue(theSheet,"Potential Rating",1,9);
	
	SpreadsheetSetCellValue(theSheet,"Short Description",1,10);
	SpreadsheetSetCellValue(theSheet,"Record Status",1,11);
</cfscript>

						<cfset ctr = 1>
						<cfoutput query="getincidents">
						<cfif isfatality eq "Yes">
							<cfset dspinctype = "Fatality">
						<cfelse>
							<cfset dspinctype = IncType>
						</cfif>
						<cfset ctr = ctr+1>
						<cfscript> 
							SpreadsheetSetCellValue(theSheet,"#TrackingNum#",#ctr#,1);
							SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,2);
							SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,3);
							SpreadsheetSetCellValue(theSheet,"#Group_Name#",#ctr#,4);
							SpreadsheetSetCellValue(theSheet,"#isnearmiss#",#ctr#,5);
							SpreadsheetSetCellValue(theSheet,"#dateformat(incidentDate,sysdateformatxcel)#",#ctr#,6);
							SpreadsheetSetCellValue(theSheet,"#dspinctype#",#ctr#,7);
							SpreadsheetSetCellValue(theSheet,"#Category#",#ctr#,8);
							SpreadsheetSetCellValue(theSheet,"#PotentialRating#",#ctr#,9);
							
							SpreadsheetSetCellValue(theSheet,"#ShortDesc#",#ctr#,10);
							SpreadsheetSetCellValue(theSheet,"#status#",#ctr#,11);
						</cfscript>
						
						</cfoutput>
		
					
		 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">