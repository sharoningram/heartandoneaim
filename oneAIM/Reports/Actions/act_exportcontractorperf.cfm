<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportcontractorperf.cfm", "exports"))>
<cfset thefilename = "ContractorPerformance_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Contractor Performance","true")>
<cfset ctr = 1>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Contractor Performance",1,1);
</cfscript>


<cfoutput query="getcontractorreplist" group="cotype">
<cfset ctr = ctr+1>
<cfset cotypedsp = "">
	<cfif cotype eq "jv">
		<cfset cotypedsp = "Joint Venture Performance">
	<cfelseif cotype eq "sub">
		<cfset cotypedsp = "Subcontractor Performance">
	<cfelseif cotype eq "mgd">
		<cfset cotypedsp = "Managed Contractor Performance">
	</cfif>
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#cotypedsp#",#ctr#,1);
		
	</cfscript>
		

	<cfoutput group="ContractorNameID">
	<cfset ctr = ctr+1>
	<cfset contothrs = 0>
	<cfset contotFAT = 0>
	<cfset contotLTI = 0>
	<cfset contotRWC = 0>
	<cfset contotMTC = 0>
	<cfset contotOID = 0>
	<cfset contotFAid = 0>
	<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#ContractorName#",#ctr#,1);
		SpreadsheetSetCellValue(theSheet,"Hours",#ctr#,2);
		SpreadsheetSetCellValue(theSheet,"Fatality",#ctr#,3);
		SpreadsheetSetCellValue(theSheet,"LTI",#ctr#,4);
		SpreadsheetSetCellValue(theSheet,"RWC",#ctr#,5);
		SpreadsheetSetCellValue(theSheet,"MTC",#ctr#,6);
		SpreadsheetSetCellValue(theSheet,"OI-D",#ctr#,7);
		SpreadsheetSetCellValue(theSheet,"LTIR",#ctr#,8);
		SpreadsheetSetCellValue(theSheet,"TRIR",#ctr#,9);
		SpreadsheetSetCellValue(theSheet,"First Aid",#ctr#,10);
		SpreadsheetSetCellValue(theSheet,"AIR",#ctr#,11);
	</cfscript>
	<cfoutput>
	<cfset ctr = ctr+1>
	<cfset thisrwhrs = 0>
	<cfset thisrwlti = 0>
	<cfset thisrwtotrec = 0>
	<cfset thisrwallinc = 0>
		<cfif structkeyexists(conhrsstruct,contractorid)>
			<cfset hrs = numberformat(conhrsstruct[contractorid])>
			<cfset contothrs = contothrs+conhrsstruct[contractorid]>
			<cfset thisrwhrs = conhrsstruct[contractorid]>
		<cfelse>
			<cfset hrs = 0>
		</cfif>
		<cfif structkeyexists(conFATstruct,contractorid)>
			<cfset fat = numberformat(conFATstruct[contractorid])>
			<cfset contotFAT = contotFAT+conFATstruct[contractorid]>
			<cfset thisrwtotrec = thisrwtotrec+conFATstruct[contractorid]>
		<cfelse>
			<cfset fat = 0>
		</cfif>
		<cfif structkeyexists(LTIstruct,contractorid)>
			<cfset ltis = numberformat(LTIstruct[contractorid])>
			<cfset contotLTI = contotLTI+LTIstruct[contractorid]>
			<cfset thisrwlti = LTIstruct[contractorid]>
			<cfset thisrwtotrec = thisrwtotrec+LTIstruct[contractorid]>
		<cfelse>
			<cfset ltis = 0>
		</cfif>
		<cfif structkeyexists(RWCstruct,contractorid)>
			<cfset rwcs = numberformat(RWCstruct[contractorid])>
			<cfset contotRWC = contotRWC+RWCstruct[contractorid]>
			<cfset thisrwtotrec = thisrwtotrec+RWCstruct[contractorid]>
		<cfelse>
			<cfset rwcs = 0>
		</cfif>
		<cfif structkeyexists(MTCstruct,contractorid)>
			<cfset mtcs = numberformat(MTCstruct[contractorid])>
			<cfset contotMTC = contotMTC+MTCstruct[contractorid]>
			<cfset thisrwtotrec = thisrwtotrec+MTCstruct[contractorid]>
		<cfelse>
			<cfset mtcs = 0>
		</cfif>
		<cfif structkeyexists(OIDstruct,contractorid)>
			<cfset oids = numberformat(OIDstruct[contractorid])>
			<cfset contotOID = contotOID+OIDstruct[contractorid]>
			<cfset thisrwtotrec = thisrwtotrec+OIDstruct[contractorid]>
		<cfelse>
			<cfset oids = 0>
		</cfif>
		<cfif thisrwhrs gt 0 and thisrwlti gt 0><cfset ltirrw = numberformat((thisrwlti*200000)/thisrwhrs,"0.00")><cfelse><cfset ltirrw = 0></cfif>
		<cfif thisrwhrs gt 0 and thisrwtotrec gt 0><cfset totri = numberformat((thisrwtotrec*200000)/thisrwhrs,"0.00")><cfelse><cfset totri = 0></cfif>
		<cfif structkeyexists(FAstruct,contractorid)>
			<cfset faids = numberformat(FAstruct[contractorid])>
			<cfset contotFAid = contotFAid+FAstruct[contractorid]>
			<cfset thisrwallinc = thisrwtotrec+FAstruct[contractorid]>
		<cfelse>
			<cfset faids = 0>
		</cfif>
		<cfif thisrwhrs gt 0 and thisrwallinc gt 0><cfset airt = numberformat((thisrwallinc*200000)/thisrwhrs,"0.00")><cfelse><cfset airt = 0></cfif>
		
		
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#group_name#",#ctr#,1);
		SpreadsheetSetCellValue(theSheet,"#hrs#",#ctr#,2);
		SpreadsheetSetCellValue(theSheet,"#fat#",#ctr#,3);
		SpreadsheetSetCellValue(theSheet,"#ltis#",#ctr#,4);
		SpreadsheetSetCellValue(theSheet,"#rwcs#",#ctr#,5);
		SpreadsheetSetCellValue(theSheet,"#mtcs#",#ctr#,6);
		SpreadsheetSetCellValue(theSheet,"#oids#",#ctr#,7);
		SpreadsheetSetCellValue(theSheet,"#ltirrw#",#ctr#,8);
		SpreadsheetSetCellValue(theSheet,"#totri#",#ctr#,9);
		SpreadsheetSetCellValue(theSheet,"#faids#",#ctr#,10);
		SpreadsheetSetCellValue(theSheet,"#airt#",#ctr#,11);
	</cfscript>
		
		</cfoutput>
		<cfset contotrec = contotFAT+contotLTI+contotRWC+contotMTC+contotOID>
		<cfset contotar = contotrec+contotFAid>
		<cfset ctr = ctr+1>
		<cfif contothrs gt 0 and contotLTI gt 0><cfset conlti = numberformat((contotLTI*200000)/contothrs,"0.00")><cfelse><cfset conlti = 0></cfif>
		<cfif contothrs gt 0 and contotrec gt 0><cfset contri = numberformat((contotrec*200000)/contothrs,"0.00")><cfelse><cfset contri = 0></cfif>
		<cfif contothrs gt 0 and contotar gt 0><cfset conair = numberformat((contotar*200000)/contothrs,"0.00")><cfelse><cfset conair = 0></cfif>
		<cfscript> 
		SpreadsheetSetCellValue(theSheet,"#ContractorName# Total",#ctr#,1);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contothrs)#",#ctr#,2);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contotFAT)#",#ctr#,3);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contotLTI)#",#ctr#,4);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contotRWC)#",#ctr#,5);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contotMTC)#",#ctr#,6);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contotOID)#",#ctr#,7);
		SpreadsheetSetCellValue(theSheet,"#conlti#",#ctr#,8);
		SpreadsheetSetCellValue(theSheet,"#contri#",#ctr#,9);
		SpreadsheetSetCellValue(theSheet,"#numberformat(contotFAid)#",#ctr#,10);
		SpreadsheetSetCellValue(theSheet,"#conair#",#ctr#,11);
	</cfscript>
		
</cfoutput>
</cfoutput>
 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">