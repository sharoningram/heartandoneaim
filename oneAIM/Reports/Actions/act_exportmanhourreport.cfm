<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportmanhourreport.cfm", "exports"))>
<cfset thefilename = "ManHourReport_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Man Hour Report","true")>
<cfscript> 
	 SpreadsheetMergeCells(theSheet,1,1,1,2); 
	 SpreadsheetSetCellValue(theSheet,"Man-Hours Total Report",1,1);
	 SpreadsheetMergeCells(theSheet,1,1,3,8); 
	 SpreadsheetSetCellValue(theSheet,"#dspoulist#",1,3);
	 SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",2,1);
	 SpreadsheetSetCellValue(theSheet,"AFW Employees",2,2);
	 SpreadsheetSetCellValue(theSheet,"Managed Contractor",2,3);
	 SpreadsheetSetCellValue(theSheet,"Subcontractor",2,4);
	 SpreadsheetSetCellValue(theSheet,"Joint Venture",2,5);
	 SpreadsheetSetCellValue(theSheet,"Total",2,6);
	
</cfscript>

	<cfset ctr = 2>
	<cfoutput query="getOU" group="buname">
	<cfset rowtot = 0>
	<cfif structkeyexists(manhrstruct,qrybuid)><cfset jdehrs = manhrstruct[qrybuid]><cfelse><cfset jdehrs = 0></cfif>
	<cfif structkeyexists(subhrsstruct,qrybuid)><cfset subhours = subhrsstruct[qrybuid]><cfelse><cfset subhours = 0></cfif>
	<cfif structkeyexists(jvhrsstruct,qrybuid)><cfset jvhours = jvhrsstruct[qrybuid]><cfelse><cfset jvhours = 0></cfif>
	<cfif structkeyexists(mdghrsstruct,qrybuid)><cfset mdghours = mdghrsstruct[qrybuid]><cfelse><cfset mdghours = 0></cfif>
	<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	<cfset ctr = ctr+1>
		<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jdehrs)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mdghours)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subhours)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvhours)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	
	<cfoutput>
	<cfset rowtot = 0>
	<cfif structkeyexists(manhrstruct,id)><cfset jdehrs = manhrstruct[id]><cfelse><cfset jdehrs = 0></cfif>
	<cfif structkeyexists(subhrsstruct,id)><cfset subhours = subhrsstruct[id]><cfelse><cfset subhours = 0></cfif>
	<cfif structkeyexists(jvhrsstruct,id)><cfset jvhours = jvhrsstruct[id]><cfelse><cfset jvhours = 0></cfif>
	<cfif structkeyexists(mdghrsstruct,id)><cfset mdghours = mdghrsstruct[id]><cfelse><cfset mdghours = 0></cfif>
	<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	<cfif listfind(openbu,qrybuid) gt 0>
	<cfset ctr = ctr+1>
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#name#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jdehrs)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mdghours)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subhours)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvhours)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	</cfif>
	
	<cfloop query="getgroups">
		<cfset rowtot = 0>
		<cfif listfind(openou,ouid) gt 0>
		<cfif getgroups.ouid eq getOU.ID>
		<cfset ctr = ctr + 1>
		<cfif structkeyexists(manhrstructgrp,getgroups.group_number)><cfset jdehrs = manhrstructgrp[getgroups.group_number]><cfelse><cfset jdehrs = 0></cfif>
		<cfif structkeyexists(subhrsstructgrp,getgroups.group_number)><cfset subhours = subhrsstructgrp[getgroups.group_number]><cfelse><cfset subhours = 0></cfif>
		<cfif structkeyexists(jvhrsstructgrp,getgroups.group_number)><cfset jvhours = jvhrsstructgrp[getgroups.group_number]><cfelse><cfset jvhours = 0></cfif>
		<cfif structkeyexists(mdghrsstructgrp,getgroups.group_number)><cfset mdghours = mdghrsstructgrp[getgroups.group_number]><cfelse><cfset mdghours = 0></cfif>
		<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
		<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#getgroups.group_name#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jdehrs)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mdghours)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subhours)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvhours)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	
	
	
	
		<cfloop query="getgroupsites">
		<cfset rowtot = 0>
		<cfif listfind(opengnum,GroupNumber) gt 0>
		
		
			<cfif getgroupsites.GroupNumber eq getgroups.Group_Number>
			<cfset ctr = ctr + 1>
			<cfif structkeyexists(manhrstructloc,getgroupsites.GroupLocID)><cfset jdehrs = manhrstructloc[getgroupsites.GroupLocID]><cfelse><cfset jdehrs = 0></cfif>
		<cfif structkeyexists(subhrsstructloc,getgroupsites.GroupLocID)><cfset subhours = subhrsstructloc[getgroupsites.GroupLocID]><cfelse><cfset subhours = 0></cfif>
		<cfif structkeyexists(jvhrsstructloc,getgroupsites.GroupLocID)><cfset jvhours = jvhrsstructloc[getgroupsites.GroupLocID]><cfelse><cfset jvhours = 0></cfif>
		<cfif structkeyexists(mdghrsstructloc,getgroupsites.GroupLocID)><cfset mdghours = mdghrsstructloc[getgroupsites.GroupLocID]><cfelse><cfset mdghours = 0></cfif>
		<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
		<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"#getgroupsites.SiteName#",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jdehrs)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mdghours)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subhours)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvhours)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>
	
	
			
			</cfif>
		</cfif>
		
		</cfloop>
		
	
	
		</cfif>
		</cfif>
	</cfloop>
	
	
	</cfoutput>
	</cfoutput>
	
	
	
	
	
	
	<cfset rowtot = 0>
	<cfif structkeyexists(manhrstruct,"Global")><cfset jdehrs = manhrstruct["Global"]><cfelse><cfset jdehrs = 0></cfif>
	<cfif structkeyexists(subhrsstruct,"Global")><cfset subhours = subhrsstruct["Global"]><cfelse><cfset subhours = 0></cfif>
	<cfif structkeyexists(jvhrsstruct,"Global")><cfset jvhours = jvhrsstruct["Global"]><cfelse><cfset jvhours = 0></cfif>
	<cfif structkeyexists(mdghrsstruct,"Global")><cfset mdghours = mdghrsstruct["Global"]><cfelse><cfset mdghours = 0></cfif>
	<cfset rowtot = rowtot+jdehrs+subhours+jvhours+mdghours>
	
	<cfset ctr = ctr+1>
	<cfscript> 
		  SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler Global",#ctr#,1);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jdehrs)#",#ctr#,2);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(mdghours)#",#ctr#,3);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(subhours)#",#ctr#,4);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(jvhours)#",#ctr#,5);
		 SpreadsheetSetCellValue(theSheet,"#numberformat(rowtot)#",#ctr#,6);
	</cfscript>

<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#"> 
