<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportfrb.cfm", "exports"))>
<cfset thefilename = "FrequencyRateTable_#incyear#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Frequency Rate Breakdown Table","true")>
<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"#dspoulist# Frequency Rate Breakdown Table For #incyear#",1,1);
	 SpreadsheetMergeCells(theSheet,2,3,1,1); 
	SpreadsheetSetCellValue(theSheet,"Company",2,1);
	SpreadsheetMergeCells(theSheet,2,3,2,2); 
	SpreadsheetSetCellValue(theSheet,"Hours Worked",2,2);
	SpreadsheetMergeCells(theSheet,2,2,3,4); 
	SpreadsheetSetCellValue(theSheet,"All Injury Rate",2,3);
	SpreadsheetMergeCells(theSheet,2,2,5,6); 
	SpreadsheetSetCellValue(theSheet,"Total Recordable Incident Rate",2,5);
	SpreadsheetMergeCells(theSheet,2,2,7,8);
	SpreadsheetSetCellValue(theSheet,"Lost Time Incident Rate",2,7);
	SpreadsheetSetCellValue(theSheet,"Target",3,3);
	SpreadsheetSetCellValue(theSheet,"YTD Performance",3,4);
	SpreadsheetSetCellValue(theSheet,"Target",3,5);
	SpreadsheetSetCellValue(theSheet,"YTD Performance",3,6);
	SpreadsheetSetCellValue(theSheet,"Target",3,7);
	SpreadsheetSetCellValue(theSheet,"YTD Performance",3,8);
	
</cfscript>

 	<!---  if (dosearch IS "") {
	
		 SpreadsheetSetCellValue(theSheet,"#dspoulist# From #dateformat("1/1/#year(now())#",sysdateformat)# to #dateformat(now(),sysdateformat)#; All Incidents",2,1);
	}
	else {
		 SpreadsheetSetCellValue(theSheet,"#dspoulist# For #incyear#; All Incidents",2,1);
	}
	SpreadsheetSetCellValue(theSheet,"BU",3,1);
	SpreadsheetSetCellValue(theSheet,"Incident Assigned To",3,2);
	SpreadsheetSetCellValue(theSheet,"Hours",3,3);
	SpreadsheetSetCellValue(theSheet,"Fatality",3,4);
	SpreadsheetSetCellValue(theSheet,"LTI",3,5);
	SpreadsheetSetCellValue(theSheet,"RWC",3,6);
	SpreadsheetSetCellValue(theSheet,"MTC",3,7);
	SpreadsheetSetCellValue(theSheet,"LTIR",3,8);
	SpreadsheetSetCellValue(theSheet,"TRIR",3,9);
	SpreadsheetSetCellValue(theSheet,"First Aid",3,10);
	SpreadsheetSetCellValue(theSheet,"AIR",3,11);  --->
	<cfset ctr = 0>
	
	<cfset execctr = 3>
	
	<cfoutput query="getOU" group="buname">
	<cfset execctr = execctr+1>
	<cfif structkeyexists(manhrstructprev,qrybuid)><cfset prevrowhr = manhrstructprev[qrybuid]><cfelse><cfset prevrowhr = 0></cfif>
	<cfif structkeyexists(manhrstruct,qrybuid)><cfset rowhr = manhrstruct[qrybuid]><cfelse><cfset rowhr = 0></cfif>
	
	<cfif structkeyexists(targetstructair,qrybuid)><cfset airgoal = targetstructair[qrybuid]><cfelse><cfset airgoal = "Not Set"></cfif>
	<cfif structkeyexists(AIRstructprev,qrybuid)><cfset useairprev = AIRstructprev[qrybuid]><cfelse><cfset useairprev = 0></cfif>
	<cfif structkeyexists(AIRstruct,qrybuid)><cfset useair = AIRstruct[qrybuid]><cfelse><cfset useair = 0></cfif>
	<cfif prevrowhr gt 0 and useairprev gt 0>
		<cfset prevairrate = (useairprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevairrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useair gt 0>
		<cfset airrate = (useair*200000)/rowhr>
	<cfelse>
		<cfset airrate = 0>
	</cfif>
	
	
	<cfif structkeyexists(targetstructtrir,qrybuid)><cfset TRIRgoal = targetstructtrir[qrybuid]><cfelse><cfset TRIRgoal = "Not Set"></cfif>
	<cfif structkeyexists(TRIRstructprev,qrybuid)><cfset useTRIRprev = TRIRstructprev[qrybuid]><cfelse><cfset useTRIRprev = 0></cfif>
	<cfif structkeyexists(TRIRstruct,qrybuid)><cfset useTRIR = TRIRstruct[qrybuid]><cfelse><cfset useTRIR = 0></cfif>
	<cfif prevrowhr gt 0 and useTRIRprev gt 0>
		<cfset prevTRIRrate = (useTRIRprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevTRIRrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useTRIR gt 0>
		<cfset TRIRrate = (useTRIR*200000)/rowhr>
	<cfelse>
		<cfset TRIRrate = 0>
	</cfif>
	
	
	<cfif structkeyexists(targetstructlti,qrybuid)><cfset LTIgoal = targetstructlti[qrybuid]><cfelse><cfset LTIgoal = "Not Set"></cfif>
	<cfif structkeyexists(LTIstructprev,qrybuid)><cfset useLTIprev = LTIstructprev[qrybuid]><cfelse><cfset useLTIprev = 0></cfif>
	<cfif structkeyexists(LTIstruct,qrybuid)><cfset useLTI = LTIstruct[qrybuid]><cfelse><cfset useLTI = 0></cfif>
	<cfif prevrowhr gt 0 and useLTIprev gt 0>
		<cfset prevLTIrate = (useLTIprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevLTIrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useLTI gt 0>
		<cfset LTIrate = (useLTI*200000)/rowhr>
	<cfelse>
		<cfset LTIrate = 0>
	</cfif>
	
	
	<cfset ctr = ctr+1>
	<!--- <cfif buname neq "plc"> --->
	<cfscript>
	SpreadsheetSetCellValue(theSheet,"#buname#",#execctr#,1);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowhr)#",#execctr#,2); 
	SpreadsheetSetCellValue(theSheet,"#airgoal#",#execctr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(airrate,"0.00")#",#execctr#,4);
	SpreadsheetSetCellValue(theSheet,"#TRIRgoal#",#execctr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(TRIRrate,"0.00")#",#execctr#,6);
	SpreadsheetSetCellValue(theSheet,"#LTIgoal#",#execctr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(LTIrate,"0.000")#",#execctr#,8);
	</cfscript>
		<!--- </cfif> --->
		<cfoutput>
		
	<!--- <cfif buname neq "plc"> --->
		<cfif qrybuid eq 3173>
			<cfif id neq 3240>
				<cfset execctr = execctr+1>
			</cfif>
		<cfelse>
			<cfset execctr = execctr+1>
		</cfif>
	<!--- </cfif> --->
		
		<cfif structkeyexists(manhrstructprev,id)><cfset prevrowhr = manhrstructprev[id]><cfelse><cfset prevrowhr = 0></cfif>
		<cfif structkeyexists(manhrstruct,id)><cfset rowhr = manhrstruct[id]><cfelse><cfset rowhr = 0></cfif>
		
		<cfif structkeyexists(targetstructair,id)><cfset airgoal = targetstructair[id]><cfelse><cfset airgoal = "Not Set"></cfif>
		<cfif structkeyexists(AIRstructprev,id)><cfset useairprev = AIRstructprev[id]><cfelse><cfset useairprev = 0></cfif>
		<cfif structkeyexists(AIRstruct,id)><cfset useair = AIRstruct[id]><cfelse><cfset useair = 0></cfif>
		<cfif prevrowhr gt 0 and useairprev gt 0>
			<cfset prevairrate = (useairprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevairrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useair gt 0>
			<cfset airrate = (useair*200000)/rowhr>
		<cfelse>
			<cfset airrate = 0>
		</cfif>
		
		
		<cfif structkeyexists(targetstructtrir,id)><cfset TRIRgoal = targetstructtrir[id]><cfelse><cfset TRIRgoal = "Not Set"></cfif>
		<cfif structkeyexists(TRIRstructprev,id)><cfset useTRIRprev = TRIRstructprev[id]><cfelse><cfset useTRIRprev = 0></cfif>
		<cfif structkeyexists(TRIRstruct,id)><cfset useTRIR = TRIRstruct[id]><cfelse><cfset useTRIR = 0></cfif>
		<cfif prevrowhr gt 0 and useTRIRprev gt 0>
			<cfset prevTRIRrate = (useTRIRprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevTRIRrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useTRIR gt 0>
			<cfset TRIRrate = (useTRIR*200000)/rowhr>
		<cfelse>
			<cfset TRIRrate = 0>
		</cfif>
		
		
	<cfif structkeyexists(targetstructlti,id)><cfset LTIgoal = targetstructlti[id]><cfelse><cfset LTIgoal = "Not Set"></cfif>
	<cfif structkeyexists(LTIstructprev,id)><cfset useLTIprev = LTIstructprev[id]><cfelse><cfset useLTIprev = 0></cfif>
	<cfif structkeyexists(LTIstruct,id)><cfset useLTI = LTIstruct[id]><cfelse><cfset useLTI = 0></cfif>
	<cfif prevrowhr gt 0 and useLTIprev gt 0>
		<cfset prevLTIrate = (useLTIprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevLTIrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useLTI gt 0>
		<cfset LTIrate = (useLTI*200000)/rowhr>
	<cfelse>
		<cfset LTIrate = 0>
	</cfif>
	<!--- <cfif buname neq "plc"> --->
		<cfif qrybuid eq 3173>
			<cfif id neq 3240>
	<cfscript>
	SpreadsheetSetCellValue(theSheet,"#name#",#execctr#,1);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowhr)#",#execctr#,2); 
	SpreadsheetSetCellValue(theSheet,"#airgoal#",#execctr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(airrate,"0.00")#",#execctr#,4);
	SpreadsheetSetCellValue(theSheet,"#TRIRgoal#",#execctr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(TRIRrate,"0.00")#",#execctr#,6);
	SpreadsheetSetCellValue(theSheet,"#LTIgoal#",#execctr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(LTIrate,"0.000")#",#execctr#,8);
	</cfscript>
		</cfif>
		<cfelse>
		<cfscript>
	SpreadsheetSetCellValue(theSheet,"#name#",#execctr#,1);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowhr)#",#execctr#,2); 
	SpreadsheetSetCellValue(theSheet,"#airgoal#",#execctr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(airrate,"0.00")#",#execctr#,4);
	SpreadsheetSetCellValue(theSheet,"#TRIRgoal#",#execctr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(TRIRrate,"0.00")#",#execctr#,6);
	SpreadsheetSetCellValue(theSheet,"#LTIgoal#",#execctr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(LTIrate,"0.000")#",#execctr#,8);
	</cfscript>
	</cfif>
	<!--- </cfif> --->
		</cfoutput>
	</cfoutput>
<cfoutput>
		<cfif structkeyexists(manhrstructprev,"Global")><cfset prevrowhr = manhrstructprev["Global"]><cfelse><cfset prevrowhr = 0></cfif>
		<cfif structkeyexists(manhrstruct,"Global")><cfset rowhr = manhrstruct["Global"]><cfelse><cfset rowhr = 0></cfif>
		
		<cfif structkeyexists(targetstructair,"Global")><cfset airgoal = targetstructair["Global"]><cfelse><cfset airgoal = "Not Set"></cfif>
		<cfif structkeyexists(AIRstructprev,"Global")><cfset useairprev = AIRstructprev["Global"]><cfelse><cfset useairprev = 0></cfif>
		<cfif structkeyexists(AIRstruct,"Global")><cfset useair = AIRstruct["Global"]><cfelse><cfset useair = 0></cfif>
		<cfif prevrowhr gt 0 and useairprev gt 0>
			<cfset prevairrate = (useairprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevairrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useair gt 0>
			<cfset airrate = (useair*200000)/rowhr>
		<cfelse>
			<cfset airrate = 0>
		</cfif>
		
		
		<cfif structkeyexists(targetstructtrir,"Global")><cfset TRIRgoal = targetstructtrir["Global"]><cfelse><cfset TRIRgoal = "Not Set"></cfif>
		<cfif structkeyexists(TRIRstructprev,"Global")><cfset useTRIRprev = TRIRstructprev["Global"]><cfelse><cfset useTRIRprev = 0></cfif>
		<cfif structkeyexists(TRIRstruct,"Global")><cfset useTRIR = TRIRstruct["Global"]><cfelse><cfset useTRIR = 0></cfif>
		<cfif prevrowhr gt 0 and useTRIRprev gt 0>
			<cfset prevTRIRrate = (useTRIRprev*200000)/prevrowhr>
		<cfelse>
			<cfset prevTRIRrate = 0>
		</cfif>
		<cfif rowhr gt 0 and useTRIR gt 0>
			<cfset TRIRrate = (useTRIR*200000)/rowhr>
		<cfelse>
			<cfset TRIRrate = 0>
		</cfif>
		
		
	<cfif structkeyexists(targetstructlti,"Global")><cfset LTIgoal = targetstructlti["Global"]><cfelse><cfset LTIgoal = "Not Set"></cfif>
	<cfif structkeyexists(LTIstructprev,"Global")><cfset useLTIprev = LTIstructprev["Global"]><cfelse><cfset useLTIprev = 0></cfif>
	<cfif structkeyexists(LTIstruct,"Global")><cfset useLTI = LTIstruct["Global"]><cfelse><cfset useLTI = 0></cfif>
	<cfif prevrowhr gt 0 and useLTIprev gt 0>
		<cfset prevLTIrate = (useLTIprev*200000)/prevrowhr>
	<cfelse>
		<cfset prevLTIrate = 0>
	</cfif>
	<cfif rowhr gt 0 and useLTI gt 0>
		<cfset LTIrate = (useLTI*200000)/rowhr>
	<cfelse>
		<cfset LTIrate = 0>
	</cfif>
	<cfset execctr = execctr+1>
		<cfscript>
	SpreadsheetSetCellValue(theSheet,"Amec Foster Wheeler Overall",#execctr#,1);
	SpreadsheetSetCellValue(theSheet,"#numberformat(rowhr)#",#execctr#,2); 
	SpreadsheetSetCellValue(theSheet,"#airgoal#",#execctr#,3);
	SpreadsheetSetCellValue(theSheet,"#numberformat(airrate,"0.00")#",#execctr#,4);
	SpreadsheetSetCellValue(theSheet,"#TRIRgoal#",#execctr#,5);
	SpreadsheetSetCellValue(theSheet,"#numberformat(TRIRrate,"0.00")#",#execctr#,6);
	SpreadsheetSetCellValue(theSheet,"#LTIgoal#",#execctr#,7);
	SpreadsheetSetCellValue(theSheet,"#numberformat(LTIrate,"0.000")#",#execctr#,8);
	</cfscript>
	
</cfoutput>

 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">