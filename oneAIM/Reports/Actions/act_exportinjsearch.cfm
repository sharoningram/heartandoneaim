<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_exportinjsearch.cfm", "exports"))>
<cfset thefilename = "IncidentSearch_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("Incident Search Results","true")>
<cfscript> 
	SpreadsheetSetCellValue(theSheet,"Incident Number",1,1);
	SpreadsheetSetCellValue(theSheet,"#request.bulabellong#",1,2);
	SpreadsheetSetCellValue(theSheet,"#request.oulabellong#",1,3);
	SpreadsheetSetCellValue(theSheet,"Project/Office",1,4);
	SpreadsheetSetCellValue(theSheet,"Work Related",1,5);
	SpreadsheetSetCellValue(theSheet,"Is Near Miss",1,6);
	SpreadsheetSetCellValue(theSheet,"Incident Date",1,7);
	SpreadsheetSetCellValue(theSheet,"Incident Type",1,8);
	SpreadsheetSetCellValue(theSheet,"OSHA Classification",1,9);
	SpreadsheetSetCellValue(theSheet,"Potential Rating",1,10);
	
	SpreadsheetSetCellValue(theSheet,"Short Description",1,11);
	SpreadsheetSetCellValue(theSheet,"Record Status",1,12);
</cfscript>

						<cfset ctr = 1>
						<cfset alreadyshown = "">
						<cfoutput query="getincidents">
						<cfif listfindnocase(alreadyshown,irn) eq 0>
							<cfset showrw = "yes">
							<cfset alreadyshown = listappend(alreadyshown,irn)>
						<cfelse>
							<cfset showrw = "no">
						</cfif>
						
							
						<cfif showrw eq "yes">
						<cfif isfatality eq "Yes">
							<cfset dspinctype = "Fatality">
						<cfelse>
							<cfset dspinctype = IncType>
						</cfif>
						
						<cfset ctr = ctr+1>
						<cfscript> 
							SpreadsheetSetCellValue(theSheet,"#TrackingNum#",#ctr#,1);
							SpreadsheetSetCellValue(theSheet,"#buname#",#ctr#,2);
							SpreadsheetSetCellValue(theSheet,"#ouname#",#ctr#,3);
							SpreadsheetSetCellValue(theSheet,"#Group_Name#",#ctr#,4);
							SpreadsheetSetCellValue(theSheet,"#isworkrelated#",#ctr#,5);
							SpreadsheetSetCellValue(theSheet,"#isnearmiss#",#ctr#,6);
							SpreadsheetSetCellValue(theSheet,"#dateformat(incidentDate,sysdateformatxcel)#",#ctr#,7);
							SpreadsheetSetCellValue(theSheet,"#dspinctype#",#ctr#,8);
							SpreadsheetSetCellValue(theSheet,"#Category#",#ctr#,9);
							SpreadsheetSetCellValue(theSheet,"#PotentialRating#",#ctr#,10);
							
							SpreadsheetSetCellValue(theSheet,"#ShortDesc#",#ctr#,11);
							SpreadsheetSetCellValue(theSheet,"#status#",#ctr#,12);
						</cfscript>
						</cfif>
						</cfoutput>
		
					
		 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
		 <cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">