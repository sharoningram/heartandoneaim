<cfset conhrsstruct = {}>

<cfloop query="getreporthours">
	<cfif not structkeyexists(conhrsstruct,contractorid)>
		<cfset conhrsstruct[contractorid] = conhrs>
	<cfelse>
		<cfset conhrsstruct[contractorid] = conhrsstruct[contractorid]+conhrs>
	</cfif>
</cfloop>

<cfset conFATstruct = {}>

<cfloop query="getfatalities">
	<cfif not structkeyexists(conFATstruct,contractor)>
		<cfset conFATstruct[contractor] = inccnt>
	<cfelse>
		<cfset conFATstruct[contractor] = conFATstruct[contractor]+inccnt>
	</cfif>
</cfloop>

<cfset LTIstruct = {}>
<cfset RWCstruct = {}>
<cfset MTCstruct = {}>
<cfset FAstruct = {}>


<cfloop query="getcfdincs">
	<cfswitch expression="#OSHAclass#">
		<cfcase value="1">
			<cfif not structkeyexists(FAstruct,contractor)>
				<cfset FAstruct[contractor] = inccnt>
			<cfelse>
				<cfset FAstruct[contractor] = FAstruct[contractor] + inccnt>
			</cfif>
		</cfcase>
		<cfcase value="2">
			<cfif not structkeyexists(LTIstruct,contractor)>
				<cfset LTIstruct[contractor] = inccnt>
			<cfelse>
				<cfset LTIstruct[contractor] = LTIstruct[contractor] + inccnt>
			</cfif>
		</cfcase>
		<cfcase value="3">
			<cfif not structkeyexists(MTCstruct,contractor)>
				<cfset MTCstruct[contractor] = inccnt>
			<cfelse>
				<cfset MTCstruct[contractor] = MTCstruct[contractor] + inccnt>
			</cfif>
		</cfcase>
		<cfcase value="4">
			<cfif not structkeyexists(RWCstruct,contractor)>
				<cfset RWCstruct[contractor] = inccnt>
			<cfelse>
				<cfset RWCstruct[contractor] = RWCstruct[contractor] + inccnt>
			</cfif>
		</cfcase>
	</cfswitch>
</cfloop>

<cfset OIDstruct = {}>

<cfloop query="getoi">
	<cfif not structkeyexists(OIDstruct,contractor)>
		<cfset OIDstruct[contractor] = inccnt>
	<cfelse>
		<cfset OIDstruct[contractor] = OIDstruct[contractor] + inccnt>
	</cfif>
</cfloop>