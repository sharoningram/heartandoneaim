<!--- <cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0> --->

<cfset accessglist = "">
<cfquery name="getrights" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, Users.Status, Users.Entered_By,  UserRoles.UserRole, 
                         UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#cookie.useremail#">) AND (Users.Status = 1)
order by users.UserId
</cfquery>
<cfloop list="#request.userlevel#" index="i">
	<cfswitch expression = "#i#">
		<cfcase value="Global Admin,Senior Executive View">
			<cfset accessglist = "all">
		</cfcase>
		<cfcase value="BU Admin,Senior Reviewer">
			<cfquery name="getrights" datasource="#request.dsn#">
				SELECT        UserRoles.AssignedLocs
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) AND (Users.Status = 1) and UserRoles.UserRole = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">
				order by users.UserId
			</cfquery>
			<cfquery name="getgnums" datasource="#request.dsn#">
				select group_number 
				from groups
				where business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getrights.AssignedLocs)#" list="Yes">)
			</cfquery>
			<cfloop query="getgnums">
				<cfif listfind(accessglist,group_number) eq 0>
					<cfset accessglist = listappend(accessglist,group_number)>
				</cfif>
			</cfloop>
		</cfcase>
		<cfcase value="Reviewer,Reports Only,OU Admin">
			<cfquery name="getrights" datasource="#request.dsn#">
				SELECT        UserRoles.AssignedLocs
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) AND (Users.Status = 1) and UserRoles.UserRole = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">
				order by users.UserId
			</cfquery>
			<cfquery name="getgnums" datasource="#request.dsn#">
				select group_number 
				from groups
				where ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getrights.AssignedLocs)#" list="Yes">)
			</cfquery>
			<cfloop query="getgnums">
				<cfif listfind(accessglist,group_number) eq 0>
					<cfset accessglist = listappend(accessglist,group_number)>
				</cfif>
			</cfloop>
		</cfcase>
		
		
	</cfswitch>
</cfloop>