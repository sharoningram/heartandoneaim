<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>
<cfparam name="sortordby" default="incnum">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfparam name="INCNM" default="no">
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="chartvalue" default="">
<cfparam name="charttype" default="No">
<cfparam name="incandor" default="And">
<cfparam name="potentialrating" default="All">
<cfparam name="droppedobj" default="All">
<cfset qryproject = projectoffice>

<cfif listfindnocase(projectoffice,"All") eq 0>
	<cfif assocg eq "Yes">
		<cfset gtoview = "">
			<cfloop list="#projectoffice#" index="i">
				<cfif listfind(gtoview,i) eq 0>
					<cfset gtoview = listappend(gtoview,i)>
				</cfif>
				
				<cfquery name="getassocg" datasource="#request.dsn#">
					SELECT        GroupNumber1, GroupNumber2
					FROM            GroupAssociations
					WHERE        (GroupNumber1 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">) OR
                         (GroupNumber2 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
				</cfquery>
				<cfloop query="getassocg">
					<cfif listfind(gtoview,GroupNumber1) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber1)>
					</cfif>
					<cfif listfind(gtoview,GroupNumber2) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber2)>
					</cfif>
				</cfloop>
			</cfloop>
		
		<cfset qryproject = gtoview>
	</cfif>
</cfif>

<cfset contractlist = "">
<cfif trim(frmmgdcontractor) neq ''>
<cfif listfindnocase(frmmgdcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmmgdcontractor)>
</cfif>
</cfif>
<cfif trim(frmjv) neq ''>
<cfif listfindnocase(frmjv,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmjv)>
</cfif>
</cfif>
<cfif trim(frmsubcontractor) neq ''>
<cfif listfindnocase(frmsubcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmsubcontractor)>
</cfif>
</cfif>




<cfparam name="searchname" default="">
<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.dsn#">
		select UserReportID
		from UserSavedReports
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#"> and reportType = 'Diamond'
	</cfquery>
	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.dsn#">
			delete from UserSavedReports
			where UserReportID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.UserReportID#">
		</cfquery>
	</cfif>
	<!--- <cfif getnamedsearch.recordcount gt 0>
		<cfquery name="getmax" datasource="#request.dsn#">
			select max(savectr) as savectr
			from UserSavedReports
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
		</cfquery>
		<cfset newsave = getmax.savectr+1>
	<cfelse>
		<cfset newsave = 1>
	</cfif> --->
	
	
		<cfquery name="addsearch" datasource="#request.dsn#">
			insert into UserSavedReports (SearchName, UserID, incyear, invlevel, bu, ou, bs, project, siteoffice, mgdcontractor, jv, subcon, clientid, locdetail, countryid, 
                         incassigned, wronly, primarytype, secondtype, hipoony, oshaclass, oidef, seriousinj, secinccats, eic, damagesrc, startdate, enddate, SaveCtr,incnm,reportType,charttype,chartvalue,assocgroups,droppedobj)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incyear#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmmgdcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmjv#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmsubcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclient#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#locdetail#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmcountryid#">, 
                         <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmincassignto#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wronly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#primarytype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secondtype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#hipoonly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oshaclass#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oidef#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secinccats#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#eic#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#damagesrc#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#newsave#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incnm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Diamond">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#charttype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#chartvalue#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#assocg#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#droppedobj#">)
		</cfquery>
	
</cfif>
</cfif>



<cfif isdefined("dosearch") and trim(dosearch) neq ''>
<!--- <cfif fusebox.fuseaction does not contain "print"> --->
<cfparam name="searchname" default="">
<script type="text/javascript">
function submitsfrm(){
// alert(document.userlookupfrm.searchitem.value);
var oktosubmit = "yes"

if(document.savesearchfrm.searchname.value==""){
	var oktosubmit = "no";
	 document.savesearchfrm.searchname.style.border = "2px solid F00000";
	
}
else{
document.savesearchfrm.searchname.style.border = "1px solid 5f2468";
}



if (oktosubmit=="no"){
	document.getElementById("reqfldsincfrm").style.display='';
	return false;
}  
// alert(oktosubmit);
}

function submitsrtfrm(srtcol){
document.sortresfrm.sortordby.value=srtcol;
document.sortresfrm.submit();

}

</script>
<cfparam name="newsave" default="1">
<cfparam name="frompg" default="srchfrm">
<cfif fusebox.fuseaction neq "exportrundiamond">
<cfform action="#self#?fuseaction=reports.diamond" method="post" name="backtosearch">
				<cfoutput>
				
		<input type="hidden" name="fromedit" value="yes">		
		<input type="hidden" name="dosearch" value="redir">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		

<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
	
	<input type="hidden" name="assocg" value="#assocg#">
	<input type="hidden" name="charttype" value="#charttype#">
	<input type="hidden" name="chartvalue" value="#chartvalue#">
	<input type="hidden" name="incandor" value="#incandor#">
	<input type="hidden" name="potentialrating" value="#potentialrating#">		
<input type="hidden" name="droppedobj" value="#droppedobj#">	
	</cfoutput>
	</cfform>
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<td colspan="2" class="bodyTextWhite">Criteria</td>
	</tr>

		<tr>
			<td align="center" bgcolor="ffffff" colspan="2">
			<cfform action="#self#?fuseaction=#attributes.xfa.rundiamond#" method="post" name="savesearchfrm" onsubmit="return submitsfrm();">
				<cfoutput>
				
				
				
	<input type="hidden" name="dosearch" value="#dosearch#">			
<input type="hidden" name="reptype" value="#fusebox.fuseaction#">		
<input type="hidden" name="savefrm" value="yes">
<input type="hidden" name="incyear" value="#incyear#">
<input type="hidden" name="invlevel" value="#invlevel#">
<input type="hidden" name="bu" value="#bu#">
<input type="hidden" name="ou" value="#ou#">
<input type="hidden" name="INCNM" value="#INCNM#">
<input type="hidden" name="businessstream" value="#businessstream#">
<input type="hidden" name="projectoffice" value="#projectoffice#">
<input type="hidden" name="site" value="#site#">
<input type="hidden" name="frmmgdcontractor" value="#frmmgdcontractor#">
<input type="hidden" name="frmjv" value="#frmjv#">
<input type="hidden" name="frmsubcontractor" value="#frmsubcontractor#">
<input type="hidden" name="frmclient" value="#frmclient#">
<input type="hidden" name="locdetail" value="#locdetail#">
<input type="hidden" name="frmcountryid" value="#frmcountryid#">
<input type="hidden" name="frmincassignto" value="#frmincassignto#">
<input type="hidden" name="wronly" value="#wronly#">
<input type="hidden" name="primarytype" value="#primarytype#">
<input type="hidden" name="secondtype" value="#secondtype#">
<input type="hidden" name="hipoonly" value="#hipoonly#">
<input type="hidden" name="oshaclass" value="#oshaclass#">
<input type="hidden" name="oidef" value="#oidef#">
<input type="hidden" name="seriousinj" value="#seriousinj#">
<input type="hidden" name="secinccats" value="#secinccats#">
<input type="hidden" name="eic" value="#eic#">
<input type="hidden" name="damagesrc" value="#damagesrc#">
<input type="hidden" name="startdate" value="#startdate#">
<input type="hidden" name="enddate" value="#enddate#">
	
	<input type="hidden" name="assocg" value="#assocg#">
	<input type="hidden" name="charttype" value="#charttype#">
	<input type="hidden" name="chartvalue" value="#chartvalue#">	
	<input type="hidden" name="incandor" value="#incandor#">
	<input type="hidden" name="potentialrating" value="#potentialrating#">	
	<input type="hidden" name="droppedobj" value="#droppedobj#">			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<tr id="reqfldsincfrm">
							<td colspan="8" class="redText" align="center">Please fill in all required fields</td>
						</tr>
						<tr>
							<td align="center" colspan="8" class="formlable"><strong>Location Detail Parameters</strong></td>
						</tr>
						<tr>
						<td class="formlable" align="left" width="12%"><strong>#request.bulabellong#:</strong></td><!---  --->
						<td class="bodyTextGrey" width="12%"><cfif listfind(bu,"all") gt 0>All<br></cfif>
								<cfloop query="getbus">
										<cfif listfind(bu,id) gt 0>#Name#<br></cfif>
										</cfloop></td>
						<td class="formlable" align="left" width="12%"><strong>#request.oulabellong#:</strong></td>
						<td class="bodyTextGrey" width="12%" id="ouselect"><cfif listfind(ou,"all") gt 0>All<br></cfif>
								<cfloop query="getOU">
										<cfif listfind(ou,id) gt 0>#Name#<br></cfif>
										</cfloop>
						</td>
					
					
					
						<td class="formlable" align="left" width="12%"><strong>Business Stream:</strong></td>
						<td class="bodyTextGrey" width="12%" id="bsselect"><cfif listfind(businessstream,"all") gt 0>All<br></cfif>
								<cfloop query="getBS">
										<cfif listfind(businessstream,id) gt 0>#Name#<br></cfif>
										</cfloop></td>
						
					<td class="formlable" align="left" width="12%"><strong>Project/Office:</strong><cfif assocg eq "yes"><br>Include associated groups (if any): Yes</cfif></td>
						<td class="bodyTextGrey" width="12%" id="poselect"><cfif listfind(projectoffice,"all") gt 0>All<br></cfif>
								<cfloop query="getgroups">
										<cfif listfind(projectoffice,group_number) gt 0>#group_Name#<br></cfif>
										</cfloop></td>
					</tr>
					
					
					
					
					
					<tr>
					<td class="formlable" align="left" width="12%"><strong>Site/Office Name:</strong></td>
						<td class="bodyTextGrey" width="12%" id="soselect"><cfif listfind(site,"all") gt 0>All<br></cfif>
								<cfloop query="getsites">
										<cfif listfind(site,GroupLocID) gt 0>#sitename#<br></cfif>
										</cfloop></td>
										
						<td class="formlable" align="left" width="12%"><strong>Location Details:</strong></td>
						<td class="bodyTextGrey" width="12%" id="ldselect"><cfif listfind(locdetail,"all") gt 0>All<br></cfif>
								<cfloop query="getlocdetails">
										<cfif listfind(locdetail,LocationDetailID) gt 0>#LocationDetail#<br></cfif>
										</cfloop></td>
										
							<td class="formlable" align="left" width="12%"><strong>Country:</strong></td>
						<td class="bodyTextGrey" width="12%" id="countryselect"><cfif listfind(frmcountryid,"all") gt 0>All<br></cfif>
								<cfloop query="getcountries">
										<cfif listfind(frmcountryid,CountryID) gt 0>#CountryName#<br></cfif>
										</cfloop></td>  			
						<td class="formlable" align="left" width="12%"><strong>Client:</strong></td>
						<td class="bodyTextGrey" width="12%" id="clientselect"><cfif listfind(frmclient,"all") gt 0>All<br></cfif>
								<cfloop query="getclients">
										<cfif listfind(frmclient,ClientID) gt 0>#ClientName#<br></cfif>
										</cfloop></td>				
						
					
					
					</tr>
					<tr>
					
						<td class="formlable" align="left" width="12%"><strong>Incident Assigned To:</strong></td>
						<td class="bodyTextGrey" width="12%" id="iatselect"><cfif listfind(frmincassignto,"all") gt 0>All<br></cfif>
								<cfloop query="getincassignedto">
										<cfif listfind(frmincassignto,IncAssignedID) gt 0>#IncAssignedTo#<br></cfif>
										</cfloop></td>
							<td class="formlable" align="left" width="12%"><strong>Subcontractor:</strong></td>
						<td class="bodyTextGrey" width="12%" id="subselect"><cfif listfind(frmsubcontractor,"all") gt 0>All<br></cfif>
								<cfloop query="getcontractors">
										<cfif cotype eq "sub"><cfif listfind(frmsubcontractor,ContractorID) gt 0>#ContractorName#<br></cfif></cfif>
										</cfloop></td>
										
									<td class="formlable" align="left" width="12%"><strong>Managed Contractor:</strong></td>
						<td class="bodyTextGrey" width="12%" id="mcselect"><cfif listfind(frmmgdcontractor,"all") gt 0>All<br></cfif>
								<cfloop query="getcontractors">
										<cfif cotype eq "mgd"><cfif listfind(frmmgdcontractor,ContractorID) gt 0>#ContractorName#<br></cfif></cfif>
										</cfloop></td>
				
				
				
						<td class="formlable" align="left" width="12%"><strong>Joint Venture:</strong></td>
						<td class="bodyTextGrey" width="12%" id="jvselect"><cfif listfind(frmjv,"all") gt 0>All<br></cfif>
								<cfloop query="getcontractors">
										<cfif cotype eq "jv"><cfif listfind(frmjv,ContractorID) gt 0>#ContractorName#<br></cfif></cfif>
										</cfloop></td>	
										
					</tr>
					
					<tr>
							<td align="center" colspan="8" class="formlable"><strong>Incident Type Parameters</strong></td>
						</tr>
					<tr>
					
					
						
						
					<td class="formlable" align="left" width="12%"><strong>Primary Incident type:</strong><cfif secondtype eq "yes"><br>Secondary incident type included</cfif></td>
						<td class="bodyTextGrey" width="12%" id="ldselect"><cfif listfind(primarytype,"all") gt 0>All<br></cfif>
								<cfloop query="getincidenttypes">
										<cfif listfind(primarytype,IncTypeID) gt 0>#IncType#<br></cfif>
										</cfloop></td>
					<!--- <td class="bodyTextGrey"  colspan="2" align="center"><table cellpadding="0" cellspacing="0" border="0"><tr><td colspan="2" class="bodyTextGrey"  align="center"><strong>#incandor#</strong></td></tr></table></td>
				
				
					<td class="formlable" align="left" width="12%"><strong>Secondary incident type:</strong></td>
						<td class="bodyTextGrey" width="12%" id="ldselect"><cfif listfind(secondtype,"all") gt 0>All<br></cfif>
								<cfloop query="getincidenttypes">
										<cfif listfind(secondtype,IncTypeID) gt 0>#IncType#<br></cfif>
										</cfloop></td> --->
										
											<td class="formlable" align="left" width="12%"><strong>Work Related Incidents</strong></td>
						<td class="bodyTextGrey" width="12%" id="clientselect"><cfif wronly eq "yes">Work related only<cfelseif wronly eq "both">Include non-work related<cfelseif wronly eq "no">Non-work related only</cfif></td><!--- 
						<td class="formlable" align="left" width="12%"><strong>Only include High Potential incidents?</strong></td>
						<td class="bodyTextGrey" width="12%" id="clientselect">#hipoonly#</td> --->
						<td class="formlable" align="left" width="12%"><strong>Near Miss Incidents</strong></td>
						<td class="bodyTextGrey" width="12%" colspan="3"><cfif incnm eq "no">Exclude near miss<cfelseif incnm eq "yes">Include near miss<cfelseif incnm eq "nmonly">Near miss only</cfif></td>
					</tr>
					 
					
					
					<tr>
					
					
					<td class="formlable" align="left" width="12%"><strong>Investigation Level:</strong></td>
						<td class="bodyTextGrey" width="12%" id="clientselect">#invlevel#</td>
						<td class="formlable" align="left" width="12%"><strong>Potential&nbsp;Rating:</strong></td><!--- onchange="this.style.border='1px solid 5f2468';" --->
						<td class="bodyTextGrey" width="12%">#replace(potentialrating,",",", ","All")#</td>	
						<td class="formlable" align="left" width="12%"><strong>Dropped Objects:</strong></td>
						<td class="bodyTextGrey" width="12%">#droppedobj#</td>
						<td></td>
						<td></td>	
						<!--- <td class="formlable" align="left" width="12%" id="oshalable"><strong>OSHA Classification:</strong></td>
						<td class="bodyTextGrey" width="12%" id="oshafld"><cfif listfind(oshaclass,"all") gt 0>All<br></cfif>
								<cfloop query="getoshaclass">
										<cfif listfind(oshaclass,catid) gt 0>#category#<br></cfif>
										</cfloop></td>
							<td class="formlable" align="left" width="12%" id="oilable"><strong>Occupational Illness Definition:</strong></td><!--- onchange="this.style.border='1px solid 5f2468';" --->
						<td class="bodyTextGrey" width="12%" id="oifld"><cfif listfindnocase(oidef,"all") gt 0>All<br></cfif>
										<cfloop query="getoidef"><cfif listfindnocase(oidef,OIdefID) gt 0>#OIdef#<br></cfif></cfloop>
										</td> --->

					</tr>
					

						<tr>
						<td class="formlable" align="left" width="12%" id="oshalable"><strong>OSHA Classification:</strong></td>
						<td class="bodyTextGrey" width="12%" id="oshafld"><cfif listfind(oshaclass,"all") gt 0>All<br></cfif>
								<cfloop query="getoshaclass">
										<cfif listfind(oshaclass,catid) gt 0>#category#<br></cfif>
										</cfloop></td>
							<td class="formlable" align="left" width="12%" id="oilable"><strong>Occupational Illness Definition:</strong></td><!--- onchange="this.style.border='1px solid 5f2468';" --->
						<td class="bodyTextGrey" width="12%" id="oifld"><cfif listfindnocase(oidef,"all") gt 0>All<br></cfif>
										<cfloop query="getoidef"><cfif listfindnocase(oidef,OIdefID) gt 0>#OIdef#<br></cfif></cfloop>
										</td>
					<td class="formlable" align="left" width="12%" id="serinjlable"><strong>Is this injury classified as a serious injury?</strong></td>
						<td class="bodyTextGrey" width="12%" id="serinjfld" colspan="3">#seriousinj#</td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="12%" id="siclable"><strong>Security Incident Category:</strong></td>
						<td class="bodyTextGrey" width="12%" id="sicfld"><cfif listfindnocase(secinccats,"all") gt 0>All<br></cfif>
					<cfloop query="getsecinccats"><cfif listfindnocase(secinccats,SecIncCatID) gt 0>#secinccategory#<br></cfif></cfloop>
					</td>
						<td class="formlable" align="left" width="12%" id="eiclable"><strong>Environmental Incident Category:</strong></td>
						<td class="bodyTextGrey" width="12%" id="eicfld"><cfif listfindnocase(eic,"all") gt 0>All<br></cfif>
					<cfloop query="getenvinccats"><cfif listfindnocase(eic,EnvIncCateID) gt 0>#EnvIncCategory#<br></cfif></cfloop>
					</td>
						
						
					<td class="formlable" align="left" width="12%" id="sodlable"><strong>Source of Damage:</strong></td><!---  onchange="this.style.border='1px solid 5f2468';" --->
						<td class="bodyTextGrey" id="sodfld" colspan="3"><cfif listfindnocase(damagesrc,"all") gt 0>All<br></cfif>
						<cfloop query="getdamagesource"><cfif listfindnocase(damagesrc,DamageSourceID) gt 0>#DamageSource#<br></cfif></cfloop>
										</td>
									
						
						
					
					</tr>
						<tr>
							<td align="center" colspan="8" class="formlable"><strong>Date Parameters</strong></td>
						</tr>
					
					
					
					<tr>
					
							<td class="formlable" align="left" width="12%"><strong>Date Range by Year:</strong></td>
						<td class="bodyTextGrey" width="12%" id="ldselect">#incyear#</td>
					
					
					<td class="formlable" align="left" width="12%"><strong>Start Date:</strong></td>
						<td class="bodyTextGrey" width="12%" id="ldselect">#dateformat(startdate,sysdateformat)#</td>
						<td class="formlable" align="left" width="12%"><strong>End Date:</strong></td>
						<td class="bodyTextGrey" width="12%" id="clientselect">#dateformat(enddate,sysdateformat)#</td>
<td colspan="2"></td>
					</tr>
						
						
						<!--- <cfif trim(searchname) eq ''> ---><cfif fusebox.fuseaction does not contain "print">
						<tr><!--- onkeypress="this.style.border='1px solid 5f2468';" --->
						<td colspan="4"  class="formlable"><strong>Assign Name to Filter Criteria:</strong><br><em>Enter a name ONLY if you wish to save this report filter on your "Reports" page.</em></td>
						<td colspan="4" ><input type="text" name="searchname" value="#searchname#" class="selectgen" size="60" maxlength="75" ></td>
					</tr>
					
					<tr>
						<td colspan="8" align="center" class="bodyTextGrey">
						<input type="submit" class="selectGenBTN" value="Save Criteria">&nbsp;&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Edit Criteria" onclick="document.backtosearch.submit();"></td>
					</tr></cfif>
					<!--- <cfelse>
					<tr>
						<td  class="formlable"><strong>Report Filter saved as:</strong></td>
						<td colspan="3" class="bodyTextGrey">#searchname#<cfif newsave gt 1> (#newsave#)</cfif></td>
					</tr>
					</cfif> --->
				</table>
				</cfoutput>
				</cfform>
			</td>
		</tr>

</table>
	<script type="text/javascript">
	document.getElementById("reqfldsincfrm").style.display='none';
	
	<cfif listfind(primarytype,1) eq 0 and listfind(primarytype,5) eq 0 and listfind(secondtype,1) eq 0 and listfind(secondtype,5) eq 0>
	// document.getElementById("iocvrow").style.display='none';
	document.getElementById("oshalable").style.display='none';
	document.getElementById("oshafld").style.display='none';
	document.getElementById("oilable").style.display='none';
	document.getElementById("oifld").style.display='none';
	document.getElementById("serinjlable").style.display='none';
	document.getElementById("serinjfld").style.display='none';
	
	     
</cfif>
<cfif listfind(primarytype,3) eq 0 and  listfind(secondtype,3) eq 0>
		//	document.getElementById("adcvrow").style.display='none';
			document.getElementById("sodlable").style.display='none';
			document.getElementById("sodfld").style.display='none';
</cfif>
<cfif listfind(primarytype,2) eq 0 and  listfind(secondtype,2) eq 0>
		//	document.getElementById("envcvrow").style.display='none';
			document.getElementById("eiclable").style.display='none';
			document.getElementById("eicfld").style.display='none';
</cfif>
<cfif listfind(primarytype,4) eq 0 and  listfind(secondtype,4) eq 0>
		//	document.getElementById("seccvrow").style.display='none';
			document.getElementById("siclable").style.display='none';
			document.getElementById("sicfld").style.display='none';
			 
</cfif>
</script>
</cfif>
</cfif><!--- </cfif> --->



<cfparam name="charttype" default="table">
<cfparam name="chartvalue" default="">
<cfif trim(chartvalue) neq ''>
	<cfloop list="#chartvalue#" index="i">
		<cfswitch expression="#i#">
			<cfcase value="TIO">
				<br>
				<cfinclude template="../queries/qry_getTIO.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_tio.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_tio.cfm">
				</cfif>
			</cfcase>
			<cfcase value="weekday">
				<br>
				<cfinclude template="../queries/qry_getweekday.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_weekday.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_weekday.cfm">
				</cfif>
			</cfcase>
			<cfcase value="weather">
				<br>
				<cfinclude template="../queries/qry_getweather.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_weather.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_weather.cfm">
				</cfif>
			</cfcase>
			<cfcase value="statbody">
				<br>
				<cfinclude template="../queries/qry_getstatbody.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_statbody.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_statbody.cfm">
				</cfif>
			</cfcase>
			<cfcase value="immcause">
				<br>
				<cfinclude template="../queries/qry_getimmcause.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_immcause.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_immcause.cfm">
				</cfif>
			</cfcase>
			<cfcase value="immcausesub">
				<br>
				<cfinclude template="../queries/qry_getimmcausesub.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_immcausesub.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_immcausesub.cfm">
				</cfif>
			</cfcase>
			<cfcase value="immcausesub10">
				<br>
				<cfinclude template="../queries/qry_getimmcausesub10.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_immcausesub10.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_immcausesub10.cfm">
				</cfif>
			</cfcase>
			<cfcase value="rootcause">
				<br>
				<cfinclude template="../queries/qry_getrootcause.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_rootcause.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_rootcause.cfm">
				</cfif>
			</cfcase>
			<cfcase value="rootcausesub">
				<br>
				<cfinclude template="../queries/qry_getrootcausesub.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_rootcausesub.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_rootcausesub.cfm">
				</cfif>
			</cfcase>
			<cfcase value="rootcausesub10">
				<br>
				<cfinclude template="../queries/qry_getrootcausesub10.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_rootcausesub10.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_rootcausesub10.cfm">
				</cfif>
			</cfcase>
			<cfcase value="seb">
				<br>
				<cfinclude template="../queries/qry_getseb.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_seb.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_seb.cfm">
				</cfif>
			</cfcase>
			<cfcase value="lsb">
				<br>
				<cfinclude template="../queries/qry_getlsb.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_lsb.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_lsb.cfm">
				</cfif>
			</cfcase>
			<cfcase value="bymonth">
				<br>
				<cfinclude template="../queries/qry_bymonth.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_bymonth.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_bymonth.cfm">
				</cfif>
			</cfcase>
		</cfswitch>
		<cfif listfindnocase(primarytype,1) gt 0 or  listfindnocase(secondtype,1) gt 0 or listfindnocase(primarytype,5) gt 0 or  listfindnocase(secondtype,5) gt 0 >
		<cfswitch expression="#i#">
			<cfcase value="hazard">
				<br>
				<cfinclude template="../queries/qry_hazard.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_hazard.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_hazard.cfm">
				</cfif>
			</cfcase>
			<cfcase value="age">
				<br>
				<cfinclude template="../queries/qry_agemd.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_agemd.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_agemd.cfm">
				</cfif>
			</cfcase>
			<cfcase value="occupation">
				<br>
				<cfinclude template="../queries/qry_mdoccupation.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdoccupation.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdoccupation.cfm">
				</cfif>
			</cfcase>
			<cfcase value="gender">
				<br>
				<cfinclude template="../queries/qry_mdgender.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdgender.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdgender.cfm">
				</cfif>
			</cfcase>
			<cfcase value="shift">
				<br>
				<cfinclude template="../queries/qry_mdshift.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdshift.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdshift.cfm">
				</cfif>
			</cfcase>
			<cfcase value="injtype">
				<br>
				<cfinclude template="../queries/qry_mdinjtype.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdinjtype.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdinjtype.cfm">
				</cfif>
			</cfcase>
			<cfcase value="illtype">
				<br>
				<cfinclude template="../queries/qry_mdilltype.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdilltype.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdilltype.cfm">
				</cfif>
			</cfcase>
			<cfcase value="bodypart">
				<br>
				<cfinclude template="../queries/qry_mdbodypart.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdbodypart.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdbodypart.cfm">
				</cfif>
			</cfcase>
			<cfcase value="injcause">
				<br>
				<cfinclude template="../queries/qry_mdinjcause.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdinjcause.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdinjcause.cfm">
				</cfif>
			</cfcase>
			<cfcase value="illnature">
				<br>
				<cfinclude template="../queries/qry_mdillnature.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdillnature.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdillnature.cfm">
				</cfif>
			</cfcase>
		</cfswitch>
		</cfif>
		<cfif listfindnocase(primarytype,2) gt 0 or  listfindnocase(secondtype,2) gt 0>
		<cfswitch expression="#i#">
			<cfcase value="inbreach">
				<br>
				<cfinclude template="../queries/qry_inbreach.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_inbreach.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_inbreach.cfm">
				</cfif>
			</cfcase>
			<cfcase value="polltype">
				<br>
				<cfinclude template="../queries/qry_polltype.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_polltype.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_polltype.cfm">
				</cfif>
			</cfcase>
			<cfcase value="substance">
				<br>
				<cfinclude template="../queries/qry_pollsubstance.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_pollsubstance.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_pollsubstance.cfm">
				</cfif>
			</cfcase>
			<cfcase value="releasesrc">
				<br>
				<cfinclude template="../queries/qry_pollreleasesrc.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_pollreleasesrc.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_pollreleasesrc.cfm">
				</cfif>
			</cfcase>
			<cfcase value="releasedur">
				<br>
				<cfinclude template="../queries/qry_pollreleasedur.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_pollreleasedur.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_pollreleasedur.cfm">
				</cfif>
			</cfcase>
			<cfcase value="enviro">
				<br>
				<cfinclude template="../queries/qry_pollenviro.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_pollenviro.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_pollenviro.cfm">
				</cfif>
			</cfcase>
			</cfswitch>
			</cfif>
		<cfif listfindnocase(primarytype,3) gt 0 or  listfindnocase(secondtype,3) gt 0>
			<cfswitch expression="#i#">
			<cfcase value="damagenature">
				<br>
				<cfinclude template="../queries/qry_damagenature.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_damagenature.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_damagenature.cfm">
				</cfif>
			</cfcase>
			<cfcase value="proptype">
				<br>
				<cfinclude template="../queries/qry_mdproptype.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdproptype.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdproptype.cfm">
				</cfif>
			</cfcase>
			</cfswitch>
			</cfif>
			<cfif listfindnocase(primarytype,4) gt 0 or  listfindnocase(secondtype,4) gt 0>
			<cfswitch expression="#i#">
			
			<cfcase value="secthreat">
				<br>
				<cfinclude template="../queries/qry_mdsecthreat.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdsecthreat.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdsecthreat.cfm">
				</cfif>
			</cfcase> 
			<cfcase value="whileabroad">
				<br>
				<cfinclude template="../queries/qry_mdwhileabroad.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdwhileabroad.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdwhileabroad.cfm">
				</cfif>
			</cfcase>
			<cfcase value="whichcountry">
				<br>
				<cfinclude template="../queries/qry_mdwhichcountry.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdwhichcountry.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdwhichcountry.cfm">
				</cfif>
			</cfcase>
			<cfcase value="weapontype">
				<br>
				<cfinclude template="../queries/qry_mdweapontype.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdweapontype.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdweapontype.cfm">
				</cfif>
			</cfcase>
			<cfcase value="allnat">
				<br>
				<cfinclude template="../queries/qry_mdnatall.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdnatall.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdnatall.cfm">
				</cfif>
			</cfcase> 
			<cfcase value="natperson">
				<br>
				<cfinclude template="../queries/qry_mdnatperson.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdnatperson.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdnatperson.cfm">
				</cfif>
			</cfcase> 
			
			<cfcase value="natbiz">
				<br>
				<cfinclude template="../queries/qry_mdnatbiz.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdnatbiz.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdnatbiz.cfm">
				</cfif>
			</cfcase>
			<cfcase value="intelpropcat">
				<br>
				<cfinclude template="../queries/qry_mdintelpropcat.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdintelpropcat.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdintelpropcat.cfm">
				</cfif>
			</cfcase>
			<cfcase value="natinfo">
				<br>
				<cfinclude template="../queries/qry_mdnatinfo.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdnatinfo.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdnatinfo.cfm">
				</cfif>
			</cfcase>
			<cfcase value="natsecurity">
				<br>
				<cfinclude template="../queries/qry_mdnatsecurity.cfm">
				<cfif fusebox.fuseaction eq "exportrundiamond">
					<cfinclude template="act_mdnatsecurity.cfm">
				<cfelse>
					<cfinclude template="../displays/dsp_mdnatsecurity.cfm">
				</cfif>
			</cfcase>
			
			</cfswitch>
			</cfif>
		
	</cfloop>
</cfif>

