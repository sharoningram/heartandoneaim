
<cfparam name="sortordby" default="incnum">
<cfparam name="incyear" default="all">
<cfparam name="invlevel" default="all">
<cfparam name="bu" default="all">
<cfparam name="ou" default="all">
<cfif listfindnocase("incbytype",fusebox.fuseaction) gt 0>
<cfparam name="INCNM" default="yes">
<cfelse>
<cfparam name="INCNM" default="no">
</cfif>
<cfparam name="businessstream" default="all">
<cfparam name="projectoffice" default="all">
<cfparam name="site" default="all">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="all">
<cfparam name="locdetail" default="all">
<cfparam name="frmcountryid" default="all">
<cfparam name="frmincassignto" default="all">
<cfparam name="wronly" default="yes">
<cfparam name="primarytype" default="all">
<cfparam name="secondtype" default="all">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="all">
<cfparam name="oidef" default="all">
<cfparam name="seriousinj" default="ALL">
<cfparam name="secinccats" default="all">
<cfparam name="eic" default="all">
<cfparam name="damagesrc" default="all">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="assocg" default="No">
<cfparam name="potentialrating" default="all">
<cfparam name="irnumber" default="">
<cfparam name="keywords" default="">
<cfparam name="incstatus" default="">
<cfset contractlist = "">
<cfif trim(frmmgdcontractor) neq ''>
<cfif listfindnocase(frmmgdcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmmgdcontractor)>
</cfif>
</cfif>
<cfif trim(frmjv) neq ''>
<cfif listfindnocase(frmjv,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmjv)>
</cfif>
</cfif>
<cfif trim(frmsubcontractor) neq ''>
<cfif listfindnocase(frmsubcontractor,"All") eq 0>
	<cfset contractlist = listappend(contractlist,frmsubcontractor)>
</cfif>
</cfif>
<cfset showouonly = "yes">
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0 or listfindnocase(request.userlevel,"Reports Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
	<cfset showouonly = "no">
</cfif>

<cfset qryproject = projectoffice>

<cfif listfindnocase(projectoffice,"All") eq 0>
	<cfif assocg eq "Yes">
		<cfset gtoview = "">
			<cfloop list="#projectoffice#" index="i">
				<cfif listfind(gtoview,i) eq 0>
					<cfset gtoview = listappend(gtoview,i)>
				</cfif>
				
				<cfquery name="getassocg" datasource="#request.dsn#">
					SELECT        GroupNumber1, GroupNumber2
					FROM            GroupAssociations
					WHERE        (GroupNumber1 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">) OR
                         (GroupNumber2 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">)
				</cfquery>
				<cfloop query="getassocg">
					<cfif listfind(gtoview,GroupNumber1) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber1)>
					</cfif>
					<cfif listfind(gtoview,GroupNumber2) eq 0>
						<cfset gtoview = listappend(gtoview,GroupNumber2)>
					</cfif>
				</cfloop>
			</cfloop>
		
		<cfset qryproject = gtoview>
	</cfif>
</cfif>


<cfif trim(startdate) neq ''>
	<!--- <cfset rollstart = "#month(startdate)#/#day(startdate)#/#year(startdate)-2#">
	<cfset fullrollstart = "#month(startdate)#/#day(startdate)#/#year(startdate)-3#"> --->
	<cfset rollstart = "#month(startdate)#/#day(startdate)#/#year(startdate)#">
	<cfset fullrollstart = "#month(startdate)#/#day(startdate)#/#year(startdate)-1#">
<cfelse>
	<cfif listfindnocase(incyear,"All") eq 0>
		
		<cfif incyear eq year(now())>
			<!--- <cfset rollstart = "#month(now())#/1/#year(now())-1#">
			<cfset fullrollstart = "#month(now())#/1/#year(now())-2#"> --->
			<cfset rollstart = "1/1/#incyear#">
			<cfset fullrollstart = "1/1/#incyear-1#">
		<cfelse>
			<cfset rollstart = "1/1/#incyear#">
			<cfset fullrollstart = "1/1/#incyear-1#">
		</cfif>
	<cfelse>
		<cfset rollstart = "#month(now())+1#/1/#year(now())-1#">
		<cfset fullrollstart = "#month(now())+1#/1/#year(now())-2#">
	</cfif>
</cfif>



<cfset ytdhrs = 0>
<cfset ytdallinc = 0>
<cfset ytdtririnc = 0>
<cfset ytdltiinc = 0>

<cfset rolldate = rollstart>

<cfloop from="1" to="11" index="i">
	<cfset rolldate = dateadd("m",1,rolldate)>
	<cfset rolldate = "#month(rolldate)#/#daysinmonth(rolldate)#/#year(rolldate)#">
	<cfif rolldate lte "#month(now())#/#daysinmonth(now())#/#year(now())#">
		<cfset rollstart = listappend(rollstart,rolldate)>
	</cfif>
</cfloop>

<cfset fullrolldate = fullrollstart>
<cfloop from="1" to="23" index="i">
	<cfset fullrolldate = dateadd("m",1,fullrolldate)>
	<cfset fullrolldate = "#month(fullrolldate)#/#daysinmonth(fullrolldate)#/#year(fullrolldate)#">
	<cfset fullrollstart = listappend(fullrollstart,fullrolldate)>
</cfloop>


<cfquery name="getrecordable" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass, YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) 
                         AS incmonth
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.isnearmiss = 'No'  AND (oneAIMincidents.IncAssignedTo IN (1, 2, 3, 4, 11))  and   oneAIMincidents.isWorkRelated = 'yes' and   (oneAIMincidents.Status <> 'Cancel') 
AND
				 ((oneAIMincidents.PrimaryType = 1) AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)) OR 
				 (oneAIMincidents.PrimaryType = 5) AND oneaimincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND (oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">) 

<cfelse>


AND (oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">) 

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>


<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>




</cfif>

<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>


GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), oneAIMInjuryOI.OSHAclass
ORDER BY incyr, incmonth
</cfquery>


<cfquery name="getchartfats" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, YEAR(oneAIMincidents.incidentDate) AS fatyr, MONTH(oneAIMincidents.incidentDate) AS fatmonth
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE      oneAIMincidents.isWorkRelated = 'yes'  AND (oneAIMincidents.IncAssignedTo IN (1, 2, 3, 4, 11)) and    (oneAIMincidents.Status <> 'Cancel')  AND (oneAIMincidents.isFatality = 'yes')

<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
AND (oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">) 

<cfelse>
AND (oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">) 

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and oneAIMincidents.contractor in (SELECT        ContractorID
FROM            Contractors
WHERE        (ContractorNameID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)))
</cfif>
<cfif listfindnocase(frmincassignto,"All") eq 0>
	and oneAIMincidents.IncAssignedTo in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmincassignto#" list="Yes">)
</cfif>


<cfif wronly eq "yes">
	and oneAIMincidents.isworkrelated = 'yes'
<cfelseif wronly eq "no">
	and oneAIMincidents.isworkrelated = 'no'
</cfif>
<cfif listfindnocase(potentialrating,"All") eq 0>
	<cfset prlist = "">
	<cfif listfindnocase(potentialrating,"Low") gt 0>
		<cfset prlist = listappend(prlist,"A1,B1,C1,D1,A2,B2,C2")>
	</cfif>
	<cfif listfindnocase(potentialrating,"Medium") gt 0>
		<cfset prlist = listappend(prlist,"E1,D2,E2,A3,B3,C3,D3")>
	</cfif>
	<cfif listfindnocase(potentialrating,"High") gt 0>
		<cfset prlist = listappend(prlist,"E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5")>
	</cfif>
	and oneAIMincidents.PotentialRating  in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prlist#" list="Yes">)
</cfif>
<cfif listfindnocase(invlevel,"All") eq 0>
	and oneAIMincidents.InvestigationLevel in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invlevel#" list="Yes">)
</cfif>


</cfif>

<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate)
ORDER BY fatyr, fatmonth
</cfquery>

<cfset totrecchrtstruct = {}>
<cfset allrecchrtstruct = {}>
<cfset LTIrecchrtstruct = {}>


<cfloop query="getrecordable">
	<cfswitch expression="#OSHAclass#">
		<cfcase value="1">
			<cfif incyr eq year(now())>
				<cfset ytdallinc = ytdallinc+inccnt>
			</cfif>
			<cfif not structkeyexists(allrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = allrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="2">
			<cfif incyr eq year(now())>
				<cfset ytdallinc = ytdallinc+inccnt>
				<cfset ytdtririnc = ytdtririnc+inccnt>
				<cfset ytdltiinc = ytdltiinc+inccnt>
			</cfif>
			<cfif not structkeyexists(allrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = allrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(totrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = totrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(LTIrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset LTIrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset LTIrecchrtstruct["#incyr#_#incmonth#"] = LTIrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="3,4">
			<cfif incyr eq year(now())>
				<cfset ytdallinc = ytdallinc+inccnt>
				<cfset ytdtririnc = ytdtririnc+inccnt>
			</cfif>
			<cfif not structkeyexists(allrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = allrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(totrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = totrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
		</cfcase>
	</cfswitch>
</cfloop>
<cfloop query="getchartfats">
	<cfif fatyr eq year(now())>
		<cfset ytdallinc = ytdallinc+inccnt>
		<cfset ytdtririnc = ytdtririnc+inccnt>
	</cfif>
	<cfif not structkeyexists(allrecchrtstruct,"#fatyr#_#fatmonth#")>
		<cfset allrecchrtstruct["#fatyr#_#fatmonth#"] = inccnt>
	<cfelse>
		<cfset allrecchrtstruct["#fatyr#_#fatmonth#"] = allrecchrtstruct["#fatyr#_#fatmonth#"]+inccnt>
	</cfif>
	<cfif not structkeyexists(totrecchrtstruct,"#fatyr#_#fatmonth#")>
		<cfset totrecchrtstruct["#fatyr#_#fatmonth#"] = inccnt>
	<cfelse>
		<cfset totrecchrtstruct["#fatyr#_#fatmonth#"] = totrecchrtstruct["#fatyr#_#fatmonth#"]+inccnt>
	</cfif>
</cfloop>
<cfset allcharthrsstruct = {}>
<cfquery name="chartjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID, 
                         YEAR(LaborHours.WEEK_ENDING_DATE) AS jdeyr, MONTH(LaborHours.WEEK_ENDING_DATE) AS jdemonth
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE    1 = 1
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
   and  (LaborHours.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)

<cfelse>
   and  (LaborHours.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)

<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and 1 = 2
</cfif>





</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(LaborHours.WEEK_ENDING_DATE), MONTH(LaborHours.WEEK_ENDING_DATE), GroupLocations.LocationDetailID
ORDER BY jdeyr, jdemonth
</cfquery>
<cfloop query="chartjdehrs">
	<cfif jdeyr eq year(now())>
		<cfset ytdhrs = ytdhrs+jdehrs>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#jdeyr#_#jdemonth#")>
		<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = jdehrs>
	<cfelse>
		<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = allcharthrsstruct["#jdeyr#_#jdemonth#"]+jdehrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif jdeyr eq year(now())>
			<cfset ytdhrs = ytdhrs+jdehrs>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#jdeyr#_#jdemonth#")>
			<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = jdehrs>
		<cfelse>
			<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = allcharthrsstruct["#jdeyr#_#jdemonth#"]+jdehrs>
		</cfif>
	</cfif>
</cfloop>
<cfquery name="chartohhrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, YEAR(LaborHoursOffice.WEEK_ENDING_DATE) AS ohyr, 
                         MONTH(LaborHoursOffice.WEEK_ENDING_DATE) AS ohmonth
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE     1 = 1
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
and   (LaborHoursOffice.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)
<cfelse>
and   (LaborHoursOffice.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and 1 = 2
</cfif>
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(LaborHoursOffice.WEEK_ENDING_DATE), MONTH(LaborHoursOffice.WEEK_ENDING_DATE), GroupLocations.LocationDetailID
ORDER BY ohyr, ohmonth
</cfquery>
<cfloop query="chartohhrs">
	<cfif ohyr eq year(now())>
		<cfset ytdhrs = ytdhrs+oh>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#ohyr#_#ohmonth#")>
		<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = oh>
	<cfelse>
		<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = allcharthrsstruct["#ohyr#_#ohmonth#"]+oh>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif ohyr eq year(now())>
			<cfset ytdhrs = ytdhrs+oh>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#ohyr#_#ohmonth#")>
			<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = oh>
		<cfelse>
			<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = allcharthrsstruct["#ohyr#_#ohmonth#"]+oh>
		</cfif>
	</cfif>
</cfloop>
<cfquery name="getallhrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) 
                         + SUM(LaborHoursTotal.SubHrs) + SUM(LaborHoursTotal.JVHours) + SUM(LaborHoursTotal.ManagedContractorHours) AS allhrs, GroupLocations.LocationDetailID, 
                         YEAR(LaborHoursTotal.WeekEndingDate) AS manyr, MONTH(LaborHoursTotal.WeekEndingDate) AS manmonth
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
WHERE     1 = 1
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
and   (LaborHoursTotal.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)
<cfelse>
and   (LaborHoursTotal.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and 1 = 2
</cfif>
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(LaborHoursTotal.WeekEndingDate), MONTH(LaborHoursTotal.WeekEndingDate), GroupLocations.LocationDetailID
ORDER BY manyr, manmonth
</cfquery>
<cfloop query="getallhrs">
	<cfif manyr eq year(now())>
		<cfset ytdhrs = ytdhrs+allhrs>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#manyr#_#manmonth#")>
		<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allhrs>
	<cfelse>
		<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allcharthrsstruct["#manyr#_#manmonth#"]+allhrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif manyr eq year(now())>
			<cfset ytdhrs = ytdhrs+allhrs>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#manyr#_#manmonth#")>
			<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allhrs>
		<cfelse>
			<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allcharthrsstruct["#manyr#_#manmonth#"]+allhrs>
		</cfif>
	</cfif>
</cfloop>
<cfquery name="getconhrsall" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, YEAR(ContractorHours.WeekEndingDate) AS conyr, 
                         MONTH(ContractorHours.WeekEndingDate) AS conmonth
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE  1 = 1
<cfif not isdefined("dosearch") or trim(dosearch) eq ''>
and      (ContractorHours.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)
<cfelse>
and      (ContractorHours.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)
<cfif listfindnocase(bu,"All") eq 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#" list="Yes">)
</cfif>
<cfif listfindnocase(ou,"All") eq 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#" list="Yes">)
</cfif>
<cfif listfindnocase(businessstream,"All") eq 0>
	and Groups.BusinessStreamID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#businessstream#" list="Yes">)
</cfif>
<cfif listfindnocase(frmclient,"All") eq 0>
	and GroupLocations.ClientID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmclient#" list="Yes">)
</cfif>
<cfif listfindnocase(qryproject,"All") eq 0>
	and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#qryproject#" list="Yes">)
</cfif>
<cfif listfindnocase(site,"All") eq 0>
	and GroupLocations.grouplocid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#site#" list="Yes">)
</cfif>
<cfif listfindnocase(frmcountryid,"All") eq 0>
	and GroupLocations.countryid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmcountryid#" list="Yes">)
</cfif>
<cfif listfindnocase(locdetail,"All") eq 0>
	and GroupLocations.LocationDetailID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#locdetail#" list="Yes">)
</cfif>
<cfif trim(contractlist) neq ''>
	and Contractors.ContractorNameID  in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#contractlist#" list="Yes">)
</cfif>
</cfif>
<cfif showouonly>and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)</cfif>
GROUP BY YEAR(ContractorHours.WeekEndingDate), MONTH(ContractorHours.WeekEndingDate), GroupLocations.LocationDetailID
ORDER BY conyr, conmonth
</cfquery>
<cfloop query="getconhrsall">
	<cfif conyr eq year(now())>
		<cfset ytdhrs = ytdhrs+conhrs>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#conyr#_#conmonth#")>
		<cfset allcharthrsstruct["#conyr#_#conmonth#"] = conhrs>
	<cfelse>
		<cfset allcharthrsstruct["#conyr#_#conmonth#"] = allcharthrsstruct["#conyr#_#conmonth#"]+conhrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif conyr eq year(now())>
			<cfset ytdhrs = ytdhrs+conhrs>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#conyr#_#conmonth#")>
			<cfset allcharthrsstruct["#conyr#_#conmonth#"] = conhrs>
		<cfelse>
			<cfset allcharthrsstruct["#conyr#_#conmonth#"] = allcharthrsstruct["#conyr#_#conmonth#"]+conhrs>
		</cfif>
	</cfif>
</cfloop>


<cfset TRIRratestructc = {}>
<cfset LTIratestructc = {}>
<cfset ALLRratestructc = {}>

<cfloop list="#rollstart#" index="rdate">

	<cfset thisrollwin = dateformat(dateadd("m",-12,rdate),"m/d/yyyy")>
	<cfset rolldate = thisrollwin>
	
	<cfset rollhours = 0>
	<cfset rollincsTRI = 0>
	<cfset rollincsLTI = 0>
	<cfset rollincsALL= 0>
	
	<cfloop from="1" to="12" index="i">
		<cfset rolldate = dateadd("m",1,rolldate)>
		<cfset rolldate = "#month(rolldate)#/#daysinmonth(rolldate)#/#year(rolldate)#">
		<!--- <cfset thisrollwin = listappend(thisrollwin,rolldate)> --->
		<cfif structkeyexists(allcharthrsstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollhours = rollhours + allcharthrsstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif>
		<cfif structkeyexists(totrecchrtstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollincsTRI = rollincsTRI + totrecchrtstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif>
		<cfif structkeyexists(LTIrecchrtstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollincsLTI = rollincsLTI + LTIrecchrtstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif>
		<cfif structkeyexists(allrecchrtstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollincsALL = rollincsALL + allrecchrtstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif> 
		
		
		
		
	</cfloop>
	<!--- <cfoutput>#rdate# hrs (#rollhours#) tr (#rollincsTRI#) #numberformat(((5+rollincsTRI)*200000)/rollhours,"0.000")# lt(#rollincsLTI#)  all(#rollincsALL#)<br></cfoutput> --->
	<cfif rollhours gt 0 and rollincsTRI gt 0>
		<cfset TRIRratestructc["#year(rdate)#_#month(rdate)#"] = numberformat((rollincsTRI*200000)/rollhours,"0.00")>
	<cfelse>
		<cfset TRIRratestructc["#year(rdate)#_#month(rdate)#"] = 0>
	</cfif>
	<cfif rollhours gt 0 and rollincsLTI gt 0>
		<cfset LTIratestructc["#year(rdate)#_#month(rdate)#"] = numberformat((rollincsLTI*200000)/rollhours,"0.000")>
	<cfelse>
		<cfset LTIratestructc["#year(rdate)#_#month(rdate)#"] = 0>
	</cfif>
	<cfif rollhours gt 0 and rollincsALL gt 0>
		<cfset ALLRratestructc["#year(rdate)#_#month(rdate)#"] = numberformat((rollincsALL*200000)/rollhours,"0.00")>
	<cfelse>
		<cfset ALLRratestructc["#year(rdate)#_#month(rdate)#"] = 0>
	</cfif>
	
	

</cfloop>


<cfset chart3xml = "<chart palettecolors='##5f2468' linecolor='##6d8833' linethickness='3' showShadow='0' drawAnchors='0'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1'  caption='Lost Time Injuries' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint' showvalues='0' baseFont='Segoe UI' baseFontColor='##5f2468' showToolTip='0'><categories>">
<cfloop list="#rollstart#" index="cdate">
	<cfset chart3xml = chart3xml  & "<category label='#left(monthasstring(month(cdate)),3)# #right(year(cdate),2)#' />">
</cfloop>
<cfset chart3xml = chart3xml  & "</categories><dataset seriesname='Monthly Cases' renderas='bar'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(LTIrecchrtstruct,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = LTIrecchrtstruct["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart3xml = chart3xml  & "<set value='#cval#' />">
</cfloop>
<cfset chart3xml = chart3xml  & "</dataset><dataset seriesname='Rolling Frequency' parentyaxis='S' renderas='line' showvalues='0'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(LTIratestructc,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = LTIratestructc["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart3xml = chart3xml  & "<set value='#cval#' />">
</cfloop>
<cfset chart3xml = chart3xml  & "</dataset></chart>">