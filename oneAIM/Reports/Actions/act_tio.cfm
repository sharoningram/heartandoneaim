<cfparam name="diamondtype" default="">

<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_tio.cfm", "exports"))>
 <cfif not directoryexists("#FileDir#")>
	<cfdirectory action="CREATE" directory="#FileDir#">
</cfif>

<cfif diamondtype eq "pdf">
<cfset thefilename = "TimeOfIncident_#request.userid##datetimeformat(now(),'HHnnssl')#.pdf">
<cfdocument format="PDF" filename="#filedir#\#thefilename#"  overwrite="yes">
<table cellpadding="4" cellspacing="1" bgcolor="5f2468" width="60%" align="center">
	<tr>
		<td bgcolor="5f2468" colspan="2"  align="center"><strong style="font-family: Segoe UI;font-size:10pt;color:ffffff;">Time Incident Occurred</strong></td>
	</tr>
	<cfoutput query="gettio">
		<tr>
			<td bgcolor="ffffff"  width="80%" bgcolor="ffffff" style="font-family: Segoe UI;font-size:10pt;">#incidentTime#</td>
			<Td bgcolor="ffffff" align="center" bgcolor="ffffff" style="font-family: Segoe UI;font-size:10pt;">#timecnt#</td>
		</tr>
	</cfoutput>
</table>
</cfdocument>

<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">

<cfelseif diamondtype eq "xcl">
	<cfset thefilename = "TimeOfIncident_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
	<cfset thefile = "#filedir#\#thefilename#">
	<cfset theSheet = spreadsheetnew("Time of Incident","true")>
	<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"Time Incident Occurred",1,1);
	 	SpreadsheetSetCellValue(theSheet,"Number of Incidents",1,2);
	</cfscript>
	<cfset ctr = 1>
	<cfoutput query="gettio">
	<cfset ctr = ctr + 1>
		<cfscript> 
	 		SpreadsheetSetCellValue(theSheet,"#incidentTime#",#ctr#,1);
	 		SpreadsheetSetCellValue(theSheet,"#timecnt#",#ctr#,2);
		</cfscript>
	</cfoutput>
 	<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
	<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">



</cfif>
