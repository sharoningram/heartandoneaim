<cfparam name="diamondtype" default="">
<cfset chrttitle = "Shift">
<cfset qryname = "getmdshift">
<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "actions\act_mdshift.cfm", "exports"))>
 <cfif not directoryexists("#FileDir#")>
	<cfdirectory action="CREATE" directory="#FileDir#">
</cfif>

<cfif diamondtype eq "pdf">
<cfset thefilename = "#replace(chrttitle,' ','','all')#_#request.userid##datetimeformat(now(),'HHnnssl')#.pdf">
<cfdocument format="PDF" filename="#filedir#\#thefilename#"  overwrite="yes">
<table cellpadding="4" cellspacing="1" bgcolor="5f2468" width="60%" align="center">
	<tr>
		<td bgcolor="5f2468" colspan="2"  align="center"><strong style="font-family: Segoe UI;font-size:10pt;color:ffffff;"><cfoutput>#chrttitle#</cfoutput></strong></td>
	</tr>
	<cfoutput query="#qryname#">
		<tr>
			<td bgcolor="ffffff"  width="80%" bgcolor="ffffff" style="font-family: Segoe UI;font-size:10pt;">#lbl#</td>
			<Td bgcolor="ffffff" align="center" bgcolor="ffffff" style="font-family: Segoe UI;font-size:10pt;">#cnt#</td>
		</tr>
	</cfoutput>
</table>
</cfdocument>

<!--- <cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "act_mdshift.cfm", ""))>	
<cffile action="MOVE" source="#deldir##thefilename#" destination="#FileDir#" nameconflict="OVERWRITE"> --->
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">

<cfelseif diamondtype eq "xcl">
	<cfset thefilename = "#replace(chrttitle,' ','','all')#_#request.userid##datetimeformat(now(),'HHnnssl')#.xlsx">
	<cfset thefile = "#filedir#\#thefilename#">
	<cfset theSheet = spreadsheetnew("#chrttitle#","true")>
	<cfscript> 
	 	SpreadsheetSetCellValue(theSheet,"#chrttitle#",1,1);
	 	SpreadsheetSetCellValue(theSheet,"Number of Incidents",1,2);
	</cfscript>
	<cfset ctr = 1>
	<cfoutput query="#qryname#">
	<cfset ctr = ctr + 1>
		<cfscript> 
	 		SpreadsheetSetCellValue(theSheet,"#lbl#",#ctr#,1);
	 		SpreadsheetSetCellValue(theSheet,"#cnt#",#ctr#,2);
		</cfscript>
	</cfoutput>
 	<cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
	<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#">



</cfif>