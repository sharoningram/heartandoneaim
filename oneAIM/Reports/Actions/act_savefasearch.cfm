<cfparam name="incyear" default="">
<cfparam name="invlevel" default="">
<cfparam name="bu" default="">
<cfparam name="ou" default="">
<cfparam name="businessstream" default="">
<cfparam name="projectoffice" default="">
<cfparam name="site" default="">
<cfparam name="frmmgdcontractor" default="">
<cfparam name="frmjv" default="">
<cfparam name="frmsubcontractor" default="">
<cfparam name="frmclient" default="">
<cfparam name="locdetail" default="">
<cfparam name="frmcountryid" default="">
<cfparam name="frmincassignto" default="">
<cfparam name="wronly" default="">
<cfparam name="primarytype" default="">
<cfparam name="secondtype" default="">
<cfparam name="hipoonly" default="">
<cfparam name="oshaclass" default="">
<cfparam name="oidef" default="">
<cfparam name="seriousinj" default="">
<cfparam name="secinccats" default="">
<cfparam name="eic" default="">
<cfparam name="damagesrc" default="">
<cfparam name="startdate" default="">
<cfparam name="enddate" default="">
<cfparam name="INCNM" default="no">
<cfparam name="searchname" default="">
<cfparam name="irnumber" default="">
<cfparam name="incandor" default="And">
<cfparam name="searchtype" default="basic">
<cfparam name="potentialrating" default="All">
<cfif isdefined("savefrm")>
<cfif trim(searchname) neq ''>
	<cfquery name="getnamedsearch" datasource="#request.dsn#">
		select UserIncSearchID
		from UserIncidentSearches
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
	</cfquery>
	<cfset newsave = 1>
	<cfif getnamedsearch.recordcount gt 0>
		<cfquery name="deleteoldcriteria" datasource="#request.dsn#">
			delete from UserIncidentSearches
			where UserIncSearchID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getnamedsearch.UserIncSearchID#">
		</cfquery>
	</cfif>
	<!--- <cfif getnamedsearch.recordcount gt 0>
		<cfquery name="getmax" datasource="#request.dsn#">
			select max(savectr) as savectr
			from UserIncidentSearches
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and SearchName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">
		</cfquery>
		<cfset newsave = getmax.savectr+1>
	<cfelse>
		<cfset newsave = 1>
	</cfif> --->
	
	
		<cfquery name="addsearch" datasource="#request.dsn#">
			insert into UserIncidentSearches (SearchName, UserID, incyear, invlevel, bu, ou, bs, project, siteoffice, mgdcontractor, jv, subcon, clientid, locdetail, countryid, 
                         incassigned, wronly, primarytype, secondtype, hipoony, oshaclass, oidef, seriousinj, secinccats, eic, damagesrc, startdate, enddate, SaveCtr,incnm,irnumber,incandor,searchtype,potentialrating,incsearchtype)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incyear#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#bu#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ou#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#businessstream#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#projectoffice#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#site#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmmgdcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmjv#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmsubcontractor#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclient#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#locdetail#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmcountryid#">, 
                         <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmincassignto#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#wronly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#primarytype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secondtype#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#hipoonly#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oshaclass#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oidef#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#seriousinj#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#secinccats#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#eic#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#damagesrc#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startdate#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#enddate#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#newsave#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incnm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#irnumber#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incandor#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#searchtype#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">,'fa')
		</cfquery>
	
</cfif>
</cfif>