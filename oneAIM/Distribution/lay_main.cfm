<cfset titleaddon = "Distribution Management">
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="lookup">
		<cfset titleaddon = titleaddon & " - Email Lookup">
	</cfcase>
	<cfcase value="diallevel">
		<cfset titleaddon = titleaddon & " - Organisational Level Groups">
	</cfcase>
	<cfcase value="addupdate">
		<cfset titleaddon = titleaddon & " - Add/Update Distribution Groups">
	</cfcase>
</cfswitch>
<cfoutput>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
<!--- 	<!--- 	<cfif trim(titleaddon) neq ''><div align="center" class="h2">#titleaddon#</div></cfif> --->
		<table width="100%" cellpadding="2" cellspacing="0" border="0">
<!--- <tr bgcolor="ffffff">
	<td align="right" colspan="2"><img src="images/logoR.jpg" border="0" class="img"></td>
</tr> --->
<tr bgcolor="5f2468"><td style="font-family:Segoe UI;color:ffffff;font-size:12pt;">&nbsp;<a href="index.cfm?fuseaction=adminfunctions.main" style="color:ffffff;text-decoration:none;">Admin Functions</a></td><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;"><cfif trim(request.fname) neq ''>Welcome <cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td></tr>


</table>
		#Fusebox.layout# --->
		<div class="stripe1"><table width="100%" cellpadding="0" cellspacing="0" border="0">

<tr><td>
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr class="purplebg">
	<td class="bodytextwhitelg"  width="90%">&nbsp;Admin&nbsp;Functions&nbsp;-&nbsp;Email&nbsp;Distribution</td><td align="right" class="bodytextwhite" ><cfif trim(request.fname) neq ''>Welcome&nbsp;<cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td><td align="right"  class="helptext">&nbsp;<a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.oneAIMpath#/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');"  class="helptext">Help</a>&nbsp;</span><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;">&nbsp;&nbsp;&nbsp;</td></tr>
</table></td></tr>
<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>

<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>
</table>
</div>
	<table cellpadding="0" border="0" cellspacing="0" width="75%" id="mytab">
	<tr>
		<td><img src="images/spacer.gif" height="30" border="0"></td>
	</tr>
	<tr>
	<td align="right">
		<table  cellpadding="0" cellspacing="0">
		<cfoutput>
			<tr>
				<!--- <cfif groupnumber gt 0>
				<td class="tab1bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab1bkg"  background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.main" class="bodyTextPurple">Add&nbsp;Project&nbsp;Group</a></td>
				<td class="tab1bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td class="tab2bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab2bkg" style="#irstyle#" >Update&nbsp;Project&nbsp;Group</td>
				<td class="tab2bkg"><img src="images/tabright.png" border="0"></td>
				<td class="tab3bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab3bkg"  background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.grouplist" class="bodyTextPurple">View&nbsp;Group&nbsp;List</a></td>
				<td class="tab3bkg"><img src="images/tabrightsm.png" border="0"></td>
				<cfelse>
				<cfif fusebox.fuseaction eq "grouplist">
				<td class="tab1bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab1bkg" background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.main" class="bodyTextPurple">Add&nbsp;Project&nbsp;Group</a></td>
				<td class="tab1bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td class="tab2bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab2bkg"  style="#irstyle#">View&nbsp;Group&nbsp;List</td>
				<td class="tab2bkg"><img src="images/tabright.png" border="0"></td>
				<cfelse>
				<td class="tab1bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab1bkg" style="#irstyle#" >Add&nbsp;Project&nbsp;Group</td>
				<td class="tab1bkg"><img src="images/tabright.png" border="0"></td>
				<td class="tab2bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab2bkg"  background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.grouplist" class="bodyTextPurple">View&nbsp;Group&nbsp;List</a></td>
				<td class="tab2bkg"><img src="images/tabrightsm.png" border="0"></td>
				</cfif>
				</cfif> --->
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=adminfunctions.main" class="bodytextsmpurple"><<&nbsp;Admin&nbsp;Home</a>&nbsp;&nbsp;</td>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple"><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;</td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
<tr>
<td>#Fusebox.layout#</td></tr></table>
</cfif>

</cfoutput>