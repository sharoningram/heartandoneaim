


<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="diallevel">
	<cfset attributes.xfa.managedist = "distribution.managedist">
	<cfset attributes.xfa.diallevel = "distribution.diallevel">
	<cfparam name="type" default="none">
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
		<cfinclude template="queries/qry_getDialEmails.cfm">
		<cfinclude template="actions/act_createFullDialList.cfm">
		<cfinclude template="forms/frm_dialleveldist.cfm">
		<cfinclude template="forms/frm_diallevelemails.cfm">
	<cfelseif listfindnocase(request.userlevel,"BU Admin") gt 0>
		<cfif listfindnocase("FA,WD",type) gt 0>
			<cfinclude template="queries/qry_getDialEmails.cfm">
			<cfinclude template="actions/act_createFullDialList.cfm">
			<cfinclude template="forms/frm_dialleveldist.cfm">
			<cfinclude template="forms/frm_diallevelemails.cfm">
		</cfif>
	</cfif>
</cfcase>
<cfcase value="managedist">
	<cfset attributes.xfa.diallevel = "distribution.diallevel">
	<cfset attributes.xfa.addupdate = "distribution.addupdate">
	<cfset attributes.xfa.lookup = "distribution.diallevel">
	<cfinclude template="actions/act_managedist.cfm">
</cfcase>
<cfcase value="addupdate">
	<cfset attributes.xfa.managedist = "distribution.managedist">
	<cfset attributes.xfa.addupdate = "distribution.addupdate">
	<cfinclude template="queries/qry_getdistemails.cfm">
	<cfinclude template="queries/qry_getdistBU.cfm">
	<cfinclude template="queries/qry_getdistgroups.cfm">
	<cfinclude template="queries/qry_getuserBUs.cfm">
	<cfinclude template="actions/act_checkdistemail.cfm">
	<cfinclude template="forms/frm_addselectdistgroup.cfm">
	<cfinclude template="forms/frm_distgroupemails.cfm">
</cfcase>
<!--- <cfcase value="lookup">
	<cfset attributes.xfa.managedist = "distribution.managedist">
	<cfset attributes.xfa.lookup = "distribution.lookup">
	<cfinclude template="queries/qry_lookupemails.cfm">
	<cfinclude template="forms/frm_lookupemail.cfm">
	<cfinclude template="displays/dsp_emaildistlist.cfm">
</cfcase> --->
<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
