<cfparam name="DLid" default="0">
<cfparam name="type" default="none">

<cfquery name="getdialname" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials.Parent AS buid, NewDials_1.Name AS buname, NewDials_2.Name AS parentname, NewDials_2.ID AS pid
FROM            NewDials LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON NewDials_1.Parent = NewDials_2.ID
WHERE        (NewDials.ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#DLid#">)
</cfquery>
<cfset dlidlist = getdialname.id>
<cfif trim(getdialname.buid) neq 0>
	<cfset dlidlist = listappend(dlidlist,getdialname.buid)>
</cfif>
<cfif trim(getdialname.pid) neq ''>
	<cfset dlidlist = listappend(dlidlist,getdialname.pid)>
</cfif>

<cfquery name="getdialemails" datasource="#request.dsn#">
SELECT       DistributionEmail.emailID, DistributionEmail.distid, DistributionEmail.EnteredBy,DistributionEmail.EmailAddress, DistributionEmail.DialID, DistributionEmail.type, DistributionEmail.emailtype, NewDials.Name, NewDials.Parent,DistributionEmail.HiPoFAonly
FROM            DistributionEmail INNER JOIN
                         NewDials ON DistributionEmail.DialID = NewDials.ID
WHERE        (DistributionEmail.DialID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dlidlist#" list="yes">)) AND (DistributionEmail.emailtype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#type#">)
ORDER BY NewDials.Parent, DistributionEmail.EmailAddress
</cfquery>