<cfparam name="lookuptype" default="exact">
<cfparam name="fullemailaddress" default="">
<cfif trim(fullemailaddress) neq ''>
<cfquery name="getemailgroups" datasource="#request.dsn#">
SELECT        DistributionEmail.EmailID, DistributionEmail.EmailAddress, DistributionEmail.DialID, DistributionEmail.EnteredBy, NewDials_1.Name, 
                         DistributionGroups.DistributionName, DistributionGroups.BusinessLineId, NewDials.Name AS distname, NewDials_1.businessline AS distbl, 
                         DistributionEmail.type
FROM            NewDials INNER JOIN
                         DistributionGroups ON NewDials.businessline = DistributionGroups.BusinessLineId AND NewDials.Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_2
                               WHERE        (Parent = 0)) RIGHT OUTER JOIN
                         DistributionEmail LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON DistributionEmail.DialID = NewDials_1.ID ON DistributionGroups.DistID = DistributionEmail.DistID
<cfif lookuptype eq "exact">
	WHERE        (DistributionEmail.EmailAddress = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#fullemailaddress#">)
<cfelseif lookuptype eq "partial">
	WHERE        (DistributionEmail.EmailAddress LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#fullemailaddress#%">)
</cfif>
ORDER BY DistributionEmail.EmailAddress, DistributionEmail.type, distname, DistributionGroups.DistributionName, NewDials_1.Name
</cfquery>
</cfif>