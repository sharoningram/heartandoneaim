<cfquery name="getdistgroups" datasource="#request.dsn#">
SELECT        DistributionGroups.DistID, DistributionGroups.DistributionName, DistributionGroups.EnteredBy, DistributionGroups.Status, DistributionGroups.BusinessLineId, 
                         NewDials.Name
FROM            DistributionGroups INNER JOIN
                         NewDials ON DistributionGroups.BusinessLineId = NewDials.businessline
WHERE        (DistributionGroups.BusinessLineId IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="yes">)) AND (NewDials.Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0)))
ORDER BY NewDials.Name, DistributionGroups.DistributionName, DistributionGroups.DistID
</cfquery>
