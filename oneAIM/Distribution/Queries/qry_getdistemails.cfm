<cfparam name="dist" default="0">
<cfquery name="getdistemails" datasource="#request.dsn#">
SELECT        DistributionGroups.DistID, DistributionGroups.DistributionName, DistributionGroups.BusinessLineId, DistributionEmail.EmailAddress, 
                         DistributionEmail.EmailID
FROM            DistributionGroups INNER JOIN
                         DistributionEmail ON DistributionGroups.DistID = DistributionEmail.DistID
WHERE        (DistributionGroups.DistID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dist#">)
Order by EmailAddress
</cfquery>
