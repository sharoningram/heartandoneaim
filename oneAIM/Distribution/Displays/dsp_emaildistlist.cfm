<cfif isdefined("getemailgroups.recordcount")>
<!--- <div class="content1of2r"> --->


	<cfif getemailgroups.recordcount gt 0>
	<!--- 
	<style>
	
	/* Scrollable table styling */
	.scrollable.has-scroll {
		position:relative;
		overflow:hidden; /* Clips the shadow created with the pseudo-element in the next rule. Not necessary for the actual scrolling. */
	}
	.scrollable.has-scroll:after {
		position:absolute;
		top:0;
		left:100%;
		width:50px;
		height:100%;
		border-radius:10px 0 0 10px / 50% 0 0 50%;
		box-shadow:-5px 0 10px rgba(0, 0, 0, 0.25);
		content:'';
	}

	/* This is the element whose content will be scrolled if necessary */
	.scrollable.has-scroll > div {
		overflow-x:auto;
	}

	/* Style the scrollbar to make it visible in iOS, Android and OS X WebKit browsers (where user preferences can make scrollbars invisible until you actually scroll) */
	.scrollable > div::-webkit-scrollbar {
		height:12px;
	}
	.scrollable > div::-webkit-scrollbar-track {
		box-shadow:0 0 2px rgba(0,0,0,0.15) inset;
		background:#f0f0f0;
	}
	.scrollable > div::-webkit-scrollbar-thumb {
		border-radius:6px;
		background:#ccc;
	}

	</style>
	<script src="/#getappconfig.oneAIMpath#/shared/js/jquery-1.8.3.min.js"></script>
	<script>
	// Run on window load in case images or other scripts affect element widths
	$(window).on('load', function() {
		// Check all tables. You may need to be more restrictive.
		$(mytab).each(function() {
			var element = $(this);
			// Create the wrapper element
			var scrollWrapper = $('<div />', {
				'class': 'scrollable',
				'html': '<div />' // The inner div is needed for styling
			}).insertBefore(element);
			// Store a reference to the wrapper element
			element.data('scrollWrapper', scrollWrapper);
			// Move the scrollable element inside the wrapper element
			element.appendTo(scrollWrapper.find('div'));
			// Check if the element is wider than its parent and thus needs to be scrollable
			if (element.outerWidth() > element.parent().outerWidth()) {
				element.data('scrollWrapper').addClass('has-scroll');
			}
			// When the viewport size is changed, check again if the element needs to be scrollable
			$(window).on('resize orientationchange', function() {
				if (element.outerWidth() > element.parent().outerWidth()) {
					element.data('scrollWrapper').addClass('has-scroll');
				} else {
					element.data('scrollWrapper').removeClass('has-scroll');
				}
			});
		});
	});
	</script>
	 --->
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="mytab">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Assigned Distribution Groups</strong></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="0" border="0"   width="100%"  class="ltTeal">
			<cfset emctr = 0>
				<cfoutput query="getemailgroups" group="emailaddress">
				<cfset thisdistBLlist = "">
					<cfoutput>
						<cfif trim(businesslineid) neq ''>
							<cfif listfind(thisdistBLlist,businesslineid) eq 0>
								<cfset thisdistBLlist = listappend(thisdistBLlist,businesslineid)>
							</cfif>
						</cfif>
						<cfif trim(distbl) neq ''>
							<cfif listfind(thisdistBLlist,distbl) eq 0>
								<cfset thisdistBLlist = listappend(thisdistBLlist,distbl)>
							</cfif>
						</cfif>
					</cfoutput>
					<cfset showemail = "no">
					<cfloop list="#request.userBUs#" index="i">
						<cfif listfind(thisdistBLlist,i) gt 0>
							<cfset showemail = "yes">
							<cfbreak>
						</cfif>
					</cfloop>
					
					
					<cfset emctr = emctr+1>
					<tr <cfif emctr mod 2 is 0>bgcolor="ffffff"</cfif>>
						<td colspan="2" class="bodytext" align="center"><strong style="font-size:11pt;">#emailaddress#</strong></td>
					</tr>
					<cfoutput group="type">
						<tr <cfif emctr mod 2 is 0>bgcolor="ffffff"</cfif>>
							<td colspan="2" class="bodytext"><strong><cfif type eq "org">Organisation Level Distribution<cfelse>Distribution Group</cfif></strong></td>
						</tr>
						
							<cfif type eq "dist">
							<cfoutput group="distname">
							<tr <cfif emctr mod 2 is 0>bgcolor="ffffff"</cfif>>
								<td colspan="2" class="bodytext">&nbsp;&nbsp;#distname#</td>
							</tr>
							<cfoutput>
								<tr <cfif emctr mod 2 is 0>bgcolor="ffffff"</cfif>>
									<td class="bodytext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<cfif showemail><cfif listfind(request.userbus,businesslineid) gt 0><a href="#self#?fuseaction=#attributes.xfa.managedist#&em=#EmailID#&fullemailaddress=#fullemailaddress#&lookuptype=#lookuptype#&submittype=deleteemg" onclick="return confirm('Are you sure you want to delete this email address from this group?');"><img src="images/trash.gif" border="0"></a>&nbsp;</cfif></cfif>#distributionname#</td>
									
								</tr>
								
							</cfoutput>
							</cfoutput>
							<cfelseif type eq "org">
							<cfoutput>
								<tr <cfif emctr mod 2 is 0>bgcolor="ffffff"</cfif>>
									<td  class="bodytext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<cfif showemail><cfif listfind(request.userbus,distbl) gt 0><a href="#self#?fuseaction=#attributes.xfa.managedist#&em=#EmailID#&fullemailaddress=#fullemailaddress#&lookuptype=#lookuptype#&submittype=deleteemg" onclick="return confirm('Are you sure you want to delete this email address from this group?');"><img src="images/trash.gif" border="0"></a>&nbsp;</cfif></cfif>#name#</td>
									
								</tr>
							</cfoutput>
							</cfif>
						</cfoutput>
						<tr <cfif emctr mod 2 is 0>bgcolor="ffffff"</cfif>>
							<td colspan="2"><br></td>
						</tr>
						
				</cfoutput>
	
</table>
</td></tr></table>
<cfelse>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="toptablefrm">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Assigned Email Addresses</strong></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="12" cellspacing="1" border="0"   width="100%"  class="ltTeal">
	<tr>
		<td class="bodytext" colspan="2" align="center"><strong>Your lookup returned zero results</strong></td>
	</tr>

	
</table>
</td></tr></table>

</cfif>		




</cfif>
 </div>