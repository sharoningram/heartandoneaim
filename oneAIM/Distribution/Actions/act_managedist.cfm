<cfparam name="submittype" default="">
<cfparam name="HiPoFAonly" default="0">

<cfswitch expression="#submittype#">
	<cfcase value="addDLemail">
		<cfparam name="emailaddress" default="">
		<cfparam name="dlid" default="0">
		<cfif trim(emailaddress) neq '' and trim(dlid) neq 0>
			<cfquery name="checkemail" datasource="#request.dsn#">
				select emailid
				from DistributionEmail 
				where dialid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dlid#">
				and emailaddress = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#emailaddress#">
				and type = 'org'
				and emailtype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#type#">
			</cfquery>
			<cfif checkemail.recordcount eq 0>
				<cfquery name="addemail" datasource="#request.dsn#">
					insert into DistributionEmail (emailaddress, dialid, enteredby, type, emailtype,HiPoFAonly)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#emailaddress#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dlid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,'org',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#type#">,<cfqueryparam cfsqltype="CF_SQL_BIT" value="#HiPoFAonly#">)
				</cfquery>
			</cfif>
		</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.diallevel#" name="sendbackfrm" method="post">
			<cfoutput><input type="hidden" name="DLid" value="#DLid#"><input type="hidden" name="type" value="#type#"></cfoutput>
		</cfform>
		<script type="text/javascript">
			document.sendbackfrm.submit();
		</script>
	</cfcase>
	<cfcase value="deleteDL">
		<cfparam name="em" default="0">
		<cfparam name="DLid" default="0">
		<cfif em neq 0 and DLid neq 0>
			<cfquery name="delem" datasource="#request.dsn#">
				delete from DistributionEmail
				where EmailId = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#em#">
			</cfquery>
		</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.diallevel#" name="sendbackfrm" method="post">
			<cfoutput><input type="hidden" name="DLid" value="#DLid#"><input type="hidden" name="type" value="#type#"></cfoutput>
		</cfform>
		<script type="text/javascript">
			document.sendbackfrm.submit();
		</script>
	</cfcase>
	<cfcase value="addnewdist">
		<cfparam name="newgroup" default="">
		<cfparam name="BU" default="0">
		<cfset newdist1 = 0>
		<cfif trim(newgroup) neq '' and BU neq 0>
			<cfquery name="addgroup" datasource="#request.dsn#">
				insert into DistributionGroups (DistributionName, EnteredBy, Status, BusinessLineId)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newgroup#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,1,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">)
				select ident_current('DistributionGroups') as newdist
			</cfquery>
			<cfset newdist1 = addgroup.newdist>
		</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.addupdate#" name="sendbackfrm" method="post">
			<cfoutput><input type="hidden" name="dist" value="#newdist1#"></cfoutput>
		</cfform>
		<script type="text/javascript">
			document.sendbackfrm.submit();
		</script>
	</cfcase>
	<cfcase value="adddistemail">
		<cfparam name="emailaddress" default="">
		<cfparam name="dist" default="0">
		<cfif trim(emailaddress) neq '' and trim(dist) neq 0>
			<cfquery name="checkemail" datasource="#request.dsn#">
				select emailid
				from DistributionEmail 
				where distid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dist#">
				and emailaddress = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#emailaddress#">
			</cfquery>
			<cfif checkemail.recordcount eq 0>
				<cfquery name="addemail" datasource="#request.dsn#">
					insert into DistributionEmail (emailaddress, distid, enteredby,type,HiPoFAonly)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#emailaddress#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dist#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,'dist',<cfqueryparam cfsqltype="CF_SQL_BIT" value="#HiPoFAonly#">)
				</cfquery>
			</cfif>
		</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.addupdate#" name="sendbackfrm" method="post">
			<cfoutput><input type="hidden" name="dist" value="#dist#"></cfoutput>
		</cfform>
		<script type="text/javascript">
			document.sendbackfrm.submit();
		</script>
	</cfcase>
	<cfcase value="deletedist">
		<cfparam name="em" default="0">
		<cfparam name="dist" default="0">
		<cfif em neq 0 and dist neq 0>
			<cfquery name="delem" datasource="#request.dsn#">
				delete from DistributionEmail
				where EmailId = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#em#">
			</cfquery>
		</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.addupdate#" name="sendbackfrm" method="post">
			<cfoutput><input type="hidden" name="dist" value="#dist#"></cfoutput>
		</cfform>
		<script type="text/javascript">
			document.sendbackfrm.submit();
		</script>
	</cfcase>
	<cfcase value="deleteemg">
		<cfparam name="em" default="0">
		<cfparam name="fullemailaddress" default="">
		<cfparam name="lookuptype" default="">
		
		<cfif em neq 0>
			<cfquery name="delem" datasource="#request.dsn#">
				delete from DistributionEmail
				where EmailId = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#em#">
			</cfquery>
		</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.lookup#" name="sendbackfrm" method="post">
			<cfoutput>
			<input type="hidden" name="DLid" value="#DLid#">
			<input type="hidden" name="type" value="#type#">
			</cfoutput>
		</cfform>
		<script type="text/javascript">
			document.sendbackfrm.submit();
		</script>
	</cfcase>
</cfswitch>