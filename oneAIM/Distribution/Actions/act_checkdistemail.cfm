<cfquery name="getdistgs" datasource="#request.dsn#">
SELECT DISTINCT DistID
FROM            DistributionEmail
WHERE        (DistID IN (SELECT DISTINCT DistID
FROM            DistributionGroups
WHERE        (BusinessLineId IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="yes">))))
</cfquery>
<cfset hasemailsstruct = {}>
<cfloop query="getdistgs">
	<cfif not structkeyexists(hasemailsstruct,DistID)>
		<cfset hasemailsstruct[DistID] = "yes">
	</cfif>
</cfloop>
