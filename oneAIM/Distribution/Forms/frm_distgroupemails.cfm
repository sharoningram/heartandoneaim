<!--- <div class="content1of1"> --->
<cfif dist neq  0><br>
<cfform action="#self#?fuseaction=#attributes.xfa.managedist#" method="post" name="adddialfrm">
<cfoutput><input type="hidden" name="dist" value="#dist#"></cfoutput>
<input type="hidden" name="submittype" value="adddistemail">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Add Email Address</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	<tr>
		<td class="bodytext"><strong>Enter an email address to add to the distribution list</strong></td>
	</tr>
	<cfoutput>
	<tr>
		<td class="bodytext"><strong>#request.bulabellong#:</strong> #getdistbu.name#</td>
	</tr>
	<tr>
		<td class="bodytext"><strong>Distribution Group:</strong> #getdistbu.DistributionName#</td>
	</tr>
	</cfoutput>
	<tr>
		<td>
			<cfinput type="text" name="emailaddress" size="30" class="selectgen" required="Yes" validate="email" message="Please enter a valid email address">
			</td>
		</tr>
	<tr>
		<td><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
	</table>
</td></tr></table>

	<cfif getdistemails.recordcount gt 0><br>
	<!--- 
	<style>
	
	/* Scrollable table styling */
	.scrollable.has-scroll {
		position:relative;
		overflow:hidden; /* Clips the shadow created with the pseudo-element in the next rule. Not necessary for the actual scrolling. */
	}
	.scrollable.has-scroll:after {
		position:absolute;
		top:0;
		left:100%;
		width:50px;
		height:100%;
		border-radius:10px 0 0 10px / 50% 0 0 50%;
		box-shadow:-5px 0 10px rgba(0, 0, 0, 0.25);
		content:'';
	}

	/* This is the element whose content will be scrolled if necessary */
	.scrollable.has-scroll > div {
		overflow-x:auto;
	}

	/* Style the scrollbar to make it visible in iOS, Android and OS X WebKit browsers (where user preferences can make scrollbars invisible until you actually scroll) */
	.scrollable > div::-webkit-scrollbar {
		height:12px;
	}
	.scrollable > div::-webkit-scrollbar-track {
		box-shadow:0 0 2px rgba(0,0,0,0.15) inset;
		background:#f0f0f0;
	}
	.scrollable > div::-webkit-scrollbar-thumb {
		border-radius:6px;
		background:#ccc;
	}

	</style>
	<script src="/#getappconfig.oneAIMpath#/shared/js/jquery-1.8.3.min.js"></script>
	<script>
	// Run on window load in case images or other scripts affect element widths
	$(window).on('load', function() {
		// Check all tables. You may need to be more restrictive.
		$(mytab).each(function() {
			var element = $(this);
			// Create the wrapper element
			var scrollWrapper = $('<div />', {
				'class': 'scrollable',
				'html': '<div />' // The inner div is needed for styling
			}).insertBefore(element);
			// Store a reference to the wrapper element
			element.data('scrollWrapper', scrollWrapper);
			// Move the scrollable element inside the wrapper element
			element.appendTo(scrollWrapper.find('div'));
			// Check if the element is wider than its parent and thus needs to be scrollable
			if (element.outerWidth() > element.parent().outerWidth()) {
				element.data('scrollWrapper').addClass('has-scroll');
			}
			// When the viewport size is changed, check again if the element needs to be scrollable
			$(window).on('resize orientationchange', function() {
				if (element.outerWidth() > element.parent().outerWidth()) {
					element.data('scrollWrapper').addClass('has-scroll');
				} else {
					element.data('scrollWrapper').removeClass('has-scroll');
				}
			});
		});
	});
	</script> --->
	
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="mytab">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Assigned Email Addresses</strong></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">
	<tr>
		<td class="bodytext" colspan="3" bgcolor="ffffff"><strong>Email addresses assigned to this group</strong></td>
	</tr>
				<cfset recctr = 0>
				<cfset ctr = 1>
				<tr>
				<cfoutput query="getdistemails">
					<cfset recctr = recctr+1>
					<td class="bodytext" width="33%"  <cfif ctr mod 2 is 0>bgcolor="ffffff"</cfif> nowrap><a href="#self#?fuseaction=#attributes.xfa.managedist#&em=#EmailID#&dist=#distid#&submittype=deletedist" onclick="return confirm('Are you sure you want to delete this email address?');"><img src="images/trash.gif" border="0"></a>&nbsp;#emailaddress#
<cfif ctr mod 2 is 0><cfset usebg="ffffff"><cfelse><cfset usebg = "none"></cfif>
					<cfif currentrow mod 3 is 0>
						<cfset ctr = ctr+1></td></tr>
					<cfelse>
						<cfif recctr eq recordcount>
							<cfif usebg eq "none">
								<cfset needtd = 3 - (recctr mod 3)>#repeatstring("<td >&nbsp;</td>",needtd)#</tr>
							<cfelse>
								<cfset needtd = 3 - (recctr mod 3)>#repeatstring("<td bgcolor='ffffff'>&nbsp;</td>",needtd)#</tr>
							</cfif>
						<cfelse>
						</td>
					</cfif></cfif>
				</cfoutput>
				
	
	
</table>
</td></tr></table></cfif>		
</cfform>



</cfif>
</div>