<div class="content1of1" align="center"> 

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="toptablefrm">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Add/Update Distribution Groups</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	<cfif getdistgroups.recordcount gt 0>
	<tr>
		<td class="bodytext"><strong>Choose a distribution group or add a new one using the forms below</strong></td>
	</tr>
	<cfform action="#self#?fuseaction=#attributes.xfa.addupdate#" method="post" name="dialfrm">
	<tr>
		<td>
			<select name="dist" size="1" class="selectgen"  id="lrgselect">
				<option value="0">-- Choose One --</option>

	
<cfoutput query="getdistgroups" group="name">
	<option value="" disabled>#name#</option>
			<cfoutput>
			<option  value="#distid#" <cfif dist eq distid>selected</cfif> >&nbsp;&nbsp;#DistributionName#<cfif structkeyexists(hasemailsstruct,DistID)> &##9993;</cfif></option>
		</cfoutput>
	</cfoutput>
				</select> 
			</td>
		</tr>
		<tr>
		<td><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
	<tr>
		<td><hr width="95%" size="1" color="#5f2467" /></td>
	</tr>
	</cfform>
	</cfif>
		<cfform action="#self#?fuseaction=#attributes.xfa.managedist#" method="post" name="dialfrm">
		<input type="hidden" name="submittype" value="addnewdist">
	<tr>	
		<td class="bodytext"><strong>Enter a new distribution group name:*</strong><br>
			<cfinput type="text" name="newgroup" value="" size="30" class="selectgen" required="Yes" message="Please enter a Distribution Group Name">
		</td>
	</tr>
	<tr>
		<td class="bodytext"><strong>Select #request.bulabellong#:*</strong><br>
		<cfselect name="BU" class="selectgen" size="1" required="Yes" message="Please select a #request.bulabellong#" id="lrgselect">
		<option value="">-- Select One --</option>
			<cfoutput query="getuserbus">
				<option value="#businessline#">#name#</option>
			</cfoutput>
			</cfselect>
	<tr>
		<td><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
	</cfform>
	
	
</table>
</td></tr></table>

<!--- </div> --->