<div class="content1of1" align="center">

<cfform action="#self#?fuseaction=#attributes.xfa.lookup#" method="post" name="dialfrm">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Lookup Email</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	<tr>
		<td class="bodytext"><strong>Enter an email below to lookup what distribution groups it is assigned to</strong></td>
	</tr>
	<tr>
		<td>
			<cfinput type="text" name="fullemailaddress" size="30" value="#fullemailaddress#" required="yes" message="Please enter an email address">
			</td>
		</tr>
	<tr>
		<td class="bodytext"><strong>Choose exact or partial match</strong></td>
	</tr>
	<tr>
		<td class="bodytext"><input type="radio" value="exact" name="lookuptype" <cfif lookuptype eq "exact">checked</cfif>>&nbsp;Exact&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" value="partial" name="lookuptype" <cfif lookuptype eq "partial">checked</cfif>>&nbsp;Partial</td>
	</tr>
	<tr>
		<td><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
</table>
</td></tr></table>
</cfform>
<!--- </div> --->