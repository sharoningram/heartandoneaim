


<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="main">
	<cfinclude template="displays/dsp_adminmain.cfm">
</cfcase>

<!--- <cfcase value="azmanagement">
	<cfif request.isAdmin>
		<cfset attributes.xfa.domanageaz = "adminfunctions.domanageaz">
		<cfinclude template="queries/qry_getazfactors.cfm">
		<cfinclude template="forms/frm_getazfactors.cfm">
	</cfif>
</cfcase>
<cfcase value="domanageaz">
	<cfif request.isAdmin>
		<cfset attributes.xfa.azmanagement = "adminfunctions.azmanagement">
		<cfinclude template="actions/act_manageAZ.cfm">
	</cfif>
</cfcase> --->
<cfcase value="mhdealine">
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
	<cfset attributes.xfa.mhdealine = "adminfunctions.mhdealine">
	<cfinclude template="actions/act_mhdeadline.cfm">
	<cfinclude template="queries/qry_mhdeadline.cfm">
	<cfinclude template="forms/frm_mhdeadline.cfm">
	</cfif>
</cfcase>
<cfcase value="dialadmin">
<cfif listfindnocase(request.userlevel,"Global IT") gt 0>
	<cfset attributes.xfa.dialadmin = "adminfunctions.dialadmin">
	<cfset attributes.xfa.updatedial = "adminfunctions.updatedial">
	<cfset attributes.xfa.dialadminbuoulists = "adminfunctions.dialadminbuoulists">
	<cfinclude template="queries/qry_getallparents.cfm">
	<cfinclude template="displays/dsp_dialadmin.cfm">
</cfif>
</cfcase>
<cfcase value="dialadminbuoulists">
	<cfinclude template="actions/act_dialadminbuoulists.cfm">
</cfcase>
<cfcase value="updatedial">
	<cfif listfindnocase(request.userlevel,"Global IT") gt 0>
	<cfset attributes.xfa.dialadmin = "adminfunctions.dialadmin">
	<cfinclude template="actions/act_updatedial.cfm">
	</cfif>
</cfcase>
<cfcase value="erpassign">
	<cfset attributes.xfa.erpassign = "adminfunctions.erpassign">
	<cfset attributes.xfa.manageerpassign = "adminfunctions.manageerpassign">
	<cfset attributes.xfa.popgroupinfo = "adminfunctions.popgroupinfo">
	<cfinclude template="queries/qry_geterpsys.cfm">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfinclude template="queries/qry_getERPcontracts.cfm">
	<cfinclude template="actions/act_erpextrainfo.cfm">
	<cfinclude template="forms/frm_erpmethod.cfm">
	<cfinclude template="forms/frm_assigncontracts.cfm">
</cfcase>
<cfcase value="popgroupinfo">
	<cfinclude template="actions/act_popgroupinfo.cfm">
</cfcase>
<cfcase value="manageerpassign">
	<cfset attributes.xfa.erpassign = "adminfunctions.erpassign">
	<cfset attributes.xfa.erpreassign = "adminfunctions.erpreassign">
	<cfinclude template="actions/act_managercontracts.cfm">
</cfcase>
<cfcase value="erpreassign">
	<cfset attributes.xfa.erpreassign = "adminfunctions.erpreassign">
	<cfset attributes.xfa.manageerpassign = "adminfunctions.manageerpassign">
	<cfset attributes.xfa.popgroupinfo = "adminfunctions.popgroupinfo">
	<cfinclude template="queries/qry_geterpsys.cfm">
	<cfinclude template="queries/qry_getcontractlist.cfm">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfinclude template="actions/act_erpextrainfoRE.cfm">
	<cfinclude template="forms/frm_lookupcontract.cfm">
	<cfinclude template="forms/frm_REassigncontracts.cfm">
</cfcase>
<cfcase value="clients">
	<cfset attributes.xfa.managefunctions = "adminfunctions.managefunctions">
	<cfset attributes.xfa.clients = "adminfunctions.clients">
	<cfinclude template="queries/qry_getclients.cfm">
	<cfinclude template="queries/qry_getclientdetail.cfm">
	<cfinclude template="forms/frm_addclient.cfm">
	<cfinclude template="forms/frm_lookupclient.cfm">
	<cfinclude template="forms/frm_updateclient.cfm">
</cfcase>
<cfcase value="countries">
	<cfset attributes.xfa.managefunctions = "adminfunctions.managefunctions">
	<cfset attributes.xfa.countries = "adminfunctions.countries">
	<cfinclude template="queries/qry_getcountries.cfm">
	<cfinclude template="queries/qry_getcountrydetail.cfm">
	<cfinclude template="forms/frm_addcountry.cfm">
	<cfinclude template="forms/frm_lookupcountry.cfm">
	<cfinclude template="forms/frm_updatecountry.cfm">
</cfcase>
<cfcase value="occupations">
	<cfset attributes.xfa.managefunctions = "adminfunctions.managefunctions">
	<cfset attributes.xfa.occupations = "adminfunctions.occupations">
	<cfinclude template="queries/qry_getoccupations.cfm">
	<cfinclude template="queries/qry_getoccupationdetail.cfm">
	<cfinclude template="forms/frm_addoccupation.cfm">
	<cfinclude template="forms/frm_lookupoccupation.cfm">
	<cfinclude template="forms/frm_updateoccupation.cfm">
</cfcase>
<cfcase value="managefunctions">
	<cfset attributes.xfa.clients = "adminfunctions.clients">
	<cfset attributes.xfa.countries = "adminfunctions.countries">
	<cfset attributes.xfa.occupations = "adminfunctions.occupations">
	<cfinclude template="actions/act_managefunctions.cfm">
</cfcase>
<cfcase value="createsublists">
	<cfinclude template="actions/act_createsublists.cfm">
</cfcase>
<cfcase value="objectives">
	<cfset attributes.xfa.manageobjectives = "adminfunctions.manageobjectives">
	<cfset attributes.xfa.objectives = "adminfunctions.objectives">
	<cfset attributes.xfa.createsublists = "adminfunctions.createsublists">
	
	<cfinclude template="queries/qry_getBUs.cfm">
	
	<cfinclude template="queries/qry_getcurrobjectives.cfm">
	<cfinclude template="queries/qry_getobjectivedetails.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="actions/act_buildObjStructs.cfm">
	
	<cfinclude template="actions/act_createFullDialList.cfm">
	<cfinclude template="forms/frm_objectives.cfm">
	<cfinclude template="displays/dsp_objectives.cfm">
</cfcase>
<cfcase value="manageobjectives">
	<cfset attributes.xfa.objectives = "adminfunctions.objectives">
	<cfinclude template="actions/act_manageobjectives.cfm">
</cfcase>
<cfcase value="leading">
	<cfset attributes.xfa.manageleading = "adminfunctions.manageleading">
	<cfset attributes.xfa.leading = "adminfunctions.leading">
	<cfinclude template="queries/qry_getcurrleading.cfm">
	<!--- <!cfinclude template="actions/act_buildLeadStructs.cfm"> --->
	<cfinclude template="queries/qry_getsecondlevels.cfm">
	<cfinclude template="queries/qry_getleadingdetails.cfm">
	<!--- <!cfinclude template="actions/act_createFullDialList.cfm"> --->
	<cfinclude template="forms/frm_leading.cfm">
	<cfinclude template="displays/dsp_leading.cfm">
</cfcase>
<cfcase value="manageleading">
	<cfset attributes.xfa.leading = "adminfunctions.leading">
	<cfinclude template="actions/act_manageleading.cfm">
</cfcase>
<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
