<cfparam name="showall" default="yes">
<cfparam name="showonlydash" default="1">

<cfquery name="getallparents" datasource="#request.dsn#">
SELECT     NewDials.ID, NewDials.Name, NewDials.Parent, NewDials.isgroupdial, NewDials.GroupNumber, NewDials.businessline, Groups.TrackingDate, 
                      Groups.Tracking_Num, newdials.ViewOnDashboard as active_group
FROM         NewDials LEFT OUTER JOIN
                      Groups ON NewDials.GroupNumber = Groups.Group_Number 
where  newdials.status <> 99
<cfif showonlydash eq 1>
and newdials.viewondashboard = 1

</cfif>
order by NewDials.businessline , newdials.viewondashboard desc, id

</cfquery>




<cfquery name="getgnums" datasource="#request.dsn#">
select group_number, group_name, business_line_id, ouid, BusinessStreamID, active_group, Tracking_Num, trackingdate
from groups
where active_group = 1
</cfquery>

<cfset havegstruct = {}>

<cfloop query="getgnums">
	<cfif trim(BusinessStreamID) neq '' and BusinessStreamID gt 0>
		<cfif not structkeyexists(havegstruct,BusinessStreamID)>
			<cfset havegstruct[BusinessStreamID] = "#group_number#~P#trackingdate##numberformat(tracking_num,'0000')#~#group_name#">
		<cfelse>
			<cfset havegstruct[BusinessStreamID] = listappend(havegstruct[BusinessStreamID],"#group_number#~P#trackingdate##numberformat(tracking_num,'0000')#~#group_name#","^")>
		</cfif>
	<cfelse>
		<cfif trim(ouid) neq '' and ouid gt 0>
			<cfif not structkeyexists(havegstruct,ouid)>
				<cfset havegstruct[ouid] = "#group_number#~P#trackingdate##numberformat(tracking_num,'0000')#~#group_name#">
			<cfelse>
				<cfset havegstruct[ouid] = listappend(havegstruct[ouid],"#group_number#~P#trackingdate##numberformat(tracking_num,'0000')#~#group_name#","^")>
			</cfif>
		<cfelse>
			<cfif trim(business_line_id) neq '' and business_line_id gt 0>
				<cfif not structkeyexists(havegstruct,business_line_id)>
					<cfset havegstruct[business_line_id] = "#group_number#~P#trackingdate##numberformat(tracking_num,'0000')#~#group_name#">
				<cfelse>
					<cfset havegstruct[business_line_id] = listappend(havegstruct[business_line_id],"#group_number#~P#trackingdate##numberformat(tracking_num,'0000')#~#group_name#","^")>
				</cfif>
			</cfif>
		</cfif>
	</cfif>
</cfloop>
<!--- <cfdump var="#havegstruct#"> --->