<cfparam name="contractnum" default="">
<cfparam name="lookuptype" default="exact">
<cfparam name="frmclientname" default="">
<cfparam name="erp" default="">
<cfif trim(contractnum) neq '' or trim(frmclientname) neq ''>
	<cfquery name="getcontractlist" datasource="#request.dsn#">
		SELECT DISTINCT ProjectsByGroup.Project_Number, ProjectsByGroup.Project_Desc, ProjectsByGroup.Group_Number, Groups.Group_Name, GroupLocations.SiteName
		FROM            ProjectsByGroup INNER JOIN
                         Groups ON ProjectsByGroup.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID AND Groups.Group_Number = GroupLocations.GroupNumber
		WHERE  (Groups.ERPsys = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#erp#">)
		<cfif lookuptype eq "exact">
		 	<cfif trim(contractnum) neq ''>
				and (ProjectsByGroup.Project_Number =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#contractnum#">)
			</cfif>
			<cfif trim(frmclientname) neq ''>
				and (ProjectsByGroup.Project_Desc =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#frmclientname#">)
			</cfif>
		
		<cfelse>
			<cfif trim(contractnum) neq ''>
				and  (ProjectsByGroup.Project_Number like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#contractnum#%">)
			</cfif>
			<cfif trim(frmclientname) neq ''>
				and (ProjectsByGroup.Project_Desc like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#frmclientname#%">)
			</cfif>
		</cfif>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">)
		</cfif>
		<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
			and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="Yes">)
		</cfif>
		ORDER BY ProjectsByGroup.Project_Number, ProjectsByGroup.Project_Desc, ProjectsByGroup.Group_Number
	</cfquery>
</cfif>