<cfparam name="lookupyear" default="#year(now())#">
<cfquery name="getcurrobjectives" datasource="#request.dsn#">
SELECT        Objectives.ObjID, Objectives.ObjYear, Objectives.TRIRType, Objectives.TRIRgoal, Objectives.useTRIR, Objectives.useLTIR, Objectives.TRIR, Objectives.LTIR, 
                         Objectives.DialID, Objectives.LTIRtype, Objectives.LTIRgoal, NewDials.Name, Objectives.AIR, Objectives.AIRgoal
FROM            Objectives INNER JOIN
                         NewDials ON Objectives.DialID = NewDials.ID
WHERE        (Objectives.ObjYear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) 
ORDER BY NewDials.Parent, NewDials.Name
</cfquery>
