<cfquery name="getsecondlevels" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials.Parent, NewDials.isgroupdial, NewDials.GroupNumber, NewDials.businessline, Groups.TrackingDate, 
                         Groups.Tracking_Num, NewDials.ViewOnDashboard AS active_group
FROM            NewDials LEFT OUTER JOIN
                         Groups ON NewDials.GroupNumber = Groups.Group_Number
WHERE        (NewDials.businessline IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0,#request.userBUs#" list="Yes">)) AND (NewDials.ID IN
                             (SELECT        ID
                               FROM            NewDials AS NewDials_2
                               WHERE        (Parent =
                                                             (SELECT        ID
                                                               FROM            NewDials AS NewDials_1
                                                               WHERE        (Parent = 0)))))
ORDER BY NewDials.businessline, Groups.Active_Group DESC, NewDials.ID
</cfquery>