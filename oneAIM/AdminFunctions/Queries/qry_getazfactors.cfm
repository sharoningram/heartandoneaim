<cfquery name="getfactors" datasource="#request.dsn#">
SELECT        AZcauseTypes.CauseID, AZcauseTypes.CauseType, AZcauseTypes.SortORd, AZSubCauses.SubCauseID, AZSubCauses.SubCause, 
                         AZSubCauses.SortOrd AS subcausesort, AZcategories.CategoryID, AZcategories.CategoryLetter, AZcategories.CategoryName, AZfactors.FactorID, 
                         AZfactors.FactorName, AZfactors.FactorNumber, AZfactors.CategoryLetter AS azletter
FROM            AZcauseTypes LEFT OUTER JOIN
                         AZSubCauses ON AZcauseTypes.CauseID = AZSubCauses.CauseID LEFT OUTER JOIN
                         AZcategories ON AZSubCauses.SubCauseID = AZcategories.SubCauseID LEFT OUTER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter AND AZfactors.status = 1
ORDER BY AZcauseTypes.SortORd, subcausesort, AZcategories.CategoryLetter, AZfactors.FactorNumber
</cfquery>