<cfswitch expression="#erp#">
	<cfcase value="JDE">
		 <cfset costoview = "">
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			<cfif listfindnocase(request.userbus,2708) gt 0>
				<cfset costoview = listappend(costoview,"GPG")>
			</cfif>
			<cfif listfindnocase(request.userbus,3171) gt 0  or listfindnocase(request.userbus,3172)  gt 0 or  listfindnocase(request.userbus,3173)  gt 0 or listfindnocase(request.userbus,3234) gt 0>
				<cfset costoview = listappend(costoview,"EC")>
			</cfif>
		</cfif>
		<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
			<cfquery name="getids" datasource="#request.dsn#">
				select distinct parent
				from newdials
				where id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
			</cfquery>
		
			<cfif listfindnocase(valuelist(getids.parent),2708) gt 0>
				<cfset costoview = listappend(costoview,"GPG")>
			</cfif>
			<cfif listfindnocase(valuelist(getids.parent),3171) gt 0  or listfindnocase(valuelist(getids.parent),3172)  gt 0 or  listfindnocase(valuelist(getids.parent),3173)  gt 0 or listfindnocase(valuelist(getids.parent),3234) gt 0>
				<cfset costoview = listappend(costoview,"EC")>
			</cfif>
		</cfif>
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
			<cfset costoview = "all">
		</cfif>
		<!--- <Cfoutput>#costoview#</cfoutput> --->
		<!--- <cfif listfindnocase(request.userlevel,"OU Admin") gt 0> --->
		<cfset colist = "">
		<cfif listfindnocase(costoview,"GPG") gt 0>
			<!--- <cfset colist = listappend(colist,"2,9,16,21,39,65,139,176,178")> --->
			<cfset colist = listappend(colist,"2,9,16,21,39,65,139,176,178")>
		</cfif>
		<cfif listfindnocase(costoview,"EC") gt 0>
			<!--- <cfset colist = listappend(colist,"13,92,124,158,165,171,173,174,181,182,186,411,427")> --->
		<cfset colist = listappend(colist,"181,182,186")>
		</cfif>
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
			<cfset colist = "2,9,16,21,39,65,139,176,178,181,182,186">
		</cfif>
		<cfquery name="getERPcontracts" datasource="#request.dsn#">
			SELECT  PROJECT_NUMBER, MAX(PROJECT_DESC) AS PROJECT_DESC
			FROM  Laborhours
			WHERE left(PROJECT_NUMBER,3) = '434' and (PROJECT_DESC IS NOT NULL) AND (Already_Grouped = 0)
			<cfif trim(costoview) eq "">
				and 1=2
			<cfelseif costoview eq "all">
				 and (Company_Num IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#colist#" list="Yes">))
			<cfelseif costoview neq "all">
				
					 and (Company_Num IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#colist#" list="Yes">))
				
			</cfif>
					
			GROUP BY PROJECT_NUMBER
			ORDER BY  PROJECT_NUMBER,PROJECT_DESC
		</cfquery>
	</cfcase>
</cfswitch>