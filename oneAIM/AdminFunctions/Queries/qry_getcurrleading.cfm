<cfparam name="lookupyear" default="#year(now())#">
<cfquery name="getcurrleading" datasource="#request.dsn#">
SELECT        DashboardTrending.dbtID, DashboardTrending.dbtType, DashboardTrending.zplanned, DashboardTrending.zcomplete, DashboardTrending.dbtyear, 
                         DashboardTrending.updateby, DashboardTrending.updatedate, DashboardTrending.psiPlanned, DashboardTrending.psiComplete, 
                         DashboardTrending.hsseaccplanned, DashboardTrending.hsseacccomplete, DashboardTrending.rmPlanned, DashboardTrending.rmComplete, 
                         DashboardTrending.DialID, NewDials.Name
FROM            DashboardTrending INNER JOIN
                         NewDials ON DashboardTrending.DialID = NewDials.ID
WHERE        (DashboardTrending.dbtyear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) and NewDials.businessline in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="0,#request.userBUs#" list="Yes">)
</cfquery>