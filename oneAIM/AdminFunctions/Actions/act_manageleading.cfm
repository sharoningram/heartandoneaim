<cfparam name="submittype" default="">
<cfswitch expression="#submittype#">
	<cfcase value="addobjective">
		<cfparam name="DLid" default="0">
		<cfparam name="zplanned" default="">
		<cfparam name="zcomplete" default="0">
		<cfparam name="psiPlanned" default="">
		<cfparam name="psiComplete" default="0">
		<cfparam name="hsseaccplanned" default="">
		<cfparam name="hsseacccomplete" default="0">
		<cfparam name="rmPlanned" default="">
		<cfparam name="rmComplete" default="0">
		<cfparam name="lookupyear" default="">
		<cfif dlid gt 0 and trim(zplanned) neq '' and trim(psiPlanned) neq '' and trim(hsseaccplanned) neq '' and trim(rmPlanned) neq '' and listfind("#year(now())-1#,#year(now())#",lookupyear) gt 0>
			<cfquery name="checkcurr" datasource="#request.dsn#">
				SELECT        dbtID
				FROM            DashboardTrending
				WHERE        (dbtyear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) AND (DialID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dlid#">)
			</cfquery>
			<cfif checkcurr.recordcount eq 0>
			<cfif trim(zcomplete) eq ''>
				<cfset zcomplete = 0>
			</cfif>
			<cfif trim(psiComplete) eq ''>
				<cfset psiComplete = 0>
			</cfif>
			<cfif trim(hsseacccomplete) eq ''>
				<cfset hsseacccomplete = 0>
			</cfif>
			<cfif trim(rmComplete) eq ''>
				<cfset rmComplete = 0>
			</cfif>
				<cfquery name="addgoal" datasource="#request.dsn#">
					insert into DashboardTrending (   dbtyear, zplanned, zcomplete, updateby, updatedate, psiPlanned, psiComplete, hsseaccplanned, hsseacccomplete, rmPlanned, rmComplete, 
                         DialID)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">,<cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#zplanned#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#zcomplete#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#psiPlanned#">, <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#psiComplete#">, <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#hsseaccplanned#">, <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#hsseacccomplete#">, <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#rmPlanned#">, <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#rmComplete#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#dlid#">)
				</cfquery>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.leading#" method="post" name="frmobjret">
				<input type="hidden" name="lookupyear" value="#lookupyear#">
			</form>
			<script type="text/javascript">
				document.frmobjret.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="updateobjective">
		<cfparam name="liid" default="0">
		<cfparam name="zplanned" default="">
		<cfparam name="zcomplete" default="0">
		<cfparam name="psiPlanned" default="">
		<cfparam name="psiComplete" default="0">
		<cfparam name="hsseaccplanned" default="">
		<cfparam name="hsseacccomplete" default="0">
		<cfparam name="rmPlanned" default="">
		<cfparam name="rmComplete" default="0">
		<cfparam name="lookupyear" default="">
		<cfif liid gt 0 and trim(zplanned) neq '' and trim(psiPlanned) neq ''  and trim(hsseaccplanned) neq ''  and trim(rmPlanned) neq '' and listfind("#year(now())-1#,#year(now())#",lookupyear) gt 0>
			<cfif trim(zcomplete) eq ''>
				<cfset zcomplete = 0>
			</cfif>
			<cfif trim(psiComplete) eq ''>
				<cfset psiComplete = 0>
			</cfif>
			<cfif trim(hsseacccomplete) eq ''>
				<cfset hsseacccomplete = 0>
			</cfif>
			<cfif trim(rmComplete) eq ''>
				<cfset rmComplete = 0>
			</cfif>
			<cfquery name="updategoal" datasource="#request.dsn#">
				update DashboardTrending
				set zplanned = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#zplanned#">,
					zcomplete = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#zcomplete#">,
					psiPlanned = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#psiPlanned#">, 
					psiComplete = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#psiComplete#">, 
					hsseaccplanned = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#hsseaccplanned#">, 
					hsseacccomplete = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#hsseacccomplete#">, 
					rmPlanned = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#rmPlanned#">, 
					rmComplete = <cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#rmComplete#">,
					updateby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,
					updatedate = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">
				where dbtID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#liid#">
			</cfquery>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.leading#" method="post" name="frmobjret">
				<input type="hidden" name="lookupyear" value="#lookupyear#">
				<input type="hidden" name="liid" value="0">
			</form>
			<script type="text/javascript">
				document.frmobjret.submit();
			</script>
		</cfoutput>
	</cfcase>
</cfswitch>