<cfparam name="type"  default="">
<cfparam name="BU"  default="">

<cfif trim(type) neq ''>
	<cfswitch expression="#type#">
		<cfcase value="OU">
			<cfif trim(BU) neq '' and isnumeric(BU)>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        ID, Name
					FROM            NewDials
					WHERE        (Parent = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (status <> 99) and isgroupdial = 0
					ORDER BY Name
				</cfquery><!---   onchange="this.style.border='1px solid 5f2468';" --->
				<select name="ou" size="1" class="selectgen" onchange="buildbslist(this.value);">
					{zz{<option value="">-- Select One --</option>
					<cfoutput query="getOU"><option value="#id#">#name#</option></cfoutput>}zz}
					</select>
					
			</cfif>
		</cfcase>
		<cfcase value="BS">
			<cfif trim(BU) neq '' and isnumeric(BU)>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        ID, Name
					FROM            NewDials
					WHERE        (Parent = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (status <> 99) and isgroupdial = 0
					ORDER BY Name
				</cfquery>
				<cfquery name="getuser" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
					FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
					WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (UserRoles.UserRole in ( 'user'))
					order by Users.Firstname, Users.Lastname
				</cfquery>
				<select name="bs" size="1" class="selectgen" <cfif getOU.recordcount eq 0>disabled</cfif>>
					{zz{<option value="">-- Select One --</option>
					<cfoutput query="getOU"><option value="#id#">#name#</option></cfoutput>}zz}
					</select><!---  onchange="this.style.border='1px solid 5f2468';"  --->
					
			</cfif>
		</cfcase>
	</cfswitch>
</cfif>