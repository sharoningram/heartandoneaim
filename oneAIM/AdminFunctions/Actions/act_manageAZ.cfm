<cfparam name="submittype" default="">

<cfswitch expression="#submittype#">
	<cfcase value="addfactor">
		<cfparam name="catletter" default="">
		<cfparam name="newfactor" default="">
		<cfif trim(catletter) neq '' and trim(newfactor) neq ''>
			<cfquery name="getmaxnum" datasource="#request.dsn#">
				SELECT        MAX(FactorNumber) AS hignnum
				FROM            AZfactors
				WHERE        (CategoryLetter = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#catletter#">) and status = 1
			</cfquery>
			
			
			
			
			<cfif getmaxnum.recordcount gt 0>
				<cfif trim(getmaxnum.hignnum) neq ''>
					<cfset nextnum = getmaxnum.hignnum+1>
				<cfelse>
					<cfset nextnum = 1>
				</cfif>
			<cfelse>
				<cfset nextnum = 1>
			</cfif>
			
			<cfquery name="changeother" datasource="#request.dsn#" result="updother">
				update AZfactors
				set FactorNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#nextnum#">
				where (CategoryLetter = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#catletter#">)
				and FactorNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getmaxnum.hignnum#">
				and factorname = 'other'
			</cfquery>
			<cfif updother.recordcount eq 1>
				<cfquery name="addfactor" datasource="#request.dsn#">
					insert into AZfactors (CategoryLetter, FactorName, FactorNumber)
					values ( <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#catletter#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newfactor#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getmaxnum.hignnum#">)
				</cfquery>
			<cfelse>
				<cfquery name="addfactor" datasource="#request.dsn#">
					insert into AZfactors (CategoryLetter, FactorName, FactorNumber)
					values ( <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#catletter#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newfactor#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#nextnum#">)
				</cfquery>
			</cfif>		
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.azmanagement####catletter#" method="post" name="catretfrm">
				<input type="hidden" name="catletter" value="#catletter#">
				<input type="hidden" name="swide" value="#swide#">
			</form>
			<script type="text/javascript">
				document.catretfrm.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="deletefactor">
		<cfparam name="factnum" default="0">
		<cfparam name="catletter" default="">
		<cfparam name="fid" default="0">
		<cfif factnum neq 0 and trim(catletter) neq '' and fid neq 0 >
			<cfquery name="getmaxnum" datasource="#request.dsn#">
				SELECT        MAX(FactorNumber) AS hignnum
				FROM            AZfactors
				WHERE        (CategoryLetter = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#catletter#">) and status = 1
			</cfquery>
			<cfif factnum eq getmaxnum.hignnum>
				<cfquery name="deletefactor" datasource="#request.dsn#">
					update AZfactors
					set status = 0
					where factorid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fid#">
				</cfquery>
			<cfelse>
				<cfset startnum = factnum>
				<cfloop from="#factnum+1#" to="#getmaxnum.hignnum#" index="i">
						<cfquery name="renumfactors" datasource="#request.dsn#">
							update AZfactors
							set FactorNumber =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#startnum#">
							where CategoryLetter = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#catletter#">
							and FactorNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">
							and status = 1
						</cfquery>					
					<cfset startnum = startnum+1>
				</cfloop>
				<cfquery name="deletefactor" datasource="#request.dsn#">
					update AZfactors
					set status = 0
					where factorid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fid#">
				</cfquery>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.azmanagement####catletter#" method="post" name="catretfrm">
				<input type="hidden" name="catletter" value="#catletter#">
				<input type="hidden" name="swide" value="#swide#">
			</form>
			<script type="text/javascript">
				document.catretfrm.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="editfactor">
		<cfparam name="catletter" default="">
		<cfparam name="fid" default="0">
		<cfparam name="newfactor" default="">
		<cfif trim(catletter) neq '' and fid neq 0 and  trim(newfactor) neq ''>
			<cfquery name="deletefactor" datasource="#request.dsn#">
				update AZfactors
				set factorname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newfactor#">
				where factorid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fid#">
			</cfquery>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.azmanagement####catletter#" method="post" name="catretfrm">
				<input type="hidden" name="catletter" value="#catletter#">
				<input type="hidden" name="swide" value="#swide#">
			</form>
			<script type="text/javascript">
				document.catretfrm.submit();
			</script>
		</cfoutput>
	</cfcase>
</cfswitch>