<cfparam name="submittype" default="">

<cfswitch expression="#submittype#">
	<cfcase value="assigncontracts">
		<cfparam name="erp" default="">
		<cfswitch expression="#erp#">
		<cfcase value="jde">
			<cfloop list="#fieldnames#" index="i">
				<cfif i contains "projectnumber">
					<cfset thenum = listgetat(i,2,"_")>
					<cfset thegnum = evaluate("groupnum_#thenum#")>
					<cfset theloc = evaluate("site_#thenum#")>
					<cfif trim(thegnum) neq '' and trim(theloc) neq ''>
						<cfquery name="addjoin" datasource="#request.dsn#">
							insert into ProjectsByGroup (Project_Number, Project_Desc, Group_Number, Entered_By, Date_Time, ERPsys, locationid) 
							values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#evaluate(i)#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#evaluate("projectdesc_#thenum#")#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#thegnum#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#erp#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theloc#">)
						</cfquery>
						<cfquery name="thisQuery" datasource="#request.dsn#">
							UPDATE LABORHOURS SET ALREADY_GROUPED = 1 WHERE PROJECT_NUMBER = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#evaluate(i)#">
						</cfquery>
					</cfif>
				</cfif>
			</cfloop>
			<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.erpassign#" method="post" name="retcontractsfrm">
				<input type="hidden" name="erp" value="#erp#">
			</form>
			<script type="text/javascript">
				document.retcontractsfrm.submit();
			</script>
			</cfoutput>
			</cfcase>
		</cfswitch>
	</cfcase>
	<cfcase value="reassigncontracts">
		<cfparam name="erp" default="">
		<cfparam name="lookuptype" default="exact">
		<cfparam name="contractnum" default="">
		<cfswitch expression="#erp#">
		<cfcase value="jde">
			<cfloop list="#fieldnames#" index="i">
				<cfif i contains "projectnumber">
					<cfset prjnum = trim(evaluate(i))>
					<cfset thenum = listgetat(i,2,"_")>
					<cfset thegnum = evaluate("groupnum_#thenum#")>
					<cfset theloc = evaluate("site_#thenum#")>
					<cfif trim(thegnum) neq ''  and trim(theloc) neq ''>
						<cfoutput>
							<cfquery name="getsubparentcontracts" datasource="#request.commondsn#">
								SELECT       Parent_Num
								FROM            CostCenter
								WHERE        (Num = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#prjnum#">)
							</cfquery>
							<cfquery name="getsubcontracts" datasource="#request.commondsn#">
								SELECT       Num,  Parent_Num
								FROM            CostCenter
								WHERE        (Parent_Num = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getsubparentcontracts.Parent_Num#">)
							</cfquery>
							
							<cfif getsubcontracts.recordcount gt 0>
								<cfloop query="getsubcontracts">
																	
									<cfquery name="thisQuery" datasource="#request.dsn#">
										UPDATE PROJECTSBYGROUP SET 
										GROUP_NUMBER = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#thegnum#">,
										locationid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theloc#">
										WHERE PROJECT_NUMBER = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Num#">
									</cfquery>
									
								</cfloop>
							</cfif>
						</cfoutput>
					</cfif>
				</cfif>
			</cfloop>
			<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.erpreassign#" method="post" name="retcontractsfrm">
				<input type="hidden" name="erp" value="#erp#">
				<input type="hidden" name="lookuptype" value="#lookuptype#">
				<input type="hidden" name="frmclientname" value="#frmclientname#">
				<input type="hidden" name="contractnum" value="#contractnum#">
			</form>
			<script type="text/javascript">
				document.retcontractsfrm.submit();
			</script>
			</cfoutput>
		</cfcase>
		</cfswitch>
	</cfcase>
</cfswitch>