<cfparam name="submittype" default="">

<cfswitch expression="#submittype#">
	<cfcase value="addclient">
		<cfset msg = 0>
		<cfparam name="clientname" default="">
		<cfif trim(clientname) neq ''>
			<cfquery name="checkforname" datasource="#request.dsn#">
				SELECT        ClientID, ClientName
				FROM            Clients
				WHERE        (ClientName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#clientname#">)
			</cfquery>
			<cfif checkforname.recordcount eq 0>
				<cfquery name="addclient" datasource="#request.dsn#">
					insert into clients (clientname)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#clientname#">)
				</cfquery>
				<cfset msg = 1>
				<cfset clid = 0>
			<cfelse>
				<cfset msg = 2>
				<cfset clid = checkforname.ClientID>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.clients#" name="clientback" method="post">
				<input type="hidden" name="msg" value="#msg#">
				<input type="hidden" name="clid" value="#clid#">
			</form>
			<script type="text/javascript">
				document.clientback.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="updateclient">
		<cfset msg = 0>
		<cfparam name="clientname" default="">
		<cfparam name="clid" default="0">
		<cfparam name="status" default="">
		<cfif trim(clientname) neq '' and clid gt 0 and trim(status) neq ''>
			<cfquery name="checkforname" datasource="#request.dsn#">
				SELECT        ClientID, ClientName
				FROM            Clients
				WHERE        (ClientName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#clientname#">) and clientid <> <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#clid#">
			</cfquery>
			<cfif checkforname.recordcount eq 0>
				<cfquery name="updateclient" datasource="#request.dsn#">
					update clients 
					set clientname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#clientname#">,
					status = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#status#">
					where clientid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#clid#">
				</cfquery> 
				<cfset msg = 3>
			<cfelse>
				<cfset msg = 4>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.clients#" name="clientback" method="post">
				<input type="hidden" name="msg" value="#msg#">
				<input type="hidden" name="clid" value="#clid#">
			</form>
			<script type="text/javascript">
				document.clientback.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="addcountry">
		<cfset msg = 0>
		<cfparam name="countryname" default="">
		<cfif trim(countryname) neq ''>
			<cfquery name="checkforname" datasource="#request.dsn#">
				SELECT        countryID, countryName
				FROM            countries
				WHERE        (countryName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#countryName#">)
			</cfquery>
			<cfif checkforname.recordcount eq 0>
				<cfquery name="addcountry" datasource="#request.dsn#">
					insert into countries (countryName)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#countryName#">)
				</cfquery>
				<cfset msg = 1>
				<cfset clid = 0>
			<cfelse>
				<cfset msg = 2>
				<cfset clid = checkforname.countryID>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.countries#" name="clientback" method="post">
				<input type="hidden" name="msg" value="#msg#">
				<input type="hidden" name="clid" value="#clid#">
			</form>
			<script type="text/javascript">
				document.clientback.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="updatecountry">
		<cfset msg = 0>
		<cfparam name="countryname" default="">
		<cfparam name="clid" default="0">
		<cfparam name="status" default="">
		<cfif trim(countryname) neq '' and clid gt 0 and trim(status) neq ''>
			<cfquery name="checkforname" datasource="#request.dsn#">
				SELECT        countryID, countryName
				FROM            countries
				WHERE        (countryName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#countryName#">) and countryID <> <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#clid#">
			</cfquery>
			<cfif checkforname.recordcount eq 0>
				<cfquery name="updatecountry" datasource="#request.dsn#">
					update countries 
					set countryName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#countryName#">,
					status = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#status#">
					where countryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#clid#">
				</cfquery> 
				<cfset msg = 3>
			<cfelse>
				<cfset msg = 4>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.countries#" name="clientback" method="post">
				<input type="hidden" name="msg" value="#msg#">
				<input type="hidden" name="clid" value="#clid#">
			</form>
			<script type="text/javascript">
				document.clientback.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="addoccupation">
		<cfset msg = 0>
		<cfparam name="occupation" default="">
		<cfif trim(occupation) neq ''>
			<cfquery name="checkforname" datasource="#request.dsn#">
				SELECT        occupationID, occupation
				FROM            occupations
				WHERE        (occupation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#occupation#">)
			</cfquery>
			<cfif checkforname.recordcount eq 0>
				<cfquery name="addoccupation" datasource="#request.dsn#">
					insert into occupations (occupation)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#occupation#">)
				</cfquery>
				<cfset msg = 1>
				<cfset clid = 0>
			<cfelse>
				<cfset msg = 2>
				<cfset clid = checkforname.countryID>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.occupations#" name="clientback" method="post">
				<input type="hidden" name="msg" value="#msg#">
				<input type="hidden" name="clid" value="#clid#">
			</form>
			<script type="text/javascript">
				document.clientback.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="updateoccupation">
		<cfset msg = 0>
		<cfparam name="occupation" default="">
		<cfparam name="clid" default="0">
		<cfparam name="status" default="">
		<cfif trim(occupation) neq '' and clid gt 0 and trim(status) neq ''>
			<cfquery name="checkforname" datasource="#request.dsn#">
				SELECT        occupationID, occupation
				FROM            occupations
				WHERE        (occupation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#occupation#">) and occupationID <> <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#clid#">
			</cfquery>
			<cfif checkforname.recordcount eq 0>
				<cfquery name="updateoccupation" datasource="#request.dsn#">
					update occupations 
					set occupation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#occupation#">,
					status = <cfqueryparam cfsqltype="CF_SQL_BIT" value="#status#">
					where occupationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#clid#">
				</cfquery> 
				<cfset msg = 3>
			<cfelse>
				<cfset msg = 4>
			</cfif>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.occupations#" name="clientback" method="post">
				<input type="hidden" name="msg" value="#msg#">
				<input type="hidden" name="clid" value="#clid#">
			</form>
			<script type="text/javascript">
				document.clientback.submit();
			</script>
		</cfoutput>
	</cfcase>
</cfswitch>	