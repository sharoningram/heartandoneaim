<cfparam name="mode" default="">

<cfif mode eq "edit">
	<cfquery name="updatedial" datasource="#request.dsn#">
		update newdials
		set name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form.dialname#">
		where id =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form.thisdial#">
	</cfquery>
<cfelseif mode eq "editdash">
	<cfparam name="thisdial" default="">
	<cfparam name="bu" default="">
	<cfparam name="ou" default="">
	<cfif trim(thisdial) neq '' and trim(bu) neq '' and trim(ou) neq ''>
		<cfquery name="updatdial" datasource="#request.dsn#">
			update groups
			set business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">,
			buid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">,
			ouid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#">,
			<cfif trim(bs) neq ''>
			businessstreamid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bs#">
			<cfelse>
			businessstreamid = NULL
			</cfif>
			where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#thisdial#">
		</cfquery>
		
		<cfquery name="getbuname" datasource="#request.dsn#">
			select name
			from newdials
			where id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">
		</cfquery>
		
		<cfquery name="getouname" datasource="#request.dsn#">
			select name
			from newdials
			where id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#">
		</cfquery>
		<cfset nbsname = "">
		<cfif trim(bs) neq ''>
		<cfquery name="getbsname" datasource="#request.dsn#">
			select name
			from newdials
			where id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bs#">
		</cfquery>
		<cfset nbsname = getbsname.name>
		<cfelse>
			<cfset nbsname = "">
		</cfif>
		
		<cfquery name="updateobs" datasource="#request.HEART_DS#">
			update Observations
			set ObservationBU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getbuname.name#">, 
			ObservationOU = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getouname.name#">, 
            BusinessStream = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#nbsname#">,
			BUID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">, 
			OUID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ou#">
			WHERE        (ProjectNumber =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#thisdial#">)
		</cfquery>
		
		
	</cfif>
<cfelseif mode eq "delete">
	<cfquery name="getparentid" datasource="#request.dsn#">
		SELECT     Parent
		FROM         NewDials
		WHERE    (ID =
                          (SELECT     Parent
                            FROM          NewDials AS NewDials_1
                            WHERE      (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#url.parent#">)))
	</cfquery>
	<cfif getparentid.parent eq 0>
		<cfquery name="updategroup" datasource="#request.dsn#">
			delete from UserBusinessLines
			where BusinessLine =  (select businessline from NewDials where ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#url.parent#">)
		</cfquery>
	</cfif>

	<cfquery name="updategroup" datasource="#request.dsn#">
		update newdials
		set status = 99
		where id =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#url.parent#">
	</cfquery>
	
	
<cfelseif mode eq "add">
	<cfquery name="getparentbl" datasource="#request.dsn#">
		select parent, businessline
		from newdials
		where id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form.parent#">
	</cfquery>
	<cfset usestat = 1>
	<cfset useblid = 1>
	<cfif getparentbl.parent neq 0>
		<cfset useblid = getparentbl.businessline>
	<cfelse>
		<cfquery name="gtemaxbl" datasource="#request.dsn#">
			select max(businessline) as maxbl
			from newdials
		</cfquery>
			<cfset useblid = gtemaxbl.maxbl+1>
			<cfset usestat = 0>
			
			<!--- <cftry> --->
			<cfquery name="addjoin" datasource="#request.dsn#">
				insert into UserBusinessLines ( UserID, BusinessLine)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useblid#">)
			</cfquery>
			<!--- <cfcatch type="Any"></cfcatch>
			</cftry> --->
			<!--- <cfquery name="updateadmbl" datasource="#request.dsn#">
				update safetyuser
				set business_line_id = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newuserBL#">
				where UserEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">
			</cfquery> --->
	</cfif>
	
	
	
	<cfquery name="adddial" datasource="#request.dsn#">
		insert into newdials (name, parent,  isgroupdial, businessline, status)
		values(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form.dialname#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form.parent#">,0,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useblid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usestat#">)
	</cfquery>
	
</cfif>


<cflocation url="#self#?fuseaction=#attributes.xfa.dialadmin#&msg=yes" addtoken="No">