<cfparam name="setthedate" default="">
<cfparam name="datefor" default="">
<cfif trim(setthedate) neq '' and trim(datefor) neq ''>
	<cfset themonv = month(datefor)>
	<cfset theyearv = year(datefor)>
	<cfquery name="delold" datasource="#request.dsn#">
		delete from ManHourDeadLines
		WHERE        (DLyear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theyearv#">) AND (DLmonth = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#themonv#">)
	</cfquery>
	
	<cfquery name="adddl" datasource="#request.dsn#">
		insert into ManHourDeadLines (DLyear, DLmonth, DLdate)
		values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theyearv#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#themonv#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#setthedate#">)
	</cfquery>
</cfif>