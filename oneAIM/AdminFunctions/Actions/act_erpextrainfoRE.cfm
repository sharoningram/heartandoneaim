
<cfif trim(contractnum) neq '' or trim(frmclientname) neq ''>
<cfset pnumlist = "">

	<cfloop query="getcontractlist">
		<cfif trim(project_number) neq ''>
			<cfset pnumlist = listappend(pnumlist,project_number)>
		</cfif>
	</cfloop>
	
<cfquery name="getextradata" datasource="common"><!---QUERYREPORT:PASSED--->
	select distinct num, division_desc,product_line_desc,desc3, desc4,Company_Name
	 from vw_safety
	where num in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#pnumlist#" list="Yes">)
</cfquery>
<cfset divstruct = {}>
<cfset plstruct = {}>
<cfset desc3struct = {}>
<cfset desc4struct = {}>
<cfset compstruct = {}>
<cfloop query="getextradata">
	<cfif not structkeyexists(divstruct,num)>
		<cfset divstruct[num] = division_desc>
	</cfif>
	<cfif not structkeyexists(plstruct,num)>
		<cfset plstruct[num] = product_line_desc>
	</cfif>
	<cfif not structkeyexists(desc3struct,num)>
		<cfset desc3struct[num] = desc3>
	</cfif>
	<cfif not structkeyexists(desc4struct,num)>
		<cfset desc4struct[num] = desc4>
	</cfif>
	<cfif not structkeyexists(compstruct,num)>
		<cfset compstruct[num] = Company_Name>
	</cfif>
</cfloop>

</cfif>
