<cfparam name="submittype" default="">
<cfswitch expression="#submittype#">
	<cfcase value="addgoal">
		<cfparam name="DLid" default="0">
		<cfparam name="trirgoal" default="">
		<cfparam name="ltirgoal" default="">
		<cfparam name="airgoal" default="">
		<cfparam name="trir" default="">
		<cfparam name="ltir" default="">
		<cfparam name="air" default="">
		<cfparam name="targetallocated" default="">
		<cfparam name="lookupyear" default="">
		<cfparam name="bu" default="0">
		<cfparam name="ou" default="0">
		<cfset useblid = 0>
			<cfif trim(targetallocated) eq "Global">
				<cfset useblid = 2707>
			<cfelseif  trim(targetallocated) eq "Business Unit">
				<cfset useblid = bu>
			<cfelseif  trim(targetallocated) eq "Operating Unit">
				<cfset useblid = ou>
			</cfif>
		<cfif trim(targetallocated) neq '' and useblid gt 0>
			
			 <cfquery name="checkcurr" datasource="#request.dsn#">
				SELECT        ObjID
				FROM            Objectives
				WHERE        (ObjYear = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">) AND (DialID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useblid#">)
			</cfquery>
			<cfif checkcurr.recordcount eq 0>
				<cfquery name="addgoal" datasource="#request.dsn#">
					insert into Objectives (ObjYear, TRIRgoal, DialID, LTIRgoal, AIRgoal, updateby, updatedate,allocatedto)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#lookupyear#">,<cfif trim(trir) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#trir#"><cfelse>NULL</cfif>,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useblid#">,<cfif trim(ltir) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#ltir#"><cfelse>NULL</cfif>,<cfif trim(air) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#air#"><cfelse>NULL</cfif>,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#originatorname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#targetallocated#">)
				</cfquery>
			</cfif> 
			
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.objectives#" method="post" name="frmobjret">
				<input type="hidden" name="lookupyear" value="#lookupyear#">
			</form>
			<script type="text/javascript">
				document.frmobjret.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="updategoal">
		<cfparam name="obid" default="0">
		<cfparam name="trirgoal" default="">
		<cfparam name="ltirgoal" default="">
		<cfparam name="airgoal" default="">
		<cfparam name="air" default="">
		<cfparam name="trir" default="">
		<cfparam name="ltir" default="">
		<cfparam name="lookupyear" default="">
		<cfif obid gt 0>
			<cfquery name="updategoal" datasource="#request.dsn#">
				update Objectives
				set TRIRgoal = <cfif trim(trir) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#trir#"><cfelse>NULL</cfif>,
					LTIRgoal = <cfif trim(ltir) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#ltir#"><cfelse>NULL</cfif>,
					<!--- updateby = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,
					updatedate = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,
					trir = <cfif trim(trir) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#trir#"><cfelse>NULL</cfif>,
					ltir = <cfif trim(ltir) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#ltir#"><cfelse>NULL</cfif>, --->
					airgoal = <cfif trim(air) neq ''><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#air#"><cfelse>NULL</cfif>
					
				where objid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#obid#">
			</cfquery>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.objectives#" method="post" name="frmobjret">
				<input type="hidden" name="lookupyear" value="#lookupyear#">
				<input type="hidden" name="obid" value="0">
			</form>
			<script type="text/javascript">
				document.frmobjret.submit();
			</script>
		</cfoutput>
	</cfcase>
</cfswitch>