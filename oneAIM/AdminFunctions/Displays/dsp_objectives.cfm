<div class="content1of1" align="center">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="55%" id="mytab">
	<thead>
	<tr><cfoutput>
		<td width="45%" class="bodyTextWhite">&nbsp;<cfif lookupyear eq year(now())><a href="#self#?fuseaction=#attributes.xfa.objectives#&lookupyear=#year(now())-1#"  class="bodyTextWhite"><&nbsp;#year(now())-1#</a>&nbsp;|&nbsp;<a href="#self#?fuseaction=#attributes.xfa.objectives#&lookupyear=#year(now())+1#"  class="bodyTextWhite">#year(now())+1#&nbsp;></a><cfelseif lookupyear eq year(now())-1><a href="#self#?fuseaction=#attributes.xfa.objectives#&lookupyear=#year(now())#"  class="bodyTextWhite">#year(now())#&nbsp;></a><cfelseif lookupyear gt year(now())><a href="#self#?fuseaction=#attributes.xfa.objectives#&lookupyear=#year(now())#"  class="bodyTextWhite"><&nbsp;#year(now())#</a></cfif></td>
		<td width="55%" class="bodyTextWhite">#lookupyear# Targets</td></cfoutput>
	</tr>
	</thead>
	<tr>
		<td colspan="2"> 
		<table cellpadding="3" cellspacing="0" border="0" class="ltTeal" width="100%">
			<tr class="whitebg">
				<td width="40%"><strong class="bodyTextGrey">Organisational Level</strong></td>
				<td align="center" width="8%"><strong class="bodyTextGrey">TRIR</strong></td>
				
				<td align="center" width="8%"><strong class="bodyTextGrey">LTIR</strong></td>
				
				<td align="center" width="8%"><strong class="bodyTextGrey">AIR</strong></td>
				
			</tr>
	
	<tbody>
	<cfset ctr = 0>
	<cfoutput>
	<cfloop list="#listsort(structkeylist(fulltopdownstruct),'numeric')#" index="i">
	<cfif structkeyexists(currobjstruct,listgetat(fulltopdownstruct[i],1,'^'))>
	<cfset ctr = ctr+1>
	
	<tr  <cfif ctr mod 2 is 0>class="whitebg"<cfelse>class="formlable"</cfif>>
		<td class="bodyTextGrey" nowrap width="20%">#repeatstring("&nbsp;&nbsp;",listgetat(fulltopdownstruct[i],2,"^"))#<a href="#self#?fuseaction=#attributes.xfa.objectives#&obid=#currobjstruct[listgetat(fulltopdownstruct[i],1,'^')]#&lookupyear=#lookupyear#&pid=#listgetat(fulltopdownstruct[i],4,"^")#&thid=#listgetat(fulltopdownstruct[i],1,"^")#">#listgetat(fulltopdownstruct[i],3,"^")#</a></td>
		<td class="bodyTextGrey" align="center" width="8%"><cfif structkeyexists(currtrirgoalstruct,listgetat(fulltopdownstruct[i],1,'^'))>#numberformat(currtrirgoalstruct[listgetat(fulltopdownstruct[i],1,'^')],"0.00")#</cfif></td>
		<!--- <cfif lookupyear eq year(now())-1>
			<td class="bodytext" align="center" width="8%"><cfif structkeyexists(currtrirstruct,listgetat(fulltopdownstruct[i],1,'^'))>#numberformat(currtrirstruct[listgetat(fulltopdownstruct[i],1,'^')],"0.00")#</cfif></td>
		</cfif> --->
		<td class="bodyTextGrey" align="center" width="8%"><cfif structkeyexists(currltirgoalstruct,listgetat(fulltopdownstruct[i],1,'^'))>#numberformat(currltirgoalstruct[listgetat(fulltopdownstruct[i],1,'^')],"0.000")#</cfif></td>
		<!--- <cfif lookupyear eq year(now())-1>
			<td class="bodytext" align="center" width="8%"><cfif structkeyexists(currltirstruct,listgetat(fulltopdownstruct[i],1,'^'))>#numberformat(currltirstruct[listgetat(fulltopdownstruct[i],1,'^')],"0.000")#</cfif></td>
		</cfif> --->
		
		<td class="bodyTextGrey" align="center" width="8%"><cfif structkeyexists(currairgoalstruct,listgetat(fulltopdownstruct[i],1,'^'))>#numberformat(currairgoalstruct[listgetat(fulltopdownstruct[i],1,'^')],"0.00")#</cfif></td>
		<!--- <cfif lookupyear eq year(now())-1>
			<td class="bodytext" align="center" width="8%"><cfif structkeyexists(currairstruct,listgetat(fulltopdownstruct[i],1,'^'))>#numberformat(currairstruct[listgetat(fulltopdownstruct[i],1,'^')],"0.000")#</cfif></td>
		</cfif> --->
	</tr>
	
	</cfif>
	</cfloop></cfoutput> 

	</tbody>
</table></td></tr></table>
</div>