<div class="content1of1" align="center">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="mytab">
	<thead>
	<tr><cfoutput>
		<td width="45%"><strong><cfif lookupyear eq year(now())><a href="#self#?fuseaction=#attributes.xfa.leading#&lookupyear=#year(now())-1#"  class="bodyTextWhite"><&nbsp;#year(now())-1#</a><cfelseif lookupyear eq year(now())-1><a href="#self#?fuseaction=#attributes.xfa.leading#&lookupyear=#year(now())#"  class="bodyTextWhite">#year(now())#&nbsp;></a></cfif></strong></td>
		<td width="55%"><strong class="bodyTextWhite">#lookupyear# Leading Indicators</strong></td></cfoutput>
	</tr>
	</thead>
	<tr>
		<td colspan="2"> 
		<table cellpadding="3" cellspacing="0" border="0" class="ltTeal" width="100%">
			<tr class="whitebg">
				<td width="40%"><strong class="bodytext">Organisational Level</strong></td>
				<td align="center" width="15%" colspan="2"><strong class="bodytext">Beyond zero refresh</strong></td>
				<td align="center" width="15%" colspan="2"><strong class="bodytext">Performance standard implementation</strong></td>
				<td align="center" width="15%" colspan="2"><strong class="bodytext">HSSE Accountability</strong></td>
				<td align="center" width="15%" colspan="2"><strong class="bodytext">Risk management</strong></td>
			</tr>
			<tr class="whitebg">
				<td></td>
				<td align="center"><strong class="bodytext">Planned</strong></td>
				<td align="center"><strong class="bodytext">Complete</strong></td>
				<td align="center"><strong class="bodytext">Planned</strong></td>
				<td align="center"><strong class="bodytext">Complete</strong></td>
				<td align="center"><strong class="bodytext">Planned</strong></td>
				<td align="center"><strong class="bodytext">Complete</strong></td>
				<td align="center"><strong class="bodytext">Planned</strong></td>
				<td align="center"><strong class="bodytext">Complete</strong></td>
			</tr>
	<tbody>
<cfset ctr = 0>
	<cfoutput query="getcurrleading">
	<cfset ctr = ctr+1>
	
	<tr  <cfif ctr mod 2 is 0>class="whitebg"<cfelse>class="ltTeal"</cfif>>
		<td class="bodytext" nowrap width="20%"><a href="#self#?fuseaction=#attributes.xfa.leading#&liid=#dbtid#&lookupyear=#lookupyear#">#name#</a></td>

		<td class="bodytext" align="center">#numberformat(zplanned,"9,999,999")#</td>
		<td class="bodytext" align="center">#numberformat(zcomplete,"9,999,999")#</td>
		
		<td class="bodytext" align="center" >#numberformat(psiplanned,"9,999,999")#</td>
		<td class="bodytext" align="center" >#numberformat(psicomplete,"9,999,999")#</td>
		
		<td class="bodytext" align="center" >#numberformat(hsseaccplanned,"9,999,999")#</td>
		<td class="bodytext" align="center" >#numberformat(hsseacccomplete,"9,999,999")#</td>
		
		<td class="bodytext" align="center" >#numberformat(rmplanned,"9,999,999")#</td>
		<td class="bodytext" align="center" >#numberformat(rmcomplete,"9,999,999")#</td>
		
	</tr> 
</cfoutput>  

	</tbody>
</table></td></tr></table>
</div>