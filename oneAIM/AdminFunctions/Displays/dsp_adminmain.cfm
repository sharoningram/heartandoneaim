<div align="center" class="midboxadmin">
	<div align="center" class="midboxtitle">What would you like to do?</div>
<table cellpadding="8" cellspacing="0" border="0" align="center" width="75%">
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=groups.main" class="midboxtitletxt">Manage Project/Office Groups</a></td>
	</tr>
	<!--- <tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.erpassign" class="midboxtitletxt">Assign ERP Contracts</a></td>
	</tr>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.erpreassign" class="midboxtitletxt">Re-Assign ERP Contracts</a></td>
	</tr> --->
	<tr>
		<td></td>
	</tr>
	</cfif>
	
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=distribution.diallevel&type=FA" class="midboxtitletxt">First Alert/IRP Distribution</a></td>
	</tr>
	</cfif>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=distribution.diallevel&type=secFA" class="midboxtitletxt">Security First Alert Distribution</a></td>
	</tr>
	</cfif>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=distribution.diallevel&type=Fatality" class="midboxtitletxt">Fatality Distribution</a></td>
	</tr> 
	</cfif>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=distribution.diallevel&type=WD" class="midboxtitletxt">Weekly Roundup Distribution</a></td>
	</tr>
	<tr>
		<td></td>
	</tr>
	</cfif>
	
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=users.main" class="midboxtitletxt">Manage Users</a></td>
	</tr>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.objectives" class="midboxtitletxt">Manage Targets</a></td>
	</tr>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.mhdealine" class="midboxtitletxt">Man-Hour Entry Deadlines</a></td>
	</tr>
	</cfif>
	
	<cfif listfindnocase(request.userlevel,"Global IT") gt 0>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.dialadmin" class="midboxtitletxt">Manage System Org Structure</a></td>
	</tr>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.clients" class="midboxtitletxt">Manage Client List</a></td>
	</tr>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.countries" class="midboxtitletxt">Manage Country List</a></td>
	</tr>
	<tr>
		<td class="midboxtitletxt"><a href="index.cfm?fuseaction=adminfunctions.occupations" class="midboxtitletxt">Manage Occupation List</a></td>
	</tr>
	 </cfif>


</table><br>
</div>