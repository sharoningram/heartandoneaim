<cfset titleaddon = "Admin Functions">
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="azmanagement">
		<cfset titleaddon = titleaddon & " - Manage A-Z Categories">
	</cfcase>
	<cfcase value="dialadmin">
		<cfset titleaddon = titleaddon & " - Organisation Level Admin">
	</cfcase>
	<cfcase value="erpassign">
		<cfset titleaddon = titleaddon & " - ERP Contract Assignment">
	</cfcase>
	<cfcase value="erpreassign">
		<cfset titleaddon = titleaddon & " - ERP Contract Re-assignment">
	</cfcase>
	<cfcase value="clients">
		<cfset titleaddon = titleaddon & " - Client List Management">
	</cfcase>
	<cfcase value="countries">
		<cfset titleaddon = titleaddon & " - Country List Management">
	</cfcase>
	<cfcase value="occupations">
		<cfset titleaddon = titleaddon & " - Occupation List Management">
	</cfcase>
	<cfcase value="objectives">
		<cfset titleaddon = titleaddon & " - Objectives Management">
	</cfcase>
	<cfcase value="leading">
		<cfset titleaddon = titleaddon & " - Leading Indicator Management">
	</cfcase>
</cfswitch>
<cfoutput>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Global IT") gt 0>
		<!--- <cfif trim(titleaddon) neq ''><div align="center" class="h2">#titleaddon#</div></cfif> --->
		<table width="100%" cellpadding="2" cellspacing="0" border="0">
<!--- <tr bgcolor="ffffff">
	<td align="right" colspan="2"><img src="images/logolong.jpg" border="0" class="img"></td>
</tr> --->
<tr class="purplebg"><td class="bodytextwhitelg" width="90%">&nbsp;Admin&nbsp;Functions<cfif fusebox.fuseaction eq "erpassign">&nbsp;-&nbsp;ERP&nbsp;Contract&nbsp;Assignment<cfelseif fusebox.fuseaction eq "erpreassign">&nbsp;-&nbsp;ERP&nbsp;Contract&nbsp;Reassignment<cfelseif  fusebox.fuseaction eq "objectives">&nbsp;-&nbsp;Manage&nbsp;Targets<cfelseif  fusebox.fuseaction eq "mhdealine">&nbsp;-&nbsp;Manage&nbsp;Manhour&nbsp;Entry&nbsp;Deadlines</cfif></td><td align="right" class="bodytextwhite"><cfif trim(request.fname) neq ''>Welcome&nbsp;<cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td><td align="right"  class="helptext">&nbsp;<a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.oneAIMpath#/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');"  class="helptext">Help</a>&nbsp;</span><td align="right" >&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<table width="75%" cellpadding="0" cellspacing="0" border="0"  id="mytab">
<tr>
		<td><img src="images/spacer.gif" height="5" border="0"></td>
	</tr>
<tr>
	<td bgcolor="ffffff" colspan="2" align="right">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
				<cfif listfindnocase("erpassign,erpreassign,objectives,mhdealine",fusebox.fuseaction) gt 0>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=adminfunctions.main" class="bodytextsmpurple"><<&nbsp;Admin&nbsp;Home</a>&nbsp;&nbsp;</td>
				</cfif>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple" ><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;</td></tr></table>
		</td>
</tr>
</table>
		#Fusebox.layout#
</cfif>

</cfoutput>