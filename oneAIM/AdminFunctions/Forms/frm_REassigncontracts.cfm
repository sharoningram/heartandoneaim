<cfif isdefined("getcontractlist.recordcount")>
	<cfif getcontractlist.recordcount gt 0>
		<cfset showfrmctr = "no">
		<cfloop query="getcontractlist">
			<cfif right(trim(project_number),2) eq '00'>
				<cfset showfrmctr = "yes">
				<cfbreak>
			</cfif>
		</cfloop>
		
		<cfif showfrmctr>
	<script type="text/javascript"> 
 http = new XMLHttpRequest()
 <cfoutput query="getcontractlist">
 function popgroupinfo#currentrow#(gnum,rnum){

 //alert(rnum);


 var params = "&gnum=" + gnum +  "&rnum=" + rnum;
		var myurl = "#self#?fuseaction=#attributes.xfa.popgroupinfo#" ;
		//alert(myurl + ' ' + params);
		http.open('POST', myurl , true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopupGinfo#currentrow#;

 
 }
 
 	
 function handlePopupGinfo#currentrow#(){
		if (http.readyState == 4){
			var startpos3 = http.responseText.search("{dd{") + 4;
			var endpos3 = http.responseText.search("}dd}");
			var textout3 = http.responseText.substring(startpos3,endpos3);
			document.getElementById("SOTD#currentrow#").innerHTML=textout3;
			
			
			}
		
	}
	 
 </cfoutput>
 </script>
	<cfform action="#self#?fuseaction=#attributes.xfa.manageerpassign#" method="post" name="doassigncontracts">
<cfoutput>
	<input type="hidden" name="contractnum" value="#contractnum#">
	<input type="hidden" name="frmclientname" value="#frmclientname#">
	<input type="hidden" name="lookuptype" value="#lookuptype#">
	<input type="hidden" name="erp" value="#erp#">
	<input  type="hidden" name="submittype" value="reassigncontracts">
</cfoutput>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="mytab">
<tr><td colspan="2"> <span class="bodyTextWhite"><strong>Re-assign Contracts</strong></span></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">	
				<tr>
					<td  class="formlable"><strong>Project Number</strong></td>
					<td  class="formlable"><strong >Description</strong></td>
					<td  class="formlable"><strong>Company Name</strong></td>
					<td  class="formlable"><strong>Product Line</strong></td>
					<td  class="formlable"><strong>Division</strong></td>
					<td  class="formlable"><strong>Additional Desc.</strong></td>
					<td  class="formlable"><strong >Assigned Group<br>Site/Office Name</strong></td>
					<td  class="formlable"><strong >Project Group*</strong></td>
					<td  class="formlable"><strong>Site/Office Name*</strong></td>
				</tr>
				<cfoutput query="getcontractlist">
				<cfif right(trim(project_number),2) eq '00'>
				<input type="hidden" name="projectnumber_#currentrow#" value="#project_number#">
				<input type="hidden" name="projectdesc_#currentrow#" value="#project_desc#">
				<tr <cfif currentrow mod 2 is 1>bgcolor="ffffff"</cfif>>
					<td class="bodytext">#PROJECT_NUMBER#</td>
					<td class="bodytext">#PROJECT_DESC#</td>
					<td class="bodytext"><cfif structkeyexists(compstruct,PROJECT_NUMBER)>#compstruct[PROJECT_NUMBER]#</cfif></td>
					<td class="bodytext"><cfif structkeyexists(plstruct,PROJECT_NUMBER)>#plstruct[PROJECT_NUMBER]#</cfif></td>
					<td class="bodytext"><cfif structkeyexists(divstruct,PROJECT_NUMBER)>#divstruct[PROJECT_NUMBER]#</cfif></td>
					<td class="bodytext"><cfif structkeyexists(desc3struct,PROJECT_NUMBER)>#desc3struct[PROJECT_NUMBER]#</cfif>
						<cfif structkeyexists(desc4struct,PROJECT_NUMBER)><br>#desc4struct[PROJECT_NUMBER]#</cfif></td>
					<td class="bodytext">#group_name#<br>#SiteName#</td>
					<!--- this.style.border='1px solid 5f2468'; --->
					<td><cfselect class="selectgen" size="1" name="groupnum_#currentrow#" required="no" message="Please select a project group" id="lrgselect" onchange="popgroupinfo#currentrow#(this.value,'#currentrow#');">
						<option value="">-- Select One --</option>
						<cfloop query="getgrouplist" group="name">
						<option value="" disabled>#name#</option>
						<cfloop>
							<option value="#group_number#">&nbsp;&nbsp;#group_name#</option>
						</cfloop>
						</cfloop>
						<cfif listfindnocase("carmine.freda@amecfw.com",request.userlogin) gt 0>
						<option value="20055564">&nbsp;&nbsp;North America Constructors/PPA</option>
						</cfif>
						</cfselect></td>
						<td id="SOTD#currentrow#"><select name="site_#currentrow#" size="1" class="selectgen" id="lrgselect"><option value="">-- Select One --</option></select>
						
						</td>
					</tr>
					</cfif>
					</cfoutput>
				<!--- <tr>
					<td align="center"><cfselect class="selectgen" size="1" name="groupnum" required="Yes" message="Please select a project group" id="lrgselect">
						<option value="">-- Select One --</option>
						<cfoutput query="getgrouplist" group="name">
						<option value="" disabled>#name#</option>
						<cfoutput>
							<option value="#group_number#">&nbsp;&nbsp;#group_name#</option>
						</cfoutput>
						</cfoutput>
						</cfselect></td>
				</tr> --->
				<tr>	
					<td colspan="7"></td>
					<td><input type="submit" value="Submit" class="selectGenBTN"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</cfform>
<cfelse>
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="toptablefrm">
	<tr><td colspan="2"> <span class="bodyTextWhite"><strong>Re-assign Contracts</strong></span></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">	
				<tr>
					<td align="center"><strong class="bodytext">There are no contracts that match your search</strong></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
	</cfif>
	<cfelse>
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%"  id="toptablefrm">
	<tr><td colspan="2"> <span class="bodyTextWhite"><strong>Re-assign Contracts</strong></span></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">	
				<tr>
					<td align="center"><strong class="bodytext">There are no contracts that match your search</strong></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
	</cfif>
</cfif>

</div>