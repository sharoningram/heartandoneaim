<cfset offfridays = "">
<CFPARAM NAME="cheight" DEFAULT="110">
<CFIF isdefined("attributes.ColumnHeight")>
	<cfset cheight = #attributes.ColumnHeight#>
</CFIF>

<CFSET TheFirst = "#DateFormat(DateRequested, 'mm')#/01/#DateFormat(DateRequested, 'yyyy')#">


<cfset startpos = dayofweek(thefirst) - 1>


<CFSET DateCount = 1>

<CFOUTPUT>
<table width="50%" cellpadding=0 cellspacing=0 border=0 bordercolor="666666" align="center" class="purplebg">
	
	<tr>
		<td width="100%">
		<cfset currmonth = #dateformat(daterequested, "M/D/YYYY")#>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			
				<tr>
					<td colspan="7">
						<table border=0 width="100%"  cellpadding=0 cellspacing=0 bgcolor="ffffff">
						<tr>
						<cfloop from="1" to="12" index="monval">
							<cfif datepart("m",daterequested) eq monval>
								<cfset tdcl = "purplebg">
								<cfset txcl = "bodytextwhite">
							<cfelse>
								<cfset tdcl = "formlable">
								<cfset txcl = "bodyTextGrey">
							</cfif>
							<td class="#tdcl#"><img src="images/spacer.gif" height="1" width="10" border="0"></td>
							
							<td align="center" nowrap  class="#tdcl#"><a href="#self#?fuseaction=#attributes.xfa.mhdealine#&daterequested=#monval#/#DateCount#/#year(daterequested)#" class="#txcl#" style="text-decoration:none;">#monthasstring(monval)#</a></td>
							<td  class="#tdcl#"><img src="images/spacer.gif" height="1" width="10" border="0"></td>
						</cfloop>
							<td width="100%">&nbsp;</td>
						</tr>
			
		</table>
		</td>
	</tr>
	
 	<tr valign="top">
		<TD COLSPAN=3>
		<cfset prevy = dateadd("yyyy",-1,currmonth)>
		<cfset nexty = dateadd("yyyy",1,currmonth)>
			<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center" >
				
				<tr  class="selected_tab"> 
				    <td width="14%" ><a href="#self#?fuseaction=#fuseaction#&daterequested=#dateformat(prevy,'m/d/yyyy')#" class="bodytextwhite" style="text-decoration:none;"><<&nbsp;#datepart("yyyy",prevy)#</a></TD>
				    <td colspan="5" align="center">&nbsp;</TD>
				   <td align="right"><a href="#self#?fuseaction=#fuseaction#&daterequested=#dateformat(nexty,'m/d/yyyy')#" class="bodytextwhite" style="text-decoration:none;">#datepart("yyyy",nexty)#&nbsp;>></a></TD>
			   </TR>
				
				<tr  > 
				    <td width="14%" align=CENTER class="formlable"><strong >Sunday</strong></TD>
				    <td width="14%" align=CENTER class="formlable"><strong  >Monday</strong></TD>
				    <td width="14%" align=CENTER class="formlable"><strong  >Tuesday</strong></TD>
				    <td width="14%" align=CENTER  class="formlable"><strong >Wednesday</strong></TD>
				    <td width="14%" align=CENTER class="formlable"><strong  >Thursday</strong></TD>
				    <td width="14%" align=CENTER class="formlable"><strong  >Friday</strong></TD>
				   	<td width="14%" align=CENTER class="formlable"><strong  >Saturday</strong></TD>
				</TR>

				<TR>
	  			<CFIF StartPos NEQ 0>
	  				<CFLOOP FROM="1" TO="#StartPos#" INDEX="Loop1">
	  					<TD ALIGN="center" WIDTH="14%" HEIGHT="#cheight#" <cfif loop1 eq "1">bgcolor="dfdfdf"<cfelse>bgcolor="ffffff"</cfif>>&nbsp;</TD>
	  				</CFLOOP>
	  			</CFIF>
				</cfoutput>
	  			<CFSET WEEK1 = 7 - StartPos>
	  			<CFLOOP FROM="1" TO="#week1#" INDEX="Loop2">
	  			<cfoutput>
				<cfset checkdate = #datepart('m', daterequested)# &"/" & datecount & "/" & #datepart('yyyy', daterequested)#>
					<TD  ALIGN="left" VALIGN="TOP" HEIGHT="#cheight#" <CFIF (Month(DateRequested)) EQ (#Month(Now())#) AND (#DateCount# EQ (#Day(Now())#))  and (year(daterequested)) eq year(Now())>
				BGCOLOR="ffff9f"<cfelseif loop2 eq week1>bgcolor="dfdfdf"<cfelseif loop2 eq 1 and week1 eq 7>bgcolor="dfdfdf"<CFELSE><cfif listfind(offFridays,dateformat(checkdate,"mm/dd/yyyy")) gt 0 and dateformat(checkdate,"mm/dd/yyyy") gt "08/01/2008">bgcolor="dfdfdf"<cfelse>BGCOLOR="ffffff"</cfif></CFIF>  class="bodytext">
				<cfset linkdate = #month(daterequested)# & "/" & #DateCount# & "/" & #year(daterequested)#>
				<cfset setmhdate = dateadd("m",-1,linkdate)>
				&nbsp;<a href="#self#?fuseaction=#attributes.xfa.mhdealine#&daterequested=#daterequested#&setthedate=#linkdate#&datefor=#setmhdate#" onclick="return confirm('You will now set the deadline for #monthasstring(month(setmhdate))# #year(setmhdate)# to #dateformat(linkdate,sysdateformat)#');">#DateCount#</a>
				<br>
				<cfif dateformat(getdl.DLdate) eq linkdate>
				&nbsp;#monthasstring(getdl.dlmonth)#&nbsp;#getdl.dlyear#<br>&nbsp;Man&nbsp;Hour&nbsp;Deadline
				</cfif>
				
			</TD>
			</cfoutput>
	  		<CFSET DateCount = DateCount + 1>
	  		</CFLOOP>	  
  		</TR>
	  <CFSET WeekIndex = 0>
	  <CFSET LOOPTO = DaysInMonth(DateRequested) - 1>
	  <CFLOOP FROM="#week1#" TO="#loopto#" INDEX="Loop3">
	  	<CFIF WeekIndex EQ 0><TR></CFIF>
			<cfoutput>
			<cfset needcolor = "no">
			<cfif loop3 eq week1>
				<cfset needcolor = "yes">
			<cfelseif loop3 eq week1 + 7 or loop3 eq week1 + 14 or loop3 eq week1 + 21 or loop3 eq week1 + 28>
				<cfset needcolor = "yes">
			<cfelseif loop3 eq week1 + 6 or loop3 eq week1 + 13 or loop3 eq week1 + 20 or loop3 eq week1 + 27>
				<cfset needcolor = "yes">
			</cfif>
			<cfset checkdate = #datepart('m', daterequested)# &"/" & datecount & "/" & #datepart('yyyy', daterequested)#>
   			<TD ALIGN="left" VALIGN="TOP"  HEIGHT="#cheight#"
		<CFIF (Month(DateRequested)) EQ (#Month(Now())#) AND (#DateCount# EQ (#Day(Now())#)) and (year(daterequested)) eq year(Now())>BGCOLOR="ffff9f"
				<cfelseif loop3 eq week1>bgcolor="dfdfdf"<cfelseif needcolor eq "yes">bgcolor="dfdfdf"<CFELSE><cfif listfind(offFridays,dateformat(checkdate,"mm/dd/yyyy")) gt 0 and dateformat(checkdate,"mm/dd/yyyy") gt "8/01/2008">bgcolor="dfdfdf"<cfelse>BGCOLOR="ffffff"</cfif></CFIF> class="bodytext">
			<cfset linkdate = #month(daterequested)# & "/" & #DateCount# & "/" & #year(daterequested)#>
			<cfset setmhdate = dateadd("m",-1,linkdate)>
			&nbsp;<a href="#self#?fuseaction=#attributes.xfa.mhdealine#&daterequested=#daterequested#&setthedate=#linkdate#&datefor=#setmhdate#" onclick="return confirm('You will now set the deadline for #monthasstring(month(setmhdate))# #year(setmhdate)# to #dateformat(linkdate,sysdateformat)#');">#DateCount#</a>
				<br>
				<cfif dateformat(getdl.DLdate) eq linkdate>
				&nbsp;#monthasstring(getdl.dlmonth)#&nbsp;#getdl.dlyear#<br>&nbsp;Man&nbsp;Hour&nbsp;Deadline
				</cfif>
		
	</TD>
		</cfoutput>
		<CFSET WeekIndex = WeekIndex + 1><CFSET DateCount = DateCount + 1>
	  <CFIF WeekIndex EQ 7></TR><CFSET WeekIndex = 0></CFIF>
	  </CFLOOP><cfoutput>
	  <CFIF WeekIndex NEQ 0>
	 <CFLOOP FROM="#WeekIndex#" TO="6" INDEX="Loop4">
	<TD ALIGN="center"  HEIGHT="#cheight#" <cfif loop4 eq 6>bgcolor="dfdfdf"<cfelse>bgcolor="ffffff"</cfif>>
		&nbsp;</TD>
	</CFLOOP>
	</TR>
  </CFIF>
</TABLE>
</TD>
</TR>
</TABLE>
</td>

</tr>
</table>
</cfoutput>
