<cfif isdefined("getclientdetail.recordcount")>
<div class="content1of1" align="center">
<cfparam name="msg" default="">
<cfform action="#self#?fuseaction=#attributes.xfa.managefunctions#" method="post" name="updateclient">
<input type="hidden" name="submittype" value="updateclient">
<cfoutput>
<input type="hidden" name="clid" value="#clid#">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
	<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Update Client</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	<cfif msg eq 3>
	<tr>
		<td class="bodytext" align="center"><strong style="color:008000;">Client Record Updated</strong></td>
	</tr>
	<cfelseif msg eq 4>
	<tr>
		<td class="bodytext" align="center"><strong style="color:red;">Client Name Could Not Be Updated, Another Client With This Name Already Exists</strong></td>
	</tr>
	</cfif>
	<tr>
		<td class="bodytext" align="center"><strong>Client Name:</strong></td>
	</tr>
	<tr>
		<td align="center"><cfinput  type="text" name="clientname" size="30" value="#getclientdetail.clientname#" class="selectgen" required="Yes" message="Please enter a client name" maxlength="125">
			
			</td>
		</tr>
	<tr>
		<td class="bodytext" align="center"><strong>Status:</strong></td>
	</tr>
	<tr>
		<td align="center"><select name="status" size="1" class="selectgen">
										<option value="1" <cfif getclientdetail.status eq 1>selected</cfif>>Active</option>
										<option value="0" <cfif getclientdetail.status neq 1>selected</cfif>>In-Active</option>
									</select></td>
	</tr>
									
	<tr>
		<td align="center"><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
</table>
</td></tr></table>
</cfoutput>
</cfform>
</div>
</cfif>