<div class="content1of1" align="center">
<cfparam name="msg" default="">
<cfparam name="DLid" default="0">


<cfform action="#self#?fuseaction=#attributes.xfa.manageleading#" method="post" name="addobjective">
<cfoutput>
<cfif liid eq 0>
<input type="hidden" name="submittype" value="addobjective">
<input type="hidden" name="lookupyear" value="#lookupyear#">
<cfelse>
<input type="hidden" name="lookupyear" value="#lookupyear#">
<input type="hidden" name="submittype" value="updateobjective">
<input type="hidden" name="liid" value="#liid#">
</cfif>
</cfoutput>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
	<tr>
		<td colspan="2"><strong class="bodyTextWhite"><cfif liid eq 0>Add<cfelse>Update</cfif> Leading Indicators</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	<tr>
		<td class="bodytext" align="center"><strong><cfoutput>#lookupyear# Leading Indicators</cfoutput></strong></td>
	</tr>
	<tr>
		<td class="bodytext" align="center"><strong>Choose an organisational level below</strong></td>
	</tr>
	<tr>
		<td align="center">
			<cfselect name="DLid" size="1" class="selectgen"  id="lrgselect" required="Yes" message="Please select a organisation level">
				<option value="">-- Choose One --</option>
				<cfoutput query="getsecondlevels">
					<option value="#id#" <cfif listfind(valuelist(getcurrleading.dialid),id) gt 0>disabled</cfif> <cfif getleadingdetail.dialid eq id>selected</cfif>>#name#<cfif listfind(valuelist(getcurrleading.dialid),id) gt 0> *</cfif></option>
				</cfoutput>
<!--- <cfoutput>
	
<cfloop list="#listsort(structkeylist(fulltopdownstruct),'numeric')#" index="i">
			<option  value="#listgetat(fulltopdownstruct[i],1,'^')#" <cfif listfind(dialidlist1,listgetat(fulltopdownstruct[i],1,'^')) gt 0>disabled</cfif> <cfif getleadingdetail.dialid eq listgetat(fulltopdownstruct[i],1,'^')>selected</cfif> >#repeatstring("&nbsp;&nbsp;",listgetat(fulltopdownstruct[i],2,"^"))##listgetat(fulltopdownstruct[i],3,"^")#<cfif listfind(dialidlist1,listgetat(fulltopdownstruct[i],1,'^')) gt 0> *</cfif></option>

</cfloop></cfoutput> --->
				</cfselect>
			</td>
		</tr>
		
	<tr>
		<td class="bodytext" align="center" colspan="2">
		<cfoutput>
			<table cellpadding="4" cellspacing="0" border="0" width="75%">
			<tr>
			<td class="bodytext" align="center" colspan="2"><strong>Beyond zero refresh</strong></td>
		</tr>
				<tr>
					<td class="bodytext" width="50%" align="center"><strong>#lookupyear#&nbsp;Planned<br><cfinput type="text" class="selectgen" size="3" name="zplanned" value="#getleadingdetail.zplanned#" required="Yes" message="Please enter numeric Beyond zero refresh planned number" validate="float"></strong></td>
					<td class="bodytext"  width="50%" align="center"><strong>#lookupyear#&nbsp;Complete<br><cfinput type="text" class="selectgen" size="3" name="zcomplete" value="#getleadingdetail.zcomplete#" required="no" message="Please enter numeric Beyond zero refresh complete number" validate="float"></strong></td> 
				</tr>
				
			</table>
		</cfoutput>
		</td>
	</tr>
	
	<tr>
		<td class="bodytext" align="center" colspan="2">
		<cfoutput>
			<table cellpadding="4" cellspacing="0" border="0" width="75%">
			<tr>
			<td class="bodytext" align="center" colspan="2"><strong>Performance standard implementation</strong></td>
		</tr>
				<tr>
					<td class="bodytext" width="50%" align="center"><strong>#lookupyear#&nbsp;Planned<br><cfinput type="text" class="selectgen" size="3" name="psiPlanned" value="#getleadingdetail.psiPlanned#" required="Yes" message="Please enter numeric Performance standard implementation planned number" validate="float"></strong></td>
					<td class="bodytext"  width="50%" align="center"><strong>#lookupyear#&nbsp;Complete<br><cfinput type="text" class="selectgen" size="3" name="psiComplete" value="#getleadingdetail.psiComplete#" required="no" message="Please enter numeric Performance standard implementation complete number" validate="float"></strong></td> 
				</tr>
				
			</table>
		</cfoutput>
		</td>
	</tr>
	
	<tr>
		<td class="bodytext" align="center" colspan="2">
		<cfoutput>
			<table cellpadding="4" cellspacing="0" border="0" width="75%">
			<tr>
			<td class="bodytext" align="center" colspan="2"><strong>HSSE Accountability</strong></td>
		</tr>
				<tr>
					<td class="bodytext" width="50%" align="center"><strong>#lookupyear#&nbsp;Planned<br><cfinput type="text" class="selectgen" size="3" name="hsseaccplanned" value="#getleadingdetail.hsseaccplanned#" required="Yes" message="Please enter numeric HSSE Accountability planned number" validate="float"></strong></td>
					<td class="bodytext"  width="50%" align="center"><strong>#lookupyear#&nbsp;Complete<br><cfinput type="text" class="selectgen" size="3" name="hsseacccomplete" value="#getleadingdetail.hsseacccomplete#" required="no" message="Please enter numeric HSSE Accountability complete number" validate="float"></strong></td> 
				</tr>
				
			</table>
		</cfoutput>
		</td>
	</tr>
	
	<tr>
		<td class="bodytext" align="center" colspan="2">
		<cfoutput>
			<table cellpadding="4" cellspacing="0" border="0" width="75%">
			<tr>
			<td class="bodytext" align="center" colspan="2"><strong>Risk management</strong></td>
		</tr>
				<tr>
					<td class="bodytext" width="50%" align="center"><strong>#lookupyear#&nbsp;Planned<br><cfinput type="text" class="selectgen" size="3" name="rmPlanned" value="#getleadingdetail.rmPlanned#" required="Yes" message="Please enter numeric Risk management planned number" validate="float"></strong></td>
					<td class="bodytext"  width="50%" align="center"><strong>#lookupyear#&nbsp;Complete<br><cfinput type="text" class="selectgen" size="3" name="rmComplete" value="#getleadingdetail.rmComplete#" required="no" message="Please enter numeric Risk management complete number" validate="float"></strong></td> 
				</tr>
				
			</table>
		</cfoutput>
		</td>
	</tr>
	<tr>
		<td align="center"><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
</table>
</td></tr></table>
</cfform>
</div>