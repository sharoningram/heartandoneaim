<div class="content1of1" align="center">
<cfparam name="msg" default="">
<cfform action="#self#?fuseaction=#attributes.xfa.Occupations#" method="post" name="lookupOccupation">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
	<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Lookup Occupation</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	
	<tr>
		<td class="bodytext" align="center"><strong>Select an Occupation:</strong></td>
	</tr>
	<tr>
		<td align="center"><cfselect name="clID" class="selectgen" size="1" required="Yes" message="Please select an Occupation" id="lrgselect"> 
			<option value="">-- Select One --</option>
			<cfoutput query="getOccupations" group="status">
				<option value="" disabled><cfif status eq 1>ACTIVE<cfelse>IN-ACTIVE</cfif></option>
				<cfoutput><option value="#Occupationid#" <cfif clid eq Occupationid>selected</cfif>>&nbsp;&nbsp;&nbsp;#Occupation#</option></cfoutput></cfoutput>
			</cfselect></td>
		</tr>
	<tr>
		<td align="center"><input type="submit" class="selectGenBTN" value="Submit"><cfif trim(clid) neq 0>&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Clear" onclick="document.clrfrm.submit();"></cfif></td>
	</tr>
</table>
</td></tr></table>
</cfform>
<cfoutput>
<form action="#self#?fuseaction=#attributes.xfa.Occupations#" method="post" name="clrfrm">
</form>
</cfoutput>
</div>