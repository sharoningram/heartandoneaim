<div class="content1of1" align="center">
<cfparam name="msg" default="">
<cfform action="#self#?fuseaction=#attributes.xfa.clients#" method="post" name="lookupclient">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
	<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Lookup Client</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	
	<tr>
		<td class="bodytext" align="center"><strong>Select a Client:</strong></td>
	</tr>
	<tr>
		<td align="center"><cfselect name="clID" class="selectgen" size="1" required="Yes" message="Please select a client" id="lrgselect"> 
			<option value="">-- Select One --</option>
			<cfoutput query="getclients" group="status">
				<option value="" disabled><cfif status eq 1>ACTIVE<cfelse>IN-ACTIVE</cfif></option>
				<cfoutput><option value="#clientid#" <cfif clid eq clientid>selected</cfif>>&nbsp;&nbsp;&nbsp;#clientname#</option></cfoutput></cfoutput>
			</cfselect></td>
		</tr>
	<tr>
		<td align="center"><input type="submit" class="selectGenBTN" value="Submit"><cfif trim(clid) neq 0>&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Clear" onclick="document.clrfrm.submit();"></cfif></td>
	</tr>
</table>
</td></tr></table>
</cfform>
<cfoutput>
<form action="#self#?fuseaction=#attributes.xfa.clients#" method="post" name="clrfrm">
</form>
</cfoutput>
</div>