<div class="content1of1" align="center">
<cfparam name="msg" default="">
<cfform action="#self#?fuseaction=#attributes.xfa.managefunctions#" method="post" name="addoccupation">
<input type="hidden" name="submittype" value="addOccupation">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="toptablefrm">
	<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Add Occupation</strong></td>
	</tr>
	<tr>
		<td align="center" class="ltTeal">
	<table cellpadding="4" cellspacing="1" border="0" class="ltTeal">
	<cfif msg eq 1>
	<tr>
		<td class="bodytext" align="center"><strong style="color:008000;">New Occupation Added</strong></td>
	</tr>
	<cfelseif msg eq 2>
	<tr>
		<td class="bodytext" align="center"><strong style="color:red;">Occupation Already Exists</strong></td>
	</tr>
	</cfif>
	<tr>
		<td class="bodytext" align="center"><strong>New Occupation:</strong></td>
	</tr>
	<tr>
		<td align="center"><cfinput  type="text" name="Occupation" size="30" class="selectgen" required="Yes" message="Please enter a Occupation" maxlength="125">
			
			</td>
		</tr>
	
	
	<tr>
		<td align="center"><input type="submit" class="selectGenBTN" value="Submit"></td>
	</tr>
</table>
</td></tr></table>
</cfform>
</div>