<cfquery name="getlocforimport" datasource="oneaim">
SELECT  top 25 ImpLocationID, OldID, GroupName, BU, OU, LocDetail, SiteName, NeedMH, ClientName, BuildingNo, Add2, Add3, County, Country, StartDate, IsActive, wasImported,ZipCode
FROM            _LocationImport
where wasImported = 0
</cfquery>
<cfset testPath = ExpandPath("ImportLocationsLog.txt")>
<cfif not fileexists(testpath)>
<cffile action="WRITE" file="#testpath#" output="Location Import Log">
</cfif>
<cfoutput query="getlocforimport">
	<cfquery name="getsitetype" datasource="oneaim">
		SELECT      LocationDetailID, LocationDetail, Status
		FROM            LocationDetails
		where LocationDetail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LocDetail#">
	</cfquery>
<cfif getsitetype.recordcount gt 0>
	<cfif NeedMH eq "Man Hours only">
		<cfset nusemh = "MHOnly">
	<cfelseif NeedMH eq "incidents only">
		<cfset nusemh = "IncidentOnly">
	<cfelseif NeedMH eq "no">
		<cfset nusemh = "IncidentOnly">
	<cfelse>
		<cfset nusemh = "both">
	</cfif>
	<cfif trim(OldID) neq ''>
		<cfquery name="getg" datasource="oneaim">
		select group_number from groups
		where OldSysGroupNum = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OldID#">
		</cfquery>
	<cfelse>

		<cfquery name="getg" datasource="oneaim">
		select group_number from groups
		where group_name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#GroupName#"> and Business_line_id = 3172
		</cfquery>
	</cfif>
	
	<cfif getg.recordcount gt 0>
	<cfif IsActive eq "yes">
		<cfset useactive = 1>
	<cfelse>
		<cfset useactive = 0>
	</cfif>
	
	<cfset useclient = 0>
	<cfif trim(ClientName) neq ''>
		<cfquery name="getclientid" datasource="oneaim">
			select clientid
			from clients
			where clientname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ClientName#">
		</cfquery>
		<cfif getclientid.recordcount gt 0>
			<cfset useclient = getclientid.clientid>
		<cfelse>
			<cfquery name="addclient" datasource="oneaim">
				insert into clients (clientname, status)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ClientName#">,1)
				select ident_current('clients') as newcli
			</cfquery>
			<cfset useclient = addclient.newcli>
		</cfif>
	</cfif>
	
	<cfset usecountry = 0>
	<cfquery name="getcountry" datasource="oneaim">
			select countryid
			from countries
			where countryname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Country#">
		</cfquery>
		<cfif getcountry.recordcount gt 0>
			<cfset usecountry = getcountry.countryid>
		</cfif>
		<cfif usecountry gt 0>
#ImpLocationID#, #OldID#, #GroupName#, #BU#, #OU#, #LocDetail#, <strong>#SiteName#</strong>, #NeedMH#, #ClientName#, #BuildingNo#, #Add2#, #Add3#, #County#, <strong>#Country#</strong>, #StartDate#, #IsActive#, #wasImported#,#ZipCode# (#getsitetype.LocationDetailID#) #nusemh# #getg.group_number# #useactive# #useclient# #usecountry#<br><br>
	<cfquery name="addloc" datasource="oneaim">
		insert into GroupLocations (GroupNumber, LocationDetailID, SiteName, ClientID, BuildingNo, Address2, Address3, County, ZipCode, CountryID, startDate, isActive, entryType)
		values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getsitetype.LocationDetailID#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#SiteName#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useclient#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#BuildingNo#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Add2#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Add3#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#County#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ZipCode#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usecountry#">, <cfqueryparam cfsqltype="CF_SQL_DATE" value="#StartDate#">, 1, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#nusemh#">)
	</cfquery>
	<cfquery name="updaterec" datasource="oneaim">
		update _LocationImport
		set wasImported = 1
		where ImpLocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ImpLocationID#">
	</cfquery>
<!--- SELECT        GroupLocID, GroupNumber, LocationDetailID, SiteName, ClientID, BuildingNo, Address2, Address3, County, ZipCode, CountryID, startDate, endDate, isActive, entryType
FROM            GroupLocations  --->
		<cfelse>
		
		<cffile action="append" file="#testpath#" output="#ImpLocationID# FAILED: Country #Country# not found" addnewline="Yes">
		</cfif>
<cfelse>
<cffile action="append" file="#testpath#" output="#ImpLocationID# FAILED: Group #GroupName# not found" addnewline="Yes">
</cfif>
<cfelse>
<cffile action="append" file="#testpath#" output="#ImpLocationID# FAILED: Site Type #LocDetail# not found" addnewline="Yes">
</cfif>
</cfoutput>