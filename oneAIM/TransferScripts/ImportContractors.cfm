<cfquery name="getsub" datasource="oneaim">
SELECT       ImpConID, OldID, GroupName, Conname, wasImported
FROM            _SubConImport
where wasimported = 0
</cfquery>
<cfset testPath = ExpandPath("ImportContractorsLog.txt")>
<cfif not fileexists(testpath)>
<cffile action="WRITE" file="#testpath#" output="Contractor Import Log">
</cfif>

<cfoutput query="getsub">
	<cfset useconid = 0>
	<cfquery name="getconid" datasource="oneaim">
		select contractorid
		from contractornames
		where contractorname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#"> and cotype = 'sub'
	</cfquery>
	<cfif getconid.recordcount eq 0>
		<cfquery name="addcon" datasource="oneaim">
			insert into contractornames (contractorname, status, cotype)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#">, 1, 'sub')
			select ident_current('contractornames') as newcon
		</cfquery>
		<cfset useconid = addcon.newcon>
	<cfelse>
		<cfset useconid = getconid.contractorid>
	</cfif>
	<cfquery name="getg" datasource="oneaim">
		select group_number from groups
		where OldSysGroupNum = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OldID#">
	</cfquery>
	<cfif getg.recordcount gt 0>
		<cfquery name="checkjoin" datasource="oneaim">
			select ContractorID
			from Contractors
			where ContractorNameID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useconid#">
			and groupnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">
			and cotype = 'sub'
		</cfquery>
		<cfif checkjoin.recordcount eq 0>
			<cfquery name="addjoin" datasource="oneaim">
				insert into Contractors (ContractorName, GroupNumber, Status, ContYear, CoType, ContractorNameID)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">, 1, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">, 'sub', <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useconid#">)
			</cfquery>
		</cfif>
		<cfquery name="updaterec" datasource="oneaim">
			update _SubConImport
			set wasimported = 1
			where ImpConID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ImpConID#">
		</cfquery>
 #ImpConID#, #OldID#, #GroupName#, #Conname#, #wasImported# #useconid#<br>
 	<cfelse>
	<cffile action="append" file="#testpath#" output="#ImpConID# FAILED: Group #GroupName# not found" addnewline="Yes">
	</cfif>
</cfoutput>


<cfquery name="getmc" datasource="oneaim">
SELECT       ImpConID, OldID, GroupName, Conname, wasImported
FROM            _ManagedConImport
where wasimported = 0
</cfquery>
<cfset testPath = ExpandPath("ImportContractorsLog.txt")>
<cfif not fileexists(testpath)>
<cffile action="WRITE" file="#testpath#" output="Contractor Import Log">
</cfif>

<cfoutput query="getmc">
	<cfset useconid = 0>
	<cfquery name="getconid" datasource="oneaim">
		select contractorid
		from contractornames
		where contractorname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#"> and cotype = 'mgd'
	</cfquery>
	<cfif getconid.recordcount eq 0>
		<cfquery name="addcon" datasource="oneaim">
			insert into contractornames (contractorname, status, cotype)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#">, 1, 'mgd')
			select ident_current('contractornames') as newcon
		</cfquery>
		<cfset useconid = addcon.newcon>
	<cfelse>
		<cfset useconid = getconid.contractorid>
	</cfif>
	<cfquery name="getg" datasource="oneaim">
		select group_number from groups
		where OldSysGroupNum = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OldID#">
	</cfquery>
	<cfif getg.recordcount gt 0>
		<cfquery name="checkjoin" datasource="oneaim">
			select ContractorID
			from Contractors
			where ContractorNameID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useconid#">
			and groupnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">
			and cotype = 'mgd'
		</cfquery>
		<cfif checkjoin.recordcount eq 0>
			<cfquery name="addjoin" datasource="oneaim">
				insert into Contractors (ContractorName, GroupNumber, Status, ContYear, CoType, ContractorNameID)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">, 1, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">, 'mgd', <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useconid#">)
			</cfquery>
		</cfif>
		<cfquery name="updaterec" datasource="oneaim">
			update _ManagedConImport
			set wasimported = 1
			where ImpConID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ImpConID#">
		</cfquery>
 #ImpConID#, #OldID#, #GroupName#, #Conname#, #wasImported# #useconid#<br>
 	<cfelse>
	<cffile action="append" file="#testpath#" output="#ImpConID# FAILED: Group #GroupName# not found" addnewline="Yes">
	</cfif>
</cfoutput>

<cfquery name="getjv" datasource="oneaim">
SELECT       ImpConID, OldID, GroupName, Conname, wasImported
FROM            _JVImport
where wasimported = 0
</cfquery>
<cfset testPath = ExpandPath("ImportContractorsLog.txt")>
<cfif not fileexists(testpath)>
<cffile action="WRITE" file="#testpath#" output="Contractor Import Log">
</cfif>

<cfoutput query="getjv">
	<cfset useconid = 0>
	<cfquery name="getconid" datasource="oneaim">
		select contractorid
		from contractornames
		where contractorname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#"> and cotype = 'jv'
	</cfquery>
	<cfif getconid.recordcount eq 0>
		<cfquery name="addcon" datasource="oneaim">
			insert into contractornames (contractorname, status, cotype)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#">, 1, 'jv')
			select ident_current('contractornames') as newcon
		</cfquery>
		<cfset useconid = addcon.newcon>
	<cfelse>
		<cfset useconid = getconid.contractorid>
	</cfif>
	<cfquery name="getg" datasource="oneaim">
		select group_number from groups
		where OldSysGroupNum = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OldID#">
	</cfquery>
	<cfif getg.recordcount gt 0>
		<cfquery name="checkjoin" datasource="oneaim">
			select ContractorID
			from Contractors
			where ContractorNameID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useconid#">
			and groupnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">
			and cotype = 'jv'
		</cfquery>
		<cfif checkjoin.recordcount eq 0>
			<cfquery name="addjoin" datasource="oneaim">
				insert into Contractors (ContractorName, GroupNumber, Status, ContYear, CoType, ContractorNameID)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Conname#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getg.group_number#">, 1, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">, 'jv', <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useconid#">)
			</cfquery>
		</cfif>
		<cfquery name="updaterec" datasource="oneaim">
			update _JVImport
			set wasimported = 1
			where ImpConID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ImpConID#">
		</cfquery>
 #ImpConID#, #OldID#, #GroupName#, #Conname#, #wasImported# #useconid#<br>
 	<cfelse>
	<cffile action="append" file="#testpath#" output="#ImpConID# FAILED: Group #GroupName# not found" addnewline="Yes">
	</cfif>
</cfoutput>