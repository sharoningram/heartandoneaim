<cfquery name="getgimport" datasource="oneAim">
SELECT  top 10 ImpGroupNumber, OldID, GroupName, ContractNo, BU, OU, BS, MHtype, ProjManager, AssignedUsers, MHuser, wasImported, BUID, OUID, BSID
FROM            _ImportProjects
where wasImported = 0
</cfquery>

<cfoutput query="getgimport">
<cfquery name="getnextnum" datasource="oneaim">
select max(tracking_num) as maxnum
from groups
where TrackingDate = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">
</cfquery>
<cfif trim(getnextnum.maxnum) eq ''>
	<cfset nextnum = 1>
<cfelse>
	<cfset nextnum = getnextnum.maxnum + 1>
</cfif>
<cfif MHtype eq "Manual">
	<cfset usemhtype = "none">
<cfelseif mhtype eq "JDE">
	<cfset usemhtype = "JDE">
<cfelse>
	<cfset usemhtype = "none">
</cfif>

<cfquery name="checkgrp" datasource="oneAIM">
	select group_number, business_line_id, ouid
	from groups
	where group_name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#GroupName#">
</cfquery>


<cfif checkgrp.recordcount gt 0>
	<cfquery name="updateg" datasource="oneaim">
		update groups
		set ContractNo = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ContractNo#">,
		business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BUID#">,
		buid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BUID#">,
		ouid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">,
		BusinessStreamID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BSID#">,
		ERPsys = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#usemhtype#">,
		OldSysGroupNum = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OldID#">,
		ProjectManager = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ProjManager#">,
		Active_Group = 1
		where group_number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#checkgrp.group_number#">
	</cfquery>
	<cfset usegnum = checkgrp.group_number>
<cfelse>
	<cfquery name="addgroup" datasource="oneAIM">
		insert into groups (Group_Name, Entered_By, DateTime, Active_Group, TrackingDate, Business_Line_ID, Tracking_Num, ERPsys, OUid, BusinessStreamID, ContractNo, BUid, ProjectManager, EnteredByName, OldSysGroupNum)
		values  (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#GroupName#">, 'System Import', <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, 1, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BUID#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#nextnum#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#usemhtype#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BSID#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ContractNo#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BUID#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ProjManager#">, 'System Import', <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OldID#">)
		select ident_current('groups') as newgroup
	</cfquery>
	<cfset usegnum = addgroup.newgroup>
</cfif>

	<cfloop list="#AssignedUsers#" index="i">
	<cfif i contains "@">
		<cfset unamepart = listgetat(i,1,"@")>
		<cfif unamepart contains ".">
		<cfset ufname = listgetat(unamepart,1,".")>
		<cfset ulname = listgetat(unamepart,2,".")>
		
	<cfquery name="getextuser" datasource="oneaim">
		select userid 
		from users
		where useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">
	</cfquery>
	
		
		<cfif getextuser.recordcount eq 0>
		
			<cfquery name="adduser" datasource="oneaim">
				insert into users (Firstname, Lastname, Useremail, Status, Entered_By, Is_Admin, IsHSSElead, useradmin)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ufname#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ulname#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">,1,'System Import',0,0,0)
				select ident_current('Users') as newuser
			</cfquery>
			<cfset useuserid = adduser.newuser>
		<cfelse>
			<cfset useuserid = getextuser.userid>
		</cfif>
		
		<cfquery name="chkjoin" datasource="oneaim">
			select roleid
			from userroles
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">
			and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ouid#">
			and userrole = 'User'
		</cfquery>
		<cfif chkjoin.recordcount eq 0>
			<cfquery name="addjoin" datasource="oneaim">
				insert into userroles (UserID, UserRole, AssignedLocs)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">,'User',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ouid#">)
			</cfquery>
		
		</cfif>
		
		<cfquery name="getgassign" datasource="oneaim">
			SELECT       UserGroupID, GroupNumber, UserID, GroupRole, status
			FROM            GroupUserAssignment
			where groupnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usegnum#"> and UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#"> and GroupRole = 'dataentry'
		</cfquery>
		<cfif getgassign.recordcount gt 0>
			<cfquery name="ugassign" datasource="oneaim">
				update GroupUserAssignment
				set status = 1
				where UserGroupID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgassign.UserGroupID#">
			</cfquery>
		<cfelse>
			<cfquery name="addassign" datasource="oneaim">
				insert into GroupUserAssignment (GroupNumber, UserID, GroupRole, status)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usegnum#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#"> , 'dataentry', 1)
			</cfquery>
	
	
		</cfif>
		</cfif>
	</cfif>
	</cfloop>
	
	
	<cfloop list="#MHuser#" index="i">
	<cfif i contains "@">
		<cfset unamepart = listgetat(i,1,"@")>
		<cfif unamepart contains ".">
		<cfset ufname = listgetat(unamepart,1,".")>
		<cfset ulname = listgetat(unamepart,2,".")>
		
	<cfquery name="getextuser" datasource="oneaim">
		select userid 
		from users
		where useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">
	</cfquery>
	
		
		<cfif getextuser.recordcount eq 0>
		
			<cfquery name="adduser" datasource="oneaim">
				insert into users (Firstname, Lastname, Useremail, Status, Entered_By, Is_Admin, IsHSSElead, useradmin)
				values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ufname#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ulname#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">,1,'System Import',0,0,0)
				select ident_current('Users') as newuser
			</cfquery>
			<cfset useuserid = adduser.newuser>
		<cfelse>
			<cfset useuserid = getextuser.userid>
		</cfif>
		
		<cfquery name="chkjoin" datasource="oneaim">
			select roleid
			from userroles
			where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">
			and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ouid#">
			and userrole = 'User'
		</cfquery>
		<cfif chkjoin.recordcount eq 0>
			<cfquery name="addjoin" datasource="oneaim">
				insert into userroles (UserID, UserRole, AssignedLocs)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">,'User',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ouid#">)
			</cfquery>
		
		</cfif>
		
		<cfquery name="getgassign" datasource="oneaim">
			SELECT       UserGroupID, GroupNumber, UserID, GroupRole, status
			FROM            GroupUserAssignment
			where groupnumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usegnum#"> and UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#"> and GroupRole = 'manhours'
		</cfquery>
		<cfif getgassign.recordcount gt 0>
			<cfquery name="ugassign" datasource="oneaim">
				update GroupUserAssignment
				set status = 1
				where UserGroupID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgassign.UserGroupID#">
			</cfquery>
		<cfelse>
			<cfquery name="addassign" datasource="oneaim">
				insert into GroupUserAssignment (GroupNumber, UserID, GroupRole, status)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usegnum#">, <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#"> , 'manhours', 1)
			</cfquery>
	
	
		</cfif>
		</cfif>
	</cfif>
	</cfloop>
	
#OldID#, #GroupName#, #ContractNo#, #BU#, #OU#, #BS#, #MHtype#, #ProjManager#, #AssignedUsers#, #MHuser#, #BUID#, #OUID#, #BSID# #nextnum# #usemhtype# (#checkgrp.recordcount#)<br><br>

	<cfquery name="updaterec" datasource="oneaim">
		update _ImportProjects
		set wasImported = 1
		where ImpGroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ImpGroupNumber#">
	</cfquery>
</cfoutput>
<!--- SELECT        Group_Name, Entered_By, DateTime, Active_Group, TrackingDate, Business_Line_ID, Tracking_Num, ERPsys, OUid, BusinessStreamID, ContractNo, BUid, 
                         ProjectManager, EnteredByName, OldSysGroupNum
						 
						 
values (#GroupName#, Entered_By, DateTime, Active_Group, TrackingDate, #BU#, Tracking_Num, #MHtype#, #OU#, #BS#, #ContractNo#, #BU#, 
                         #ProjManager#, EnteredByName, #OldID#)	 
FROM            Groups --->