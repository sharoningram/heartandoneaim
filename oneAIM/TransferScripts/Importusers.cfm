<cfquery name="getusers" datasource="oneaim">
SELECT top 25 ImpUserID, FirstName, LastName, BU, OU, UserRole, UserEmail, wasImported, isadvisor
FROM            _UserImport
where wasImported = 0
</cfquery>

<cfoutput query="getusers">
	<cfquery name="getextuser" datasource="oneaim">
		select userid 
		from users
		where useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">
	</cfquery>
	<cfif getextuser.recordcount eq 0>
		<cfquery name="adduser" datasource="oneaim">
			insert into users (Firstname, Lastname, Useremail, Status, Entered_By, Is_Admin, IsHSSElead, useradmin)
			values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#firstname#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#lastname#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">,1,'System Import',0,0,0)
			select ident_current('Users') as newuser
		</cfquery>
		<cfset useuserid = adduser.newuser>
	<cfelse>
		<cfset useuserid = getextuser.userid>
	</cfif>
	
	
<cfswitch expression="#userrole#">
	<cfcase value="Senior Reviewer,BU Admin">
		<cfquery name="getbassignloc" datasource="oneaim">
			select id
			from newdials
			where name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#BU#">
		</cfquery>
	</cfcase>
	<cfcase value="Reviewer,OU Admin,User">
		<cfquery name="getbassignloc" datasource="oneaim">
			select id
			from newdials
			where name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OU#">
		</cfquery>
	</cfcase>
</cfswitch>

<cfif getbassignloc.recordcount gt 0>
#FirstName#, #LastName#, #BU#, #OU#, <strong>#UserRole#</strong>, #UserEmail#, #wasImported# #isadvisor#<br>
	<cfquery name="chkjoin" datasource="oneaim">
		select roleid
		from userroles
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">
		and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getbassignloc.id#">
		and userrole = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#userrole#">
	</cfquery>
	<cfif chkjoin.recordcount eq 0>
		<cfquery name="addjoin" datasource="oneaim">
			insert into userroles (UserID, UserRole, AssignedLocs)
			values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#userrole#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getbassignloc.id#">)
		</cfquery>
		<cfquery name="updateimp" datasource="oneaim">
			update _UserImport
			set wasImported = 1
			where ImpUserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ImpUserID#">
		</cfquery>

	</cfif>
<cfelse>
<span style="color:red;">#FirstName#, #LastName#, #BU#, #OU#, <strong>#UserRole#</strong>, #UserEmail#, #wasImported# #isadvisor#<br></span>
</cfif>


<cfif isadvisor eq "yes">
	<cfquery name="getbassignloc" datasource="oneaim">
		select id
		from newdials
		where name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#OU#">
	</cfquery>
	<cfquery name="chkjoin" datasource="oneaim">
		select roleid
		from userroles
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">
		and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getbassignloc.id#">
		and userrole = 'HSSE Advisor'
	</cfquery>
	<cfif chkjoin.recordcount eq 0>
		<cfquery name="addjoin" datasource="oneaim">
			insert into userroles (UserID, UserRole, AssignedLocs)
			values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#useuserid#">,'HSSE Advisor',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getbassignloc.id#">)
		</cfquery>
	</cfif>
</cfif>



</cfoutput>