<cfparam name="type"  default="">
<cfparam name="BU"  default="">

<cfif trim(type) neq ''>
	<cfswitch expression="#type#">
		<cfcase value="OU">
			<cfif trim(BU) neq '' and isnumeric(BU)>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        ID, Name
					FROM            NewDials
					WHERE        (Parent = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (status <> 99) and isgroupdial = 0
					ORDER BY Name
				</cfquery><!--- this.style.border='1px solid 5f2468'; --->
				{zz{<select name="ou" size="1" class="selectgen" id="lrgselect"  onchange="buildbslist(this.value);">
					<option value="">-- Select One --</option>
					<cfoutput query="getOU"><option value="#id#">#name#</option></cfoutput>
					</select>}zz}
					
			</cfif>
		</cfcase>
		<cfcase value="BS">
			<cfif trim(BU) neq '' and isnumeric(BU)>
				<cfquery name="getOU" datasource="#request.dsn#">
					SELECT        ID, Name
					FROM            NewDials
					WHERE        (Parent = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (status <> 99) and isgroupdial = 0
					ORDER BY Name
				</cfquery>
				<cfquery name="getuser" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
					FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
					WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (UserRoles.UserRole in ( 'user'))
					order by Users.Firstname, Users.Lastname
				</cfquery>
				<cfquery name="getadvisors" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
					FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
					WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (UserRoles.UserRole in ( 'HSSE Advisor'))
					order by Users.Firstname, Users.Lastname
				</cfquery>
				<cfquery name="getGA" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, UserRoles.UserRole
						FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole IN ('Global Admin'))
						ORDER BY Users.Firstname, Users.Lastname
					</cfquery>
					<cfquery name="getOUadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
						FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">) AND (UserRoles.UserRole in ( 'OU Admin'))
						order by Users.Firstname, Users.Lastname
					</cfquery>
					<cfquery name="getBUadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
						FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1)  AND (UserRoles.UserRole in ( 'BU Admin')) AND (UserRoles.AssignedLocs IN (SELECT        Parent
FROM            NewDials
WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#bu#">)))
						order by Users.Firstname, Users.Lastname
					</cfquery>
				{zz{<select name="businessstream" size="1" class="selectgen" id="lrgselect" <cfif getOU.recordcount eq 0>disabled</cfif>>
					<option value="">-- Select One --</option>
					<cfoutput query="getOU"><option value="#id#">#name#</option></cfoutput>
					</select>}zz}<!--- onchange="this.style.border='1px solid 5f2468';"  --->
					<cfif getuser.recordcount gt 0>
					{yy{yes}yy}
					<cfelse>
					{yy{no}yy}
					</cfif>
					{uu{<select name="assign_user_id" size="4" class="selectgen" id="lrgselect" multiple="Yes" >
										<option value="" selected>-- Select From List --</option>
										<cfif getuser.recordcount gt 0>
										<option value="" disabled class="formlable">Users</option>
										<cfoutput query="getuser">
										<cfif userrole eq "user">
										<option value="#userid#">#firstname# #lastname#</option>
										</cfif>
										</cfoutput> 
										</cfif>
										<cfif getBUadmin.recordcount gt 0>
										<cfoutput><option value="" disabled class="formlable">#request.bulabellong# Administrators</option></cfoutput>
										 <cfoutput query="getBUadmin">
											<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
												<option value="#userid#">#firstname# #lastname#</option>
											<cfelse>
												<option value="#userid#" disabled>#firstname# #lastname#</option>
											</cfif>
										</cfoutput>
										</cfif>
										<cfif getOUadmin.recordcount gt 0>
										<cfoutput><option value="" disabled class="formlable">#request.oulabellong# Administrators</option></cfoutput>
										<cfoutput query="getOUadmin">
										<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
										<option value="#userid#" >#firstname# #lastname#</option>
										<cfelse>
										<option value="#userid#" disabled>#firstname# #lastname#</option>
										</cfif>
										</cfoutput>
										</cfif>
										<cfif getGA.recordcount gt 0>
										<option value="" disabled class="formlable">Global Administrators</option>
										<cfoutput query="getGA">
										<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
										<option value="#userid#" >#firstname# #lastname#</option>
										<cfelse>
										<option value="#userid#" disabled>#firstname# #lastname#</option>
										</cfif>
										</cfoutput> 
										</cfif>
										
										</select>}uu}<!--- onchange="this.style.border='1px solid 5f2468';"  --->
										
					
					{mm{<select name="mhperson" size="4" class="selectgen" id="lrgselect" multiple="Yes" >
										<option value="" selected>-- Select From List --</option>
										<cfif getuser.recordcount gt 0>
										<option value="" disabled class="formlable">Users</option>
										<cfoutput query="getuser">
										
										<option value="#userid#">#firstname# #lastname#</option>
										
										</cfoutput> 
										</cfif>
										<cfif getBUadmin.recordcount gt 0>
										<cfoutput><option value="" disabled class="formlable">#request.bulabellong# Administrators</option></cfoutput>
										 <cfoutput query="getBUadmin">
										<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
										<option value="#userid#" >#firstname# #lastname#</option>
										<cfelse>
										<option value="#userid#" disabled>#firstname# #lastname#</option>
										</cfif>
										</cfoutput>
										</cfif>
										<cfif getOUadmin.recordcount gt 0>
										<cfoutput><option value="" disabled class="formlable">#request.oulabellong# Administrators</option></cfoutput>
										<cfoutput query="getOUadmin">
										<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
										<option value="#userid#" >#firstname# #lastname#</option>
										<cfelse>
										<option value="#userid#" disabled>#firstname# #lastname#</option>
										</cfif>
										</cfoutput>
										</cfif>
										<cfif getGA.recordcount gt 0>
										<option value="" disabled class="formlable">Global Administrators</option>
										<cfoutput query="getGA">
										<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 >
										<option value="#userid#" >#firstname# #lastname#</option>
										<cfelse>
										<option value="#userid#" disabled>#firstname# #lastname#</option>
										</cfif>
										</cfoutput> 
										</cfif>
										</select>}mm}
					{ad{<select name="hsseadv" size="3" class="selectgen" multiple>
										<option value="" selected>-- Select From List --</option>
										<cfoutput query="getadvisors">
										<option value="#userid#">#firstname# #lastname#</option>
										</cfoutput>
										</select>}ad}
			</cfif>
		</cfcase>
	</cfswitch>
</cfif>