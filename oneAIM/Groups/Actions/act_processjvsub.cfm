<cfparam name="currsubs" default="">
<cfparam name="sid" default="">
<cfparam name="currnames" default="">
<cfparam name="delsub" default="">
<cfparam name="type" default="">
<cfparam name="newsub" default="">
<cfparam name="doadd" default="no">
<cfif type eq "sub">
	<cfif doadd eq "yes">
		<cfif trim(newsub) neq ''>
			<cfquery name="checkfor" datasource="#request.dsn#">
				select ContractorID
				from ContractorNames
				where ContractorName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newsub#"> and cotype = 'sub'
			</cfquery>
			<cfif checkfor.recordcount eq 0>
				<cfquery name="addsub" datasource="#request.dsn#">
					insert into ContractorNames (ContractorName, CoType)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newsub#">,'sub')
					select ident_current('ContractorNames') as newsubid
				</cfquery>
				<cfset sid = addsub.newsubid>
			<cfelse>
				<cfset sid = checkfor.ContractorID>
			</cfif>
		</cfif>
	</cfif>


	<cfif trim(delsub) eq ''>
		<cfif trim(sid) neq ''>
			<cfif trim(currsubs) eq ''>
				<cfset currsubs = sid>
			<cfelse>
				<cfset currsubs = listappend(currsubs,sid)>
			</cfif>
		</cfif>
	<cfelse>
		<!--- <cfif listlen(sid) gt 1> --->
			<cfloop list="#sid#" index="i">
				<cfset currsubs = listdeleteat(currsubs,listfind(currsubs,i))>
			</cfloop>
		<!--- <cfelse>
			<cfset currsubs = listdeleteat(currsubs,listfind(currsubs,sid))>
		</cfif> --->
	</cfif>
<cfelseif type eq "jv">
	<cfif doadd eq "yes">
		<cfif trim(newsub) neq ''>
			<cfquery name="checkfor" datasource="#request.dsn#">
				select ContractorID
				from ContractorNames
				where ContractorName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newsub#"> and cotype = 'jv'
			</cfquery>
			<cfif checkfor.recordcount eq 0>
				<cfquery name="addsub" datasource="#request.dsn#">
					insert into ContractorNames (ContractorName, CoType)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newsub#">,'jv')
					select ident_current('ContractorNames') as newsubid
				</cfquery>
				<cfset sid = addsub.newsubid>
			<cfelse>
				<cfset sid = checkfor.ContractorID>
			</cfif>
		</cfif>
	</cfif>


	<cfif trim(delsub) eq ''>
		<cfif trim(sid) neq ''>
			<cfif trim(currsubs) eq ''>
				<cfset currsubs = sid>
			<cfelse>
				<cfset currsubs = listappend(currsubs,sid)>
			</cfif>
		</cfif>
	<cfelse>
		<cfloop list="#sid#" index="i">
			<cfset currsubs = listdeleteat(currsubs,listfind(currsubs,i))>
		</cfloop>
	</cfif>
<cfelseif type eq "mgd">
	<cfif doadd eq "yes">
		<cfif trim(newsub) neq ''>
			<cfquery name="checkfor" datasource="#request.dsn#">
				select ContractorID
				from ContractorNames
				where ContractorName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newsub#"> and cotype = 'mgd'
			</cfquery>
			<cfif checkfor.recordcount eq 0>
				<cfquery name="addsub" datasource="#request.dsn#">
					insert into ContractorNames (ContractorName, CoType)
					values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newsub#">,'mgd')
					select ident_current('ContractorNames') as newsubid
				</cfquery>
				<cfset sid = addsub.newsubid>
			<cfelse>
				<cfset sid = checkfor.ContractorID>
			</cfif>
		</cfif>
	</cfif>


	<cfif trim(delsub) eq ''>
		<cfif trim(sid) neq ''>
			<cfif trim(currsubs) eq ''>
				<cfset currsubs = sid>
			<cfelse>
				<cfset currsubs = listappend(currsubs,sid)>
			</cfif>
		</cfif>
	<cfelse>
		<cfloop list="#sid#" index="i">
			<cfset currsubs = listdeleteat(currsubs,listfind(currsubs,i))>
		</cfloop>
	</cfif>

<cfelseif type eq "ag">
	<cfif trim(delsub) eq ''>
		<cfif trim(sid) neq ''>
			<cfif trim(currsubs) eq ''>
				<cfset currsubs = sid>
			<cfelse>
				<cfset currsubs = listappend(currsubs,sid)>
			</cfif>
		</cfif>
	<cfelse>
		<cfloop list="#sid#" index="i">
			<cfset currsubs = listdeleteat(currsubs,listfind(currsubs,i))>
		</cfloop>
	</cfif>
</cfif>