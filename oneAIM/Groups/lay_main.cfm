<cfif listlen(request.userlevel eq 1)>
	<cfif request.userlevel eq "Global IT">
		<cflocation url="/#getappconfig.oneAIMpath#/index.cfm?fuseaction=adminfunctions.main" addtoken="No">
	</cfif>
</cfif>

<cfset titleaddon = "Group Management">
<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="main">
		<cfset titleaddon = titleaddon & " - main">
	</cfcase>
</cfswitch>
<cfoutput>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 >

<cfset irstyle = "genTabonStyle">
<cfset irvalign = "middle">
<cfset irtabs = "sm">
<cfparam name="groupnumber" default="0">
<cfif fusebox.fuseaction neq "createscfield">
	<div class="stripe1"><table width="100%" cellpadding="0" cellspacing="0" border="0">

<tr><td>
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr class="purplebg">
	<td class="bodytextwhitelg"  width="90%">&nbsp;Admin&nbsp;Functions&nbsp;-&nbsp;Manage&nbsp;Project/Office&nbsp;Groups</td><td align="right" class="bodytextwhite" ><cfif trim(request.fname) neq ''>Welcome&nbsp;<cfoutput>#replace(request.fname," ","&nbsp;","all")#&nbsp;#replace(request.lname," ","&nbsp;","all")#</cfoutput>&nbsp;&nbsp;</cfif></td><td align="right" class="helptext">&nbsp;<a href="javascript:void(0);" onclick="NewWindow('/#getappconfig.oneAIMpath#/shared/utils/help.cfm?ul=#request.userlevel#','Info','280','400','yes');" class="helptext">Help</a>&nbsp;</span><td align="right" >&nbsp;&nbsp;&nbsp;</td></tr>
</table></td></tr>
<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>

<tr>
	<td colspan="2" bgcolor="ffffff"><img src="images/spacer.gif" height="2" border="0"></td>
</tr>
</table>
</div>
	<table cellpadding="0" border="0" cellspacing="0" width="75%" id="mytab">
	<tr>
		<td><img src="images/spacer.gif" height="30" border="0"></td>
	</tr>
	<tr>
	<td>
		<table  cellpadding="0" cellspacing="0">
		<cfoutput>
			<tr>
				<cfif groupnumber gt 0>
				<td class="tab1bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab1bkg" background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.main" class="bodyTextPurple">Add&nbsp;Project/Office</a></td>
				<td class="tab1bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td class="tab1bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab1bkgon">Update&nbsp;Project&nbsp;Group<cfif isdefined("getgroupdetails.recordcount")>&nbsp;P#getgroupdetails.trackingdate##numberformat(getgroupdetails.tracking_num,"0000")#</cfif></td>
				<td class="tab1bkg"><img src="images/tabright.png" border="0"></td>
				<td class="tab3bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab3bkg" background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.grouplist" class="bodyTextPurple">View&nbsp;Project/Office&nbsp;List</a></td>
				<td class="tab3bkg"><img src="images/tabrightsm.png" border="0"></td>
				<cfelse>
				<cfif fusebox.fuseaction eq "grouplist">
				<td class="tab1bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab1bkg" background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.main" class="bodyTextPurple">Add&nbsp;Project/Office</a></td>
				<td class="tab1bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td class="tab2bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab2bkgon">View&nbsp;Project/Office&nbsp;List</td>
				<td class="tab2bkg"><img src="images/tabright.png" border="0"></td>
				<cfelse>
				<td class="tab1bkg"><img src="images/tableft.png" border="0"></td>
				<td class="tab1bkgon">Add&nbsp;Project/Office</td>
				<td class="tab1bkg"><img src="images/tabright.png" border="0"></td>
				<td class="tab2bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab2bkg"  background="images/smtabbg.png"><a href="index.cfm?fuseaction=groups.grouplist" class="bodyTextPurple">View&nbsp;Project/Office&nbsp;List</a></td>
				<td class="tab2bkg"><img src="images/tabrightsm.png" border="0"></td>
				</cfif>
				</cfif>
				<td class="tab4bkg"><img src="images/tableftsm.png" border="0"></td>
				<td class="tab4bkg" background="images/smtabbg.png"><a href="index.cfm?fuseaction=reports.prjsearchadmin" class="bodyTextPurple">Search&nbsp;for&nbsp;Project/Office</a></td>
				<td class="tab4bkg"><img src="images/tabrightsm.png" border="0"></td>
				<td width="90%"></td>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="index.cfm?fuseaction=adminfunctions.main" class="bodytextsmpurple" ><<&nbsp;Admin&nbsp;Home</a>&nbsp;&nbsp;</td>
				<td align="right"><img src="images/navlft3.png" border="0"></td>
				<td align="left" style="font-family:Segoe UI;font-size:9pt; background:url(images/navbk3.png);" valign="middle"><a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple"><<&nbsp;oneAIM&nbsp;Home</a>&nbsp;&nbsp;</td>
				<!--- <td align="right" width="100%" style="font-family:Segoe UI;font-size:9pt;" valign="bottom"><a href="/#getappconfig.oneAIMpath#" class="bodytextsmpurple"><<&nbsp;oneAIM&nbsp;Home</a></td> --->
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
<tr>
<td>#Fusebox.layout#</td></tr></table>
<cfelse>
#Fusebox.layout#
</cfif>
	</cfif>
</cfoutput>