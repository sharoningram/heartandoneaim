<cfquery name="getgrouplocations" datasource="#request.dsn#">
SELECT        GroupLocations.GroupLocID, GroupLocations.GroupNumber, GroupLocations.LocationDetailID, GroupLocations.SiteName, GroupLocations.ClientID, 
                         GroupLocations.BuildingNo, GroupLocations.Address2, GroupLocations.Address3, GroupLocations.County, GroupLocations.ZipCode, GroupLocations.CountryID, 
                         GroupLocations.startDate, GroupLocations.endDate, GroupLocations.isActive, LocationDetails.LocationDetail, Countries.CountryName, Clients.ClientName
FROM            GroupLocations INNER JOIN
                         LocationDetails ON GroupLocations.LocationDetailID = LocationDetails.LocationDetailID INNER JOIN
                         Countries ON GroupLocations.CountryID = Countries.CountryID LEFT OUTER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID
WHERE        (GroupLocations.GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">)
ORDER BY GroupLocations.isActive DESC, GroupLocations.SiteName
</cfquery>