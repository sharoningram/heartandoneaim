<cfquery name="getassocgroups" datasource="#request.dsn#">
SELECT        GroupAssociations.GroupNumber1, Groups.Group_Number, Groups.Group_Name
FROM            GroupAssociations INNER JOIN
                         Groups ON GroupAssociations.GroupNumber2 = Groups.Group_Number
WHERE        (GroupAssociations.GroupNumber1 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">)
UNION
SELECT        GroupAssociations.GroupNumber2, Groups.Group_Number, Groups.Group_Name
FROM            GroupAssociations INNER JOIN
                         Groups ON GroupAssociations.GroupNumber1 = Groups.Group_Number
WHERE        (GroupAssociations.GroupNumber2 = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">)
</cfquery>