<cfquery name="getgrouplist" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, NewDials_1.Name AS BUname, NewDials.Name AS ouname, NewDials_2.Name AS bsname, Groups.Active_Group, 
                         Groups.TrackingDate, Groups.Tracking_Num, Users.Firstname, Users.Lastname, GroupUserAssignment.GroupRole
FROM            Users INNER JOIN
                         GroupUserAssignment ON Users.UserId = GroupUserAssignment.UserID RIGHT OUTER JOIN
                         Groups INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON GroupUserAssignment.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	Where Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
</cfif>
ORDER BY active_group desc, buname, ouname, bsname, Groups.Group_Name
</cfquery><!--- SELECT        Groups.Group_Number, Groups.Group_Name, NewDials_1.Name AS BUname, NewDials.Name AS ouname, NewDials_2.Name AS bsname, groups.active_group,Groups.trackingdate,Groups.tracking_num
FROM            Groups INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID --->