<cfquery name="getdataentry" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
	FROM            Users LEFT OUTER JOIN
                     UserRoles ON Users.UserId = UserRoles.UserID
	WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupdetails.ouid#">) AND (UserRoles.UserRole in ( 'user'))
	order by Users.Firstname, Users.Lastname
</cfquery>
<cfquery name="getGA" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, UserRoles.UserRole
						FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole IN ('Global Admin'))
						ORDER BY Users.Firstname, Users.Lastname
					</cfquery>
					<cfquery name="getOUadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
						FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupdetails.ouid#">) AND (UserRoles.UserRole in ( 'OU Admin'))
						order by Users.Firstname, Users.Lastname
					</cfquery>
					<cfquery name="getBUadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
						FROM            Users LEFT OUTER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1)  AND (UserRoles.UserRole in ( 'BU Admin')) AND (UserRoles.AssignedLocs IN (SELECT        Parent
FROM            NewDials
WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupdetails.ouid#">)))
						order by Users.Firstname, Users.Lastname
					</cfquery>