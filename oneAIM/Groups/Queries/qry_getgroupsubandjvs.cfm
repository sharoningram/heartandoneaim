<cfquery name="getgroupsubjv" datasource="#request.dsn#">
SELECT        Contractors.CoType, Contractors.ContractorNameID, ContractorNames.ContractorName
FROM            Contractors INNER JOIN
                         ContractorNames ON Contractors.ContractorNameID = ContractorNames.ContractorID
WHERE     Contractors.status = 1 and   (Contractors.GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">)
</cfquery>