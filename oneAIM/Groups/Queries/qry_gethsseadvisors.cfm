<cfquery name="gethsseadvisors" datasource="#request.dsn#">
	SELECT        Users.UserId, Users.Firstname, Users.Lastname,UserRoles.UserRole
	FROM            Users LEFT OUTER JOIN
                     UserRoles ON Users.UserId = UserRoles.UserID
	WHERE        (Users.Status = 1) AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupdetails.ouid#">) AND (UserRoles.UserRole in ( 'HSSE Advisor'))
	order by Users.Firstname, Users.Lastname
</cfquery>