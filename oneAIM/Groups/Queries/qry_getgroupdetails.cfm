<cfparam name="groupnumber" default="0">
<cfquery name="getgroupdetails" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Description, Groups.Add_Group_Description, Groups.Group_Name, Groups.Group_Address1, Groups.Group_Address2, 
                         Groups.Geographic_Region, Groups.Country_Code, Groups.Client_Name, Groups.Entered_By, Groups.DateTime, Groups.Distribution_ID, Groups.Assigned_User_Id, 
                         Groups.Comments, Groups.Active_Group, Groups.TrackingDate, Groups.Business_Line_ID, Groups.Tracking_Num, Groups.BuildDial, Groups.CarryOver, 
                         Groups.parentdial, Groups.OUManager, Groups.ERPsys, Groups.OUid, Groups.BusinessStreamID, Groups.ContractNo, Groups.BUid, Groups.ProjectManager, 
                         Groups.LFcontactemail, Groups.ProjectType, Groups.EnteredByName, NewDials.Name AS BUname, NewDials_1.Name AS ouname, 
                         NewDials_2.Name AS bsname
FROM            Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupnumber#">)
</cfquery>