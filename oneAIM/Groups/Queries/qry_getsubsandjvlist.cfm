<cfif type neq "ag">

<cfquery name="getsubsandjv" datasource="#request.dsn#">
SELECT        ContractorID, ContractorName, CoType, left(contractorname,1) as coninit
FROM            ContractorNames
where status = 1 and cotype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#type#"> <cfif trim(currsubs) neq ''>and contractorid not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif>
ORDER BY coninit,ContractorName
</cfquery>

<cfif trim(currsubs) neq ''>
<cfquery name="getassignedsubsandjv" datasource="#request.dsn#">
SELECT        ContractorID, ContractorName, CoType, left(contractorname,1) as coninit
FROM            ContractorNames
where status = 1 and cotype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#type#">  <cfif trim(currsubs) neq ''>and contractorid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif>
ORDER BY coninit,ContractorName
</cfquery>
<cfloop query="getassignedsubsandjv">
	<cfset currnames = listappend(currnames,contractorname)>
</cfloop>

</cfif>

<cfelse>
<cfquery name="getsubsandjv" datasource="#request.dsn#">
SELECT        Groups.Group_Number as ContractorID, Groups.Group_Name as ContractorName, 'ag' as cotype,  left(Group_Name,1) as coninit, NewDials_1.Name AS BUname, NewDials.Name AS ouname, NewDials_2.Name AS bsname
FROM            Groups INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID
WHERE 1 = 1
<cfif trim(currsubs) neq ''>
	and Groups.Group_Number not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
</cfif>
ORDER BY buname, ouname, bsname, coninit, Groups.Group_Name
</cfquery>

<cfif trim(currsubs) neq ''>
<cfquery name="getassignedsubsandjv" datasource="#request.dsn#">
SELECT        Groups.Group_Number as ContractorID, Groups.Group_Name as ContractorName, 'ag' as cotype,  left(Group_Name,1) as coninit, NewDials_1.Name AS BUname, NewDials.Name AS ouname, NewDials_2.Name AS bsname
FROM            Groups INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID
WHERE 1 = 1
<cfif trim(currsubs) neq ''>
	and Groups.Group_Number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
</cfif>
ORDER BY buname, ouname, bsname, coninit, Groups.Group_Name
</cfquery>
<cfloop query="getassignedsubsandjv">
	<cfset currnames = listappend(currnames,contractorname)>
</cfloop>

</cfif>



</cfif>