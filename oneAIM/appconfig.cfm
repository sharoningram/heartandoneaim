<!--- Path to applications in file system --->
<cfset oneaimroot = "oneaim">
<cfset heartroot = "heart">
<!--- ****** --->

<!--- Data source names in coldfusion --->
<cfset oneaimdsn = "oneaim">
<cfset heartdsn = "heart">
<cfset commondsn = "common">
<cfset laborsafetydsn = "labor_safety">
<!--- ****** --->

<!--- Database names in SQL Server --->
<cfset oneaimSQLname = "oneaim">
<cfset heartSQLname = "heart">
<!--- ****** --->

<!--- Application encryption keys --->
<cfset key1 = "r4eBcdRntP6AkHXPcoJ6/w==">
<cfset key2 = "cZ6c0GmDUdMCkshdAfQrig==">
<cfset key3 = "2Yr6T4xr/n87ACCvUboVAw==">
<cfset key4 = "igyebN5PpVe3AnDIgxaWTg==">
<cfset key5 = "RrhV5RRgKbvCtI3OgAH+hQ==">
<cfset key6 = "8txDnAFBxsAEyvh1pUg1JA==">
<!--- ****** --->


<!--- Support Emails --->
<cfset erroremail = "sharon.ingram@amecfw.com">
<cfset fromemail = "oneAIM.NoReply@amecfw.com">
<cfset HeartEmail = "Heart@amecfw.com">
<!--- ****** --->
