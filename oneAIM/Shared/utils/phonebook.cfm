<cfinclude template="../../appconfig.cfm">

<cfquery name="getconnections" datasource="#oneaimdsn#">
SELECT        initID, oneAIMpath, heartPath, LDAP1, LDAP1user, LDAP1pw, LDAP2, LDAP2user, LDAP2pw,  LDAP1start, LDAP2start
FROM            app_config
</cfquery>


<cfset connectuname = "#decrypt(getconnections.LDAP1user,key6,'AES','Hex')#">
<cfset connectpassword = "#decrypt(getconnections.LDAP1pw,key1,'AES','Hex')#">
<cfset ldapserver = "#decrypt(getconnections.LDAP1,key2,'AES','Hex')#">
<cfset ldap1start = "#decrypt(getconnections.LDAP1start,key2,'AES','Hex')#">

<cfset connectuname2 = "#decrypt(getconnections.LDAP2user,key2,'AES','Hex')#">
<cfset connectpassword2 = "#decrypt(getconnections.LDAP2pw,key3,'AES','Hex')#">
<cfset ldapserver2 = "#decrypt(getconnections.LDAP2,key1,'AES','Hex')#">
<cfset ldap2start = "#decrypt(getconnections.LDAP2start,key5,'AES','Hex')#">

<cfparam name="srcscriptname" default = "">
<cfparam name="searchitem" default = "">
<cfparam name="docname" default = "">
<cfparam name="fldname" default = "">
<script type="text/javascript">
function submitsfrm(){
// alert(document.userlookupfrm.searchitem.value);
var oktosubmit = "yes"

if(document.userlookupfrm.searchitem.value==""){
	var oktosubmit = "no";
	 
	 document.userlookupfrm.searchitem.style.border = "2px solid F00000";
}
else{
document.userlookupfrm.searchitem.style.border = "1px solid 5f2468";
}

if (oktosubmit=="no"){
	document.getElementById("reqflds").style.display='';
	return false;
}  
// alert(oktosubmit);
}
</script>

<cfoutput><link href="/#getconnections.oneAIMpath#/shared/css/ers_local.css" rel="stylesheet" type="text/css"></cfoutput>
<body leftmargin="0" topmargin="0" rightmargin="0">
<form name="userlookupfrm" action="phonebook.cfm" method="post" onsubmit="return submitsfrm();">

<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr class="purplebg">
		<td class="bodytextwhite" colspan="2">User Lookup</td>
	</tr>
	<tr id="reqflds">
		<td colspan="2" class="redText" align="center">Please fill in all required fields</td>
	</tr><!--- onkeypress="this.style.border = '1px solid 5f2468';" --->
	<cfoutput>
	<input type="hidden" name="srcscriptname" value="#srcscriptname#">
	<tr>
		<td class="formlable" width="45%"><strong>Name:</strong></td>
		<td width="55%"><input type="text" name="searchitem" size="25" class="selectgen" value="#searchitem#" >&nbsp;<input type="submit" value="Search" class="selectGenBTN"></td>
	</tr>
	</cfoutput>
</table>
</form>
<!--- <p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p> --->
<script type="text/javascript">
	document.getElementById("reqflds").style.display='none';
</script>
<cfif trim(searchitem) neq ''>
<cfldap 
		server = "#ldapserver#"
		port="3268"
	    action = "query"
		filter="(|(mail=#ucase(trim(searchitem))#*) (mail=#trim(searchitem)#*) (displayname=#ucase(trim(searchitem))#*) (displayname=#trim(searchitem)#*))"
	    name = "myq1"
	    start = "#ldap1start#"
	   	scope="subtree"
	    attributes = "name, mail, title, physicaldeliveryofficename, department,displayname, sAMAccountName,msRTCSIP-PrimaryUserAddress,proxyaddresses,UserPrincipalName"
		username ="#connectuname#"
		password = "#connectpassword#"> 
		
<cfset useforstr = "">
<cfset amecem = "">	
<cfset emstruct = {}>
<cfloop query="myq1">
<cfif trim(myq1["msRTCSIP-PrimaryUserAddress"][currentrow]) neq ''>
	<cfif not structkeyexists(emstruct,mail)>
		<cfloop list="#myq1.proxyaddresses#" index="i">
		<cfset useforstr = "">
		<cfset amecem = "">
			<cfif i contains "smtp" and i contains "@amecfw.com">
				<cfset useforstr = name>
				<cfset useforstr = listappend(useforstr,displayname,"~")>
				<cfif trim(title) neq ''>
					<cfset useforstr = listappend(useforstr,title,"~")>
				<cfelse>
					<cfset useforstr = listappend(useforstr," ","~")>
				</cfif>
				<cfset amecem = replacenocase(i,"smtp:","","all")>
				<cfset amecem = replacenocase(amecem," ","","all")>
				<cfset useforstr = listappend(useforstr,amecem,"~")>
				<cfbreak>
			</cfif>
		</cfloop>
		<cfif trim(useforstr) eq ''>
			<cfset mailrt = listgetat(mail,1,"@")>
			<cfif mailrt contains "_">
				<cfset useforstr = "#listgetat(mailrt,2,'_')#, #listgetat(mailrt,1,'_')#~#listgetat(mailrt,2,'_')#, #listgetat(mailrt,1,'_')#~(#mail#)~#mail#">
			<cfelseif mailrt contains ".">
				<cfset useforstr = "#listgetat(mailrt,2,'.')#, #listgetat(mailrt,1,'.')#~#listgetat(mailrt,2,'.')#, #listgetat(mailrt,1,'.')#~(#mail#)~#mail#">
			</cfif>
		</cfif>
		<cfset emstruct[mail] = useforstr>
	</cfif>
</cfif>
</cfloop>


<cfldap 
		server = "#ldapserver2#"
		port="3268"
	    action = "query"
		filter="(|(mail=#ucase(trim(searchitem))#*) (mail=#trim(searchitem)#*) (displayname=#ucase(trim(searchitem))#*) (displayname=#trim(searchitem)#*))"
	    name = "myq2"
	    start = "#ldap2start#"
	   	scope="subtree"
	    attributes = "name, mail, title, physicaldeliveryofficename, department,displayname, sAMAccountName,msRTCSIP-PrimaryUserAddress,proxyaddresses,UserPrincipalName"
		username ="#connectuname2#"
		password = "#connectpassword2#"> 
		
<cfset amcstruct = {}>
<cfloop query="myq2">
	<cfif not structkeyexists(amcstruct,mail)>
		<cfloop list="#myq2.proxyaddresses#" index="i">
		<cfset useforstr = "">
		<cfset amecem = "">
			<cfif i contains "smtp" and i contains "@amecfw.com">
								
				<cfset useforstr = name>
				<cfset useforstr = listappend(useforstr,displayname,"~")>
				<cfif trim(title) neq ''>
					<cfset useforstr = listappend(useforstr,title,"~")>
				<cfelse>
					<cfset useforstr = listappend(useforstr," ","~")>
				</cfif>
				<cfset amecem = replacenocase(i,"smtp:","","all")>
				<cfset amecem = replacenocase(amecem," ","","all")>
				<cfset useforstr = listappend(useforstr,amecem,"~")>
				
				<cfbreak>
			</cfif>
		</cfloop>
		<cfif trim(useforstr) eq ''>
			<cfset mailrt = listgetat(mail,1,"@")>
			<cfif mailrt contains "_">
				<cfset useforstr = "#listgetat(mailrt,2,'_')#, #listgetat(mailrt,1,'_')#~#listgetat(mailrt,2,'_')#, #listgetat(mailrt,1,'_')#~(#mail#)~#mail#">
				<cfset amecem = mail>
			<cfelseif mailrt contains ".">
				<cfset useforstr = "#listgetat(mailrt,2,'.')#, #listgetat(mailrt,1,'.')#~#listgetat(mailrt,2,'.')#, #listgetat(mailrt,1,'.')#~(#mail#)~#mail#">
				<cfset amecem = mail>
			</cfif>
		</cfif>
		<cfset emstruct[mail] = useforstr>
		<cfset amcstruct[mail] = amecem>
	</cfif>
</cfloop>
		
		<cfset myq3 = QueryNew("name, displayname, title, mail")>
		<cfset ctr = 0>
		<cfloop list="#structkeylist(emstruct)#" index="i">
		<cfif trim(emstruct[i]) neq ''>
		<cfset ctr = ctr+1>
			<cfset QueryAddRow( myq3 ) />
			<cfset myq3["name"][ctr] = listgetat(emstruct[i],1,"~") />
			<cfset myq3["displayname"][ctr] = listgetat(emstruct[i],2,"~")>
			<cfset myq3["title"][ctr] = listgetat(emstruct[i],3,"~") />
			<cfset myq3["mail"][ctr] = listgetat(emstruct[i],4,"~") />
		</cfif>
		</cfloop>	
		
		
<cfquery name="myq" dbtype="query">
select distinct name, displayname, title, mail
from myq3
order by displayname
</cfquery>

	<cflayout name="thelayout"  type="vbox">
            <cflayoutarea style="height:150;width:100%;" overflow="auto">
<table bgcolor="ffffff" cellpadding="4" cellspacing="0" width="100%">
<!--- <tr>
	<td><strong class="bodyTextsm">Mail</strong></td>
	<td><strong class="bodyTextsm">Title</strong></td>
</tr> --->
<cfset currrow = "">
<cfset ctr = 0>

<cfoutput query="myq">
<cfif mail contains "@">
	<cfset newem = listgetat(mail,1,"@") & "@amecfw.com">
<cfelse>
	<cfset newem = mail>
</cfif>
<cfif trim(name) neq ''>
<cfif trim(currrow) neq "#trim(displayname)#~#trim(title)#">

<tr <cfif ctr mod 2 is 0>bgcolor="efefef"</cfif>>
	<td class="bodyTextsm" >&nbsp;&nbsp;<a href="javascript:void(0);" onclick="window.opener.#srcscriptname#('#displayname#');window.close();"  class="bodyTextsm">#displayname#</a></td>
	<td class="bodyTextsm">#title#</td>
</tr>

	<cfset ctr = ctr+1>
	<cfset currrow = "#trim(displayname)#~#trim(title)#">
</cfif>
</cfif>
</cfoutput>
</table></cflayoutarea></cflayout>
		
</cfif>

</body>