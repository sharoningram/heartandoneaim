<p style="font-family:Segoe UI;font-size:11pt;">The immediate hospitalisation of an injured person for more than 24hrs or an injury resulting from:</p>
<ul>
	<li style="font-family:Segoe UI;font-size:11pt;">Electrical contact</li>
	<li style="font-family:Segoe UI;font-size:11pt;">Unconsciousness as the result of a concussion</li>
	<li style="font-family:Segoe UI;font-size:11pt;">A fracture of the skull, spine, pelvis, arm, leg, hand or foot</li>
	<li style="font-family:Segoe UI;font-size:11pt;">Amputation of an arm, leg, hand, foot, finger or toe</li>
	<li style="font-family:Segoe UI;font-size:11pt;">Third degree burns</li>
	<li style="font-family:Segoe UI;font-size:11pt;">Permanent or temporary loss of sight, or</li>
	<li style="font-family:Segoe UI;font-size:11pt;">Asphyxiation or poisoning</li>
</ul>
<p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p>
