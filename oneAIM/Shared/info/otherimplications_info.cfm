<p style="font-family:Segoe UI;font-size:11pt;">Further to the Primary Incident type are there any other incident types that need recording?<br><br>
A selection in the Secondary Incident type field will activate the appropriate tab for data entry.<br><br><em>Example: A vehicle collision occurs which results in the injury of the driver. The injury will be recorded as the 'Primary Incident Type'. The Asset Damage resulting from the collision will be recorded as a 'Secondary Incident Type'.</em></p>
<p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p>


