<p style="font-family:Segoe UI;font-size:11pt;">Describe what happened. Be concise and to the point and do not include any personal details such as someone's name.<br>
Example:<br>
<strong>(When and Where)</strong> At approximately 08:00am on Monday 1st January an electrician was walking outside between tank A and tank B heading towards the slurry pit on Client A�s site. The electrician was walking on a dedicated walkway at ground floor level.<br>
<strong>(Conditions)</strong> It had previously been raining and the ground conditions were wet, but the lighting was adequate at the time.<br>
<strong>(What)</strong> The electrician was carrying his tool kit with him when he stepped onto wet uneven ground which caused him to stumble. The weight of his body was shifted onto his left ankle, which resulted in the ankle becoming swollen. There were three witnesses.<br>
<strong>(Result)</strong> The electrician was treated on the scene by a first aider and was transferred to the medical room for further assessment. 
</p>
<p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p>

