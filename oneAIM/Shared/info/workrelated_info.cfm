<p style="font-family:Segoe UI;font-size:11pt;">If in the work environment, assume work related unless:<br>
<ul style="font-family:Segoe UI;font-size:11pt;">
<li>Member of general public</li>
<li>Signs and symptoms surface at work but result solely from non-work related event or exposure</li>
<li>Eating, drinking and preparing own food</li>
<li>Doing personal things outside working hours, even if at the establishment.</li>
<li>Personal grooming, self-medication for non-work-related condition, intentional self-inflicted injury</li>
<li>Company parking lot and/or whilst commuting</li>
<li>Common cold or flu</li>
</ul>
</p>
<p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p>

