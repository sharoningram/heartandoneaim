<!---  <CF_WEEKENDDATE>

Returns a list of dates based on values enetered in.
Default returns Dates for Fridays of the current year.

Attributes:
Startday - Specifies what day to return. Values can be Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday. Default is Friday
Checkyear - Specifies the year to look up dates for. Default is current year.
Numberweeks - Specifies the interval to get dates to return. Default is 2.
PayPeriod - specifies whether to display bi-weekly pay period dates "yes" or "No" to specify non paying week dates. Can only be used if week iteration is 2

Returns variable caller.weekEndingDates (list of dates)


Usage:
<cf_weekenddate> 
Returns list of weekending dates with defaul values, Fridays of Current year. 

<cf_weekenddate Startday="Monday" Checkyear="2004" Numberweeks="1">
Returns list of dates that are Mondays for every week in 2004

The returned list can be output in the calling page with a loop:
<cfloop list="#weekEndingDates#" index="i">
	#dateformat(i,"dddd mmmm d, yyyy")#<br>
</cfloop>
 --->

<cfset weekEndingDates = "">
<cfparam name="attributes.startday" default="Friday">
<cfparam name="attributes.checkyear" default="#year(Now())#">
<cfparam name="attributes.numberweeks" default="2">
<cfparam name="attributes.PayPeriod" default="yes">

<cfswitch expression="#attributes.startday#">
	<cfcase value="Monday,Mon,2">
		<cfset basenum = "2">
	</cfcase>
	<cfcase value="Tuesday,Tue,Tues,3">
		<cfset basenum = "3">
	</cfcase>
	<cfcase value="Wednesday,Wed,4">
		<cfset basenum = "4">
	</cfcase>
	<cfcase value="Thursday,Thur,Thurs,5">
		<cfset basenum = "5">
	</cfcase>
	<cfcase value="Friday,Fri,6">
		<cfset basenum = "6">
	</cfcase>
	<cfcase value="Saturday,Sat,7">
		<cfset basenum = "7">
	</cfcase>
	<cfcase value="Sunday,Sun,1">
		<cfset basenum = "1">
	</cfcase>
	<cfdefaultcase>
		<cfset basenum = "6">
	</cfdefaultcase>
</cfswitch>
<cfif not isNumeric(attributes.checkyear)>
	<cfset attributes.checkyear = year(Now())>
</cfif>
<cfif not isNumeric(attributes.numberweeks)>
	<cfset attributes.numberweeks = 2>
</cfif>
<cfset q1 = "1/1/#attributes.checkyear#">




<cfif not attributes.PayPeriod and attributes.numberweeks eq 2>

	<cfset firstNum = basenum - DAYOFWEEK(q1)>
	<cfif firstNum gte 0>
		<cfset firstDate = dateadd("d",firstNum,q1)>
	<cfelseif firstNum lt 0>
		<cfset firstDate = dateadd("d",7-abs(firstNum),q1)>
	</cfif>
	<!--- <cfset firstdate = dateadd("ww",attributes.numberweeks-1,firstdate)> --->
	<cfset weekiteration = attributes.numberweeks*7>
	<cfloop from="1" to="#evaluate(round(53/attributes.numberweeks))#" index="i">
		<cfif year(firstdate) eq attributes.checkyear>
			<cfset weekEndingDates = listappend(weekEndingDates,dateformat(firstDate,'mm/dd/yyyy'))>
		</cfif>
		<cfset firstDate = dateadd("d",weekiteration,dateformat(firstDate,'mm/dd/yyyy'))>
	</cfloop>

<cfelse>

	<cfset firstNum = basenum - DAYOFWEEK(q1)>
	<cfif firstNum gte 0>
		<cfset firstDate = dateadd("d",firstNum,q1)>
	<cfelseif firstNum lt 0>
		<cfset firstDate = dateadd("d",7-abs(firstNum),q1)>
	</cfif>
	<cfset firstdate = dateadd("ww",attributes.numberweeks-1,firstdate)>
	<cfset weekiteration = attributes.numberweeks*7>
	<cfloop from="1" to="#evaluate(round(53/attributes.numberweeks))#" index="i">
		<cfif year(firstdate) eq attributes.checkyear>
			<cfset weekEndingDates = listappend(weekEndingDates,dateformat(firstDate,'mm/dd/yyyy'))>
		</cfif>
		<cfset firstDate = dateadd("d",weekiteration,dateformat(firstDate,'mm/dd/yyyy'))>
	</cfloop>

</cfif>

<cfset caller.weekEndingDates = weekEndingDates>