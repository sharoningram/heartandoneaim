
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 

"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">

<cfoutput>
<link href="/#getappconfig.oneAIMpath#/shared/css/ers_local.css" rel="stylesheet" type="text/css"> 
<link href="/#getappconfig.oneAIMpath#/shared/css/jquery-ui.css" rel="stylesheet" type="text/css"> 
<link href="/#getappconfig.oneAIMpath#/shared/css/jquery-uismooth.css" rel="stylesheet" type="text/css"> 


	<!--- Include scripts for fusion charts and jquery --->
<script src="/#getappconfig.oneAIMpath#/shared/js/jquery-1.8.3.min.js"></script>
<script src="/#getappconfig.oneAIMpath#/shared/js/jquery-ui.js"></script>
<script type="text/javascript" src="/#getappconfig.oneAIMpath#/shared/js/fusioncharts.js"></script>
<script type="text/javascript" src="/#getappconfig.oneAIMpath#/shared/js/themes/fusioncharts.theme.fint.js"></script>
<script type="text/javascript" src="/#getappconfig.oneAIMpath#/shared/js/themes/fusioncharts.theme.carbon.js"></script>
<script type="text/javascript" src="/#getappconfig.oneAIMpath#/shared/js/themes/fusioncharts.theme.ocean.js"></script>
<script type="text/javascript" src="/#getappconfig.oneAIMpath#/shared/js/themes/fusioncharts.theme.zune.js"></script>
</cfoutput>

<!--- Script used for making scrollable tables --->
<script>
	// Run on window load in case images or other scripts affect element widths
	$(window).on('load', function() {
		// Check all tables. You may need to be more restrictive.
		$(mytab).each(function() {
			var element = $(this);
			// Create the wrapper element
			var scrollWrapper = $('<div />', {
				'class': 'scrollable',
				'html': '<div />' // The inner div is needed for styling
			}).insertBefore(element);
			// Store a reference to the wrapper element
			element.data('scrollWrapper', scrollWrapper);
			// Move the scrollable element inside the wrapper element
			element.appendTo(scrollWrapper.find('div'));
			// Check if the element is wider than its parent and thus needs to be scrollable
			if (element.outerWidth() > element.parent().outerWidth()) {
				element.data('scrollWrapper').addClass('has-scroll');
			}
			// When the viewport size is changed, check again if the element needs to be scrollable
			$(window).on('resize orientationchange', function() {
				if (element.outerWidth() > element.parent().outerWidth()) {
					element.data('scrollWrapper').addClass('has-scroll');
				} else {
					element.data('scrollWrapper').removeClass('has-scroll');
				}
			});
		});
	});
	</script>
</head>

<cfif fusebox.fuseaction contains "print">
<body onLoad="xprint()"> 
<cfelse>
<body leftmargin="0" topmargin="0" rightmargin="0">
</cfif>

