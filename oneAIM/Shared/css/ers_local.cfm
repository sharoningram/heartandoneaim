<style>
.breakhere {page-break-before: always}
.breakhereafter {page-break-after: always}


.midbox{
border:1px solid;
border-color:#afafaf;
font-family:Segoe UI;
font-size: 10pt;
color:#5f2468;
margin-top:40px;
margin-bottom:40px;
width: 42%;
 box-shadow: 10px 3px  10px 2px #888888;

}
.midboxadmin{
border:1px solid;
border-color:#afafaf;
font-family:Segoe UI;
font-size: 10pt;
color:#5f2468;
margin-top:40px;
margin-bottom:40px;
width: 20%;
 box-shadow: 10px 3px  10px 2px #888888;

}
.midboxhome{
border:1px solid;
border-color:#afafaf;
font-family:Segoe UI;
font-size: 10pt;
color:#5f2468;
margin-top:2px;
margin-bottom:0px;
margin-left:10px;
width: 79%;
 box-shadow: 10px 3px  10px 2px #888888;

}

#lrgselect {
width:81%;
}

#lrgselectsearch {
width:100%;
}

#widetxt {
width:94%;
}
.formlable{
background-color:#e7f5ff;
font-family: Segoe UI;
	font-size: 10pt;

	color: #5f5f5f;
	font-style: normal;
}





.formlablelt{
background-color:#f2f9fe;
font-family: Segoe UI;
	font-size: 10pt;

	color: #5f5f5f;
	font-style: normal;
}

.tubemapon{
color:#ffffff;
font-family:Segoe UI;
font-size:10pt;
background-color:#5f2468;
}
.tubemapcancel{
color:#2f2f2f;
font-family:Segoe UI;
font-size:10pt;
background-color:#88DBDF;
}

.tubemapoff{
color:#000000;
font-family:Segoe UI;
font-size:10pt;
background-color:#95d2fb;
}
.midboxtitle{
background:#c8e72d;
color:#5f2468;
font-family:Segoe UI;
font-size: 11pt;
width: 100%;
padding-top:3px;
padding-bottom:3px;
}
.midboxtitletxt{
text-decoration: none;
color:#800080;
font-family:Segoe UI;
font-size: 10pt;

}
.greytext {
	font-family: Segoe UI;
	font-size: 11px;
	color: #a0a0a0;
	font-style: normal;
}
.greyText {
	font-family: Segoe UI;
	font-size: 11px;
	color: #afafaf;
	font-style: normal;
}
.error {  
	font-family: Segoe UI, Segoe UI, Verdana; 
	font-size: 10px; 
	font-weight: bold; 
	color: #FF0000
}

.button { 
	background-color: #EBEBEB; 
	border-color: #DEDEDE; 
	border-width: 1; 
	color: 5C5C5C; 
	font-size: 8pt; 
	font-family: Segoe UI; 
	cursor : hand;
}

.password {
	font-family : Segoe UI;
	font-size : 12px;
	font-style : normal;
}

.input {
	background-color: #FFF9CD; 
	border-color: #DEDEDE; 
	border-width: 1; 
	color: 5C5C5C; 
	font-size: 8pt; 
	font-family: Segoe UI; 
	width: 415px;
	
}
.select {

	border-color: #DEDEDE; 
	border-width: 1; 
	color: 5C5C5C; 
	font-size: 8pt; 
	font-family: Segoe UI; 
	width: 525px;
}

.selectGen {

	
	border: 1px solid #5f2468; 
	color: #5f2468; 
	font-size: 10pt; 
	font-family: Segoe UI; 
}

.selectgen {

	
	border: 1px solid #5f2468; 
	color: #5f2468; 
	font-size: 10pt; 
	font-family: Segoe UI; 
}
.selectGenBTN {

	background-color:#cfcfcf;
	border: 2px solid #cfcfcf; 
	color: #5f2468; 
	font-size: 10pt; 
	font-family: Segoe UI; 
}
.selectGenBTNgreen {

	background-color:#88cdfc;
	border: 2px solid #88cdfc; 
	color: #5f2468; 
	font-size: 10pt; 
	font-family: Segoe UI; 
}

.selectGenoff {

	border-color: #DEDEDE; 
	border-width: 1; 
	color: 000000; 
	background-color: efefef;
	font-size: 8pt; 
	font-family: Segoe UI; 
}
.select2 {
	clip:rect(3px, 139px, 186px, 3px);
	border-color: #DEDEDE; 
	border-width: 1; 
	color: 5C5C5C; 
	font-size: 8pt; 
	font-family: Segoe UI; 
	width: 415px;
}
.bodyTextBlk {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #000000;
	font-style: normal;
}
.bodytext {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyText {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}




.bodyTexthd {
	font-family: Segoe UI;
	font-size: 14pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextPurple {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f2468;
	font-style: normal;
	
}

.bodytextpurple {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f2468;
	font-style: normal;
	
}

.bodyTextpurple {
	font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f2468;
	font-style: normal;
	
}
.bodyTextbldsm {
	font-family: Segoe UI;
	font-size: 10px;
	/* color: #5f2468; */
	color: #000000;
	font-style: bold;
}
.bodyTextsm {
	font-family: Segoe UI;
	font-size: 12px;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodytextsm {
	font-family: Segoe UI;
	font-size: 12px;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodytextsmpurple{
	font-family: Segoe UI;
	font-size: 12px;
	color: #5f2468; 
	font-style: normal;
	text-decoration:none;
}
.bodyTextlg {
	font-family: Segoe UI;
	font-size: 11pt;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10pt;
	/* color: #5f2468; */
	color: #5f5f5f;
	font-style: normal;
	}

.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #FFFFFF;
	font-style: normal;
}
.bodytextwhite{
	font-family: Segoe UI;
	font-size: 10pt;
	color: #FFFFFF;
	font-style: normal;
}


.bodytextwhitelg {
	font-family: Segoe UI;
	font-size: 12pt;
	color: #FFFFFF;
	font-style: normal;
}
.redText {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #F00000;
	font-style: normal;
}



.calText {
	font-family: Segoe UI;
	font-size: 9px;
	color: #000000;
	font-style: normal;
}
.callinkText {
	font-family: Segoe UI;
	font-size: 9px;
	color: blue;
	font-style: normal;
}
.redcalText {
	font-family: Segoe UI;
	font-size: 9px;
	color: #F00000;
	font-style: normal;
}
.greencalText {
	font-family: Segoe UI;
	font-size: 9px;
	color: #008000;
	font-style: normal;
}
.greenText {
	font-family: Segoe UI;
	font-size: 10pt;
	color: #008000;
	font-style: normal;
}
.greenTextdd {
	font-family: Segoe UI;
	font-size: 10pt;
	background-color: #00b050;
	font-style: normal;
	color: #000000;
}
.redTextdd {
	font-family: Segoe UI;
	font-size: 10pt;
	background-color: #ff0000;
	font-style: normal;
	color: #000000;
}
.orangeTextdd{
font-family: Segoe UI;
	font-size: 10pt;
	background-color: #f36729;
	font-style: normal;
	color: #000000;

}
.purpleTextdd {
	font-family: Segoe UI;
	font-size: 10pt;
	background-color: #dfdfdf;
	font-style: normal;
	color: #5f2468;
}
.yellowTextdd {
	font-family: Segoe UI;
	font-size: 10pt;
	background-color: #ffc000;
	font-style: normal;
	color: #000000;
}
.whitebg2 { 
	background-color: #ffffff; 
	font-family: Segoe UI;
	font-size: 10pt;
	color: #000000;
	font-style: normal;
	
}

.yellowhilite { 
	background-color: #FFFF8f; 
	font-family: Segoe UI;
	font-size: 11px;
	color: #000000;
	font-style: normal;
	
}

.headerTextWhite {
	font-family: Segoe UI;
	font-size: 31px;
	color: #FFFFFF;
	font-style: normal;
}
.fileUpload {
	background-color: #FFF9CD; 
	border-color: #DEDEDE; 
	border-width: 1; 
	color: 5C5C5C; 
	font-size: 8pt; 
	font-family: Segoe UI; 
	width: 315px;
}

.tableRowOver {
background-color: #E0D9FF; 
}
.tableRowOut {
background-color: #000000; 
}
.purplebg {
background: #0076cd;

}

.blackbg{
background: #000000;
}
.whitebg {
background: #ffffff;

}
.greybg {
background: #cfcfcf;

}
.ltTeal {
background: #ffffff;
}
.h2 {
font-family:Segoe UI;
font-size:16pt;
font-weight:normal;
color:#301034;

}
.h3 {
font-family:Segoe UI;
font-size:14pt;
font-weight:normal;
color:#301034;

}
.h3sm {
font-family:Segoe UI;
font-size:11pt;
font-weight:normal;
color:#301034;

}
#leftnav {
    float: left;
    width: 10%;
    height: 100%;
    background: #cfcfcf; 
    }
#midstripe {
 background: #10b2ba;
	width: 1%;
  float: left;
  height: 100%;
}
#bodycontent {
   
    background: #ffffff;
	padding-top: 0px;
	padding-bottom: 0px;
	padding-left: 0px;
	padding-right: 0px;
    }
.stripe0 {
width: 100%;
font-family:Segoe UI;
	font-size:16pt;
color:#5f2468;
	 position:fixed;
}
.stripeb {
    width: 100%;
     background: #0076cd;
	font-family:Segoe UI;
	font-size:10pt;
	color:#ffffff;
	padding-top: 2px;
	padding-bottom: 2px;
	      }
.stripe1 {
    width: 100%;
    position:fixed;
    background: #0076cd;
	font-family:Segoe UI;
	font-size:10pt;
	color:#ffffff;
	padding-top: 0px;
	padding-bottom: 0px;
	      }
.stripe2 {
    width: 100%;
    height: 33;
    background: #cfcfcf;
	 text-align: center;
    }
	.stripe3 {
    width: 100%;
    height: 2;
    background: #cfdf24;
    }
.leftstrong {
	font-family:Segoe UI;
	font-size:11pt;
	color:#5f2468;
	padding-left: 1px;
}
.leftlink {
	font-family:Segoe UI;
	font-size:10pt;
	color:#5f2468;
	text-decoration:none;
	padding-left: 8px;
	
}
.leftlink:hover {
	text-decoration:underline;
}
.content1of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 24%;
	 float: left;
text-align: left;
padding-top: 6px;
padding-left: 6px;
display:block;
clear: left
}
.content4of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 99%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content1of4r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 24.5%;
	 float: left;
text-align: left;
padding-top: 6px;
padding-left: 4px;
display:block;

}
.content3of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 74%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content3of4r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 75%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content1of1 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	
padding-top: 6px;


}
.content1of2 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 49%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content1of2r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 50%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content2of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 49%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content2of4r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 50%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content1of5 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 19%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content1of5r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 20%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content2of5 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 39%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content2of5r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 40%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content1of3r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 33%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content2of3r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 66%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content4of10 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 39%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content1of9 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 10%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content1of9r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 11%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content3of10r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 30%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;

}
.content1of3 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 33%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content2of3 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 66%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.content3of5 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 59%;
	 float: left;
text-align: left;
padding-top: 6px;
display:block;
clear: left
}
.homechartl {
height:26%;
width:79.5%;
border:1px solid;
border-radius: 4px;
float:left; 
}
.homechart {
height:26%;
width:79.5%;
border:1px solid;
border-radius: 4px;
float:left; 
}

.homechartr {
display:block;
float:left;
height:26%;
width:40px;
border:1px solid;
border-radius: 4px;
vertical-align: text-bottom;}



#sddm
{	margin: 0;
	padding: 0;
	z-index: 1;
	padding-top: 3px;
	float: center;
	background: #cfcfcf; 
	 text-align: center;
	 display:inline-block;
	 position:relative;
	}

#sddm li
{	margin: 0;
	padding-left: 10;
	padding-right: 10;
	list-style: none;
	float: left;
	font:  11pt Segoe UI;
	background: #cfcfcf; 
	 text-align: center;}

#sddm li a
{	display: block;
	margin: 0 3px 0 0;
	padding: 4px 0px;
	
	
	color: #5f2468;
	text-align: center;
	text-decoration: none}



#sddm div
{	position: absolute;
	visibility: hidden;
	margin: 0;
	padding-top:4px;
	 text-align: center;
	background: #cfcfcf;
	border: 1px solid #cfcfcf}

	#sddm div a
	{	position: relative;
		display: block;
		margin: 0;
		padding: 5px 3px;
		width: auto;
		white-space: nowrap;
		text-align: left;
		text-decoration: none;
		background: #ffffff;
		color: #5f2468;
		font: 11pt Segoe UI}

	#sddm div a:hover
	{	background: #cfdb00;
		color: #5f2468}

/*Style 'show menu' label button and hide it by default*/
.show-menu {
	font-family: "Helvetica Neue", Helvetica, Segoe UI, sans-serif;
	text-decoration: none;
	color: #5f2468;
	background: #cfdb00;
	text-align: center;
	padding: 6px 0;
	display: none;
	
}


/*Hide checkbox*/
#show-menu{
    display: none;
    -webkit-appearance: none;
}

/*Show menu when invisible checkbox is checked*/
#show-menu:checked ~ #sddm{
    display: block;
}

@media screen and (max-width : 1100px){
	/*Make dropdown links appear inline*/
	#sddm {
		position: relative;
		 display: none; 
		 z-index: 1;
	}
	/*Create vertical spacing*/
	#sddm li, #sddm div {
		margin-bottom: 1px;
	}
	/*Make all menu links full width*/
	#sddm li, #sddm a, #sddm div {
		width: 96%;
	}
	/*Display 'show menu' link*/
	.show-menu {
		display:block;
	}
	
	.stripe0 {
       width: 100%;
font-family:Segoe UI;
	font-size:9pt;
    }
.content1of5 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content3of5 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of2 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of2r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of9 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of9r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content4of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
	.content3of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
		.content2of4 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of5r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content1of4r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
	.content3of4r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
		.content2of4r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content2of5 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content2of5r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}

.content4of10 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.content3of10r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;

}

		.content1of3 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
		.content2of3 {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
		.content1of3r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
		.content2of3r {
	font-family:Segoe UI;
	font-size:10pt;
	width: 100%;
	 float: left;
text-align: left;
}
.homechart {
height:70%;
width:83%;
border:1px solid;
border-radius: 4px;
float:left; }

.homechartl {
height:70%;
width:83%;
border:1px solid;
border-radius: 4px;
float:left; }

.homechartr {
display:block;
float:left;
height:70%;
width:42px;
border:1px solid;
border-radius: 4px;
vertical-align: text-bottom;}

#lrgselect {
width:81%;
}
#widetxt {
width:94%;
}
#toptablefrm {
width:100%;
}
}
.img {
     vertical-align: middle;
	max-width: 21%;
 } 
 
 
 	
	/* Scrollable table styling */
	.scrollable.has-scroll {
		position:relative;
		overflow:hidden; /* Clips the shadow created with the pseudo-element in the next rule. Not necessary for the actual scrolling. */
	}
	.scrollable.has-scroll:after {
		position:absolute;
		top:0;
		left:100%;
		width:50px;
		height:100%;
		border-radius:10px 0 0 10px / 50% 0 0 50%;
		box-shadow:-5px 0 10px rgba(0, 0, 0, 0.25);
		content:'';
	}

	/* This is the element whose content will be scrolled if necessary */
	.scrollable.has-scroll > div {
		overflow-x:auto;
	}

	/* Style the scrollbar to make it visible in iOS, Android and OS X WebKit browsers (where user preferences can make scrollbars invisible until you actually scroll) */
	.scrollable > div::-webkit-scrollbar {
		height:12px;
	}
	.scrollable > div::-webkit-scrollbar-track {
		box-shadow:0 0 2px rgba(0,0,0,0.15) inset;
		background:#f0f0f0;
	}
	.scrollable > div::-webkit-scrollbar-thumb {
		border-radius:6px;
		background:#ccc;
	}
	
	</style>