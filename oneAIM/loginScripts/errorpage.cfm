<body leftmargin="0" topmargin="0" rightmargin="0">
<cfinclude template="../appconfig.cfm">
<table width="100%" cellpadding="2" cellspacing="0" border="0">
<tr bgcolor="ffffff">
	<td align="right" colspan="2"><img src="/#oneaimroot#/images/logoR.jpg" border="0" class="img"></td>
</tr>
<tr bgcolor="5f2468"><td style="font-family:Segoe UI;color:ffffff;font-size:12pt;">&nbsp;<a href="/#oneaimroot#" style="color:ffffff;text-decoration:none;">oneAIM</a></td><td align="right" style="font-family:Segoe UI;color:ffffff;font-size:10pt;"></td></tr>

<tr>
	<td colspan="2" align="center" style="font-family:Segoe UI;color:5f2468;font-size:12pt;"><br><br>
We're sorry, there seems to an error accessing the page you have requested. <br>
A site administrator has been notified and will be working to correct the problem.<br><br>
<a href="/#oneaimroot#" style="font-family:Segoe UI;font-size:12pt;">Return to oneAIM</a>
	</td>
</tr>
</table>


<cfset newdiag = #replace(error.diagnostics, "#chr(34)#", "'", "all")#>
<cfmail to="#error.mailto#" Subject="oneAIM error" From="#fromemail#">
Attention an Error has Occurred:
		
Error created by: #cookie.useremail#
Client Remote Address: #error.remoteaddress#		
Client Browser: #error.browser#		
Date/Time: #dateformat(error.datetime, "MM/DD/YYYY")# at #timeformat(error.datetime, "h:mm tt")#
HTTP Referer: #error.HTTPReferer#		
Query String: #error.querystring#		
Template: #error.template#		
Diagnostics: #newdiag#
		
</cfmail>
<br><br>
<div  style="font-family:Segoe UI;backGround-color:5f2468;font-size:10pt;color:ffffff" align="right">Amec Foster Wheeler oneAIM&nbsp;&nbsp;&nbsp;</div>
</body>