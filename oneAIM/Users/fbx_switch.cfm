


<cfswitch expression = "#fusebox.fuseaction#">

<cfcase value="main,exportuserlist">
	<cfset attributes.xfa.usermanagement = "users.usermanagement">
	<cfset attributes.xfa.main = "users.main">
	<cfset attributes.xfa.lookupemail = "users.lookupemail">
	<cfset attributes.xfa.buildoulistslookup = "users.buildoulistslookup">
	<cfset attributes.xfa.exportuserlist = "users.exportuserlist">
	<cfparam name="user" default="0">
	<cfinclude template="queries/qry_getSearchResult.cfm">
	<cfinclude template="actions/act_getuserassignments.cfm">
	<cfinclude template="queries/qry_getuserDetails.cfm">
	
	<cfinclude template="queries/qry_getBusinessLines.cfm">
	<cfinclude template="queries/qry_getOUs.cfm">
	<cfinclude template="queries/qry_getallOUs.cfm">
	<cfif fusebox.fuseaction eq "exportuserlist">
		<cfinclude template="actions/act_exportuserlist.cfm">
	<cfelse>
		<cfinclude template="forms/frm_adduser.cfm">
		<cfinclude template="forms/frm_searchuser.cfm">
	</cfif>
	
</cfcase>

<cfcase value="usermanagement">
	<cfset attributes.xfa.main = "users.main">
	
	 <cfinclude template="actions/act_usermanagement.cfm">
</cfcase>
<cfcase value="buildoulistslookup">
	<cfset attributes.xfa.buildoulistslookup = "users.buildoulistslookup">
	<cfinclude template="actions/act_buildoulists.cfm">
	<cfinclude template="queries/qry_getoulists.cfm">
	<cfinclude template="forms/frm_buildoulists.cfm">
</cfcase>
<cfcase value="groups">
	<cfinclude template="displays/dsp_usergroups.cfm">
</cfcase>
<cfcase value="lookupemail">
	<cfinclude template="actions/act_lookupemail.cfm">
</cfcase>

<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
