<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "users\actions\act_exportuserlist.cfm", "reports\exports"))>
<cfset thefilename = "UserList_#timeformat(now(),'hhnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset theSheet = spreadsheetnew("User List","true")>
<cfset ublstruct = {}>
<cfloop query="getbusinesslines">
	<cfif not structkeyexists(ublstruct,ID)>
		<cfset ublstruct[ID] = name>
	</cfif>
</cfloop>
<cfloop query="getous">
	<cfif not structkeyexists(ublstruct,ID)>
		<cfset ublstruct[ID] = name>
	</cfif>
</cfloop>
<cfset userbls = getuserDetails.AssignedLocs>
<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"Name",1,1);
	 SpreadsheetSetCellValue(theSheet,"Email",1,2);
	 SpreadsheetSetCellValue(theSheet,"Status",1,3);
	 SpreadsheetSetCellValue(theSheet,"Role/Assigned To",1,4);
	 SpreadsheetSetCellValue(theSheet,"Assigned Projects",1,5);
	 SpreadsheetSetCellValue(theSheet,"Assigned Projects (man hours)",1,6);
</cfscript>



<cfset ctr = 1>
	<cfoutput query="getusersearch" group="userid">
	<cfset ctr = ctr+1>
	<cfif status neq 1>
		<cfset usestatus = "In-Active">
	<cfelse>
		<cfset usestatus = "Active">
	</cfif>
	 <cfset ctr2 = 0>
	 <cfset roleassigns = "">
		<cfoutput group="userrole"><cfset ctr2 = ctr2+1>
			<cfif userrole contains "OU "><cfset newrole = replace(userrole,"OU ","#request.oulabelshort# ")><cfelseif userrole contains "BU "><cfset newrole = replace(userrole,"BU ","#request.bulabelshort# ")><cfelse><cfset newrole = UserRole></cfif>
		
			<cfset useroleassign = newrole>
			<cfoutput>
				<cfloop list="#AssignedLocs#" index="i">
					<cfif structkeyexists(ublstruct,i)><cfset useroleassign = useroleassign & "(#ublstruct[i]#)"></cfif>
				</cfloop>
			</cfoutput>
			<cfset roleassigns = listappend(roleassigns,useroleassign)>
		</cfoutput>
	<cfscript> 
	 SpreadsheetSetCellValue(theSheet,"#lastname#, #firstname#",#ctr#,1);
	 SpreadsheetSetCellValue(theSheet,"#Useremail#",#ctr#,2);
	 SpreadsheetSetCellValue(theSheet,"#usestatus#",#ctr#,3);
	 SpreadsheetSetCellValue(theSheet,"#roleassigns# ",#ctr#,4);
	 if (structkeyexists(dataentrystruct,userid)) {
	 SpreadsheetSetCellValue(theSheet,"#replace(dataentrystruct[userid],'^',',','all')#",#ctr#,5);
	 }
	 else{
	 SpreadsheetSetCellValue(theSheet,"",#ctr#,5);
	}
	if (structkeyexists(manhourstruct,userid)) {
	 SpreadsheetSetCellValue(theSheet,"#replace(manhourstruct[userid],'^',',','all')#",#ctr#,6);
	 } else {
	  SpreadsheetSetCellValue(theSheet,"",#ctr#,6);
	}
</cfscript>

	
	</cfoutput> 


 <cfspreadsheet action="write" filename="#thefile#" name="theSheet" overwrite="true">  
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/reports/exports/#thefilename#"> 