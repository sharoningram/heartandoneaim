<cfparam name="currsubs" default="">
<cfparam name="sid" default="">
<cfparam name="currnames" default="">
<cfparam name="delsub" default="">
<cfparam name="type" default="">
<cfparam name="newsub" default="">
<cfparam name="doadd" default="no">

<cfif listfindnocase("buadmin,buvo,srrev",type) gt 0>
<cfquery name="getbusinesslines" datasource="#request.dsn#">
SELECT      ID,  Name, businessline
FROM            NewDials
WHERE   status <> 99 <cfif trim(currsubs) neq ''>and ID not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif> and    (Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (isgroupdial = 0)
</cfquery>


<cfif trim(currsubs) neq ''>
<cfquery name="getassignedbusinesslines" datasource="#request.dsn#">
SELECT      ID,  Name, businessline
FROM            NewDials
WHERE   status <> 99 <cfif trim(currsubs) neq ''>and ID  in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif> and    (Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (isgroupdial = 0)
</cfquery>
<cfloop query="getassignedbusinesslines">
	<cfset currnames = listappend(currnames,Name)>
</cfloop>

</cfif>

<cfelseif listfindnocase("oua,user,reviewer,mhi,advisor,ouvo",type) gt 0>
<cfquery name="getbusinesslines1" datasource="#request.dsn#">
SELECT      ID,  Name, businessline
FROM            NewDials
WHERE   status <> 99 <cfif trim(currsubs) neq ''>and ID not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif> and    (Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (isgroupdial = 0)
</cfquery>

<cfquery name="getbusinesslines" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS parentname
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
WHERE        (NewDials.status <> 99) <cfif trim(currsubs) neq ''>and NewDials.ID not in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif> AND (NewDials.isgroupdial = 0) AND (NewDials.Parent IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getbusinesslines1.id)#" list="Yes">))
ORDER BY parentname, NewDials.Name
</cfquery>


<cfif trim(currsubs) neq ''>
<cfquery name="getassignedbusinesslines" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS parentname
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
WHERE        (NewDials.status <> 99) <cfif trim(currsubs) neq ''>and NewDials.ID  in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#currsubs#" list="Yes">)</cfif> AND (NewDials.isgroupdial = 0) AND (NewDials.Parent IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getbusinesslines1.id)#" list="Yes">))
ORDER BY parentname, NewDials.Name
</cfquery>
<cfloop query="getassignedbusinesslines">
	<cfset currnames = listappend(currnames,Name)>
</cfloop>

</cfif>

</cfif>