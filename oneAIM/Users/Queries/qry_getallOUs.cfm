<cfquery name="getallous" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS parentname
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
WHERE        (NewDials.status <> 99) AND (NewDials.isgroupdial = 0) AND (NewDials.Parent IN (SELECT      ID
FROM            NewDials
WHERE   status <> 99 and    (Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (isgroupdial = 0)))
ORDER BY parentname, NewDials.Name
</cfquery>
