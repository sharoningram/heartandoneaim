<cfquery name="getous" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS parentname
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
WHERE        (NewDials.status <> 99) AND (NewDials.isgroupdial = 0) AND (NewDials.Parent IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getbusinesslines.id)#" list="Yes">))
ORDER BY parentname, NewDials.Name
</cfquery>