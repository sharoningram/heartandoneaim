<!--- 
<div class="content1of2r"> --->
<cfset ublstruct = {}>
<cfloop query="getbusinesslines">
	<cfif not structkeyexists(ublstruct,ID)>
		<cfset ublstruct[ID] = name>
	</cfif>
</cfloop>
<cfloop query="getous">
	<cfif not structkeyexists(ublstruct,ID)>
		<cfset ublstruct[ID] = name>
	</cfif>
</cfloop>
<cfset userbls = getuserDetails.AssignedLocs>
<!--- <cfdump var="#ublstruct#"> --->
<cfoutput>

<script type="text/javascript">
function validatefrm() {
 if((document.srchuserfrm2.sfirstname.value=="")&&(document.srchuserfrm2.slastname.value=="")&&(document.srchuserfrm2.sUseremail.value=="")&&(document.srchuserfrm2.sstatus[0].selected)&&(document.srchuserfrm2.sisadmin[0].selected)&&(document.srchuserfrm2.sishsselead[0].selected)){
 alert('Please enter a first name, last name, email address or make a selection');
 return false;
 }
}
</script>
<cfform action="#self#?fuseaction=#attributes.xfa.main#" method="post" name="srchuserfrm2" onsubmit="return validatefrm();">

<input type="hidden" name="submittype" value="search">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="98%">
<tr>
		<td colspan="2"><strong class="bodyTextWhite"> Search User</strong></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="4" cellspacing="1" border="0"   width="100%"  class="ltTeal">
	
	<tr >
		<td align="left" width="45%" class="formlable"><strong>First Name:</strong></td>
		<td width="55%"><cfinput type="text" name="sfirstname" value="#sfirstname#" class="selectgen" size="20" maxlength="100" required="No" message="Please enter a First Name"></td>
	</tr>
	<tr >
		<td align="left" width="45%" class="formlable"><strong>Last Name:</strong></td>
		<td width="55%"><cfinput type="text" name="slastname" value="#slastname#" class="selectgen" size="20" maxlength="100" required="No" message="Please enter a Last Name"></td>
	</tr>
	<tr >
		<td align="left" width="45%" class="formlable"><strong>Email Address:</strong></td>
		<td width="55%"><cfinput type="text" name="sUseremail" value="#sUseremail#" class="selectgen" size="20" maxlength="200" required="No" message="Please enter an email an email address"></td>
	</tr>
	<tr >
		<td align="left" width="45%" class="formlable"><strong>Role:</strong></td>
		<td width="55%"><select name="srole" size="1" class="selectgen"><option value="">-- Select One --</option>
		<option value="Global Admin" <cfif srole eq "Global Admin">selected</cfif>>Global Admin</option>
		<option value="BU Admin" <cfif srole eq "BU Admin">selected</cfif>>#request.bulabelshort# Admin</option>
		<option value="OU Admin" <cfif srole eq "OU Admin">selected</cfif>>#request.oulabelshort# Admin</option>
		<option value="HSSE Advisor" <cfif srole eq "HSSE Advisor">selected</cfif>>HSSE Advisor</option>
		<option value="User" <cfif srole eq "User">selected</cfif>>User</option>
		<option value="Reviewer" <cfif srole eq "Reviewer">selected</cfif>>Reviewer</option>
		<option value="Senior Reviewer" <cfif srole eq "Senior Reviewer">selected</cfif>>Senior Reviewer</option>
		<option value="Senior Executive View" <cfif srole eq "Senior Executive View">selected</cfif>>Senior Executive View</option>
		<option value="Reports Only" <cfif srole eq "Reports Only">selected</cfif>>Reports Only</option>
		<option value="BU View Only" <cfif srole eq "BU View Only">selected</cfif>>#request.bulabelshort# View Only</option>
		<option value="OU View Only" <cfif srole eq "OU View Only">selected</cfif>>#request.oulabelshort# View Only</option>
		<option value="Global IT" <cfif srole eq "Global IT">selected</cfif>>Global IT</option></select></td>
	</tr>
		<tr >
		<td align="left" width="45%" class="formlable"><strong>#request.bulabellong#:</strong></td>
		<td width="55%"><select name="sbuid" size="1" class="selectgen"><option value="">-- Select One --</option>
		<Cfloop query="getbusinesslines"><option value="#id#" <cfif listfind(sbuid,id) gt 0>selected</cfif>>#Name#</option></cfloop></select></td>
	</tr>
	<tr >
		<td align="left" width="45%" class="formlable"><strong>#request.oulabellong#:</strong></td>
		<td width="55%"><select name="souid" size="4" class="selectgen"  multiple>
		<Cfloop query="getallous" group="parentname"><option value="" disabled>#parentname#</option><cfloop><option value="#id#" <cfif listfind(souid,id) gt 0>selected</cfif>>&nbsp;&nbsp;#Name#</option></cfloop></cfloop></select></td>
	</tr>
	<tr >
		<td align="left" width="45%" class="formlable"><strong>Status:</strong></td>
		<td width="55%"><select name="sstatus" size="1" class="selectgen"><option value="">-- Select One --</option><option value="All" <cfif sstatus eq "all">selected</cfif>>All</option><option value="1" <cfif sstatus eq 1>selected</cfif>>Active</option><option value="0" <cfif sstatus eq 0>selected</cfif>>In-Active</option></select></td>
	</tr>
		<!--- <tr >
		<td align="right" width="45%"><strong class="bodytext">Is Admin:</strong></td>
		<td width="55%"><select name="sisadmin" size="1" class="selectgen"><option value="">-- Select One --</option><option value="All" <cfif sisadmin eq "all">selected</cfif>>All</option><option value="1" <cfif sisadmin eq 1>selected</cfif>>Yes</option><option value="0" <cfif sisadmin eq 0>selected</cfif>>No</option></select></td>
	</tr> --->
	<!--- <tr >
		<td align="right" width="45%"><strong class="bodytext">HSSE Lead:</strong></td>
		<td width="55%"><select name="sishsselead" size="1" class="selectgen"><option value="">-- Select One --</option><option value="All" <cfif sishsselead eq "all">selected</cfif>>All</option><option value="1" <cfif sishsselead eq 1>selected</cfif>>Yes</option><option value="0" <cfif sishsselead eq 0>selected</cfif>>No</option></select></td>
	</tr> --->
	<tr >
		<td width="45%"></td>
		<td width="55%"><input type="submit" value="Search" class="selectGenBTN">&nbsp;<input type="button" value="Clear Search Results" class="selectGenBTN" onclick="document.clearsearch.submit();"></td>
	</tr>
</table>
</td></tr></table>
</cfform>
<form name="clearsearch" method="post" action="#self#?fuseaction=#attributes.xfa.main#">

</form>
</cfoutput>
<cfif isdefined("getusersearch.recordcount")>
<!--- 
<style>
	
	/* Scrollable table styling */
	.scrollable.has-scroll {
		position:relative;
		overflow:hidden; /* Clips the shadow created with the pseudo-element in the next rule. Not necessary for the actual scrolling. */
	}
	.scrollable.has-scroll:after {
		position:absolute;
		top:0;
		left:100%;
		width:50px;
		height:100%;
		border-radius:10px 0 0 10px / 50% 0 0 50%;
		box-shadow:-5px 0 10px rgba(0, 0, 0, 0.25);
		content:'';
	}

	/* This is the element whose content will be scrolled if necessary */
	.scrollable.has-scroll > div {
		overflow-x:auto;
	}

	/* Style the scrollbar to make it visible in iOS, Android and OS X WebKit browsers (where user preferences can make scrollbars invisible until you actually scroll) */
	.scrollable > div::-webkit-scrollbar {
		height:12px;
	}
	.scrollable > div::-webkit-scrollbar-track {
		box-shadow:0 0 2px rgba(0,0,0,0.15) inset;
		background:#f0f0f0;
	}
	.scrollable > div::-webkit-scrollbar-thumb {
		border-radius:6px;
		background:#ccc;
	}

	</style>
	<script src="/#getappconfig.oneAIMpath#/shared/js/jquery-1.8.3.min.js"></script>
	<script>
	// Run on window load in case images or other scripts affect element widths
	$(window).on('load', function() {
		// Check all tables. You may need to be more restrictive.
		$(mytab).each(function() {
			var element = $(this);
			// Create the wrapper element
			var scrollWrapper = $('<div />', {
				'class': 'scrollable',
				'html': '<div />' // The inner div is needed for styling
			}).insertBefore(element);
			// Store a reference to the wrapper element
			element.data('scrollWrapper', scrollWrapper);
			// Move the scrollable element inside the wrapper element
			element.appendTo(scrollWrapper.find('div'));
			// Check if the element is wider than its parent and thus needs to be scrollable
			if (element.outerWidth() > element.parent().outerWidth()) {
				element.data('scrollWrapper').addClass('has-scroll');
			}
			// When the viewport size is changed, check again if the element needs to be scrollable
			$(window).on('resize orientationchange', function() {
				if (element.outerWidth() > element.parent().outerWidth()) {
					element.data('scrollWrapper').addClass('has-scroll');
				} else {
					element.data('scrollWrapper').removeClass('has-scroll');
				}
			});
		});
	});
	</script> --->
	<cfoutput>
<form action="#self#?fuseaction=#attributes.xfa.exportuserlist#" name="exportfrm" method="post" target="_blank">
	<input type="hidden" name="SUBMITTYPE" value="#SUBMITTYPE#">
<input type="hidden" name="SFIRSTNAME" value="#SFIRSTNAME#">
<input type="hidden" name="SLASTNAME" value="#SLASTNAME#">
<input type="hidden" name="SUSEREMAIL" value="#SUSEREMAIL#">
<input type="hidden" name="SROLE" value="#SROLE#">
<input type="hidden" name="SBUID" value="#SBUID#">
<input type="hidden" name="SOUID" value="#SOUID#">
<input type="hidden" name="SSTATUS" value="#SSTATUS#">
<input type="hidden" name="user" value="#user#">
	</form>
	</cfoutput>
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="98%" id="mytab">
	
	<tr>
		<td ><strong class="bodyTextWhite">Search Results</strong></td>
		<td align="right" ><a href="javascript:void(0);" onclick="document.exportfrm.submit();"><img src="images/excel.png" border="0"  style="padding-right:8px;padding-top:0px;"></a></td>
	</tr>
	
	<tr>
		<td colspan="2"> 
		<table cellpadding="3" cellspacing="0" border="0" class="ltTeal" width="100%">
			<tr class="whitebg">
				<td align="left" width="12%"><strong class="bodytext">Name</strong></td>
				<td align="left" width="16%"><strong class="bodytext">Email</strong></td>
				<td align="left" width="6%"><strong class="bodytext">Status</strong></td>
				<td align="left" width="32%"><strong class="bodytext">Role/Assigned To</strong></td>
				<td align="left" width="17%"><strong class="bodytext">Assigned Projects</strong></td>
				<td align="left" width="17%"><strong class="bodytext">Assigned Projects (man hours)</strong></td>
				<!--- <td align="center"><strong class="bodytext">HSSE Lead</strong></td> --->
				<!--- <td align="left"><strong class="bodytext">Assigned To</strong></td> --->
			</tr>
	
	<tbody>
	<cfset ctr = 0>
	<cfoutput query="getusersearch" group="userid"><cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>bgcolor="f7f1f9"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="bodytext" nowrap><a href="javascript:void(0);" onclick="document.editusersearch.user.value=#userid#;document.editusersearch.submit();">#lastname#, #firstname#</a></td>
		<td class="bodytext">#Useremail#</td>
		<td class="bodytext" nowrap align="left"><cfif status neq 1>In-</cfif>Active</td>
		<td class="bodytext" align="left">
			<table cellpadding="2" cellspacing="0" border="0" width="60%">
			<cfset ctr2 = 0>
				<cfoutput group="userrole"><cfset ctr2 = ctr2+1>
				
				<tr <cfif ctr2 mod 2 is 0>bgcolor="d6a6d3"</cfif>><td class="bodytext" width="50%"><cfif userrole contains "OU ">#replace(userrole,"OU ","#request.oulabelshort# ")#<cfelseif userrole contains "BU ">#replace(userrole,"BU ","#request.bulabelshort# ")#<cfelse>#UserRole#</cfif></td><td class="bodytext" width="50%"><cfoutput><cfloop list="#AssignedLocs#" index="i"><cfif structkeyexists(ublstruct,i)>#ublstruct[i]#<br></cfif></cfloop></cfoutput></td></tr></cfoutput>
			</table>
		</td>
		<td class="bodytext" align="left"><cfif structkeyexists(dataentrystruct,userid)>#replace(dataentrystruct[userid],"^","<br>","all")#</cfif></td>
		<td class="bodytext" align="left"><cfif structkeyexists(manhourstruct,userid)>#replace(manhourstruct[userid],"^","<br>","all")#</cfif></td>
		<!--- <td class="bodytext" align="center"><cfif ishsselead>Yes<cfelse>No</cfif></td> --->
		<!--- <td class="bodytext" nowrap><cfloop list="#AssignedLocs#" index="i"><cfif structkeyexists(ublstruct,i)>#ublstruct[i]#<br></cfif></cfloop></td> --->
	</tr>
	</cfoutput>
	</tbody>
</table></td></tr></table>
<cfoutput>
<form name="editusersearch" method="post" action="#self#?fuseaction=#attributes.xfa.main#">
<input type="hidden" name="user" value="0">
<input type="hidden" name="submittype" value="search">
<input type="hidden" name="sfirstname" value="#sfirstname#">
<input type="hidden" name="slastname" value="#slastname#">
<input type="hidden" name="sUseremail" value="#sUseremail#">
<input type="hidden" name="sstatus" value="#sstatus#">
<input type="hidden" name="sisadmin" value="#sisadmin#">
<input type="hidden" name="sishsselead" value="#sishsselead#">
<input type="hidden" name="srole" value="#srole#">
<input type="hidden" name="sbuid" value="#sbuid#">
<input type="hidden" name="souid" value="#souid#">
</form>
</cfoutput>
</cfif>
<!--- </div> --->

<!--- 
<br><br>



<script src="/#getappconfig.oneAIMpath#/shared/js/jquery-1.8.3.min.js"></script>
<style>
#mytab2 th { font-weight: bold;font-family:Segoe UI;font-size:10pt;}
#mytab2 td, #mytab2 th { padding: 4px 4px;  }
#mytab2 td {font-family:Segoe UI;font-size:10pt; }
/* Mobile */
@media only screen and (max-width: 767px) {
	
	#mytab2.responsive { margin-bottom: 0; }
	
	.pinned { position: absolute; left: 0; top: 0; background: #fff; width: 45%; overflow: hidden; overflow-x: scroll; border-right: 1px solid #ccc; border-left: 1px solid #ccc; }
	.pinned #mytab2 { border-right: none; border-left: none; width: 100%; }
	.pinned #mytab2 th, .pinned #mytab2 td { white-space: nowrap; }
	.pinned td:last-child { border-bottom: 0; }
	
	div.table-wrapper { position: relative; margin-bottom: 20px; overflow: hidden; border-right: 1px solid #ccc; }
	div.table-wrapper div.scrollable #mytab2 { margin-left: 45%; }
	div.table-wrapper div.scrollable { overflow: scroll; overflow-y: hidden; }	
	
	#mytab2.responsive td, #mytab2.responsive th { position: relative; white-space: nowrap; overflow: hidden; }
	#mytab2.responsive th:first-child, #mytab2.responsive td:first-child, #mytab2.responsive td:first-child, #mytab2.responsive.pinned td { display: none; }
	
}
</style>


<script src="/#getappconfig.oneAIMpath#/shared/js/responsive-tables.js"></script>
<div style="border:1px solid;width:97.75%;border-color:#5f2468" >
<div class="purplebg" style="color:#ffffff;width:99.8%;font-family:Segoe UI;font-weight:bold;font-size:11pt;text-align:left;padding: 1px 1px;">Search Results</div> 
<table cellpadding="3" cellspacing="0" border="0" class="responsive" id="mytab2" width="100%">
		
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th >Status</th>
				<th>Admin</th>
				<th >Business Line</th>
			</tr>
	
	
	<cfset ctr = 0>
	<cfoutput query="getusersearch" group="userid"><cfset ctr = ctr+1>
	<tr  <cfif ctr mod 2 is 0>class="whitebg"<cfelse>class="ltTeal"</cfif>>
		<td class="selectgen" ><a href="javascript:void(0);" onclick="document.editusersearch.user.value=#userid#;document.editusersearch.submit();">#lastname#, #firstname#</a></td>
		<td class="selectgen">#Useremail#</td>
		<td class="selectgen" nowrap align="center"><cfif status neq 1>In-</cfif>Active</td>
		<td class="selectgen" align="center"><cfif Is_Admin>Yes<cfelse>No</cfif></td>
		<td class="selectgen" nowrap><cfoutput><cfif structkeyexists(ublstruct,businessline)>#ublstruct[BusinessLine]#<br></cfif></cfoutput></td>
	</tr>
	</cfoutput>

</table>
</div>
<cfoutput>
<form name="editusersearch" method="post" action="#self#?fuseaction=#attributes.xfa.main#">
<input type="hidden" name="user" value="0">
<input type="hidden" name="submittype" value="search">
<input type="hidden" name="sfirstname" value="#sfirstname#">
<input type="hidden" name="slastname" value="#slastname#">
<input type="hidden" name="sUseremail" value="#sUseremail#">
<input type="hidden" name="sstatus" value="#sstatus#">
</form>
</cfoutput>
</cfif>
</div>
 --->