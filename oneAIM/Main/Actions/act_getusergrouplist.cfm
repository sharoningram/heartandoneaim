<cfset gnuml = "">

<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>

<cfquery name="getbugnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
</cfquery>
<cfloop query="getbugnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>


</cfif>
<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>

<cfquery name="getsrgnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer')
</cfquery>
<cfloop query="getsrgnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>


</cfif>
<cfif listfindnocase(request.userlevel,"oU Admin") gt 0>

<cfquery name="getougnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
 and   groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
</cfquery>
<cfloop query="getougnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>


</cfif>
<cfif listfindnocase(request.userlevel,"Reviewer") gt 0>

<cfquery name="getrevgnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
 and   groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Reviewer')
</cfquery>
<cfloop query="getrevgnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>


</cfif>
<cfif trim(gnuml) eq ''>
	<cfset gnuml = 0>
</cfif>

