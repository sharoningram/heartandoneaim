<cfset rollstart = "#month(now())#/1/#year(now())-2#">
<cfset fullrollstart = "#month(now())#/1/#year(now())-3#">

<cfset ytdhrs = 0>
<cfset ytdallinc = 0>
<cfset ytdtririnc = 0>
<cfset ytdltiinc = 0>

<cfset rolldate = rollstart>
<cfloop from="1" to="24" index="i">
	<cfset rolldate = dateadd("m",1,rolldate)>
	<cfset rolldate = "#month(rolldate)#/#daysinmonth(rolldate)#/#year(rolldate)#">
	<cfset rollstart = listappend(rollstart,rolldate)>
</cfloop>

<cfset fullrolldate = fullrollstart>
<cfloop from="1" to="36" index="i">
	<cfset fullrolldate = dateadd("m",1,fullrolldate)>
	<cfset fullrolldate = "#month(fullrolldate)#/#daysinmonth(fullrolldate)#/#year(fullrolldate)#">
	<cfset fullrollstart = listappend(fullrollstart,fullrolldate)>
</cfloop>
<cfquery name="getrecordable" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass, YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) 
                         AS incmonth
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE   oneAIMincidents.isnearmiss = 'No'  AND (oneAIMincidents.IncAssignedTo IN (1, 2, 3, 4, 11))  and   oneAIMincidents.isWorkRelated = 'yes' and   (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">) AND
				 ((oneAIMincidents.PrimaryType = 1) AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)) OR 
				 (oneAIMincidents.PrimaryType = 5) AND oneaimincidents.OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))
				 
<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), oneAIMInjuryOI.OSHAclass
ORDER BY incyr, incmonth
</cfquery>
<cfquery name="getchartfats" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, YEAR(oneAIMincidents.incidentDate) AS fatyr, MONTH(oneAIMincidents.incidentDate) AS fatmonth
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE      oneAIMincidents.isWorkRelated = 'yes'  AND (oneAIMincidents.IncAssignedTo IN (1, 2, 3, 4, 11)) and    (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.incidentDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">) AND (oneAIMincidents.isFatality = 'yes')

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate)
ORDER BY fatyr, fatmonth
</cfquery>

<cfset totrecchrtstruct = {}>
<cfset allrecchrtstruct = {}>
<cfset LTIrecchrtstruct = {}>


<cfloop query="getrecordable">
	<cfswitch expression="#OSHAclass#">
		<cfcase value="1">
			<cfif incyr eq year(now())>
				<cfset ytdallinc = ytdallinc+inccnt>
			</cfif>
			<cfif not structkeyexists(allrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = allrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="2">
			<cfif incyr eq year(now())>
				<cfset ytdallinc = ytdallinc+inccnt>
				<cfset ytdtririnc = ytdtririnc+inccnt>
				<cfset ytdltiinc = ytdltiinc+inccnt>
			</cfif>
			<cfif not structkeyexists(allrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = allrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(totrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = totrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(LTIrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset LTIrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset LTIrecchrtstruct["#incyr#_#incmonth#"] = LTIrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
		</cfcase>
		<cfcase value="3,4">
			<cfif incyr eq year(now())>
				<cfset ytdallinc = ytdallinc+inccnt>
				<cfset ytdtririnc = ytdtririnc+inccnt>
			</cfif>
			<cfif not structkeyexists(allrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset allrecchrtstruct["#incyr#_#incmonth#"] = allrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
			<cfif not structkeyexists(totrecchrtstruct,"#incyr#_#incmonth#")>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = inccnt>
			<cfelse>
				<cfset totrecchrtstruct["#incyr#_#incmonth#"] = totrecchrtstruct["#incyr#_#incmonth#"]+inccnt>
			</cfif>
		</cfcase>
	</cfswitch>
</cfloop>
<cfloop query="getchartfats">
	<cfif fatyr eq year(now())>
		<cfset ytdallinc = ytdallinc+inccnt>
		<cfset ytdtririnc = ytdtririnc+inccnt>
	</cfif>
	<cfif not structkeyexists(allrecchrtstruct,"#fatyr#_#fatmonth#")>
		<cfset allrecchrtstruct["#fatyr#_#fatmonth#"] = inccnt>
	<cfelse>
		<cfset allrecchrtstruct["#fatyr#_#fatmonth#"] = allrecchrtstruct["#fatyr#_#fatmonth#"]+inccnt>
	</cfif>
	<cfif not structkeyexists(totrecchrtstruct,"#fatyr#_#fatmonth#")>
		<cfset totrecchrtstruct["#fatyr#_#fatmonth#"] = inccnt>
	<cfelse>
		<cfset totrecchrtstruct["#fatyr#_#fatmonth#"] = totrecchrtstruct["#fatyr#_#fatmonth#"]+inccnt>
	</cfif>
</cfloop>
<cfset allcharthrsstruct = {}>
<cfquery name="chartjdehrs" datasource="#request.dsn#">
SELECT        SUM(LaborHours.HO_HRS) + SUM(LaborHours.FIELD_HRS) + SUM(LaborHours.CRAFT_HRS) AS jdehrs, GroupLocations.LocationDetailID, 
                         YEAR(LaborHours.WEEK_ENDING_DATE) AS jdeyr, MONTH(LaborHours.WEEK_ENDING_DATE) AS jdemonth
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE        (LaborHours.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
GROUP BY YEAR(LaborHours.WEEK_ENDING_DATE), MONTH(LaborHours.WEEK_ENDING_DATE), GroupLocations.LocationDetailID
ORDER BY jdeyr, jdemonth
</cfquery>
<cfloop query="chartjdehrs">
	<cfif jdeyr eq year(now())>
		<cfset ytdhrs = ytdhrs+jdehrs>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#jdeyr#_#jdemonth#")>
		<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = jdehrs>
	<cfelse>
		<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = allcharthrsstruct["#jdeyr#_#jdemonth#"]+jdehrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif jdeyr eq year(now())>
			<cfset ytdhrs = ytdhrs+jdehrs>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#jdeyr#_#jdemonth#")>
			<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = jdehrs>
		<cfelse>
			<cfset allcharthrsstruct["#jdeyr#_#jdemonth#"] = allcharthrsstruct["#jdeyr#_#jdemonth#"]+jdehrs>
		</cfif>
	</cfif>
</cfloop>
<cfquery name="chartohhrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursOffice.OH_HOURS) AS oh, GroupLocations.LocationDetailID, YEAR(LaborHoursOffice.WEEK_ENDING_DATE) AS ohyr, 
                         MONTH(LaborHoursOffice.WEEK_ENDING_DATE) AS ohmonth
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE        (LaborHoursOffice.WEEK_ENDING_DATE BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
GROUP BY YEAR(LaborHoursOffice.WEEK_ENDING_DATE), MONTH(LaborHoursOffice.WEEK_ENDING_DATE), GroupLocations.LocationDetailID
ORDER BY ohyr, ohmonth
</cfquery>
<cfloop query="chartohhrs">
	<cfif ohyr eq year(now())>
		<cfset ytdhrs = ytdhrs+oh>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#ohyr#_#ohmonth#")>
		<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = oh>
	<cfelse>
		<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = allcharthrsstruct["#ohyr#_#ohmonth#"]+oh>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif ohyr eq year(now())>
			<cfset ytdhrs = ytdhrs+oh>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#ohyr#_#ohmonth#")>
			<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = oh>
		<cfelse>
			<cfset allcharthrsstruct["#ohyr#_#ohmonth#"] = allcharthrsstruct["#ohyr#_#ohmonth#"]+oh>
		</cfif>
	</cfif>
</cfloop>
<cfquery name="getallhrs" datasource="#request.dsn#">
SELECT        SUM(LaborHoursTotal.HomeOfficeHrs) + SUM(LaborHoursTotal.OverheadHrs) + SUM(LaborHoursTotal.FieldHrs) + SUM(LaborHoursTotal.CraftHrs) 
                         + SUM(LaborHoursTotal.SubHrs) + SUM(LaborHoursTotal.JVHours) + SUM(LaborHoursTotal.ManagedContractorHours) AS allhrs, GroupLocations.LocationDetailID, 
                         YEAR(LaborHoursTotal.WeekEndingDate) AS manyr, MONTH(LaborHoursTotal.WeekEndingDate) AS manmonth
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON LaborHoursTotal.LocationID = GroupLocations.GroupLocID
WHERE        (LaborHoursTotal.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
GROUP BY YEAR(LaborHoursTotal.WeekEndingDate), MONTH(LaborHoursTotal.WeekEndingDate), GroupLocations.LocationDetailID
ORDER BY manyr, manmonth
</cfquery>
<cfloop query="getallhrs">
	<cfif manyr eq year(now())>
		<cfset ytdhrs = ytdhrs+allhrs>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#manyr#_#manmonth#")>
		<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allhrs>
	<cfelse>
		<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allcharthrsstruct["#manyr#_#manmonth#"]+allhrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif manyr eq year(now())>
			<cfset ytdhrs = ytdhrs+allhrs>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#manyr#_#manmonth#")>
			<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allhrs>
		<cfelse>
			<cfset allcharthrsstruct["#manyr#_#manmonth#"] = allcharthrsstruct["#manyr#_#manmonth#"]+allhrs>
		</cfif>
	</cfif>
</cfloop>
<cfquery name="getconhrsall" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, YEAR(ContractorHours.WeekEndingDate) AS conyr, 
                         MONTH(ContractorHours.WeekEndingDate) AS conmonth
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE        (ContractorHours.WeekEndingDate BETWEEN <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,1)#"> AND <cfqueryparam cfsqltype="CF_SQL_DATE" value="#listgetat(fullrollstart,listlen(fullrollstart))#">)

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
GROUP BY YEAR(ContractorHours.WeekEndingDate), MONTH(ContractorHours.WeekEndingDate), GroupLocations.LocationDetailID
ORDER BY conyr, conmonth
</cfquery>
<cfloop query="getconhrsall">
	<cfif conyr eq year(now())>
		<cfset ytdhrs = ytdhrs+conhrs>
	</cfif>
	<cfif not structkeyexists(allcharthrsstruct,"#conyr#_#conmonth#")>
		<cfset allcharthrsstruct["#conyr#_#conmonth#"] = conhrs>
	<cfelse>
		<cfset allcharthrsstruct["#conyr#_#conmonth#"] = allcharthrsstruct["#conyr#_#conmonth#"]+conhrs>
	</cfif>
	<cfif LocationDetailID eq 4>
		<cfif conyr eq year(now())>
			<cfset ytdhrs = ytdhrs+conhrs>
		</cfif>
		<cfif not structkeyexists(allcharthrsstruct,"#conyr#_#conmonth#")>
			<cfset allcharthrsstruct["#conyr#_#conmonth#"] = conhrs>
		<cfelse>
			<cfset allcharthrsstruct["#conyr#_#conmonth#"] = allcharthrsstruct["#conyr#_#conmonth#"]+conhrs>
		</cfif>
	</cfif>
</cfloop>


<cfset TRIRratestructc = {}>
<cfset LTIratestructc = {}>
<cfset ALLRratestructc = {}>

<cfloop list="#rollstart#" index="rdate">

	<cfset thisrollwin = dateformat(dateadd("m",-12,rdate),"m/d/yyyy")>
	<cfset rolldate = thisrollwin>
	
	<cfset rollhours = 0>
	<cfset rollincsTRI = 0>
	<cfset rollincsLTI = 0>
	<cfset rollincsALL= 0>
	
	<cfloop from="1" to="12" index="i">
		<cfset rolldate = dateadd("m",1,rolldate)>
		<cfset rolldate = "#month(rolldate)#/#daysinmonth(rolldate)#/#year(rolldate)#">
		<!--- <cfset thisrollwin = listappend(thisrollwin,rolldate)> --->
		<cfif structkeyexists(allcharthrsstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollhours = rollhours + allcharthrsstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif>
		<cfif structkeyexists(totrecchrtstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollincsTRI = rollincsTRI + totrecchrtstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif>
		<cfif structkeyexists(LTIrecchrtstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollincsLTI = rollincsLTI + LTIrecchrtstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif>
		<cfif structkeyexists(allrecchrtstruct,"#year(rolldate)#_#month(rolldate)#")>
			<cfset rollincsALL = rollincsALL + allrecchrtstruct["#year(rolldate)#_#month(rolldate)#"]>
		</cfif> 
		
		
		
		
	</cfloop>
	<!--- <cfoutput>#rdate# hrs (#rollhours#) tr (#rollincsTRI#) #numberformat(((5+rollincsTRI)*200000)/rollhours,"0.000")# lt(#rollincsLTI#)  all(#rollincsALL#)<br></cfoutput> --->
	<cfif rollhours gt 0 and rollincsTRI gt 0>
		<cfset TRIRratestructc["#year(rdate)#_#month(rdate)#"] = numberformat((rollincsTRI*200000)/rollhours,"0.00")>
	<cfelse>
		<cfset TRIRratestructc["#year(rdate)#_#month(rdate)#"] = 0>
	</cfif>
	<cfif rollhours gt 0 and rollincsLTI gt 0>
		<cfset LTIratestructc["#year(rdate)#_#month(rdate)#"] = numberformat((rollincsLTI*200000)/rollhours,"0.000")>
	<cfelse>
		<cfset LTIratestructc["#year(rdate)#_#month(rdate)#"] = 0>
	</cfif>
	<cfif rollhours gt 0 and rollincsALL gt 0>
		<cfset ALLRratestructc["#year(rdate)#_#month(rdate)#"] = numberformat((rollincsALL*200000)/rollhours,"0.00")>
	<cfelse>
		<cfset ALLRratestructc["#year(rdate)#_#month(rdate)#"] = 0>
	</cfif>
	
	

</cfloop>
<!--- <cfdump var="#TRIRratestructc#">
<cfdump var="#LTIratestructc#">
<cfdump var="#ALLRratestructc#"> --->




<cfset chart1xml = "<chart palettecolors='##5f2468' linecolor='##6d8833' linethickness='3' showShadow='0' drawAnchors='0'  captionFontSize='12'  legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1' caption='Total Recordable Incidents' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint' showvalues='0' baseFont='Segoe UI' baseFontColor='##5f2468' showToolTip='0'><categories>">
<cfloop list="#rollstart#" index="cdate">
	<cfset chart1xml = chart1xml  & "<category label='#left(monthasstring(month(cdate)),3)#' />">
</cfloop>
	<cfset chart1xml = chart1xml  & "</categories><dataset seriesname='Monthly Cases' renderas='bar'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(totrecchrtstruct,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = totrecchrtstruct["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart1xml = chart1xml  & "<set value='#cval#' />">
</cfloop>
	<cfset chart1xml = chart1xml  & "</dataset><dataset seriesname='Rolling Frequency' parentyaxis='S' renderas='line' showvalues='0'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(TRIRratestructc,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = TRIRratestructc["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart1xml = chart1xml  & "<set value='#cval#' />">
</cfloop>
	<cfset chart1xml = chart1xml  & "</dataset></chart>">



<cfset chart2xml = "<chart palettecolors='##5f2468' linecolor='##6d8833' linethickness='3' showShadow='0' drawAnchors='0'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1'  caption='All Injury Incidents' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix='' theme='fint' showvalues='0' baseFont='Segoe UI' baseFontColor='##5f2468' showToolTip='0'><categories>">
<cfloop list="#rollstart#" index="cdate">
	<cfset chart2xml = chart2xml  & "<category label='#left(monthasstring(month(cdate)),3)#' />">
</cfloop>
<cfset chart2xml = chart2xml  & "</categories><dataset seriesname='Monthly Cases' renderas='bar'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(allrecchrtstruct,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = allrecchrtstruct["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart2xml = chart2xml  & "<set value='#cval#' />">
</cfloop>
<cfset chart2xml = chart2xml  & "</dataset><dataset seriesname='Rolling Frequency' parentyaxis='S' renderas='line' showvalues='0'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(ALLRratestructc,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = ALLRratestructc["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart2xml = chart2xml  & "<set value='#cval#' />">
</cfloop>
<cfset chart2xml = chart2xml  & "</dataset></chart>">



<cfset chart3xml = "<chart palettecolors='##5f2468' linecolor='##6d8833' linethickness='3' showShadow='0' drawAnchors='0'  captionFontSize='12' legendPosition='bottom' legendItemFontSize='9' legendItemFontBold='1'  caption='Lost Time Injuries' subcaption='' xaxisname='Month' pyaxisname='Monthly Cases' syaxisname='Rolling Frequency' numberprefix='' snumbersuffix=''  theme='fint' showvalues='0' baseFont='Segoe UI' baseFontColor='##5f2468' showToolTip='0'><categories>">
<cfloop list="#rollstart#" index="cdate">
	<cfset chart3xml = chart3xml  & "<category label='#left(monthasstring(month(cdate)),3)#' />">
</cfloop>
<cfset chart3xml = chart3xml  & "</categories><dataset seriesname='Monthly Cases' renderas='bar'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(LTIrecchrtstruct,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = LTIrecchrtstruct["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart3xml = chart3xml  & "<set value='#cval#' />">
</cfloop>
<cfset chart3xml = chart3xml  & "</dataset><dataset seriesname='Rolling Frequency' parentyaxis='S' renderas='line' showvalues='0'>">
<cfloop list="#rollstart#" index="cdate">
	<cfif structkeyexists(LTIratestructc,"#year(cdate)#_#month(cdate)#")>
		<cfset cval = LTIratestructc["#year(cdate)#_#month(cdate)#"]>
	<cfelse>
		<cfset cval = 0>
	</cfif>
	<cfset chart3xml = chart3xml  & "<set value='#cval#' />">
</cfloop>
<cfset chart3xml = chart3xml  & "</dataset></chart>">


<!--- 

y name="getincclsoed" datasource="#request.dsn#">
SELECT        IRN, Status, incidentDate, dateClosed --->

<cfset incstatstruct = {}>

<cfif getincclsoed.recordcount gt 0>
<cfloop query="getincclsoed">
	<cfif trim(dateClosed) neq ''>
		<cfif datediff("d",incidentDate,dateClosed) lte 21>
			<cfif not structkeyexists(incstatstruct,"Closed on Time")>
				<cfset incstatstruct["Closed on Time"] = 1>
			<cfelse>
				<cfset incstatstruct["Closed on Time"] = incstatstruct["Closed on Time"]+1>
			</cfif>
		<cfelse>
			<cfif not structkeyexists(incstatstruct,"Closed Late")>
				<cfset incstatstruct["Closed Late"] = 1>
			<cfelse>
				<cfset incstatstruct["Closed Late"] = incstatstruct["Closed Late"]+1>
			</cfif>
		</cfif>
	<cfelse>
		<cfif datediff("d",incidentDate,now()) lte 21>
			<cfif not structkeyexists(incstatstruct,"On Schedule")>
				<cfset incstatstruct["On Schedule"] = 1>
			<cfelse>
				<cfset incstatstruct["On Schedule"] = incstatstruct["On Schedule"]+1>
			</cfif>
		<cfelse>
			<cfif not structkeyexists(incstatstruct,"Overdue")>
				<cfset incstatstruct["Overdue"] = 1>
			<cfelse>
				<cfset incstatstruct["Overdue"] = incstatstruct["Overdue"]+1>
			</cfif>
			<cfif needirp eq 1>
				<cfif listfindnocase("Approved,Complete",asirpstat) eq 0>
					<cfif not structkeyexists(incstatstruct,"Awaiting IRP")>
						<cfset incstatstruct["Awaiting IRP"] = 1>
					<cfelse>
						<cfset incstatstruct["Awaiting IRP"] = incstatstruct["Awaiting IRP"]+1>
					</cfif>
				
				</cfif>
			</cfif>
		</cfif>
	</cfif>
</cfloop>




<cfset donut1xml="<chart caption='Incident Status YTD' showborder='0' use3dlighting='0' enablesmartlabels='1' startingangle='310' showlabels='0' showpercentvalues='1' showlegend='1' showtooltip='0' decimals='0' usedataplotcolorforlabels='1' theme='fint' labelDistance='2' smartLabelClearance='3' doughnutRadius='56'>">
<cfif listlen(structkeylist(incstatstruct)) gt 0>
	<cfloop list="#structkeylist(incstatstruct)#" index="i">
		<cfset donut1xml=donut1xml & "<set label='#i#' value='#incstatstruct[i]#' />">
	</cfloop>
<cfelse>
	<cfset donut1xml=donut1xml & "<set label='No Incidents Available' value='0' />">
</cfif>
<cfset donut1xml=donut1xml & "</chart>">

<cfelse>

<cfset donut1xml="<chart caption='Incident Status YTD' showborder='0' use3dlighting='0' enablesmartlabels='0' startingangle='310' showlabels='0' showpercentvalues='0' showlegend='1' showtooltip='0' decimals='0' usedataplotcolorforlabels='1' theme='fint' labelDistance='2' smartLabelClearance='3' doughnutRadius='56' showvalues='0'>">

	<cfset donut1xml=donut1xml & "<set label='No Incidents Available' value='1' />">

<cfset donut1xml=donut1xml & "</chart>">

</cfif>


<cfquery name="getdeadlines" datasource="#request.dsn#">
select DLdate
from ManHourDeadLines
where  year(DLdate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#"> and month(DLdate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#month(now())#">
order by DLdate
</cfquery>

 <cfif getdeadlines.recordcount gt 0>
<cfoutput query="getdeadlines">
	<cfif month(now()) eq month(dldate)>
		<cfif now() lt dateadd("ww",1,dldate)>
			<cfset chkmonth = month(dateadd("m",-1,dldate))>
			<cfset chkyear = year(dateadd("m",-1,dldate))>
		<cfelse>
			<cfset chkmonth = month(dldate)>
			<cfset chkyear = year(dldate)>
		</cfif>


	</cfif>
</cfoutput>

<cfelse>
	<cfmodule template="/#getappconfig.oneAIMpath#/shared/weekenddate.cfm" numberweeks="1" startday="Thursday" checkyear="#year(now())#">
	<cfset monthwks = "">
	<cfset chkmonth = month(now())>
	<cfset chkyear = year(now())>
	<cfloop list="#weekendingdates#" index="x">
		<cfif month(x) eq month(now())>
			<cfset monthwks = listappend(monthwks,x)>
		</cfif>
	</cfloop>
	
	<cfif now() lt listgetat(monthwks,2)>
		<cfif month(now()) eq 1>
			<cfset chkmonth = 12>
			<cfset chkyear = year(now())-1>
		<cfelse>
			<cfset chkmonth = chkmonth-1>
		</cfif>
	</cfif>
</cfif>

<cfset gothrslist = "">
<cfquery name="getgrouplistc" datasource="#request.dsn#">
SELECT    distinct   GroupLocations.GroupLocID as  Group_Number
FROM            Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber
WHERE        (Groups.Active_Group = 1) AND (NewDials.status <> 99) AND (NewDials.Parent =
                             (SELECT        ID
                               FROM            NewDials AS NewDials_1
                               WHERE        (Parent = 0))) AND (GroupLocations.entryType IN ('both', 'MHOnly')) and groupLocations.isactive = 1
<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
</cfquery>


<cfquery name="getjdehrsc" datasource="#request.dsn#">
SELECT       distinct  GroupLocations.GroupLocID as group_number
FROM            Groups INNER JOIN
                         ProjectsByGroup ON Groups.Group_Number = ProjectsByGroup.Group_Number INNER JOIN
                         LaborHours ON ProjectsByGroup.Project_Number = LaborHours.PROJECT_NUMBER INNER JOIN
                         GroupLocations ON ProjectsByGroup.LocationID = GroupLocations.GroupLocID
WHERE        YEAR(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(LaborHours.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
</cfquery>
<cfloop query="getjdehrsc">
	<cfif listfind(gothrslist,group_number) eq 0>
		<cfset gothrslist = listappend(gothrslist,group_number)>
	</cfif>
</cfloop>
<cfquery name="getjdeohc" datasource="#request.dsn#">
SELECT       distinct  GroupLocations.GroupLocID as group_number
FROM            OfficeLaborMap INNER JOIN
                         LaborHoursOffice ON OfficeLaborMap.Dept_Home = LaborHoursOffice.Dept_home INNER JOIN
                         Groups ON OfficeLaborMap.Group_Number = Groups.Group_Number INNER JOIN
                         GroupLocations ON OfficeLaborMap.LocationID = GroupLocations.GroupLocID
WHERE        YEAR(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(LaborHoursOffice.WEEK_ENDING_DATE) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
</cfquery>
<cfloop query="getjdeohc">
	<cfif listfind(gothrslist,group_number) eq 0>
		<cfset gothrslist = listappend(gothrslist,group_number)>
	</cfif>
</cfloop>
<cfquery name="getnonjdehrsc" datasource="#request.dsn#">
SELECT DISTINCT LaborHoursTotal.LocationID AS GroupNumber
FROM            LaborHoursTotal INNER JOIN
                         Groups ON LaborHoursTotal.GroupNumber = Groups.Group_Number
WHERE        YEAR(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(LaborHoursTotal.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
</cfquery>
<cfloop query="getnonjdehrsc">
	<cfif listfind(gothrslist,GroupNumber) eq 0>
		<cfset gothrslist = listappend(gothrslist,GroupNumber)>
	</cfif>
</cfloop>
<cfquery name="getcontractorhrsc" datasource="#request.dsn#">
SELECT DISTINCT ContractorHours.LocationID AS GroupNumber
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number
WHERE        YEAR(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkyear#"> and month(ContractorHours.WeekEndingDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#chkmonth#">

<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
</cfquery>
<cfloop query="getcontractorhrsc">
	<cfif listfind(gothrslist,GroupNumber) eq 0>
		<cfset gothrslist = listappend(gothrslist,GroupNumber)>
	</cfif>
</cfloop>

<cfset notent = listlen(valuelist(getgrouplistc.group_number)) - listlen(gothrslist)>



<cfset donut2xml="<chart  caption='Man-Hours Status - #monthasstring(chkmonth)# #chkyear#' showborder='0' use3dlighting='0' enablesmartlabels='1' startingangle='310' showlabels='0' showpercentvalues='1' showlegend='1' showtooltip='0' decimals='0' usedataplotcolorforlabels='1'  labelDistance='2' smartLabelClearance='3'  theme='fint'  doughnutRadius='56'><set label='Man-hours Entered' value='#listlen(gothrslist)#' color='008000'/><set label='Man-hours Outstanding' value='#notent#' color='ff0000'/></chart>">
