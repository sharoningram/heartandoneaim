<cfset groupsallowed = "">
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
	<cfquery name="getgroups" datasource="#request.dsn#">
		select group_number 
		from groups
		where business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="yes">)
	</cfquery>
	<cfloop query="getgroups">
		<cfif listfind(groupsallowed,group_number) eq 0>
			<cfset groupsallowed = listappend(groupsallowed,group_number)>
		</cfif>
	</cfloop>
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0 or listfindnocase(request.userlevel,"User") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0  or listfindnocase(request.userlevel,"HSSE Advisor") gt 0>
	<cfquery name="getgroups" datasource="#request.dsn#">
		select group_number 
		from groups
		where ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="yes">)
	</cfquery>
	<cfloop query="getgroups">
		<cfif listfind(groupsallowed,group_number) eq 0>
			<cfset groupsallowed = listappend(groupsallowed,group_number)>
		</cfif>
	</cfloop>
</cfif>
