<cfparam name="erp" default="">
<cfquery name="getgrouplist" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, Groups.ERPsys, Groups.Business_Line_ID, NewDials.Name
FROM            Groups INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID
WHERE        (Groups.Active_Group = 1) AND (Groups.ERPsys = 'JDE') 
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
	and Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
	and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="Yes">)
</cfif>
</cfif>
ORDER BY NewDials.Name, Groups.Group_Name
</cfquery>