<cfquery name="getmoreinfo" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes_1.IncType, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate, oneAIMincidents.InvestigationLevel, 
                         IncidentTypes.IncType AS sectype
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status in ('More Info Requested')) AND 
                         (IncidentAudits.Status = 'More Info Requested')  and oneAIMincidents.withdrawnto is null
<cfif not showall>
	<cfif listlen(request.userlevel) eq 1 and listfindnocase(request.userlevel,"User") gt 0>
		<cfif getinactusers.recordcount eq 0>
			AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<cfelse>
			AND ((oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) OR (oneAIMincidents.createdbyEmail in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#valuelist(getinactusers.Useremail)#" list="Yes">)) AND groups.group_number IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.groupnumber)#" list="Yes">) )
		</cfif>
	<cfelse>
 		AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
 	</cfif>

<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
	<cfif not admlist>
		<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		
		<cfif  listfindnocase(request.userlevel,"Reviewer") gt 0>
			and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Reviewer') <!--- Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)  --->
			and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer')
			and ((oneAIMincidents.investigationlevel = 2) or (oneAIMincidents.needFA = 1))
		</cfif>
	<cfelse>
		 AND 1 = 2 <!--- (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) --->
	</cfif>
	<cfelse>
	<!--- <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		
	</cfif> ---><!---  or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0 --->
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin') and isfatality = 'No'))
		<cfelse>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
				and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
			and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin') and isfatality = 'No'
		</cfif>
		</cfif>
		</cfif>
		<!--- <cfif  listfindnocase(request.userlevel,"Reviewer") gt 0>
		and  groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Reviewer') 
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)  --->
				and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer')
			and ((oneAIMincidents.investigationlevel = 2) or (oneAIMincidents.needFA = 1))
		</cfif> --->
	<cfelse>
		 AND 1 = 2 <!--- (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) --->
	</cfif>
	</cfif>
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes_1.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.incidentDate
</cfquery>
<!--- <cfquery name="getmoreinfo" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes_1.IncType, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate, oneAIMincidents.InvestigationLevel, 
                         IncidentTypes.IncType AS sectype
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status in ('More Info Requested')) AND 
                         (IncidentAudits.Status = 'More Info Requested')  and oneAIMincidents.withdrawnto is null
<cfif not showall>
	AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) 
<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
	<cfif not admlist>
		<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		
		<cfif  listfindnocase(request.userlevel,"Reviewer") gt 0>
			and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
			and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) 
			and ((oneAIMincidents.investigationlevel = 2) or (oneAIMincidents.needFA = 1))
		</cfif>
	<cfelse>
		 AND 1 = 2 <!--- (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) --->
	</cfif>
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnuml#" list="Yes">) 
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		
			and ((oneAIMincidents.investigationlevel = 2) or (oneAIMincidents.needFA = 1))
		</cfif>
		<!--- <cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0>
			and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
			and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) 
			and ((oneAIMincidents.investigationlevel = 2) or (oneAIMincidents.needFA = 1))
		</cfif> --->
	<cfelse>
		 AND 1 = 2 <!--- (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) --->
	</cfif>
	</cfif>
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes_1.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.incidentDate
</cfquery>
 --->