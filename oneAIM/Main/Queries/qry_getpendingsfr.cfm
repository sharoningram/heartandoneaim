<cfquery name="getpendingsfr" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes_1.IncType AS sectype
FROM            IncidentTypes AS IncidentTypes_1 RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentTypes_1.IncTypeID = oneAIMincidents.SecondaryType LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status in ('started','initiated','withdrawn to initiator')) and oneAIMincidents.withdrawnto is null
<cfif not showall>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
	<cfif listlen(request.userlevel) eq 1 and listfindnocase(request.userlevel,"User") gt 0>
		<cfif getinactusers.recordcount eq 0>
			AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<cfelse>
			AND ((oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) OR (oneAIMincidents.createdbyEmail in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#valuelist(getinactusers.Useremail)#" list="Yes">)) AND groups.group_number IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.groupnumber)#" list="Yes">) )
		</cfif>
	<cfelse>
 		AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
 	</cfif>
<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
	<cfif not admlist>
		and 1 = 2
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin') and isfatality = 'No'))
		<cfelse>
			<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
				and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			</cfif>
			<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) and isfatality = 'No' --->
				 and   groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin') and isfatality = 'No' 
			</cfif>
		</cfif>
		</cfif>
	<cfelse>
		 AND 1 = 2<!---  (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		 <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 or  listfindnocase(request.userlevel,"BU Admin") eq 0>
			and isfatality = 'No'
		</cfif> --->
	</cfif>
	</cfif>
</cfif>
ORDER BY oneAIMincidents.isFatality, oneAIMincidents.TrackingNum
</cfquery>
<!--- <cfquery name="getpendingsfr" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes_1.IncType AS sectype
FROM            IncidentTypes AS IncidentTypes_1 RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentTypes_1.IncTypeID = oneAIMincidents.SecondaryType LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status in ('started','initiated','withdrawn to initiator')) and oneAIMincidents.withdrawnto is null
<cfif not showall>
 AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
 	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and  listfindnocase(request.userlevel,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>
	<cfif not admlist>
		and 1 = 2
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- <cfif request.userlevel eq "BU Admin"> --->
				and oneAIMincidents.groupnumber in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnuml#" list="Yes">) 
			<!--- <cfelseif request.userlevel eq "OU Admin">
				and oneAIMincidents.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnuml#" list="Yes">) and isfatality = 'No'
			<cfelse>
				and (Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnuml#" list="Yes">) 
			</cfif> --->
		</cfif>
		<!--- <cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) and isfatality = 'No'
		</cfif> --->
	<cfelse>
		 AND 1 = 2<!---  (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		 <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 or  listfindnocase(request.userlevel,"BU Admin") eq 0>
			and isfatality = 'No'
		</cfif> --->
	</cfif>
	</cfif>
</cfif>
ORDER BY oneAIMincidents.isFatality, oneAIMincidents.TrackingNum
</cfquery> --->