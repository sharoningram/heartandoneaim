<cfquery name="needLTIdate" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, oneAIMincidents.PotentialRating, Groups.Group_Name, NewDials.Name AS ouname, 
                         oneAIMInjuryOI.LTIFirstDate, oneAIMInjuryOI.LTIDateReturned, oneAIMincidents.incidentDate, oneAIMincidents.ShortDesc
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID
WHERE   oneAIMincidents.status <> 'Cancel'  and oneAIMincidents.withdrawnto is null and   (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMInjuryOI.LTIDateReturned IS NULL) and oneAIMInjuryOI.LTIFirstDate is not null AND
                       ((oneAIMincidents.PrimaryType IN (1, 5))  or   (oneAIMincidents.SecondaryType IN (1, 5)))
<cfif not showall>
	<cfif listlen(request.userlevel) eq 1 and listfindnocase(request.userlevel,"User") gt 0>
		<cfif getinactusers.recordcount eq 0>
			AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<cfelse>
			AND ((oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) OR (oneAIMincidents.createdbyEmail in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#valuelist(getinactusers.Useremail)#" list="Yes">)) AND groups.group_number IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.groupnumber)#" list="Yes">) )
		</cfif>
	<cfelse>
 		AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
 	</cfif>
<cfelse>
	<cfif not admlist>
	and 1 = 2
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0> 
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')))
		<cfelse>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
			and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
			and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
		</cfif>
		</cfif>
		</cfif>
	<cfelse>
		AND 1 = 2 <!---  (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)  --->
	</cfif>
	</cfif>
</cfif>
ORDER BY oneAIMincidents.TrackingNum
</cfquery>