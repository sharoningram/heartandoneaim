<cfparam name="userqrybus" default="#valuelist(getuserBUs.id)#">
<cfif isdefined("openbu") and trim(openbu) neq ''>
<cfif listfindnocase(openbu,"All") eq 0>
	<cfset userqrybus = openbu>
</cfif>
</cfif>

<cfquery name="getuserOU" datasource="#request.dsn#">
SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS buname, NewDials_1.ID AS qrybuid
					FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.Parent = NewDials_1.ID
		WHERE        (NewDials.Parent in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userqrybus#" list="Yes">)) AND (NewDials.status <> 99) and NewDials.isgroupdial = 0
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
			
		<cfif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0>
			and NewDials.id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"User") gt 0 and listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"OU Admin") eq 0 and listfindnocase(request.userlevel,"BU Admin") eq 0 and listfindnocase(request.userlevel,"Reviewer") eq 0 and listfindnocase(request.userlevel,"OU View Only") eq 0>
		and NewDials.id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) 
		</cfif>
		</cfif>
ORDER BY buname,NewDials.Name
</cfquery>

