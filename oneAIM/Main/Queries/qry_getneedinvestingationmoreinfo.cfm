<cfquery name="needinvestingationmoreinfo" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         IncidentInvestigation.Status, MAX(IncidentAudits.CompletedDate) AS compdate, IncidentInvestigation.dateentered AS datecreated, 
                         oneAIMincidents.InvestigationLevel, IncidentTypes.IncType AS sectype
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.isWorkRelated = 'yes')  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 'Review Completed'))
                         AND (IncidentAudits.Status = 'Investigation Sent for More Information')
<cfif not showall>
	 
	 <cfif listfindnocase(request.userlevel,"Reviewer") gt 0>
	 AND ((IncidentInvestigation.Status = 'More Info Requested')  AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
	 or (IncidentInvestigation.Status = 'Senior More Info Requested') AND (oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) and oneAIMincidents.InvestigationLevel = 2) 
	 <cfelse>
	 	<cfif listlen(request.userlevel) eq 1 and listfindnocase(request.userlevel,"User") gt 0>
		<cfif getinactusers.recordcount eq 0>
			 AND (IncidentInvestigation.Status = 'More Info Requested') AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<cfelse>
			 AND (IncidentInvestigation.Status = 'More Info Requested') AND ((oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) OR (oneAIMincidents.createdbyEmail in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#valuelist(getinactusers.Useremail)#" list="Yes">)) AND groups.group_number IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.groupnumber)#" list="Yes">) )
		</cfif>
	<cfelse>
 		 AND (IncidentInvestigation.Status = 'More Info Requested') AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
 	</cfif>
	
	 </cfif>
<cfelse>
	<cfif not admlist>
		<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
		<cfif   listfindnocase(request.userlevel,"Reviewer") gt 0>
		 AND ((IncidentInvestigation.Status = 'More Info Requested') 	AND (oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) and oneAIMincidents.InvestigationLevel = 1
		or (IncidentInvestigation.Status = 'Senior More Info Requested') AND (oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) and oneAIMincidents.InvestigationLevel = 2) 
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">)  --->
		 AND (IncidentInvestigation.Status = 'More Info Requested') 	and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer') and oneAIMincidents.InvestigationLevel = 2
		</cfif>
		<cfelse>
		 AND (IncidentInvestigation.Status = 'More Info Requested') and 1 = 2
		</cfif>
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		 AND ((IncidentInvestigation.Status = 'More Info Requested') and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin') or
(IncidentInvestigation.Status = 'Senior More Info Requested') and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
)
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin'))))
		<cfelse>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
		 AND ((IncidentInvestigation.Status = 'More Info Requested') 	and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin') OR (IncidentInvestigation.Status = 'Senior More Info Requested') 	and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin') 

)
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
		 AND ((IncidentInvestigation.Status = 'More Info Requested') 	and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin') or (IncidentInvestigation.Status = 'Senior More Info Requested') 	and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin') )
		</cfif>
		</cfif>
		</cfif>
	<cfelse>
		 AND (IncidentInvestigation.Status = 'More Info Requested')  AND 1 = 2 <!--- (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) --->
	</cfif>
	</cfif>
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         IncidentInvestigation.Status, IncidentInvestigation.dateentered, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.TrackingNum

</cfquery>