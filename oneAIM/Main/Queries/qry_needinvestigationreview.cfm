<cfquery name="needinvestingationreview" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         IncidentInvestigation.Status, IncidentInvestigation.dateentered AS datecreated, MAX(IncidentAudits.CompletedDate) AS compdate, 
                         oneAIMincidents.InvestigationLevel, IncidentTypes.IncType AS sectype
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE      (oneAIMincidents.isWorkRelated = 'yes')  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed'))  AND (IncidentAudits.Status = 'Investigation Sent for Review')
<cfif not showall>  
	<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0> 
		<cfif listfindnocase(request.userlevel,"Reviewer") gt 0 and listfindnocase(request.userlevel,"Senior Reviewer") eq 0>
			AND (IncidentInvestigation.Status = 'Sent for Review') AND (oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) and oneAIMincidents.InvestigationLevel in (1,2)
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0 and listfindnocase(request.userlevel,"Reviewer") eq 0>
			<!--- and (Groups.business_line_id IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">))  ---> 
			AND (IncidentInvestigation.Status = 'Sent for Senior Review') and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer') and oneAIMincidents.InvestigationLevel = 2
		</cfif>
		<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0 and listfindnocase(request.userlevel,"Reviewer") gt 0>
			AND ((oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) and oneAIMincidents.InvestigationLevel in (1,2) AND (IncidentInvestigation.Status = 'Sent for Review')
			or
			 groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer') and oneAIMincidents.InvestigationLevel = 2 AND (IncidentInvestigation.Status = 'Sent for Senior Review'))
		
		</cfif>
		<!--- <cfif listfindnocase(request.userlevel,"User") gt 0>
			AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		</cfif> --->
	<cfelse>
		and 1 = 0
	</cfif>
<cfelse>
	<cfif not admlist>
		<!--- <cfif  listfindnocase(request.userlevel,"User") gt 0> --->
			
			 AND ((oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) and oneAIMincidents.InvestigationLevel in (1,2) AND (IncidentInvestigation.Status = 'Sent for Review')
			or
			 groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'Senior Reviewer') and oneAIMincidents.InvestigationLevel = 2 AND (IncidentInvestigation.Status = 'Sent for Senior Review'))
		<!--- <cfelse>
		And 1 = 2
		</cfif> --->
	<cfelse>

	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0> 
				<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
				and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
					or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')))
					
					and ( oneAIMincidents.InvestigationLevel in (1,2) AND (IncidentInvestigation.Status = 'Sent for Review') or oneAIMincidents.InvestigationLevel = 2 AND (IncidentInvestigation.Status = 'Sent for Senior Review'))
				<cfelse>
						<cfif listfindnocase(request.userlevel,"OU Admin") gt 0>
							<!--- and  (Groups.OUid IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="Yes">)) --->
							and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
							and ( oneAIMincidents.InvestigationLevel in (1,2) AND (IncidentInvestigation.Status = 'Sent for Review') or oneAIMincidents.InvestigationLevel = 2 AND (IncidentInvestigation.Status = 'Sent for Senior Review'))
						</cfif>
						<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
							<!--- and (Groups.business_line_id IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">)) --->
							and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
							and ( oneAIMincidents.InvestigationLevel in (1,2) AND (IncidentInvestigation.Status = 'Sent for Review') or oneAIMincidents.InvestigationLevel = 2 AND (IncidentInvestigation.Status = 'Sent for Senior Review'))
						</cfif>
				</cfif>
		<cfelse>
				and ( oneAIMincidents.InvestigationLevel in (1,2) AND (IncidentInvestigation.Status = 'Sent for Review') or oneAIMincidents.InvestigationLevel = 2 AND (IncidentInvestigation.Status = 'Sent for Senior Review'))
		</cfif>
	<cfelse>
		and 1 = 0
	</cfif>
	</cfif>
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         IncidentInvestigation.Status, IncidentInvestigation.dateentered, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.TrackingNum
</cfquery>