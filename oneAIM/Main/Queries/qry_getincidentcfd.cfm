<cfquery name="getcfdincs" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, 
                         YEAR(oneAIMincidents.incidentDate) AS incyr, MONTH(oneAIMincidents.incidentDate) AS incmonth, Groups.OUid, Groups.Group_Number
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    oneAIMincidents.isnearmiss = 'No' 

AND (oneAIMincidents.IncAssignedTo IN (1, 2, 3, 4, 11)) and oneAIMincidents.isWorkRelated = 'yes' and   (oneAIMincidents.Status <> 'Cancel')  AND 
                         (YEAR(oneAIMincidents.incidentDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">)) AND 
					 	 ((oneAIMincidents.PrimaryType = 1) AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4))
						 OR
                         (oneAIMincidents.PrimaryType = 5) AND OIdefinition = 1 AND (oneAIMInjuryOI.OSHAclass IN (1, 2, 3, 4)))

GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, 
                         oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating, Groups.OUid, Groups.Group_Number
ORDER BY incyr, incmonth, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, oneAIMincidents.IncAssignedTo, oneAIMInjuryOI.OSHAclass, oneAIMincidents.PotentialRating

</cfquery>
<cfquery name="getfatalities" datasource="#request.dsn#">
SELECT        COUNT(oneAIMincidents.IRN) AS inccnt, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, YEAR(oneAIMincidents.incidentDate) AS fatyr, 
                         MONTH(oneAIMincidents.incidentDate) AS fatmonth, Groups.OUid, Groups.Group_Number
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID
WHERE        (oneAIMincidents.Status <> 'Cancel')  AND (oneAIMincidents.IncAssignedTo IN (1, 2, 3, 4, 11)) AND (YEAR(oneAIMincidents.incidentDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">)) AND (oneAIMincidents.isFatality = 'yes') AND isWorkRelated = 'yes'
<!--- <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)
</cfif> --->
GROUP BY YEAR(oneAIMincidents.incidentDate), MONTH(oneAIMincidents.incidentDate), Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, Groups.Group_Number
ORDER BY fatyr, fatmonth, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, oneAIMincidents.IncAssignedTo
</cfquery>