
<cfquery name="getfatsentforreview" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, Groups.OUid, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    oneAIMincidents.isfatality = 'Yes'  and oneAIMincidents.withdrawnto is null and    (oneAIMincidents.Status IN ('sent for review')) AND  (IncidentAudits.Status = 'Sent for Review')
<cfif not showall>
<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
 AND (Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">))
<cfelse>
	AND 1 = 2
</cfif>
<Cfelse>
	<cfif not admlist>
	AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) 
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
		</cfif>
	<cfelse>
		<cfif  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
 			AND 1 = 2 <!--- (Groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userBUs#" list="Yes">)) --->
		<cfelse>
			AND 1 = 2
		</cfif>
	</cfif>
	</cfif>
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, Groups.OUid, oneAIMincidents.dateCreated
ORDER BY oneAIMincidents.TrackingNum
</cfquery>
