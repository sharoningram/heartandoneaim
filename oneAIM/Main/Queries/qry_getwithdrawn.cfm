<cfquery name="getwithdrawn" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes_1.IncType AS sectype, oneAIMincidents.withdrawnto, 
                         oneAIMincidents.withdrawndate
FROM            IncidentTypes AS IncidentTypes_1 RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentTypes_1.IncTypeID = oneAIMincidents.SecondaryType LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE     1 = 1
<cfif not showall>
	<cfif listlen(request.userlevel) eq 1 and listfindnocase(request.userlevel,"User") gt 0>
		<cfif getinactusers.recordcount eq 0>
			 and (oneAIMincidents.withdrawnto = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">)
		<cfelse>
			AND ((oneAIMincidents.withdrawnto = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">) OR (oneAIMincidents.withdrawnto in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getinactusers.userid)#" list="Yes">)) AND groups.group_number IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.groupnumber)#" list="Yes">) )
		</cfif>
	<cfelse>
 		 and (oneAIMincidents.withdrawnto = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">)
 	</cfif>
	
  
<cfelse>
	<cfif not admlist>
   and (oneAIMincidents.withdrawnto != <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">)  AND (oneAIMincidents.returnTo = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)  
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0> 
	and oneAIMincidents.withdrawnto is not null  and oneAIMincidents.withdrawnto > 0
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')))
		<cfelse>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
		<!--- 	   and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
		<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
		</cfif>
		</cfif>
		</cfif>
	<cfelse>
		AND 1 = 2 
	</cfif>
	</cfif>
</cfif>
ORDER BY oneAIMincidents.TrackingNum
</cfquery>