<cfquery name="getincclsoed" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.Status, oneAIMincidents.incidentDate, oneAIMincidents.dateClosed, oneAIMincidents.needIRP, 
                         oneAIMIncidentIRP.Status AS asirpstat
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN
WHERE        (oneAIMincidents.Status <> 'cancel') and year(incidentDate) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">


<cfif listfindnocase(openbu,"all") eq 0 and trim(openbu) neq ''>
	and groups.business_line_id in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openbu#" list="Yes">)
</cfif>
<cfif trim(openoulist) neq ''>
	and groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#openoulist#" list="Yes">)
</cfif>
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
	and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)	
</cfif>
ORDER BY oneAIMincidents.Status

</cfquery>