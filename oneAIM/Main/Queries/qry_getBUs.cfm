<cfquery name="getBUs" datasource="#request.dsn#">
<!--- SELECT        ID, Name
FROM            NewDials
WHERE        (Parent = 2707) AND (status <> 99)

ORDER BY Name --->
SELECT        NewDials.ID, NewDials.Name, NewDials_1.Name AS ouname, NewDials_1.ID AS ouid, Groups.Group_Number, Groups.Group_Name
FROM            NewDials INNER JOIN
                         NewDials AS NewDials_1 ON NewDials.ID = NewDials_1.Parent INNER JOIN
                         Groups ON NewDials.ID = Groups.Business_Line_ID AND NewDials_1.ID = Groups.OUid
WHERE        (NewDials.Parent = 2707) AND (NewDials.status <> 99) AND (NewDials_1.status <> 99) AND (Groups.Active_Group = 1)
<!--- <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)
</cfif> --->
ORDER BY NewDials.Name, ouname, Groups.Group_Name
</cfquery>