<cfquery name="getuserBUs" datasource="#request.dsn#">
SELECT DISTINCT NewDials.ID, NewDials.Name
FROM            NewDials INNER JOIN
                         Groups ON NewDials.ID = Groups.Business_Line_ID
<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
Where groups.active_group = 1  and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)
<cfelse>
where groups.active_group = 1
</cfif>
order by name
</cfquery>