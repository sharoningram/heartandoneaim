
<cfquery name="getneedfirstalertreview" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         oneAimFirstAlerts.dateentered, MAX(IncidentAudits.CompletedDate) AS compdate, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType AS sectype
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.needFA = 1)  and  oneAimFirstAlerts.Status = 'Sent for Review'  and oneAIMincidents.withdrawnto is null AND (IncidentAudits.Status = 'First Alert Sent for Review')
<cfif not showall>
	<cfif listfindnocase(request.userlevel,"Reviewer") gt 0>
		AND   (oneAIMincidents.revieweremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<!--- and   (Groups.OUid IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userOUs#" list="Yes">)) --->
	<!--- <cfelseif listfindnocase(request.userlevel,"User") gt 0>
		AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) ---> 
	<cfelse>
		and 1 = 2
	</cfif>
<cfelse>
	<cfif not admlist>
		<!--- <cfif listfindnocase(request.userlevel,"User") gt 0> --->
			AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<!--- <cfelse>
			and 1 = 2
		</cfif> --->
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')))
		<cfelse>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
			and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
			and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
		</cfif>
		</cfif>
		</cfif>
	<cfelse>
		
			and  1 = 2
		
	</cfif>
	</cfif>
</cfif>
GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         oneAimFirstAlerts.dateentered, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.TrackingNum
</cfquery>
