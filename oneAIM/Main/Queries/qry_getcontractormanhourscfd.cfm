
<cfquery name="getcontractorhrs" datasource="#request.dsn#">
SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Contractors.CoType, Groups.Business_Line_ID, 
                         YEAR(ContractorHours.WeekEndingDate) AS conyr, MONTH(ContractorHours.WeekEndingDate) AS conmonth, Groups.OUid, Groups.Group_Number
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE        (YEAR(ContractorHours.WeekEndingDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
<!--- <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)
</cfif> --->
GROUP BY YEAR(ContractorHours.WeekEndingDate), MONTH(ContractorHours.WeekEndingDate), Contractors.CoType, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number
ORDER BY conyr, conmonth, Contractors.CoType, Groups.OUid, Groups.Group_Number, GroupLocations.LocationDetailID
<!--- SELECT        SUM(ContractorHours.ContractorHrs) AS conhrs, GroupLocations.LocationDetailID, Contractors.CoType, Groups.Business_Line_ID, 
                         YEAR(ContractorHours.WeekEndingDate) AS conyr, MONTH(ContractorHours.WeekEndingDate) AS conmonth
FROM            ContractorHours INNER JOIN
                         Groups ON ContractorHours.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON ContractorHours.LocationID = GroupLocations.GroupLocID INNER JOIN
                         Contractors ON ContractorHours.ContractorID = Contractors.ContractorID
WHERE        (YEAR(ContractorHours.WeekEndingDate) IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#" list="Yes">))
GROUP BY YEAR(ContractorHours.WeekEndingDate), MONTH(ContractorHours.WeekEndingDate), Contractors.CoType, GroupLocations.LocationDetailID, 
                         Groups.Business_Line_ID
ORDER BY conyr, conmonth, Contractors.CoType, GroupLocations.LocationDetailID --->

</cfquery>
