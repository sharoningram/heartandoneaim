<cfquery name="getcapaoverdue" datasource="#request.dsn#">
SELECT        oneAIMCAPA.CAPAid, oneAIMCAPA.CAPAtype, NewDials.Name AS ouname, oneAIMCAPA.DueDate, oneAIMCAPA.AssignedTo, oneAIMCAPA.ActionDetail, 
                         oneAIMCAPA.EnteredBy, oneAIMCAPA.DateComplete, oneAIMincidents.IRN, Groups.Group_Name,oneAIMincidents.TrackingNum,oneAIMCAPA.DateEntered as capadate
FROM            oneAIMCAPA INNER JOIN
                         oneAIMincidents ON oneAIMCAPA.IRN = oneAIMincidents.IRN INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID
WHERE    oneAIMCAPA.status = 'open' and   (oneAIMCAPA.DueDate < <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) AND (oneAIMCAPA.DateComplete IS NULL)  and oneAIMincidents.withdrawnto is null
<cfif not showall>
	<cfif listlen(request.userlevel) eq 1 and listfindnocase(request.userlevel,"User") gt 0>
		<cfif getinactusers.recordcount eq 0>
			AND (oneAIMCAPA.EnteredBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
		<cfelse>
			AND ((oneAIMCAPA.EnteredBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) OR (oneAIMCAPA.EnteredBy in (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#valuelist(getinactusers.Useremail)#" list="Yes">)) AND groups.group_number IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.groupnumber)#" list="Yes">) )
		</cfif>
	<cfelse>
 		AND (oneAIMCAPA.EnteredBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">)
 	</cfif>
<cfelse>
	<cfif not admlist>
	and 1 = 2
	<cfelse>
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0> 
		<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>
	<cfif listfindnocase(request.userlevel,"BU Admin") gt 0 and  listfindnocase(request.userlevel,"OU Admin") gt 0>
		and (groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
			or (groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')))
		<cfelse>
		<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>
			<!--- and Groups.business_Line_ID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userbus#" list="Yes">) --->
			and groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
		</cfif>
		<cfif  listfindnocase(request.userlevel,"OU Admin") gt 0>
			<!--- and Groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">) --->
			and groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
		</cfif>
		</cfif>
		</cfif>
	<cfelse>
		AND  1 = 2 <!---  (oneAIMCAPA.EnteredBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">) --->
	</cfif>
	</cfif>
</cfif>
ORDER BY oneAIMCAPA.CAPAtype
</cfquery>