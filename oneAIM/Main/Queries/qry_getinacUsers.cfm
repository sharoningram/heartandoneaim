<cfquery name="getusergroups" datasource="#request.dsn#">
SELECT        GroupNumber
FROM            GroupUserAssignment
WHERE        (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#">) AND (GroupRole = 'dataentry')
</cfquery>

<cfquery name="getinactusers" datasource="#request.dsn#">
SELECT        Users.Useremail, Users.UserID
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID INNER JOIN
                         GroupUserAssignment ON Users.UserId = GroupUserAssignment.UserID
WHERE        (UserRoles.UserRole = 'User') AND (UserRoles.AssignedLocs IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userous#" list="Yes">)) AND (Users.Status = 0) AND (GroupUserAssignment.GroupNumber IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#valuelist(getusergroups.GroupNumber)#" list="Yes">)) AND 
                         (GroupUserAssignment.GroupRole = 'dataentry')

</cfquery>