<cfquery name="getlastlti" datasource="#request.dsn#">
SELECT        MAX(oneAIMincidents.incidentDate) AS ltidate, Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, Groups.Group_Number
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType IN (1, 5)) AND (oneAIMInjuryOI.OSHAclass = 2) AND (oneAIMincidents.IncAssignedTo IN (1, 2,
                          3, 4, 11)) and oneAIMincidents.isworkrelated = 'yes'
<!--- <cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
and groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#groupsallowed#" list="Yes">)
</cfif> --->
GROUP BY Groups.Business_Line_ID, oneAIMincidents.IncAssignedTo, Groups.OUid, Groups.Group_Number
ORDER BY ltidate desc, Groups.Business_Line_ID, Groups.OUid, Groups.Group_Number, oneAIMincidents.IncAssignedTo
</cfquery>
<!--- <cfquery name="getgloballastlti" datasource="#request.dsn#">
SELECT        MAX(oneAIMincidents.incidentDate) AS ltidate, oneAIMincidents.IncAssignedTo
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE        (oneAIMincidents.Status <> 'Cancel') AND (oneAIMincidents.PrimaryType IN (1, 5)) AND (oneAIMInjuryOI.OSHAclass = 2)
GROUP BY oneAIMincidents.IncAssignedTo
ORDER BY ltidate DESC, oneAIMincidents.IncAssignedTo
</cfquery> --->