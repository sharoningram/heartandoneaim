


<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="main">
	<cfset attributes.xfa.incidentreport = "incidents.incidentreport">
	<cfset attributes.xfa.IRP = "incidents.IRP">
	<cfset attributes.xfa.firstalert = "incidents.firstalert">
	<cfset attributes.xfa.capa = "incidents.capa">
	<cfset attributes.xfa.investigation = "incidents.investigation">
	<cfset attributes.xfa.fatalityreport = "incidents.fatalityreport">
	<cfset attributes.xfa.printconfigdash= "reports.printconfigdash">
	<cfset attributes.xfa.exportconfigdash= "reports.exportconfigdash">
	<cfset attributes.xfa.createsublistsmulti= "main.createsublistsmulti">
	<cfinclude template="queries\qry_getinacUsers.cfm">
	<cfparam name="admlist" default="no">
	<cfparam name="openbu" default="">
	<cfparam name="openou" default="">
	<cfset openoulist = "">
	<cfif trim(openou) neq '' and listfindnocase(openou,"All") eq 0>
		<cfloop list="#openou#" index="i">
			<cfif i contains "_">
				<cfset openoulist = listappend(openoulist,listgetat(i,2,"_"))>
			</cfif>
		</cfloop>
	</cfif>
	
	
	<table width="100%" cellpadding="3" cellspacing="0" border="0" id="mytab">
		<tr>
			<td width="15%" valign="top">
				<cfinclude template="displays/dsp_mainpage.cfm">
			</td>
			<td valign="top" width="85%">
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"User") gt 0>
				<cfparam name="toggleview" default="tasks">
			<cfelse>
				<cfset toggleview = "dashboard">
			</cfif>
				<cfif toggleview eq "tasks">
					
					<cfparam name="showall" default="no">
					<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"User") gt 0>
					<cfif admlist eq "no">
					<cfinclude template="queries/qry_getpendingsfr.cfm">
					<cfinclude template="queries/qry_getmoreinfo.cfm">
					<cfinclude template="queries/qry_getsentforreview.cfm">
					<cfinclude template="queries/qry_getfatsentforreview.cfm">
					<cfinclude template="queries/qry_getneedfirstalert.cfm">
					<cfinclude template="queries/qry_getneedfirstalertreview.cfm">
					<cfinclude template="queries/qry_getmoreinfoFA.cfm">
					<cfinclude template="queries/qry_getneedfirstalertSRreview.cfm">
					<cfinclude template="queries/qry_capaincomplete.cfm">
					<cfinclude template="queries/qry_capaoverdue.cfm">
					<cfinclude template="queries/qry_needinvestigation.cfm">
					<cfinclude template="queries/qry_needinvestigationreview.cfm">
					<cfinclude template="queries/qry_getneedinvestingationmoreinfo.cfm">
					<cfinclude template="queries/qry_needIRP.cfm">
					<cfinclude template="queries/qry_needIRPmoreinfo.cfm">
					<cfinclude template="queries/qry_needIRPreview.cfm">
					<cfinclude template="queries/qry_getwithdrawn.cfm">
					<cfinclude template="queries/qry_needLTIdate.cfm">
					<cfinclude template="queries/qry_needRWCdate.cfm">
					<!--- <cfinclude template="queries/qry_getNeedJDEassign.cfm">
					<cfinclude template="queries/qry_getNeedJDEassigndepts.cfm"> --->
					<cfinclude template="displays/dsp_mytasks.cfm">
					
					<cfset showall = "yes">
					
					<cfinclude template="queries/qry_getpendingsfr.cfm">
					<cfinclude template="queries/qry_getmoreinfo.cfm">
					<cfinclude template="queries/qry_getsentforreview.cfm">
					<cfinclude template="queries/qry_getfatsentforreview.cfm">
					<cfinclude template="queries/qry_getneedfirstalert.cfm">
					<cfinclude template="queries/qry_getneedfirstalertreview.cfm">
					<cfinclude template="queries/qry_getmoreinfoFA.cfm">
					<cfinclude template="queries/qry_getneedfirstalertSRreview.cfm">
					<cfinclude template="queries/qry_capaincomplete.cfm">
					<cfinclude template="queries/qry_capaoverdue.cfm">
					<cfinclude template="queries/qry_needinvestigation.cfm">
					<cfinclude template="queries/qry_needinvestigationreview.cfm">
					<cfinclude template="queries/qry_getneedinvestingationmoreinfo.cfm">
					<cfinclude template="queries/qry_needIRP.cfm">
					<cfinclude template="queries/qry_needIRPmoreinfo.cfm">
					<cfinclude template="queries/qry_needIRPreview.cfm">
					<cfinclude template="queries/qry_needLTIdate.cfm">
					<cfinclude template="queries/qry_getwithdrawn.cfm">
					<cfinclude template="queries/qry_needRWCdate.cfm">
					<cfinclude template="queries/qry_pendingclosure.cfm">
					<cfinclude template="displays/dsp_mywatchlist.cfm">
					
					<cfelse>
						<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>
						<!--- <cfinclude template="actions/act_getusergrouplist.cfm"> --->
					<cfinclude template="queries/qry_getpendingsfr.cfm">
					<cfinclude template="queries/qry_getmoreinfo.cfm">
					<cfinclude template="queries/qry_getsentforreview.cfm">
					<cfinclude template="queries/qry_getfatsentforreview.cfm">
					<cfinclude template="queries/qry_getneedfirstalert.cfm">
					<cfinclude template="queries/qry_getneedfirstalertreview.cfm">
					<cfinclude template="queries/qry_getmoreinfoFA.cfm">
					<cfinclude template="queries/qry_getneedfirstalertSRreview.cfm">
					<cfinclude template="queries/qry_capaincomplete.cfm">
					<cfinclude template="queries/qry_capaoverdue.cfm">
					<cfinclude template="queries/qry_needinvestigation.cfm">
					<cfinclude template="queries/qry_needinvestigationreview.cfm">
					<cfinclude template="queries/qry_getneedinvestingationmoreinfo.cfm">
					<cfinclude template="queries/qry_needIRP.cfm">
					<cfinclude template="queries/qry_needIRPmoreinfo.cfm">
					<cfinclude template="queries/qry_needIRPreview.cfm">
					<cfinclude template="queries/qry_needLTIdate.cfm">
					<cfinclude template="queries/qry_getwithdrawn.cfm">
					<cfinclude template="queries/qry_needRWCdate.cfm">
					<cfinclude template="queries/qry_pendingclosure.cfm">
						<cfinclude template="displays/dsp_alltasks.cfm">
						</cfif>
					</cfif>
					<cfelse>
						<cfinclude template="actions/act_buildusergroups.cfm">
						<cfinclude template="queries/qry_getBUs.cfm">
						<cfinclude template="queries/qry_incassignedto.cfm">
						<cfinclude template="queries/qry_getjdemanhourscfd.cfm">
						<cfinclude template="queries/qry_getmanualmanhourscfd.cfm">
						<cfinclude template="queries/qry_getcontractormanhourscfd.cfm">
						<cfinclude template="queries/qry_getincidentcfd.cfm">
						<cfinclude template="queries/qry_getlastLTI.cfm">
						<cfinclude template="actions/act_builddashstructs.cfm">
						<cfinclude template="queries/qry_getincclosed.cfm">
						<cfinclude template="actions/act_buildchartxml.cfm">
						<cfinclude template="queries/qry_gettargets.cfm">
						<cfinclude template="queries/qry_getuserbus.cfm">
						<cfinclude template="queries/qry_getuserOU.cfm">
						<cfinclude template="displays/dsp_dashfilters.cfm">
						<cfinclude template="displays/dsp_dashboardcharts.cfm">
						<cfinclude template="displays/dsp_dashboard.cfm">
					</cfif>
				<cfelse>
					<cfinclude template="actions/act_buildusergroups.cfm">
					<cfinclude template="queries/qry_getBUs.cfm">
					<cfinclude template="queries/qry_incassignedto.cfm">
					<cfinclude template="queries/qry_getjdemanhourscfd.cfm">
					<cfinclude template="queries/qry_getmanualmanhourscfd.cfm">
					<cfinclude template="queries/qry_getcontractormanhourscfd.cfm">
					<cfinclude template="queries/qry_getincidentcfd.cfm">
					<cfinclude template="queries/qry_getlastLTI.cfm">
					<cfinclude template="actions/act_builddashstructs.cfm">
					<cfinclude template="queries/qry_getincclosed.cfm">
					<cfinclude template="actions/act_buildchartxml.cfm">
					<cfinclude template="queries/qry_gettargets.cfm">
					<cfinclude template="queries/qry_getuserbus.cfm">
					<cfinclude template="queries/qry_getuserOU.cfm">
					<cfinclude template="displays/dsp_dashfilters.cfm">
					<cfinclude template="displays/dsp_dashboardcharts.cfm">
					<cfinclude template="displays/dsp_dashboard.cfm">
				
				</cfif>
				
			</td>
		</tr>
	</table>
</cfcase>
<cfcase value="assignjdedepts">
	<cfset attributes.xfa.assignjdedepts = "main.assignjdedepts">
	<cfset attributes.xfa.managejdeassign = "main.managejdeassign">
	<cfset attributes.xfa.popgroupinfo = "main.popgroupinfo">
	<cfinclude template="queries/qry_getgrouplist.cfm">
	<cfinclude template="queries/qry_getneededepts.cfm">
	<cfinclude template="queries/qry_getapplieddepts.cfm">
	<cfinclude template="displays/frm_assigndepts.cfm">
</cfcase>
<cfcase value="managejdeassign">
	<cfset attributes.xfa.assignjdedepts = "main.assignjdedepts">
	<cfinclude template="actions/act_manageassigndepts.cfm">
</cfcase>
<cfcase value="popgroupinfo">
	<cfinclude template="actions/act_popgroupinfo.cfm">
</cfcase>
<cfcase value="createsublistsmulti">
	<cfinclude template="actions/act_createsublistsmulti.cfm">
</cfcase>
<!--- <cfcase value="dashboard">
	<cfinclude template="queries/qry_getBUs.cfm">
	<cfinclude template="queries/qry_incassignedto.cfm">
	<cfinclude template="queries/qry_getjdemanhourscfd.cfm">
	<cfinclude template="queries/qry_getmanualmanhourscfd.cfm">
	<cfinclude template="queries/qry_getcontractormanhourscfd.cfm">
	<cfinclude template="queries/qry_getincidentcfd.cfm">
	<cfinclude template="queries/qry_getlastLTI.cfm">
	<cfinclude template="actions/act_builddashstructs.cfm">
	<cfinclude template="queries/qry_getincclosed.cfm">
	<cfinclude template="actions/act_buildchartxml.cfm">
	<cfinclude template="queries/qry_gettargets.cfm">
	<cfinclude template="displays/dsp_dashboardcharts.cfm">
	<cfinclude template="displays/dsp_dashboard.cfm">
</cfcase> --->
<!--- <cfcase value="mytasks">
	<cfset attributes.xfa.incidentreport = "incidents.incidentreport">
	<cfset attributes.xfa.firstalert = "incidents.firstalert">
	<cfset attributes.xfa.capa = "incidents.capa">
	<cfset attributes.xfa.investigation = "incidents.investigation">
	<cfset attributes.xfa.fatalityreport = "incidents.fatalityreport">
	<cfinclude template="queries/qry_getpendingsfr.cfm">
	<cfinclude template="queries/qry_getmoreinfo.cfm">
	<cfinclude template="queries/qry_getsentforreview.cfm">
	<cfinclude template="queries/qry_getfatsentforreview.cfm">
	<cfinclude template="queries/qry_getneedfirstalert.cfm">
	<cfinclude template="queries/qry_getneedfirstalertreview.cfm">
	<cfinclude template="queries/qry_getmoreinfoFA.cfm">
	<cfinclude template="queries/qry_getneedfirstalertSRreview.cfm">
	<cfinclude template="queries/qry_capaincomplete.cfm">
	<cfinclude template="queries/qry_capaoverdue.cfm">
	<cfinclude template="queries/qry_needinvestigation.cfm">
	<cfinclude template="queries/qry_needinvestigationreview.cfm">
	<cfinclude template="queries/qry_getneedinvestingationmoreinfo.cfm">
	<cfinclude template="queries/qry_needIRP.cfm">
	<cfinclude template="displays/dsp_mytasks.cfm">
</cfcase> --->
<!--- <cfcase value="sample2">
	<cfinclude template="displays/dsp_showplaceholder2.cfm">
</cfcase>
<cfcase value="sample2">
	<cfinclude template="displays/dsp_showplaceholder.cfm">
</cfcase> --->
<!--- <cfcase value="sample3,sample4">
	<!cfinclude template="displays/dsp_showplaceholder.cfm">
</cfcase> --->
<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
