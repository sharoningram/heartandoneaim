<cfoutput>
<!--- <cfdump var="#lastltistruct#"> --->
<cfparam name="dosearch" default="">

<table cellpadding="2" cellspacing="0" border="0" width="100%">
<tr>
<td valign="top">
<table cellpadding="3" cellspacing="0"  width="95%" border="0">
	<tr>
		<td class="purplebg" align="center" colspan="14"><strong class="bodytextwhite">#year(now())# YTD Performance Summary: Amec Foster Wheeler Global</strong></td>
	</tr>
	<tr>
		
		<td class="formlable" width="15%"><strong>#request.bulabellong#</strong></td>
		<td class="formlable" ><strong>Incident Assigned To</strong></td>
		<td class="formlable" align="center"><strong>Hours</strong></td>
		<td class="formlable" align="center"><strong>Fatality</strong></td>
		<td class="formlable" align="center"><strong>LTI</strong></td>
		<td class="formlable" align="center"><strong>RWC</strong></td>
		<td class="formlable" align="center"><strong>MTC</strong></td>
		<td class="formlable" align="center"><strong>LTIR</strong></td>
		<td class="formlable" align="center"><strong>TRIR</strong></td>
		<td class="formlable" align="center"><strong>First Aid</strong></td>
		<td class="formlable" align="center"><strong>AIR</strong></td>
		<td class="formlable" align="center"><strong>Days Since Last LTI</strong></td>
		<td class="formlable" align="center"><strong>Manhours Since Last LTI</strong></td>
		<td class="formlable" align="center"><strong>Date of Last LTI</strong></td>
	</tr>
	<tr>
		<td colspan="14" class="formlable" ><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2468;" width="100%"></td>
	</tr>
	<cfset amfwctr = 0>
	<cfset subctr = 0>
	<cfset jvctr = 0>
	<cfset mgdctr = 0>
	<cfset globaltot = 0>
	<cfset buctr = 0>
	
	<cfset ltitotctrafw = 0>
	<cfset fattotctrafw = 0>
	<cfset rwctotctrafw = 0>
	<cfset mtctotctrafw = 0>
	<cfset fatotctrafw = 0>
	
	<cfset ltitotctrsub = 0>
	<cfset fattotctrsub = 0>
	<cfset rwctotctrsub = 0>
	<cfset mtctotctrsub = 0>
	<cfset fatotctrsub = 0>
	
	<cfset ltitotctrjv = 0>
	<cfset fattotctrjv = 0>
	<cfset rwctotctrjv = 0>
	<cfset mtctotctrjv = 0>
	<cfset fatotctrjv = 0>
	
	<cfset ltitotctrmgd = 0>
	<cfset fattotctrmgd = 0>
	<cfset rwctotctrmgd = 0>
	<cfset mtctotctrmgd = 0>
	<cfset fatotctrmgd = 0>
	
	<cfset ltitotctrglobal = 0>
	<cfset fattotctrglobal= 0>
	<cfset rwctotctrglobal = 0>
	<cfset mtctotctrglobal = 0>
	<cfset fatotctrglobal = 0>
	
	<cfset afwmaxdate = "">
	<cfset submaxdate = "">
	<cfset mgdmaxdate = "">
	<cfset jvmaxdate = "">
	
	<cfset hrstotafwc = "">
	<cfset hrstotsubc = "">
	<cfset hrstotjvc = "">
	<cfset hrstotmdgc = "">
	
	<cfloop query="getBUs" group="id">
	
	<cfset gtotair= 0>	
	<cfset gtotrec= 0>	
	<cfset gtothrs = 0>
	<cfset gtotfat = 0>	
	<cfset gtotlti = 0>	
	<cfset gtotrwc = 0>	
	<cfset gtotmtc = 0>
	<cfset gtotfa = 0>
	
	<cfset showrow = "no">
	
		<cfloop  group="group_number">
			<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
				<cfif listfind(groupsallowed,group_number) gt 0>
					<cfset showrow = "yes">
				</cfif>
			<cfelse>
				<cfset showrow = "yes">
			</cfif>
		</cfloop>
		
		
		<cfif showrow>
			<cfif trim(openbu) neq '' or trim(openou) neq ''>
				<cfif  trim(openbu) neq ''>
					<cfif listfindnocase(openbu,"all") eq 0>
						<cfif listfind(openbu,id) eq 0>
							<cfset showrow = "no">
						</cfif>
					</cfif>
				<cfelse>
					
						<cfif trim(openou) neq ''>
						
							<cfif listfindnocase(openou,"all") eq 0>
								<cfloop list="#openou#" index="i">
									<cfloop group="ouid">
									<cfif listgetat(i,2,"_") neq ouid>
										<cfset showrow = "no">
									<cfelse>
										<cfset showrow = "yes">
										<cfbreak>
									</cfif>
									</cfloop>
								</cfloop>
							</cfif>
						
					</cfif>
					
				</cfif>
				
			<!--- 	<cfloop  group="ouid">
					<cfif listfind(openou,ouid) eq 0>
						<cfset showrow = "no">
					</cfif>
					
				</cfloop> --->
			</cfif>
		</cfif>
		
		
		
			<!--- 	<cfif listfindnocase(openou,"all") eq 0>
					<cfloop list="#openou#" index="i">
						<cfif listgetat(i,2,"_") neq ouid>
							<cfset showourow = "no">
						</cfif>
					</cfloop>
				</cfif> --->
			
		
		
	<cfif showrow>
	<script type="text/javascript">
		function openBU#getBUs.id#(){
		
		document.getElementById("closeBU#getBUs.id#").style.display='';
		document.getElementById("openBU#getBUs.id#").style.display='none';
			<cfloop group="ouid">
				<cfloop query="getincassignedto">
				document.getElementById("ou_row_#getBUs.ouid#_spc").style.display=''; 
				document.getElementById("ou_row_#getBUs.ouid#_#IncAssignedID#").style.display=''; 
				</cfloop>
			</cfloop>
		
		}
		function closeBU#getBUs.id#(){
		
		document.getElementById("closeBU#getBUs.id#").style.display='none';
		document.getElementById("openBU#getBUs.id#").style.display='';
			<cfloop group="ouid">
				<cfloop query="getincassignedto">
				document.getElementById("ou_row_#getBUs.ouid#_spc").style.display='none'; 
				document.getElementById("ou_row_#getBUs.ouid#_#IncAssignedID#").style.display='none'; 
				</cfloop>
			</cfloop>
		
		}
	</script>
	<!--- 
	
	<cfset  = 0>
	<cfset gtotfa = 0> --->
	<cfset buctr = buctr+1>
		<cfset ctr = 0>
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("Amec-FW,Managed Contractor,Sub-Contractor,Joint Venture",IncAssignedTo) gt 0>
		<cfset ctr = ctr+1><!--- <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> --->
	<tr bgcolor="ffffff" >
		<td class="bodyTextGrey"><cfif ctr eq 1><!--- <a href="javascript:void(0);" onclick="openBU#getBUs.id#();" id="openBU#getBUs.id#" style="text-decoration:none;">+</a><a href="javascript:void(0);" onclick="closeBU#getBUs.id#();" id="closeBU#getBUs.id#" style="text-decoration:none;">-</a>&nbsp; --->#getBUs.Name#</cfif></td>
		<td class="formlablelt">#IncAssignedTo#</td>
		<td class="bodyTextGrey" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getBUs.ID)>#numberformat(manhrstruct[getBUs.id])#<cfset amfwctr = amfwctr+manhrstruct[getBUs.id]><cfset rowhrs = rowhrs+manhrstruct[getBUs.id]><cfset gtothrs = gtothrs+manhrstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getBUs.ID)>#numberformat(subhrsstruct[getBUs.id])#<cfset subctr = subctr+subhrsstruct[getBUs.id]><cfset gtothrs = gtothrs+subhrsstruct[getBUs.id]><cfset rowhrs = rowhrs+subhrsstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getBUs.ID)>#numberformat(mdgconstruct[getBUs.id])#<cfset mgdctr = mgdctr+mdgconstruct[getBUs.id]><cfset gtothrs = gtothrs+mdgconstruct[getBUs.id]><cfset rowhrs = rowhrs+mdgconstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getBUs.ID)>#numberformat(jvhrsstruct[getBUs.id])#<cfset jvctr = jvctr+jvhrsstruct[getBUs.id]><cfset gtothrs = gtothrs+jvhrsstruct[getBUs.id]><cfset rowhrs = rowhrs+jvhrsstruct[getBUs.id]><cfelse>0</cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center">
		
		<cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")>#FATstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrec = gtotrec+FATstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfat = gtotfat + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfat = gtotfat + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrmgd = fattotctrmgd + FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfat = gtotfat + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfat = gtotfat + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		</td>
		<td class="bodyTextGrey" align="center"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")>#LTIstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset rowlti = rowlti+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrec = gtotrec+LTIstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotlti = gtotlti + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotlti = gtotlti + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotlti = gtotlti + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotlti = gtotlti + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")>#RWCstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrec = gtotrec+RWCstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrwc = gtotrwc + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrwc = gtotrwc + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrmgd = rwctotctrmgd + RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrwc = gtotrwc + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrwc = gtotrwc + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="bodyTextGrey" align="center"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")>#MTCstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotrec = gtotrec+MTCstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotmtc = gtotmtc + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotmtc = gtotmtc + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrmgd = mtctotctrmgd + MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotmtc = gtotmtc + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotmtc = gtotmtc + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")>#FAstruct["#getBUs.ID#_#IncAssignedID#"]#<cfset rowair = rowair+FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotair = gtotair+FAstruct["#getBUs.ID#_#IncAssignedID#"]><Cfelse>0</cfif>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfa = gtotfa + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfa = gtotfa + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfa = gtotfa + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getBUs.ID#_#IncAssignedID#"]><cfset gtotfa = gtotfa + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		</td>
		
		<cfset dayssincelti = "">
		<cfset lastltidate = "">
		<cfset hrscount = "">
		<cfif structkeyexists(lastltistruct,"#getBUs.ID#_#IncAssignedID#")>
			<cfset lastltidate = dateformat(lastltistruct["#getBUs.ID#_#IncAssignedID#"],sysdateformat)>
			<cfif trim(lastltistruct["#getBUs.ID#_#IncAssignedID#"]) neq ''>
				<cfset dayssincelti = numberformat(datediff("d",lastltistruct["#getBUs.ID#_#IncAssignedID#"],now()))>
			</cfif>
		</cfif>
		<cfif trim(lastltidate) neq ''>
		<cfinclude template="../queries/qry_cust_getmhsincelti.cfm">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif trim(afwmaxdate) eq ''><cfset afwmaxdate = lastltidate><cfset hrstotafwc = hrscount><cfelse><cfif afwmaxdate lt lastltidate><cfset afwmaxdate = lastltidate><cfset hrstotafwc = hrscount></cfif></cfif></cfcase>
		<cfcase value="3"><cfif trim(submaxdate) eq ''><cfset submaxdate = lastltidate><cfset hrstotsubc = hrscount><cfelse><cfif submaxdate lt lastltidate><cfset submaxdate = lastltidate><cfset hrstotsubc = hrscount></cfif></cfif></cfcase>
		<cfcase value="4"><cfif trim(mgdmaxdate) eq ''><cfset mgdmaxdate = lastltidate><cfset hrstotmdgc = hrscount><cfelse><cfif mgdmaxdate lt lastltidate><cfset mgdmaxdate = lastltidate><cfset hrstotmdgc = hrscount></cfif></cfif></cfcase>
		<cfcase value="11"><cfif trim(jvmaxdate) eq ''><cfset jvmaxdate = lastltidate><cfset hrstotjvc = hrscount><cfelse><cfif jvmaxdate lt lastltidate><cfset jvmaxdate = lastltidate><cfset hrstotjvc = hrscount></cfif></cfif></cfcase>
		
		</cfswitch>
		</cfif>
		
		
		
		<td class="bodyTextGrey" align="center"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center">#dayssincelti#</td>
		<td class="bodyTextGrey" align="center">#hrscount#</td>
		<td class="formlablelt" align="center">#lastltidate#</td>
	</tr>
		</cfif>
		</cfloop>
		
	<tr class="formlablelt">
		<td></td>
		<td class="formlable"><strong>Total</strong></td>
		<td align="center">#numberformat(gtothrs)#</td>
		<td class="formlable" align="center">#numberformat(gtotfat)#</td>
		<td align="center">#numberformat(gtotlti)#</td>
		<td class="formlable" align="center">#numberformat(gtotrwc)#</td>
		<td align="center">#numberformat(gtotmtc)#</td>
		<td class="formlable" align="center"><cfif gtothrs gt 0 and gtotlti gt 0>#numberformat((gtotlti*200000)/gtothrs,"0.000")#<cfelse>0</cfif></td>
		<td align="center"><cfif gtothrs gt 0 and gtotrec gt 0>#numberformat((gtotrec*200000)/gtothrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable" align="center">#numberformat(gtotfa)#</td>
		<td align="center"><cfset gtotair = gtotair+gtotrec><cfif gtothrs gt 0 and gtotair gt 0>#numberformat((gtotair*200000)/gtothrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable"></td>
		<td></td>
		<td class="formlable"></td>
	</tr>
	
		
	<tr>
		<td colspan="14"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2468;" width="100%"></td>
	</tr>
	
	
	
	
	
	
	<cfloop  group="ouid">
	
	<cfset showourow = "no">
	
		<cfloop  group="group_number">
			<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
				<cfif listfind(groupsallowed,group_number) gt 0>
					<cfif listfind(openbu,getBUs.ID) gt 0 or listfindnocase(openbu,"All") gt 0>
						<cfset showourow = "yes">
					</cfif>
				</cfif>
			<cfelse>
				<cfif listfind(openbu,getBUs.ID) gt 0 or listfindnocase(openbu,"All") gt 0>
					<cfset showourow = "yes">
				<cfelseif trim(openbu) eq ''>
					<cfif trim(openou) neq '' and  listfindnocase(openou,"all") eq 0>
						<cfloop list="#openou#" index="i">
							<cfif listgetat(i,2,"_") eq ouid>
								<cfset showourow = "yes">
							</cfif>
						</cfloop>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		
		
		<cfif showourow>
			<cfif trim(openou) neq ''>
				<cfif listfindnocase(openou,"all") eq 0>
					<cfloop list="#openou#" index="i">
						<cfif listgetat(i,2,"_") neq ouid>
							<cfset showourow = "no">
						</cfif>
					</cfloop>
					
					
				</cfif>
			</cfif>
		</cfif>	
		
		
		
	<cfif showourow>
	<!--- <script type="text/javascript">
		function openou#getBUs.ouid#(){
		
		document.getElementById("closeou#getBUs.ouid#").style.display='';
		document.getElementById("openou#getBUs.ouid#").style.display='none';
			<cfloop group="group_number">
				<cfloop query="getincassignedto">
				document.getElementById("grp_row_#getBUs.group_number#_spc").style.display=''; 
				document.getElementById("grp_row_#getBUs.group_number#_#IncAssignedID#").style.display=''; 
				</cfloop>
			</cfloop>
		
		}
		function closeou#getBUs.ouid#(){
		
		document.getElementById("closeou#getBUs.ouid#").style.display='none';
		document.getElementById("openou#getBUs.ouid#").style.display='';
			<cfloop group="group_number">
				<cfloop query="getincassignedto">
				document.getElementById("grp_row_#getBUs.group_number#_spc").style.display='none'; 
				document.getElementById("grp_row_#getBUs.group_number#_#IncAssignedID#").style.display='none'; 
				</cfloop>
			</cfloop>
		
		}
	</script> --->
	<cfset outotair= 0>	
	<cfset outotrec= 0>	
	<cfset outothrs = 0>
	<cfset outotfat = 0>	
	<cfset outotlti = 0>	
	<cfset outotrwc = 0>	
	<cfset outotmtc = 0>
	<cfset outotfa = 0>
	
	
		<cfset buctr = buctr+1>
		<cfset ctr = 0>
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("Amec-FW,Managed Contractor,Sub-Contractor,Joint Venture",IncAssignedTo) gt 0>
		<cfset ctr = ctr+1><!--- <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> --->
	<tr bgcolor="ffffff"  id="ou_row_#getBUs.ouid#_#IncAssignedID#">
		<td class="bodyTextGrey"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td width="1%">&nbsp;&nbsp;&nbsp;</td><td width="99%" class="bodyTextGrey"><cfif ctr eq 1><!--- <a href="javascript:void(0);" onclick="openou#getBUs.ouid#();" id="openou#getBUs.ouid#" style="text-decoration:none;">+</a><a href="javascript:void(0);" onclick="closeou#getBUs.ouid#();" id="closeou#getBUs.ouid#" style="text-decoration:none;">-</a>&nbsp; --->#getBUs.ouname#</cfif></td></tr></table></td>
		<td class="formlablelt">#IncAssignedTo#</td>
		<td class="bodyTextGrey" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getBUs.ouid)>#numberformat(manhrstruct[getBUs.ouid])#<cfset rowhrs = rowhrs+manhrstruct[getBUs.ouid]><cfset outothrs = outothrs+manhrstruct[getBUs.ouid]><cfelse>0</cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getBUs.ouid)>#numberformat(subhrsstruct[getBUs.ouid])#<cfset rowhrs = rowhrs+subhrsstruct[getBUs.ouid]><cfset outothrs = outothrs+subhrsstruct[getBUs.ouid]><cfelse>0</cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getBUs.ouid)>#numberformat(mdgconstruct[getBUs.ouid])#<cfset rowhrs = rowhrs+mdgconstruct[getBUs.ouid]><cfset outothrs = outothrs+mdgconstruct[getBUs.ouid]><cfelse>0</cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getBUs.ouid)>#numberformat(jvhrsstruct[getBUs.ouid])#<cfset rowhrs = rowhrs+jvhrsstruct[getBUs.ouid]><cfset outothrs = outothrs+jvhrsstruct[getBUs.ouid]><cfelse>0</cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center">
		
		<cfif structkeyexists(FATstruct,"#getBUs.ouid#_#IncAssignedID#")>#FATstruct["#getBUs.ouid#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+FATstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotrec = outotrec+FATstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotfat = outotfat+FATstruct["#getBUs.ouid#_#IncAssignedID#"]><Cfelse>0</cfif>
			
		</td>
		<td class="bodyTextGrey" align="center"><cfif structkeyexists(LTIstruct,"#getBUs.ouid#_#IncAssignedID#")>#LTIstruct["#getBUs.ouid#_#IncAssignedID#"]#<cfset rowlti = rowlti+LTIstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotlti = outotlti+LTIstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset rowtrir = rowtrir+LTIstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotrec = outotrec+LTIstruct["#getBUs.ouid#_#IncAssignedID#"]><Cfelse>0</cfif>
	
		</td>
		<td class="formlablelt" align="center"><cfif structkeyexists(RWCstruct,"#getBUs.ouid#_#IncAssignedID#")>#RWCstruct["#getBUs.ouid#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+RWCstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotrwc = outotrwc+RWCstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotrec = outotrec+RWCstruct["#getBUs.ouid#_#IncAssignedID#"]><Cfelse>0</cfif>

		</td>
		<td class="bodyTextGrey" align="center"><cfif structkeyexists(MTCstruct,"#getBUs.ouid#_#IncAssignedID#")>#MTCstruct["#getBUs.ouid#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+MTCstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotmtc = outotmtc+MTCstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotrec = outotrec+MTCstruct["#getBUs.ouid#_#IncAssignedID#"]><Cfelse>0</cfif>
		
		</td>
		<td class="formlablelt" align="center"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center"><cfif structkeyexists(FAstruct,"#getBUs.ouid#_#IncAssignedID#")>#FAstruct["#getBUs.ouid#_#IncAssignedID#"]#<cfset outotfa = outotfa+FAstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset rowair = rowair+FAstruct["#getBUs.ouid#_#IncAssignedID#"]><cfset outotair = outotair+FAstruct["#getBUs.ouid#_#IncAssignedID#"]><Cfelse>0</cfif>
		
		</td>
		
		<cfset dayssincelti = "">
		<cfset lastltidate = "">
		<cfset hrscount = "">
		<cfif structkeyexists(lastltistruct,"#getBUs.ouid#_#IncAssignedID#")>
			<cfset lastltidate = dateformat(lastltistruct["#getBUs.ouid#_#IncAssignedID#"],sysdateformat)>
			<cfif trim(lastltistruct["#getBUs.ouid#_#IncAssignedID#"]) neq ''>
				<cfset dayssincelti = numberformat(datediff("d",lastltistruct["#getBUs.ouid#_#IncAssignedID#"],now()))>
			</cfif>
		</cfif>
	
		
		<td class="bodyTextGrey" align="center"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center">#dayssincelti#</td>
		<td class="bodyTextGrey" align="center">#hrscount#</td>
		<td class="formlablelt" align="center">#lastltidate#</td>
	</tr>
		</cfif>
		</cfloop>
			
	<tr class="formlablelt">
		<td></td>
		<td class="formlable"><strong>Total</strong></td>
		<td align="center">#numberformat(outothrs)#</td>
		<td class="formlable" align="center">#numberformat(outotfat)#</td>
		<td align="center">#numberformat(outotlti)#</td>
		<td class="formlable" align="center">#numberformat(outotrwc)#</td>
		<td align="center">#numberformat(outotmtc)#</td>
		<td class="formlable" align="center"><cfif outothrs gt 0 and outotlti gt 0>#numberformat((outotlti*200000)/outothrs,"0.000")#<cfelse>0</cfif></td>
		<td align="center"><cfif outothrs gt 0 and outotrec gt 0>#numberformat((outotrec*200000)/outothrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable" align="center">#numberformat(outotfa)#</td>
		<td align="center"><cfset outotair = outotair+outotrec><cfif outothrs gt 0 and outotair gt 0>#numberformat((outotair*200000)/outothrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable"></td>
		<td></td>
		<td class="formlable"></td>
	</tr>
	
	<tr id="ou_row_#getBUs.ouid#_spc">
		<td colspan="14"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2468;" width="100%"></td>
	</tr>
	
	
	<cfloop  group="group_number">
	<cfif listfind(openou,"#getBUs.id#_#getBUs.ouid#") gt 0 or listfindnocase(openou,"All") gt 0>
	
	<cfset prjtotair= 0>	
	<cfset prjtotrec= 0>	
	<cfset prjtothrs = 0>
	<cfset prjtotfat = 0>	
	<cfset prjtotlti = 0>	
	<cfset prjtotrwc = 0>	
	<cfset prjtotmtc = 0>
	<cfset prjtotfa = 0>
	
	<cfset buctr = buctr+1>
		<cfset ctr = 0>
		<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("Amec-FW,Managed Contractor,Sub-Contractor,Joint Venture",IncAssignedTo) gt 0>
		<cfset ctr = ctr+1><!--- <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif> --->
	<tr bgcolor="ffffff" id="grp_row_#getBUs.group_number#_#IncAssignedID#">
		<td class="bodyTextGrey"><cfif ctr eq 1><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td width="1%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td width="99%" class="bodyTextGrey">#getBUs.group_name#</td></tr></table></cfif></td>
		<td class="formlablelt">#IncAssignedTo#</td>
		<td class="bodyTextGrey" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(grpmanhrstruct,getBUs.group_number)>#numberformat(grpmanhrstruct[getBUs.group_number])#<cfset rowhrs = rowhrs+grpmanhrstruct[getBUs.group_number]><cfset prjtothrs = prjtothrs+grpmanhrstruct[getBUs.group_number]><cfelse>0</cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(grpsubhrsstruct,getBUs.group_number)>#numberformat(grpsubhrsstruct[getBUs.group_number])#<cfset rowhrs = rowhrs+grpsubhrsstruct[getBUs.group_number]><cfset prjtothrs = prjtothrs+grpsubhrsstruct[getBUs.group_number]><cfelse>0</cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(grpmdgconstruct,getBUs.group_number)>#numberformat(grpmdgconstruct[getBUs.group_number])#<cfset rowhrs = rowhrs+grpmdgconstruct[getBUs.group_number]><cfset prjtothrs = prjtothrs+grpmdgconstruct[getBUs.group_number]><cfelse>0</cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(grpjvhrsstruct,getBUs.group_number)>#numberformat(grpjvhrsstruct[getBUs.group_number])#<cfset rowhrs = rowhrs+grpjvhrsstruct[getBUs.group_number]><cfset prjtothrs = prjtothrs+grpjvhrsstruct[getBUs.group_number]><cfelse>0</cfif></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center">
		
		<cfif structkeyexists(grpFATstruct,"#getBUs.group_number#_#IncAssignedID#")>#grpFATstruct["#getBUs.group_number#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+grpFATstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotrec = prjtotrec+grpFATstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotfat = prjtotfat+grpFATstruct["#getBUs.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
			
		</td>
		<td class="bodyTextGrey" align="center"><cfif structkeyexists(grpLTIstruct,"#getBUs.group_number#_#IncAssignedID#")>#grpLTIstruct["#getBUs.group_number#_#IncAssignedID#"]#<cfset rowlti = rowlti+grpLTIstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotrec = prjtotrec+grpLTIstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset rowtrir = rowtrir+grpLTIstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotlti = prjtotlti+grpLTIstruct["#getBUs.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
	
		</td>
		<td class="formlablelt" align="center"><cfif structkeyexists(grpRWCstruct,"#getBUs.group_number#_#IncAssignedID#")>#grpRWCstruct["#getBUs.group_number#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+grpRWCstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotrec = prjtotrec+grpRWCstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotrwc = prjtotrwc+grpRWCstruct["#getBUs.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>

		</td>
		<td class="bodyTextGrey" align="center"><cfif structkeyexists(grpMTCstruct,"#getBUs.group_number#_#IncAssignedID#")>#grpMTCstruct["#getBUs.group_number#_#IncAssignedID#"]#<cfset rowtrir = rowtrir+grpMTCstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotrec = prjtotrec+grpMTCstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotmtc = prjtotmtc+grpMTCstruct["#getBUs.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		
		</td>
		<td class="formlablelt" align="center"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="bodyTextGrey" align="center"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center"><cfif structkeyexists(grpFAstruct,"#getBUs.group_number#_#IncAssignedID#")>#grpFAstruct["#getBUs.group_number#_#IncAssignedID#"]#<cfset rowair = rowair+grpFAstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotfa = prjtotfa+grpFAstruct["#getBUs.group_number#_#IncAssignedID#"]><cfset prjtotair = prjtotair+grpFAstruct["#getBUs.group_number#_#IncAssignedID#"]><Cfelse>0</cfif>
		
		</td>
		
		<cfset dayssincelti = "">
		<cfset lastltidate = "">
		<cfset hrscount = "">
		<cfif structkeyexists(grplastltistruct,"#getBUs.group_number#_#IncAssignedID#")>
			<cfset lastltidate = dateformat(grplastltistruct["#getBUs.group_number#_#IncAssignedID#"],sysdateformat)>
			<cfif trim(grplastltistruct["#getBUs.group_number#_#IncAssignedID#"]) neq ''>
				<cfset dayssincelti = numberformat(datediff("d",grplastltistruct["#getBUs.group_number#_#IncAssignedID#"],now()))>
			</cfif>
		</cfif>
	
		
		<td class="bodyTextGrey" align="center"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center">#dayssincelti#</td>
		<td class="bodyTextGrey" align="center">#hrscount#</td>
		<td class="formlablelt" align="center">#lastltidate#</td>
	</tr>
		</cfif>
		</cfloop>
	
		<tr class="formlablelt">
		<td></td>
		<td class="formlable"><strong>Total</strong></td>
		<td align="center">#numberformat(prjtothrs)#</td>
		<td class="formlable" align="center">#numberformat(prjtotfat)#</td>
		<td align="center">#numberformat(prjtotlti)#</td>
		<td class="formlable" align="center">#numberformat(prjtotrwc)#</td>
		<td align="center">#numberformat(prjtotmtc)#</td>
		<td class="formlable" align="center"><cfif prjtothrs gt 0 and prjtotlti gt 0>#numberformat((prjtotlti*200000)/prjtothrs,"0.000")#<cfelse>0</cfif></td>
		<td align="center"><cfif prjtothrs gt 0 and prjtotrec gt 0>#numberformat((prjtotrec*200000)/prjtothrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable" align="center">#numberformat(prjtotfa)#</td>
		<td align="center"><cfset prjtotair = prjtotair+prjtotrec><cfif prjtothrs gt 0 and prjtotair gt 0>#numberformat((prjtotair*200000)/prjtothrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlable"></td>
		<td></td>
		<td class="formlable"></td>
	</tr>
	<tr id="grp_row_#getBUs.group_number#_spc">
		<td colspan="14"><img src="images/spacer.gif" border="0" height="1" style="background-color:5f2468;" width="100%"></td>
	</tr>
		</cfif>
	</cfloop>
	
	
	
	
	
	</cfif>
	
	
	
	
	</cfloop>
	
	<cfelse>
	
	<cfloop query="getincassignedto">
	<cfif listfindnocase("Amec-FW,Managed Contractor,Sub-Contractor,Joint Venture",IncAssignedTo) gt 0>
			<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(manhrstruct,getBUs.ID)><cfset amfwctr = amfwctr+manhrstruct[getBUs.id]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(subhrsstruct,getBUs.ID)><cfset subctr = subctr+subhrsstruct[getBUs.id]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(mdgconstruct,getBUs.ID)><cfset mgdctr = mgdctr+mdgconstruct[getBUs.id]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(jvhrsstruct,getBUs.ID)><cfset jvctr = jvctr+jvhrsstruct[getBUs.id]></cfif></cfcase>
		
		</cfswitch>
		
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrafw = fattotctrafw + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrsub = fattotctrsub + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrmgd = fattotctrmgd + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FATstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fattotctrjv = fattotctrjv + FATstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrafw = ltitotctrafw + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrsub = ltitotctrsub + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrmgd = ltitotctrmgd + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(LTIstruct,"#getBUs.ID#_#IncAssignedID#")><cfset ltitotctrjv = ltitotctrjv + LTIstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrafw = rwctotctrafw + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrsub = rwctotctrsub + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrmgd = rwctotctrmgd + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(RWCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset rwctotctrjv = rwctotctrjv + RWCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrafw = mtctotctrafw + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrsub = mtctotctrsub + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrmgd = mtctotctrmgd + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(MTCstruct,"#getBUs.ID#_#IncAssignedID#")><cfset mtctotctrjv = mtctotctrjv + MTCstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrafw = fatotctrafw + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="3"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrsub = fatotctrsub + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="4"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrmgd = fatotctrmgd + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		<cfcase value="11"><cfif structkeyexists(FAstruct,"#getBUs.ID#_#IncAssignedID#")><cfset fatotctrjv = fatotctrjv + FAstruct["#getBUs.ID#_#IncAssignedID#"]></cfif></cfcase>
		
		</cfswitch>
		<cfset dayssincelti = "">
		<cfset lastltidate = "">
		<cfset hrscount = "">
		<cfif structkeyexists(lastltistruct,"#getBUs.ID#_#IncAssignedID#")>
			<cfset lastltidate = dateformat(lastltistruct["#getBUs.ID#_#IncAssignedID#"],sysdateformat)>
			<cfif trim(lastltistruct["#getBUs.ID#_#IncAssignedID#"]) neq ''>
				<cfset dayssincelti = numberformat(datediff("d",lastltistruct["#getBUs.ID#_#IncAssignedID#"],now()))>
			</cfif>
		</cfif>
		<cfif trim(lastltidate) neq ''>
		<cfinclude template="../queries/qry_cust_getmhsincelti.cfm">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif trim(afwmaxdate) eq ''><cfset afwmaxdate = lastltidate><cfset hrstotafwc = hrscount><cfelse><cfif afwmaxdate lt lastltidate><cfset afwmaxdate = lastltidate><cfset hrstotafwc = hrscount></cfif></cfif></cfcase>
		<cfcase value="3"><cfif trim(submaxdate) eq ''><cfset submaxdate = lastltidate><cfset hrstotsubc = hrscount><cfelse><cfif submaxdate lt lastltidate><cfset submaxdate = lastltidate><cfset hrstotsubc = hrscount></cfif></cfif></cfcase>
		<cfcase value="4"><cfif trim(mgdmaxdate) eq ''><cfset mgdmaxdate = lastltidate><cfset hrstotmdgc = hrscount><cfelse><cfif mgdmaxdate lt lastltidate><cfset mgdmaxdate = lastltidate><cfset hrstotmdgc = hrscount></cfif></cfif></cfcase>
		<cfcase value="11"><cfif trim(jvmaxdate) eq ''><cfset jvmaxdate = lastltidate><cfset hrstotjvc = hrscount><cfelse><cfif jvmaxdate lt lastltidate><cfset jvmaxdate = lastltidate><cfset hrstotjvc = hrscount></cfif></cfif></cfcase>
		
		</cfswitch>
		</cfif>
		
		</cfif>
		</cfloop>
	</cfif>
	</cfloop>
	<cfset ctr = 0>
	<cfset buctr = buctr + 1>
	
	<cfset showtotrow = "no">
	<cfif trim(openbu) eq '' and trim(openou) eq ''>
		<cfset showtotrow = "yes">
	<cfelse>
		<cfif listfindnocase(openbu,"all") gt 0 and  listfindnocase(openou,"all") gt 0 >
			<cfset showtotrow = "yes">
		</cfif>
	</cfif>
	
	<cfif showtotrow>
	<cfloop query="getincassignedto">
		<cfset rowhrs = 0>
		<cfset rowlti = 0>
		<cfset rowtrir = 0>
		<cfset rowair = 0>
	<cfif listfindnocase("Amec-FW,Managed Contractor,Sub-Contractor,Joint Venture",IncAssignedTo) gt 0>
		<cfset ctr = ctr+1>
	<tr <cfif buctr mod 2 eq 0>class="formlable"<cfelse>bgcolor="ffffff"</cfif>>
		<td class="formlable"><cfif ctr eq 1>Amec Foster Wheeler Global</cfif></td>
		<td class="formlablelt">#IncAssignedTo#</td>
		<td class="formlable" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(amfwctr)#<cfset globaltot = globaltot+amfwctr><cfset rowhrs = rowhrs+amfwctr></cfcase>
		<cfcase value="3">#numberformat(subctr)#<cfset globaltot = globaltot+subctr><cfset rowhrs = rowhrs+subctr></cfcase>
		<cfcase value="4">#numberformat(mgdctr)#<cfset globaltot = globaltot+mgdctr><cfset rowhrs = rowhrs+mgdctr></cfcase>
		<cfcase value="11">#numberformat(jvctr)#<cfset globaltot = globaltot+jvctr><cfset rowhrs = rowhrs+jvctr></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(fattotctrafw)#<cfset fattotctrglobal = fattotctrglobal+fattotctrafw><cfset rowtrir = rowtrir+fattotctrafw></cfcase>
		<cfcase value="3">#numberformat(fattotctrsub)#<cfset fattotctrglobal = fattotctrglobal+fattotctrsub><cfset rowtrir = rowtrir+fattotctrsub></cfcase>
		<cfcase value="4">#numberformat(fattotctrmgd)#<cfset fattotctrglobal = fattotctrglobal+fattotctrmgd><cfset rowtrir = rowtrir+fattotctrmgd></cfcase>
		<cfcase value="11">#numberformat(fattotctrjv)#<cfset fattotctrglobal = fattotctrglobal+fattotctrjv><cfset rowtrir = rowtrir+fattotctrjv></cfcase>
		
		</cfswitch>
		
		</td>
		<td class="formlable" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(ltitotctrafw)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrafw><cfset rowlti =rowlti+ltitotctrafw><cfset rowtrir = rowtrir+ltitotctrafw></cfcase>
		<cfcase value="3">#numberformat(ltitotctrsub)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrsub><cfset rowlti =rowlti+ltitotctrsub><cfset rowtrir = rowtrir+ltitotctrsub></cfcase>
		<cfcase value="4">#numberformat(ltitotctrmgd)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrmgd><cfset rowlti =rowlti+ltitotctrmgd><cfset rowtrir = rowtrir+ltitotctrmgd></cfcase>
		<cfcase value="11">#numberformat(ltitotctrjv)#<cfset ltitotctrglobal = ltitotctrglobal+ltitotctrjv><cfset rowlti =rowlti+ltitotctrjv><cfset rowtrir = rowtrir+ltitotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(rwctotctrafw)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrafw><cfset rowtrir = rowtrir+rwctotctrafw></cfcase>
		<cfcase value="3">#numberformat(rwctotctrsub)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrsub><cfset rowtrir = rowtrir+rwctotctrsub></cfcase>
		<cfcase value="4">#numberformat(rwctotctrmgd)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrmgd><cfset rowtrir = rowtrir+rwctotctrmgd></cfcase>
		<cfcase value="11">#numberformat(rwctotctrjv)#<cfset rwctotctrglobal = rwctotctrglobal+rwctotctrjv><cfset rowtrir = rowtrir+rwctotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlable" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(mtctotctrafw)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrafw><cfset rowtrir = rowtrir+mtctotctrafw></cfcase>
		<cfcase value="3">#numberformat(mtctotctrsub)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrsub><cfset rowtrir = rowtrir+mtctotctrsub></cfcase>
		<cfcase value="4">#numberformat(mtctotctrmgd)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrmgd><cfset rowtrir = rowtrir+mtctotctrmgd></cfcase>
		<cfcase value="11">#numberformat(mtctotctrjv)#<cfset mtctotctrglobal = mtctotctrglobal+mtctotctrjv><cfset rowtrir = rowtrir+mtctotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<td class="formlablelt" align="center"><cfif rowhrs gt 0 and rowlti gt 0>#numberformat((rowlti*200000)/rowhrs,"0.000")#<cfelse>0</cfif></td>
		<td class="formlable" align="center"><cfif rowhrs gt 0 and rowtrir gt 0>#numberformat((rowtrir*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1">#numberformat(fatotctrafw)#<cfset fatotctrglobal = fatotctrglobal+fatotctrafw><cfset rowair = rowair+fatotctrafw></cfcase>
		<cfcase value="3">#numberformat(fatotctrsub)#<cfset fatotctrglobal = fatotctrglobal+fatotctrsub><cfset rowair = rowair+fatotctrsub></cfcase>
		<cfcase value="4">#numberformat(fatotctrmgd)#<cfset fatotctrglobal = fatotctrglobal+fatotctrmgd><cfset rowair = rowair+fatotctrmgd></cfcase>
		<cfcase value="11">#numberformat(fatotctrjv)#<cfset fatotctrglobal = fatotctrglobal+fatotctrjv><cfset rowair = rowair+fatotctrjv></cfcase>
		
		</cfswitch>
		</td>
		<cfset dspdate = "">
		<cfset dspdaysince = "">
		<cfset dspdayssince = "">
		<cfswitch expression="#incassignedid#">
		
		<cfcase value="1"><cfif trim(afwmaxdate) neq ''><cfset dspdate = afwmaxdate><cfset dspdaysince = datediff("d",dspdate,now())><cfset dspdayssince = hrstotafwc></cfif></cfcase>
		<cfcase value="3"><cfif trim(submaxdate) neq ''><cfset dspdate = submaxdate><cfset dspdaysince = datediff("d",dspdate,now())><cfset dspdayssince = hrstotsubc></cfif></cfcase>
		<cfcase value="4"><cfif trim(mgdmaxdate) neq ''><cfset dspdate = mgdmaxdate><cfset dspdaysince = datediff("d",dspdate,now())><cfset dspdayssince = hrstotmdgc></cfif></cfcase>
		<cfcase value="11"><cfif trim(jvmaxdate) neq ''><cfset dspdate = jvmaxdate><cfset dspdaysince = datediff("d",dspdate,now())><cfset dspdayssince = hrstotjvc></cfif></cfcase>
		
		</cfswitch>
		
		<td class="formlable" align="center"><cfset rowair = rowair+rowtrir><cfif rowhrs gt 0 and rowair gt 0>#numberformat((rowair*200000)/rowhrs,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center">#dspdaysince#</td>
		<td class="bodyTextGrey" align="center"><cfif trim(dspdayssince) neq ''>#dspdayssince#</cfif></td>
		<td class="formlablelt" align="center">#dspdate#</td>
	</tr>
	</cfif>

	</cfloop>
	
	<tr>
		<td class="formlable"></td>
		<td class="formlablelt"><strong>Total</strong></td>
		<td class="formlable" align="center"><strong>#numberformat(globaltot)#</strong></td>
		<td class="formlablelt" align="center">#numberformat(fattotctrglobal)#</td>
		<td class="formlable" align="center">#numberformat(ltitotctrglobal)#</td>
		<td class="formlablelt" align="center">#numberformat(rwctotctrglobal)#</td>
		<td class="formlable" align="center">#numberformat(mtctotctrglobal)#</td>
		<td class="formlablelt" align="center"><cfif globaltot gt 0 and ltitotctrglobal gt 0>#numberformat((ltitotctrglobal*200000)/globaltot,"0.000")#<cfelse>0</cfif></td>
		<td class="formlable" align="center"><cfset glbtrir = fattotctrglobal+ltitotctrglobal+rwctotctrglobal+mtctotctrglobal><cfif globaltot gt 0 and glbtrir gt 0>#numberformat((glbtrir*200000)/globaltot,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center">#numberformat(fatotctrglobal)#</td>
		<td class="formlable" align="center"><cfset glbair = fatotctrglobal+glbtrir><cfif globaltot gt 0 and glbair gt 0>#numberformat((glbair*200000)/globaltot,"0.00")#<cfelse>0</cfif></td>
		<td class="formlablelt" align="center"></td>
		<td class="formlable" align="center"></td>
		<td class="formlablelt" align="center"></td>
	</tr>
	</cfif>
</table>

</td></tr></table>
</cfoutput>
<script type="text/javascript">
<cfoutput query="getBUs" group="id">

<cfset showrow = "no">
	
		<cfloop  group="group_number">
			<cfif listfindnocase(request.userlevel,"Global Admin") eq 0 and listfindnocase(request.userlevel,"Senior Executive View") eq 0 and listfindnocase(request.userlevel,"Reports Only") eq 0>
				<cfif listfind(groupsallowed,group_number) gt 0>
					<cfset showrow = "yes">
				</cfif>
			<cfelse>
				<cfset showrow = "yes">
			</cfif>
		</cfloop>
	<cfif showrow>



//document.getElementById("closeBU#getBUs.id#").style.display='none';

	<cfoutput group="ouid">
		// document.getElementById("closeou#getBUs.ouid#").style.display='none';
		<cfloop query="getincassignedto">
			// document.getElementById("ou_row_#getBUs.ouid#_#IncAssignedID#").style.display='none';
			// document.getElementById("ou_row_#getBUs.ouid#_spc").style.display='none';
		</cfloop>
		<cfoutput group="group_number">
			<cfloop query="getincassignedto">
				// document.getElementById("grp_row_#getBUs.group_number#_#incassignedid#").style.display='none';
				// document.getElementById("grp_row_#getBUs.group_number#_spc").style.display='none';
			</cfloop>
		</cfoutput>
	</cfoutput>
	</cfif>
</cfoutput>
</script>
