
<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="95%">
<tr>
	<td class="bodyTexthd" colspan="2"><cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"User") gt 0><cfif toggleview eq "dashboard"><cfoutput><a href="/#getappconfig.oneAIMpath#"></cfoutput>My Incidents</a>&nbsp;/&nbsp;Dashboard<cfelse>My Incidents</a>&nbsp;/&nbsp;<a href="index.cfm?fuseaction=main.main&toggleview=dashboard">Dashboard</a></cfif><cfelse>Dashboard</cfif></td>
<td align="right"><span class="bodytext"  id="para1"></span>&nbsp;<a href="index.cfm?fuseaction=main.main&toggleview=dashboard"><img src="images/recycle.gif" border="0" style="vertical-align:-3px"></a></td>
</tr>
</table>
<br>
<table cellpadding="2" cellspacing="0" border="0" width="95%">
	<tr>
		<td valign="top" align="center" id="chartContainer1" width="33%"></td>
		<td valign="top" align="center" id="chartContainer2" width="34%"></td>
		<td valign="top" align="center" id="chartContainer3" width="33%"></td>
	</tr>
</table>
<cfoutput>	
<script type="text/javascript">

FusionCharts.ready(function () {
    var multiseriesChart = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer1",
        "width": "90%",
        "height": "300",
        "dataFormat": "xml",
        "dataSource":  "#chart1xml#"
		
    }
	
	);

    multiseriesChart.render();
var multiseriesChart2 = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer2",
        "width": "90%",
        "height": "300",
        "dataFormat": "xml",
        "dataSource":  "#chart2xml#"

    });

    multiseriesChart2.render();
var multiseriesChart3 = new FusionCharts({
        "type": "MSCombiDY2D",
        "renderAt": "chartContainer3",
        "width": "90%",
        "height": "300",
        "dataFormat": "xml",
        "dataSource":  "#chart3xml#"

    });

    multiseriesChart3.render();
	
});
</script>
</cfoutput>
<br>
<cfif ytdhrs gt 0 and ytdallinc gt 0>
	<cfset ytdair = numberformat((ytdallinc*200000)/ytdhrs,"0.00")>
<cfelse>
	<cfset ytdair = 0>
</cfif>
<cfif ytdhrs gt 0 and ytdtririnc gt 0>
	<cfset ytdTRIR = numberformat((ytdtririnc*200000)/ytdhrs,"0.00")>
<cfelse>
	<cfset ytdTRIR = 0>
</cfif>
<cfif ytdhrs gt 0 and ytdltiinc gt 0>
	<cfset ytdLTI = numberformat((ytdltiinc*200000)/ytdhrs,"0.000")>
<cfelse>
	<cfset ytdLTI = 0>
</cfif>
<cfset airbg = "f7f1f9">
<cfset airtxt = "000000">
<cfif ytdair lt gettargets.airgoal>
	<cfset airbg = "00b050">
	<cfset airtxt = "ffffff">
<cfelseif ytdair gte gettargets.airgoal>
	<!--- <cfif airrate lt prevairrate> --->
		<cfset airbg = "ffc000">
	<!--- <cfelse>
		<cfset airbg = "ff0000">
		<cfset airtxt = "ffffff">
	</cfif> --->
</cfif>
<cfset tribg = "f7f1f9">
<cfset tritxt = "000000">
<cfif ytdTRIR lt gettargets.trirgoal>
	<cfset tribg = "00b050">
	<cfset tritxt = "ffffff">
<cfelseif ytdTRIR gte gettargets.trirgoal>
	<!--- <cfif airrate lt prevairrate> --->
		<cfset tribg = "ffc000">
	<!--- <cfelse>
		<cfset airbg = "ff0000">
		<cfset airtxt = "ffffff">
	</cfif> --->
</cfif>
<cfset ltbg = "f7f1f9">
<cfset lttxt = "000000">
<cfif ytdLTI lt gettargets.ltirgoal>
	<cfset ltbg = "00b050">
	<cfset lttxt = "ffffff">
<cfelseif ytdLTI gte gettargets.ltirgoal>
	<!--- <cfif airrate lt prevairrate> --->
		<cfset ltbg = "ffc000">
	<!--- <cfelse>
		<cfset airbg = "ff0000">
		<cfset airtxt = "ffffff">
	</cfif> --->
</cfif>
<cfoutput>
<table cellpadding="2" cellspacing="0" border="0" width="95%">
	<tr>
		<td valign="middle" align="center" width="44%">
			<table cellpadding="4" cellspacing="1" border="0" width="100%"  class="purplebg">
				<tr class="purplebg">
					<td class="bodytextwhite" align="center">Group Targets</td>
					<td class="bodytextwhite" align="center">#year(now())# Target</td>
					<td class="bodytextwhite" align="center">YTD Performance</td>
					<td class="bodytextwhite" align="center">Million Man Hours</td>
				</tr>
				
				<tr>
					<td class="formlable">TRIR</td>
					<td class="formlable" align="center"><cfif gettargets.trirgoal eq 0>Zero<cfelse>#gettargets.trirgoal#</cfif></td>
					<td align="center" class="bodyTextGrey"  bgcolor="#tribg#"><span style="color:#tritxt#;">#ytdTRIR#</span></td>
					<td rowspan="3" class="formlable" align="center"><cfif trim(ytdhrs) neq ''>#numberformat(ytdhrs/1000000,"0.00")#<cfelse>0</cfif></td>
				</tr>
				<tr>
					<td class="formlable">LTIR</td>
					<td class="formlable" align="center"><cfif gettargets.ltirgoal eq 0>Zero<cfelse>#gettargets.ltirgoal#</cfif></td>
					<td bgcolor="#ltbg#" align="center" class="bodyTextGrey"><span style="color:#lttxt#;">#ytdLTI#</span></td>
					
				</tr>
				<tr>
					<td class="formlable">AIR</td>
					<td class="formlable" align="center"><cfif gettargets.airgoal eq 0>Zero<cfelse>#gettargets.airgoal#</cfif></td>
					<td bgcolor="#airbg#" align="center" class="bodyTextGrey"><span style="color:#airtxt#;">#ytdair#</span></td>
					
				</tr>
				
			</table>
		</td>
		<td valign="top" align="center" width="28%" id="donut1"></td>
		<td valign="top" align="center"  width="28%" id="donut2"></td>
	</tr>
</table>
<script type="text/javascript">
						FusionCharts.ready(function () {
						    var thisChart = new FusionCharts({
						        "type": "doughnut2d",
						        "renderAt": "donut1",
								<cfif getincclsoed.recordcount gt 0>
						        "width": "375",
						        "height": "375",
								<cfelse>
								"width": "330",
						        "height": "330",
								</cfif>
						        "dataFormat": "xml",
						        "dataSource":  "#donut1xml#"
								
						    });
						
						    thisChart.render();
						
						 var thisChart2 = new FusionCharts({
						        "type": "doughnut2d",
						        "renderAt": "donut2",
						        "width": "375",
						        "height": "357",
						        "dataFormat": "xml",
						        "dataSource":  "#donut2xml#"
								
						    });
						
						    thisChart2.render();
						});
						</script>	
	</cfoutput>	
<script type="text/javascript">
document.getElementById("para1").innerHTML = formatAMPM();

function formatAMPM() {
var d = new Date(),
    minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
    hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
    ampm = d.getHours() >= 12 ? 'PM' : 'AM',
    months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
    days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
return 'Last updated:' + ' ' + days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getFullYear()+' '+hours+':'+minutes;
}
</script>
<br>
