<cfquery name="getgroup" datasource="safety_34">
SELECT    top 20    Group_Number, Group_Name, Group_Address1, OUManager, Tracking_Num, TrackingDate, Group_Description, Geographic_Region, Country_Code, 
                         Orginating_Company, Performing_Company
FROM            Groups
order by Group_Number
</cfquery>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>HSSE Suite</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
	@import '/css/lab.css';
	#body {
		padding:0 20px;
	}

	/* Basic table styling */
	table {
		width:100%;
		border-spacing:0;
		border-collapse:collapse;
	}
	table,
	th,
	td {
		border:0px solid #ccc;
	}
	th,
	td {
		padding:6px 8px;
		vertical-align:top;
	}
	caption,
	th {
		font-weight:bold;
		text-align:left;
	}
	thead th {
		background:#f3f3f3;
		white-space:nowrap;
	}

	/* Scrollable table styling */
	.scrollable.has-scroll {
		position:relative;
		overflow:hidden; /* Clips the shadow created with the pseudo-element in the next rule. Not necessary for the actual scrolling. */
	}
	.scrollable.has-scroll:after {
		position:absolute;
		top:0;
		left:100%;
		width:50px;
		height:100%;
		border-radius:10px 0 0 10px / 50% 0 0 50%;
		box-shadow:-5px 0 10px rgba(0, 0, 0, 0.25);
		content:'';
	}

	/* This is the element whose content will be scrolled if necessary */
	.scrollable.has-scroll > div {
		overflow-x:auto;
	}

	/* Style the scrollbar to make it visible in iOS, Android and OS X WebKit browsers (where user preferences can make scrollbars invisible until you actually scroll) */
	.scrollable > div::-webkit-scrollbar {
		height:12px;
	}
	.scrollable > div::-webkit-scrollbar-track {
		box-shadow:0 0 2px rgba(0,0,0,0.15) inset;
		background:#f0f0f0;
	}
	.scrollable > div::-webkit-scrollbar-thumb {
		border-radius:6px;
		background:#ccc;
	}

	</style>
	<cfoutput><script src="/#getappconfig.oneAIMpath#/shared/js/jquery-1.8.3.min.js"></script></cfoutput>
	<script>
	// Run on window load in case images or other scripts affect element widths
	$(window).on('load', function() {
		// Check all tables. You may need to be more restrictive.
		$(mytab).each(function() {
			var element = $(this);
			// Create the wrapper element
			var scrollWrapper = $('<div />', {
				'class': 'scrollable',
				'html': '<div />' // The inner div is needed for styling
			}).insertBefore(element);
			// Store a reference to the wrapper element
			element.data('scrollWrapper', scrollWrapper);
			// Move the scrollable element inside the wrapper element
			element.appendTo(scrollWrapper.find('div'));
			// Check if the element is wider than its parent and thus needs to be scrollable
			if (element.outerWidth() > element.parent().outerWidth()) {
				element.data('scrollWrapper').addClass('has-scroll');
			}
			// When the viewport size is changed, check again if the element needs to be scrollable
			$(window).on('resize orientationchange', function() {
				if (element.outerWidth() > element.parent().outerWidth()) {
					element.data('scrollWrapper').addClass('has-scroll');
				} else {
					element.data('scrollWrapper').removeClass('has-scroll');
				}
			});
		});
	});
	</script>
</head>
<body>
	<div id="body">
				
		<table id="mytab">
		<thead>
	<tr>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Group Name</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Group Number</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Group Description</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Country</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Address</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Address 2</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Orginating Company</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>Performing Company</strong></td>
	<td  style="font-family:Segoe UI;font-size:9pt;height:25;"><strong>HSE Lead</strong></td>
	
	</tr>
	</thead>
	<tbody>
	<cfoutput query="getgroup">
		<tr <cfif currentrow mod 2 is 0>bgcolor="##dfdfdf"</cfif>>
			<td style="font-family:Segoe UI;font-size:9pt;height:25;">#group_name#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#trackingdate##numberformat(Tracking_Num,"0000")#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#Group_Description#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#Country_Code#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#Group_Address1#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#Group_Address1#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#Orginating_Company#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#Performing_Company#</td>
			<td  style="font-family:Segoe UI;font-size:9pt;height:25;">#OUManager#</td>
			
		</tr>
	</cfoutput>
	</tbody>
</table>
		 
	</div>
</body>
</html>




