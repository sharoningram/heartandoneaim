

<table cellpadding="4" cellspacing="1" bgcolor="ffffff" width="95%">
<tr>
	<td class="bodyTexthd" width="50%"><cfif toggleview eq "dashboard"><cfoutput><a href="/#getappconfig.oneAIMpath#"></cfoutput>My Incidents</a>&nbsp;/&nbsp;Dashboard<cfelse>My Incidents</a>&nbsp;/&nbsp;<a href="index.cfm?fuseaction=main.main&toggleview=dashboard">Dashboard</a></cfif></td>
	<td align="right"><span class="bodytext"  id="para1"></span>&nbsp;<a href="index.cfm?fuseaction=main.main&toggleview=tasks&admlist=yes&showall=yes"><img src="images/recycle.gif" border="0" style="vertical-align:-3px"></a></td>
	<!--- <cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0><td align="right" class="bodytext"><cfif not showall><a href="index.cfm?fuseaction=main.main&showall=yes">Show all tasks</a><cfelse><a href="index.cfm?fuseaction=main.main&showall=no">Show only my tasks</a></cfif></td></cfif> --->
</tr>
<tr>
	<td class="bodyTexthd" width="50%">All Incidents<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0>&nbsp;&nbsp;<span class="bodytext"><cfoutput><a href="/#getappconfig.oneAIMpath#"></cfoutput>My Tasks</a></span></cfif></td>
	<!--- <cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"OU Admin") gt 0 or  listfindnocase(request.userlevel,"Reviewer") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0><td align="right" class="bodytext"><cfif not showall><a href="index.cfm?fuseaction=main.main&showall=yes">Show all tasks</a><cfelse><a href="index.cfm?fuseaction=main.main&showall=no">Show only my tasks</a></cfif></td></cfif> --->
</tr>
</table>

<cfset hastasks = "no">
<cfset showkey = "no">
<table width="95%" cellpadding="2" cellspacing="0" border="0">
	<cfif getpendingsfr.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<cfoutput query="getpendingsfr" group="isfatality">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
				<tr>
					<td colspan="10"><strong class="bodyTextPurple"><cfif isfatality eq "yes">Fatalities<cfelse>Incidents</cfif> not submitted for review</strong></td>
				</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext">#request.oulabellong#</strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput>
					<cfset daccolor = "##ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",dateCreated,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",dateCreated,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",dateCreated,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="##ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",dateCreated,now())#</span></td>
						<td class="bodytext"><cfif isfatality eq "yes"><a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#irn#"><cfelse><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#"></cfif>#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext"><cfif isfatality eq "yes">Fatality<cfelse>#IncType#</cfif><cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif getmoreinfo.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" >
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">Incidents requiring more information</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getmoreinfo">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><cfif isfatality eq "yes"><a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#irn#"><cfelse><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#"></cfif>#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext"><cfif isfatality eq "yes">Fatality<cfelse>#IncType#</cfif><cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
<!--- <cfif   listfindnocase(request.userlevel,"Reviewer") gt 0> --->
	<cfif getsentforreview.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">Incidents awaiting review</strong></td>
					</tr>
					</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getsentforreview">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><!--- <cfif isfatality eq "yes"><a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#irn#"><cfelse> ---><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#"><!--- </cfif> --->#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif><!--- </cfif> ---></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<!--- </cfif> --->
	
	<cfif getfatsentforreview.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">Fatality incidents awaiting review</strong></td>
					</tr>
					</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<!--- <td  width="7%"><strong class="bodytext">OSHA Category</strong></td> --->
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getfatsentforreview">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><cfif isfatality eq "yes"><a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#irn#"><cfelse><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#"></cfif>#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif isfatality eq "yes">Fatality<cfelse>#IncType#</cfif></td>
						<!--- <td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td> --->
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	
	<cfif getneedfirstalert.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">Incidents awaiting First Alert completion</strong></td>
					</tr>
					</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getneedfirstalert">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.firstalert#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center" >#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	
	<cfif getneedfirstalertreview.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">First Alerts pending review</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getneedfirstalertreview">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.firstalert#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateentered,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	
	<cfif getmoreinfoFA.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">First Alerts returned for more information</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getmoreinfoFA">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.firstalert#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateentered,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>

	<cfif getneedfirstalertSRreview.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">First Alerts submitted for approval</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getneedfirstalertSRreview">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.firstalert#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center" >#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateentered,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>

	<cfif needinvestingation.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr >
						<td colspan="8"><strong class="bodyTextPurple">Incidents awaiting investigation information</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="needinvestingation">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.investigation#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",datecreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<cfif needinvestingationreview.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr >
						<td colspan="8"><strong class="bodyTextPurple">Incidents awaiting investigation review</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="needinvestingationreview">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.investigation#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center" >#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",datecreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<cfif needinvestingationmoreinfo.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr >
						<td colspan="8"><strong class="bodyTextPurple">Incident investigations returned for more information</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="needinvestingationmoreinfo">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.investigation#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",datecreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<cfif needirp.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr >
						<td colspan="8"><strong class="bodyTextPurple">Incidents needing IRP completion</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="needirp">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.IRP#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",datecreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<cfif needIRPmoreinfo.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr >
						<td colspan="8"><strong class="bodyTextPurple">IRP needing more information</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="needIRPmoreinfo">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.IRP#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",datecreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<cfif needIRPreview.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr >
						<td colspan="8"><strong class="bodyTextPurple">Incidents needing IRP review</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="needIRPreview">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.IRP#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",datecreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	<cfif getpendingclosure.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8"><strong class="bodyTextPurple">Incidents pending closure</strong></td>
					</tr>
					</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getpendingclosure">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",compdate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",compdate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",compdate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",compdate,now())#</span></td>
						<td class="bodytext"><!--- <cfif isfatality eq "yes"><a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#irn#"><cfelse> ---><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#"><!--- </cfif> --->#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext">#IncType#<cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif><!--- </cfif> ---></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	
	<cfif getwithdrawn.recordcount gt 0>
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
				<tr>
					<td colspan="10"><strong class="bodyTextPurple">Withdrawn incidents</strong></td>
				</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td  width="7%"><strong class="bodytext">Inv. Level</strong></td>
						<td  width="7%"><strong class="bodytext">Incident Type</strong></td>
						<td  width="7%"><strong class="bodytext">OSHA Category</strong></td>
						<td align="center" width="4%"><strong class="bodytext">Near Miss?</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td   width="13%"><strong class="bodytext">Short Description</strong></td>
						<td  width="7%" ><strong class="bodytext">Days Since Open</strong></td>
					</tr>
					<cfoutput query="getwithdrawn">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",withdrawndate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",withdrawndate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",withdrawndate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",withdrawndate,now())#</span></td>
						<td class="bodytext"><cfif isfatality eq "yes"><a href="#self#?fuseaction=#attributes.xfa.fatalityreport#&irn=#irn#"><cfelse><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#"></cfif>#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext"><cfif listfind("1,2",investigationlevel) gt 0>Level #investigationlevel#</cfif></td>
						<td class="bodytext"><cfif isfatality eq "yes">Fatality<cfelse>#IncType#</cfif><cfif trim(sectype) neq ''>/#sectype#</cfif></td>
						<td class="bodytext"><cfif trim(category) neq ''>#category#</cfif></td>
						<td class="bodytext" align="center">#isNearMiss#</td>
						<td class="bodytext"  align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
						<td class="bodytext" align="center">#datediff("d",dateCreated,now())#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfif>
	
<cfif needLTIdate.recordcount gt 0>
<cfset hastasks = "yes">
	<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8" class="bodyTextPurple" bgcolor="ffff00"><strong class="bodyTextPurple">Lost Time Injury incidents awaiting return to work date</strong> (days will be capped at 180 as per OSHA requirements)</td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Lost to Date</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td align="center" width="12%"><strong class="bodytext">1st Date of Absence</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td  ><strong class="bodytext">Short Description</strong></td>
										
					</tr>
					<cfoutput query="needLTIdate">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<!--- <cfif datediff("d",LTIFirstDate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",LTIFirstDate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",LTIFirstDate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif> --->
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#"><cfif datediff("d",LTIFirstDate,now()) gte 180>180<cfelse>#datediff("d",LTIFirstDate,now())#</cfif></span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext">#dateformat(LTIFirstDate,sysdateformat)#</td>
						<td class="bodytext" align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
					
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	
	
</cfif>
<cfif needRWCdate.recordcount gt 0>
	<cfset hastasks = "yes">
		
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="8" class="bodyTextPurple" bgcolor="ffff00"><strong class="bodyTextPurple">Restricted Work Cases awaiting date returned to full duty</td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">No. days restricted to date</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext"><cfoutput>#request.oulabellong#</cfoutput></strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Incident Date</strong></td>
						<td align="center" width="12%"><strong class="bodytext">1st Date of Absence</strong></td>
						<td align="center"  width="4%"><strong class="bodytext">Potential Rating</strong></td>
						<td ><strong class="bodytext">Short Description</strong></td>
										
					</tr>
					<cfoutput query="needRWCdate">
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<!--- <cfif datediff("d",LTIFirstDate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",LTIFirstDate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",LTIFirstDate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif> --->
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",RWCFirstDate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.incidentreport#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(incidentDate,sysdateformat)#</td>
						<td class="bodytext">#dateformat(RWCFirstDate,sysdateformat)#</td>
						<td class="bodytext" align="center">#PotentialRating#</td>
						<td class="bodytext">#ShortDesc#</td>
					
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	
	
</cfif>
<cfif getincompletecapa.recordcount gt 0>
	<cfoutput query="getincompletecapa" group="CAPAtype">
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="7"><strong class="bodyTextPurple">#capatype# actions pending completion</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="18%"><strong class="bodytext">#request.oulabellong#</strong></td>
						<td width="18%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Due Date</strong></td>
						<td width="20%"><strong class="bodytext">Person Responsible</strong></td>
						<td><strong class="bodytext">Details of action required</strong></td>
						
					</tr>
					<cfoutput>
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",capadate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",capadate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",capadate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",capadate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.capa#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(DueDate,sysdateformat)#</td>
						<td class="bodytext">#AssignedTo#</td>
						<td class="bodytext">#ActionDetail#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfoutput>
	
</cfif>
<cfif getcapaoverdue.recordcount gt 0>
	<cfoutput query="getcapaoverdue" group="CAPAtype">
		<cfset showkey = "yes">
		<cfset hastasks = "yes">
		<tr>
			<td>
				<table cellspacing="1" cellpadding="4" border="0" width="100%">
					<tr>
						<td colspan="6"><strong class="bodyTextPurple">#capatype# actions past due date</strong></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="4" border="0" width="100%" bgcolor="cfcfcf">
					<tr bgcolor="ffffff">
						<td width="8%"><strong class="bodytext">Days Awaiting Action</strong></td>
						<td width="8%"><strong class="bodytext">Incident No.</strong></td>
						<td width="14%"><strong class="bodytext">#request.oulabellong#</strong></td>
						<td width="14%"><strong class="bodytext">Project/Office</strong></td>
						<td align="center" width="8%"><strong class="bodytext">Due Date</strong></td>
						<td width="20%"><strong class="bodytext">Person Responsible</strong></td>
						<td><strong class="bodytext">Details of action required</strong></td>
						
					</tr>
					<cfoutput>
					<cfset daccolor = "ffffff">
					<cfset dactxt = "##000000">
					<cfif datediff("d",DueDate,now()) lte 2>
						<cfset daccolor = "##00b050">
						<cfset dactxt = "##ffffff">
					<cfelseif listfind("3,4,5",datediff("d",DueDate,now())) gt 0>
						<cfset daccolor = "##ffc000">
						<cfset dactxt = "##000000">
					<cfelseif datediff("d",DueDate,now()) gt 5>
						<cfset daccolor = "##ff0000">
						<cfset dactxt = "##ffffff">
					</cfif>
					<tr bgcolor="ffffff">
						<td class="bodytext" align="center" bgcolor="#daccolor#"><span style="color:#dactxt#">#datediff("d",DueDate,now())#</span></td>
						<td class="bodytext"><a href="#self#?fuseaction=#attributes.xfa.capa#&irn=#irn#">#TrackingNum#</a></td>
						<td class="bodytext">#ouname#</td>
						<td class="bodytext">#Group_Name#</td>
						<td class="bodytext" align="center">#dateformat(DueDate,sysdateformat)#</td>
						<td class="bodytext">#AssignedTo#</td>
						<td class="bodytext">#ActionDetail#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</cfoutput>
	
</cfif>
</table>

<cfif not hastasks>
<table cellpadding="3" cellspacing="1" border="0">
	<tr>
		<td class="bodyTextlg">&nbsp;&nbsp;There are no outstanding incidents</td>
	</tr>
</table>
</cfif>
<cfif showkey>
<table cellpadding="3" cellspacing="1" border="0">
	<tr>
		<td bgcolor="00b050" class="bodytextwhite">Awaiting action for 0-2 days</td>
	</tr>
	<tr>
		<td bgcolor="ffc000" class="bodytext">Awaiting action for 2-5 days</td>
	</tr>
	<tr>
		<td bgcolor="ff0000" class="bodytextwhite">Awaiting action for 5+ days</td>
	</tr>
</table>
</cfif>
<script type="text/javascript">
document.getElementById("para1").innerHTML = formatAMPM();

function formatAMPM() {
var d = new Date(),
    minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
    hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
    ampm = d.getHours() >= 12 ? 'PM' : 'AM',
    months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
    days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
return 'Last updated:' + ' ' + days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getFullYear()+' '+hours+':'+minutes;
}
</script>