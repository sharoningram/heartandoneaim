


<cfswitch expression = "#fusebox.fuseaction#">
<cfcase value="incidentreport">
	<cfset attributes.xfa.incidentmanament = "incidents.incidentmanament">
	<cfset attributes.xfa.drawdropchart = "incidents.drawdropchart">
	<cfset attributes.xfa.popgroupinfo = "incidents.popgroupinfo">
	<cfset attributes.xfa.getconnames = "incidents.getconnames">
	<cfset attributes.xfa.fieldaudit = "incidents.fieldaudit">
	<cfset attributes.xfa.addcontractor = "incidents.addcontractor">
	<cfinclude template="queries/qry_getgroups.cfm">
	<cfinclude template="queries/qry_getinctypes.cfm">
	<cfinclude template="queries/qry_getincplaces.cfm">
	<cfinclude template="queries/qry_getgweather.cfm">
	<cfinclude template="queries/qry_getincassignto.cfm">
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getInjurySources.cfm">
	<cfinclude template="queries/qry_getOccupations.cfm">
	<cfinclude template="queries/qry_getAges.cfm">
	<cfinclude template="queries/qry_getextents.cfm">
	<cfinclude template="queries/qry_getinjurytypes.cfm">
	<cfinclude template="queries/qry_getbodyparts.cfm">
	<cfinclude template="queries/qry_getinjurycause.cfm">
	<cfinclude template="queries/qry_getillnesstypes.cfm">
	<cfinclude template="queries/qry_getillnessnature.cfm">
	<cfinclude template="queries/qry_getassetdamagesrc.cfm">
	<cfinclude template="queries/qry_getassetdamagenature.cfm">
	<cfinclude template="queries/qry_getpropertytypes.cfm">
	<cfinclude template="queries/qry_getcurrencies.cfm">
	<cfinclude template="queries/qry_geteievents.cfm">
	<cfinclude template="queries/qry_getpollET.cfm">
	<cfinclude template="queries/qry_getpollsubstance.cfm">
	<cfinclude template="queries/qry_getreleaseunit.cfm">
	<cfinclude template="queries/qry_getreleasesrc.cfm">
	<cfinclude template="queries/qry_getreleaseduration.cfm">
	<cfinclude template="queries/qry_getenviros.cfm">
	<cfinclude template="queries/qry_getsecinccats.cfm">
	<cfinclude template="queries/qry_getsecthreats.cfm">
	<cfinclude template="queries/qry_getsecnature.cfm">
	<cfinclude template="queries/qry_getcountries.cfm">
	<cfinclude template="queries/qry_getinfocats.cfm">
	<cfinclude template="queries/qry_getweapons.cfm">
	<cfinclude template="queries/qry_getoidef.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentreviewers.cfm">
	<cfinclude template="queries/qry_getincidentadvisors.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="queries/qry_getincidentenv.cfm">
	<cfinclude template="queries/qry_getincidentasset.cfm">
	<cfinclude template="queries/qry_getincidentsecurity.cfm">
	<cfinclude template="queries/qry_getincidentcomments.cfm">
	<cfinclude template="queries/qry_getincidentfieldcomments.cfm">
	<cfinclude template="queries/qry_getpropdamage.cfm">
	<cfinclude template="queries/qry_getpollutionevents.cfm">
	<cfinclude template="queries/qry_getfadetail.cfm">
	<cfset showfrm = "no">
	
		<cfif trim(getincidentdetails.status) eq '' or listfindnocase("Started,Initiated,More Info Requested",getincidentdetails.status) gt 0>
			
			<cfif  listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
				<cfset showfrm = "yes">
			<cfelseif listfindnocase(request.userlevel,"User") gt 0>
				<cfif getincidentdetails.createdbyEmail eq request.userlogin or trim(getincidentdetails.createdbyEmail) eq ''>
					<cfset showfrm = "yes">
				<cfelse>	
					<cfif listfind(valuelist(getugnum.group_number),getincidentdetails.groupnumber) gt 0>
						<cfset showfrm = "yes">
					</cfif>
				</cfif>
			</cfif>
		
		</cfif>
		<cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
			<cfif request.userid eq getincidentdetails.withdrawnto>
				<cfset showfrm = "yes">
			<cfelse>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
					<cfset showfrm = "yes">
				<cfelseif  listfindnocase(request.userlevel,"User") gt 0 and listfind(valuelist(getugnum.group_number),getincidentdetails.groupnumber) gt 0>
					<cfset showfrm = "yes">
				</cfif>
			</cfif>
		</cfif>
		
		<cfif showfrm>
			<cfinclude template="forms/frm_firstincident.cfm">
			<!--- <cfinclude template="forms/frm_firstincident_walerts.cfm"> --->
		<cfelse>
			<cfinclude template="displays/dsp_firstincident.cfm">
		</cfif>
	<!--- <cfif getincidentdetails.status neq "Cancel">
		<cfif trim(getincidentdetails.status) eq '' or listfindnocase("Started,Initiated,Withdrawn to Initiator,More Info Requested",getincidentdetails.status) gt 0>
			<cfinclude template="forms/frm_firstincident.cfm">
		<cfelse>
			<cfinclude template="displays/dsp_firstincident.cfm">
		</cfif>
	<cfelse>
		<cfinclude template="displays/dsp_firstincident.cfm">
	</cfif> --->
	
		
		
	<!--- <cfelse>
		<cfinclude template="displays/dsp_firstincident.cfm">
	</cfif> --->
<!--- 	<cfinclude template="forms/frm_injury.cfm">
	<cfinclude template="forms/frm_environmental.cfm">
	<cfinclude template="forms/frm_property.cfm">
	<cfinclude template="forms/frm_security.cfm">
	<cfinclude template="forms/frm_occhealth.cfm"> --->
</cfcase>
<cfcase value="exportincpdf">
	<cfset attributes.xfa.drawdropchart = "incidents.drawdropchart">
	<cfinclude template="queries/qry_getgroups.cfm">
	<cfinclude template="queries/qry_getinctypes.cfm">
	<cfinclude template="queries/qry_getincplaces.cfm">
	<cfinclude template="queries/qry_getgweather.cfm">
	<cfinclude template="queries/qry_getincassignto.cfm">
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getInjurySources.cfm">
	<cfinclude template="queries/qry_getOccupations.cfm">
	<cfinclude template="queries/qry_getAges.cfm">
	<cfinclude template="queries/qry_getextents.cfm">
	<cfinclude template="queries/qry_getinjurytypes.cfm">
	<cfinclude template="queries/qry_getbodyparts.cfm">
	<cfinclude template="queries/qry_getinjurycause.cfm">
	<cfinclude template="queries/qry_getillnesstypes.cfm">
	<cfinclude template="queries/qry_getillnessnature.cfm">
	<cfinclude template="queries/qry_getassetdamagesrc.cfm">
	<cfinclude template="queries/qry_getassetdamagenature.cfm">
	<cfinclude template="queries/qry_getpropertytypes.cfm">
	<cfinclude template="queries/qry_getcurrencies.cfm">
	<cfinclude template="queries/qry_geteievents.cfm">
	<cfinclude template="queries/qry_getpollET.cfm">
	<cfinclude template="queries/qry_getpollsubstance.cfm">
	<cfinclude template="queries/qry_getreleaseunit.cfm">
	<cfinclude template="queries/qry_getreleasesrc.cfm">
	<cfinclude template="queries/qry_getreleaseduration.cfm">
	<cfinclude template="queries/qry_getenviros.cfm">
	<cfinclude template="queries/qry_getsecinccats.cfm">
	<cfinclude template="queries/qry_getsecthreats.cfm">
	<cfinclude template="queries/qry_getsecnature.cfm">
	<cfinclude template="queries/qry_getcountries.cfm">
	<cfinclude template="queries/qry_getinfocats.cfm">
	<cfinclude template="queries/qry_getweapons.cfm">
	<cfinclude template="queries/qry_getoidef.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentreviewers.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="queries/qry_getincidentenv.cfm">
	<cfinclude template="queries/qry_getincidentasset.cfm">
	<cfinclude template="queries/qry_getincidentsecurity.cfm">
	<cfinclude template="queries/qry_getincidentcomments.cfm">
	<cfinclude template="queries/qry_getincidentfieldcomments.cfm">
	<cfinclude template="queries/qry_getpropdamage.cfm">
	<cfinclude template="queries/qry_getpollutionevents.cfm">
	<cfinclude template="queries/qry_getfadetail.cfm">
	<cfinclude template="actions/act_checkforinv.cfm">
	<cfinclude template="queries/qry_getrules.cfm">
	<cfinclude template="queries/qry_getessentials.cfm">
	<cfinclude template="queries/qry_getAZ.cfm">
	<cfinclude template="queries/qry_getAZcategories.cfm">
	<cfinclude template="queries/qry_getAZfactors.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getinvcomments.cfm">
	<cfinclude template="actions/act_buildazstructs.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getcapadetail.cfm">
	<cfinclude template="queries/qry_getcapalist.cfm">
	<cfinclude template="queries/qry_getincaudits.cfm">
	<cfinclude template="actions/act_whocansee.cfm">
	<cfinclude template="queries/qry_getincazirp.cfm">
	<cfinclude template="actions/act_exportincident.cfm">
</cfcase>
<cfcase value="incidentcomments">
	<cfset attributes.xfa.incidentcomments = "incidents.incidentcomments">
	<cfinclude template="actions/act_addcomment.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentcommenttab.cfm">
	<cfinclude template="forms/frm_addincidentcommenttab.cfm">
</cfcase>
<cfcase value="fieldaudit">
	<cfinclude template="queries/qry_getfieldaudit.cfm">
	<cfinclude template="displays/dsp_fieldaudit.cfm">
</cfcase>
<cfcase value="addcontractor">
	<cfset attributes.xfa.addcontractor = "incidents.addcontractor">
	<cfinclude template="queries/qry_getinccondetail.cfm">
	<cfinclude template="actions/act_addcontractor.cfm">
	<cfinclude template="forms/frm_addcontractor.cfm">
</cfcase>
<cfcase value="incidentmanament">
	<cfset attributes.xfa.incidentreport = "incidents.incidentreport">
	<cfset attributes.xfa.fatalityreport = "incidents.fatalityreport">
	<cfinclude template="actions/act_manageincident.cfm">
</cfcase>
<cfcase value="drawdropchart">
	<cfinclude template="actions/act_drawdropchart.cfm">
</cfcase>
<cfcase value="popgroupinfo">
	<cfinclude template="actions/act_popgroupinfo.cfm">
</cfcase>
<cfcase value="getconnames">
	<cfinclude template="actions/act_getconnames.cfm">
</cfcase>
<cfcase value="firstalert">
	<cfset attributes.xfa.famanagement = "incidents.famanagement">
	<cfinclude template="queries/qry_getgroups.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getfadetail.cfm">
	<cfinclude template="queries/qry_getincidentoiFA.cfm">
	<cfinclude template="queries/qry_getincidentenvFA.cfm">
	<cfinclude template="queries/qry_getincidenttypeFA.cfm">
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getincidentfieldcommentsFA.cfm">
	<cfinclude template="queries/qry_getFAcomments.cfm"><!--- getincidentdetails.investigationlevel eq 2 or  --->
	<cfinclude template="queries/qry_getFAbp.cfm">
	<cfif getincidentdetails.needFA eq 1>
	<cfif trim(getFAdetail.status) eq '' or listfindnocase("Started,More Info Requested,SR More Info",trim(getFAdetail.status)) gt 0>
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or getincidentdetails.createdbyEmail eq request.userlogin  or listfindnocase(request.userlevel,"OU Admin") gt 0 >
			<cfinclude template="forms/frm_firstalert.cfm">
		<cfelseif listfindnocase(request.userlevel,"User") gt 0 and listlen(request.userlevel) eq 1 and listfind(valuelist(getugnum.group_number),getincidentdetails.groupnumber) gt 0>
			
				<cfinclude template="forms/frm_firstalert.cfm">
			
		<cfelseif  listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU View Only") gt 0 or listfindnocase(request.userlevel,"BU View Only") gt 0>
			<cfinclude template="displays/dsp_firstalert.cfm">
		</cfif>
	<cfelse>
		<cfif getFAdetail.status eq "Sent for Review">
			<cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
				<cfif request.userid eq getincidentdetails.withdrawnto>
					<cfinclude template="forms/frm_firstalert.cfm">
				<cfelse>
					<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0>
						<cfinclude template="forms/frm_firstalert.cfm">
					<Cfelse>
						<cfinclude template="displays/dsp_firstalert.cfm">
					</cfif>
				</cfif>
			<cfelse>
				<cfinclude template="displays/dsp_firstalert.cfm">
			</cfif>
		<cfelse>
			<cfinclude template="displays/dsp_firstalert.cfm">
		</cfif>
	</cfif>
	<cfelse>
		<cfif getFAdetail.wasissued eq 1>
			<cfinclude template="displays/dsp_firstalert.cfm">
		</cfif>
	</cfif>
</cfcase>
<cfcase value="fapdf">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getfadetail.cfm">
	<cfinclude template="queries/qry_getincidentoiFA.cfm">
	<cfinclude template="queries/qry_getincidentenvFA.cfm">
	<cfinclude template="queries/qry_getincidenttypeFA.cfm">
	
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getincidentfieldcommentsFA.cfm">
	<cfinclude template="queries/qry_getFAcomments.cfm">
	<cfinclude template="queries/qry_getFAbp.cfm">
	<cfinclude template="displays/dsp_fapdf.cfm">
</cfcase>
<cfcase value="famanagement">
	<cfset attributes.xfa.firstalert = "incidents.firstalert">
	<cfinclude template="actions/act_firstalert.cfm">
</cfcase>
<cfcase value="adddateval">
	<cfset attributes.xfa.adddateval = "incidents.adddateval">
	<cfinclude template="queries/qry_getdateval.cfm">
	<cfinclude template="actions/act_managedateval.cfm">
	<cfinclude template="forms/frm_dateval.cfm">
</cfcase>
<cfcase value="investigation">
	<!--- <cfset attributes.xfa.manageinvestigation = "incidents.manageinvestigation">
	<cfset attributes.xfa.createsublists = "incidents.createsublists">
	<cfinclude template="actions/act_checkforinv.cfm">
	<cfinclude template="queries/qry_getrules.cfm">
	<cfinclude template="queries/qry_getessentials.cfm">
	<cfinclude template="queries/qry_getAZ.cfm">
	<cfinclude template="forms/frm_investigation.cfm"> --->
	<cfset attributes.xfa.manageinvestigation = "incidents.manageinvestigation">
	<cfset attributes.xfa.createsublists = "incidents.createsublists">
	
	<cfinclude template="actions/act_checkforinv.cfm">
	<cfinclude template="queries/qry_getgroups.cfm">
	<cfinclude template="queries/qry_getrules.cfm">
	<cfinclude template="queries/qry_getessentials.cfm">
	<cfinclude template="queries/qry_getAZ.cfm">
	<cfinclude template="queries/qry_getAZcategories.cfm">
	<cfinclude template="queries/qry_getAZfactors.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getinvcomments.cfm">
	<cfinclude template="actions/act_buildazstructs.cfm">
	<cfif trim(getinvdetail.status) eq '' or trim(getinvdetail.status) eq "Started" or trim(getinvdetail.status) eq "More Info Requested">
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"User") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
			<cfif listfindnocase(request.userlevel,"User") gt 0>
				<cfif getincidentdetails.createdbyEmail eq request.userlogin>
					<cfinclude template="forms/frm_investigation3.cfm">
				<cfelse>
					<cfif listfind(valuelist(getugnum.group_number),getincidentdetails.groupnumber) gt 0>
						<cfinclude template="forms/frm_investigation3.cfm">
					<cfelse>
						<cfinclude template="displays/dsp_investigation3.cfm">
					</cfif>
				</cfif>
			<cfelse>
				<cfinclude template="forms/frm_investigation3.cfm">
			</cfif>
		<cfelse>
			<cfinclude template="displays/dsp_investigation3.cfm">
		</cfif>
	<cfelseif trim(getinvdetail.status) eq "Senior More Info Requested">
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
			<cfinclude template="forms/frm_investigation3.cfm">
		<cfelse>
			<cfinclude template="displays/dsp_investigation3.cfm">
		</cfif>
	<cfelse>
		<cfif listfind("1,2",getincidentdetails.withdrawnstatus) gt 0>
			<cfif request.userid eq getincidentdetails.withdrawnto>
				<cfinclude template="forms/frm_investigation3.cfm">
			<cfelse>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
						<cfinclude template="forms/frm_investigation3.cfm">
					<cfelse>
						<cfinclude template="displays/dsp_investigation3.cfm">
					</cfif>
			</cfif>
		<cfelse>
			<cfinclude template="displays/dsp_investigation3.cfm">
		</cfif>
	</cfif>
</cfcase>
<cfcase value="reviewinvestigation">
	<cfset attributes.xfa.investigation = "incidents.investigation">
	<cfinclude template="actions/act_reviewinvestigation.cfm">
</cfcase>
<cfcase value="raiseCAPA">
	<cfset attributes.xfa.manageCAPA = "incidents.manageCAPA">
	<cfinclude template="forms/frm_raisecapa.cfm">
</cfcase>

<cfcase value="createsublists">
	<cfinclude template="actions/act_createsublists.cfm">
</cfcase>
<cfcase value="manageinvestigation">
	<cfset attributes.xfa.investigation = "incidents.investigation">
	<cfinclude template="actions/act_manageinvestigation.cfm">
</cfcase>
<cfcase value="IRP">
	<cfset attributes.xfa.manageIRP = "incidents.manageIRP">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="queries/qry_getincidenttypeFA.cfm">
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getincazirp.cfm">
	<cfinclude template="queries/qry_getirpcomments.cfm">
	<cfinclude template="queries/qry_getIRPdetail.cfm">
	<cfif getincidentdetails.needIRP eq 1>
	<cfif getirpdetail.status neq "Complete">
		
		<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"User") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
			<cfif trim(getirpdetail.status) eq '' or getirpdetail.status eq "Started" or getirpdetail.status eq "More Info Requested">
				<cfinclude template="forms/frm_IRP.cfm">
			<cfelse>
				<cfinclude template="displays/dsp_IRP.cfm">
			</cfif>
		<cfelse>
			<cfinclude template="displays/dsp_IRP.cfm">
		</cfif>
	<cfelse>
		<cfinclude template="displays/dsp_IRP.cfm">
	</cfif>
	<cfelse>
		<cfif getirpdetail.wasissued eq 1>
			<cfinclude template="displays/dsp_IRP.cfm">
		</cfif>
	</cfif>
</cfcase>
<cfcase value="reviewirn">
	<cfset attributes.xfa.incidentmanament= "incidents.incidentmanament">
	<cfset attributes.xfa.managereview= "incidents.managereview">
	<cfset attributes.xfa.famanagement = "incidents.famanagement">
	<cfset attributes.xfa.reviewinvestigation = "incidents.reviewinvestigation">
	<cfset attributes.xfa.manageIRP = "incidents.manageIRP">
	<cfinclude template="queries/qry_getincidentforreview.cfm">
	<cfinclude template="queries/qry_getinvstatus.cfm">
	<!--- <cfinclude template="queries/qry_getreviewfactors.cfm"> --->
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="forms/frm_reviewincident.cfm">
	<!--- <cfinclude template="forms/frm_withdraw.cfm"> --->
</cfcase>
<cfcase value="cancel">
	<cfset attributes.xfa.incidentmanament= "incidents.incidentmanament">
	
	<!cfinclude template="queries/qry_getincidentforreview.cfm">
	<!--- <cfinclude template="queries/qry_getreviewfactors.cfm"> --->
	<!cfinclude template="queries/qry_getOSHAcats.cfm">
	<!cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="forms/frm_cancel.cfm">
	<!--- <cfinclude template="forms/frm_withdraw.cfm"> --->
</cfcase>
<cfcase value="managereview">
	<cfinclude template="actions/act_managereview.cfm">
</cfcase>
<cfcase value="manageIRP">
	<cfset attributes.xfa.IRP = "incidents.IRP">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="queries/qry_getincidenttypeFA.cfm">
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getincazirp.cfm">
	<cfinclude template="actions/act_managerIRP.cfm">
</cfcase>
<cfcase value="CAPA">
	<cfset attributes.xfa.capa = "incidents.capa">
	<cfset attributes.xfa.manageCAPA = "incidents.manageCAPA">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getcapadetail.cfm">
	<cfinclude template="queries/qry_getcapalist.cfm">
	<cfinclude template="forms/frm_CAPA.cfm">
</cfcase>
<cfcase value="manageCAPA">
	<cfset attributes.xfa.CAPA = "incidents.CAPA">
	<cfinclude template="actions/act_CAPA.cfm">
</cfcase>
<cfcase value="audit">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincaudits.cfm">
	<cfinclude template="actions/act_whocansee.cfm">
	<cfinclude template="displays/dsp_incaudit.cfm">
</cfcase>
<cfcase value="fatalityreport">
	<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0> 
	<cfset attributes.xfa.incidentmanament = "incidents.incidentmanament">
	<cfset attributes.xfa.drawdropchart = "incidents.drawdropchart">
	<cfset attributes.xfa.popgroupinfo = "incidents.popgroupinfo">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentreviewers.cfm">
	<cfinclude template="queries/qry_getgroups.cfm">
	<cfinclude template="queries/qry_getincassignto.cfm">
<!--- 	<cfinclude template="queries/qry_getinctypes.cfm">
	<cfinclude template="queries/qry_getincplaces.cfm">
	<cfinclude template="queries/qry_getgweather.cfm">
	
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getInjurySources.cfm">
	<cfinclude template="queries/qry_getOccupations.cfm">
	<cfinclude template="queries/qry_getAges.cfm">
	<cfinclude template="queries/qry_getextents.cfm">
	<cfinclude template="queries/qry_getinjurytypes.cfm">
	<cfinclude template="queries/qry_getbodyparts.cfm">
	<cfinclude template="queries/qry_getinjurycause.cfm">
	<cfinclude template="queries/qry_getillnesstypes.cfm">
	<cfinclude template="queries/qry_getillnessnature.cfm">
	<cfinclude template="queries/qry_getassetdamagesrc.cfm">
	<cfinclude template="queries/qry_getassetdamagenature.cfm">
	<cfinclude template="queries/qry_getpropertytypes.cfm">
	<cfinclude template="queries/qry_getcurrencies.cfm">
	<cfinclude template="queries/qry_geteievents.cfm">
	<cfinclude template="queries/qry_getpollET.cfm">
	<cfinclude template="queries/qry_getpollsubstance.cfm">
	<cfinclude template="queries/qry_getreleaseunit.cfm">
	<cfinclude template="queries/qry_getreleasesrc.cfm">
	<cfinclude template="queries/qry_getreleaseduration.cfm">
	<cfinclude template="queries/qry_getenviros.cfm">
	<cfinclude template="queries/qry_getsecinccats.cfm">
	<cfinclude template="queries/qry_getsecthreats.cfm">
	<cfinclude template="queries/qry_getsecnature.cfm">
	<cfinclude template="queries/qry_getcountries.cfm">
	<cfinclude template="queries/qry_getinfocats.cfm">
	<cfinclude template="queries/qry_getweapons.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="queries/qry_getincidentenv.cfm">
	<cfinclude template="queries/qry_getincidentasset.cfm">
	<cfinclude template="queries/qry_getincidentsecurity.cfm">--->
	<cfinclude template="queries/qry_getincidentcomments.cfm"> 
	<cfinclude template="queries/qry_getFATteam.cfm">
	<cfif trim(getincidentdetails.status) eq '' or listfindnocase("Started,Initiated,More Info Requested",getincidentdetails.status) gt 0>
		<cfinclude template="forms/frm_fatality.cfm">
	<cfelse>
		<cfinclude template="displays/dsp_fatality.cfm">
	</cfif>
	</cfif>
</cfcase>

<cfcase value="exporttoexcelinc">
	<cfparam name="url.ou" default="0">
	<cfset attributes.xfa.drawdropchart = "incidents.drawdropchart">
	<cfset mainctr = 1>
	<cfset injctr = 1>
	<cfset assetctr = 1>
	<cfset envctr = 1>
	<cfset secctr = 1>
	<cfif url.ou gt 0>
<cfinclude template="actions/act_createbaseexcel.cfm">
	
	
	
<cfloop list="#irnlist#" index="irn">
	<cfinclude template="queries/qry_getgroups.cfm">
	<cfinclude template="queries/qry_getinctypes.cfm">
	<cfinclude template="queries/qry_getincplaces.cfm">
	<cfinclude template="queries/qry_getgweather.cfm">
	<cfinclude template="queries/qry_getincassignto.cfm">
	<cfinclude template="queries/qry_getOSHAcats.cfm">
	<cfinclude template="queries/qry_getInjurySources.cfm">
	<cfinclude template="queries/qry_getOccupations.cfm">
	<cfinclude template="queries/qry_getAges.cfm">
	<cfinclude template="queries/qry_getextents.cfm">
	<cfinclude template="queries/qry_getinjurytypes.cfm">
	<cfinclude template="queries/qry_getbodyparts.cfm">
	<cfinclude template="queries/qry_getinjurycause.cfm">
	<cfinclude template="queries/qry_getillnesstypes.cfm">
	<cfinclude template="queries/qry_getillnessnature.cfm">
	<cfinclude template="queries/qry_getassetdamagesrc.cfm">
	<cfinclude template="queries/qry_getassetdamagenature.cfm">
	<cfinclude template="queries/qry_getpropertytypes.cfm">
	<cfinclude template="queries/qry_getcurrencies.cfm">
	<cfinclude template="queries/qry_geteievents.cfm">
	<cfinclude template="queries/qry_getpollET.cfm">
	<cfinclude template="queries/qry_getpollsubstance.cfm">
	<cfinclude template="queries/qry_getreleaseunit.cfm">
	<cfinclude template="queries/qry_getreleasesrc.cfm">
	<cfinclude template="queries/qry_getreleaseduration.cfm">
	<cfinclude template="queries/qry_getenviros.cfm">
	<cfinclude template="queries/qry_getsecinccats.cfm">
	<cfinclude template="queries/qry_getsecthreats.cfm">
	<cfinclude template="queries/qry_getsecnature.cfm">
	<cfinclude template="queries/qry_getcountries.cfm">
	<cfinclude template="queries/qry_getinfocats.cfm">
	<cfinclude template="queries/qry_getweapons.cfm">
	<cfinclude template="queries/qry_getoidef.cfm">
	<cfinclude template="queries/qry_getincidentdetail.cfm">
	<cfinclude template="queries/qry_getincidentreviewers.cfm">
	<cfinclude template="queries/qry_getincidentoi.cfm">
	<cfinclude template="queries/qry_getincidentenv.cfm">
	<cfinclude template="queries/qry_getincidentasset.cfm">
	<cfinclude template="queries/qry_getincidentsecurity.cfm">
	<cfinclude template="queries/qry_getincidentcomments.cfm">
	<cfinclude template="queries/qry_getincidentfieldcomments.cfm">
	<cfinclude template="queries/qry_getpropdamage.cfm">
	<cfinclude template="queries/qry_getpollutionevents.cfm">
	<cfinclude template="actions/act_exportincidenttoexcel.cfm">
</cfloop>
<cfspreadsheet action="update" filename="#thefile#" name="incSheet" sheetname="Incident Details">  
 <cfspreadsheet action="update" filename="#thefile#" name="injuryOISheet"  sheetname="Injury - Occupation Illness">  
 <cfspreadsheet action="update" filename="#thefile#" name="envSheet"  sheetname="Environmental">  
 <cfspreadsheet action="update" filename="#thefile#" name="adSheet"  sheetname="Asset Damage">  
 <cfspreadsheet action="update" filename="#thefile#" name="secSheet"  sheetname="Security"> 
<cflocation addtoken="No" url="/#getappconfig.oneAIMpath#/incidents/actions/pdfgen/#thefilename#">
	</cfif>
</cfcase>




<cfdefaultcase>
	<cfoutput>
       I received a fuseaction called <B><FONT COLOR="000066">"#fusebox.fuseaction#"</FONT></B> that circuit <B><FONT COLOR="000066">"#fusebox.circuit#"</FONT></B> doesn't have a handler for.
	</cfoutput>
</cfdefaultcase>

</cfswitch>
