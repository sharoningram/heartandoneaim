<cfparam name="closesave" default="no">
<cfsetting enablecfoutputonly="No" requesttimeout="4800">
<cfparam name="submittype" default="">
<cfparam name="irn" default="0">
<cfparam name="incidentlearning" default="">
<cfparam name="supportinginfo" default="">
<cfparam name="iscomplete" default="no">
<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_managerIRP.cfm", "uploads\incidents\#irn#\IRP"))>
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_managerIRP.cfm", "uploads\incidents\#irn#\IRPPDF"))>
<cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "act_managerIRP.cfm", ""))>				
<cfswitch expression="#submittype#">
	<cfcase value="addirp">
	<cfset imgmsg = "none">
		<cfif trim(irn) neq 0>
			
			
			<cfquery name="checkfordupe" datasource="#request.dsn#">
				select IRPid
				from oneAIMIncidentIRP
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			
			<cfif checkfordupe.recordcount eq 0>
				<cfif iscomplete eq "yes">
					<cfset newstat = "Sent for Review">
				<Cfelse>
					<cfset newstat = "Started">
				</cfif>
			
				<cfquery name="addirp" datasource="#request.dsn#">
					insert into oneAIMIncidentIRP(IRN, IncidentLearning, SupportingInfo, Status)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incidentlearning#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#supportinginfo#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newstat#">)	
				</cfquery>
				
				
				 
				<cfif not directoryexists("#FileDir#")>
					<cfdirectory action="CREATE" directory="#FileDir#">
				</cfif>
			
				<cfif trim(UPLOADDOC1) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC1" destination="#FileDir#" nameconflict="OVERWRITE">
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
				
				<cfif trim(UPLOADDOC2) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC2" destination="#FileDir#" nameconflict="OVERWRITE">
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
				
				<cfif trim(UPLOADDOC3) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC3" destination="#FileDir#" nameconflict="OVERWRITE">
			<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
				
				<cfif trim(UPLOADDOC4) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC4" destination="#FileDir#" nameconflict="OVERWRITE">
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
			
				<cfif trim(UPLOADDOC5) neq ''>
					<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC5" destination="#FileDir#" nameconflict="OVERWRITE">
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
			
		
			
			
			<cfquery name="getirpdetail" datasource="#request.dsn#">
				SELECT   IRPid, IRN, IncidentLearning, SupportingInfo, Status
				FROM            oneAIMIncidentIRP
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#IRN#">
			</cfquery>
			<cfdocument format="PDF" filename="IRP_#getincidentdetails.trackingnum#.pdf"  overwrite="yes">
				<cfinclude template="/#getappconfig.oneAIMpath#/incidents/displays/dsp_irppdf.cfm">
			</cfdocument>
			 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
			
			
			
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfset ctr = 0>
					<cfloop query="getincfileslist">
						<cfif left(name,6) neq ".~lock">
						<cfset ctr = ctr+1>
						<cfif not isImageFile("#filedir#\#name#")>
							<cfif right(name,4) eq ".pdf">
								<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
    								<cfpdfparam source="#filedir#\#name#">
 								</cfpdf>
							<cfelse>
								<cfdocument 
	    							 format="pdf" 
	    							 srcfile="#filedir#\#name#" 
	    							 filename="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf"
		  							overwrite="yes"> 
	 							</cfdocument> 
								<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
	    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
	    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
	 							</cfpdf>
								
								<cffile action="DELETE" file="#deldir#IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
							</cfif>
						</cfif>
						<cfelse>
							<cftry>
							<cffile action="DELETE" file="#filedir#\#name#">
							<cfcatch type="Any">
								<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
									Could not delete #filedir#\#name#:<br><br>
									#cfcatch.message#<br>#cfcatch.detail#<br><br>
								</cfmail>	
							</cfcatch>
							</cftry>
						</cfif>
						
					</cfloop>
				</cfif>
			</cfif>		
			
			
			<cffile action="MOVE" source="#deldir#IRP_#getincidentdetails.trackingnum#.pdf" destination="#PDFFileDir#" nameconflict="OVERWRITE">
			
			 <cfif iscomplete eq "yes">
			 	<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset tolist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail,Users.Firstname,Users.Lastname
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset tolist = getemaildata.reviewerEmail>
			 			<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.status#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'IRP Prepare',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.HSSEManager#">, 'IRP Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="IRP sent for review by #request.fname# #request.lname# to #getemaildata.HSSEManager#">)
						</cfquery>
			 				<cfif trim(tolist) neq ''>
							<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - IRP sent for review">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - IRP sent for review">
												</cfif>
												<cfmail to="#tolist#" from="#request.userlogin#" type="html" subject="#emsubj#">
											
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following IRP has been sent for review. <br>Please go to oneAIM via this link to review the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.IRP&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
							</cfif>
			 
			 </cfif>
				
			<!--- <cfif iscomplete eq "yes">
			<cfset subjaddon = "">
			<cfquery name="getcurstat" datasource="#request.dsn#">
					select status 
					from IncidentInvestigation
					where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
					<cfquery name="updateinvstat" datasource="#request.dsn#">
						update IncidentInvestigation
						set status = 'Approved'
						where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
			<cfquery name="getinccapa" datasource="#request.dsn#">
						select capaid, DateComplete,status, duedate, actiondetail
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					<cfset newincstat = "Closed">
					<cfset needcapastruct = {}>
					<cfloop query="getinccapa">
						<cfif status eq "Open">
							<cfset newincstat = "Review Completed">
							<cfset subjaddon = listappend(subjaddon,"CA")>
							<!--- <cfbreak> --->
							<cfif not structkeyexists(needcapastruct,capaid)>
								<cfset needcapastruct[capaid] = "#duedate#~#actiondetail#">
							</cfif>
						</cfif>
					</cfloop>
					
					<cfquery name="getinctype" datasource="#request.dsn#">
						select PrimaryType,SecondaryType
						from oneAIMincidents
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
				<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
					<cfquery name="getoshacat" datasource="#request.dsn#">
						SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
						FROM            oneAIMInjuryOI
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
					<cfif getoshacat.OSHAclass eq 2>
						<cfif trim(getoshacat.LTIDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"LTI")>
							<cfset newincstat = "Review Completed">
						</cfif>
					<cfelseif getoshacat.OSHAclass eq 4> 
						<cfif trim(getoshacat.RWCDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"RWC")>
							<cfset newincstat = "Review Completed">
						</cfif>
					</cfif> 
				</cfif>
					
					
					
					
					
					<cfif newincstat eq "Closed">
					
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
						
							<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon completion of all CA">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss
												FROM            Groups INNER JOIN
							                         oneAIMincidents INNER JOIN
							                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID ON Groups.Group_Number = oneAIMincidents.GroupNumber INNER JOIN
							                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
							                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID INNER JOIN
							                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
							                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
							                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
							                         OSHACategories RIGHT OUTER JOIN
							                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
							                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
							                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
							                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
											
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
							<cfelse>
							
									<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.createdBy, oneAIMincidents.isNearMiss
												FROM            Groups INNER JOIN
							                         oneAIMincidents INNER JOIN
							                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID ON Groups.Group_Number = oneAIMincidents.GroupNumber INNER JOIN
							                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
							                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID INNER JOIN
							                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
							                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
							                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
							                         OSHACategories RIGHT OUTER JOIN
							                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
							                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
							                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
							                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfquery name="addaudit" datasource="#request.dsn#">
											insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
										values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdBy#">, 'Incident Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Approved - Pending Closure">)
										</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
													<cfif trim(getemaildata.createdbyEmail) neq ''>
													<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident approved � outstanding items to be actioned">
												<cfelse>
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident approved � outstanding items to be actioned">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
													<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
												
													The following incident has been approved and is awaiting the closure of one or more items before the record can be closed. <br>
													Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br>
													<cfif listfindnocase(subjaddon,"RWC")>
														Restricted Work Case Return Date has not been entered<br><br>
													<cfelseif listfindnocase(subjaddon,"LTI")>
														Lost Time Incident Return Date has not been entered<br><br>
													</cfif></span>
													<cfif listlen(structkeylist(needcapastruct)) gt 0>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" colspan="2"><strong>Oustanding Corrective Actions</strong></td>
															</tr>
															<cfloop list="#structkeylist(needcapastruct)#" index="id">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(listgetat(needcapastruct[id],1,"~"),sysdateformat)#</td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#listgetat(needcapastruct[id],2,"~")#</td>
															</tr>
															</cfloop>
															</table><br>
															</cfif>
															<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
											</cfif>
			
			</cfif>
 --->				</cfif> 
			 	</cfif>
		<cfoutput>
			<cfif imgmsg eq "fail">
			<form action="#self#?fuseaction=#attributes.xfa.IRP#" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="filemsg" value="#imgmsg#">
			</form>
			<cfelse>
			
			
			<cfif closesave eq "no">
				
				<form action="#self#?fuseaction=#attributes.xfa.IRP#" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
				</form>
				
			<cfelse>
				
				<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
					</form>
				
			</cfif>
			
			<!--- <form action="#self#?fuseaction=main.main" method="post" name="redirinc">
			
			</form> --->
			
			</cfif>
			<script type="text/javascript">
				document.redirinc.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="updateirp">
	<cfset imgmsg = "none">
		<cfif trim(irn) neq 0>
			
			
				<cfif iscomplete eq "yes">
					<cfset newstat = "Sent for Review">
				<Cfelse>
					<cfset newstat = "Started">
				</cfif>
			
				<cfquery name="updateirp" datasource="#request.dsn#">
					update oneAIMIncidentIRP
					set IncidentLearning = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#incidentlearning#">,
					SupportingInfo = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#supportinginfo#">,
					Status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newstat#">
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				
				
				 
				<cfif not directoryexists("#FileDir#")>
					<cfdirectory action="CREATE" directory="#FileDir#">
				</cfif>
				
				<cfif trim(UPLOADDOC1) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC1" destination="#FileDir#" nameconflict="OVERWRITE">
				
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
	
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry> 
				</cfif>
				
				<cfif trim(UPLOADDOC2) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC2" destination="#FileDir#" nameconflict="OVERWRITE">
				
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
				
				<cfif trim(UPLOADDOC3) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC3" destination="#FileDir#" nameconflict="OVERWRITE">
					<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
				
				<cfif trim(UPLOADDOC4) neq ''>
				<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC4" destination="#FileDir#" nameconflict="OVERWRITE">
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
			
				<cfif trim(UPLOADDOC5) neq ''>
					<cftry>
				<cffile action="upload" accept="image/jpg,image/jpeg,image/gif,image/pjpeg,image/pjpg,image/png,application/vnd.ms-powerpoint,application/vnd.ms-excel,application/msword,application/octet-stream,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/x-tika-msoffice,application/pdf" filefield="UPLOADDOC5" destination="#FileDir#" nameconflict="OVERWRITE">
				<cfset fname = replace(cffile.serverFile,"&","","all")>
				<cffile action="RENAME" source="#FileDir#\#cffile.serverFile#" destination="#FileDir#\#fname#">
				
				<cfif cffile.contentType eq "image">
					<cfset smname = replace(cffile.serverFileName,"&","","all")>
					<cfimage  action = "info"   source = "#FileDir#\#fname#"   structname = "imageinfo">
					
					<cfif imageinfo.width gt 375>
						<cfimage   action = "resize"  destination="#smname#_sm.#cffile.serverfileext#"   height = ""  source = "#FileDir#\#fname#"   width = "375"  overwrite = "yes">
						<cfimage source="#smname#_sm.#cffile.serverfileext#" action="write" destination="#FileDir#\#smname#_sm.#cffile.serverfileext#" overwrite="yes">
						<cftry>
							<cffile action="delete" file="#FileDir#\#fname#"> 
							<cffile action="delete" file="#deldir##smname#_sm.#cffile.serverfileext#">
							<cfcatch type="Any">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>	
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>
				<cfcatch type="Any">
					<cfset imgmsg="fail">
							<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
								Errors occurred during upload:<br><br>
								#cfcatch.message#<br>#cfcatch.detail#<br><br>
								Upload by: #request.userlogin#<br><br>
							</cfmail>
				</cfcatch>
				</cftry>
				</cfif>
			
			
			<cfquery name="getirpdetail" datasource="#request.dsn#">
				SELECT   IRPid, IRN, IncidentLearning, SupportingInfo, Status
				FROM            oneAIMIncidentIRP
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#IRN#">
			</cfquery>
			
			
			<cfdocument format="PDF" filename="IRP_#getincidentdetails.trackingnum#.pdf"  overwrite="yes">
				<cfinclude template="/#getappconfig.oneAIMpath#/incidents/displays/dsp_irppdf.cfm">
			</cfdocument>
			 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
			
			
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfset ctr = 0>
					<cfloop query="getincfileslist">
						<cfif left(name,6) neq ".~lock">
							<cfset ctr = ctr+1>
							<cfif not isImageFile("#filedir#\#name#")>
								<cfif right(name,4) eq ".pdf">
									<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
    										<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
    								<cfpdfparam source="#filedir#\#name#">
 									</cfpdf>
								<cfelse>
									<cfdocument 
	    							 format="pdf" 
	    							 srcfile="#filedir#\#name#" 
	    							 filename="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf"
		  							overwrite="yes"> 
	 								</cfdocument> 
									<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
	    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
	    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
	 								</cfpdf>
								
									<cffile action="DELETE" file="#deldir#IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
								</cfif>
							</cfif>
						<cfelse>
							<cftry>
							<cffile action="DELETE" file="#filedir#\#name#">
							<cfcatch type="Any">
								<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
									Could not delete #filedir#\#name#:<br><br>
									#cfcatch.message#<br>#cfcatch.detail#<br><br>
								</cfmail>	
							</cfcatch>
							</cftry>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>		
			
			<cffile action="MOVE" source="#deldir#IRP_#getincidentdetails.trackingnum#.pdf" destination="#PDFFileDir#" nameconflict="OVERWRITE"> 
			
			
			<!--- 
			<cfquery name="getirpdetail" datasource="#request.dsn#">
				SELECT   IRPid, IRN, IncidentLearning, SupportingInfo, Status
				FROM            oneAIMIncidentIRP
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#IRN#">
			</cfquery>
			<!--- <cfhtmltopdf>
					This is a test <cfoutput>#now()#</cfoutput>
			</cfhtmltopdf> --->


			<!--- <cfdocument format="PDF" filename="IRP_#getincidentdetails.trackingnum#.pdf"  overwrite="yes">
				aaa<!--- <cfinclude template="/#getappconfig.oneAIMpath#/incidents/displays/dsp_irppdf.cfm">  --->
				
			</cfdocument> ---><!---  --->
			 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
		<!--- 	<cffile action="MOVE" source="#deldir#\IRP_#getincidentdetails.trackingnum#.pdf" destination="#PDFFileDir##" nameconflict="OVERWRITE"> ---> --->
			
			 <cfif iscomplete eq "yes">
			 	<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset tolist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail,Users.Firstname,Users.Lastname
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset tolist = getemaildata.reviewerEmail>
			 			<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.status#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'IRP Prepare',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.HSSEManager#">, 'IRP Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="IRP sent for review by #request.fname# #request.lname# to #getemaildata.HSSEManager#">)
						</cfquery>
			 				<cfif trim(tolist) neq ''>
							<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - IRP sent for review">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - IRP sent for review">
												</cfif>
												<cfmail to="#tolist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following IRP has been sent for review. <br>Please go to oneAIM via this link to review the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.IRP&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
							</cfif>
			 
			 </cfif>
			 <!--- <cfif iscomplete eq "yes">
			 <cfset subjaddon = "">
			<cfquery name="getcurstat" datasource="#request.dsn#">
					select status 
					from IncidentInvestigation
					where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="updateinvstat" datasource="#request.dsn#">
						update IncidentInvestigation
						set status = 'Approved'
						where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
			<cfquery name="getinccapa" datasource="#request.dsn#">
						select capaid, DateComplete,status, duedate, actiondetail
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					<cfset newincstat = "Closed">
					<cfset needcapastruct = {}>
					<cfloop query="getinccapa">
						<cfif status eq "Open">
							<cfset subjaddon = listappend(subjaddon,"CA")>
							<cfset newincstat = "Review Completed">
							<!--- <cfbreak> --->
							<cfif not structkeyexists(needcapastruct,capaid)>
								<cfset needcapastruct[capaid] = "#duedate#~#actiondetail#">
							</cfif>
						</cfif>
					</cfloop>
					
					<cfquery name="getinctype" datasource="#request.dsn#">
						select PrimaryType,SecondaryType
						from oneAIMincidents
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
				<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
					<cfquery name="getoshacat" datasource="#request.dsn#">
						SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
						FROM            oneAIMInjuryOI
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
					<cfif getoshacat.OSHAclass eq 2>
						<cfif trim(getoshacat.LTIDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"LTI")>
							<cfset newincstat = "Review Completed">
						</cfif>
					<cfelseif getoshacat.OSHAclass eq 4> 
						<cfif trim(getoshacat.RWCDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"RWC")>
							<cfset newincstat = "Review Completed">
						</cfif>
					</cfif> 
				</cfif>
					
					
					
					
					
					<cfif newincstat eq "Closed">
					
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
						
							<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon completion of all CA">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss
												FROM            Groups INNER JOIN
							                         oneAIMincidents INNER JOIN
							                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID ON Groups.Group_Number = oneAIMincidents.GroupNumber INNER JOIN
							                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
							                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID INNER JOIN
							                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
							                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
							                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
							                         OSHACategories RIGHT OUTER JOIN
							                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
							                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
							                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
							                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="Incident closed">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
							<cfelse>
							
									<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.createdBy, oneAIMincidents.isNearMiss
												FROM            Groups INNER JOIN
							                         oneAIMincidents INNER JOIN
							                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID ON Groups.Group_Number = oneAIMincidents.GroupNumber INNER JOIN
							                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID INNER JOIN
							                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID INNER JOIN
							                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
							                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
							                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
							                         OSHACategories RIGHT OUTER JOIN
							                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
							                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
							                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
							                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfquery name="addaudit" datasource="#request.dsn#">
											insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
										values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdBy#">, 'Incident Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Approved - Pending Closure">)
										</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident approved � outstanding items to be actioned">
												<cfelse>
													<cfset emsubj = "#getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident approved � outstanding items to be actioned">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident has been approved and is awaiting the closure of one or more items before the record can be closed. <br>
													Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br>
													<cfif listfindnocase(subjaddon,"RWC")>
														Restricted Work Case Return Date has not been entered<br><br>
													<cfelseif listfindnocase(subjaddon,"LTI")>
														Lost Time Incident Return Date has not been entered<br><br>
													</cfif></span>
													<cfif listlen(structkeylist(needcapastruct)) gt 0>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" colspan="2"><strong>Oustanding Corrective Actions</strong></td>
															</tr>
															<cfloop list="#structkeylist(needcapastruct)#" index="id">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(listgetat(needcapastruct[id],1,"~"),sysdateformat)#</td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#listgetat(needcapastruct[id],2,"~")#</td>
															</tr>
															</cfloop>
															</table><br>
															</cfif>
															<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
											</cfif>
		
			</cfif> ---> 
		
			</cfif>
			
			<cfoutput>
			<cfif imgmsg eq "fail">
			<form action="#self#?fuseaction=#attributes.xfa.IRP#" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="filemsg" value="#imgmsg#">
			</form>
			<cfelse>
			
			<cfif closesave eq "no">
				
				<form action="#self#?fuseaction=#attributes.xfa.IRP#" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
				</form>
				
			<cfelse>
				
				<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
					</form>
				
			</cfif>
			
			<!--- <form action="#self#?fuseaction=main.main" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="filemsg" value="#imgmsg#">
			</form> --->
			</cfif>
			<script type="text/javascript">
				document.redirinc.submit();
			</script>
		</cfoutput>
	</cfcase>
	<cfcase value="regenerateIRP">
	<cfset imgmsg = "none">
		<cfif trim(irn) neq 0>
			
			
			
			<cfquery name="getirpdetail" datasource="#request.dsn#">
				SELECT   IRPid, IRN, IncidentLearning, SupportingInfo, Status
				FROM            oneAIMIncidentIRP
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#IRN#">
			</cfquery>
			
			
			<cfdocument format="PDF" filename="IRP_#getincidentdetails.trackingnum#.pdf"  overwrite="yes">
				<cfinclude template="/#getappconfig.oneAIMpath#/incidents/displays/dsp_irppdf.cfm">
			</cfdocument>
			 <cfif not directoryexists("#PDFFileDir#")>
				<cfdirectory action="CREATE" directory="#PDFFileDir#">
			</cfif>
			
			
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfset ctr = 0>
					<cfloop query="getincfileslist">
						<cfif left(name,6) neq ".~lock">
							<cfset ctr = ctr+1>
							<cfif not isImageFile("#filedir#\#name#")>
								<cfif right(name,4) eq ".pdf">
									<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
    									<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
    									<cfpdfparam source="#filedir#\#name#">
 									</cfpdf>
								<cfelse>
									<cfdocument 
	    							 format="pdf" 
	    							 srcfile="#filedir#\#name#" 
	    							 filename="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf"
		  							overwrite="yes"> 
	 							</cfdocument> 
									<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
	    									<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
	    									<cfpdfparam source="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
	 								</cfpdf>
								
									<cffile action="DELETE" file="#deldir#IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
								</cfif>
							</cfif>
						<cfelse>
							<cftry>
							<cffile action="DELETE" file="#filedir#\#name#">
							<cfcatch type="Any">
								<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
									Could not delete #filedir#\#name#:<br><br>
									#cfcatch.message#<br>#cfcatch.detail#<br><br>
								</cfmail>	
							</cfcatch>
							</cftry>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>		
			
			<cffile action="MOVE" source="#deldir#IRP_#getincidentdetails.trackingnum#.pdf" destination="#PDFFileDir#" nameconflict="OVERWRITE"> 
		
			
			 			
		
			</cfif>
	
	
	
			<cfoutput>
				<form action="#self#?fuseaction=#attributes.xfa.IRP#" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
				</form>
				<script type="text/javascript">
				document.redirinc.submit();
			</script>
			</cfoutput> 
	</cfcase>
	<cfcase value="deleteIRPfile">
		<cfset irpmsg = 0>   
		<cfparam name="filename" default="">
			<cfif irn neq 0 and trim(filename) neq ''>
				<cfif fileexists("#filedir#\#filename#")>
					<cftry>
					<cffile action="DELETE" file="#filedir#\#filename#">
					<cfcatch type="Any">
					<cfset irpmsg = 1>
					</cfcatch>
					</cftry>
				</cfif>
			</cfif>
			<cfoutput>
				<form action="#self#?fuseaction=#attributes.xfa.IRP#" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="irpmsg" value="#irpmsg#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script>
			</cfoutput>
	</cfcase>
	<cfcase value="approveIRP">
		<cfparam name="approveFA" default="">
		
			<cfquery name="updateirp" datasource="#request.dsn#">
					update oneAIMIncidentIRP
					set Status = 'Complete'
					<cfif approveFA eq "yes">, wasissued = 1</cfif>
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfparam name="revcomments" default="">
			
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'IRP Approve','irp')
					</cfquery>
				</cfif>
				<cfif approveFA eq "yes">
					<cfquery name="addaudit" datasource="#request.dsn#">
						insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'IRP Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'IRP Sent for Review','','IRP Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="IRP approved by #request.fname# #request.lname#">)
					</cfquery>
				<cfelse>
					<cfquery name="addaudit" datasource="#request.dsn#">
						insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'IRP Declined',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'IRP Sent for Review','','IRP Declined',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="IRP declined by #request.fname# #request.lname#">)
					</cfquery>
				</cfif>
				
				
			 <cfset subjaddon = "">
			<cfquery name="getcurstat" datasource="#request.dsn#">
					select status 
					from IncidentInvestigation
					where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="updateinvstat" datasource="#request.dsn#">
						update IncidentInvestigation
						set status = 'Approved'
						where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
			<cfquery name="getinccapa" datasource="#request.dsn#">
						select capaid, DateComplete,status, duedate, actiondetail
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					<cfset newincstat = "Closed">
					<cfset needcapastruct = {}>
					<cfloop query="getinccapa">
						<cfif status eq "Open">
							<cfset subjaddon = listappend(subjaddon,"CA")>
							<cfset newincstat = "Review Completed">
							<!--- <cfbreak> --->
							<cfif not structkeyexists(needcapastruct,capaid)>
								<cfset needcapastruct[capaid] = "#duedate#~#actiondetail#">
							</cfif>
						</cfif>
					</cfloop>
					
					<cfquery name="getinctype" datasource="#request.dsn#">
						select PrimaryType,SecondaryType
						from oneAIMincidents
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
				<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
					<cfquery name="getoshacat" datasource="#request.dsn#">
						SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
						FROM            oneAIMInjuryOI
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
					<cfif getoshacat.OSHAclass eq 2>
						<cfif trim(getoshacat.LTIDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"LTI")>
							<cfset newincstat = "Review Completed">
						</cfif>
					<cfelseif getoshacat.OSHAclass eq 4> 
						<cfif trim(getoshacat.RWCDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"RWC")>
							<cfset newincstat = "Review Completed">
						</cfif>
					</cfif> 
				</cfif>
					
					
					
					
					
					<cfif newincstat eq "Closed">
					
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
						
							<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon completion of all CA">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
											
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
							<cfelse>
							
									<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.createdBy, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfquery name="addaudit" datasource="#request.dsn#">
											insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
										values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdBy#">, 'Incident Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Approved - Pending Closure">)
										</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident approved � outstanding items to be actioned">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident approved � outstanding items to be actioned">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
											
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident has been approved and is awaiting the closure of one or more items before the record can be closed. <br>
													Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br>
													<cfif listfindnocase(subjaddon,"RWC")>
														Restricted Work Case Return Date has not been entered<br><br>
													<cfelseif listfindnocase(subjaddon,"LTI")>
														Lost Time Incident Return Date has not been entered<br><br>
													</cfif></span>
													<cfif listlen(structkeylist(needcapastruct)) gt 0>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" colspan="2"><strong>Oustanding Corrective Actions</strong></td>
															</tr>
															<cfloop list="#structkeylist(needcapastruct)#" index="id">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(listgetat(needcapastruct[id],1,"~"),sysdateformat)#</td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#listgetat(needcapastruct[id],2,"~")#</td>
															</tr>
															</cfloop>
															</table><br>
															</cfif>
															<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
											</cfif>
		
			<cfif approveFA eq "yes">
			
				<cfset sendfatolist = "">
				<cfset emailids = "">
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.InvestigationLevel, oneAIMincidents.isWorkRelated, IncidentTypes.IncType, 
                         oneAIMincidents.PotentialRating, OSHACategories.Category, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, 
                         Groups.Group_Name, GroupLocations.SiteName, IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, 
                         NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss, oneAIMInjuryOI.isSerious, oneAIMEnvironmental.inBreach, 
                         oneAIMincidents.EnforcementNotice, oneAIMincidents.EnforcementNoticeType, NewDials_2.Name AS buname, Countries.CountryName, 
                         oneAIMincidents.SpecificLocation, Occupations.Occupation, oneAIMInjuryOI.ipGender, oneAimFirstAlerts.PreventAction, oneAimFirstAlerts.description AS FAdesc, 
                         oneAIMincidents.ActionTaken, oneAIMincidents.HSSEAdvisor, Groups.OUid, oneAIMInjuryOI.injuryType
FROM            oneAIMInjuryOI LEFT OUTER JOIN
                         Occupations ON oneAIMInjuryOI.ipOccupation = Occupations.OccupationID LEFT OUTER JOIN
                         OSHACategories ON oneAIMInjuryOI.OSHAclass = OSHACategories.CatID RIGHT OUTER JOIN
                         Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid INNER JOIN
                         NewDials AS NewDials_2 ON Groups.Business_Line_ID = NewDials_2.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID ON 
                         GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID ON oneAIMInjuryOI.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				
				<cfquery name="getemaillist" datasource="#request.dsn#">		
					SELECT        EmailAddress, DialID, type, emailtype
					FROM            DistributionEmail
					WHERE        (type = 'org') AND (emailtype = 'fa')	AND (DialID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2707,#getemaildata.Business_Line_ID#,#getemaildata.OUid#" list="Yes">)) and HiPoFAonly = 0	 
				</cfquery>
				<cfset sendfatolist = valuelist(getemaillist.EmailAddress)>
				<cfif trim(sendfatolist) neq ''>
					<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_managerIRP.cfm", "uploads\incidents\#irn#\IRPPDF"))>
					<cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">
					</cfif>
					<cfif not directoryexists("#PDFFileDir#")>
						<cfdirectory action="CREATE" directory="#PDFFileDir#">
					</cfif>
					<cfif not fileexists("#PDFFileDir#\IRP_#getemaildata.TrackingNum#.pdf")>
					
					<cfquery name="getirpdetail" datasource="#request.dsn#">
						SELECT   IRPid, IRN, IncidentLearning, SupportingInfo, Status
						FROM            oneAIMIncidentIRP
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#IRN#">
					</cfquery>
					<cfdocument format="PDF" filename="IRP_#getincidentdetails.trackingnum#.pdf"  overwrite="yes">
						<cfinclude template="/#getappconfig.oneAIMpath#/incidents/displays/dsp_irppdf.cfm">
					</cfdocument>
					 
			
			
			
						<cfif isdefined("getincfileslist.recordcount")>
							<cfif getincfileslist.recordcount gt 0>
								<cfset ctr = 0>
								<cfloop query="getincfileslist">
									<cfif left(name,6) neq ".~lock">
									<cfset ctr = ctr+1>
									<cfif not isImageFile("#filedir#\#name#")>
										<cfif right(name,4) eq ".pdf">
											<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
    											<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
    											<cfpdfparam source="#filedir#\#name#">
 											</cfpdf>
										<cfelse>
											<cfdocument 
			    							 format="pdf" 
			    							 srcfile="#filedir#\#name#" 
			    							 filename="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf"
				  							overwrite="yes"> 
			 								</cfdocument> 
											<cfpdf action="merge" destination="IRP_#getincidentdetails.trackingnum#.pdf" overwrite="yes" keepBookmark="yes">
			    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#.pdf">
			    								<cfpdfparam source="IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
			 								</cfpdf>
										
											<cffile action="DELETE" file="#deldir#IRP_#getincidentdetails.trackingnum#_#ctr#.pdf">
										</cfif>
									</cfif>
									<cfelse>
										<cftry>
										<cffile action="DELETE" file="#filedir#\#name#">
										<cfcatch type="Any">
											<cfmail to="#erroremail#" from="#fromemail#" subject="oneAIM error" type="html">
												Could not delete #filedir#\#name#:<br><br>
												#cfcatch.message#<br>#cfcatch.detail#<br><br>
											</cfmail>	
										</cfcatch>
										</cftry>
									</cfif>
									
								</cfloop>
							</cfif>
						</cfif>		
						
						
						<cffile action="MOVE" source="#deldir#IRP_#getincidentdetails.trackingnum#.pdf" destination="#PDFFileDir#" nameconflict="OVERWRITE">
					
					</cfif>
					
					
					<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - #getemaildata.IncType# - #getemaildata.buname# - #getemaildata.TrackingNum# - Near Miss - IRP Learning Sheet">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - #getemaildata.IncType# - #getemaildata.buname# - #getemaildata.TrackingNum# - IRP Learning Sheet">
					</cfif>
					<cfmail to="#sendfatolist#" from="#request.userlogin#" type="html" subject="#emsubj#">
					
					<CFMAILPARAM NAME="X-Priority" VALUE="1">
					<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">An IRP incident learning sheet for the following incident is attached for onward distribution and/or display on notice boards.<br><br></span>
							<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Is this a near miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr> 
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
							<br><br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">For further details on this incident please go to oneAIM via this link � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.trackingnum#</a><br><br> 
If you cannot access oneAIM but require further information please contact the HSSE Advisor for this incident � #getemaildata.HSSEadvisor#<br><br> 
Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
							<br><br>
						
								<cfif fileexists("#PDFFileDir#\IRP_#getemaildata.TrackingNum#.pdf")>
									<cfmailparam file="#PDFFileDir#\IRP_#getemaildata.TrackingNum#.pdf">
								</cfif>
								
					</cfmail>
					
					
					
				</cfif>
			
			
			
			</cfif>
		
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="moreinfoIRP">
		<cfparam name="revcomments" default="">
			
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'IRP Review','irp')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update oneAIMIncidentIRP
					set status = 'More Info Requested'
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				
				
				
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss, oneAIMincidents.createdBy
					FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'IRP Sent for More Information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'IRP Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdBy#">,'IRP form sent for more information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="IRP form sent for more information by #request.fname# #request.lname# to #getemaildata.createdBy#">)
				</cfquery>
				<cfquery name="getbuname" datasource="#request.dsn#">
					SELECT      Name
					FROM            NewDials
					WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
				</cfquery>
					<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif trim(getemaildata.createdbyEmail) neq ''>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss - IRP - provide further information">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - IRP - provide further information">
					</cfif>
				<cfmail to="#getemaildata.createdbyEmail#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							Further information has been requested for an IRP raised for the following incident.  
							<br>Please go to oneAIM via this link to update and progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.irp&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Reviewer Comments:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#revcomments#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<!--- <tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr> --->
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
					</cfif>
				
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="cancelIRP">
		<cfif trim(irn) neq ''>
			<cftransaction>
				<cfquery name="updateIRN" datasource="#request.dsn#">
					update oneAIMincidents
					set needIRP = 0
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				
				<cfquery name="removeIRP" datasource="#request.dsn#">
					delete from oneAIMIncidentIRP
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'IRP Cancel','irp')
					</cfquery>
				</cfif>
				<cfset statd = "IRP form cancelled by #request.fname# #request.lname#">
				<cfif trim(revcomments) neq ''>
					<cfset statd = statd & ": #revcomments#">
				</cfif>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'IRP Cancelled',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'IRP Prepare','','IRP form cancelled',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#statd#">)
				</cfquery>
				
				
				
				
				<cfset subjaddon = "">
			<cfquery name="getcurstat" datasource="#request.dsn#">
					select status 
					from IncidentInvestigation
					where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="updateinvstat" datasource="#request.dsn#">
						update IncidentInvestigation
						set status = 'Approved'
						where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
			<cfquery name="getinccapa" datasource="#request.dsn#">
						select capaid, DateComplete,status, duedate, actiondetail
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					<cfset newincstat = "Closed">
					<cfset needcapastruct = {}>
					<cfloop query="getinccapa">
						<cfif status eq "Open">
							<cfset subjaddon = listappend(subjaddon,"CA")>
							<cfset newincstat = "Review Completed">
							<!--- <cfbreak> --->
							<cfif not structkeyexists(needcapastruct,capaid)>
								<cfset needcapastruct[capaid] = "#duedate#~#actiondetail#">
							</cfif>
						</cfif>
					</cfloop>
					
					<cfquery name="getinctype" datasource="#request.dsn#">
						select PrimaryType,SecondaryType
						from oneAIMincidents
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
				<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
					<cfquery name="getoshacat" datasource="#request.dsn#">
						SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
						FROM            oneAIMInjuryOI
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
					<cfif getoshacat.OSHAclass eq 2>
						<cfif trim(getoshacat.LTIDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"LTI")>
							<cfset newincstat = "Review Completed">
						</cfif>
					<cfelseif getoshacat.OSHAclass eq 4> 
						<cfif trim(getoshacat.RWCDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"RWC")>
							<cfset newincstat = "Review Completed">
						</cfif>
					</cfif> 
				</cfif>
					
					
					
					
					
					<cfif newincstat eq "Closed">
					
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
						
							<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by Cancellation of IRP">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
							</cfif>
				
				
				
			</cftransaction>
		</cfif>
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
</cfswitch>
</cfsetting>