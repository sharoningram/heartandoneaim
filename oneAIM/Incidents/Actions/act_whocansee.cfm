<cfset altnextby = "">
<cfset altnextbywb = "">
<cfquery name="getentbystat" datasource="#request.dsn#">
SELECT        Users.Status, GroupUserAssignment.GroupNumber
FROM            Users LEFT OUTER JOIN
                         GroupUserAssignment ON Users.UserId = GroupUserAssignment.UserID AND GroupUserAssignment.GroupRole = 'dataentry'
WHERE        (Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getincidentdetails.createdbyEmail#">)
order by GroupNumber desc
</cfquery>
<cfif getentbystat.status eq 0 and trim(getentbystat.GroupNumber) neq ''>
	<cfquery name="getaltnextusr" datasource="#request.dsn#">
		SELECT        GroupUserAssignment.GroupNumber, Users.Firstname + ' ' + Users.Lastname as nm
		FROM            Users LEFT OUTER JOIN
                         GroupUserAssignment ON Users.UserId = GroupUserAssignment.UserID
		WHERE        (GroupUserAssignment.GroupRole = 'dataentry') AND (GroupUserAssignment.GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">) AND (Users.Status = 1)
	</cfquery>
	<cfloop query="getaltnextusr">
		<cfset altnextby = listappend(altnextby,nm)>
	</cfloop>
</cfif>

<cfif trim(getincidentdetails.withdrawnto) neq '' and trim(getincidentdetails.withdrawnto) gt 0>
<cfquery name="getentbystatwb" datasource="#request.dsn#">
SELECT        Users.Status, GroupUserAssignment.GroupNumber
FROM            Users LEFT OUTER JOIN
                         GroupUserAssignment ON Users.UserId = GroupUserAssignment.UserID AND GroupUserAssignment.GroupRole = 'dataentry'
WHERE        (Users.userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.withdrawnto#">)
order by GroupNumber desc
</cfquery>
<cfif getentbystatwb.status eq 0 and trim(getentbystatwb.GroupNumber) neq ''>
	<cfquery name="getaltnextusr" datasource="#request.dsn#">
		SELECT        GroupUserAssignment.GroupNumber, Users.Firstname + ' ' + Users.Lastname as nm
		FROM            Users LEFT OUTER JOIN
                         GroupUserAssignment ON Users.UserId = GroupUserAssignment.UserID
		WHERE        (GroupUserAssignment.GroupRole = 'dataentry') AND (GroupUserAssignment.GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">) AND (Users.Status = 1)
	</cfquery>
	<cfloop query="getaltnextusr">
		<cfset altnextbywb = listappend(altnextbywb,nm)>
	</cfloop>
</cfif>
</cfif>

<cfset viewerlist = arraynew(2)>
<cfquery name="getstat" datasource="#request.dsn#">
	select status
	from oneAimFirstAlerts
	where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
</cfquery>
<cfquery name="getinvstat" datasource="#request.dsn#">
	select status
	from IncidentInvestigation
	where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
</cfquery>
<cfquery name="getirpstat" datasource="#request.dsn#">
	SELECT        Status
	FROM            oneAIMIncidentIRP
	WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>
<cfif getincidentdetails.isFatality eq "yes">
	<cfset ctr = 1>
	<cfquery name="getglbadmin" datasource="#request.dsn#">
		SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
		FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
		WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
	</cfquery>
	<cfloop query="getglbadmin">
		<cfset viewerlist[ctr][1] = UserRole>
		<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
		<cfset ctr = ctr + 1>
	</cfloop>
	<cfquery name="getbuadmin" datasource="#request.dsn#">
		SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
		FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
		WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
	</cfquery>
	<cfloop query="getbuadmin">
		<cfset viewerlist[ctr][1] = UserRole>
		<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
		<cfset ctr = ctr + 1>
	</cfloop>
	<cfif getincidentdetails.status eq "Sent for Review">
		<cfquery name="getreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
	</cfif>
	<cfset getwhocansee = QueryNew("userrole, name")>
		<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
			<cfset QueryAddRow( getwhocansee ) />
			<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
			<cfset getwhocansee["name"][i] = viewerlist[i][2]>
		</cfloop>
<cfelse>
<cfswitch expression="#getincidentdetails.status#">
	<cfcase value="Review Completed">
			<!--- <cfset getwhocansee = QueryNew("userrole, name")> getincidentdetails.investigationlevel eq 2 or  --->
			<cfif getincidentdetails.needFA eq 1>
				
				
					<cfset ctr = 1>
					<cfquery name="getglbadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
					</cfquery>
					<cfloop query="getglbadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getbuadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                             (SELECT        BUid
			                               FROM            Groups
			                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getbuadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getouadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'ou admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                             (SELECT        OUid
			                               FROM            Groups
			                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getouadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					
					
					
					
					<cfif trim(getstat.status) eq '' or trim(getstat.status) eq "More Info Requested">
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfelseif trim(getstat.status) eq "sr more info">
					
					<cfquery name="getreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
					FROM            Users INNER JOIN
			                       UserRoles ON Users.UserId = UserRoles.UserID
					WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                           (SELECT        business_line_id
			                             FROM            Groups
			                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
				</cfquery>
				<cfloop query="getreviewer">
					<cfset viewerlist[ctr][1] = UserRole>
					<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
					<cfset ctr = ctr + 1>
				</cfloop>
					
					
					<cfelseif trim(getstat.status) eq "sent for review">
					<cfif not isdefined("getreviewer.recordcount")>
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					</cfif>
						
					
					<cfelseif trim(getstat.status) eq "sent for approval">
					<cfif not isdefined("getreviewer.recordcount")>
						<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					</cfif>
						
					
					</cfif>
					
					
					
					<cfif getinvstat.status eq "IRP Pending">
					<cfif not isdefined("getreviewer.recordcount")>
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					</cfif>
					<cfquery name="getsrreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getsrreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
					<cfelseif getinvstat.status eq "Sent for Review">
						<!--- <cfif getincidentdetails.investigationlevel eq 1> --->
						<cfif not isdefined("getreviewer.recordcount")>
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
						</cfif> 
						<cfif getincidentdetails.investigationlevel eq 2>
						<cfquery name="getreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
						
						
						</cfif>
					
					</cfif>
					
					
					
					
					<cfset viewerlist[ctr][1] = "Initiator">
						<cfset viewerlist[ctr][2] = getincidentdetails.createdBy>
						<cfset ctr = ctr + 1>
					
					
					<cfset getwhocansee = QueryNew("userrole, name")>
					<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
						<cfset QueryAddRow( getwhocansee ) />
						<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
						<cfset getwhocansee["name"][i] = viewerlist[i][2]>
					</cfloop>
			
		<cfelse>
		
				<cfset ctr = 1>
					<cfquery name="getglbadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
					</cfquery>
					<cfloop query="getglbadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getbuadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                             (SELECT        BUid
			                               FROM            Groups
			                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getbuadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getouadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'ou admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                             (SELECT        OUid
			                               FROM            Groups
			                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getouadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfif getinvstat.status eq "IRP Pending">
					
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getsrreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getsrreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
					<cfelseif getinvstat.status eq "Sent for Review">
						<!--- <cfif getincidentdetails.investigationlevel eq 1> --->
						
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
						<cfif getincidentdetails.investigationlevel eq 2>
						<cfquery name="getreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
						
						
						</cfif>
					
					</cfif>
					<cfset viewerlist[ctr][1] = "Initiator">
						<cfset viewerlist[ctr][2] = getincidentdetails.createdBy>
						<cfset ctr = ctr + 1>
			<cfset getwhocansee = QueryNew("userrole, name")>
					<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
						<cfset QueryAddRow( getwhocansee ) />
						<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
						<cfset getwhocansee["name"][i] = viewerlist[i][2]>
					</cfloop>
					
				
				
		</cfif>
		
		
		
		
	</cfcase>
	<cfcase value="closed">
		<cfset getwhocansee = QueryNew("userrole, name")><!--- getincidentdetails.investigationlevel eq 2 or  --->
				<cfset ctr = 1>
					<cfquery name="getglbadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
					</cfquery>
					<cfloop query="getglbadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getbuadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                             (SELECT        BUid
			                               FROM            Groups
			                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getbuadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					<cfquery name="getouadmin" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
			                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (UserRoles.UserRole = 'ou admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
			                             (SELECT        OUid
			                               FROM            Groups
			                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getouadmin">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
					
			<!--- <cfif getincidentdetails.needFA eq 1>
				<cfif trim(getstat.status) eq ''> --->
					<cfif getincidentdetails.investigationlevel eq 2 or getincidentdetails.needfa eq 1>
		
		<cfquery name="getsrreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getsrreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		
		</cfif>
					
					<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
				
					<cfset viewerlist[ctr][1] = "Initiator">
						<cfset viewerlist[ctr][2] = getincidentdetails.createdBy>
						<cfset ctr = ctr + 1>
					
				<!--- </cfif>
		
			</cfif> --->
				<cfset getwhocansee = QueryNew("userrole, name")>
					<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
						<cfset QueryAddRow( getwhocansee ) />
						<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
						<cfset getwhocansee["name"][i] = viewerlist[i][2]>
					</cfloop>
		<!--- <cfloop from="1" to="#arraylen(viewerlist)#" index="i">
			<cfset QueryAddRow( getwhocansee ) />
			<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
			<cfset getwhocansee["name"][i] = viewerlist[i][2]>
		</cfloop> --->
	</cfcase>
	<cfcase value="Withdrawn to Initiator">
		<cfset viewerlist[1][1] = "Initiator">
		<cfset viewerlist[1][2] = getincidentdetails.createdBy>
		
		<cfset getwhocansee = QueryNew("userrole, name")>
		<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
			<cfset QueryAddRow( getwhocansee ) />
			<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
			<cfset getwhocansee["name"][i] = viewerlist[i][2]>
		</cfloop>
	</cfcase>
	<cfcase value="initiated,started,More Info Requested">
	
	
		<cfset ctr = 1>
		<cfquery name="getglbadmin" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
		</cfquery>
		<cfloop query="getglbadmin">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		<cfquery name="getbuadmin" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
                             (SELECT        BUid
                               FROM            Groups
                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getbuadmin">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		<cfquery name="getouadmin" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'ou admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getouadmin">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		
		<cfif getincidentdetails.status eq "More Info Requested">
		
		<cfif getincidentdetails.investigationlevel eq 2 or getincidentdetails.needfa eq 1>
		
		<cfquery name="getsrreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getsrreviewer">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		
		</cfif>
		
		<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
		
		</cfif>
		
		
		
		<cfset viewerlist[ctr][1] = "Initiator">
		<cfset viewerlist[ctr][2] = getincidentdetails.createdBy>
		
		
		
		<cfset getwhocansee = QueryNew("userrole, name")>
		<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
			<cfset QueryAddRow( getwhocansee ) />
			<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
			<cfset getwhocansee["name"][i] = viewerlist[i][2]>
		</cfloop>
	</cfcase>
	<cfcase value="cancel">
		<cfset getwhocansee = QueryNew("userrole, name")>
		<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
			<cfset QueryAddRow( getwhocansee ) />
			<cfset getwhocansee["userrole"][i] = "" />
			<cfset getwhocansee["name"][i] = "">
		</cfloop>
	</cfcase>
	<cfcase value="Sent for Review">
	
	
	
	
		<cfset ctr = 1>
		<cfquery name="getglbadmin" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'global admin') AND (Users.Status = 1)
		</cfquery>
		<cfloop query="getglbadmin">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		<cfquery name="getbuadmin" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'bu admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
                             (SELECT        BUid
                               FROM            Groups
                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getbuadmin">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		<cfquery name="getouadmin" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'ou admin') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getouadmin">
			<cfset viewerlist[ctr][1] = UserRole>
			<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
			<cfset ctr = ctr + 1>
		</cfloop>
		
		<!--- <cfif getincidentdetails.needFA eq 1>
		
		
		
		</cfif> --->
		<cfquery name="getreviewer" datasource="#request.dsn#">
						SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
						FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
						WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
					</cfquery>
					<cfloop query="getreviewer">
						<cfset viewerlist[ctr][1] = UserRole>
						<cfset viewerlist[ctr][2] = "#Firstname# #lastname#">
						<cfset ctr = ctr + 1>
					</cfloop>
		<cfset viewerlist[ctr][1] = "Initiator">
		<cfset viewerlist[ctr][2] = getincidentdetails.createdBy>
		<cfset getwhocansee = QueryNew("userrole, name")>
		<cfloop from="1" to="#arraylen(viewerlist)#" index="i">
			<cfset QueryAddRow( getwhocansee ) />
			<cfset getwhocansee["userrole"][i] = viewerlist[i][1] />
			<cfset getwhocansee["name"][i] = viewerlist[i][2]>
		</cfloop>
		
	</cfcase>
</cfswitch>
</cfif>
<cfset fanextuser = "">
<cfif trim(getstat.status) eq ''>
<cfset fanextuser = getincidentdetails.createdBy>
<cfelse>
<cfswitch expression="#getstat.status#">
	<cfcase value="Started,Initiated,More info Requested,SR More Info">
		<cfset fanextuser = getincidentdetails.createdBy>
	</cfcase>
	<cfcase value="Sent for Review">
		<cfquery name="getreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
                      UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                          (SELECT        OUid
                            FROM            Groups
                            WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getreviewer">
			<cfset fanextuser = listappend(fanextuser,"#firstname# #lastname#")>
		</cfloop>
	</cfcase>
	<cfcase value="Sent for Approval">
		<cfquery name="getreviewer" datasource="#request.dsn#">
			SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
			FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
			WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
		</cfquery>
		<cfloop query="getreviewer">
			<cfset fanextuser = listappend(fanextuser,"#firstname# #lastname#")>
		</cfloop>
	</cfcase>
	
</cfswitch>
</cfif>
<cfset irpnextuser = "">

<cfif trim(getirpstat.status) eq ''><cfset irpnextuser = getincidentdetails.createdBy><cfelse>
					<cfswitch expression="#getirpstat.status#">
						<cfcase value="Started,Initiated">
							<cfset irpnextuser = getincidentdetails.createdBy>
						</cfcase>
						<cfcase value="Sent for Review">
							<cfquery name="getreviewer" datasource="#request.dsn#">
								SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
								FROM            Users INNER JOIN
	                       UserRoles ON Users.UserId = UserRoles.UserID
								WHERE        (UserRoles.UserRole = 'senior reviewer') AND (Users.Status = 1) AND (UserRoles.AssignedLocs =
	                           (SELECT        business_line_id
	                             FROM            Groups
	                             WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentdetails.groupnumber#">)))
							</cfquery>
						
							<cfloop query="getreviewer">
								<cfset irpnextuser = listappend(irpnextuser,"#firstname# #lastname#")>
								<!--- <cfset QueryAddRow( getwhocansee ) />
								<cfset getwhocansee["userrole"][i] = "Senior Reviewer" />
								<cfset getwhocansee["name"][i] = "#firstname# #lastname#">
								 --->
							</cfloop>
						</cfcase>
						<cfcase value="More Info Requested">
							<cfset irpnextuser = getincidentdetails.createdBy>
						</cfcase>
						
					</cfswitch>
					</cfif>

