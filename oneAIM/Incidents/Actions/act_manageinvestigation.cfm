<cfparam name="submittype" default="">
<cfparam name="closesave" default="no">
<cfswitch expression="#submittype#">
	<cfcase value="addaz">
		<cfparam name="causeid" default="0">
		<cfparam name="subcauseid" default="0">
		<cfparam name="irn" default="0">
		<cfparam name="aztype" default="0">
		<cfparam name="invid" default="0">
		<cfif trim(causeid) neq 0 and trim(subcauseid) neq 0 and trim(aztype) neq 0 and trim(invid) neq 0>
			<cfquery name="addaz" datasource="#request.dsn#">
				insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#aztype#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#causeid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#subcauseid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#actomm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#comment#">)
			</cfquery>
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.investigation#" name="retfrm" method="post">
				<input type="hidden" name="irn" value="#irn#">
			</form>
			<script type="text/javascript">
				document.retfrm.submit();
			</script>
		</cfoutput>
	</cfcase>
	
	<cfcase value="addrowroot">
			<cfparam name="actiontype" default="">
			<cfparam name="invazid" default="0">
			<cfparam name="causeid" default="">
			<cfparam name="subcauseid" default="">
			<cfparam name="irn" default="">
			<cfparam name="aztype" default="">
			<cfparam name="invid" default="">
		<cfswitch expression="#actiontype#">
				<cfcase value="addrowroot">
			
			<cfif trim(causeid) neq '' and trim(subcauseid) neq '' and trim(aztype) neq '' and trim(invid) neq ''>
				<cfif urldecode(comment) contains "cfaddedpercent">
					<CFSET comment  = Replace(urldecode(comment), "cfaddedpercent", "%", "ALL")>
				<cfelse>
					<CFSET comment = urldecode(comment)>
				</cfif>
				
				<cfquery name="addaz" datasource="#request.dsn#">
					insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#aztype#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#causeid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#subcauseid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#actomm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#comment#">)
				</cfquery>
			</cfif>
				</cfcase>
				<cfcase value="deleteazroot">
					<cfif trim(invazid) neq 0>
						<cfquery name="getaz" datasource="#request.dsn#">
							delete from incidentaz
							where IncAZid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invazid#">
						</cfquery>
					</cfif>
				</cfcase>
				<cfcase value="copyrootaz">
					<cfif trim(invazid) neq 0>
						<cfquery name="getaz" datasource="#request.dsn#">
							select invid, aztype, azcatid, azfactorid,AZSubCause, azcomment
							from incidentaz
							where IncAZid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invazid#">
						</cfquery>
						<cfif getaz.recordcount eq 1>
							<cfquery name="addaz" datasource="#request.dsn#">
								insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
								values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.invid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.aztype#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.azcatid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.azfactorid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getaz.AZSubCause#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getaz.azcomment#">)
							</cfquery>
						</cfif>
					</cfif>
				</cfcase>
	</cfswitch>
	<cfquery name="getinvdetail" datasource="#request.dsn#">
				SELECT        IncidentInvestigation.InvID, IncidentInvestigation.IRN, IncidentInvestigation.SrSiteRep, IncidentInvestigation.InvCondBy, IncidentInvestigation.TeamMembers, 
                         IncidentInvestigation.InvSummary, IncidentInvestigation.DirSupervisor, IncidentInvestigation.InvRelevance, IncidentInvestigation.rulebreach, 
                         IncidentInvestigation.whynorulebreach, IncidentInvestigation.essentialbreach, IncidentInvestigation.whynoessbreach, IncidentAZ.AZType, IncidentAZ.IncAZid, 
                         IncidentAZ.AZCatID, IncidentAZ.AZFactorID, IncidentAZ.AZSubCause, IncidentAZ.AZComment, IncidentAZ.DateCreated, AZfactors.CategoryLetter
				FROM            AZfactors RIGHT OUTER JOIN
                         IncidentAZ ON AZfactors.FactorID = IncidentAZ.AZFactorID RIGHT OUTER JOIN
                         IncidentInvestigation ON IncidentAZ.InvID = IncidentInvestigation.InvID
				WHERE IncidentInvestigation.InvID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">
				ORDER BY IncidentAZ.DateCreated
			</cfquery>
			<cfquery name="getaz" datasource="#request.dsn#">
				SELECT        CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
				FROM            AZcategories
				ORDER BY SubCauseID, CategoryLetter
			</cfquery>
			<cfquery name="getazfactors" datasource="#request.dsn#">
				SELECT        FactorID, CategoryLetter, FactorName, FactorNumber, status
				FROM            AZfactors
				WHERE        (status = 1)
			</cfquery>
			<cfquery name="getazcatgories" datasource="#request.dsn#">
				SELECT      CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
				FROM            AZcategories
			</cfquery>
			<cfset azcatstruct = {}>
			<cfset azfactorstruct = {}>
			
			<cfloop query="getazcatgories">
				<cfset azcatstruct[CategoryID] = "#CategoryLetter# #CategoryName#">
			</cfloop>
			
			<cfloop query="getazfactors">
				<cfset azfactorstruct[factorid] = "#CategoryLetter#-#FactorNumber# #FactorName#">
			</cfloop>
	{zz{<table width="100%" cellpadding="1" cellspacing="0" border="0">
						
						
				<tr>
					<td class="formlable" width="25%" align="center"><strong>Root Cause Type</strong></td>
					<td class="formlable" width="25%" align="center"><strong>Root Cause Sub-Type</strong></td>
					<td class="formlable" width="25%" align="center"><strong>Personal/Job Factors</strong></td>
					<td class="formlable" width="25%" align="center"><strong>Root Cause Description</strong></td>
					<td class="formlable"></td>
				</tr>
				
				<tr id="rootrow1">
				<input type="hidden" name="atyperoot" value="2"><!--- Immediate Cause --->
					<td class="bodyTextGrey" width="25%" align="center" id="rootrowa1"><select name="rootcause1a"  size="1" class="selectgen" id="rootcause1" onchange="buildsubtyperoot(this.value,1);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfoutput query="getaz"><cfif listfind("3,4",SubCauseID) gt 0><option value="#CategoryID#">#CategoryLetter# #CategoryName#</option></cfif></cfoutput>
						</select></td>
						<td id="rootsubtype1" align="center" width="25%"><select name="rootsubtype1a"  size="1" class="selectgen" id="rootsubtypef1" style="width:190px;">
						<option value="">-- Select One --</option>
						</select></td>
						<td id="rootactomm1" align="center" width="25%"><input type="text" name="rootactom1" size="22" class="selectgen" id="rootactom1" readonly><!--- <select name="rootactom1" size="1" class="selectgen" id="rootactom1" style="width:190px;">
						<option value="">-- Select One --</option>
						</select> ---></td>
						<td id="rootcomm1" align="center" width="25%"><textarea name="rootcomm1a" id="rootcommf1" class="selectgen" cols="30" rows="2"></textarea></td>
						<td class="bodytextsm" align="left" colspan="4"><a onclick="addrowroot('addrowroot');" href="javascript:void(0);">Add&nbsp;Row</a></td>
				</tr>
						<!--- </table>
						</td> 
				</tr>--->
				<cfoutput query="getinvdetail">
		
				<cfif aztype eq 2>
				<tr>
					<td class="bodyTextGrey" width="25%" align="center">
					<select name="rootcause_#IncAZid#"  size="1" class="selectgen" id="rootcause_#IncAZid#" onchange="buildsubtyperoot(this.value,#IncAZid#);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfloop query="getaz"><cfif listfind("3,4",SubCauseID) gt 0><option value="#CategoryID#" <cfif categoryid eq getinvdetail.azcatid>selected</cfif>>#CategoryLetter# #CategoryName#</option></cfif></cfloop>
						</select><!--- 
					
					<cfif structkeyexists(azcatstruct,azcatid)>#azcatstruct[azcatid]#</cfif> ---></td>
					<td id="rootsubtype#IncAZid#" align="center" width="25%"><select name="rootsubtype_#IncAZid#" size="1" class="selectgen" id="rootsubtype_#IncAZid#"  style="width:190px;" >
					<option value="">-- Select One --</option>
					<cfloop query="getazfactors"><cfif getazfactors.CategoryLetter eq getinvdetail.CategoryLetter><option value="#FactorID#" <cfif factorid eq getinvdetail.azfactorid>selected</cfif>>#CategoryLetter#-#FactorNumber# #FactorName#</option></cfif></cfloop>
					</select></td>
					<!--- <td  width="25%" align="left"><cfif structkeyexists(azfactorstruct,azfactorid)>#azfactorstruct[azfactorid]#</cfif></td> --->
					<td  width="25%" align="center"  id="rootactomm#IncAZid#"><input type="text" name="rootactom_#IncAZid#" size="22" class="selectgen" id="rootactom_#IncAZid#" readonly  value="#AZSubCause#">
					<!--- <select name="rootactom_#IncAZid#" size="1" class="selectgen" id="rootactom_#IncAZid#" style="width:190px;">
						<option value="#AZSubCause#">#AZSubCause#</option>
						</select> --->
					<!--- #azsubcause# ---></td>
					<td  width="25%" align="center"><textarea name="rootcomm_#IncAZid#" id="rootcommf#incazid#" class="selectgen" cols="30" rows="2">#azcomment#</textarea></td>
					<td class="bodytextsm" align="left"><a onclick="deleteazroot(#IncAZid#,#irn#);" href="javascript:void(0);">Delete</a>&nbsp;&nbsp;&nbsp;<br><a onclick="copyrootaz(#IncAZid#,#irn#);" href="javascript:void(0);">Copy</a></td>
				</tr>
				</cfif>
				
				</cfoutput>
				</table>}zz}
	</cfcase>
	<cfcase value="addrowimm">
			<cfparam name="actiontype" default="">
			<cfparam name="invazid" default="0">
			<cfparam name="causeid" default="">
			<cfparam name="subcauseid" default="">
			<cfparam name="irn" default="">
			<cfparam name="aztype" default="">
			<cfparam name="invid" default="">
			
			<cfswitch expression="#actiontype#">
				<cfcase value="addrowimm">
			
			<cfif trim(causeid) neq '' and trim(subcauseid) neq '' and trim(aztype) neq '' and trim(invid) neq ''>
				<cfif urldecode(comment) contains "cfaddedpercent">
					<CFSET comment  = Replace(urldecode(comment), "cfaddedpercent", "%", "ALL")>
				<cfelse>
					<CFSET comment = urldecode(comment)>
				</cfif>
				<cfquery name="addaz" datasource="#request.dsn#">
					insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#aztype#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#causeid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#subcauseid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#actomm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#comment#">)
				</cfquery>
			</cfif>
				</cfcase>
				<cfcase value="deleteaz">
					<cfif trim(invazid) neq 0>
						<cfquery name="getaz" datasource="#request.dsn#">
							delete from incidentaz
							where IncAZid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invazid#">
						</cfquery>
					</cfif>
				</cfcase>
				<cfcase value="copyaz">
					<cfif trim(invazid) neq 0>
						<cfquery name="getaz" datasource="#request.dsn#">
							select invid, aztype, azcatid, azfactorid,AZSubCause, azcomment
							from incidentaz
							where IncAZid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invazid#">
						</cfquery>
						<cfif getaz.recordcount eq 1>
							<cfquery name="addaz" datasource="#request.dsn#">
								insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
								values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.invid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.aztype#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.azcatid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getaz.azfactorid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getaz.AZSubCause#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getaz.azcomment#">)
							</cfquery>
						</cfif>
					</cfif>
				</cfcase>
				</cfswitch>
				
			<cfquery name="getinvdetail" datasource="#request.dsn#">
				SELECT        IncidentInvestigation.InvID, IncidentInvestigation.IRN, IncidentInvestigation.SrSiteRep, IncidentInvestigation.InvCondBy, IncidentInvestigation.TeamMembers, 
                         IncidentInvestigation.InvSummary, IncidentInvestigation.DirSupervisor, IncidentInvestigation.InvRelevance, IncidentInvestigation.rulebreach, 
                         IncidentInvestigation.whynorulebreach, IncidentInvestigation.essentialbreach, IncidentInvestigation.whynoessbreach, IncidentAZ.AZType, IncidentAZ.IncAZid, 
                         IncidentAZ.AZCatID, IncidentAZ.AZFactorID, IncidentAZ.AZSubCause, IncidentAZ.AZComment, IncidentAZ.DateCreated, AZfactors.CategoryLetter
				FROM            AZfactors RIGHT OUTER JOIN
                         IncidentAZ ON AZfactors.FactorID = IncidentAZ.AZFactorID RIGHT OUTER JOIN
                         IncidentInvestigation ON IncidentAZ.InvID = IncidentInvestigation.InvID
				WHERE IncidentInvestigation.InvID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">
				ORDER BY IncidentAZ.DateCreated
			</cfquery>
			<cfquery name="getaz" datasource="#request.dsn#">
				SELECT        CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
				FROM            AZcategories
				ORDER BY SubCauseID, CategoryLetter
			</cfquery>
			<cfquery name="getazfactors" datasource="#request.dsn#">
				SELECT        FactorID, CategoryLetter, FactorName, FactorNumber, status
				FROM            AZfactors
				WHERE        (status = 1)
			</cfquery>
			<cfquery name="getazcatgories" datasource="#request.dsn#">
				SELECT      CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
				FROM            AZcategories
			</cfquery>
			<cfset azcatstruct = {}>
			<cfset azfactorstruct = {}>
			
			<cfloop query="getazcatgories">
				<cfset azcatstruct[CategoryID] = "#CategoryLetter# #CategoryName#">
			</cfloop>
			
			<cfloop query="getazfactors">
				<cfset azfactorstruct[factorid] = "#CategoryLetter#-#FactorNumber# #FactorName#">
			</cfloop>
			{zz{<table width="100%" cellpadding="3" cellspacing="0" border="0">
						
						
				<tr>
					<td class="formlable" width="25%" align="center"><strong>Immediate Cause Type</strong></td>
					<td class="formlable" width="25%" align="center"><strong>Immediate Cause Sub-Type</strong></td>
					<td class="formlable" width="25%" align="center"><strong>Acts or Omissions/Conditions</strong></td>
					<td class="formlable" width="25%" align="center"><strong>Immediate Cause Description</strong></td>
					<td class="formlable"></td>
				</tr>
				
				<tr id="immrow1">
				<input type="hidden" name="atype" value="1"><!--- Immediate Cause --->
					<td class="bodyTextGrey" width="25%" align="center" id="immrowa1"><select name="immcause1"  size="1" class="selectgen" id="immcause1" onchange="buildsubtype(this.value,1);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfoutput query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#">#CategoryLetter# #CategoryName#</option></cfif></cfoutput>
						</select></td>
						<td id="immsubtype1" align="center" width="25%"><select name="immsubtype1"  size="1" class="selectgen" id="immsubtypef1" style="width:190px;">
						<option value="">-- Select One --</option>
						</select></td>
						<td id="actomm1" align="center" width="25%"><input type="text" name="actom1" size="22" class="selectgen" id="actom1" readonly ><!--- <select name="actom1" size="1" class="selectgen" id="actom1" style="width:190px;">
						<option value="">-- Select One --</option>
						</select> ---></td>
						<td id="comm1" align="center" width="25%"><textarea name="comm1f" id="commf1" class="selectgen" cols="30" rows="2"></textarea></td>
						<td class="bodytextsm" align="left" colspan="4"><a onclick="addrow('addrowimm');" href="javascript:void(0);">Add&nbsp;Row</a></td>
				</tr>
				<cfoutput query="getinvdetail">
				<cfif aztype eq 1>
				<tr>
					<td class="bodyTextGrey" width="25%" align="center">
					<select name="immcause_#IncAZid#"  size="1" class="selectgen" id="immcause_#IncAZid#" onchange="subtypeImm(this.value,#IncAZid#);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfloop query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#" <cfif categoryid eq getinvdetail.azcatid>selected</cfif>>#CategoryLetter# #CategoryName#</option></cfif></cfloop>
						</select><!--- 
					
					<cfif structkeyexists(azcatstruct,azcatid)>#azcatstruct[azcatid]#</cfif> ---></td>
					<td id="immsubtype#IncAZid#" align="center" width="25%"><select name="immsubtype_#IncAZid#" size="1" class="selectgen" id="immsubtype_#IncAZid#"  style="width:190px;" >
					<option value="">-- Select One --</option>
					<cfloop query="getazfactors"><cfif getazfactors.CategoryLetter eq getinvdetail.CategoryLetter><option value="#FactorID#" <cfif factorid eq getinvdetail.azfactorid>selected</cfif>>#CategoryLetter#-#FactorNumber# #FactorName#</option></cfif></cfloop>
					</select></td>
					<!--- <td  width="25%" align="left"><cfif structkeyexists(azfactorstruct,azfactorid)>#azfactorstruct[azfactorid]#</cfif></td> --->
					<td  width="25%" align="center"  id="actomm#IncAZid#"><input type="text" name="actom_#IncAZid#" size="22" class="selectgen" id="actom_#IncAZid#" readonly  value="#AZSubCause#">
					<!--- <select name="actom_#IncAZid#" size="1" class="selectgen" id="actom_#IncAZid#" style="width:190px;">
						<option value="#AZSubCause#">#AZSubCause#</option>
						</select> --->
					<!--- #azsubcause# ---></td>
					<td  width="25%" align="center"><textarea name="comm_#IncAZid#" id="commf1" class="selectgen" cols="30" rows="2">#azcomment#</textarea></td>
					<td class="bodytextsm" align="left"><a onclick="deleteaz(#IncAZid#,#irn#);" href="javascript:void(0);">Delete</a>&nbsp;&nbsp;&nbsp;<br><a onclick="copyaz(#IncAZid#,#irn#);" href="javascript:void(0);">Copy</a></td>
				</tr>
				</cfif>
				
				</cfoutput>
				</table>}zz}
		</cfcase>
		<cfcase value="updateinv">
			<cfparam name="irn" default="0">
			<cfparam name="theinvid" default="0">
			<cfparam name="essbreach" default="0">
			<cfparam name="rulebreach" default="0">
			<cfparam name="srsiterep" default="">
			<cfparam name="conductedbt" default="">
			<cfparam name="invsummary" default="">
			<cfparam name="dirsuper" default="">
			<cfparam name="daysinceoff" default="">
			<cfparam name="rbwhy" default="">
			<cfparam name="esswhy" default="">
			<cfparam name="sendreview" default="no">
			<cfparam name="ruleother" default="">
			<cfif trim(irn) gt 0 and trim(theinvid) gt 0>
			<cftransaction>
				<cfquery name="updateinvrec" datasource="#request.dsn#">
					update IncidentInvestigation
					set SrSiteRep = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#srsiterep#">, 
					InvCondBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#conductedbt#">, 
					InvSummary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invsummary#">, 
					DirSupervisor = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#dirsuper#">, 
					daysinceoff = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#daysinceoff#">, 
					rulebreach = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#rulebreach#">, 
					whynorulebreach = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#rbwhy#">, 
					essentialbreach = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#essbreach#">, 
					whynoessbreach = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#esswhy#">,
					ruleother = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ruleother#">
					where InvID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#theinvid#">
				</cfquery>
				<cfquery name="delteam" datasource="#request.dsn#">
					delete FROM            InvestigationTeamMembers
					where invid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theinvid#">
				</cfquery>
				<cfparam name="teammem_0" default="">
				<cfparam name="INVRELEVANCE_0" default=""><!---  and trim(INVRELEVANCE_0) neq ''  --->
				<cfif trim(teammem_0) neq ''>
					<cfquery name="addteammem" datasource="#request.dsn#">
						insert into InvestigationTeamMembers (MemberName, RelevenceToInv, IRN, invid)
						values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["teammem_0"]#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["INVRELEVANCE_0"]#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theinvid#">)
					</cfquery>
				</cfif><!---  and trim('#form["INVRELEVANCE_#i#"]#') neq '' --->
				<cfloop from="1" to="20" index="i">
					<cfif trim('#form["teammem_#i#"]#') neq ''>
					<cfquery name="addteammem" datasource="#request.dsn#">
						insert into InvestigationTeamMembers (MemberName, RelevenceToInv, IRN, invid)
						values (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["teammem_#i#"]#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["INVRELEVANCE_#i#"]#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theinvid#">)
					</cfquery>
					</cfif>
				</cfloop>
				
				<cfparam name="immcause1" default="0">
				<cfparam name="immsubtype1" default="0">
				<cfparam name="actom1" default="">
				<cfparam name="comm1f" default="">
				<cfif trim(immcause1) gt 0 and trim(immsubtype1) gt 0 and trim(actom1) neq '' and trim(comm1f) neq ''>
					<cfquery name="addaz" datasource="#request.dsn#">
						insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theinvid#">,1,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#immcause1#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#immsubtype1#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#actom1#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#comm1f#">)
				</cfquery>
				</cfif> 
				<!--- rootcause1a rootsubtype1a rootactom1 rootcomm1a --->
				<!--- <cfoutput>(#rootcause1a#) (#rootsubtype1a#) (#rootactom1#) (#rootcomm1a#)</cfoutput><cfabort> --->
				<cfparam name="rootcause1a" default="0">
				<cfparam name="rootsubtype1a" default="0">
				<cfparam name="rootactom1" default="">
				<cfparam name="rootcomm1a" default="">
				<cfif trim(rootcause1a) gt 0 and trim(rootsubtype1a) gt 0 and trim(rootactom1) neq '' and trim(rootcomm1a) neq ''>
					<cfquery name="addaz" datasource="#request.dsn#">
						insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theinvid#">,2,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#rootcause1a#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#rootsubtype1a#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#rootactom1#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#rootcomm1a#">)
				</cfquery>
				</cfif> 
				
				
				<cfoutput>
				<cfloop list="#fieldnames#" index="c">
					<cfif c contains "immcause_">
						<cfset theazid = listgetat(c,"2","_")>
							<cfif trim('#form["immcause_#theazid#"]#') gt 0 and trim('#form["immsubtype_#theazid#"]#') gt 0 and trim('#form["actom_#theazid#"]#') neq '' and trim('#form["comm_#theazid#"]#') neq ''>
								<cfquery name="updateaz" datasource="#request.dsn#">
									update incidentaz
									set azcatid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form["immcause_#theazid#"]#">,
									azfactorid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form["immsubtype_#theazid#"]#">,
									AZSubCause = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["actom_#theazid#"]#">,
									azcomment = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["comm_#theazid#"]#">
									where IncAZid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theazid#">
								</cfquery>
							</cfif>
					</cfif>
					
					<cfif c contains "rootcause_">
						<cfset theazid = listgetat(c,"2","_")>
							<cfif trim('#form["rootcause_#theazid#"]#') gt 0 and trim('#form["rootsubtype_#theazid#"]#') gt 0 and trim('#form["rootactom_#theazid#"]#') neq '' and trim('#form["rootcomm_#theazid#"]#') neq ''>
								<cfquery name="updateaz" datasource="#request.dsn#">
									update incidentaz
									set azcatid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form["rootcause_#theazid#"]#">,
									azfactorid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#form["rootsubtype_#theazid#"]#">,
									AZSubCause = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["rootactom_#theazid#"]#">,
									azcomment = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#form["rootcomm_#theazid#"]#">
									where IncAZid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#theazid#">
								</cfquery>
							</cfif>
					</cfif>
					
				</cfloop>
				</cfoutput>
				
				
			</cftransaction>
			
			<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_manageinvestigation.cfm", "uploads\incidents\#irn#\Investigation"))>
				<cfif not directoryexists(filedir)>
					<cfdirectory action="CREATE" directory="#filedir#">
				</cfif>
				<cfif trim(uploadfile1) neq ''>
					<cffile action="upload" filefield="uploadfile1" destination="#FileDir#" result="file_result1" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result1.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result1.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(uploadfile2) neq ''>
					<cffile action="upload" filefield="uploadfile2" destination="#FileDir#" result="file_result2" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result2.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result2.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(uploadfile3) neq ''>
					<cffile action="upload" filefield="uploadfile3" destination="#FileDir#" result="file_result3" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result3.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result3.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(uploadfile4) neq ''>
					<cffile action="upload" filefield="uploadfile4" destination="#FileDir#" result="file_result4" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result4.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result4.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(uploadfile5) neq ''>
					<cffile action="upload" filefield="uploadfile5" destination="#FileDir#" result="file_result5" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result5.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result5.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
			
			
			<cfif sendreview eq "yes">
			
			
			<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
				
			<cfset tolistemail = "">
				<cfset tolistnames = "">
			<cfif getemaildata.invstatus eq "Senior More Info Requested">
			
			<cfquery name="updateinv" datasource="#request.dsn#">
				update IncidentInvestigation
				set status = 'Sent for Senior Review'
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			
			<cfquery name="getsrreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND UserRoles.AssignedLocs in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.business_line_id#" list="Yes">)
				</cfquery>
			
			<cfelse>
				
			<cfquery name="updateinv" datasource="#request.dsn#">
				update IncidentInvestigation
				set status = 'Sent for Review'
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			<!--- <cfif getemaildata.InvestigationLevel eq 1> --->
				<cfquery name="getreviewer" datasource="#request.dsn#">
				SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND UserRoles.AssignedLocs in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#" list="Yes">)
				</cfquery>
			</cfif>
				<cfif trim(getemaildata.reviewerEmail) neq ''>
				<!--- <cfloop query="getreviewer"> --->
				<!--- <cfset cclist = listappend(cclist,getemaildata.reviewerEmail)> --->
				
					<cfset tolistemail = listappend(tolistemail,getemaildata.reviewerEmail)>
					<cfset tolistnames =  listappend(tolistnames,getemaildata.HSSEManager)>
				<!--- </cfloop> --->
				</cfif>
			<!--- <cfelseif getemaildata.InvestigationLevel eq 2>
				<cfquery name="getsrreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND UserRoles.AssignedLocs in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.business_line_id#" list="Yes">)
				</cfquery>
				<cfif getsrreviewer.recordcount gt 0>
				<cfloop query="getsrreviewer">
					<cfset tolistemail = listappend(tolistemail,Useremail)>
					<cfset tolistnames = listappend(tolistnames,"#Firstname# #lastname#")>
				</cfloop>
				</cfif>
			</cfif> --->
			
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation sent for review by #request.fname# #request.lname# to #tolistnames#">)
				</cfquery>
				<cfif trim(tolistemail) neq ''>
				<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Review incident investigation details">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Review incident investigation details">
												</cfif>
				<cfmail to="#tolistemail#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													Investigation information has been entered for the following incident which is now awaiting your review. <br>Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
													</cfif>
			
			</cfif>
			
			
			
		<!--- <cfoutput>
			<form action="#self#?fuseaction=main.main" name="retfrm" method="post">
				<input type="hidden" name="irn" value="#irn#">
			</form>
			<script type="text/javascript">
				document.retfrm.submit();
			</script>
		</cfoutput> --->
		
		
		<cfif closesave eq "no">
				<cfoutput>
				<form action="#self#?fuseaction=incidents.investigation" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script> 
				</cfoutput>
			<cfelse>
				<cfoutput>
				<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
					
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script> 
				</cfoutput>
			</cfif>
			</cfif>
		</cfcase>
		<cfcase value="deleteinvfile">
			<cfparam name="filename" default="">
			<cfset irpmsg = 0>
			<cfif irn neq 0 and trim(filename) neq ''>
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_manageinvestigation.cfm", "uploads\incidents\#irn#\Investigation"))>
				
				<cfif fileexists("#filedir#\#filename#")>
					<cftry>
					<cffile action="DELETE" file="#filedir#\#filename#">
					<cfcatch type="Any">
					<cfset irpmsg = 1>
					</cfcatch>
					</cftry>
				</cfif>
			</cfif>
			<cfoutput>
				<form action="#self#?fuseaction=#attributes.xfa.investigation#" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="irpmsg" value="#irpmsg#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script>
			</cfoutput>
	
		</cfcase>
</cfswitch>