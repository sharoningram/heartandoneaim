


<cfset mainctr = mainctr+1>

<cfset gname = "">
<cfloop query="getgroups">
	<cfif getincidentdetails.groupnumber eq group_number><cfset gname = group_name></cfif>
</cfloop>

<cfif trim(getincidentdetails.bsname) neq ''><cfset bsname = getincidentdetails.bsname><cfelse><cfset bsname = "Not Applicable"></cfif>

<cfset assignto = "">
<cfloop query="getincassignto">
	<cfif getincidentdetails.incassignedto eq IncAssignedID><cfset assignto = IncAssignedTo></cfif>
</cfloop>

<cfset conname = "">
<cfif getincidentdetails.Contractor neq 0>	
	<cfif isdefined("getincsublist.recordcount")>
		<cfloop query="getincsublist"><cfif getincidentdetails.Contractor eq ContractorID><cfset conname = ContractorName></cfif></cfloop>
	</cfif>
<cfelse>
	<cfif listfind("3,4,11",getincidentdetails.incassignedto) gt 0>
		<cfset conname = "Not Listed"><cfif trim(getincidentdetails.otherContName) neq ''><cfset conname = conname & ":&nbsp;#getincidentdetails.otherContName#"></cfif>
	</cfif>
</cfif>

<cfset wlocation = "">
<cfif getincidentdetails.occuratlocation eq "No">
	<cfloop query="getincplaces"><cfif getincidentdetails.whereoccur eq PlaceID><cfset wlocation = IncPlace></cfif></cfloop>
</cfif>

<cfset pinctype = "">
<cfloop query="getinctypes">
	<cfif listfindnocase("P,PS",IncTypeDsp) gt 0>
		<cfif getincidentdetails.PrimaryType eq IncTypeID><cfset pinctype = IncType></cfif>
	</cfif>
</cfloop>

<cfset sinjtype = "">
<cfset oid = "">

<cfif getincidentdetails.secondarytype neq 0 or getincidentdetails.oidefinition neq 0>
	<cfif getincidentdetails.secondarytype neq 0>
		<cfloop query="getinctypes">
			<cfif listfindnocase("S,PS",IncTypeDsp) gt 0>
				<cfif getincidentdetails.secondarytype eq IncTypeID><cfset sinjtype = IncType></cfif>
			</cfif>
		</cfloop>
	</cfif>
	<cfif getincidentdetails.oidefinition neq 0>
		<cfloop query="getoidef"><cfif getincidentdetails.oidefinition eq OIdefID><cfset oid = OIdef></cfif></cfloop>
	</cfif>
</cfif>

<cfset weathtype = "">
<cfif trim(getincidentdetails.weather) gt 0>
	<cfloop query="getweather"><cfif getincidentdetails.weather eq WeatherCondID><cfset weathtype = weathercond></cfif></cfloop>
</cfif>

<cfset increpby = "">
<cfif trim(getincidentdetails.reportedby) neq ''>
	<cfset increpby = getincidentdetails.reportedby>
</cfif>
<cfif trim(getincidentdetails.nonamecfwemail) neq ''>
	<cfset increpby = getincidentdetails.nonamecfwemail>
</cfif>

<cfset inlev = "">
<cfif getincidentdetails.investigationlevel eq "1"><cfset inlev = "Level 1"><cfelseif getincidentdetails.investigationlevel eq "2"><cfset inlev = "Level 2"></cfif>

<cfset stbody = "">
<cfset stbodydate = "">
<cfif getincidentdetails.wasreported eq "Yes">
	<cfset stbody = getincidentdetails.StatutoryBody>
	<cfset stbodydate = dateformat(getincidentdetails.datestatreported,sysdateformat)>
</cfif>
		
<cfset noticetype = "">
<cfif getincidentdetails.EnforcementNotice eq "Yes">
	<cfset noticetype = getincidentdetails.EnforcementNoticeType>
</cfif>

<cfset injsrc = "">
<cfset injsrcoth = "">
<cfif getincidentdetails.primarytype eq 1 or getincidentdetails.secondarytype eq 1>
	<cfoutput query="getinjurysources">
		<cfif listfind(getincidentdetails.HazardSource,InjSourceID) gt 0><cfset injsrc = injurysource></cfif>
	</cfoutput>
	<cfif trim(getincidentdetails.otherHazard) neq ''>
		<cfset injsrcoth = getincidentdetails.otherHazard>
	</cfif>
</cfif>

<cfset doH = "">
<cfset doW = "">
<cfset doE = "">
<cfset doC = "">
<cfset rlate = "">
<cfif getincidentdetails.droppedObject eq "Yes">
	<cfset doH = getincidentdetails.DOheight>
	<cfset doW = getincidentdetails.DOweight>
	<cfset doE = getincidentdetails.DOenergy>
		<cfif getincidentdetails.DOcategory eq "A"><cfset doC = "A - Minor">
		<cfelseif getincidentdetails.DOcategory eq "B"><cfset doC = "B - Important">
		<cfelseif getincidentdetails.DOcategory eq "C"><cfset doC = "C - High Potential">
		<cfelseif getincidentdetails.DOcategory eq "D"><cfset doC = "D - High Potential"></cfif>
</cfif>
<cfif trim(getincidentdetails.reasonlate) neq ''>
	<cfset rlate = getincidentdetails.reasonlate>
</cfif>

<cfscript> 
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.trackingnum#",#mainctr#,1);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.createdby#",#mainctr#,2);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.buname#",#mainctr#,3);
	SpreadsheetSetCellValue(incSheet,"#dateformat(getincidentdetails.datecreated,sysdateformat)#",#mainctr#,4);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.ouname#",#mainctr#,5);
	SpreadsheetSetCellValue(incSheet,"#gname#",#mainctr#,6);
	SpreadsheetSetCellValue(incSheet,"#bsname#",#mainctr#,7);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.sitename#",#mainctr#,8);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.CountryName#",#mainctr#,9);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.isworkrelated#",#mainctr#,10);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.ClientName#",#mainctr#,11);
	SpreadsheetSetCellValue(incSheet,"#assignto#",#mainctr#,12);
	SpreadsheetSetCellValue(incSheet,"#conname#",#mainctr#,13);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.OtherAssignedTo#",#mainctr#,14);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.occuratlocation#",#mainctr#,15);
	SpreadsheetSetCellValue(incSheet,"#wlocation#",#mainctr#,16);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.SpecificLocation#",#mainctr#,17);
	SpreadsheetSetCellValue(incSheet,"#pinctype#",#mainctr#,18);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.isnearmiss#",#mainctr#,19);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.issecondary#",#mainctr#,20);
	SpreadsheetSetCellValue(incSheet,"#sinjtype#",#mainctr#,21);
	SpreadsheetSetCellValue(incSheet,"#oid#",#mainctr#,22);
	SpreadsheetSetCellValue(incSheet,"#dateformat(getincidentdetails.incidentdate,sysdateformat)#",#mainctr#,23);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.incidenttime#",#mainctr#,24);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.dayofweek#",#mainctr#,25);
	SpreadsheetSetCellValue(incSheet,"#weathtype#",#mainctr#,26);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.shortdesc#",#mainctr#,27);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.reportedbyco#",#mainctr#,28);
	SpreadsheetSetCellValue(incSheet,"#increpby#",#mainctr#,29);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.description#",#mainctr#,30);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.potentialrating#",#mainctr#,31);
	SpreadsheetSetCellValue(incSheet,"#inlev#",#mainctr#,32);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.hsseadvisor#",#mainctr#,33);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.hssemanager#",#mainctr#,34);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.actiontaken#",#mainctr#,35);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.wasreported#",#mainctr#,36);
	SpreadsheetSetCellValue(incSheet,"#stbody#",#mainctr#,37);
	SpreadsheetSetCellValue(incSheet,"#stbodydate#",#mainctr#,38);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.EnforcementNotice#",#mainctr#,39);
	SpreadsheetSetCellValue(incSheet,"#noticetype#",#mainctr#,40);
	SpreadsheetSetCellValue(incSheet,"#getincidentdetails.droppedObject#",#mainctr#,41);
	SpreadsheetSetCellValue(incSheet,"#injsrc#",#mainctr#,42);
	SpreadsheetSetCellValue(incSheet,"#injsrcoth#",#mainctr#,43);
	SpreadsheetSetCellValue(incSheet,"#doH#",#mainctr#,44);
	SpreadsheetSetCellValue(incSheet,"#doW#",#mainctr#,45);
	SpreadsheetSetCellValue(incSheet,"#doE#",#mainctr#,46);
	SpreadsheetSetCellValue(incSheet,"#doC#",#mainctr#,47);
	SpreadsheetSetCellValue(incSheet,"#rlate#",#mainctr#,48);
</cfscript>

 

<cfset showinjrow = "no">

<cfif getincidentdetails.primarytype eq 1 or getincidentdetails.secondarytype eq 1 or getincidentdetails.primarytype eq 5 or getincidentdetails.secondarytype eq 5> 
	<cfif getincidentdetails.isnearmiss eq "no">
		<cfset showinjrow = "yes">
	</cfif>
</cfif>
<cfif showinjrow>
<cfset injctr = injctr+1>

<cfset ocat = "">
<cfoutput query="getoshacats">
	<cfif getincidentoi.oshaclass eq catid><cfset ocat = Category></cfif>
</cfoutput>

<cfset ipocc = "">
<cfoutput query="getoccupations">
	<cfif getincidentoi.ipOccupation eq occupationID><cfset ipocc = occupation></cfif>
</cfoutput>

<cfset ipage = "">
<cfoutput query="getages">
	<cfif getincidentoi.ipage eq ageid><cfset ipage = AgeDesc></cfif>
</cfoutput>

<cfset ipshift = "">
<cfset ipshiftday = "">
<cfset ipshiftdayof = "">
<cfif getincidentoi.workingshift eq "yes">
	<cfset ipshift = getincidentoi.ipshift>
	<cfset ipshiftday = getincidentoi.shiftday>
	<cfset ipshiftdayof = getincidentoi.shiftdayof>
</cfif>

<cfset intype = "">
<cfset intypeoth = "">
<cfset bpaffected = "">
<cfset incause = "">
<cfset incauseoth = "">
<cfset infall = "">
<cfif getincidentdetails.primarytype eq 1 or getincidentdetails.secondarytype eq 1>
	<cfoutput query="getinjurytypes">
		<cfif listfind(getincidentoi.injuryType,injtypeid) gt 0><cfset intype = listappend(intype,injurytype)></cfif>
	</cfoutput>
	<cfif trim(getincidentoi.otherInjuryType) neq ''>
		<cfset intypeoth = getincidentoi.otherInjuryType>
	</cfif>
	<cfoutput query="getbodypart">
		<cfif listfind(getincidentoi.bodyPart,bodypartid) gt 0><cfset bpaffected = listappend(bpaffected,bodypart)></cfif>
	</cfoutput>
	<cfoutput query="getinjurycause">
		<cfif listfind(getincidentoi.injuryCause,injnatureid) gt 0><cfset incause = listappend(incause,injurynature)></cfif>
	</cfoutput>
	<cfif trim(getincidentoi.otherInjuryCause) neq ''>
		<cfset incauseoth = getincidentoi.otherInjuryCause>
	</cfif>
	<cfif trim(getincidentoi.fallheight) neq ''>
		<cfset infall = getincidentoi.fallheight>
	</cfif>
</cfif>

<cfset illtype = "">
<cfset illtypeoth = "">
<cfset illnat = "">
<cfset illnatoth = "">
<cfif getincidentdetails.primarytype eq 5 or getincidentdetails.secondarytype eq 5>
	<cfoutput query="getillnesstypes">
		<cfif listfind(getincidentoi.IllnessType,illtypeid) gt 0><cfset illtype =  listappend(illtype,illnesstype)></cfif>
	</cfoutput>
	<Cfif trim(getincidentoi.OtherIllnessType) neq ''>
		<cfset illtypeoth = getincidentoi.OtherIllnessType>
	</cfif>
	<cfoutput query="getillnessnature">
		<cfif listfind(getincidentoi.IllnessNature,illnatureid) gt 0><cfset illnat =  listappend(illnat,IllnessNature)></cfif>
	</cfoutput>
	<cfif trim(getincidentoi.OtherIllnessNature) neq ''>
		<cfset illnatoth = getincidentoi.OtherIllnessNature>
	</cfif>
</cfif>

<cfset ltisdate = "">
<cfset ltirdate = "">
<cfset ltiresduty = "">
<cfset ltidatecomm = "">
<cfif getincidentoi.oshaclass eq 2>
	<cfset ltisdate = dateformat(getincidentoi.LTIFirstDate,sysdateformat)>
	<cfset ltirdate = dateformat(getincidentoi.LTIDateReturned,sysdateformat)>
	<cfset ltiresduty = getincidentoi.LTIReturnRestricted>
		<cfif trim(getincidentoi.LTIFirstDate) eq '' or trim(getincidentoi.LTIDateReturned) eq ''>
			
		<cfelse>
			<cfif datediff("d",getincidentoi.LTIFirstDate,getincidentoi.LTIDateReturned) lt 180>
			
			<cfelse>
				<cfset ltidatecomm = getincidentoi.dateretcomm>
			</cfif>
		</cfif>
</cfif>

<cfset rwcsdate = "">
<cfset rwcrdate = "">
<cfif getincidentoi.oshaclass eq 4>
	<cfset rwcsdate = dateformat(getincidentoi.RWCFirstDate,sysdateformat)>
	<cfset rwcrdate = dateformat(getincidentoi.RWCDateReturned,sysdateformat)>
</cfif>

<cfset medres = "">
<cfif trim(getincidentoi.MedicalRestrictions) neq ''>
	<cfset medres = getincidentoi.MedicalRestrictions>
</cfif>
<cfscript> 
	SpreadsheetSetCellValue(injuryOISheet,"#getincidentdetails.trackingnum#",#injctr#,1);
	SpreadsheetSetCellValue(injuryOISheet,"#ocat#",#injctr#,2);
	SpreadsheetSetCellValue(injuryOISheet,"#getincidentoi.isSerious#",#injctr#,3);
	SpreadsheetSetCellValue(injuryOISheet,"#repeatstring("X",15)#",#injctr#,4);
	SpreadsheetSetCellValue(injuryOISheet,"#ipocc#",#injctr#,5);
	SpreadsheetSetCellValue(injuryOISheet,"#ipage#",#injctr#,6);
	SpreadsheetSetCellValue(injuryOISheet,"#getincidentoi.ipGender#",#injctr#,7);
	SpreadsheetSetCellValue(injuryOISheet,"#getincidentoi.dayssinceoff#",#injctr#,8);
	SpreadsheetSetCellValue(injuryOISheet,"#getincidentoi.workingshift#",#injctr#,9);
	SpreadsheetSetCellValue(injuryOISheet,"#ipshift#",#injctr#,10);
	SpreadsheetSetCellValue(injuryOISheet,"#ipshiftday#",#injctr#,11);
	SpreadsheetSetCellValue(injuryOISheet,"#ipshiftdayof#",#injctr#,12);
	SpreadsheetSetCellValue(injuryOISheet,"#intype#",#injctr#,13);
	SpreadsheetSetCellValue(injuryOISheet,"#intypeoth#",#injctr#,14);
	SpreadsheetSetCellValue(injuryOISheet,"#bpaffected#",#injctr#,15);
	SpreadsheetSetCellValue(injuryOISheet,"#incause#",#injctr#,16);
	SpreadsheetSetCellValue(injuryOISheet,"#incauseoth#",#injctr#,17);
	SpreadsheetSetCellValue(injuryOISheet,"#infall#",#injctr#,18);
	SpreadsheetSetCellValue(injuryOISheet,"#illtype#",#injctr#,19);
	SpreadsheetSetCellValue(injuryOISheet,"#illtypeoth#",#injctr#,20);
	SpreadsheetSetCellValue(injuryOISheet,"#illnat#",#injctr#,21);
	SpreadsheetSetCellValue(injuryOISheet,"#illnatoth#",#injctr#,22);
	SpreadsheetSetCellValue(injuryOISheet,"#ltisdate#",#injctr#,23);
	SpreadsheetSetCellValue(injuryOISheet,"#ltirdate#",#injctr#,24);
	SpreadsheetSetCellValue(injuryOISheet,"#ltiresduty#",#injctr#,25);
	SpreadsheetSetCellValue(injuryOISheet,"#ltidatecomm#",#injctr#,26);
	SpreadsheetSetCellValue(injuryOISheet,"#rwcsdate#",#injctr#,27);
	SpreadsheetSetCellValue(injuryOISheet,"#rwcrdate#",#injctr#,28);
	SpreadsheetSetCellValue(injuryOISheet,"#medres#",#injctr#,29);
</cfscript>


</cfif>

<cfset showenvrow = "no">

<cfif getincidentdetails.primarytype eq 2 or getincidentdetails.secondarytype eq 2> 
	<cfif getincidentdetails.isnearmiss eq "no">
		<cfset showenvrow = "yes">
	</cfif>
</cfif>
<cfif showenvrow>
<cfset envctr = envctr+1>

<cfset envcat = "">
<cfoutput query="geteievents">
	<cfif listfind(getincidentenv.EnvIncCat,EnvIncCateID) gt 0><cfset envcat = EnvIncCategory></cfif>
</cfoutput>

<cfset brdetail = "">
<cfif getincidentenv.inBreach eq "yes">
	<cfset brdetail = getincidentenv.BreachDetail>
</cfif>

<cfset pollevdetail = "">
<cfif getincidentenv.EnvIncCat eq 1>
	<cfif getpollutionevents.recordcount gt 0>
		<cfoutput query="getpollutionevents">
			<cfset pollstr = "">
					<cfloop query="getpollET"> 
						<cfif listfind(getpollutionevents.PollutionEventType,Eventid) gt 0><cfset pollstr = PollutionEvent></cfif>
					</cfloop>
					<cfloop query="getpollsubstance">
						<cfif listfind(getpollutionevents.SubstanceType,SubstanceTypeID) gt 0><cfset pollstr = pollstr & " - #SubstanceType#"></cfif>
					</cfloop>
					<cfset pollstr = pollstr & " - #getpollutionevents.Quantity#">
					<cfloop query="getreleaseunit">
						<cfif listfind(getpollutionevents.SubstanceUnit,releaseunitid) gt 0><cfset pollstr = pollstr & " - #releaseunit#"></cfif>
					</cfloop>
					<cfloop query="getreleasesrc">
						<cfif listfind(getpollutionevents.ReleaseSource,ReleaseSourceid) gt 0><cfset pollstr = pollstr & " - #ReleaseSource#"></cfif>
					</cfloop>
					<cfloop query="getreleaseduration">
						<cfif listfind(getpollutionevents.ReleaseDuration,ReleaseDurationid) gt 0><cfset pollstr = pollstr & " - #ReleaseDuration#"></cfif>
					</cfloop>
					<cfloop query="getenviros">
						<cfif listfind(getpollutionevents.Environment,RecEnvID) gt 0><cfset pollstr = pollstr & " - #ReceivingEnv#"></cfif>
					</cfloop>
				<cfif trim(pollstr) neq ''>
					<cfset pollevdetail = listappend(pollevdetail,pollstr,"|")>
				</cfif>
		</cfoutput>
	</cfif>
</cfif>
	
<cfset noncon = "">
<cfset wasteinc = "">
<cfif getincidentenv.EnvIncCat eq 2>
	<cfset noncon = getincidentenv.NonConformDetail>
	<cfset wasteinc = getincidentenv.WasteIncorrect>
</cfif>
							
<cfscript> 
	SpreadsheetSetCellValue(envSheet,"#getincidentdetails.trackingnum#",#envctr#,1);
	SpreadsheetSetCellValue(envSheet,"#envcat#",#envctr#,2);
	SpreadsheetSetCellValue(envSheet,"#getincidentenv.inBreach#",#envctr#,3);
	SpreadsheetSetCellValue(envSheet,"#brdetail#",#envctr#,4);
	SpreadsheetSetCellValue(envSheet,"#pollevdetail#",#envctr#,5);
	SpreadsheetSetCellValue(envSheet,"#noncon#",#envctr#,6);
	SpreadsheetSetCellValue(envSheet,"#wasteinc#",#envctr#,7);
	
</cfscript>

</cfif>
<cfset showadrow = "no">

<cfif getincidentdetails.primarytype eq 3 or getincidentdetails.secondarytype eq 3> 
	<cfif getincidentdetails.isnearmiss eq "no">
		<cfset showadrow = "yes">
	</cfif>
</cfif>
<cfif showadrow>
<cfset assetctr = assetctr+1>

<cfset damsrc = "">
<cfoutput query="getassetdamagesrc">
	<cfif listfind(getincidentasset.DamageSource,DamageSourceid) gt 0><cfset damsrc = DamageSource></cfif>
</cfoutput>

<cfset damnat = "">
<cfoutput query="getassetdamagenature">
	<cfif listfind(getincidentasset.DamageNature,DamageNatureid) gt 0><cfset damnat = DamageNature></cfif>
</cfoutput>

<cfset damoth = "">
<cfif trim(getincidentasset.OtherDamage) neq ''>
	<cfset damoth = getincidentasset.OtherDamage>
</cfif>

<cfset propdetdsp = "">
<cfif getpropdamage.recordcount gt 0>
	<cfoutput query="getpropdamage">
		<cfset propdettxt = "">
			<cfloop query="getpropertytypes">
				<cfif getpropdamage.PropertyType eq PropTypeID><cfset propdettxt = propertytype></cfif>
			</cfloop>
			<cfset propdettxt = propdettxt & " - #getpropdamage.PropertyDesc#">
			<cfloop query="getextents">
				<cfif getpropdamage.DamageExtent eq DamageExtentID><cfset propdettxt = propdettxt & " - #DamageExtent#"></cfif>
			</cfloop>
			<cfif trim(propdettxt) neq ''>
				<cfset propdetdsp = listappend(propdetdsp,propdettxt,"|")>
			</cfif>
	</cfoutput>
</cfif>

<cfset currdsp = "">
<cfoutput query="getcurrencies">
	<cfif getincidentasset.Currency eq CurrencyID><cfset currdsp = CurrencyType></cfif>
</cfoutput>
<cfscript> 
	SpreadsheetSetCellValue(adSheet,"#getincidentdetails.trackingnum#",#assetctr#,1);
	SpreadsheetSetCellValue(adSheet,"#damsrc#",#assetctr#,2);
	SpreadsheetSetCellValue(adSheet,"#damnat#",#assetctr#,3);
	SpreadsheetSetCellValue(adSheet,"#damoth#",#assetctr#,4);
	SpreadsheetSetCellValue(adSheet,"#propdetdsp#",#assetctr#,5);
	SpreadsheetSetCellValue(adSheet,"#getincidentasset.EstCost#",#assetctr#,6);
	SpreadsheetSetCellValue(adSheet,"#currdsp#",#assetctr#,7);
	
</cfscript>


</cfif>
<cfset showsecrow = "no">

<cfif getincidentdetails.primarytype eq 4 or getincidentdetails.secondarytype eq 4> 
	<cfif getincidentdetails.isnearmiss eq "no">
		<cfset showsecrow = "yes">
	</cfif>
</cfif>
<cfif showsecrow>
<cfset secctr = secctr+1>

<cfset sic = "">
<cfoutput query="getsecinccats">
	<cfif getincidentsecurity.SecIncCat eq SecIncCatID><cfset sic = SecIncCategory></cfif>
</cfoutput>

<cfset threatsrc = "">
<cfoutput query="getsecthreats">
	<cfif getincidentsecurity.SecSource eq SecurityHazardID><cfset threatsrc = SecurityHazard></cfif>
</cfoutput>

<cfset persnat = "">
<cfset persoth = "">
<cfset occabroad = "">
<cfset cntrynm = "">
<cfif getincidentsecurity.SecIncCat eq 1>
	<cfoutput query="getsecnature">
		<cfif naturetype eq "person">
			<cfif getincidentsecurity.PersonNature eq SecIncNatureID><cfset persnat = IncidentNature></cfif>
		</cfif>
	</cfoutput>
	<cfif trim(getincidentsecurity.PersonOther) neq ''>
		<cfset persoth = getincidentsecurity.PersonOther>
	</cfif>
	<cfset occabroad = getincidentsecurity.OccurAbroad>
	<cfif getincidentsecurity.OccurAbroad eq "yes">
		<cfoutput query="getcountries">
			<cfif getincidentsecurity.CountryID eq countryid><cfset cntrynm = CountryName></cfif>
		</cfoutput>
	</cfif>
</cfif>

<cfset natbiz = "">
<cfset natbizoth = "">
<cfif getincidentsecurity.SecIncCat eq 2>
	<cfoutput query="getsecnature">
		<cfif naturetype eq "business">
			<cfif getincidentsecurity.BusinessNature eq SecIncNatureID><cfset natbiz = IncidentNature></cfif>
		</cfif>
	</cfoutput>
	<cfif trim(getincidentsecurity.BusinessOther) neq ''>
		<cfset natbizoth = getincidentsecurity.BusinessOther>
	</cfif>
</cfif>

<cfset infocat = "">
<cfset infonat = "">
<cfset infonatoth = "">

<cfif getincidentsecurity.SecIncCat eq 3>
	<cfoutput query="getinfocats">
		<cfif getincidentsecurity.InfoCategory eq infocatid><cfset infocat = InfoCategory></cfif>
	</cfoutput>
	<cfoutput query="getsecnature">
		<cfif naturetype eq "info">
			<cfif getincidentsecurity.InfoNature eq SecIncNatureID><cfset infonat = IncidentNature></cfif>
		</cfif>
	</cfoutput>
	<cfif trim(getincidentsecurity.InfoOther) neq ''>
		<cfset infonatoth = getincidentsecurity.InfoOther>
	</cfif>
</cfif>

<cfset spbbat = "">
<cfset spbnatoth = "">
<cfif getincidentsecurity.SecIncCat eq 4>
	<cfoutput query="getsecnature">
		<cfif naturetype eq "security">
			<cfif getincidentsecurity.breachNature eq SecIncNatureID><cfset spbbat = IncidentNature></cfif>
		</cfif>
	</cfoutput>
	<cfif trim(getincidentsecurity.breachother) neq ''>
		<cfset spbnatoth = getincidentsecurity.breachother>
	</cfif>
</cfif>

<cfset wtype = "">
<cfoutput query="getweapons"> 
	<cfif getincidentsecurity.WeaponKind eq Weaponid><cfset wtype = Weapon></cfif>
</cfoutput>

<cfset coestcost = "">
<cfset coextcostcurr = "">
<cfif getincidentsecurity.ResultInCost eq "yes">
	<cfset coestcost = getincidentsecurity.EstimateCost>
	<cfoutput query="getcurrencies">
		<cfif getincidentsecurity.CurrencyID eq CurrencyID><cfset coextcostcurr = CurrencyType></cfif>
	</cfoutput>
</cfif>

<cfscript> 
	SpreadsheetSetCellValue(secSheet,"#getincidentdetails.trackingnum#",#secctr#,1);
	SpreadsheetSetCellValue(secSheet,"#sic#",#secctr#,2);
	SpreadsheetSetCellValue(secSheet,"#threatsrc#",#secctr#,3);
	SpreadsheetSetCellValue(secSheet,"#persnat#",#secctr#,4);
	SpreadsheetSetCellValue(secSheet,"#persoth#",#secctr#,5);
	SpreadsheetSetCellValue(secSheet,"#occabroad#",#secctr#,6);
	SpreadsheetSetCellValue(secSheet,"#cntrynm#",#secctr#,7);
	SpreadsheetSetCellValue(secSheet,"#natbiz#",#secctr#,8);
	SpreadsheetSetCellValue(secSheet,"#natbizoth#",#secctr#,9);
	SpreadsheetSetCellValue(secSheet,"#infocat#",#secctr#,10);
	SpreadsheetSetCellValue(secSheet,"#infonat#",#secctr#,11);
	SpreadsheetSetCellValue(secSheet,"#infonatoth#",#secctr#,12);
	SpreadsheetSetCellValue(secSheet,"#spbbat#",#secctr#,13);
	SpreadsheetSetCellValue(secSheet,"#spbnatoth#",#secctr#,14);
	SpreadsheetSetCellValue(secSheet,"#getincidentsecurity.WeaponInvolved#",#secctr#,15);
	SpreadsheetSetCellValue(secSheet,"#wtype#",#secctr#,16);
	SpreadsheetSetCellValue(secSheet,"#getincidentsecurity.ResultInCost#",#secctr#,17);
	SpreadsheetSetCellValue(secSheet,"#coestcost#",#secctr#,18);
	SpreadsheetSetCellValue(secSheet,"#coextcostcurr#",#secctr#,19);
	
</cfscript>

	
 </cfif>