<cfparam name="submittype" default="">
<cfparam name="irn" default="0">
<cfparam name="datereturned" default="">
<cfparam name="revcomments" default="">
<cfswitch expression="#submittype#">
	<cfcase value="lti">
		<cfif irn gt 0 and trim(datereturned) neq ''>
			<cfquery name="getstat" datasource="#request.dsn#">
				select status, isworkrelated, needirp
				from oneAIMincidents
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			<cfquery name="updatedate" datasource="#request.dsn#">
				update oneAIMInjuryOI
				set LTIDateReturned = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#datereturned#">
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Adding LTI Return Date','incident')
					</cfquery>
				</cfif>
			<cfif getstat.status eq "Review Completed">
					<cfquery name="getinccapa" datasource="#request.dsn#">
						select DateComplete,status
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					<cfif getinccapa.recordcount eq 0>
						
						<cfset newincstat = "Closed">
						
						<cfif getstat.isworkrelated eq "yes">
							<cfquery name="getcurrinvstat" datasource="#request.dsn#">
								select status
								from IncidentInvestigation
								where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
							</cfquery>
							<cfif getcurrinvstat.status neq "Approved">
								<cfset newincstat = "Review Completed">
							</cfif>
							
							<cfif getstat.needirp eq 1>
								<cfquery name="getirpstat" datasource="#request.dsn#">
									SELECT      Status
									FROM            oneAIMIncidentIRP
									where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
								</cfquery>
								<cfif getirpstat.status neq "Complete">
									<cfset newincstat = "Review Completed">
								</cfif>
							</cfif>
							
						</cfif>
					
							
						
						<cfif newincstat eq "Closed">
						
						
						
						
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
					
						<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon entry of LTI Return Date">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
								</cfif>
					<cfelse>
						<cfset newincstat = "Closed">
						<cfloop query="getinccapa">
							<cfif status eq "Open">
								<cfset newincstat = "Review Completed">
								<cfbreak>
							</cfif>
						</cfloop>
						
						<cfif getstat.isworkrelated eq "yes">
							<cfquery name="getcurrinvstat" datasource="#request.dsn#">
								select status
								from IncidentInvestigation
								where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
							</cfquery>
							<cfif getcurrinvstat.status neq "Approved">
								<cfset newincstat = "Review Completed">
							</cfif>
							
							<cfif getstat.needirp eq 1>
								<cfquery name="getirpstat" datasource="#request.dsn#">
									SELECT      Status
									FROM            oneAIMIncidentIRP
									where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
								</cfquery>
								<cfif getirpstat.status neq "Complete">
									<cfset newincstat = "Review Completed">
								</cfif>
							</cfif>
							
						</cfif>

						
						<cfif newincstat eq "Closed">
					
							<cfquery name="updateincident" datasource="#request.dsn#">
								update oneAIMincidents
								set status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newincstat#">
								where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
							</cfquery>
					
							<cfquery name="addaudit" datasource="#request.dsn#">
								insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
								values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon entry of LTI Return Date">)
							</cfquery>
							<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
													<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
						</cfif>
					</cfif>
			
			</cfif>
		</cfif>
		<script type="text/javascript">
			window.opener.location.reload();
			window.close();
		</script>
	</cfcase>
	<cfcase value="rwc">
		<cfif irn gt 0 and trim(datereturned) neq ''>
			<cfquery name="getstat" datasource="#request.dsn#">
				select status,isworkrelated,needirp
				from oneAIMincidents
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			<cfquery name="updatedate" datasource="#request.dsn#">
				update oneAIMInjuryOI
				set RWCDateReturned = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#datereturned#">
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Adding RWC Return Date','incident')
					</cfquery>
				</cfif>
			<cfif getstat.status eq "Review Completed">
					<cfquery name="getinccapa" datasource="#request.dsn#">
						select DateComplete,status
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					<cfif getinccapa.recordcount eq 0>
					
					
					<cfset newincstat = "Closed">
						
						<cfif getstat.isworkrelated eq "yes">
							<cfquery name="getcurrinvstat" datasource="#request.dsn#">
								select status
								from IncidentInvestigation
								where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
							</cfquery>
							<cfif getcurrinvstat.status neq "Approved">
								<cfset newincstat = "Review Completed">
							</cfif>
							
							<cfif getstat.needirp eq 1>
								<cfquery name="getirpstat" datasource="#request.dsn#">
									SELECT      Status
									FROM            oneAIMIncidentIRP
									where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
								</cfquery>
								<cfif getirpstat.status neq "Complete">
									<cfset newincstat = "Review Completed">
								</cfif>
							</cfif>
							
						</cfif>
					
							
						
						<cfif newincstat eq "Closed">
					
					
					
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
					
						<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon entry of LTI Return Date">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
											
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
								</cfif>
					<cfelse>
						<cfset newincstat = "Closed">
						<cfloop query="getinccapa">
							<cfif status eq "Open">
								<cfset newincstat = "Review Completed">
								<cfbreak>
							</cfif>
						</cfloop>
						
						
						<cfif getstat.isworkrelated eq "yes">
							<cfquery name="getcurrinvstat" datasource="#request.dsn#">
								select status
								from IncidentInvestigation
								where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
							</cfquery>
							<cfif getcurrinvstat.status neq "Approved">
								<cfset newincstat = "Review Completed">
							</cfif>
							
							<cfif getstat.needirp eq 1>
								<cfquery name="getirpstat" datasource="#request.dsn#">
									SELECT      Status
									FROM            oneAIMIncidentIRP
									where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
								</cfquery>
								<cfif getirpstat.status neq "Complete">
									<cfset newincstat = "Review Completed">
								</cfif>
							</cfif>
							
						</cfif>
						
						<cfif newincstat eq "Closed">
					
							<cfquery name="updateincident" datasource="#request.dsn#">
								update oneAIMincidents
								set status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newincstat#">
								where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
							</cfquery>
					
							<cfquery name="addaudit" datasource="#request.dsn#">
								insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
								values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon entry of LTI Return Date">)
							</cfquery>
							<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
						</cfif>
					</cfif>
			
			</cfif>
		</cfif>
		<script type="text/javascript">
			window.opener.location.reload();
			window.close();
		</script>
	</cfcase>
</cfswitch>