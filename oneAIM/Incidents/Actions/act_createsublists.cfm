<cfparam name="type"  default="">
<cfparam name="azid"  default="">
<cfparam name="arrnum"  default="">
<cfif trim(type) neq ''>
	<cfswitch expression="#type#">
		<cfcase value="subtype">
			<cfif trim(azid) neq '' and isnumeric(azid)>
				<cfquery name="getsub" datasource="#request.dsn#">
					SELECT        AZcategories.CategoryID, AZcategories.SubCauseID, AZcategories.CategoryLetter, AZcategories.CategoryName, AZcategories.SortOrd, AZfactors.FactorName, 
                         AZfactors.FactorNumber, AZfactors.status, AZSubCauses.SubCause, AZfactors.FactorID
					FROM            AZcategories INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter INNER JOIN
                         AZSubCauses ON AZcategories.SubCauseID = AZSubCauses.SubCauseID
					WHERE        (AZcategories.CategoryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#azid#">) AND (AZfactors.status = 1)
					ORDER BY AZfactors.FactorNumber
				</cfquery><!--- this.style.border='1px solid 5f2468';" --->
				{zz{<cfoutput><select name="immsubtype#arrnum#" size="1" class="selectgen" id="immsubtype#arrnum#"  style="width:190px;" onchange="assignvals(#arrnum#,this.value,'#getsub.SubCause#');"></cfoutput>
					<option value="">-- Select One --</option>
					<cfoutput query="getsub"><option value="#FactorID#">#CategoryLetter#-#FactorNumber# #FactorName#</option></cfoutput>
					</select>}zz}<!--- onchange="this.style.border='1px solid 5f2468';" --->
				{qq{<cfoutput><input type="text" name="actom#arrnum#" size="22" class="selectgen" id="actom#arrnum#" readonly value="#getsub.SubCause#"></cfoutput>}qq}<!--- <select name="actom#arrnum#" size="1" class="selectgen" id="actom#arrnum#" style="width:190px;" >
						<option value="#getsub.SubCause#">#getsub.SubCause#</option>
						</select> --->
			</cfif>
		</cfcase>
		<cfcase value="subtypeImm">
			<cfif trim(azid) neq '' and isnumeric(azid)>
				<cfquery name="getsub" datasource="#request.dsn#">
					SELECT        AZcategories.CategoryID, AZcategories.SubCauseID, AZcategories.CategoryLetter, AZcategories.CategoryName, AZcategories.SortOrd, AZfactors.FactorName, 
                         AZfactors.FactorNumber, AZfactors.status, AZSubCauses.SubCause, AZfactors.FactorID
					FROM            AZcategories INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter INNER JOIN
                         AZSubCauses ON AZcategories.SubCauseID = AZSubCauses.SubCauseID
					WHERE        (AZcategories.CategoryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#azid#">) AND (AZfactors.status = 1)
					ORDER BY AZfactors.FactorNumber
				</cfquery><!--- this.style.border='1px solid 5f2468';" --->
				{zz{<cfoutput><select name="immsubtype_#arrnum#" size="1" class="selectgen" id="immsubtype#arrnum#"  style="width:190px;" onchange="assignvals(#arrnum#,this.value,'#getsub.SubCause#');"></cfoutput>
					<option value="">-- Select One --</option>
					<cfoutput query="getsub"><option value="#FactorID#">#CategoryLetter#-#FactorNumber# #FactorName#</option></cfoutput>
					</select>}zz}<!--- onchange="this.style.border='1px solid 5f2468';" --->
				{qq{<cfoutput><input type="text" name="actom_#arrnum#" size="22" class="selectgen" id="actom#arrnum#" readonly value="#getsub.SubCause#"></cfoutput>}qq}<!--- <select name="actom_#arrnum#" size="1" class="selectgen" id="actom#arrnum#" style="width:190px;" >
						<option value="#getsub.SubCause#">#getsub.SubCause#</option>
						</select> --->
			</cfif>
		</cfcase>
		<cfcase value="subtyperoot">
			<cfif trim(azid) neq '' and isnumeric(azid)>
				<cfquery name="getsub" datasource="#request.dsn#">
					SELECT        AZcategories.CategoryID, AZcategories.SubCauseID, AZcategories.CategoryLetter, AZcategories.CategoryName, AZcategories.SortOrd, AZfactors.FactorName, 
                         AZfactors.FactorNumber, AZfactors.status, AZSubCauses.SubCause, AZfactors.FactorID
					FROM            AZcategories INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter INNER JOIN
                         AZSubCauses ON AZcategories.SubCauseID = AZSubCauses.SubCauseID
					WHERE        (AZcategories.CategoryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#azid#">) AND (AZfactors.status = 1)
					ORDER BY AZfactors.FactorNumber
				</cfquery><!--- this.style.border='1px solid 5f2468'; --->
				{zz{<cfoutput><select name="rootsubtype#arrnum#a" size="1" class="selectgen" id="rootsubtype#arrnum#a"  style="width:190px;" onchange="assignvals(#arrnum#,this.value,'#getsub.SubCause#');"></cfoutput>
					<option value="">-- Select One --</option>
					<cfoutput query="getsub"><option value="#FactorID#">#CategoryLetter#-#FactorNumber# #FactorName#</option></cfoutput>
					</select>}zz}<!---  onchange="this.style.border='1px solid 5f2468';" --->
				{qq{<cfoutput><input type="text" name="rootactom#arrnum#" size="22" class="selectgen" id="rootactom#arrnum#a" readonly value="#getsub.SubCause#"></cfoutput>}qq}<!--- <select name="rootactom#arrnum#" size="1" class="selectgen" id="rootactom#arrnum#a" style="width:190px;">
						<option value="#getsub.SubCause#">#getsub.SubCause#</option>
						</select> --->
			</cfif>
		</cfcase>
		<cfcase value="subtyperootsub">
			<cfif trim(azid) neq '' and isnumeric(azid)>
				<cfquery name="getsub" datasource="#request.dsn#">
					SELECT        AZcategories.CategoryID, AZcategories.SubCauseID, AZcategories.CategoryLetter, AZcategories.CategoryName, AZcategories.SortOrd, AZfactors.FactorName, 
                         AZfactors.FactorNumber, AZfactors.status, AZSubCauses.SubCause, AZfactors.FactorID
					FROM            AZcategories INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter INNER JOIN
                         AZSubCauses ON AZcategories.SubCauseID = AZSubCauses.SubCauseID
					WHERE        (AZcategories.CategoryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#azid#">) AND (AZfactors.status = 1)
					ORDER BY AZfactors.FactorNumber
				</cfquery><!--- this.style.border='1px solid 5f2468'; --->
				{zz{<cfoutput><select name="rootsubtype_#arrnum#" size="1" class="selectgen" id="rootsubtype#arrnum#a"  style="width:190px;" onchange="assignvals(#arrnum#,this.value,'#getsub.SubCause#');"></cfoutput>
					<option value="">-- Select One --</option>
					<cfoutput query="getsub"><option value="#FactorID#">#CategoryLetter#-#FactorNumber# #FactorName#</option></cfoutput>
					</select>}zz}<!--- onchange="this.style.border='1px solid 5f2468';" --->
				{qq{<cfoutput><input type="text" name="rootactom_#arrnum#" size="22" class="selectgen" id="rootactom#arrnum#a" readonly value="#getsub.SubCause#"></cfoutput>}qq}<!--- <select name="rootactom_#arrnum#" size="1" class="selectgen" id="rootactom#arrnum#a" style="width:190px;" >
						<option value="#getsub.SubCause#">#getsub.SubCause#</option>
						</select> --->
			</cfif>
		</cfcase>
		<cfcase value="subtypecopy">
			<cfif trim(azid) neq '' and isnumeric(azid)>
				
				
				<cfquery name="getaz" datasource="#request.dsn#">
					SELECT        CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
					FROM            AZcategories
					ORDER BY SubCauseID, CategoryLetter
				</cfquery>
				
				
				<cfquery name="getaz" datasource="#request.dsn#">
					SELECT        CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
					FROM            AZcategories
					ORDER BY SubCauseID, CategoryLetter
				</cfquery><!--- this.style.border='1px solid 5f2468'; --->
				{aa{<cfoutput>#arrnum#</cfoutput><select name="immcause#arrnum#" size="1" class="selectgen" id="immcause#arrnum#" onchange="buildsubtype(this.value,#arrnum#);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfoutput query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#" <cfif categoryid eq azid>selected</cfif>>#CategoryLetter# #CategoryName#</option></cfif></cfoutput>
						</select>}aa}
						
					<cfquery name="getsub" datasource="#request.dsn#">
					SELECT        AZcategories.CategoryID, AZcategories.SubCauseID, AZcategories.CategoryLetter, AZcategories.CategoryName, AZcategories.SortOrd, AZfactors.FactorName, 
                         AZfactors.FactorNumber, AZfactors.status, AZSubCauses.SubCause, AZfactors.FactorID
					FROM            AZcategories INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter INNER JOIN
                         AZSubCauses ON AZcategories.SubCauseID = AZSubCauses.SubCauseID
					WHERE        (AZcategories.CategoryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#azid#">) AND (AZfactors.status = 1)
					ORDER BY AZfactors.FactorNumber
				</cfquery>
				<!--- onchange="this.style.border='1px solid 5f2468';" --->
						{bb{<cfoutput><select name="immsubtype#arrnum#" size="1" class="selectgen" id="immsubtype#arrnum#"  style="width:190px;"  ></cfoutput>
					<option value="">-- Select One --</option>
					<cfoutput query="getsub"><option value="#FactorID#" <cfif factorid eq subv>selected</cfif>>#CategoryLetter#-#FactorNumber# #FactorName#</option></cfoutput>
					</select>}bb}<!---  onchange="this.style.border='1px solid 5f2468';" --->
					{cc{<cfoutput><input type="text" name="actom#arrnum#" size="22" class="selectgen" id="actom#arrnum#" readonly  value="#getsub.SubCause#"></cfoutput>}cc}<!--- <select name="actom#arrnum#" size="1" class="selectgen" id="actom#arrnum#" style="width:190px;">
						<option value="#getsub.SubCause#">#getsub.SubCause#</option>
						</select> --->
						<!--- onkeypress="this.style.border='1px solid 5f2468';" --->
						{dd{<cfoutput><textarea name="comm#arrnum#" class="selectgen" cols="30" rows="2"  >#comm#</textarea></cfoutput>}dd}
			<!--- {zz{<cfoutput><tr id="immrow#arrnum#"><td class="bodyTextGrey" width="25%" align="center">#arrnum#<select name="immcause#arrnum#" size="1" class="selectgen" id="lrgselect" onchange="buildsubtype(this.value,#arrnum#);"></cfoutput>
						<option value="">-- Select One --</option>
						<cfoutput query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#" <cfif CategoryID eq azid>selected</cfif>>#CategoryLetter# #CategoryName#</option></cfif></cfoutput>
						</select></td>
						<td id="immsubtype#arrnum#" align="center" width="25%"><cfoutput><select name="immsubtype#arrnum#" size="1" class="selectgen" id="lrgselect" ></cfoutput>
					<option value="">-- Select One --</option>
					<cfoutput query="getsub"><option value="#FactorID#"  <cfif factorid eq subv>selected</cfif>>#CategoryLetter#-#FactorNumber# #FactorName#</option></cfoutput>
					</select></td>
						<td id="actomm#arrnum#" align="center" width="25%"><cfoutput><select name="actom#arrnum#" size="1" class="selectgen" id="lrgselect">
						<option value="#getsub.SubCause#" <cfif getsub.SubCause eq act>selected</cfif>>#getsub.SubCause#</option>
						</select></cfoutput></td>
						<cfoutput><td id="comm#arrnum#" align="center" width="25%"><textarea name="comm#arrnum#" class="selectgen" cols="30" rows="2">#comm#</textarea></td>
						<td class="bodytextsm" align="left" ><a href="javascript:void(0);" onclick="document.investfrm.immtodelete.value=document.investfrm.immtodelete.value+',#arrnum#';document.getElementById('immrow#arrnum#').style.display='none';">Delete</a><br><a href="javascript:void(0);" onclick="copysubtype(document.investfrm.immcause#arrnum#.value,document.investfrm.immsubtype#arrnum#.value,document.investfrm.actom#arrnum#.value,document.investfrm.comm#arrnum#.value,#arrnum#);">Copy</a></td></tr>}zz}</cfoutput> --->
			</cfif>
		</cfcase>
		
		<cfcase value="addrow">
			<cfif trim(arrnum) neq '' and isnumeric(arrnum)>
				<cfquery name="getaz" datasource="#request.dsn#">
					SELECT        CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
					FROM            AZcategories
					ORDER BY SubCauseID, CategoryLetter
				</cfquery><!--- this.style.border='1px solid 5f2468'; --->
				{zz{<cfoutput><tr id="immrow#arrnum#"><td class="bodyTextGrey" width="25%" align="center">#arrnum#<select name="immcause#arrnum#" size="1" class="selectgen" id="lrgselect" onchange="buildsubtype(this.value,#arrnum#);"></cfoutput>
						<option value="">-- Select One --</option>
						<cfoutput query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#">#CategoryLetter# #CategoryName#</option></cfif></cfoutput>
						</select></td><!--- onchange="this.style.border='1px solid 5f2468';" --->
						<cfoutput><td id="immsubtype#arrnum#" align="center" width="25%"><select name="immsubtype#arrnum#" size="1" class="selectgen" id="lrgselect" >
						<option value="">-- Select One --</option>
						</select></td><!---  onchange="this.style.border='1px solid 5f2468';" --->
						<td id="actomm#arrnum#" align="center" width="25%"><input type="text" name="actom#arrnum#" size="22" class="selectgen" readonly><!--- <select name="actom#arrnum#" size="1" class="selectgen" id="lrgselect">
						<option value="">-- Select One --</option>
						</select> ---></td><!--- onkeypress="this.style.border='1px solid 5f2468';" --->
						<td id="comm#arrnum#" align="center" width="25%"><textarea name="comm#arrnum#" class="selectgen" cols="30" rows="2" ></textarea></td>
						<td class="bodytextsm" align="left" ><a href="javascript:void(0);" onclick="document.investfrm.immtodelete.value=document.investfrm.immtodelete.value+',#arrnum#';document.getElementById('immrow#arrnum#').style.display='none';">Delete</a><br><a href="javascript:void(0);" onclick="copysubtype(document.investfrm.immcause#arrnum#.value,document.investfrm.immsubtype#arrnum#.value,document.investfrm.actom#arrnum#.value,document.investfrm.comm#arrnum#.value,#arrnum#);">Copy</a></td></tr>}zz}</cfoutput>
			</cfif>
		</cfcase>
		<!---  <cfcase value="addrowimm">
			<cfparam name="causeid" default="">
			<cfparam name="subcauseid" default="">
			<cfparam name="irn" default="">
			<cfparam name="aztype" default="">
			<cfparam name="invid" default="">
			<cfif trim(causeid) neq '' and trim(subcauseid) neq '' and trim(aztype) neq '' and trim(invid) neq ''>
				<cfquery name="addaz" datasource="#request.dsn#">
					insert into incidentaz(invid, aztype, azcatid, azfactorid,AZSubCause, azcomment)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#aztype#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#causeid#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#subcauseid#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#actomm#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#comment#">)
				</cfquery>
			</cfif>
			<cfquery name="getinvdetail" datasource="#request.dsn#">
				SELECT        IncidentInvestigation.InvID, IncidentInvestigation.IRN, IncidentInvestigation.SrSiteRep, IncidentInvestigation.InvCondBy, IncidentInvestigation.TeamMembers, 
                         IncidentInvestigation.InvSummary, IncidentInvestigation.DirSupervisor, IncidentInvestigation.InvRelevance, IncidentInvestigation.rulebreach, 
                         IncidentInvestigation.whynorulebreach, IncidentInvestigation.essentialbreach, IncidentInvestigation.whynoessbreach, IncidentAZ.AZType, IncidentAZ.IncAZid, 
                         IncidentAZ.AZCatID, IncidentAZ.AZFactorID, IncidentAZ.AZSubCause, IncidentAZ.AZComment, IncidentAZ.DateCreated, AZfactors.CategoryLetter
				FROM            AZfactors RIGHT OUTER JOIN
                         IncidentAZ ON AZfactors.FactorID = IncidentAZ.AZFactorID RIGHT OUTER JOIN
                         IncidentInvestigation ON IncidentAZ.InvID = IncidentInvestigation.InvID
				WHERE IncidentInvestigation.InvID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#invid#">
				ORDER BY IncidentAZ.DateCreated
			</cfquery>
			<cfquery name="getaz" datasource="#request.dsn#">
				SELECT        CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
				FROM            AZcategories
				ORDER BY SubCauseID, CategoryLetter
			</cfquery>
			<cfquery name="getazfactors" datasource="#request.dsn#">
				SELECT        FactorID, CategoryLetter, FactorName, FactorNumber, status
				FROM            AZfactors
				WHERE        (status = 1)
			</cfquery>
			<cfquery name="getazcatgories" datasource="#request.dsn#">
				SELECT      CategoryID, SubCauseID, CategoryLetter, CategoryName, SortOrd
				FROM            AZcategories
			</cfquery>
			<cfset azcatstruct = {}>
			<cfset azfactorstruct = {}>
			
			<cfloop query="getazcatgories">
				<cfset azcatstruct[CategoryID] = "#CategoryLetter# #CategoryName#">
			</cfloop>
			
			<cfloop query="getazfactors">
				<cfset azfactorstruct[factorid] = "#CategoryLetter#-#FactorNumber# #FactorName#">
			</cfloop>
			{zz{<table width="100%" cellpadding="3" cellspacing="0" border="0">
				<cfoutput query="getinvdetail">
				<cfif aztype eq 1>
				<tr>
					<td class="bodyTextGrey" width="25%" align="center">
					<select name="immcause_#IncAZid#"  size="1" class="selectgen" id="immcause_#IncAZid#" onchange="subtypeImm(this.value,#IncAZid#);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfloop query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#" <cfif categoryid eq getinvdetail.azcatid>selected</cfif>>#CategoryLetter# #CategoryName#</option></cfif></cfloop>
						</select><!--- 
					
					<cfif structkeyexists(azcatstruct,azcatid)>#azcatstruct[azcatid]#</cfif> ---></td>
					<td id="immsubtype#IncAZid#" align="center" width="25%"><select name="immsubtype_#IncAZid#" size="1" class="selectgen" id="immsubtype_#IncAZid#"  style="width:190px;" >
					<option value="">-- Select One --</option>
					<cfloop query="getazfactors"><cfif getazfactors.CategoryLetter eq getinvdetail.CategoryLetter><option value="#FactorID#" <cfif factorid eq getinvdetail.azfactorid>selected</cfif>>#CategoryLetter#-#FactorNumber# #FactorName#</option></cfif></cfloop>
					</select></td>
					<!--- <td  width="25%" align="left"><cfif structkeyexists(azfactorstruct,azfactorid)>#azfactorstruct[azfactorid]#</cfif></td> --->
					<td  width="25%" align="center"  id="actomm#IncAZid#">
					<select name="actom_#IncAZid#" size="1" class="selectgen" id="actom_#IncAZid#" style="width:190px;">
						<option value="#AZSubCause#">#AZSubCause#</option>
						</select>
					<!--- #azsubcause# ---></td>
					<td  width="25%" align="center"><textarea name="comm_#IncAZid#" id="commf1" class="selectgen" cols="30" rows="2">#azcomment#</textarea></td>
					<td class="bodytextsm" align="left"><a href="#self#?fuseaction=incidents.manageinvestigation&submittype=deleteaz&invazid=#IncAZid#&irn=#irn#">Delete</a>&nbsp;&nbsp;&nbsp;<br><a  href="#self#?fuseaction=incidents.manageinvestigation&submittype=copyaz&invazid=#IncAZid#&irn=#irn#">Copy</a></td>
				</tr>
				</cfif>
				
				</cfoutput>
				</table>}zz}
		</cfcase> --->
	</cfswitch>
</cfif>
<!--- 
<tr id="immrow1">
				<input type="hidden" name="atype" value="1"><!--- Immediate Cause --->
					<td class="bodyTextGrey" width="25%" align="center" id="immrowa1"><select name="immcause1"  size="1" class="selectgen" id="immcause1" onchange="buildsubtype(this.value,1);" style="width:190px;">
						<option value="">-- Select One --</option>
						<cfoutput query="getaz"><cfif listfind("1,2",SubCauseID) gt 0><option value="#CategoryID#">#CategoryLetter# #CategoryName#</option></cfif></cfoutput>
						</select></td>
						<td id="immsubtype1" align="center" width="25%"><select name="immsubtype1"  size="1" class="selectgen" id="immsubtypef1" style="width:190px;">
						<option value="">-- Select One --</option>
						</select></td>
						<td id="actomm1" align="center" width="25%"><select name="actom1" size="1" class="selectgen" id="actom1" style="width:190px;">
						<option value="">-- Select One --</option>
						</select></td>
						<td id="comm1" align="center" width="25%"><textarea name="comm1" id="commf1" class="selectgen" cols="30" rows="2"></textarea></td>
						<td class="bodytextsm" align="left" colspan="4"><a onclick="addrow();" href="javascript:void(0);">Add&nbsp;Row</a></td>
				</tr> --->