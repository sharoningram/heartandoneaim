<cfparam name="closesave" default="no">
<cfparam name="irn" default="0">
<cfparam name="faid" default="0">
<cfparam name="submittype" default="">
<cfparam name="proposedaction" default="">
<cfparam name="description" default="">
<cfparam name="sfr" default="yes">
<cfswitch expression="#submittype#">
	<cfcase value="addFA">
		<cfif trim(irn) neq 0>
			<cfif sfr eq "yes">
				<cfset newstat = "Sent for Review">
			<cfelse>
				<cfset newstat = "Started">
			</cfif>
		
			<cfquery name="addfa" datasource="#request.dsn#">
				insert into oneAimFirstAlerts (IRN, PreventAction,status,enteredby,dateentered,enteredbyemail,description)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#proposedaction#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newstat#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.userlogin#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#description#">)
				select ident_current('oneAimFirstAlerts') as newfa
			</cfquery>
			
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
				<cfif not directoryexists(filedir)>
					<cfdirectory action="CREATE" directory="#filedir#">
				</cfif>
				<cfif trim(UPLOADDOC1) neq ''>
					<cffile action="upload" filefield="UPLOADDOC1" destination="#FileDir#" result="file_result1" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result1.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result1.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC2) neq ''>
					<cffile action="upload" filefield="UPLOADDOC2" destination="#FileDir#" result="file_result2" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result2.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result2.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC3) neq ''>
					<cffile action="upload" filefield="UPLOADDOC3" destination="#FileDir#" result="file_result3" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result3.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result3.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC4) neq ''>
					<cffile action="upload" filefield="UPLOADDOC4" destination="#FileDir#" result="file_result4" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result4.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result4.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC5) neq ''>
					<cffile action="upload" filefield="UPLOADDOC5" destination="#FileDir#" result="file_result5" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result5.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result5.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
			<cfif sfr eq "yes">
			<cfquery name="getreviewer" datasource="#request.dsn#">
				SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
			</cfquery>
			<cfset tolist = "">
			<cfset tolistnames = "">
			<!--- <cfif getreviewer.recordcount gt 0> --->
				
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss, oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
					FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<!--- <cfloop query="getreviewer"> --->
					<cfset tolist = listappend(tolist,getemaildata.reviewerEmail)>
					<cfset tolistnames =  listappend(tolistnames,getemaildata.HSSEManager)>
				<!--- </cfloop> --->
				<cfquery name="getbuname" datasource="#request.dsn#">
					SELECT      Name
					FROM            NewDials
					WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
				</cfquery>
				<cfif trim(tolist) neq ''>
					<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss - First Alert review">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - First Alert review">
					</cfif>
				<cfmail to="#tolist#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							A First Alert form has been completed and is now awaiting your review. 
							<br>Please go to oneAIM via this link to view the incident and to review and accept/decline the First Alert � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
						
				</cfif>
			<!--- </cfif> --->
			<cfquery name="addaudit" datasource="#request.dsn#">
				insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, SentTo, ActionTaken,fromstatus,StatusDesc)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'First Alert sent for review','',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert sent for review by #request.fname# #request.lname# to #tolistnames#">)
			</cfquery>
			
			</cfif>
		</cfif>
		<cfif sfr eq "yes">
			<cfoutput>
			<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="faid" value="#addfa.newfa#">
			</form>
			<script type="text/javascript">
				document.redirinc.submit();
			</script> 
			</cfoutput>
		<cfelse>
			<cfif closesave eq "no">
				<cfoutput>
				<form action="#self#?fuseaction=incidents.firstalert" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="faid" value="#addfa.newfa#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script> 
				</cfoutput>
			<cfelse>
				<cfoutput>
				<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="faid" value="#addfa.newfa#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script> 
				</cfoutput>
			</cfif>
		</cfif>
		
			<!--- <cfoutput>
			<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="faid" value="#addfa.newfa#">
			</form>
			<script type="text/javascript">
				document.redirinc.submit();
			</script>
		</cfoutput> --->
		
		
		
		
		
	</cfcase>
	<cfcase value="updateFA">
		
		<cfif trim(irn) neq 0 and  trim(faid) neq 0>
		
		
		<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
				<cfif not directoryexists(filedir)>
					<cfdirectory action="CREATE" directory="#filedir#">
				</cfif>
				<cfif trim(UPLOADDOC1) neq ''>
					<cffile action="upload" filefield="UPLOADDOC1" destination="#FileDir#" result="file_result1" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result1.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result1.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC2) neq ''>
					<cffile action="upload" filefield="UPLOADDOC2" destination="#FileDir#" result="file_result2" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result2.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result2.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC3) neq ''>
					<cffile action="upload" filefield="UPLOADDOC3" destination="#FileDir#" result="file_result3" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result3.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result3.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC4) neq ''>
					<cffile action="upload" filefield="UPLOADDOC4" destination="#FileDir#" result="file_result4" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result4.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result4.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
				<cfif trim(UPLOADDOC5) neq ''>
					<cffile action="upload" filefield="UPLOADDOC5" destination="#FileDir#" result="file_result5" nameconflict="makeunique" accept="#allowedfilelist#">
					<cfset fname = replace(file_result5.serverFile,"&","","all")>
					<cffile action="RENAME" source="#FileDir#\#file_result5.serverFile#" destination="#FileDir#\#fname#">
				</cfif>
		
		
		
		
			<cfif sfr eq "yes">
				<cfquery name="getcurrstat" datasource="#request.dsn#">
				select status
				from oneAimFirstAlerts
				where FirstAlertID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#faid#">
			</cfquery>
			<cfquery name="addfa" datasource="#request.dsn#">
				update oneAimFirstAlerts
				set PreventAction = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#proposedaction#">,
				description =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#description#">,
				<cfif getcurrstat.status eq "SR More Info">
				status = 'Sent for Approval'
				<cfelse>
				status = 'Sent for Review'
				</cfif>
				where FirstAlertID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#faid#">
			</cfquery>
			
				
			
			 <cfif getcurrstat.status eq "SR More Info">
			
			
			
			
			<cfquery name="getsrreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND (UserRoles.AssignedLocs IN
                             (SELECT        Business_Line_ID
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
				
				</cfquery>
				<cfset tolistemail = "">
				<cfset tolistnames = "">
				<cfloop query="getsrreviewer">
					<cfset tolistemail = listappend(tolistemail,Useremail)>
					<cfset tolistnames = listappend(tolistnames,"#Firstname# #lastname#")>
				</cfloop>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Sent for Approval',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'More information requested',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'First Alert form sent for approval',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert form sent for approval by #request.fname# #request.lname# to #tolistnames#">)
				</cfquery>
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss
					FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfquery name="getbuname" datasource="#request.dsn#">
					SELECT      Name
					FROM            NewDials
					WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
				</cfquery>
				<cfif trim(tolistemail) neq ''>
				
				<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident and First Alert review - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss">
					<cfelse>
						<cfset emsubj = "oneAIM Incident and First Alert review - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType#">
					</cfif>
				<cfmail to="#tolistemail#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							An incident with a First Alert has been raised.  
							<br>Please go to oneAIM via this link to view the incident and to review and accept/decline the First Alert � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
				
					</cfif>
			
			<cfelse> 
			
			<cfquery name="getreviewer" datasource="#request.dsn#">
				SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
			</cfquery>
			<cfset tolist = "">
			<cfset tolistnames = "">
			<!--- <cfif getreviewer.recordcount gt 0> --->
				
				
				
				
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
					FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<!--- <cfloop query="getreviewer"> --->
					<cfset tolist = listappend(tolist,getemaildata.reviewerEmail)>
					<cfset tolistnames =  listappend(tolistnames,getemaildata.HSSEManager)>
				<!--- </cfloop> --->
				<cfquery name="getbuname" datasource="#request.dsn#">
					SELECT      Name
					FROM            NewDials
					WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
				</cfquery>
				<cfif trim(tolist) neq ''>
					<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss - First Alert review">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - First Alert review">
					</cfif>
				<cfmail to="#tolist#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							A First Alert form has been completed and is now awaiting your review. 
							<br>Please go to oneAIM via this link to view the incident and to review and accept/decline the First Alert � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
						
				</cfif>
				
				
				<!--- send reviewer email --->
			<!--- </cfif> --->
			
			<cfquery name="addaudit" datasource="#request.dsn#">
				insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, SentTo, ActionTaken,fromstatus,StatusDesc)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'First Alert sent for review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurrstat.status#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert sent for review by #request.fname# #request.lname# to #tolistnames#">)
			</cfquery>
			
			
			</cfif>
			<cfelse>
			<cfquery name="getcurrstat" datasource="#request.dsn#">
				select status
				from oneAimFirstAlerts
				where FirstAlertID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#faid#">
			</cfquery>
				<cfquery name="addfa" datasource="#request.dsn#">
					update oneAimFirstAlerts
					set PreventAction = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#proposedaction#">,
					description =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#description#">
					<cfif getcurrstat.recordcount eq 0 or trim(getcurrstat.status) eq ''>,status = 'Started'</cfif>
					where FirstAlertID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#faid#">
				</cfquery>
			</cfif>
			
			
			
			
			
		</cfif>
		<cfif sfr eq "yes">
			<cfoutput>
			<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="faid" value="#faid#">
			</form>
			<script type="text/javascript">
				document.redirinc.submit();
			</script> 
			</cfoutput>
		<cfelse>
			<cfif closesave eq "no">
				<cfoutput>
				<form action="#self#?fuseaction=incidents.firstalert" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="faid" value="#faid#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script> 
				</cfoutput>
			<cfelse>
				<cfoutput>
				<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="faid" value="#faid#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script> 
				</cfoutput>
			</cfif>
		</cfif>
		<!--- <cfoutput>
			<form action="#self#?fuseaction=main.main" method="post" name="redirinc">
				<input type="hidden" name="irn" value="#irn#">
				<input type="hidden" name="faid" value="#faid#">
			</form>
			<script type="text/javascript">
				document.redirinc.submit();
			</script>
		</cfoutput> --->
	</cfcase>
	<cfcase value="deletefafile">
		<cfset irpmsg = 0>
		<cfparam name="filename" default="">
			<cfif irn neq 0 and trim(filename) neq ''>
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
				
				<cfif fileexists("#filedir#\#filename#")>
					<cftry>
					<cffile action="DELETE" file="#filedir#\#filename#">
					<cfcatch type="Any">
					<cfset irpmsg = 1>
					</cfcatch>
					</cftry>
				</cfif>
			</cfif>
			<cfoutput>
				<form action="#self#?fuseaction=#attributes.xfa.firstalert#" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="faid" value="#faid#">
					<input type="hidden" name="irpmsg" value="#irpmsg#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script>
			</cfoutput>
	</cfcase>
	<cfcase value="deletefafilefrominc">
		<cfset irpmsg = 0>
		<cfparam name="filename" default="">
			<cfif irn neq 0 and trim(filename) neq ''>
				<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
				
				<cfif fileexists("#filedir#\#filename#")>
					<cftry>
					<cffile action="DELETE" file="#filedir#\#filename#">
					<cfcatch type="Any">
					<cfset irpmsg = 1>
					</cfcatch>
					</cftry>
				</cfif>
			</cfif>
			<cfoutput>
				<form action="#self#?fuseaction=incidents.incidentreport" method="post" name="redirinc">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="irpmsg" value="#irpmsg#">
				</form>
				<script type="text/javascript">
					document.redirinc.submit();
				</script>
			</cfoutput>
	
	</cfcase>
	<cfcase value="moreinfo">
			<cfparam name="fastatval" default="">
			<cfparam name="revcomments" default="">
			<cfparam name="fa" default="0">
			<cfif fa gt 0>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'First Alert Review','fa')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update oneAimFirstAlerts
					set 
					<cfif fastatval eq "Sent for Approval">
						status = 'SR More Info'
					<cfelse>
						status = 'More Info Requested'
					</cfif>
					where firstalertid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fa#">
				</cfquery>
				<cfquery name="getinitiator" datasource="#request.dsn#">
					SELECT        enteredbyemail
					FROM            oneAimFirstAlerts
					WHERE firstalertid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fa#">
				</cfquery>
				
				
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss, oneAIMincidents.createdBy
					FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Sent for More Information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'First Alert Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdBy#">,'First Alert form sent for more information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert form sent for more information by #request.fname# #request.lname# to #getemaildata.createdBy#">)
				</cfquery>
				<cfquery name="getbuname" datasource="#request.dsn#">
					SELECT      Name
					FROM            NewDials
					WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
				</cfquery>
					<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif trim(getinitiator.enteredbyemail) neq ''>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss - First Alert - provide further information">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - First Alert - provide further information">
					</cfif>
				<cfmail to="#getinitiator.enteredbyemail#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							Further information has been requested for a first alert raised for the following incident.  
							<br>Please go to oneAIM via this link to update and progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Reviewer Comments:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#revcomments#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<!--- <tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr> --->
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
					</cfif>
				
				
				
			</cfif>
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="reviewfa">
		<cfparam name="revcomments" default="">
			<cfparam name="fa" default="0">
			<cfif fa gt 0>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'First Alert Review','fa')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update oneAimFirstAlerts
					set status = 'Sent for Approval'
					where firstalertid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fa#">
				</cfquery>
				<cfquery name="getsrreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND (UserRoles.AssignedLocs IN
                             (SELECT        Business_Line_ID
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
				
				</cfquery>
				<cfset tolistemail = "">
				<cfset tolistnames = "">
				<cfloop query="getsrreviewer">
					<cfset tolistemail = listappend(tolistemail,Useremail)>
					<cfset tolistnames = listappend(tolistnames,"#Firstname# #lastname#")>
				</cfloop>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Sent for Approval',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'First Alert Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'First Alert form sent for approval',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert form sent for approval by #request.fname# #request.lname# to #tolistnames#">)
				</cfquery>
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss
					FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfquery name="getbuname" datasource="#request.dsn#">
					SELECT      Name
					FROM            NewDials
					WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
				</cfquery>
				<cfif trim(tolistemail) neq ''>
				
				<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident and First Alert review - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss">
					<cfelse>
						<cfset emsubj = "oneAIM Incident and First Alert review - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType#">
					</cfif>
				<cfmail to="#tolistemail#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							An incident with a First Alert has been raised.  
							<br>Please go to oneAIM via this link to view the incident and to review and accept/decline the First Alert � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
				
					</cfif>
				
				
			</cfif>
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="seniorreviewfa">
		<cfparam name="approveFA" default="">
		<cfif trim(approveFA) eq "no">
			<cfif fa gt 0>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'First Alert Declined','fa')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update oneAimFirstAlerts
					set status = 'Declined'
					where firstalertid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fa#">
				</cfquery>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Declined',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'First Alert Sent for Approval','','First Alert form declined',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert form declined by #request.fname# #request.lname#">)
				</cfquery>
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.InvestigationLevel, oneAIMincidents.isWorkRelated, IncidentTypes.IncType, 
                         oneAIMincidents.PotentialRating, OSHACategories.Category, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, 
                         Groups.Group_Name, GroupLocations.SiteName, IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, 
                         NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss, oneAIMInjuryOI.isSerious, oneAIMEnvironmental.inBreach, 
                         oneAIMincidents.EnforcementNotice, oneAIMincidents.EnforcementNoticeType, NewDials_2.Name AS buname, Countries.CountryName, 
                         oneAIMincidents.SpecificLocation, Occupations.Occupation, oneAIMInjuryOI.ipGender, oneAimFirstAlerts.PreventAction, oneAimFirstAlerts.description AS FAdesc, 
                         oneAIMincidents.ActionTaken, oneAIMincidents.HSSEAdvisor, Groups.OUid, oneAIMincidents.PrimaryType, oneAIMincidents.reviewerEmail, oneAIMInjuryOI.injuryType
FROM            oneAIMInjuryOI LEFT OUTER JOIN
                         Occupations ON oneAIMInjuryOI.ipOccupation = Occupations.OccupationID LEFT OUTER JOIN
                         OSHACategories ON oneAIMInjuryOI.OSHAclass = OSHACategories.CatID RIGHT OUTER JOIN
                         Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid INNER JOIN
                         NewDials AS NewDials_2 ON Groups.Business_Line_ID = NewDials_2.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID ON 
                         GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID ON oneAIMInjuryOI.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				
				<cfquery name="getintype" datasource="#request.dsn#">
				SELECT     InjTypeID, InjuryType, Status
				FROM            InjuryTypes
				where InjTypeID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.injuryType#" list="Yes">)
				</cfquery>
				<cfset toem = "">
					<cfif trim(getemaildata.reviewerEmail) neq ''>
						<cfset toem = getemaildata.reviewerEmail>
					<cfelse>
						<cfif trim(getemaildata.createdbyEmail) neq ''>
							<cfset toem = getemaildata.createdbyEmail>
						</cfif>
					</cfif>
				<cfif trim(toem) neq ''>
				
				<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - First Alert declined">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - First Alert declined">
					</cfif>
				<cfmail to="#toem#" cc="#getemaildata.createdbyEmail#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							A first alert raised against incident <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a> has been declined by #request.fname# #request.lname#.  <br><Br></span>
							<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Senior Reviewer Comments:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><cfif trim(revcomments) neq ''>#revcomments#<cfelse>N/A</cfif></td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<!--- <tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
								</tr> --->
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
								</tr>
								<!--- <tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr> --->
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
				
				</cfif>
				
			</cfif>
		<cfelseif trim(approveFA) eq "yes">
			<cfif fa gt 0>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'First Alert Approved','fa')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update oneAimFirstAlerts
					set status = 'Approved', wasissued = 1
					where firstalertid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fa#">
				</cfquery>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'First Alert Sent for Approval','','First Alert form approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert form approved by #request.fname# #request.lname#">)
				</cfquery>
				<cfquery name="getincgroups" datasource="#request.dsn#">
					SELECT        oneAIMincidents.GroupNumber, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID
					FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number
					WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfset sendfatolist = "">
				<cfset emailids = "">
				<!--- <cfif trim(getincgroups.Business_Line_ID) neq ''>
						<cfset emailids = listappend(emailids,getincgroups.Business_Line_ID)>
				</cfif>
				<cfif trim(getincgroups.OUid) neq ''>
						<cfset emailids = listappend(emailids,getincgroups.OUid)>
				</cfif>
				<cfif trim(getincgroups.BusinessStreamID) neq ''>
						<cfset emailids = listappend(emailids,getincgroups.BusinessStreamID)>
				</cfif> --->
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.InvestigationLevel, oneAIMincidents.isWorkRelated, IncidentTypes.IncType, 
                         oneAIMincidents.PotentialRating, OSHACategories.Category, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, 
                         Groups.Group_Name, GroupLocations.SiteName, IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, 
                         NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss, oneAIMInjuryOI.isSerious, oneAIMEnvironmental.inBreach, 
                         oneAIMincidents.EnforcementNotice, oneAIMincidents.EnforcementNoticeType, NewDials_2.Name AS buname, Countries.CountryName, 
                         oneAIMincidents.SpecificLocation, Occupations.Occupation, oneAIMInjuryOI.ipGender, oneAimFirstAlerts.PreventAction, oneAimFirstAlerts.description AS FAdesc, 
                         oneAIMincidents.ActionTaken, oneAIMincidents.HSSEAdvisor, Groups.OUid, oneAIMincidents.PrimaryType, oneAIMInjuryOI.bodyPart, 
                         oneAIMInjuryOI.OSHAclass, oneAIMInjuryOI.injuryType
FROM            oneAIMInjuryOI LEFT OUTER JOIN
                         Occupations ON oneAIMInjuryOI.ipOccupation = Occupations.OccupationID LEFT OUTER JOIN
                         OSHACategories ON oneAIMInjuryOI.OSHAclass = OSHACategories.CatID RIGHT OUTER JOIN
                         Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid INNER JOIN
                         NewDials AS NewDials_2 ON Groups.Business_Line_ID = NewDials_2.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID ON 
                         GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         oneAIMEnvironmental ON oneAIMincidents.IRN = oneAIMEnvironmental.IRN LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID ON oneAIMInjuryOI.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfquery name="getintype" datasource="#request.dsn#">
				SELECT     InjTypeID, InjuryType, Status
				FROM            InjuryTypes
				where InjTypeID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.injuryType#" list="Yes">)
				</cfquery>
				<cfparam name="usbp" default="0">
<cfif trim(getemaildata.bodypart) neq ''>
	<cfset usbp = getemaildata.bodypart>
</cfif>
<cfquery name="getfabp" datasource="#request.dsn#">
SELECT        BodyPartID, BodyPart, Status
FROM            BodyParts
WHERE        (BodyPartID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usbp#" list="Yes">))
</cfquery>
				<cfset emfatype = "fa">
				<cfif getemaildata.primarytype eq 4>
					<cfset emfatype = "secFA">
				</cfif>
				<cfquery name="getemaillist" datasource="#request.dsn#">		
					SELECT        EmailAddress, DialID, type, emailtype
					FROM            DistributionEmail
					WHERE        (type = 'org') AND (emailtype = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#emfatype#">)	AND (DialID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2707,#getemaildata.Business_Line_ID#,#getemaildata.OUid#" list="Yes">))	and HiPoFAonly = 0 
				</cfquery>
				<cfset sendfatolist = valuelist(getemaillist.EmailAddress)>
				
				<cfif getemaildata.OSHAclass eq 2 or listfindnocase("E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5",getemaildata.PotentialRating) gt 0>
					<cfquery name="getemaillistFAhiPoonly" datasource="#request.dsn#">		
						SELECT        EmailAddress, DialID, type, emailtype
						FROM            DistributionEmail
						WHERE        (type = 'org') AND (emailtype = 'FA')	AND (DialID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2707" list="Yes">))	and HiPoFAonly = 1 
					</cfquery>
					<cfif getemaillistFAhiPoonly.recordcount gt 0>
						<cfset highpoemails = valuelist(getemaillistFAhiPoonly.EmailAddress)> 
						<cfset sendfatolist = listappend(sendfatolist,highpoemails)>
					</cfif>
					
				</cfif>
				
				<cfif trim(sendfatolist) neq ''>
					<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\actions\act_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
					<cfif directoryexists(filedir)>
						<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">
					</cfif>
					
					
					
					
					<cfset potcolor = "ffffff">
					<cfset potclass = "bodytext">
					<cfset pottext = "000000">
					<cfswitch expression="#getemaildata.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
							<cfset potcolor="00b050">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3">
							<cfset potcolor="ffc000">
							<cfset pottext = "000000">
						</cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
							<cfset potcolor="ff0000">
							<cfset potclass = "bodytextwhite">
							<cfset pottext = "ffffff">
						</cfcase>
					</cfswitch>
					
					<cfif  getemaildata.isNearMiss eq "yes">
						<cfset emsubj = "oneAIM Incident - First Alert - #getemaildata.IncType# - #getemaildata.buname# - #getemaildata.TrackingNum# - Near Miss">
					<cfelse>
						<cfset emsubj = "oneAIM Incident - First Alert - #getemaildata.IncType# - #getemaildata.buname# - #getemaildata.TrackingNum#">
					</cfif>
					<cfquery name="getfldaudit" datasource="#request.dsn#">
					SELECT        fieldauditid, fieldname, oldvalue, newvalue, enteredby, dateentered, irn
					FROM            oneAIMfieldAudits
					WHERE        (irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
					ORDER BY fieldname, dateentered DESC
					</cfquery>
					<cfquery name="getincamendcomments" datasource="#request.dsn#">
					SELECT        commentID, IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab
					FROM            oneAIMIncidentComments
					WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) and commenttab = 'reasonamend'
					ORDER BY commenttab desc ,DateEntered DESC
					</cfquery>
					<cfquery name="getOSHAcats" datasource="#request.dsn#">
					SELECT        CatID, Category, Status
					FROM            OSHACategories
					WHERE        (Status = 1)
					</cfquery>
					<cfmail to="#sendfatolist#" from="#request.userlogin#" type="html" subject="#emsubj#">
					
					<CFMAILPARAM NAME="X-Priority" VALUE="1">
					<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">A First Alert has been raised for the following incident:<br>
							<!--- An incident with a Level 2 investigation and First Alert has been raised.  
							<br>Please go to oneAIM via this link to view the incident and to review and accept/decline the First Alert � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span> --->
						<table cellpadding="6" cellspacing="0" border="0" bgcolor="ffffff" width="60%">
							<tr>
								<td width="100%">
							<table width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td colspan="4" style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Incident Classification</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Primary Incident type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.IncType#</td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Near Miss?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.isNearMiss#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>OSHA Classification:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.category) neq ''>#getemaildata.category#<cfelse>N/A</cfif></td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Incident Potential Rating:</strong></td>
									<td bgcolor="#potcolor#" class="#potclass#"  style="font-family:Segoe UI;font-size:10pt;color:###pottext#;" width="25%">#getemaildata.PotentialRating#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Is injury classified as a Serious Incident?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.isSerious) eq ''>N/A<cfelse>#getemaildata.isSerious#</cfif></td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Environmental permit or licence breached?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.inBreach) eq ''>N/A<cfelse>#getemaildata.inBreach#</cfif></td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Has an enforcement notice been issued?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.EnforcementNotice) eq ''>N/A<cfelse>#getemaildata.EnforcementNotice#</cfif></td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Type of enforcement notice?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.EnforcementNoticeType) eq ''>N/A<cfelse>#getemaildata.EnforcementNoticeType#</cfif></td>
								</tr>
							</table>
								</td></tr>
								<tr><td width="100%">
							<table width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td colspan="4" style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Incident Location</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>#request.bulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.buname#</td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.bsname) eq ''>N/A<cfelse>#getemaildata.bsname#</cfif></td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Country:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.CountryName#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.group_name#</td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Site/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.sitename#</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Specific Location:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%" colspan="3">#getemaildata.SpecificLocation#</td>
								</tr>
							</table>
							</td></tr>
								<tr><td width="100%">
							<table width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td colspan="4" style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Incident Details</td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Incident Date/Time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Incident Assigned To:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%">#getemaildata.IncAssignedTo#</td>
								</tr>
								<cfif listfind("1,5",getemaildata.PrimaryType) gt 0>
								
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Injury Type:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getintype.InjuryType) eq ''>N/A<cfelse>#replace(valuelist(getintype.InjuryType),",",", ","all")#</cfif></td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Body Part Affected:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif getfabp.recordcount eq 0>N/A<cfelse>#replace(valuelist(getfabp.bodypart),",",", ","all")#</cfif></td>
								</tr>
								<tr>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Occupation:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.occupation) eq ''>N/A<cfelse>#getemaildata.occupation#</cfif></td>
									<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><strong>Gender:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" width="25%"><cfif trim(getemaildata.ipGender) eq ''>N/A<cfelse>#getemaildata.ipGender#</cfif></td>
								</tr>
								</cfif>
							</table>
							</td></tr>
								<tr><td width="100%">
							<table  width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Description of incident</td>
								</tr>
								<tr>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" >#getemaildata.FAdesc#</td>
								</tr>
							</table>
							</td></tr>
								<tr><td width="100%">
							<table width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Immediate action taken</td>
								</tr>
								<tr>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" >#getemaildata.ActionTaken#</td>
								</tr>
							</table>
							</td></tr>
								
										
<cfif getfldaudit.recordcount gt 0>
<cfset oshacatstruct = {}>
<cfloop query="getOSHAcats">
	<cfset oshacatstruct[catid] = category>
</cfloop>
<tr>
	
	<td width="100%">
	<table  width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">

<tr>
	<td  style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Field Updates</td>
</tr>
<tr>
	<td bgcolor="ffffff" >
		<table bgcolor="ffffff" cellpadding="3" cellspacing="1" width="100%">
		<tr bgcolor="f7f1f9">
			<td class="formlable" width="20%" bgcolor="f7f1f9" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" ><strong>Field</strong></td>
			<td class="formlable" align="center" width="20%" bgcolor="f7f1f9" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" ><strong>Old Value</strong></td>
			<td class="formlable" align="center" width="20%" bgcolor="f7f1f9" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" ><strong>New Value</strong></td>
			<td class="formlable" width="20%" bgcolor="f7f1f9" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" ><strong>Changed By</strong></td>
			<td class="formlable" width="20%" bgcolor="f7f1f9" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" ><strong>Date Changed</strong></td>
		</tr> 
		
		<cfloop query="getfldaudit">
			<cfif fieldname eq "invlevel">
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Investigation Level</td>
					<td class="bodyTextGrey"  align="center" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>Level #oldvalue#</cfif></td>
					<td class="bodyTextGrey"  align="center" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #newvalue#</td>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#enteredby#</td>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			<cfelseif fieldname eq "oshacat">
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">OSHA Classification</td>
					<td class="bodyTextGrey" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"  align="center"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse><cfif structkeyexists(oshacatstruct,oldvalue)>#oshacatstruct[oldvalue]#</cfif></cfif></td>
					<td class="bodyTextGrey" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"  align="center"><cfif structkeyexists(oshacatstruct,newvalue)>#oshacatstruct[newvalue]#</cfif></td>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#enteredby#</td>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			<cfelseif fieldname eq "potrating">
			<cfset potcolor = "ffffff">
				<cfset pottxt = "000000">
				<cfswitch expression="#oldvalue#">
					<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<cfset potcolor="00b050">
						<cfset pottxt = "ffffff">
					</cfcase>
					<cfcase value="E1,D2,E2,A3,B3,C3,D3">
						<cfset potcolor="ffc000">
						<cfset pottxt = "000000">
					</cfcase>
					<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<cfset potcolor="ff0000">
						<cfset pottxt = "ffffff">
					</cfcase>
				</cfswitch>
				<cfset potcolor2 = "ffffff">
				<cfset pottxt2 = "000000">
				<cfswitch expression="#newvalue#">
					<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<cfset potcolor2="00b050">
						<cfset pottxt2 = "ffffff">
					</cfcase>
					<cfcase value="E1,D2,E2,A3,B3,C3,D3">
						<cfset potcolor2="ffc000">
						<cfset pottxt2 = "000000">
					</cfcase>
					<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<cfset potcolor2="ff0000">
						<cfset pottxt2 = "ffffff">
					</cfcase>
				</cfswitch>
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Potential Rating</td>
					<td    align="center" ><span style="font-family:Segoe UI;font-size:10pt;color:###pottxt#;background-color:###potcolor#;padding-left:8px;padding-right:8px;padding-bottom:2px;">&nbsp;&nbsp;<cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>#oldvalue#</cfif>&nbsp;&nbsp;</span></td>
					<td   align="center"><span style="font-family:Segoe UI;font-size:10pt;color:###pottxt2#;background-color:###potcolor2#;padding-left:8px;padding-right:8px;padding-bottom:2px;">&nbsp;&nbsp;#newvalue#&nbsp;&nbsp;</span></td>
					<td class="bodyTextGrey" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" >#enteredby#</td>
					<td class="bodyTextGrey"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			</cfif>
		</cfloop>
		</table>
	</td>
</tr>
</table></td></tr>
</cfif>


<cfif getincamendcomments.recordcount gt 0>
<tr>
	
	<td width="100%">
	<table  width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">

<tr>
	<td  style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Reason for Amendment</td>
</tr>
<tr>
	<td bgcolor="ffffff" >
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr bgcolor="f7f1f9">
				<td class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"  bgcolor="f7f1f9"><strong>Reason</strong></td>
				<td class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"  bgcolor="f7f1f9"><strong>Entered By</strong></td>
				<td class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"  bgcolor="f7f1f9"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getincamendcomments">
			<tr>
				<td class="bodyTextGrey" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#Comment#</td>
				<td class="bodyTextGrey" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#EnteredBy#</td>
				<td class="bodyTextGrey" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr></table></td></tr>
</cfif>
								
								
							</table>
							<br><br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">For further details on this incident please go to oneAIM via this link � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a> 
							<br><br>If you cannot access oneAIM but require further information please contact the HSSE Advisor for this incident � #getemaildata.HSSEAdvisor#<br><br>Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
							<br><br>
						<cfif isdefined("getincfileslist.recordcount")>
							<cfif getincfileslist.recordcount gt 0>
								<cfloop query="getincfileslist">
									<cfmailparam file="#filedir#\#name#">
								</cfloop>
							</cfif>
						</cfif>
					</cfmail><!--- <tr><td width="100%">
							<table width="100%" class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
								<tr>
									<td style="font-family:Segoe UI;font-size:10pt;color:ffffff;">Proposed action to prevent reoccurrence</td>
								</tr>
								<tr>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" >#getemaildata.PreventAction#</td>
								</tr>
							</table>
							</td></tr>
					 --->
					
					
					
					
				</cfif>
				
				
			</cfif>
		</cfif>
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
</cfswitch>