<cfparam name="submittype" default="">
<cfparam name="irn" default="0">
<cfparam name="potentialrating" default="">
<cfparam name="oldPR" default="">
<cfparam name="oldIL" default="">
<cfparam name="oldFA" default="">
<cfparam name="invlevel" default="0">
<cfparam name="needFA" default="0">
<cfparam name="revcomments" default="">
<cfswitch expression="#submittype#">
	<cfcase value="workrelated">
	
		<cfset auditaddon = "">
		
		<cfif trim(irn) gt 0>
			<cfif oldPR neq potentialrating>
				<cfquery name="addfaudit" datasource="#request.dsn#">
					insert into	oneAIMfieldAudits ( fieldname, oldvalue, newvalue, enteredby, dateentered, irn)
					values ('potrating',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oldPR#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>			
				<cfset auditaddon = auditaddon &  "<br>Potential Rating changed from #oldPR# to #potentialrating#">
			</cfif>
			<cfif oldIL neq invlevel>
				<cfquery name="addfaudit" datasource="#request.dsn#">
					insert into	oneAIMfieldAudits ( fieldname, oldvalue, newvalue, enteredby, dateentered, irn)
					values ('invlevel',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oldIL#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>	
				<cfset auditaddon = auditaddon &  "<br>Investigation Level changed from #oldIL# to #invlevel#">
			</cfif>
			<cfif oldFA neq needFA>
				<cfquery name="addfaudit" datasource="#request.dsn#">
					insert into	oneAIMfieldAudits ( fieldname, oldvalue, newvalue, enteredby, dateentered, irn)
					values ('fa',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oldFA#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#needFA#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>	
				<cfset dspFA = "No">
				<cfset dspnowFA = "No">
				<cfif oldFA eq 1>
					<cfset dspFA = "Yes">
				</cfif>
				<cfif needFA eq 1>
					<cfset dspnowFA = "Yes">
				</cfif>
				<cfset auditaddon = auditaddon &  "<br>First Alert changed from #dspFA# to #dspnowFA#">
			</cfif>
			
			
			
				<cfparam name="revcomments" default="No comment added">
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Incident Review Complete')
					</cfquery>
				</cfif>
			
			<cfset wfprocess = "">
			<cfif oldIL eq 1 and invlevel eq 1>
				<cfif needFA eq 1>
					<cfset wfprocess = "L1_L1_wFA">
				<cfelse>
					<cfset wfprocess = "L1_L1_noFA">
				</cfif>
			</cfif>
			<cfif oldIL eq 1 and invlevel eq 2>
				<cfif needFA eq 1>
					<cfset wfprocess = "L1_L2_wFA">
				<cfelse>
					<cfset wfprocess = "L1_L2_noFA">
				</cfif>
			</cfif>
			<cfif oldIL eq 2 and invlevel eq 2>
				<cfif needFA eq 1>
					<cfset wfprocess = "L2_L2_wFA">
				<cfelse>
					<cfset wfprocess = "L2_L2_noFA">
				</cfif>
			</cfif>
			<cfif oldIL eq 2 and invlevel eq 1>
				<cfif needFA eq 1>
					<cfset wfprocess = "L2_L1_wFA">
				<cfelse>
					<cfset wfprocess = "L2_L1_noFA">
				</cfif>
			</cfif>
						
		
			<cfswitch expression="#wfprocess#">
				<cfcase value="L1_L1_wFA">
					<cfinclude template="L1_L1_wFA.cfm">
				</cfcase>
				<cfcase value="L1_L1_noFA">
					<cfinclude template="L1_L1_noFA.cfm">
				</cfcase>
				<cfcase value="L1_L2_wFA">
					<cfinclude template="L1_L2_wFA.cfm">
				</cfcase>
				<cfcase value="L1_L2_noFA">
					<cfinclude template="L1_L2_noFA.cfm">
				</cfcase>
				<cfcase value="L2_L2_wFA">
					<cfinclude template="L2_L2_wFA.cfm">
				</cfcase>
				<cfcase value="L2_L2_noFA">
					<cfinclude template="L2_L2_noFA.cfm">
				</cfcase>
				<cfcase value="L2_L1_wFA">
					<cfinclude template="L2_L1_wFA.cfm">
				</cfcase>
				<cfcase value="L2_L1_noFA">
					<cfinclude template="L2_L1_noFA.cfm">
				</cfcase>
			</cfswitch>
				
			
			
		</cfif>
		
		
		
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="notworkrelated">
	<cfset auditaddon = "">
									<cfif trim(irn) gt 0>
										<cfif isdefined("changetowr")>
											<cfif changetowr eq 1>
											<cfquery name="updateincident" datasource="#request.dsn#">
												update oneAIMincidents
												set isworkrelated = 'Yes' ,
												status = 'Started'
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											<cfquery name="updatefa" datasource="#request.dsn#">
												update oneAimFirstAlerts
												set	status = 'Started'
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											<cfif trim(revcomments) neq ''>
												<cfquery name="addcomment" datasource="#request.dsn#">
													insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype)
													values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Incident Review Complete')
												</cfquery>
											</cfif>
											<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.trackingnum, oneAIMincidents.GroupNumber,oneAIMincidents.withdrawndate, oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, 
							                         IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, 
							                         IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, 
							                        oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail,oneAIMincidents.createdby,
													Groups.business_line_id, oneAIMincidents.isNearMiss
											FROM            IncidentAssignedTo RIGHT OUTER JOIN
					                         oneAIMincidents INNER JOIN
					                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON IncidentAssignedTo.IncAssignedID = oneAIMincidents.IncAssignedTo LEFT OUTER JOIN
					                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
					                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
					                         OSHACategories RIGHT OUTER JOIN
					                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
					                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
					                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
					                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfquery name="getbuname" datasource="#request.dsn#">
												SELECT      Name
												FROM            NewDials
												WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
											</cfquery>
																		
											
											<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Raised',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdby#">, 'Changed to Work Related',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report changed to work related by #request.fname# #request.lname#">)
												</cfquery>
											
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
											<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss - Work related status has changed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Work related status has changed">
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
											<cfmail to="#getemaildata.createdbyEmail#" subject="#emsubj#" from="#request.userlogin#" type="html">
														
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
														The work related status on the following incident has changed. <br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
													</cfmail>
											</cfif>
											
											
											
											
									<script type="text/javascript">
										window.opener.location.assign("index.cfm?fuseaction=main.main");
										window.close();
									</script>
											
											<cfabort>
											
											
											
											
											</cfif>
										</cfif>
										<cfif oldPR neq potentialrating>
											<cfquery name="addfaudit" datasource="#request.dsn#">
												insert into	oneAIMfieldAudits ( fieldname, oldvalue, newvalue, enteredby, dateentered, irn)
												values ('potrating',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oldPR#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>		
											<cfset auditaddon = auditaddon &  "<br>Potential Rating changed from #oldPR# to #potentialrating#">	
										</cfif>
										<cfif oldIL neq invlevel>
											<cfquery name="addfaudit" datasource="#request.dsn#">
												insert into	oneAIMfieldAudits ( fieldname, oldvalue, newvalue, enteredby, dateentered, irn)
												values ('invlevel',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oldIL#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>	
											<cfset auditaddon = auditaddon &  "<br>Investigation Level changed from #oldIL# to #invlevel#">
										</cfif>
										<cfif oldFA neq needFA>
											<cfquery name="addfaudit" datasource="#request.dsn#">
												insert into	oneAIMfieldAudits ( fieldname, oldvalue, newvalue, enteredby, dateentered, irn)
												values ('fa',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oldFA#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#needFA#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>	
											<cfset dspFA = "No">
											<cfset dspnowFA = "No">
											<cfif oldFA eq 1>
												<cfset dspFA = "Yes">
											</cfif>
											<cfif needFA eq 1>
												<cfset dspnowFA = "Yes">
											</cfif>
											<cfset auditaddon = auditaddon &  "<br>First Alert changed from #dspFA# to #dspnowFA#">
										</cfif>
										
										
										
											<cfparam name="revcomments" default="No comment added">
											<cfif trim(revcomments) neq ''>
												<cfquery name="addcomment" datasource="#request.dsn#">
													insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype)
													values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Incident Review Complete')
												</cfquery>
											</cfif>
										<cfif invlevel eq 2>
											<cfif needFA eq 0>
												<cfset needFA = 1>
											</cfif>
										</cfif>
										
										<cfif needFA eq 1>
										
											<cfquery name="getfastat" datasource="#request.dsn#">
												SELECT        Status
												FROM            oneAimFirstAlerts
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											
											<cfif getfastat.status eq "Sent for Review">
										
										<cfquery name="updatefa" datasource="#request.dsn#">
											update oneAimFirstAlerts
											set status = 'Sent for Approval'
											where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
										</cfquery>
										
										<cfquery name="getsrreviewer" datasource="#request.dsn#">
															SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
										FROM            Users INNER JOIN
										                         UserRoles ON Users.UserId = UserRoles.UserID
										WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND (UserRoles.AssignedLocs IN
										                             (SELECT        Business_Line_ID
										                               FROM            Groups
										                               WHERE        (Group_Number =
										                                                             (SELECT        GroupNumber
										                                                               FROM            oneAIMincidents
										                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
														
										</cfquery>
										<cfset tolistemail = "">
										<cfset tolistnames = "">
										
										<cfloop query="getsrreviewer">
											<cfset tolistemail = listappend(tolistemail,Useremail)>
											<cfset tolistnames = listappend(tolistnames,"#Firstname# #lastname#")>
										</cfloop>
										
										
										
										<cfquery name="addaudit" datasource="#request.dsn#">
										insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
										values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'First Alert Sent for Approval',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'First Alert Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'First Alert form sent for approval',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="First Alert form sent for approval by #request.fname# #request.lname# to #tolistnames#">)
										</cfquery>
										<cfquery name="getemaildata" datasource="#request.dsn#">
										SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
										                    oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
										                    oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
										                    oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
										                    IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, Groups.Business_Line_ID, NewDials.Name AS ouname, NewDials_1.Name AS bsname, oneAIMincidents.isNearMiss
										FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.OUid LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.BusinessStreamID = NewDials_1.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
										WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
										</cfquery>
										<cfquery name="getbuname" datasource="#request.dsn#">
												SELECT      Name
												FROM            NewDials
												WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
											</cfquery>
									<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
										
										
											<cfif trim(tolistemail) neq ''>
							<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident and First Alert review - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss">
												<cfelse>
													<cfset emsubj = "oneAIM Incident and First Alert review - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType#">
												</cfif>

							<cfmail to="#tolistemail#" subject="#emsubj#" from="#request.userlogin#" type="html">
									
									<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
									An incident with a First Alert has been raised.  
									<br>Please go to oneAIM via this link to view the incident and to review and accept/decline the First Alert � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
									<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
										
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
										</tr>
										<tr>
											<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
											<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.PotentialRating#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
										</tr>
										<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
										<tr>
											<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
										</tr>
										<tr>
											<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
										</tr>
										<tr>
											<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
										</tr>
										<tr>
											<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
											<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
										</tr>
									</table>
									<br>
									<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
								</cfmail>
								</cfif>
											<cfelse>
											<cfquery name="getinitiator" datasource="#request.dsn#">
												select createdBy, createdbyEmail
												from oneAIMincidents
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											
											
											<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.trackingnum, oneAIMincidents.GroupNumber,oneAIMincidents.withdrawndate, oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, 
							                         IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, 
							                         IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, 
							                         oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail,Groups.business_line_id, oneAIMincidents.isNearMiss
											FROM            GroupLocations RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfquery name="getbuname" datasource="#request.dsn#">
												SELECT      Name
												FROM            NewDials
												WHERE        (ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
											</cfquery>
											<cfquery name="getccs" datasource="#request.dsn#">
												SELECT       Users.Useremail
												FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
												WHERE     Users.status = 1 and   (UserRoles.UserRole = 'BU Admin') AND (UserRoles.AssignedLocs =
							                             (SELECT        business_line_id
							                               FROM            Groups
							                               WHERE        (Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.business_line_id#">)))
											</cfquery>
											<cfset gotocc = valuelist(getccs.Useremail)>
												<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Near Miss - Complete First Alert form">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getbuname.Name# - #getemaildata.IncType# - Complete First Alert form">
												</cfif>
											<cfmail to="#getemaildata.createdbyEmail#" cc="#gotocc#" subject="#emsubj#" from="#request.userlogin#" type="html">
														
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
														The Investigation Level of the following incident has been changed from Level 1 to Level 2 and/or a First Alert form is now required to be completed.  
														<br>Please go to oneAIM via this link to complete the First Alert form � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg" bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
													</cfmail>
											</cfif>
											
											</cfif>
											
											
											
											
											
											
											
											
											
											
											<cfquery name="getinccapa" datasource="#request.dsn#">
												select DateComplete,status
												from oneAIMCAPA
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
											</cfquery>
											<!--- <cfset newincstat = "Review Completed"> --->
											<!--- <cfquery name="getinccapa" datasource="#request.dsn#">
												select DateComplete,status
												from oneAIMCAPA
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery> --->
											<cfset newincstat = "Closed">
												<cfloop query="getinccapa">
													<cfif status eq "Open">
														<cfset newincstat = "Review Completed">
														<cfbreak>
													</cfif>
												</cfloop>
										
										
										<cfif newincstat eq "closed">
											<cfquery name="getinctype" datasource="#request.dsn#">
												select PrimaryType,SecondaryType
												from oneAIMincidents
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
												<cfquery name="getoshacat" datasource="#request.dsn#">
													SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
													FROM            oneAIMInjuryOI
													where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
												</cfquery>
												<cfif getoshacat.OSHAclass eq 2>
													<cfif trim(getoshacat.LTIDateReturned) eq ''>
														<cfset newincstat = "Review Completed">
													</cfif>
												<cfelseif getoshacat.OSHAclass eq 4> 
													<cfif trim(getoshacat.RWCDateReturned) eq ''>
														<cfset newincstat = "Review Completed">
													</cfif>
												</cfif> 
											</cfif>
										</cfif>
										
											<cfquery name="updateincident" datasource="#request.dsn#">
												update oneAIMincidents
												set PotentialRating = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">, 
												InvestigationLevel = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">,
												needFA = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#needFA#">,
												status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newincstat#">
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											
											<cfif newincstat eq "closed">
											<cfquery name="getnextperson" datasource="#request.dsn#">
												SELECT        createdBy
												FROM            oneAIMincidents
												WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset acttaken = "Closed">
										<cfif trim(auditaddon) neq ''>
										<cfset acttaken = acttaken & "#auditaddon#">
										</cfif>
											
												<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review','', <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#acttaken#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report closed by #request.fname# #request.lname#">)
												</cfquery>
												<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus,  SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getnextperson.createdBy#">,'Sent for First Alert',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report closed by #request.fname# #request.lname#">)
												</cfquery>
												<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<!--- <cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))> --->
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
											
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
											<cfelse>
												<cfset acttaken = "Review Complete">
										<cfif trim(auditaddon) neq ''>
										<cfset acttaken = acttaken & "#auditaddon#">
										</cfif>
												<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review','', <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#acttaken#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report review completed by #request.fname# #request.lname#">)
												</cfquery>
												<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus,  SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Review Complete','','Sent for First Alert',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report review completed by #request.fname# #request.lname#">)
												</cfquery>
											
											
											<!--- E10 Closed email --->
											
											</cfif>
											
											
											
											
										<cfelse>
										
											<cfquery name="getinccapa" datasource="#request.dsn#">
												select DateComplete,status
												from oneAIMCAPA
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
											</cfquery>
											<cfset newincstat = "Closed">
												<cfloop query="getinccapa">
													<cfif status eq "Open">
														<cfset newincstat = "Review Completed">
														<cfbreak>
													</cfif>
												</cfloop>
										
										
										<cfif newincstat eq "closed">
											<cfquery name="getinctype" datasource="#request.dsn#">
												select PrimaryType,SecondaryType
												from oneAIMincidents
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
												<cfquery name="getoshacat" datasource="#request.dsn#">
													SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
													FROM            oneAIMInjuryOI
													where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
												</cfquery>
												<cfif getoshacat.OSHAclass eq 2>
													<cfif trim(getoshacat.LTIDateReturned) eq ''>
														<cfset newincstat = "Review Completed">
													</cfif>
												<cfelseif getoshacat.OSHAclass eq 4> 
													<cfif trim(getoshacat.RWCDateReturned) eq ''>
														<cfset newincstat = "Review Completed">
													</cfif>
												</cfif> 
											</cfif>
										</cfif>
										
										
											<cfif newincstat eq "closed">
												<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review','', 'Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report closed by #request.fname# #request.lname#">)
												</cfquery>
												<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<!--- <cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))> --->
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
												</cfif>
											<cfelse>
											<cfset acttaken = "Review Complete">
										<cfif trim(auditaddon) neq ''>
										<cfset acttaken = acttaken & "#auditaddon#">
										</cfif>
												<cfquery name="addaudit" datasource="#request.dsn#">
												insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
												values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review','', <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#acttaken#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report review completed by #request.fname# #request.lname#">)
												</cfquery>
											</cfif>
											<cfquery name="updateincident" datasource="#request.dsn#">
												update oneAIMincidents
												set PotentialRating = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#potentialrating#">, 
												InvestigationLevel = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#invlevel#">,
												needFA = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#needFA#">,
												status = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#newincstat#">
												where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
											</cfquery>
											
											<!--- E10 Closed email --->
											
										</cfif>
										
									</cfif>
									<script type="text/javascript">
										window.opener.location.assign("index.cfm?fuseaction=main.main");
										window.close();
									</script>
</cfcase>
	
	<cfcase value="reviewfat">
			<cfquery name="updateincident" datasource="#request.dsn#">
					update oneAIMincidents
					set status = 'Closed'
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
			<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review','', 'Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident report closed by #request.fname# #request.lname#">)
				</cfquery>
		<cfparam name="revcomments" default="No comment added">
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Incident Report Approved')
					</cfquery>
				</cfif>
			<cfquery name="getemail" datasource="#request.dsn#">
				select createdbyEmail
				from oneAIMincidents
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			<cfif trim(getemail.createdbyEmail) neq ''  and trim(request.userlogin) neq ''>
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.isWorkRelated, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, Groups.Group_Name, NewDials.Name AS buname, 
                         NewDials_1.Name AS ouname, NewDials_2.Name AS bsname, GroupLocations.SiteName, IncidentAssignedTo.IncAssignedTo, oneAIMincidents.isFatality, 
                         oneAIMincidents.TrackingNum, oneAIMincidents.PrimaryType, IncidentTypes.IncType, OSHACategories.Category, oneAIMincidents.PotentialRating, 
                         oneAIMincidents.InvestigationLevel, oneAIMincidents.Description,Groups.Business_Line_ID, oneAIMincidents.isNearMiss
FROM            OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID ON oneAIMInjuryOI.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
				WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - Fatality incident closed">
												
				<cfmail to="#getemail.createdbyEmail#" subject="#emsubj#" from="#request.userlogin#" type="html">
							
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							The following incident is now closed. Any amendments will require this record to be re-opened. Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.fatalityreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
							<table class="purplebg" cellpadding="4" cellspacing="0" border="1" bgcolor="5f2468" bordercolor="5f2468">
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Reviewer comments:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><cfif trim(revcomments) neq ''>#revcomments#</cfif></td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.bulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.buname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
								</tr>
								<tr>
									<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
									<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
								</tr>
							</table>
							<br>
							<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
			
		
				
				
				
			</cfif>
			
			<cfquery name="getFATemails" datasource="#request.dsn#">
					SELECT        EmailAddress, DialID, type, emailtype
FROM            DistributionEmail
WHERE        (emailtype = 'fatality') AND (type = 'org') AND (DialID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="2707,#getemaildata.business_line_id#" list="yes">))
				</cfquery>
				
				<cfset fatrecips = valuelist(getFATemails.EmailAddress)>
				<cfif trim(fatrecips) neq ''>
				<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - Fatality Incident">
				<cfmail to="#fatrecips#" subject="#emsubj#" from="#request.userlogin#" type="html">
					
					<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
					A Fatality incident has occurred.<br><Br></span>
					<table bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
						<tr>
							<td bgcolor="f7f1f9" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
							<td bgcolor="ffffff" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
						</tr>
						<tr>
							<td bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
							<td bgcolor="ffffff"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
						</tr>
						<!--- <tr>
							<td class="formlable"><strong>Incident description:</strong></td>
							<td bgcolor="ffffff" class="bodytext"></td>
						</tr> --->
						<tr>
							<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.bulabellong#:</strong></td>
							<td bgcolor="ffffff"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.buname#</td>
						</tr>
						<tr>
							<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
							<td bgcolor="ffffff"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
						</tr>
						<tr>
							<td bgcolor="f7f1f9"  class="formlable"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
							<td bgcolor="ffffff" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
						</tr>
						<tr>
							<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
							<td bgcolor="ffffff"  style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
						</tr>
						<tr>
							<td  bgcolor="f7f1f9" class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
							<td bgcolor="ffffff" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
						</tr>
						<tr>
							<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
							<td bgcolor="ffffff" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
						</tr>
					</table>
					<br>
					<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
					<CFMAILPARAM NAME="X-Priority" VALUE="1">
				</cfmail>
				</cfif>
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
		
	</cfcase>
	
</cfswitch>