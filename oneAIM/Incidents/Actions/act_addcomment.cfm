<cfparam name="irn" default="0">
<cfparam name="thecomment" default="">
<cfparam name="processfrm" default="">
<cfif processfrm eq "yes">
	<cfif irn gt 0 and trim(thecomment) neq ''>
		<cfquery name="addcomment" datasource="#request.dsn#">
			insert into IncidentComments (IRN, Comment, DateEntered, EnteredBy, Status)
			values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#IRN#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#theComment#">, <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">, <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">, 1)
		</cfquery>
	</cfif>
</cfif>