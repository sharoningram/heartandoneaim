<cfparam name="irn" default="20150012">
<cfquery name="getinv" datasource="#request.dsn#">
SELECT        IncidentInvestigation.InvID
FROM            IncidentInvestigation 
WHERE IncidentInvestigation.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
</cfquery>

<cfif getinv.recordcount eq 0>
	<cfquery name="addinv" datasource="#request.dsn#">
		insert into IncidentInvestigation (irn)
		values(<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
		
		select ident_current('IncidentInvestigation') as newinv
	</cfquery>
	<cfset theinvid = addinv.newinv>
<cfelse>
	<cfset theinvid = getinv.invid>
</cfif>

<cfquery name="getinvdetail" datasource="#request.dsn#">
SELECT        IncidentInvestigation.InvID, IncidentInvestigation.status, IncidentInvestigation.IRN, IncidentInvestigation.SrSiteRep, IncidentInvestigation.InvCondBy, IncidentInvestigation.TeamMembers, 
                         IncidentInvestigation.InvSummary, IncidentInvestigation.DirSupervisor, IncidentInvestigation.InvRelevance, IncidentInvestigation.rulebreach, IncidentInvestigation.daysinceoff,
                         IncidentInvestigation.whynorulebreach, IncidentInvestigation.essentialbreach, IncidentInvestigation.whynoessbreach, IncidentAZ.AZType, IncidentAZ.IncAZid, 
                         IncidentAZ.AZCatID, IncidentAZ.AZFactorID, IncidentAZ.AZSubCause, IncidentAZ.AZComment, IncidentAZ.DateCreated, AZfactors.CategoryLetter, IncidentInvestigation.ruleother
FROM            IncidentInvestigation LEFT OUTER JOIN
                         AZfactors RIGHT OUTER JOIN
                         IncidentAZ ON AZfactors.FactorID = IncidentAZ.AZFactorID ON IncidentInvestigation.InvID = IncidentAZ.InvID
WHERE IncidentInvestigation.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
ORDER BY IncidentAZ.DateCreated, IncidentAZ.AZType, IncidentAZ.IncAZid
</cfquery>

<cfquery name="getteammembers" datasource="#request.dsn#">
	SELECT        TeamID, MemberName, RelevenceToInv, IRN, invid
	FROM            InvestigationTeamMembers
	WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>