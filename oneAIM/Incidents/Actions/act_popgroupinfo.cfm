<cfparam name="gnum" default="0">
<cfparam name="sitenum" default="0">
<cfparam name="currloc" default="">
<cfif trim(currloc) neq '' and trim(currloc) gt 0>
	<cfset sitenum = currloc>
</cfif>
<cfif trim(sitenum) eq ''>
	<cfset sitenum = 0>
</cfif>

<cfif gnum gt 0><!--- SELECT        Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, Countries.CountryName, 
                         NewDials_1.Name AS BU, NewDials.Name AS OU, NewDials_2.Name AS BS, GroupLocations.GroupLocID, GroupLocations.SiteName
FROM            Groups INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID LEFT OUTER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         Countries ON Groups.Country_Code = Countries.CountryID --->
<cfquery name="getgroupinfo" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, Countries.CountryName, 
                         NewDials_1.Name AS BU, NewDials.Name AS OU, NewDials_2.Name AS BS, GroupLocations.GroupLocID, GroupLocations.SiteName
FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         Groups INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON GroupLocations.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnum#">) AND (GroupLocations.entryType IN ('both', 'IncidentOnly')) AND (GroupLocations.isActive = 1)
order by Groups.Group_Name,GroupLocations.SiteName
</cfquery>
<cfquery name="getsrrev" datasource="#request.dsn#">
SELECT        Users.Lastname, Users.Firstname, UserRoles.AssignedLocs, Users.Useremail
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.Business_Line_ID#">)
</cfquery>
<cfoutput><cfif getsrrev.recordcount gt 0>{pp{#getsrrev.Lastname#, #getsrrev.Firstname#}pp}<cfelse>{pp{}pp}</cfif></cfoutput>

<cfquery name="getrev" datasource="#request.dsn#">
SELECT        Users.Lastname, Users.Firstname, UserRoles.AssignedLocs, Users.Useremail
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.OUid#">)
</cfquery>
<cfoutput>{rr{#getrev.Lastname#, #getrev.Firstname#}rr}</cfoutput>


<cfif getrev.recordcount eq 1>
{aa{<select name="reviewerEmail" size="1" class="selectgen" id="lrgselect">
					<cfoutput query="getrev">
					<option value="#Useremail#" selected>#lastname#, #firstname#</option>
					</cfoutput>
						</select>}aa}
<cfelseif getrev.recordcount gt 1>
{aa{<select name="reviewerEmail" size="1" class="selectgen" id="lrgselect">
	<option value="">-- Select One --</option>
					<cfoutput query="getrev">
					<option value="#Useremail#">#lastname#, #firstname#</option>
					</cfoutput>
						</select>}aa}
<cfelseif getrev.recordcount eq 0>
{aa{<select name="reviewerEmail" size="1" class="selectgen" id="lrgselect">
			<option value="">-- Select One --</option></select>}aa}
</cfif>

<cfquery name="getadv" datasource="#request.dsn#">
SELECT        Users.Lastname, Users.Firstname, UserRoles.AssignedLocs, Users.Useremail
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'HSSE Advisor') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.OUid#">)
</cfquery>


<cfif getadv.recordcount eq 1>
{vv{<select name="hsseadvisor" size="1" class="selectgen">
					<cfoutput query="getadv">
					<option value="#lastname#, #firstname#" selected>#lastname#, #firstname#</option>
					</cfoutput>
						</select>}vv}
<cfelseif getadv.recordcount gt 1>
{vv{<select name="hsseadvisor" size="1" class="selectgen">
	<option value="">-- Select One --</option>
					<cfoutput query="getadv">
					<option value="#lastname#, #firstname#">#lastname#, #firstname#</option>
					</cfoutput>
						</select>}vv}
<cfelseif getadv.recordcount eq 0>
{vv{<select name="hsseadvisor" size="1" class="selectgen">
	<option value="">-- Select One --</option></select>}vv}
</cfif>



<cfoutput>
{zz{#getgroupinfo.bu#}zz}

{oo{#getgroupinfo.ou#}oo}
<cfif getgroupinfo.recordcount eq 1>
{cc{#getgroupinfo.countryname#}cc}

<cfquery name="getcli" datasource="#request.dsn#">
SELECT        GroupLocations.ClientID, Clients.ClientName
FROM            GroupLocations INNER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID
WHERE        (GroupLocations.GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.Group_Number#">) AND (GroupLocations.GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroupinfo.GroupLocID#">)
</cfquery>
<cfoutput><cfif getcli.recordcount gt 0>{qq{#getcli.ClientName#}qq}<cfelse>{qq{}qq}</cfif></cfoutput>
<cfelse>
<cfquery name="getloccnty" datasource="#request.dsn#">
SELECT        Countries.CountryName
FROM            GroupLocations INNER JOIN
                         Countries ON GroupLocations.CountryID = Countries.CountryID
WHERE        (GroupLocations.GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#sitenum#">)
</cfquery>
<cfoutput>{cc{#getloccnty.CountryName#}cc}</cfoutput>
<cfoutput>{qq{}qq}</cfoutput>
</cfif>
{bb{#getgroupinfo.bs#}bb}
</cfoutput>


{dd{
<cfif getgroupinfo.recordcount eq 1><!--- this.style.border='1px solid 5f2468'; --->
<cfoutput><select name="site" size="1" class="selectgen" id="lrgselect"><option value="#getgroupinfo.GroupLocID#" selected>#getgroupinfo.SiteName#</option></select></cfoutput>
<cfelse><select name="site" size="1" class="selectgen" id="lrgselect" onchange="popcountryinfo(this.value);"><option value="">-- Select One --</option>
<cfoutput query="getgroupinfo"><option value="#GroupLocID#" <cfif trim(currloc) neq '' and trim(currloc) gt 0><cfif currloc eq grouplocid>selected</cfif></cfif>>#SiteName#</option></cfoutput></select>
</cfif>}dd}

<cfquery name="getsubforgroup" datasource="#request.dsn#">
SELECT        ContractorID, ContractorName
FROM            Contractors
WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnum#">) and status = 1 and cotype = 'sub'
order by ContractorName
</cfquery><!---  onchange="this.style.border='1px solid 5f2468';" --->
{gg{<cfif getsubforgroup.recordcount gt 0><select name="contractordetail" size="1" class="selectgen" id="lrgselect">
										<option value="">-- Select One --</option>
										<cfoutput query="getsubforgroup"><option value="#ContractorID#">#ContractorName#</option></cfoutput>
										</select></cfif>}gg}
</cfif>

<cfif sitenum gt 0>
<cfquery name="getloccnty" datasource="#request.dsn#">
SELECT        Countries.CountryName
FROM            GroupLocations INNER JOIN
                         Countries ON GroupLocations.CountryID = Countries.CountryID
WHERE        (GroupLocations.GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#sitenum#">)
</cfquery>
<cfoutput>{cc{#getloccnty.CountryName#}cc}</cfoutput>


<cfquery name="getcli" datasource="#request.dsn#">
SELECT        GroupLocations.ClientID, Clients.ClientName
FROM            GroupLocations INNER JOIN
                         Clients ON GroupLocations.ClientID = Clients.ClientID
WHERE       (GroupLocations.GroupLocID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#sitenum#">)
</cfquery>
<cfoutput><cfif getcli.recordcount gt 0>{qq{#getcli.ClientName#}qq}<cfelse>{qq{}qq}</cfif></cfoutput>
</cfif>