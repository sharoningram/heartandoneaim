<cfset filedir = trim(replacenocase(getcurrenttemplatepath(), "act_createbaseexcel.cfm", "pdfgen"))>
<cfif not directoryexists("#filedir#")>
	<cfdirectory action="CREATE" directory="#filedir#">
</cfif>

<cfset thefilename = "IncidentExport#datetimeformat(now(),'HHnnssl')#.xlsx">
<cfset thefile = "#filedir#\#thefilename#">
<cfset incSheet = spreadsheetnew("Incident Details","true")>

<cfscript> 
	SpreadsheetSetCellValue(incSheet,"Incident Report Number",1,1);
	SpreadsheetSetCellValue(incSheet,"Originator",1,2);
	SpreadsheetSetCellValue(incSheet,"#request.bulabellong#",1,3);
	SpreadsheetSetCellValue(incSheet,"Date Created",1,4);
	SpreadsheetSetCellValue(incSheet,"#request.oulabellong#",1,5);
	SpreadsheetSetCellValue(incSheet,"Project/Office",1,6);
	SpreadsheetSetCellValue(incSheet,"Business Stream",1,7);
	SpreadsheetSetCellValue(incSheet,"Site/Office Name",1,8);
	SpreadsheetSetCellValue(incSheet,"Country",1,9);
	SpreadsheetSetCellValue(incSheet,"Work Related",1,10);
	SpreadsheetSetCellValue(incSheet,"Client",1,11);
	SpreadsheetSetCellValue(incSheet,"Assigned To",1,12);
	SpreadsheetSetCellValue(incSheet,"Contractor Details",1,13);
	SpreadsheetSetCellValue(incSheet,"Other Incident Assigned To",1,14);
	SpreadsheetSetCellValue(incSheet,"Occur at this Location",1,15);
	SpreadsheetSetCellValue(incSheet,"Where Occur",1,16);
	SpreadsheetSetCellValue(incSheet,"Specific Location",1,17);
	SpreadsheetSetCellValue(incSheet,"Primary Incident Type",1,18);
	SpreadsheetSetCellValue(incSheet,"Near Miss",1,19);
	SpreadsheetSetCellValue(incSheet,"Is Secondary Type",1,20);
	SpreadsheetSetCellValue(incSheet,"Secondary Incident Type",1,21);
	SpreadsheetSetCellValue(incSheet,"Occupational Illness Disease Definition",1,22);
	SpreadsheetSetCellValue(incSheet,"Incident Date",1,23);
	SpreadsheetSetCellValue(incSheet,"Time Incident Occurred",1,24);
	SpreadsheetSetCellValue(incSheet,"Day of Week",1,25);
	SpreadsheetSetCellValue(incSheet,"Primary Weather Condition",1,26);
	SpreadsheetSetCellValue(incSheet,"Short Description",1,27);
	SpreadsheetSetCellValue(incSheet,"Reported by AmecFW Personnel",1,28);
	SpreadsheetSetCellValue(incSheet,"Incident Reported By",1,29);
	SpreadsheetSetCellValue(incSheet,"Description of Incident",1,30);
	SpreadsheetSetCellValue(incSheet,"Potential Rating",1,31);
	SpreadsheetSetCellValue(incSheet,"Investigation Level",1,32);
	SpreadsheetSetCellValue(incSheet,"HSSE Advisor",1,33);
	SpreadsheetSetCellValue(incSheet,"HSSE Manager",1,34);
	SpreadsheetSetCellValue(incSheet,"Immediate Action Taken",1,35);
	SpreadsheetSetCellValue(incSheet,"Reported to Statutory Body",1,36);
	SpreadsheetSetCellValue(incSheet,"Statutory Body Reported To",1,37);
	SpreadsheetSetCellValue(incSheet,"Date Reported to Statutory Body",1,38);
	SpreadsheetSetCellValue(incSheet,"Enforcement Notice Issued",1,39);
	SpreadsheetSetCellValue(incSheet,"Type of Enforcement Notice",1,40);
	SpreadsheetSetCellValue(incSheet,"Dropped Object",1,41);
	SpreadsheetSetCellValue(incSheet,"Source of hazard",1,42);
	SpreadsheetSetCellValue(incSheet,"Other source of hazard",1,43);
	SpreadsheetSetCellValue(incSheet,"Dropped Object Height",1,44);
	SpreadsheetSetCellValue(incSheet,"Weight of Object",1,45);
	SpreadsheetSetCellValue(incSheet,"Dropped object energy (KJ)",1,46);
	SpreadsheetSetCellValue(incSheet,"Dropped object category",1,47);
	SpreadsheetSetCellValue(incSheet,"Reason for Late Recording",1,48);
</cfscript>

<cfset injuryOISheet = spreadsheetnew("Injury - Occupation Illness","true")>
<cfscript> 
	SpreadsheetSetCellValue(injuryOISheet,"Incident Report Number",1,1);
	SpreadsheetSetCellValue(injuryOISheet,"OSHA classification",1,2);
	SpreadsheetSetCellValue(injuryOISheet,"Serious injury",1,3);
	SpreadsheetSetCellValue(injuryOISheet,"IP Name",1,4);
	SpreadsheetSetCellValue(injuryOISheet,"IP Occupation",1,5);
	SpreadsheetSetCellValue(injuryOISheet,"Age Profile",1,6);
	SpreadsheetSetCellValue(injuryOISheet,"Gender",1,7);
	SpreadsheetSetCellValue(injuryOISheet,"Days Since Last Day Off",1,8);
	SpreadsheetSetCellValue(injuryOISheet,"Working shifts",1,9);
	SpreadsheetSetCellValue(injuryOISheet,"Shift",1,10);
	SpreadsheetSetCellValue(injuryOISheet,"Days into shift",1,11);
	SpreadsheetSetCellValue(injuryOISheet,"Total days in shift",1,12);
	SpreadsheetSetCellValue(injuryOISheet,"Injury Type",1,13);
	SpreadsheetSetCellValue(injuryOISheet,"Other Injury Type",1,14);
	SpreadsheetSetCellValue(injuryOISheet,"Body Part Affected",1,15);
	SpreadsheetSetCellValue(injuryOISheet,"Cause of Injury Incident",1,16);
	SpreadsheetSetCellValue(injuryOISheet,"Other Cause of Injury Incident",1,17);
	SpreadsheetSetCellValue(injuryOISheet,"Height of fall",1,18);
	SpreadsheetSetCellValue(injuryOISheet,"Illness Type",1,19);
	SpreadsheetSetCellValue(injuryOISheet,"Other Illness Type",1,20);
	SpreadsheetSetCellValue(injuryOISheet,"Nature of Illness",1,21);
	SpreadsheetSetCellValue(injuryOISheet,"Other Nature of Illness",1,22);
	SpreadsheetSetCellValue(injuryOISheet,"First Date of Absence",1,23);
	SpreadsheetSetCellValue(injuryOISheet,"Date Returned to Work",1,24);
	SpreadsheetSetCellValue(injuryOISheet,"Returned with Restricted Duties",1,25);
	SpreadsheetSetCellValue(injuryOISheet,"Date returned comments",1,26);
	SpreadsheetSetCellValue(injuryOISheet,"First Date Restricted Modified Duties",1,27);
	SpreadsheetSetCellValue(injuryOISheet,"Date Returned to Full Duty",1,28);
	SpreadsheetSetCellValue(injuryOISheet,"Medical Restrictions",1,29);
</cfscript>

<cfset envSheet = spreadsheetnew("Environmental","true")>
<cfscript> 
	SpreadsheetSetCellValue(envSheet,"Incident Report Number",1,1);
	SpreadsheetSetCellValue(envSheet,"Environmental Incident Category",1,2);
	SpreadsheetSetCellValue(envSheet,"In Breach",1,3);
	SpreadsheetSetCellValue(envSheet,"Details of environmental permit",1,4);
	SpreadsheetSetCellValue(envSheet,"Event - Substance - Quantity - Source - Duration - Enviro",1,5);
	SpreadsheetSetCellValue(envSheet,"Details of non-conformance",1,6);
	SpreadsheetSetCellValue(envSheet,"Was waste disposed of or managed incorrectly",1,7);
	
</cfscript>


<cfset adSheet = spreadsheetnew("Asset Damage","true")>
<cfscript> 
	SpreadsheetSetCellValue(adSheet,"Incident Report Number",1,1);
	SpreadsheetSetCellValue(adSheet,"Source of Damage",1,2);
	SpreadsheetSetCellValue(adSheet,"Nature of Damage",1,3);
	SpreadsheetSetCellValue(adSheet,"Other",1,4);
	SpreadsheetSetCellValue(adSheet,"Property Details",1,5);
	SpreadsheetSetCellValue(adSheet,"Estimated Cost of Damage",1,6);
	SpreadsheetSetCellValue(adSheet,"Currency",1,7);
	
</cfscript>


<cfset secSheet = spreadsheetnew("Security","true")>
<cfscript> 
	SpreadsheetSetCellValue(secSheet,"Incident Report Number",1,1);
	SpreadsheetSetCellValue(secSheet,"Security Incident Category",1,2);
	SpreadsheetSetCellValue(secSheet,"Source of Security Threat",1,3);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Person",1,4);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Person - Other",1,5);
	SpreadsheetSetCellValue(secSheet,"Travelling abroad",1,6);
	SpreadsheetSetCellValue(secSheet,"Which Country",1,7);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Business Asset",1,8);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Business Asset - Other",1,9);
	SpreadsheetSetCellValue(secSheet,"Information Category",1,10);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Information Security",1,11);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Information Security - Other",1,12);
	SpreadsheetSetCellValue(secSheet,"Nature of Incident: Security Practice Breach",1,13);
	SpreadsheetSetCellValue(secSheet,"Other Details of Breach",1,14);
	SpreadsheetSetCellValue(secSheet,"Weapon Involved",1,15);
	SpreadsheetSetCellValue(secSheet,"Type of Weapon",1,16);
	SpreadsheetSetCellValue(secSheet,"Immediate financial cost to company",1,17);
	SpreadsheetSetCellValue(secSheet,"Estimated Cost of Loss",1,18);
	SpreadsheetSetCellValue(secSheet,"Currency",1,19);
	
</cfscript>


<cfquery name="getirnlist" datasource="#request.dsn#">
SELECT       oneAIMincidents.IRN
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number
WHERE        (Groups.OUid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#url.ou#">)
</cfquery>
<cfset irnlist = valuelist(getirnlist.irn)>