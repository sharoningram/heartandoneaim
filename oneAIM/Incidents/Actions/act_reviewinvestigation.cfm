<cfparam name="irn" default="0">
<cfparam name="submittype" default="">

<cfswitch expression="#submittype#">
	<!--- <cfcase value="reviewinv">
		<cfif trim(irn) neq 0>
			<cfquery name="getincinfo" datasource="#request.dsn#">
			SELECT        oneAIMincidents.IRN, oneAIMincidents.createdBy, oneAIMincidents.createdbyEmail, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.OccurAtLocation, oneAIMincidents.WhereOccur, oneAIMincidents.SpecificLocation, oneAIMincidents.HSSEAdvisor, oneAIMincidents.HSSEManager, 
                         oneAIMincidents.ActionTaken, oneAIMincidents.wasReported, oneAIMincidents.StatutoryBody, oneAIMincidents.DateStatReported, 
                         oneAIMincidents.EnforcementNotice, oneAIMincidents.EnforcementNoticeType, oneAIMincidents.Status, oneAIMincidents.withdrawnstatus, 
                         oneAIMincidents.DeleteStatus, oneAIMincidents.ReasonLate, Groups.Group_Name,  Groups.ouid,Groups.Business_Line_ID, GroupLocations.SiteName, Countries.CountryName, NewDials.Name AS buname, 
                         NewDials_1.Name AS ouname, NewDials_2.Name AS bsname
			FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
			WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
			</cfquery>
				<cfset tolistemail = "">
				<cfset tolistnames = "">
			<cfquery name="updateinv" datasource="#request.dsn#">
				update IncidentInvestigation
				set status = 'Sent for Review'
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			<cfif getincinfo.InvestigationLevel eq 1>
				<cfquery name="getreviewer" datasource="#request.dsn#">
				SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.UserRole, UserRoles.AssignedLocs
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs =
                             (SELECT        OUid
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
				</cfquery>
				<cfif getreviewer.recordcount gt 0>
				<cfloop query="getreviewer">
					<cfset tolistemail = listappend(tolistemail,useremail)>
					<cfset tolistnames =  listappend(tolistnames,"#Firstname# #Lastname#")>
				</cfloop>
				</cfif>
			<cfelseif getincinfo.investigation eq 2>
				<cfquery name="getsrreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND (UserRoles.AssignedLocs IN
                             (SELECT        Business_Line_ID
                               FROM            Groups
                               WHERE        (Group_Number =
                                                             (SELECT        GroupNumber
                                                               FROM            oneAIMincidents
                                                               WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)))))
				
				</cfquery>
				<cfif gegetsrreviewertreviewer.recordcount gt 0>
				<cfloop query="getsrreviewer">
					<cfset tolistemail = listappend(tolistemail,Useremail)>
					<cfset tolistnames = listappend(tolistnames,"#Firstname# #lastname#")>
				</cfloop>
				</cfif>
			</cfif>
			
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation sent for review by #request.fname# #request.lname# to #tolistnames#">)
				</cfquery>
				
				
				<cfmail to="#erroremail#" subject="E7 inv for  approval" from="#erroremail#" type="html">
					#tolistemail#<br>
					more info
				</cfmail> 
		</cfif>
		<cfoutput>
			<form action="#self#?fuseaction=#attributes.xfa.investigation#" name="retfrm" method="post">
				<input type="hidden" name="irn" value="#irn#">
			</form>
			<script type="text/javascript">
				document.retfrm.submit();
			</script>
		</cfoutput>
	</cfcase> --->
	<cfcase value="srinvmoreinfo">
		<cfparam name="revcomments" default="">
			<cfparam name="irn" default="0">
			<cfif irn gt 0>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Investigation Review','inv')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update IncidentInvestigation
					set status = 'Senior More Info Requested'
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                        oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                        oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                        oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                        IncidentAssignedTo.IncAssignedTo, oneAIMincidents.reviewerEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
                        NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss
				FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
			WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfquery name="getinitiator" datasource="#request.dsn#">
				SELECT       Users.Firstname + ' ' + Users.Lastname as createdBy, Users.Useremail as createdbyEmail
				FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
				WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'reviewer') AND UserRoles.AssignedLocs in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#" list="Yes">)
				and Users.Useremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.reviewerEmail#">
				</cfquery>
				<!--- <cfquery name="getinitiator" datasource="#request.dsn#">
					SELECT        createdbyEmail, createdBy
					FROM            oneAIMincidents
					WHERE irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery> --->
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Sent for More Information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getinitiator.createdBy#">,'Investigation sent for more information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation sent for more information by #request.fname# #request.lname# to #getinitiator.createdBy#">)
				</cfquery>
				
				
				
				
				<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
				<cfif trim(getinitiator.createdbyEmail) neq ''>
				<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Provide further information">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Provide further information">
												</cfif>
				<cfmail to="#getemaildata.reviewerEmail#" from="#request.userlogin#" type="html" subject="#emsubj#">
						
						<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							Further information has been requested for the following incident which is at the Investigation Review stage.<br>Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
								<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Reviewer Comments:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#revcomments#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
										<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
									</tr>
									<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
									</tr>
									
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
									</tr>
								</table>
								<br>
								<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
						</cfif>
				
				
			</cfif>
		<script type="text/javascript">
			 window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
				
				
				
	</cfcase>
	<cfcase value="moreinfo">
		<cfparam name="revcomments" default="">
			<cfparam name="irn" default="0">
			<cfif irn gt 0>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Investigation Review','inv')
					</cfquery>
				</cfif>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update IncidentInvestigation
					set status = 'More Info Requested'
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="getinitiator" datasource="#request.dsn#">
					SELECT        createdbyEmail, createdBy
					FROM            oneAIMincidents
					WHERE irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Sent for More Information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getinitiator.createdBy#">,'Investigation sent for more information',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation sent for more information by #request.fname# #request.lname# to #getinitiator.createdBy#">)
				</cfquery>
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
                        oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
                        oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
                        oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
                        IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
                        NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss
				FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
			WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
				<cfif trim(getemaildata.createdbyEmail) neq ''>
				<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Provide further information">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Provide further information">
												</cfif>
				<cfmail to="#getemaildata.createdbyEmail#" from="#request.userlogin#" type="html" subject="#emsubj#">
						
						<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
							Further information has been requested for the following incident which is at the Investigation Review stage.<br>Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
								<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Reviewer Comments:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#revcomments#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
										<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
									</tr>
									<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
									</tr>
									
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
									</tr>
									<tr>
										<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
										<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
									</tr>
								</table>
								<br>
								<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
						</cfmail>
						</cfif>
				
				
			</cfif>
		<script type="text/javascript">
			 window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="approveforsenior">
		<cfparam name="irp" default="">
		<cfparam name="irn" default="0">
		<cfparam name="revcomments" default="">
		<cfif irn gt 0>
			<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Investigation Review','inv')
					</cfquery>
				</cfif>
				<cftransaction>
				
				<cfquery name="getemaildata" datasource="#request.dsn#">
				SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
				</cfquery>
				<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
				
			<cfset tolistemail = "">
				<cfset tolistnames = "">
			<cfquery name="updateinv" datasource="#request.dsn#">
				update IncidentInvestigation
				set status = 'Sent for Senior Review'
				where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
			</cfquery>
			
				<cfquery name="getsrreviewer" datasource="#request.dsn#">
					SELECT        Users.UserId, Users.Firstname, Users.Lastname, Users.Useremail, UserRoles.AssignedLocs
FROM            Users INNER JOIN
                         UserRoles ON Users.UserId = UserRoles.UserID
WHERE        (Users.Status = 1) AND (UserRoles.UserRole = 'Senior Reviewer') AND UserRoles.AssignedLocs in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.business_line_id#" list="Yes">)
				</cfquery>
				<cfif getsrreviewer.recordcount gt 0>
				<cfloop query="getsrreviewer">
					<cfset tolistemail = listappend(tolistemail,Useremail)>
					<cfset tolistnames = listappend(tolistnames,"#Firstname# #lastname#")>
				</cfloop>
				</cfif>
			
			
				<cfquery name="addaudit" datasource="#request.dsn#">
					insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tolistnames#">,'Investigation Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation sent for review by #request.fname# #request.lname# to #tolistnames#">)
				</cfquery>
				<cfif trim(tolistemail) neq ''>
				<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Review incident investigation details">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Review incident investigation details">
												</cfif>
				<cfmail to="#tolistemail#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													Investigation information has been entered for the following incident which is now awaiting your review. <br>Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
													</cfif>
		
		
				</cftransaction>
		</cfif>
		<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
	<cfcase value="approvinv">
		<cfparam name="irp" default="">
		<cfparam name="irn" default="0">
		<cfparam name="revcomments" default="">
		<cfif irn gt 0 and trim(irp) neq ''>
				<cfif trim(revcomments) neq ''>
					<cfquery name="addcomment" datasource="#request.dsn#">
						insert into oneAIMIncidentComments (IRN, Comment, DateEntered, EnteredBy, commenttype,commenttab)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#revcomments#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,'Investigation Review','inv')
					</cfquery>
				</cfif>
				<cfquery name="getcurstat" datasource="#request.dsn#">
					select status 
					from IncidentInvestigation
					where  irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				
				
				<cfif trim(irp) eq "no">
					<cfquery name="updatefa" datasource="#request.dsn#">
						update IncidentInvestigation
						set status = 'Approved'
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
					
					<cfquery name="addaudit" datasource="#request.dsn#">
						insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,'','Investigation Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation approved by #request.fname# #request.lname#">)
					</cfquery>
					
					<cfquery name="getinccapa" datasource="#request.dsn#">
						select capaid, DateComplete,status, duedate, actiondetail
						from oneAIMCAPA
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#"> and capatype = 'Corrective'
					</cfquery>
					 <cfset subjaddon = "">
					<cfset newincstat = "Closed">
					<cfset needcapastruct = {}>
					<cfloop query="getinccapa">
						<cfif status eq "Open">
							<cfset subjaddon = listappend(subjaddon,"CA")>
							<cfset newincstat = "Review Completed">
							<!--- <cfbreak> --->
							<cfif not structkeyexists(needcapastruct,capaid)>
								<cfset needcapastruct[capaid] = "#duedate#~#actiondetail#">
							</cfif>
						</cfif>
					</cfloop>
					
					<cfquery name="getinctype" datasource="#request.dsn#">
						select PrimaryType,SecondaryType
						from oneAIMincidents
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
				<Cfif listfind("1,5",getinctype.PrimaryType) gt 0 or listfind("1,5",getinctype.SecondaryType) gt 0>
					<cfquery name="getoshacat" datasource="#request.dsn#">
						SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
						FROM            oneAIMInjuryOI
						where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
					</cfquery>
					<cfif getoshacat.OSHAclass eq 2>
						<cfif trim(getoshacat.LTIDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"LTI")>
							<cfset newincstat = "Review Completed">
						</cfif>
					<cfelseif getoshacat.OSHAclass eq 4> 
						<cfif trim(getoshacat.RWCDateReturned) eq ''>
							<cfset subjaddon = listappend(subjaddon,"RWC")>
							<cfset newincstat = "Review Completed">
						</cfif>
					</cfif> 
				</cfif>
					
					
					
					
					
					<cfif newincstat eq "Closed">
						<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set status = 'Closed'
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
						
							<cfquery name="addaudit" datasource="#request.dsn#">
							insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
							values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Closed','System',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,'', 'Incident Closed',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Closed by System upon completion of all CA">)
						</cfquery>
						<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<!--- <cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))> --->
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
													<cfif trim(getemaildata.createdbyEmail) neq ''>
													<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident closed">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident closed">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident is now closed. Any further amendments will require this record to be re-opened.<br>Please go to oneAIM via this link to view the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
	</cfif>
							<cfelse>
							
									<cfquery name="getemaildata" datasource="#request.dsn#">
											SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
							                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
							                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
							                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
							                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
							                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.createdBy, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
												FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
											WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
											</cfquery>
											<cfquery name="addaudit" datasource="#request.dsn#">
											insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate,fromstatus, SentTo, ActionTaken, StatusDesc)
										values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,'Sent for Review',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getemaildata.createdBy#">, 'Incident Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Incident Approved - Pending Closure">)
										</cfquery>
											<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
												<cfset cclist = "">
												<cfquery name="getreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
												</cfquery>
												<cfset cclist = listappend(cclist,getemaildata.reviewerEmail)>
												<!--- <cfset cclist = listappend(cclist,valuelist(getreviewer.Useremail))> --->
												<cfif getemaildata.InvestigationLevel eq 2>
												<cfquery name="getsrreviewer" datasource="#request.dsn#">
													SELECT       Users.Useremail
													FROM            Users INNER JOIN
							                         UserRoles ON Users.UserId = UserRoles.UserID
													WHERE     Users.status = 1 and   (UserRoles.UserRole = 'senior reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.Business_Line_ID#">)
												</cfquery>
												<cfset cclist = listappend(cclist,valuelist(getsrreviewer.Useremail))>
												</cfif>
												<cfif trim(getemaildata.createdbyEmail) neq ''>
												<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Incident approved � outstanding items to be actioned">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Incident approved � outstanding items to be actioned">
												</cfif>
												<cfmail to="#getemaildata.createdbyEmail#" cc="#cclist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">This is an automatic notification generated by oneAIM.<br>
													The following incident has been approved and is awaiting the closure of one or more items before the record can be closed. <br>
													Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br>
													<cfif listfindnocase(subjaddon,"RWC")>
														Restricted Work Case Return Date has not been entered<br><br>
													<cfelseif listfindnocase(subjaddon,"LTI")>
														Lost Time Incident Return Date has not been entered<br><br>
													</cfif></span>
													<cfif listlen(structkeylist(needcapastruct)) gt 0>
														<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;" colspan="2"><strong>Oustanding Corrective Actions</strong></td>
															</tr>
															<cfloop list="#structkeylist(needcapastruct)#" index="id">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><cfif needcapastruct[id] contains "~">#dateformat(listgetat(needcapastruct[id],1,"~"),sysdateformat)#</cfif></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><cfif needcapastruct[id] contains "~">#listgetat(needcapastruct[id],2,"~")#</cfif></td>
															</tr>
															</cfloop>
															</table><br>
															</cfif>
															<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
</cfif>
						</cfif>
					
					
					
					
					
					
					
					
					
				<cfelseif trim(irp) eq "yes">
					<cfquery name="getemaildata" datasource="#request.dsn#">
						SELECT        oneAIMincidents.Status, oneAIMincidents.withdrawnto, oneAIMincidents.TrackingNum, oneAIMincidents.GroupNumber, oneAIMincidents.withdrawndate, 
		                         oneAIMincidents.dateClosed, oneAimFirstAlerts.Status AS fastatus, IncidentInvestigation.Status AS invstatus, oneAIMincidents.InvestigationLevel, 
		                         oneAIMincidents.isWorkRelated, oneAIMIncidentIRP.Status AS irpstatus, IncidentTypes.IncType, oneAIMincidents.PotentialRating, OSHACategories.Category, 
		                         oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, oneAIMincidents.Description, Groups.Group_Name, GroupLocations.SiteName, 
		                         IncidentAssignedTo.IncAssignedTo, oneAIMincidents.createdbyEmail, NewDials.Name AS buname, NewDials_1.Name AS ouname, 
		                         NewDials_2.Name AS bsname,Groups.Business_Line_ID, Groups.OUid, oneAIMincidents.createdBy, oneAIMincidents.isNearMiss,oneAIMincidents.reviewerEmail,oneAIMincidents.HSSEManager
							FROM            NewDials INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON NewDials.ID = Groups.Business_Line_ID INNER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         GroupLocations ON oneAIMincidents.LocationID = GroupLocations.GroupLocID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID LEFT OUTER JOIN
                         OSHACategories RIGHT OUTER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN LEFT OUTER JOIN
                         oneAIMIncidentIRP ON oneAIMincidents.IRN = oneAIMIncidentIRP.IRN LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN
						WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
					</cfquery>
					<cfset potcolor = "ffffff">
											<cfset potclass = "bodytext">
											<cfset pottext = "000000">
											<cfswitch expression="#getemaildata.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottext = "000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset potclass = "bodytextwhite">
													<cfset pottext = "ffffff">
												</cfcase>
											</cfswitch>
						<cfset tolist = "">
						<cfset tonames = "">
						<cfquery name="getreviewer" datasource="#request.dsn#">
							SELECT       Users.Useremail, Users.Firstname, Users.Lastname
							FROM            Users INNER JOIN
	                         UserRoles ON Users.UserId = UserRoles.UserID
							WHERE     Users.status = 1 and   (UserRoles.UserRole = 'reviewer') AND (UserRoles.AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getemaildata.ouid#">)
						</cfquery>
						<!--- <cfset tolist = listappend(tolist,valuelist(getreviewer.Useremail))> --->
						<cfset tolist = listappend(tolist,getemaildata.reviewerEmail)>
						<cfloop query="getreviewer">
							<cfset tonames = listappend(tonames,"#getreviewer.Firstname# #getreviewer.Lastname#")>
						</cfloop>
						<cfif trim(tolist) neq ''>
						<cfif  getemaildata.isNearMiss eq "yes">
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Near Miss - Prepare IRP presentation and Learning Sheet">
												<cfelse>
													<cfset emsubj = "oneAIM Incident - #getemaildata.TrackingNum# - #getemaildata.buName# - #getemaildata.IncType# - Prepare IRP presentation and Learning Sheet">
												</cfif>
						<cfmail to="#tolist#" from="#request.userlogin#" type="html" subject="#emsubj#">
												
												<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">
		 											This is an automatic notification generated by oneAIM.<br>
													The following incident has been selected for IRP. Please prepare the IRP PowerPoint presentation which should be uploaded as a file attachment to this record. The IRP Learning Sheet tab is available for completion.<br>Please go to oneAIM via this link to progress the record � <a href="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/index.cfm?fuseaction=incidents.incidentreport&irn=#irn#">Record ###getemaildata.TrackingNum#</a><br><Br></span>
													<table class="purplebg"  bgcolor="5f2468" cellpadding="4" cellspacing="0" border="1" bordercolor="5f2468">
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Primary Incident type:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncType#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>OSHA Classification:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Category#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Potential Rating:</strong></td>
																<td bgcolor="#potcolor#" class="#potclass#" style="font-family:Segoe UI;font-size:10pt;color:###pottext#;">#getemaildata.PotentialRating#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident date and time:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#dateformat(getemaildata.incidentDate,sysdateformat)# #getemaildata.incidentTime#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Investigation Level:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Level #getemaildata.InvestigationLevel#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Work related?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isworkrelated#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident Description:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Description#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Near Miss?</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.isNearMiss#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>#request.oulabellong#:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.ouname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Business Stream:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.bsname#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Project/Office:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.Group_Name#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Site/Office name:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.SiteName#</td>
															</tr>
															<tr>
																<td bgcolor="f7f1f9"  class="formlable" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;"><strong>Incident assigned to:</strong></td>
																<td bgcolor="ffffff" class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">#getemaildata.IncAssignedTo#</td>
															</tr>
														</table>
														<br>
														<span class="bodytext" style="font-family:Segoe UI;font-size:10pt;color:5f5f5f;">Please do not reply to this email address. If you have any questions concerning this or have received this message in error, please contact your #request.bulabellong# oneAIM Administrator.</span>
												</cfmail>
						
							</cfif>
						
				<cfquery name="getinitiator" datasource="#request.dsn#">
					SELECT        createdbyEmail, createdBy
					FROM            oneAIMincidents
					WHERE irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
				<cfquery name="updatefa" datasource="#request.dsn#">
					update IncidentInvestigation
					set status = 'IRP Pending'
					where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
				</cfquery>
					<cfquery name="updateincident" datasource="#request.dsn#">
							update oneAIMincidents
							set needIRP = 1
							where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
						</cfquery>
					<!--- SentTo	may sit with getinitiator.createdBy --->
					<cfquery name="addaudit" datasource="#request.dsn#">
						insert into IncidentAudits (IRN, Status, CompletedBy, CompletedDate, fromstatus, SentTo, ActionTaken, StatusDesc)
						values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">,'Investigation Approved',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#request.fname# #request.lname#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getcurstat.status#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getinitiator.createdBy#">,'Investigation Approved and IRP Started',<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="Investigation approved with IRP by #request.fname# #request.lname#">)
					</cfquery>
				
				</cfif>
	
		</cfif>
			<script type="text/javascript">
			window.opener.location.assign("index.cfm?fuseaction=main.main");
			window.close();
		</script>
	</cfcase>
</cfswitch>