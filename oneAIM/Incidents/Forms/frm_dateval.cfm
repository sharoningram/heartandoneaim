<cfparam name="action" default="">
<script>

  $(function() {

    $( "#datepicker1" ).datepicker({dateFormat: 'dd-MM-yy',
				showOn: "button",
                buttonImage: "images/calendar.jpg",
				buttonText: "",
				changeMonth: true,
				changeYear: true,
				monthNamesShort: ["January","February","March","April","May","June",
			"July","August","September","October","November","December"],
                buttonImageOnly: true
 });

  });
  
  $(function() {

    $( "#datepicker2" ).datepicker({dateFormat: 'dd-MM-yy',
				showOn: "button",
                buttonImage: "images/calendar.jpg",
				buttonText: "",
				changeMonth: true,
				changeYear: true,
				monthNamesShort: ["January","February","March","April","May","June",
			"July","August","September","October","November","December"],
                buttonImageOnly: true
 });

  });
  </script>
<cfswitch expression="#action#">
<cfcase value= "lti">
<script type="text/javascript">
function submitsfrm(){
// alert(document.userlookupfrm.searchitem.value);
var oktosubmit = "yes"

if(document.ltidatefrm.datereturned.value==""){
	var oktosubmit = "no";
	 document.ltidatefrm.datereturned.style.border = "2px solid F00000";
	
}
else{
//document.ltidatefrm.datereturned.style.border = "1px solid 5f2468";
if(document.ltidatefrm.firstofabsence.value!=""){
				
	
		//if(document.ltidatefrm.datereturned.value!=""){
		// alert('both');
		
		var tday = document.ltidatefrm.firstofabsence.value.substring(0,2);
		var startpos = document.ltidatefrm.firstofabsence.value.length - 4;
		var tyear = document.ltidatefrm.firstofabsence.value.substring(startpos,document.ltidatefrm.firstofabsence.length);
		var tmonth = document.ltidatefrm.firstofabsence.value.substring(3,document.ltidatefrm.firstofabsence.value.length-5);
		var hours = 00;
	// var mins = document.incidentfrm.minutes.value;
		var myDate = new Date(tmonth + '' + tday + ',' + tyear + ' ' + hours +  ':00'); 
		// alert(myDate);
		
		var tday2 = document.ltidatefrm.datereturned.value.substring(0,2);
		var startpos2 = document.ltidatefrm.datereturned.value.length - 4;
		var tyear2 = document.ltidatefrm.datereturned.value.substring(startpos,document.ltidatefrm.datereturned.length);
		var tmonth2 = document.ltidatefrm.datereturned.value.substring(3,document.ltidatefrm.datereturned.value.length-5);
		var hours2 = 00;
	// var mins = document.incidentfrm.minutes.value;
		var myDate2 = new Date(tmonth2 + '' + tday2 + ',' + tyear2 + ' ' + hours2 +  ':00'); 
		
		// alert(myDate2);
		if(new Date(myDate) > new Date(myDate2))
		{
		alert('Date returned can not be before first date of absence');
		var oktosubmit = "no";
		document.ltidatefrm.firstofabsence.style.border = "2px solid F00000";
		document.ltidatefrm.datereturned.style.border = "2px solid F00000";
		}
		
	//} 
}


}


if (oktosubmit=="no"){
	document.getElementById("reqflds").style.display='';
	return false;
}  
// alert(oktosubmit);
}
</script>
	<cfform action="#self#?fuseaction=#attributes.xfa.adddateval#" method="post" name="ltidatefrm" enctype="multipart/form-data" onsubmit="return submitsfrm();">
	<input type="hidden" name="submittype" value="lti">
	<input type="hidden" name="action" value="lti">
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr id="reqflds">
							<td colspan="4" class="redText" align="center">Please fill in all required fields</td>
						</tr>
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="firstofabsence" value="#dateformat(getdatevals.LTIFirstDate,sysdateformat)#">
					<tr>
						<td class="bodyTextGrey" colspan="2">Add LTI Return Date</td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="60%"><strong>First Date of Absence:</strong></td>
						<td class="bodyTextGrey" >#dateformat(getdatevals.LTIFirstDate,sysdateformat)#</td>
					</tr>
					<cfif datediff("d",getdatevals.LTIFirstDate,now()) lt 180>
						<cfset datedsp = "">
					<cfelse>
						<cfset datedsp = dateformat(dateadd("d",getdatevals.LTIFirstDate,180),sysdateformat)>
					</cfif>
					<tr>
						<td class="formlable" align="left" width="60%"><strong>Date Returned to Work1:*</strong></td>
						<td class="bodyTextGrey" ><cfif datediff("d",getdatevals.LTIFirstDate,now()) lt 180><input type="text" class="selectgen" size="15" readonly name="datereturned" value="#datedsp#" id="datepicker1"><cfelse><input type="text" class="selectgen" size="15" readonly name="datereturned" value="#datedsp#"></cfif><!--- <cfif datediff("d",getdatevals.LTIFirstDate,now()) lt 180>&nbsp;<a href="javascript:void(0);"   onClick="longcalendar_open(document.ltidatefrm.datereturned,0);return false;"><img src="images/calendar.jpg" style="vertical-align:-4;padding-left:0px;" border="0"></a></cfif> ---></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="30"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
	<script type="text/javascript">
	document.getElementById("reqflds").style.display='none';
</script>
</cfcase>
<cfcase value="rwc">
<script type="text/javascript">
function submitsfrm(){
// alert(document.userlookupfrm.searchitem.value);
var oktosubmit = "yes"

if(document.ltidatefrm.datereturned.value==""){
	var oktosubmit = "no";
	 document.ltidatefrm.datereturned.style.border = "2px solid F00000";
	
}
else{

if(document.ltidatefrm.firstofabsence.value!=""){
				
	
		//if(document.ltidatefrm.datereturned.value!=""){
		// alert('both');
		
		var tday = document.ltidatefrm.firstofabsence.value.substring(0,2);
		var startpos = document.ltidatefrm.firstofabsence.value.length - 4;
		var tyear = document.ltidatefrm.firstofabsence.value.substring(startpos,document.ltidatefrm.firstofabsence.length);
		var tmonth = document.ltidatefrm.firstofabsence.value.substring(3,document.ltidatefrm.firstofabsence.value.length-5);
		var hours = 00;
	// var mins = document.incidentfrm.minutes.value;
		var myDate = new Date(tmonth + '' + tday + ',' + tyear + ' ' + hours +  ':00'); 
		// alert(myDate);
		
		var tday2 = document.ltidatefrm.datereturned.value.substring(0,2);
		var startpos2 = document.ltidatefrm.datereturned.value.length - 4;
		var tyear2 = document.ltidatefrm.datereturned.value.substring(startpos,document.ltidatefrm.datereturned.length);
		var tmonth2 = document.ltidatefrm.datereturned.value.substring(3,document.ltidatefrm.datereturned.value.length-5);
		var hours2 = 00;
	// var mins = document.incidentfrm.minutes.value;
		var myDate2 = new Date(tmonth2 + '' + tday2 + ',' + tyear2 + ' ' + hours2 +  ':00'); 
		
		// alert(myDate2);
		if(new Date(myDate) > new Date(myDate2))
		{
		alert('Date returned can not be before first date of absence');
		var oktosubmit = "no";
		document.ltidatefrm.firstofabsence.style.border = "2px solid F00000";
		document.ltidatefrm.datereturned.style.border = "2px solid F00000";
		}
		
	//} 
}


}


if (oktosubmit=="no"){
	document.getElementById("reqflds").style.display='';
	return false;
}  
// alert(oktosubmit);
}
</script>

	<cfform action="#self#?fuseaction=#attributes.xfa.adddateval#" method="post" name="ltidatefrm" enctype="multipart/form-data" onsubmit="return submitsfrm();">
	<input type="hidden" name="submittype" value="rwc">
	<input type="hidden" name="action" value="rwc">
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr id="reqflds">
							<td colspan="4" class="redText" align="center">Please fill in all required fields</td>
						</tr>
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="firstofabsence" value="#dateformat(getdatevals.RWCFirstDate,sysdateformat)#">
					<tr>
						<td class="bodyTextGrey" colspan="2">Add RWC Return Date</td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="60%"><strong>First Date Restricted/Modified Duties:</strong></td>
						<td class="bodyTextGrey" >#dateformat(getdatevals.RWCFirstDate,sysdateformat)#</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="60%"><strong>Date Returned to Full Duty:*</strong></td>
						<td class="bodyTextGrey" ><input type="text" class="selectgen" size="15" readonly name="datereturned" value="" id="datepicker2"><!--- &nbsp;<a href="javascript:void(0);"   onClick="longcalendar_open(document.ltidatefrm.datereturned,0);return false;"><img src="images/calendar.jpg" style="vertical-align:-4;padding-left:0px;" border="0"></a> ---></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="25"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
	<script type="text/javascript">
	document.getElementById("reqflds").style.display='none';
</script>
</cfcase>
</cfswitch>