<cfform action="#self#?fuseaction=#attributes.xfa.incidentmanament#" method="post" name="incidnetform">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="60%" id="mytab">
<tr>
		<td colspan="2">
<table cellpadding="4" cellspacing="1" bgcolor="#000000" width="75%" >
					
					<tr style="height:37px;">
						<td rowspan="8" width="1%" align="center" bgcolor="000000"><img src="images/consequences.png" border="0"></td>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf"><strong class="bodytext">When using this matrix consider the potential outcome of the incident not the actual outcome<br>*IP = intellectual property</strong></td>
						<td width="25%" align="center"  colspan="5" bgcolor="ffffff"><strong class="bodytext">No. of People</strong></td>
						
					</tr>
					<tr style="height:37px;" bgcolor="ffffff">
						
						<td  align="center"  ></td>
						<td   align="center"  ><strong class="bodyTextsm">Injury/Health</strong></td>
						<td  align="center"  ><strong class="bodyTextsm">Environmental</strong></td>
						<td  align="center"  ><strong class="bodyTextsm">Damage</strong></td>
						<td   align="center"  ><strong class="bodyTextsm">Security</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm">0</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">1</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm">1-2</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">2-10</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">10+</strong></td>
					</tr>
					<tr bgcolor="#ffffff" style="height:37px;">
						<td align="center" bgcolor="#DDEEFF"><strong class="bodytext">1</strong></td>
						<td align="center" class="bodyTextsm">First aid/health effect</td>
						<td align="center" class="bodyTextsm">Minimal reversible environmental impact</td>
						<td align="center"  class="bodyTextsm">Minor loss/damage/business impact (<10K)</td>
						<td align="center" class="bodyTextsm">Minor crime, no impact on business operations or reputation</td>
					<td align="center" bgcolor="#00b050"><input type="radio" name="potentialC" value="1"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A1</strong></td>
					<td align="center" bgcolor="#00b050"><input type="radio" name="potentialC" value="2"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B1</strong></td>
					<td  align="center" bgcolor="#00b050"><input type="radio" name="potentialC" value="3"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C1</strong></td>
					<td align="center" bgcolor="#00b050"><input type="radio" name="potentialC" value="4"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D1</strong></td>
					<td align="center"  bgcolor="#ffc000"><input type="radio" name="potentialC" value="12"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">E1</strong></td>
					</tr>
					<tr bgcolor="#ffffff" style="height:37px;">
						<td  align="center" bgcolor="#DDEEFF"><strong class="bodytext">2</strong></td>
						<td align="center" class="bodyTextsm">Medical treatment/ restricted work/ moderate health effect</td>
						<td align="center"  class="bodyTextsm">Minor pollution with short term impact (1 month)</td>
						<td align="center"  class="bodyTextsm">Moderate loss/ damage/ business impact (10-100K)</td>
						<td align="center"  class="bodyTextsm">Theft/vandalism/loss of non-IP* information. No lasting impact on business operations or reputation</td>
					<td  align="center" bgcolor="#00b050"><input type="radio" name="potentialC" value="5"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A2</strong></td>
					<td  align="center" bgcolor="#00b050"><input type="radio" name="potentialC" value="6"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B2</strong></td>
					<td align="center"  bgcolor="#00b050"><input type="radio" name="potentialC" value="7"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C2</strong></td>
					<td  align="center" bgcolor="#ffc000"><input type="radio" name="potentialC" value="13"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">D2</strong></td>
					<td align="center"  bgcolor="#ffc000"><input type="radio" name="potentialC" value="14"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">E2</strong></td>
					</tr>
					<tr bgcolor="#ffffff" style="height:37px;">
						<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">3</strong></td>
						<td align="center"  class="bodyTextsm">Lost time injury/ significant health effect</td>
						<td align="center"  class="bodyTextsm">Moderate pollution with medium term localised impact (1 year)</td>
						<td align="center"  class="bodyTextsm">Significant loss/damage/ business impact reportable event within local legislation (100K-1M)</td>
						<td align="center"  class="bodyTextsm">Crime (threat, intimidation, sabotage) with impact on people or loss of confidential IP containing material</td>
					<td  align="center" bgcolor="#ffc000"><input type="radio" name="potentialC" value="15"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">A3</strong></td>
					<td  align="center" bgcolor="#ffc000"><input type="radio" name="potentialC" value="16"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">B3</strong></td>
					<td align="center"  bgcolor="#ffc000"><input type="radio" name="potentialC" value="17"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">C3</strong></td>
					<td align="center"  bgcolor="#ffc000"><input type="radio" name="potentialC" value="18"><br><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">D3</strong></td>
					<td  align="center" bgcolor="#ff0000"><input type="radio" name="potentialC" value="19"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E3</strong></td>
					</tr>
					<tr bgcolor="#ffffff" style="height:37px;">
						<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">4</strong></td>
						<td align="center"  class="bodyTextsm">Serious injury/ severe health effect/ long term disability</td>
						<td align="center"  class="bodyTextsm">Severe pollution with long term localised impact (+1 year)</td>
						<td align="center"  class="bodyTextsm">Severe loss/damage/ business impact reportable event within local legislation (1-10M)</td>
						<td align="center"  class="bodyTextsm">Serious and deliberate criminal attack against people, disruptive natural event or loss of sensitive IP material</td>
					<td align="center"  bgcolor="#ff0000"><input type="radio" name="potentialC" value="20"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A4</strong></td>
					<td  align="center" bgcolor="#ff0000"><input type="radio" name="potentialC" value="21"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B4</strong></td>
					<td align="center"  bgcolor="#ff0000"><input type="radio" name="potentialC" value="22"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C4</strong></td>
					<td align="center"  bgcolor="#ff0000"><input type="radio" name="potentialC" value="23"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D4</strong></td>
					<td align="center" bgcolor="#ff0000"><input type="radio" name="potentialC" value="24"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E4</strong></td>
					</tr>
					<tr bgcolor="#ffffff" style="height:37px;">
						<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">5</strong></td>
						<td align="center"  class="bodyTextsm">Fatality</td>
						<td align="center"  class="bodyTextsm">Major pollution with long term environmental change</td>
						<td align="center"  class="bodyTextsm">Major loss/damage/ reportable event within local legislation business impact (10M+)</td>
						<td align="center"  class="bodyTextsm">Sustained, serious attack/loss of confidential IP or natural disaster requiring formal emergency response</td>
					<td  align="center" bgcolor="#ff0000"><input type="radio" name="potentialC" value="25"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A5</strong></td>
					<td  align="center" bgcolor="#ff0000"><input type="radio" name="potentialC" value="26"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B5</strong></td>
					<td align="center"  bgcolor="#ff0000"><input type="radio" name="potentialC" value="27"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C5</strong></td>
					<td  align="center" bgcolor="#ff0000"><input type="radio" name="potentialC" value="28"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D5</strong></td>
					<td align="center"  bgcolor="#ff0000"><input type="radio" name="potentialC" value="29"><br><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E5</strong></td>
					</tr>
					<tr>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf" class="bodyTextsm">High Potential Incidents (Red) = LEVEL 2; Medium Potential Incidents (Amber) = LEVEL 1 or 2 (at HSSE Manager's discretion); Low Potential Incidents (Green) = LEVEL 1</td>
				<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">A</strong></td>
				<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">B</strong></td>
				<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">C</strong></td>
				<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">D</strong></td>
				<td align="center"  bgcolor="#DDEEFF"><strong class="bodytext">E</strong></td>
					</tr>
				</table>	
				</td>
					</tr>
				</table>	
				</cfform>