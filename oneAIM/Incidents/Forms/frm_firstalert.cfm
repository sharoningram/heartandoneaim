<cfparam name="faid" default="0">
<cfparam name="irpmsg" default="0">
<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\forms\frm_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
<cfif directoryexists(filedir)>
			<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">
</cfif>
<!--- <cfif isdefined("getincfileslist.recordcount")>
	<cfif getincfileslist.recordcount eq 0>
if(document.fafrm.uploaddoc1.value==""){
	var oktosubmit = "no";
	 document.fafrm.uploaddoc1.style.border = "2px solid F00000";
	
}
else{
document.fafrm.uploaddoc1.style.border = "1px solid 5f2468";
}
</cfif>
</cfif> --->
<script type="text/javascript">
function submitsfrm(){
// alert(document.userlookupfrm.searchitem.value);
var oktosubmit = "yes"
/*
if(document.fafrm.proposedaction.value==""){
	var oktosubmit = "no";
	 document.fafrm.proposedaction.style.border = "2px solid F00000";
	
}
else{
document.fafrm.proposedaction.style.border = "1px solid 5f2468";
}
*/



if(document.fafrm.description.value==""){
	var oktosubmit = "no";
	 document.fafrm.description.style.border = "2px solid F00000";
	
}
else{
document.fafrm.description.style.border = "1px solid 5f2468";
}

if (oktosubmit=="no"){
	document.getElementById("reqflds").style.display='';
	return false;
}  
else{
<cfif getincidentdetails.investigationlevel eq 2>

var retval =  confirm('A First Alert will now be generated for review by the HSSE Manager. Please ensure that you have attached any relevant photographs.');

if(retval==false){
var oktosubmit = "no";

}
</cfif>
if (oktosubmit=="yes"){
document.fafrm.submit();
}
}
}


function chkprj(sendtype){

if(sendtype=='saveonly'){
document.fafrm.closesave.value="no";
}
else{
document.fafrm.closesave.value="yes";
}  

document.fafrm.sfr.value="no"

document.fafrm.submit();

}
</script>

<cfform action="#self#?fuseaction=#attributes.xfa.famanagement#" method="post" name="fafrm" enctype="multipart/form-data" onsubmit="return submitsfrm();">
<cfoutput>
<input type="hidden" name="closesave" value="no">
<input type="hidden" name="sfr" value="yes">
<input type="hidden" name="irn" value="#irn#">
<input type="hidden" name="faid" value="#faid#">
<cfif faid eq 0>
	<input type="hidden" name="submittype" value="addFA">
<cfelse>
	<input type="hidden" name="submittype" value="updateFA">
</cfif>

</cfoutput>
<cfset potcolor="ffffff">
<cfset txtcolor="000000">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">


	<tr>
		<td align="center" bgcolor="ffffff">
		<cfoutput>
	<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
	<tr id="reqflds">
		<td colspan="4" class="redText" align="center">Please fill in all required fields</td>
	</tr>
	<cfif irpmsg eq 1>
				<tr >
					<td colspan="4" class="redText" align="center">There was a problem deleting the file you selected. Please try again later.</td>
				</tr>
				</cfif>
	<tr>
		<td colspan="4" class="redText">All data entered must be in English</td>
	</tr>
	<cfif irn gt 0>
	<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Classification</strong></td>
		
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Primary Incident Type:</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidenttypeFA.IncType#</td>
		<td class="formlable" align="left" width="25%"><strong>Is This a Near Miss Incident?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.isNearMiss#</td>
	</tr>
	<tr>
		<cfif listfind("1,5",getincidentdetails.primarytype) gt 0>
		<td class="formlable" align="left" width="25%"><strong>OSHA Classification:</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.category#</td>
		</cfif>
		
		<td class="formlable" align="left" width="25%"><strong>Incident Potential Rating</strong></td>
		<td class="bodyTextGrey" width="25%" >
		<cfswitch expression="#getincidentdetails.PotentialRating#">
			<cfcase value="A1,B1,C1,D1,A2,B2,C2">
				<cfset potcolor="00b050">
				<cfset txtcolor="ffffff">
			</cfcase>
			<cfcase value="E1,D2,E2,A3,B3,C3,D3">
				<cfset potcolor="ffc000">
				<cfset txtcolor="000000">
			</cfcase>
			<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
				<cfset potcolor="ff0000">
				<cfset txtcolor="ffffff">
			</cfcase>
		</cfswitch>
		<span style="background-color:#potcolor#;color:#txtcolor#">#getincidentdetails.PotentialRating#</span></td>
	</tr>
	<cfif listfind("1,2",getincidentdetails.primarytype) gt 0>
	<tr>
		<cfif getincidentdetails.primarytype eq 1>
		<td class="formlable" align="left" width="25%"><strong>Is injury classified as a serious incident?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.isserious#</td>
		</cfif>
		<cfif getincidentdetails.primarytype eq 2>
		<td class="formlable" align="left" width="25%"><strong>Details of environmental permit or license breached</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentenv.breachdetail#</td>
		</cfif>
	</tr>
	</cfif>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Has an enforcement notice been issued?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.EnforcementNotice#</td>
		<cfif getincidentdetails.EnforcementNotice eq "Yes">
		<td class="formlable" align="left" width="25%"><strong>Type of enforcement notice</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.EnforcementNoticeType#</td>
		</cfif>
	</tr>
		<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Location</strong></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.buname#</td>
		<td class="formlable" align="left" width="25%"><strong>#request.oulabellong#</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.ouname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Business Stream</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.bsname#</td>
		<td class="formlable" align="left" width="25%"><strong>Country</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.countryname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Project/Office</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.group_name#</td>
		<td class="formlable" align="left" width="25%"><strong>Site/Office name</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.sitename#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Specific location where incident occurred</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.Specificlocation#</td>
		<td class="formlable" align="left" width="25%"><strong></strong></td>
		<td class="bodyTextGrey" width="25%" ></td>
	</tr>
		<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Details</strong></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident Date</strong></td>
		<td class="bodyTextGrey" width="25%" >#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
		<td class="formlable" align="left" width="25%"><strong>Time incident occurred</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.incidenttime#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident assigned to</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidenttypeFA.IncAssignedTo#</td>
		<td colspan="2"></td>
		
		
	</tr>
	<cfif listfind("1,5",getincidentdetails.primarytype) gt 0>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Injury Type:</strong></td>
		<td class="bodyTextGrey" width="25%" ><cfif trim(getintype.InjuryType) eq ''>N/A<cfelse>#replace(valuelist(getintype.InjuryType),",",", ","all")#</cfif></td>
		<td class="formlable" align="left" width="25%"><strong>Body Part Affected:</strong></td>
		<td class="bodyTextGrey" width="25%" ><cfif getfabp.recordcount eq 0>N/A<cfelse>#replace(valuelist(getfabp.bodypart),",",", ","all")#</cfif></td>
	</tr>
	
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Gender</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.ipgender#</td>
		<td class="formlable" align="left" width="25%"><strong>Occupation</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.Occupation#</td>
		</cfif>
	</tr>
	<cfset showdesc = "">
	<cfif trim(getFAdetail.description) eq ''>
		<cfif getincidentdetails.description neq "Please enter a short description of the incident. Do not include any personal details, e.g. name of the individual(s) involved.">
								<cfset showdesc = getincidentdetails.description>
							</cfif>
	<cfelse>
		<cfset showdesc = getFAdetail.description>
	</cfif>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Description of incident*</strong></td><!--- onkeypress="this.style.border='1px solid 5f2468';" --->
		<td class="bodyTextGrey" width="25%" colspan="3"><textarea name="description" cols="80" rows="2" class="selectgen" >#showdesc#</textarea></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Immediate action taken</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.actiontaken#</td>
		<td  align="left" width="25%"><strong></strong></td>
		<td class="bodyTextGrey" width="25%" ></td>
	</tr>
	
	<!--- 	<tr>
		<td class="formlable" align="left" width="25%"><strong>Proposed action to prevent reocurrence*</strong></td><!--- onkeypress="this.style.border='1px solid 5f2468';" --->
		<td class="bodyTextGrey" width="25%" colspan="3"><textarea name="proposedaction" cols="80" rows="2" class="selectgen" >#getFAdetail.PreventAction#</textarea></td>
	</tr> --->
		
	
	<tr>
		<td class="formlable" align="left"><strong>Attach Photograph(s):</strong></td>
		<td class="bodyTextGrey" colspan="3">
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfloop query="getincfileslist">
						<a href="#self#?fuseaction=incidents.famanagement&submittype=deletefafile&filename=#name#&irn=#irn#&faid=#faid#" onclick="return confirm('Are you sure you want to delete this file?');"><img src="images/trash.gif" border="0"></a>&nbsp;<a href="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/FA/#name#" target="_blank">#name#</a><br>
					</cfloop>
				</cfif>
			</cfif>
		<!---  onchange="this.style.border='1px solid 5f2468';" --->
		<input type="file" name="uploaddoc1" size="22" id="selectgen" >&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file2').style.display='';" class="midboxtitletxt">+</a>
					<div id="file2"><br><input type="file" name="uploaddoc2" size="22" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file3').style.display='';" class="midboxtitletxt">+</a></div>
					<div id="file3"><br><input type="file" name="uploaddoc3" size="22" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file4').style.display='';" class="midboxtitletxt">+</a></div>
					<div id="file4"><br><input type="file" name="uploaddoc4" size="22" id="selectgen">&nbsp;<a href="javascript:void(0);" onclick="document.getElementById('file5').style.display='';" class="midboxtitletxt">+</a></div>
					<div id="file5"><br><input type="file" name="uploaddoc5" size="22" id="selectgen"></div>
		</td>
		</tr>
		<tr>
		<td class="bodyTextGrey" align="left" width="25%" colspan="4">&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Save and Close" name="sandc" onclick="return chkprj('saveandclose');">&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Save" name="sandc" onclick="return chkprj('saveonly');"><cfif getincidentdetails.investigationlevel neq 2>&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Send for Review"  onclick="return submitsfrm();"><cfelse><cfif listfindnocase("Started,Initiated,More Info Requested,Sent for Review",getincidentdetails.status) eq 0>&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Send for Review"  onclick="return submitsfrm();"></cfif>
						</cfif><!--- <cfif getincidentdetails.investigationlevel neq 2>&nbsp;&nbsp;<input type="submit" value="Send for Review" class="selectGenBTN"><cfelse><cfif listfindnocase("Started,Initiated,More Info Requested",getincidentdetails.status) eq 0>&nbsp;&nbsp;<input type="submit" value="Send for Review" class="selectGenBTN"></cfif></cfif> ---></td>
		
	</tr>
		
	</cfif>
	<!--- <tr>
		<td class="formlable" align="left" width="25%"><strong>Originator:</strong></td>
		<td class="bodyTextGrey" width="25%">#request.fname# #request.lname#</td>
		<input type="hidden" name="originatorname" value="#request.fname# #request.lname#">
		<input type="hidden" name="originatoremail" value="#request.userlogin#">
		<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#:</strong></td>
		<td class="bodyTextGrey" width="25%" id="BUTD"><cfif irn gt 0>#getincidentdetails.buname#</cfif></td>
	</tr> --->
	</table>
	</td></tr>
	</cfoutput>
</table>
</cfform>
<script type="text/javascript">
document.getElementById("reqflds").style.display='none';

	document.getElementById("file2").style.display='none';
	document.getElementById("file3").style.display='none';
	document.getElementById("file4").style.display='none';
	document.getElementById("file5").style.display='none';
	
</script>