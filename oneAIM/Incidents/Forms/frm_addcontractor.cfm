<cfif listfind("3,4,11",getincidentcondetail.incassignedto) gt 0 and getincidentcondetail.Contractor eq 0>

<cfform action="#self#?fuseaction=#attributes.xfa.addcontractor#" method="post" name="contractorfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="addcontractor">
					<input type="hidden" name="GroupNumber" value="#getincidentcondetail.GroupNumber#">
					<input type="hidden" name="incassignedtype" value="#getincidentcondetail.IncAssignedTo#">
					<tr>
						<td class="bodyTextGrey" colspan="2">Add Contractor</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Contractors&nbsp;assigned&nbsp;to&nbsp;this&nbsp;project/office:</strong></td>
						<td class="bodyTextGrey" ><select name="contractordetail" size="1" class="selectgen" id="lrgselect" onchange="checkconvalname(this.value);">
										<option value="">-- Select One --</option>
										<option value="0" <cfif getincidentcondetail.Contractor eq 0>selected</cfif>>Not Listed</option>
										<cfloop query="getincsublist"><option value="#ContractorID#" <cfif getincidentcondetail.Contractor eq ContractorID>selected</cfif>>#ContractorName#</option></cfloop>
										</select></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>All&nbsp;Other&nbsp;Contractors:</strong></td>
						<td class="bodyTextGrey" ><select name="contractordetailall" size="1" class="selectgen" id="lrgselect" onchange="checkconvalname(this.value);">
										<option value="">-- Select One --</option>
										<option value="0" selected>Not Listed</option>
											<cfloop query="getconothers"><cfif listfind(currconlist,contractorid) eq 0><option value="#ContractorID#" >#ContractorName#</option></cfif></cfloop>
										</select></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Contractor&nbsp;to&nbsp;be&nbsp;added:</strong></td>
						<td class="bodyTextGrey" ><input type="text" name="otherconame" class="selectgen" maxlength="50" value="#getincidentcondetail.otherContName#"></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="Update Contractor" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
	
</cfif>