<cfparam name="action" default="">
<cfswitch expression="#action#">
<cfcase value= "review">
<Cfoutput>
<script type="text/javascript">
function checkpotrateval(potrate){

var element = document.getElementById('invlevel');
if((potrate=="A1")||(potrate=="B1")||(potrate=="C1")||(potrate=="D1")||(potrate=="A2")||(potrate=="B2")||(potrate=="C2")){

var seltext = "<select name='invlevel' size='1' class='selectgen'><option value='1' selected>Level 1</option><option value='2' disabled>Level 2</option></select>";
document.getElementById('invlevtd').innerHTML = seltext;


}
else{
if((potrate=="E3")||(potrate=="A4")||(potrate=="B4")||(potrate=="C4")||(potrate=="D4")||(potrate=="E4")||(potrate=="A5")||(potrate=="B5")||(potrate=="C5")||(potrate=="D5")||(potrate=="E5")){

var seltext = "<select name='invlevel' size='1' class='selectgen'><option value='1' disabled>Level 1</option><option value='2' selected>Level 2</option></select>";
document.getElementById('invlevtd').innerHTML = seltext;
 

}
else{
<cfif getincidentforreview.investigationlevel eq "1">
var seltext = "<select name='invlevel' size='1' class='selectgen'><option value='1' selected>Level 1</option><option value='2' >Level 2</option></select>";
<cfelse>
var seltext = "<select name='invlevel' size='1' class='selectgen'><option value='1' >Level 1</option><option value='2' selected>Level 2</option></select>";
</cfif>
document.getElementById('invlevtd').innerHTML = seltext;

}
}


}
</script>
</cfoutput>
	<cfform action="#self#?fuseaction=#attributes.xfa.managereview#" method="post" name="investfrm" enctype="multipart/form-data">
	<cfset greenpotratings = "A1,B1,C1,D1,A2,B2,C2">
	<cfset yellowpotratings = "E1,D2,E2,A3,B3,C3,D3">
	<cfset redpotratings = "E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<cfif getincidentforreview.isworkrelated eq "yes">
						<input type="hidden" name="submittype" value="workrelated">
					<cfelse>
						<input type="hidden" name="submittype" value="notworkrelated">
					</cfif>
					<input type="hidden" name="oldPR" value="#getincidentforreview.PotentialRating#">
					<input type="hidden" name="oldIL" value="#getincidentforreview.investigationlevel#">
					<input type="hidden" name="oldFA" value="#getincidentforreview.needFA#">
					<tr>
						<td class="bodyTextGrey" colspan="2">Please review the following and change if necessary</td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Potential&nbsp;Rating:</strong></td><!--- onchange="this.style.border='1px solid 5f2468';"  onchange="checkpotrateval(this.value);"--->
						<td class="bodyTextGrey" width="70%"><select name="potentialrating" size="1" class="selectgen">
											<cfloop list="#greenpotratings#" index="i">
											<option value="#i#" <cfif getincidentforreview.PotentialRating eq i>selected</cfif> class="greenTextdd">#i#</option>
											</cfloop>
											<option value="" disabled></option>
											<cfloop list="#yellowpotratings#" index="i">
											<option value="#i#" <cfif getincidentforreview.PotentialRating eq i>selected</cfif> class="yellowTextdd">#i#</option>
											</cfloop>
											<option value="" disabled></option>
											<cfloop list="#redpotratings#" index="i">
											<option value="#i#" <cfif getincidentforreview.PotentialRating eq i>selected</cfif>  class="redTextdd">#i#</option>
											</cfloop>
												</select></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Investigation&nbsp;Level:</strong></td><!--- onchange="this.style.border='1px solid 5f2468';" --->
						<td class="bodyTextGrey" width="70%" id="invlevtd">
						<!--- <cfswitch expression="#getincidentforreview.PotentialRating#">
						<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<select name="invlevel" size="1" class="selectgen" >
										<option value="1" <cfif getincidentforreview.investigationlevel eq "1">selected</cfif>>Level 1</option>
										<option value="2" disabled>Level 2</option>
											</select>
						</cfcase>
						<cfcase value="E1,D2,E2,A3,B3,C3,D3"> --->
						<select name="invlevel" size="1" class="selectgen">
						
											<option value="1" <cfif getincidentforreview.investigationlevel eq "1">selected</cfif>>Level 1</option>
											<option value="2" <cfif getincidentforreview.investigationlevel eq "2">selected</cfif>>Level 2</option>
												</select>
						<!--- </cfcase>
						<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<select name="invlevel" size="1" class="selectgen">
										<option value="1" disabled>Level 1</option>
										<option value="2" <cfif getincidentforreview.investigationlevel eq "2">selected</cfif>>Level 2</option>
											</select>
						</cfcase>
						</cfswitch> --->
						</td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Is&nbsp;a&nbsp;First&nbsp;Alert&nbsp;required?</strong></td>
						<td class="bodyTextGrey" width="70%">Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="needFA" value="1" <cfif getincidentforreview.needFA eq "1">checked</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;<input type="radio" name="needFA" value="0" <cfif getincidentforreview.needFA Neq "1">checked</cfif>></td>
					</tr>
					<cfif getincidentforreview.isworkrelated eq "No">
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Change&nbsp;to&nbsp;Work&nbsp;Related:</strong></td>
						<td class="bodyTextGrey" width="70%"><input type="checkbox" name="changetowr" value="1"></td>
					</tr>
					
					</cfif>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value= "requestinfo">
	<cfform action="#self#?fuseaction=#attributes.xfa.incidentmanament#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="moreinfo">
					<tr>
						<td class="bodyTextGrey" colspan="2">Request Updated Information</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value= "requestinfofat">
	<cfform action="#self#?fuseaction=#attributes.xfa.incidentmanament#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="moreinfofat">
					<tr>
						<td class="bodyTextGrey" colspan="2">Request Updated Information</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value= "reviewfat">
	<cfform action="#self#?fuseaction=#attributes.xfa.managereview#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="reviewfat">
					<tr>
						<td class="bodyTextGrey" colspan="2">Approve this Fatality Report</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value= "requestinfoFA">
<cfparam name="fa" default="0">

<cfquery name="getfastatval" datasource="#request.dsn#">
select status
from oneaimfirstalerts
where firstalertid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#fa#">
</cfquery>



	<cfform action="#self#?fuseaction=#attributes.xfa.famanagement#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
						<input type="hidden" name="fa" value="#fa#">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="fastatval" value="#getfastatval.status#">
					<input type="hidden" name="submittype" value="moreinfo">
					<tr>
						<td class="bodyTextGrey" colspan="2">Request Updated Information for First Alert</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value="reviewFA">
	<cfparam name="fa" default="0">
	<cfform action="#self#?fuseaction=#attributes.xfa.famanagement#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="fa" value="#fa#">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="reviewfa">
					<tr>
						<td class="bodyTextGrey" colspan="2">Submit First Alert for Review</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value="SRreviewFA">
	<cfparam name="fa" default="0">
	<cfform action="#self#?fuseaction=#attributes.xfa.famanagement#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="fa" value="#fa#">
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="seniorreviewfa">
									
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Approve&nbsp;First&nbsp;Alert:</strong></td>
						<td class="bodyTextGrey" >Approve&nbsp;&nbsp;&nbsp;<input type="radio" name="approveFA" value="yes" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Decline&nbsp;&nbsp;&nbsp;
						<input type="radio" name="approveFA" value="no"></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value="reviewInv">
	<cfform action="#self#?fuseaction=#attributes.xfa.reviewinvestigation#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					
					<!--- <tr>
						<td class="bodyTextGrey" colspan="2">Approve Investigation</td>
					</tr> --->
					<!--- <tr>
						<td class="formlable" align="left" width="30%"><strong>Approve&nbsp;Investigation:</strong></td>
						<td class="bodyTextGrey" >Approve&nbsp;&nbsp;&nbsp;<input type="radio" name="approveInv" value="yes" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Decline&nbsp;&nbsp;&nbsp;
						<input type="radio" name="approveInv" value="no"></td>
					</tr> --->
					<cfif getinvstatus.status eq "Sent for Senior Review">
					<input type="hidden" name="submittype" value="approvinv">
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Is this incident going for IRP?:</strong></td>
						<td class="bodyTextGrey" >Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="irp" value="yes" <cfif getincidentforreview.investigationlevel eq 2>checked</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;
						<input type="radio" name="irp" value="no"  <cfif getincidentforreview.investigationlevel neq 2>checked</cfif>></td>
					</tr>
					<cfelseif getinvstatus.status eq "Sent for Review" and getincidentforreview.InvestigationLevel eq 1>
					<input type="hidden" name="submittype" value="approvinv">
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Is this incident going for IRP?:</strong></td>
						<td class="bodyTextGrey" >Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="irp" value="yes" <cfif getincidentforreview.investigationlevel eq 2>checked</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;
						<input type="radio" name="irp" value="no"  <cfif getincidentforreview.investigationlevel neq 2>checked</cfif>></td>
					</tr>
					<cfelseif getinvstatus.status eq "Sent for Review" and getincidentforreview.InvestigationLevel eq 2>
						<input type="hidden" name="submittype" value="approveforsenior">
					</cfif>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="submit" value="Approve" class="selectGenBTN">&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value="moreinfoInv">

<cfform action="#self#?fuseaction=#attributes.xfa.reviewinvestigation#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					<input type="hidden" name="irn" value="#irn#">
					<cfif getinvstatus.status eq "Sent for Senior Review">
					<input type="hidden" name="submittype" value="srinvmoreinfo">
					<cfelse>
					<input type="hidden" name="submittype" value="moreinfo">
					</cfif>
					<tr>
						<td class="bodyTextGrey" colspan="2">Request Updated Information for Investigation</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>

</cfcase>
<cfcase value= "requestinfoIRP">
<cfparam name="fa" default="0">
	<cfform action="#self#?fuseaction=#attributes.xfa.manageIRP#" method="post" name="irpmifrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
						
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="moreinfoIRP">
					<tr>
						<td class="bodyTextGrey" colspan="2">Request Updated Information for IRP</td>
					</tr>
					
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value="reviewIRP">
	<cfparam name="fa" default="0">
	<cfform action="#self#?fuseaction=#attributes.xfa.manageIRP#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="approveIRP">
									
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Approve&nbsp;IRP:</strong></td>
						<td class="bodyTextGrey" >Approve&nbsp;&nbsp;&nbsp;<input type="radio" name="approveFA" value="yes" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Decline&nbsp;&nbsp;&nbsp;
						<input type="radio" name="approveFA" value="no"></td>
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Cancel" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="OK" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
<cfcase value="cancelIRP">
	<cfparam name="fa" default="0">
	<cfform action="#self#?fuseaction=#attributes.xfa.manageIRP#" method="post" name="investfrm" enctype="multipart/form-data">
	
	<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%" align="center">
		<tr>
			<td align="center" bgcolor="ffffff">
			
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
					<cfoutput>
					
					<input type="hidden" name="irn" value="#irn#">
					<input type="hidden" name="submittype" value="cancelIRP">
									
					<tr>
						<td class="formlable" align="left" width="30%" colspan="2"><strong>Cancel&nbsp;IRP</strong></td>
						<!--- <td class="bodyTextGrey" >Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="cancelIRP" value="yes" checked>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;
						<input type="radio" name="cancelIRP" value="no"></td> --->
					</tr>
					<tr>
						<td class="formlable" align="left" width="30%"><strong>Comments:</strong></td>
						<td class="bodyTextGrey" ><textarea name="revcomments" class="selectgen" rows="3" cols="35"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" ALIGN="RIGHT"><input type="button" value="Close Form and Keep IRP" class="selectGenBTN" onclick="window.close();">&nbsp;&nbsp;&nbsp;<input type="submit" value="Cancel IRP" class="selectGenBTN"></td>
					</tr>
					</cfoutput>
				</table>
			
			</td>
		</tr>
	</table>
	</cfform>
</cfcase>
</cfswitch>