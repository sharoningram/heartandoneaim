<!--- <div class="content1of1"> --->
<!---  <script type="text/javascript">
function openinfowin(thisurl) {
window.open(thisurl, "More Information", "width=450, height=350, location=no, menubar=no, titlebar=no, toolbar=no");
}

</script> --->
<cfset fldauditstruct = {}>
<cfloop query="getfldaudit">
	<cfif listfindnocase("potrating,invlevel,oshacat",fieldname) gt 0>
		<cfset fldauditstruct[fieldname] = "yes">
	</cfif>
</cfloop>
 <script type="text/javascript">
 http = new XMLHttpRequest()
 
 <cfif trim(getincidentdetails.DOheight) gt 0 and trim(getincidentdetails.DOweight) gt 0>


// var totenergy = (mass  9.81  height);
 // document.incidentfrm.doenergyinj.value=Math.round(totenergy100)/100;
 // document.incidentfrm.doenergyinj.style.border = "1px solid 5f2468";
 <cfoutput>
 
 var params = "&dh=#getincidentdetails.DOheight#&dm=#getincidentdetails.DOweight#";
		var myurl = "#self#?fuseaction=#attributes.xfa.drawdropchart#" ;
		// alert(myurl + ' ' + params);
		http.open('POST', myurl , true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.send(params);
		http.onreadystatechange = handlePopup;
 
 </cfoutput>
 
 
 function handlePopup(){
		if (http.readyState == 4){
			document.getElementById("dochart").style.visibility="visible";
			var startpos = http.responseText.search("{zz{") + 4;
			var endpos = http.responseText.search("}zz}");
			var textout = http.responseText.substring(startpos,endpos);
			document.getElementById("dochart").innerHTML=textout;
			}
		
	}

 </cfif> 


 

	
</script> 

<cfset fileguid = createuuid()>

<cfset inccutoff = dateadd("h","-24",now())>

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">


	<tr>
		<td align="center" bgcolor="ffffff">
		<cfoutput>
	<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff">

	<cfif irn gt 0>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident Report Number:</strong></td>
		<td class="bodyTextGrey" width="25%" colspan="3">#getincidentdetails.trackingnum#</td>
	
	</tr>
	</cfif>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Originator:</strong></td>
		<td class="bodyTextGrey" width="25%">#getincidentdetails.createdby#</td>
		<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#:</strong></td>
		<td class="bodyTextGrey" width="25%" id="BUTD"><cfif irn gt 0>#getincidentdetails.buname#</cfif></td>
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Date Created:</strong></td>
		<td class="bodyTextGrey" width="25%">#dateformat(getincidentdetails.datecreated,sysdateformat)#</td>
		<td class="formlable" align="left" width="25%"><strong>#request.oulabellong#:</strong></td>
		<td class="bodyTextGrey" width="25%" id="OUTD"><cfif irn gt 0>#getincidentdetails.ouname#</cfif></td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Project/Office:</strong></td>
		<td class="bodyTextGrey" width="25%"><!--- <cfif irn eq 0> --->
										<cfloop query="getgroups">
										<cfif getincidentdetails.groupnumber eq group_number>#group_name#</cfif>
										</cfloop>
										<!--- <cfelse>#getincidentdetails.group_name#</cfif> --->
										</td>
		<td class="formlable" align="left" width="25%"><strong>Business Stream:</strong></td>
		<td class="bodyTextGrey" width="25%" id="BSTD"><cfif irn gt 0><cfif trim(getincidentdetails.bsname) neq ''>#getincidentdetails.bsname#<cfelse>Not Applicable</cfif></cfif></td>
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Site/Office Name:</strong></td>
		<td class="bodyTextGrey" width="25%"  id="SOTD"><!--- this.style.border='1px solid 5f2468';
		<cfif isdefined("getinclocations.recordcount")><cfif getinclocations.recordcount eq 1>
<select name="site" size="1" class="selectgen" id="lrgselect"><option value="#getinclocations.GroupLocID#" selected>#getinclocations.SiteName#</option></select>
<cfelse><select name="site" size="1" class="selectgen" id="lrgselect" onchange="popcountryinfo(this.value);"><option value="">-- Select One --</option>
<cfloop query="getinclocations"><option value="#GroupLocID#" <cfif getincidentdetails.LocationID eq grouplocid>selected</cfif>>#SiteName#</option></cfloop></select>
</cfif><Cfelse> --->#getincidentdetails.sitename#<!--- </cfif></cfif> --->
										</td>
		<td class="formlable" align="left" width="25%"><strong>Country:</strong></td>
		<td class="bodyTextGrey" width="25%" id="CNTYTD"><cfif irn gt 0>#getincidentdetails.CountryName#</cfif></td>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Is This Incident Work Related?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.isworkrelated#</td>
		<td class="formlable" align="left" width="25%"><strong>Client:</strong></td>
		<td class="bodyTextGrey" width="25%" id="CNTYTD"><cfif irn gt 0>#getincidentdetails.ClientName#</cfif></td>							
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Incident Assigned To:</strong></td>
		<td class="bodyTextGrey"><cfloop query="getincassignto"><cfif getincidentdetails.incassignedto eq IncAssignedID>#IncAssignedTo#</cfif></cfloop></td>
			<cfif getincidentdetails.Contractor neq 0>	
			<td class="formlable" align="left"><strong>Contractor Details:</strong></td>
			<td class="bodyTextGrey" id="subconlist">
					<cfif isdefined("getincsublist.recordcount")><cfloop query="getincsublist"><cfif getincidentdetails.Contractor eq ContractorID>#ContractorName#</cfif></cfloop></cfif></td>
			<cfelse>
				<cfif listfind("3,4,11",getincidentdetails.incassignedto) gt 0>
				<td class="formlable" align="left"><strong>Contractor Details:</strong></td>
				<td class="bodyTextGrey" id="subconlist">
					Not Listed<cfif trim(getincidentdetails.otherContName) neq ''>:&nbsp;#getincidentdetails.otherContName#</cfif>
					</td>
				</cfif>
					</cfif>
	</tr>
	<cfif trim(getincidentdetails.OtherAssignedTo) neq ''>
	<tr>
		<td class="formlable" align="left"><strong class="formlable">Other Incident Assigned To:&nbsp;&nbsp;</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.OtherAssignedTo#</td>
		<td></td>
		<td></td>
		</tr>
	</cfif>
	<tr>
		<td class="formlable" align="left"><strong>Did the Incident Occur at this Location?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.occuratlocation#</td>
		<cfif getincidentdetails.occuratlocation eq "No">
		<td class="formlable" align="left"><strong>If No, Where Did the Incident Occur?</strong></td>
		<td class="bodyTextGrey"><cfloop query="getincplaces"><cfif getincidentdetails.whereoccur eq PlaceID>#IncPlace#</cfif></cfloop></td>
		</cfif>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Specific Location Where Incident Occurred:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.SpecificLocation#</td>
		<td class="bodyTextGrey" align="left"></td>
		<td class="bodyTextGrey"></td>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Primary Incident Type:</strong></td>
		<td class="bodyTextGrey">
										<cfloop query="getinctypes">
											<cfif listfindnocase("P,PS",IncTypeDsp) gt 0>
												<cfif getincidentdetails.PrimaryType eq IncTypeID>#IncType#</cfif>
											</cfif>
										</cfloop>
										</td>
										<td></td>
										<td></td>
	</tr>
	
	<tr>
		<td class="formlable" align="left"><strong>Is This a Near Miss Incident?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.isnearmiss#</td>
											<td></td>
										<td></td>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Is there a Secondary Incident Type?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.issecondary#</td>
											<td></td>
										<td></td>
	</tr>
	
	<cfif getincidentdetails.secondarytype neq 0 or getincidentdetails.oidefinition neq 0>
	<tr>
		<cfif getincidentdetails.secondarytype neq 0>
		<td class="formlable" align="left"><strong>Secondary Incident Type:</strong></td>
		<td class="bodyTextGrey">
										<cfloop query="getinctypes">
											<cfif listfindnocase("S,PS",IncTypeDsp) gt 0>
												<cfif getincidentdetails.secondarytype eq IncTypeID>#IncType#</cfif>
											</cfif>
										</cfloop></td>
		</cfif>
		<cfif getincidentdetails.oidefinition neq 0>
	<td class="formlable" align="left"><strong>Occupational Illness/Disease Definition:</strong></td>
		<td class="bodyTextGrey"><cfloop query="getoidef"><cfif getincidentdetails.oidefinition eq OIdefID>#OIdef#</cfif></cfloop>
										
										</td>
			</cfif>
	</tr>
	</cfif>
	<tr>
		<td class="formlable" align="left"><strong>Incident Date:</strong></td>
		<td class="bodyTextGrey">#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
		<td class="formlable" align="left"><strong>Time Incident Occurred:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.incidenttime#</td>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Day of Week:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.dayofweek#</td>
		<cfif trim(getincidentdetails.weather) gt 0>
		<td class="formlable" align="left"><strong>Primary Weather Condition:</strong></td>
		<td class="bodyTextGrey"><cfloop query="getweather"><cfif getincidentdetails.weather eq WeatherCondID>#weathercond#</cfif></cfloop></td>
		</cfif>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Short Description:</strong></td>
		<td class="bodyTextGrey" colspan="3">#getincidentdetails.shortdesc#</td>
	</tr>
	
	
		<tr>
		<td class="formlable" align="left"><strong>Was the Incident Reported by AmecFW Personnel?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.reportedbyco#</td>
			<td></td><td></td>
		</tr>
	<tr>
		<cfif trim(getincidentdetails.reportedby) neq ''>
		<td class="formlable" align="left"><strong>Incident Reported By:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.reportedby#</td>
		</cfif>
		<cfif trim(getincidentdetails.nonamecfwemail) neq ''>
		<td class="formlable" align="left"><strong>Non AmecFW Contact Email:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.nonamecfwemail#</td>
		</cfif>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Description of Incident:</strong></td>
		<td class="bodyTextGrey" colspan="3">#getincidentdetails.description#</td>
	</tr>
	
	<tr>
			<td align="left" class="formlable" width="25%"><strong class="formlable"><cfif structkeyexists(fldauditstruct,"potrating")><a href="javascript:void(0);" onclick="NewWindow('index.cfm?fuseaction=#attributes.xfa.fieldaudit#&irn=#irn#&audittype=potrating','Info','650','450','no');"><img src="images/note.gif" border="0" style="vertical-align:-18%;padding-right:6px;"></a></cfif>Potential Rating</strong>
		<td colspan="3" align="left" width="75%"> 

		<table cellpadding="4" cellspacing="1" bgcolor="##000000" width="88%" id="potentialtable">
					
					<tr style="height:37px;">
						<td rowspan="8" width="1%" align="center" bgcolor="000000"><img src="images/consequences.png" border="0"></td>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf"><strong class="bodytext">When using this matrix consider the potential outcome of the incident not the actual outcome<br>IP = intellectual property</strong></td>
						<td width="25%" align="center"  colspan="5" bgcolor="ffffff"><strong class="bodytext">No. of People</strong></td>
						
					</tr>
					<tr style="height:37px;" bgcolor="ffffff">
						
						<td  align="center"  ></td>
						<td   align="center"  ><strong class="bodyTextsm">Injury/Health</strong></td>
						<td  align="center"  ><strong class="bodyTextsm">Environmental</strong></td>
						<td  align="center"  ><strong class="bodyTextsm">Damage</strong></td>
						<td   align="center"  ><strong class="bodyTextsm">Security</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm">0</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">1</strong></td>
						<td  align="center" width="5%" ><strong class="bodyTextsm">1-2</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">2-10</strong></td>
						<td align="center"  width="5%"><strong class="bodyTextsm">10+</strong></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center" bgcolor="##DDEEFF"><strong class="bodytext">1</strong></td>
						<td align="center" class="bodyTextsm">First aid/health effect</td>
						<td align="center" class="bodyTextsm">Minimal reversible environmental impact</td>
						<td align="center"  class="bodyTextsm">Minor loss/damage/business impact (<10K)</td>
						<td align="center" class="bodyTextsm">Minor crime, no impact on business operations or reputation</td>
					<td align="center" bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "A1"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A1</strong></cfif></td>
					<td align="center" bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "B1"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B1</strong></cfif></td>
					<td  align="center" bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "C1"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C1</strong></cfif></td>
					<td align="center" bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "D1"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D1</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "E1"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">E1</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td  align="center" bgcolor="##DDEEFF"><strong class="bodytext">2</strong></td>
						<td align="center" class="bodyTextsm">Medical treatment/ restricted work/ moderate health effect</td>
						<td align="center"  class="bodyTextsm">Minor pollution with short term impact (1 month)</td>
						<td align="center"  class="bodyTextsm">Moderate loss/ damage/ business impact (10-100K)</td>
						<td align="center"  class="bodyTextsm">Theft/vandalism/loss of non-IP information. No lasting impact on business operations or reputation</td>
					<td  align="center" bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "A2"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A2</strong></cfif></td>
					<td  align="center" bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "B2"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B2</strong></cfif></td>
					<td align="center"  bgcolor="##00b050"><cfif getincidentdetails.potentialrating eq "C2"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C2</strong></cfif></td>
					<td  align="center" bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "D2"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">D2</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "E2"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">E2</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">3</strong></td>
						<td align="center"  class="bodyTextsm">Lost time injury/ significant health effect</td>
						<td align="center"  class="bodyTextsm">Moderate pollution with medium term localised impact (1 year)</td>
						<td align="center"  class="bodyTextsm">Significant loss/damage/ business impact reportable event within local legislation (100K-1M)</td>
						<td align="center"  class="bodyTextsm">Crime (threat, intimidation, sabotage) with impact on people or loss of confidential IP containing material</td>
					<td  align="center" bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "A3"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">A3</strong></cfif></td>
					<td  align="center" bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "B3"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">B3</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "C3"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">C3</strong></cfif></td>
					<td align="center"  bgcolor="##ffc000"><cfif getincidentdetails.potentialrating eq "D3"><strong style="font-size:12pt;color:000000;font-family:Segoe UI;">D3</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "E3"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E3</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">4</strong></td>
						<td align="center"  class="bodyTextsm">Serious injury/ severe health effect/ long term disability</td>
						<td align="center"  class="bodyTextsm">Severe pollution with long term localised impact (+1 year)</td>
						<td align="center"  class="bodyTextsm">Severe loss/damage/ business impact reportable event within local legislation (1-10M)</td>
						<td align="center"  class="bodyTextsm">Serious and deliberate criminal attack against people, disruptive natural event or loss of sensitive IP material</td>
					<td align="center"  bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "A4"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A4</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "B4"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B4</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "C4"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C4</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "D4"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D4</strong></cfif></td>
					<td align="center" bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "E4"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E4</strong></cfif></td>
					</tr>
					<tr bgcolor="##ffffff" style="height:37px;">
						<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">5</strong></td>
						<td align="center"  class="bodyTextsm">Fatality</td>
						<td align="center"  class="bodyTextsm">Major pollution with long term environmental change</td>
						<td align="center"  class="bodyTextsm">Major loss/damage/ reportable event within local legislation business impact (10M+)</td>
						<td align="center"  class="bodyTextsm">Sustained, serious attack/loss of confidential IP or natural disaster requiring formal emergency response</td>
					<td  align="center" bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "A5"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">A5</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "B5"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">B5</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "C5"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">C5</strong></cfif></td>
					<td  align="center" bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "D5"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">D5</strong></cfif></td>
					<td align="center"  bgcolor="##ff0000"><cfif getincidentdetails.potentialrating eq "E5"><strong style="font-size:12pt;color:ffffff;font-family:Segoe UI;">E5</strong></cfif></td>
					</tr>
					<tr>
						<td width="74%" align="center" colspan="5"  bgcolor="dfdfdf" class="bodyTextsm">High Potential Incidents (Red) = LEVEL 2; Medium Potential Incidents (Amber) = LEVEL 1 or 2 (at HSSE Manager's discretion); Low Potential Incidents (Green) = LEVEL 1</td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">A</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">B</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">C</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">D</strong></td>
				<td align="center"  bgcolor="##DDEEFF"><strong class="bodytext">E</strong></td>
					</tr>
				</table>	
				</td>
			</tr> 
		<tr>
		<td class="formlable" align="left" width="25%"><strong><cfif structkeyexists(fldauditstruct,"invlevel")><a href="javascript:void(0);" onclick="NewWindow('index.cfm?fuseaction=#attributes.xfa.fieldaudit#&irn=#irn#&audittype=invlevel','Info','650','450','no');"><img src="images/note.gif" border="0" style="vertical-align:-18%;padding-right:6px;"></a></cfif>Investigation Level:</strong></td>
		<td class="bodyTextGrey"><cfif getincidentdetails.investigationlevel eq "1">Level 1<cfelseif getincidentdetails.investigationlevel eq "2">Level 2</cfif></td>
										<td></td><td></td>
	</tr>
	
		
	
		<tr>
		<td class="formlable" align="left"><strong>HSSE Advisor:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.hsseadvisor#</td>
		<td class="formlable" align="left"><strong>HSSE Manager:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.hssemanager#</td>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Immediate Action Taken:</strong></td>
		<td class="bodyTextGrey" colspan="3">#getincidentdetails.actiontaken#</td>
	</tr>
	
<tr>
		<td class="formlable" align="left"><strong>Reported to Statutory Body?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.wasreported#</td>
		<cfif getincidentdetails.wasreported eq "Yes">
		<td class="formlable" align="left"><strong>Statutory Body Reported To:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.StatutoryBody#</td>
		</cfif>
	</tr>
	<cfif getincidentdetails.wasreported eq "Yes">
	<tr>
		<td class="formlable" align="left"><strong>Date Reported to Statutory Body:</strong></td>
		<td class="bodyTextGrey">#dateformat(getincidentdetails.datestatreported,sysdateformat)#</td>
		<td></td><td></td>
	</tr>
	</cfif>
<tr>
		<td class="formlable" align="left"><strong>Has an Enforcement Notice Been Issued?</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.EnforcementNotice#</td>
		<cfif getincidentdetails.EnforcementNotice eq "Yes">
		<td class="formlable" align="left"><strong>Type of Enforcement Notice:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.EnforcementNoticeType#</td>
		</cfif>
	</tr>
	
	
	</cfoutput>
	
	
		<tr>
			
				<td class="formlable" align="left"><strong>Dropped Object?</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentdetails.droppedObject#</cfoutput></td>
			<td></td>
			<td></td>
			</tr>
		<cfif getincidentdetails.primarytype eq 1 or getincidentdetails.secondarytype eq 1>
		<tr>
		<td class="formlable" align="left"><strong>Source of hazard:</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getinjurysources">
										<cfif listfind(getincidentdetails.HazardSource,InjSourceID) gt 0>#injurysource#<br></cfif>
										</cfoutput></td>
										<cfif trim(getincidentdetails.otherHazard) neq ''>
				<td class="formlable" align="left"><strong>Other source of hazard:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentdetails.otherHazard#</cfoutput></td>
							</cfif>
	</tr>
	</cfif>
	<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_firstincident.cfm", "uploads\incidents\#irn#\irn"))>
	
	<tr>
		<td class="formlable" align="left"><strong>Supporting Information:</strong></td>
		<td class="bodyTextGrey" colspan="3">
		<cfif directoryexists(filedir)>
			<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">
				<cfif getincfileslist.recordcount gt 0>
					<cfoutput query="getincfileslist">
						<a href="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/irn/#name#" target="_blank">#name#</a><br>
					</cfoutput>
				</cfif>
			</cfif>
</td>
	</tr>
	
	
	
	
	
	

	
</table>
</td></tr>
<tr>
	<td id="injuryfrm">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><strong class="bodyTextWhite">Injury/Occupational Illness/Disease</strong></td>
			</tr>
			<tr>
				<td>
					<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
		<td class="formlable" align="left" width="25%"  id="oshacatlblsel"><strong><cfif structkeyexists(fldauditstruct,"oshacat")><cfoutput><a href="javascript:void(0);" onclick="NewWindow('index.cfm?fuseaction=#attributes.xfa.fieldaudit#&irn=#irn#&audittype=oshacat','Info','650','450','no');"><img src="images/note.gif" border="0" style="vertical-align:-18%;padding-right:6px;"></a></cfoutput></cfif>OSHA classification:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%" id="oshacatfldsel">
										<cfoutput query="getoshacats">
										<cfif getincidentoi.oshaclass eq catid>#Category#</cfif>
										</cfoutput></td>
				<td class="formlable" align="left" width="25%"><strong>Is injury classified as a serious injury?</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentoi.isSerious#</cfoutput></td>
	</tr>
				
	
				<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Injured/Ill Person's Details</strong></td>
			</tr>
			<tr>
			<td class="formlable" align="left"><strong>Injured Person's Name:</strong></td>
		<td class="bodyTextGrey"  align="left">
		<cfset canseename = "no">
			<cfif request.userlogin eq getincidentdetails.createdbyEmail>
				<cfset canseename = "yes">
			</cfif>
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"Senior Executive View") gt 0>
				<cfset canseename = "yes">
			</cfif>
			<cfif  listfindnocase(request.userlevel,"Reviewer") gt 0>
				<cfif listfind(request.userOUs,getincidentdetails.ouid) gt 0>
					<cfset canseename = "yes">
				</cfif>
			</cfif>
			<cfif  listfindnocase(request.userlevel,"BU Admin") gt 0 or  listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
				<cfif listfind(request.userBUs,getincidentdetails.business_line_id) gt 0>
					<cfset canseename = "yes">
				</cfif>
			</cfif>
		<cfoutput><cfif canseename eq "no">#repeatstring("X",15)#<cfelse>#getincidentoi.ipName#</cfif></cfoutput></td>
		<td class="formlable" align="left"><strong>Injured Person's Occupation:</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getoccupations">
										<cfif getincidentoi.ipOccupation eq occupationID>#occupation#</cfif>
										</cfoutput></td>
			</tr>
			<tr>
			<td class="formlable" align="left"><strong>Age Profile:</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getages">
										<cfif getincidentoi.ipage eq ageid>#AgeDesc#</cfif>
										</cfoutput></td>
				<td class="formlable" align="left"><strong>Gender:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.ipGender#</cfoutput></td>
			
			</tr>
			<tr>
				<td class="formlable" align="left"><strong>Days Since Last Day Off:</strong></td>
				<td class="bodyTextGrey"><cfoutput>#getincidentoi.dayssinceoff#</cfoutput></td>
				<td></td><td></td>
			</tr>
			
			
			
			<tr>
			<td class="formlable" align="left"><strong>Is injured person working shifts?</strong></td>
				<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.workingshift#</cfoutput></td>
			
			
			<cfif getincidentoi.workingshift eq "yes">
			
				<td class="formlable" align="left"><strong>Shift:</strong></td>
				<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.ipshift#</cfoutput></td>
			<cfelse>
			<td></td><td></td>
			</cfif>
		</tr>
		<cfif getincidentoi.workingshift eq "yes">
		<tr>
			<td class="formlable" align="left"><strong>The number of days into the shift when the incident occurred:</strong></td>
			<cfoutput><td class="bodyTextGrey">#getincidentoi.shiftday#</td></cfoutput>
					
			<td class="formlable" align="left"><strong>The total number of days in that shift:</strong></td>
			<cfoutput><td class="bodyTextGrey">#getincidentoi.shiftdayof#</cfoutput>
			</tr>
		</cfif>
			<cfif getincidentdetails.primarytype eq 1 or getincidentdetails.secondarytype eq 1>
			<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Injury Details</strong></td>
			</tr>
			<tr>
			<td class="formlable" align="left"><strong>Injury Type:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput query="getinjurytypes">
										<cfif listfind(getincidentoi.injuryType,injtypeid) gt 0>#injurytype#<br></cfif>
										</cfoutput></td>
										<cfif trim(getincidentoi.otherInjuryType) neq ''>
				<td class="formlable" align="left"><strong>Other Injury Type:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.otherInjuryType#</cfoutput></td>
						</cfif>
			</tr>
			<tr>
			<td class="formlable" align="left"><strong>Body Part Affected:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput query="getbodypart">
										<cfif listfind(getincidentoi.bodyPart,bodypartid) gt 0>#bodypart#<br></cfif>
										</cfoutput></td>
				<td class="formlable" align="left"><strong>Cause of Injury Incident:</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getinjurycause">
										<cfif listfind(getincidentoi.injuryCause,injnatureid) gt 0>#injurynature#<br></cfif>
										</cfoutput></td>
			
			</tr>
			<tr>
			<cfif trim(getincidentoi.otherInjuryCause) neq ''>
			<td class="formlable" align="left"><strong>Other Cause of Injury Incident:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.otherInjuryCause#</cfoutput></td>
			</cfif>
			<cfif trim(getincidentoi.fallheight) neq ''>
		<td class="formlable" align="left"><strong>Height of fall (m):</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.fallheight#</cfoutput></td>
			</cfif>
		</tr>
		<!---  <cfif trim(getincidentoi.droppedObject) neq ''>
		<tr>
			
				<td class="formlable" align="left"><strong>Dropped Object?</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.droppedObject#</cfoutput></td>
			<td></td>
			<td></td>
			</tr>
		</cfif> --->
			<!--- <cfif getincidentoi.droppedObject eq "Yes">
			<cfoutput>
		<tr>
			<td class="formlable" align="left"><strong>Dropped Object Height (m):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentoi.DOheight#</td>
		<td class="formlable" align="left"><strong>Weight of Object (kg):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentoi.DOweight#</td>
		</tr></cfoutput>
		
		<tr><cfoutput>
			<td class="formlable" align="left"><strong>Dropped object energy (KJ):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentoi.DOenergy#</cfoutput>
		<td class="formlable" align="left"><strong>Dropped object category:</strong></td>
		<td class="bodyTextGrey"  align="left"><!--- <cfif getincidentoi.DOcategory eq "A">A - Minor
															<cfelseif getincidentoi.DOcategory eq "B">B - Important
															<cfelseif getincidentoi.DOcategory eq "C">C - High Potential
															<cfelseif getincidentoi.DOcategory eq "D">D - High Potential</cfif> ---></td>
		</tr>
		</cfif> --->
		</cfif>
		<tr>
			<td colspan="4" align="center" id="injdochart"></td>
		</tr>
		<cfif getincidentdetails.primarytype eq 5 or getincidentdetails.secondarytype eq 5>
		<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Illness Details</strong></td>
			</tr>
		<tr>
			<td class="formlable" align="left"><strong>Illness Type:</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getillnesstypes">
										<cfif listfind(getincidentoi.IllnessType,illtypeid) gt 0>#illnesstype#<br></cfif>
										</cfoutput></td>
										<Cfif trim(getincidentoi.OtherIllnessType) neq ''>
				<td class="formlable" align="left"><strong>Other Illness Type:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.OtherIllnessType#</cfoutput></td>
							</cfif>
			</tr>
			<tr>
			<td class="formlable" align="left"><strong>Nature of Illness:</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getillnessnature">
										<cfif listfind(getincidentoi.IllnessNature,illnatureid) gt 0>#IllnessNature#<br></cfif>
										</cfoutput></td>
										<cfif trim(getincidentoi.OtherIllnessNature) neq ''>
				<td class="formlable" align="left"><strong>Other Nature of Illness Incident:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentoi.OtherIllnessNature#</cfoutput></td>
						</cfif>
			</tr>
			</cfif>
			<cfif getincidentoi.oshaclass eq 2>
			<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Lost Time Incidents</strong></td>
			</tr>
			<cfoutput>
			<tr>
				<td class="formlable" align="left"><strong>First Date of Absence:</strong></td>
				<td class="bodyTextGrey"  align="left">#dateformat(getincidentoi.LTIFirstDate,sysdateformat)#</td>
				<td class="formlable" align="left"><strong>Date Returned to Work:</strong></td>
				<td class="bodyTextGrey"  align="left">#dateformat(getincidentoi.LTIDateReturned,sysdateformat)#</td>
			</tr>
			<tr>
			<td class="formlable" align="left"><strong>Has Injured Person Returned with Restricted Duties:</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentoi.LTIReturnRestricted#</td>
		<cfif trim(getincidentoi.LTIFirstDate) eq '' or trim(getincidentoi.LTIDateReturned) eq ''>
			<td></td><td></td>
		<cfelse>
			<cfif datediff("d",getincidentoi.LTIFirstDate,getincidentoi.LTIDateReturned) lt 180>
			<td></td>
			<td></td>
			<cfelse>
			<td class="formlable" align="left"><strong>Date returned comments</strong></td>
			<td class="bodyTextGrey"  align="left">#getincidentoi.dateretcomm#</td>
			</cfif>
		</cfif>
		</tr></cfoutput>
			</cfif>
			<cfoutput>
			<cfif getincidentoi.oshaclass eq 4>
		<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Restricted Work Cases</strong></td>
			</tr>
			<tr>
				<td class="formlable" align="left"><strong>First Date Restricted/Modified Duties:</strong></td>
				<td class="bodyTextGrey"  align="left">#dateformat(getincidentoi.RWCFirstDate,sysdateformat)#</td>
				<td class="formlable" align="left"><strong>Date Returned to Full Duty:</strong></td>
				<td class="bodyTextGrey"  align="left">#dateformat(getincidentoi.RWCDateReturned,sysdateformat)#</td>
			</tr>
			
			</cfif>
			<cfif trim(getincidentoi.MedicalRestrictions) neq ''>
			<tr>
			<td class="formlable" align="left"><strong>Medical Restrictions:</strong></td>
		<td class="bodyTextGrey"  align="left" colspan="3">#getincidentoi.MedicalRestrictions#</td>
		
		</tr>
			</cfif>
		</cfoutput>
					</table>
				</td>
			</tr>
			
			
		</table>
	
	</td>
</tr>


<tr>
	<td id="envirofrm">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><strong class="bodyTextWhite">Environmental Incident</strong></td>
			</tr>
			<tr>
				<td>
					<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
								<td class="formlable" align="left" width="25%"><strong>Environmental Incident Category:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="geteievents">
										<cfif listfind(getincidentenv.EnvIncCat,EnvIncCateID) gt 0>#EnvIncCategory#</cfif>
										</cfoutput></td>
								<td class="formlable" align="left" width="25%"><strong>Does this incident relate to an environmental permit/licence/legislation?</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentenv.inBreach#</cfoutput></td>
						</tr>
						<cfif getincidentenv.inBreach eq "yes">
						<cfoutput>
						<tr>
								<td class="formlable" align="left" width="25%"><strong>Details of environmental permit/license/legislation breached:</strong></td>
								<td class="bodyTextGrey"  align="left" colspan="3" width="50%">#getincidentenv.BreachDetail#</td>
						</tr> 
						</cfoutput>
						</cfif>
						<cfif getincidentenv.EnvIncCat eq 1>
						<tr>
						<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Pollution Event Details</strong></td>
					</tr>
					
					<cfif getpollutionevents.recordcount gt 0>
					<cfoutput query="getpollutionevents">
					<tr id="polleventsrw_#currentrow#">
						<td colspan="4">
						<table width="100%" cellspacing="1" cellpadding="3" border="0">
					<tr>
								<td class="formlable" align="left" width="25%"><strong>Type of Pollution Event:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfloop query="getpollET"><cfif listfind(getpollutionevents.PollutionEventType,Eventid) gt 0>#PollutionEvent#</cfif>
										</cfloop></td>
								<td class="formlable" align="left" width="25%"><strong>Type of Substance:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfloop query="getpollsubstance">
										<cfif listfind(getpollutionevents.SubstanceType,SubstanceTypeID) gt 0>#SubstanceType#</cfif>
										</cfloop></td>
						</tr>
						<tr>
							<td class="formlable" align="left" width="25%"><strong>Quantity of Release:</strong></td>
								<td class="bodyTextGrey"  align="left">#getpollutionevents.Quantity#</td>
								<td class="formlable" align="left" width="25%"><strong>Substance Unit:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfloop query="getreleaseunit"><cfif listfind(getpollutionevents.SubstanceUnit,releaseunitid) gt 0>#releaseunit#</cfif>
										</cfloop></td>
						</tr>
						<tr>
								<td class="formlable" align="left" width="25%"><strong>Source of Release:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfloop query="getreleasesrc"><cfif listfind(getpollutionevents.ReleaseSource,ReleaseSourceid) gt 0>#ReleaseSource#</cfif>
										</cfloop></td>
								<td class="formlable" align="left" width="25%"><strong>Duration of Release:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfloop query="getreleaseduration"><cfif listfind(getpollutionevents.ReleaseDuration,ReleaseDurationid) gt 0>#ReleaseDuration#</cfif>
										</cfloop></td>
						</tr>
						<tr>
								<td class="formlable" align="left" width="25%"><strong>Receiving Environment:</strong></td>
								<td class="bodyTextGrey"  align="left" colspan="2"><cfloop query="getenviros"><cfif listfind(getpollutionevents.Environment,RecEnvID) gt 0>#ReceivingEnv#</cfif>
										</cfloop></td>
									<td class="bodyTextsm"></td>
								
						</tr>
						</table>
						</td>
						</tr>
					</cfoutput>
					</cfif>
					</cfif>
					<cfif getincidentenv.EnvIncCat eq 2>
						
						
						<tr>
						<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Non-Pollution Event Details</strong></td>
					</tr>
					<tr>
								<td class="formlable" align="left" width="25%"><strong>Details of non-conformance:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentenv.NonConformDetail#</cfoutput></td>
								<td class="formlable" align="left" width="25%"><strong>Was waste disposed of or managed incorrectly?</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentenv.WasteIncorrect#</cfoutput></td>
						</tr>
						</cfif>
					</table>
				</td>
			</tr>
		</table>
	 
	</td>
</tr>


<tr>
	<td id="propertfrm">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><strong class="bodyTextWhite">Asset Damage</strong></td>
			</tr>
			<tr>
				<td>
					<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
								<td class="formlable" align="left" width="25%"><strong>Source of Damage:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getassetdamagesrc">
										<cfif listfind(getincidentasset.DamageSource,DamageSourceid) gt 0>#DamageSource#</cfif>
										</cfoutput></td>
								<td class="formlable" align="left" width="25%"><strong>Nature of Damage:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getassetdamagenature">
										<cfif listfind(getincidentasset.DamageNature,DamageNatureid) gt 0>#DamageNature#</cfif>
										</cfoutput></td>
						</tr>
						<cfif trim(getincidentasset.OtherDamage) neq ''><tr><td></td><td></td>
			<td class="formlable" align="left"><strong>Other:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentasset.OtherDamage#</cfoutput></td>
		
		</tr></cfif>
			<!--- <tr>
			
				<td class="formlable" align="left"><strong>Dropped Object?</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentasset.DroppedObject#</cfoutput></td>
			<td></td>
			<td></td>
			</tr> --->
			<!--- <cfif getincidentasset.DroppedObject eq "Yes">
		<tr>	<cfoutput>
			<td class="formlable" align="left"><strong>Dropped Object Height (m):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentasset.DOheight#</td>
		<td class="formlable" align="left"><strong>Weight of Object (kg):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentasset.DOweight#</td>
		</tr>
		<tr>
			<td class="formlable" align="left"><strong>Dropped object energy (KJ):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentasset.DOenergy#</td>
		<td class="formlable" align="left"><strong>Dropped object category:</strong></td>	</cfoutput>
		<td class="bodyTextGrey"  align="left"><!--- <cfif getincidentasset.DOcategory eq "A">A - Minor
															<cfelseif getincidentasset.DOcategory eq "B">B - Important
															<cfelseif getincidentasset.DOcategory eq "C">C - High Potential
															<cfelseif getincidentasset.DOcategory eq "D">D - High Potential</cfif> --->
															</td>
		</tr>
		</cfif> --->
		<!--- <tr>
			<!--- <td class="bodyTextGrey" align="left"><strong>Dropped objects chart:</strong></td>  --->
			<td colspan="4" align="center" id="addochart"></td>
		</tr> --->
		
		<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Property Details</strong></td>
			</tr>
			
			
			
			
			<cfif getpropdamage.recordcount gt 0>
				<cfoutput query="getpropdamage">
				<tr id="propdamage_#currentrow#">
				<td colspan="4">
					<table width="100%" cellspacing="1" cellpadding="3" border="0">
			
							<tr>
								<td class="formlable" align="left" width="25%"><strong>Property Type:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%"><cfloop query="getpropertytypes"><cfif getpropdamage.PropertyType eq PropTypeID>#propertytype#</cfif>
										</cfloop></td>
								<td class="formlable" align="left" width="25%"><strong>Description of Property:</strong></td>
								<td class="bodyTextGrey"  align="left" width="25%">#getpropdamage.PropertyDesc#</td>
						</tr>
					<tr>
								<td class="formlable" align="left" width="25%"><strong>Extent of Damage:</strong></td>
								<td class="bodyTextGrey"  align="left" colspan="2"><cfloop query="getextents"><cfif getpropdamage.DamageExtent eq DamageExtentID>#DamageExtent#</cfif>
										</cfloop></td>
								<td width="25%"></td>
						</tr>
				
						</table>
						</td>
						</tr>
				</cfoutput>
			</cfif>
			
			
	<!--- 	<tr>
								<td class="formlable" align="left"><strong>Property Type:</strong></td>
								<td class="bodyTextGrey"  align="left">
										<cfoutput query="getpropertytypes">
										<cfif getincidentasset.PropertyType eq PropTypeID>#propertytype#</cfif>
										</cfoutput></td>
								<td class="formlable" align="left"><strong>Description of Property:</strong></td>
								<cfoutput><td class="bodyTextGrey"  align="left"><#getincidentasset.PropertyDesc#</td></cfoutput>
						</tr>
		<tr>
								<td class="formlable" align="left"><strong>Extent of Damage:</strong></td>
								<td class="bodyTextGrey"  align="left" colspan="3"><cfoutput query="getextents">
										<cfif getincidentasset.DamageExtent eq DamageExtentID>#DamageExtent#</cfif>
										</cfoutput></td>
								
						</tr> --->
				<tr>
				<td class="formlable" align="left"><strong>Estimated Cost of Damage:</strong></td>
								<cfoutput><td class="bodyTextGrey"  align="left">#getincidentasset.EstCost#</td></cfoutput>
								<td class="formlable" align="left"><strong>Currency:</strong></td>
								<td class="bodyTextGrey"  align="left">
										<cfoutput query="getcurrencies">
										<cfif getincidentasset.Currency eq CurrencyID>#CurrencyType#</cfif>
										</cfoutput></td>
								
						</tr>
					</table>
				</td>
			</tr>
			
		</table>
	
	</td>
</tr>

<tr>
	<td id="securityfrm">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><strong class="bodyTextWhite">Security Incident</strong></td>
			</tr>
			<tr>
				<td>
					<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
			<td class="formlable" align="left" width="25%"><strong>Security Incident Category:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getsecinccats">
										<cfif getincidentsecurity.SecIncCat eq SecIncCatID>#SecIncCategory#</cfif>
										</cfoutput></td>
				<td class="formlable" align="left" width="25%"><strong>Source of Security Threat:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%"><cfoutput query="getsecthreats">
										<cfif getincidentsecurity.SecSource eq SecurityHazardID>#SecurityHazard#</cfif>
										</cfoutput></td>
			
			</tr>
			<cfif getincidentsecurity.SecIncCat eq 1>
				<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Person</strong></td>
			</tr>
			<tr>
			<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Person:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getsecnature">
											<cfif naturetype eq "person">
												<cfif getincidentsecurity.PersonNature eq SecIncNatureID>#IncidentNature#</cfif>
											</cfif>
										</cfoutput></td>
										<cfif trim(getincidentsecurity.PersonOther) neq ''>
				<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Person - Other:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentsecurity.PersonOther#</cfoutput></td>
								</cfif>
			</tr>
			
			<tr>
			
				<td class="formlable" align="left"><strong>Did this incident occur whilst the individual was travelling abroad?</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentsecurity.OccurAbroad#</cfoutput></td>
		<cfif getincidentsecurity.OccurAbroad eq "yes">
			<td class="formlable" align="left"><strong>Which Country?</strong></td>
			<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getcountries">
											<cfif getincidentsecurity.CountryID eq countryid>#CountryName#</cfif>
										</cfoutput></td></cfif>
			</tr>
			</cfif>
			<cfif getincidentsecurity.SecIncCat eq 2>
			<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Business Asset</strong></td>
			</tr>
			<tr>
			<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Business Asset:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getsecnature">
											<cfif naturetype eq "business">
												<cfif getincidentsecurity.BusinessNature eq SecIncNatureID>#IncidentNature#</cfif>
											</cfif>
										</cfoutput></td>
										<cfif trim(getincidentsecurity.BusinessOther) neq ''>
				<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Business Asset - Other:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentsecurity.BusinessOther#</cfoutput></td>
								</cfif>
			</tr>
			</cfif>
			<cfif getincidentsecurity.SecIncCat eq 3>
			<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Information Security</strong></td>
			</tr>
			<tr>
			<td class="formlable" align="left" width="25%"><strong>Information Category:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getinfocats">
											<cfif getincidentsecurity.InfoCategory eq infocatid>#InfoCategory#</cfif>
										</cfoutput></td>
				<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Information Security:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getsecnature">
											<cfif naturetype eq "info">
												<cfif getincidentsecurity.InfoNature eq SecIncNatureID>#IncidentNature#</cfif>
											</cfif>
										</cfoutput></td>
			
			</tr>
				<cfif trim(getincidentsecurity.InfoOther) neq ''>
				<tr>
					<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Information Security - Other:</strong></td>
		<td class="bodyTextGrey"  align="left" colspan="3">	<cfoutput>#getincidentsecurity.InfoOther#</cfoutput></td>
					</tr>
					</cfif>
					</cfif>
					<cfif getincidentsecurity.SecIncCat eq 4>
					<tr>
				<td bgcolor="e8bdf9" colspan="4" align="center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Security Practice Breach</strong></td>
			</tr><!--- breachseverity1 breachseverity2 practicebreachtd 
					<tr>
						<td class="formlable" align="left"><strong>Severity of Security Practice Breach:</strong></td>
		<td class="bodyTextGrey"  align="left"><span id="practicebreachtd">Minor&nbsp;&nbsp;&nbsp;<input type="radio" name="breachseverity" id="breachseverity1" value="Minor" <cfif getincidentsecurity.BreachSeverity eq "Minor">checked</cfif> onclick="document.getElementById('practicebreachtd').style.border='0px solid ffffff';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Major&nbsp;&nbsp;&nbsp;
		<input type="radio" name="breachseverity" value="Major" <cfif getincidentsecurity.BreachSeverity eq "Major">checked</cfif> id="breachseverity2" onclick="document.getElementById('practicebreachtd').style.border='0px solid ffffff';"></span></td>
					<td class="formlable" align="left"><strong>Details of Breach:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput><textarea name="breachdetails" cols="45" rows="4" class="selectgen">#getincidentsecurity.BreachDetails#</textarea></cfoutput></td>
					</tr>
					 --->
					<tr>
			<td class="formlable" align="left" width="25%"><strong>Nature of Incident: Security Practice Breach:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">
										<cfoutput query="getsecnature">
											<cfif naturetype eq "security">
												<cfif getincidentsecurity.breachNature eq SecIncNatureID>#IncidentNature#</cfif>
											</cfif>
										</cfoutput></td>
										<cfif trim(getincidentsecurity.breachother) neq ''>
				<td class="formlable" align="left" width="25%"><strong>Other Details of Breach:</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%"><cfoutput>#getincidentsecurity.breachother#</cfoutput></td>
						</cfif>
			</tr>
				</cfif>
					<tr>
				<td bgcolor="e8bdf9" colspan="4" align=" center"><strong style="font-family:Segoe UI;font-size:10pt;color:5f2468;">Additional Information</strong></td>
			</tr>
					<tr>
						<td class="formlable" align="left"><strong>Did incident involve a weapon?</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentsecurity.WeaponInvolved#</cfoutput></td>
					<td class="formlable" align="left"><strong>What kind of weapon was included?</strong></td>
		<td class="bodyTextGrey"  align="left">
										<cfoutput query="getweapons"> <cfif getincidentsecurity.WeaponKind eq Weaponid>#Weapon#</cfif>
											
										</cfoutput></td>
					</tr>
					<tr>
						<td class="formlable" align="left"><strong>Did this incident result in an immediate financial cost to the business?</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentsecurity.ResultInCost#</cfoutput></td>
			<cfif getincidentsecurity.ResultInCost eq "yes">
					<td class="formlable" align="left"><strong>Estimated Cost of Loss:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfoutput>#getincidentsecurity.EstimateCost#</cfoutput></td>
		</cfif>
					</tr>
					<cfif getincidentsecurity.ResultInCost eq "yes">
					<tr>
				
								<td class="formlable" align="left"><strong>Currency:</strong></td>
								<td class="bodyTextGrey"  align="left" colspan="3">
										<cfoutput query="getcurrencies">
										<cfif getincidentsecurity.CurrencyID eq CurrencyID>#CurrencyType#</cfif>
										</cfoutput></td>
								
						</tr>
					</cfif>
					
					
					
					
					
					
					
					
					
					
					</table>
				</td>
			</tr>
		
		</table>
	
	</td>
</tr>
 
			<cfif getincidentdetails.droppedObject eq "Yes">
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><strong class="bodyTextWhite">Dropped Object</strong></td>
			</tr>
			<tr>
				<td>
				<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
			<cfoutput>
		<tr>
			<td class="formlable" align="left" width="25%"><strong>Dropped Object Height (m):</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">#getincidentdetails.DOheight#</td>
		<td class="formlable" align="left" width="25%"><strong>Weight of Object (kg):</strong></td>
		<td class="bodyTextGrey"  align="left" width="25%">#getincidentdetails.DOweight#</td>
		</tr></cfoutput>
		
		<tr><cfoutput>
			<td class="formlable" align="left"><strong>Dropped object energy (KJ):</strong></td>
		<td class="bodyTextGrey"  align="left">#getincidentdetails.DOenergy#</cfoutput>
		<td class="formlable" align="left"><strong>Dropped object category:</strong></td>
		<td class="bodyTextGrey"  align="left"><cfif getincidentdetails.DOcategory eq "A">A - Minor
															<cfelseif getincidentdetails.DOcategory eq "B">B - Important
															<cfelseif getincidentdetails.DOcategory eq "C">C - High Potential
															<cfelseif getincidentdetails.DOcategory eq "D">D - High Potential</cfif></td>
		</tr>
		
		
		<tr>
			<td colspan="4" align="center" id="dochart"></td>
		</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
</tr>
</cfif>
<cfif trim(getincidentdetails.reasonlate) neq ''>
<tr>
	<td id="laterep">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><strong class="bodyTextWhite">&nbsp;</strong></td>
			</tr>
			<tr>
				<td>
					<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
						<tr>
						<td class="formlable" align="left" width="25%"><strong>Reason for Late Recording:</strong></td>
						<td class="bodyTextGrey"  align="left" colspan="3"><cfoutput>#getincidentdetails.reasonlate#</cfoutput></td>
			</tr>
					</table>
				</td>
			</tr>
		</table>
	
	</td>
</tr> 
</cfif>
<cfif listfindnocase("Sent for Review",getincidentdetails.status) gt 0 and listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
			<cfif listfindnocase(request.userlevel,"Global Admin") gt 0>
			
		<cfoutput><tr>
		
		<td bgcolor="ffffff"><br>&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Review"  onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=review','Info','500','420','no');" >&nbsp;&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfo','Info','500','420','no');" >&nbsp;&nbsp;<br><br></td>
	</tr></cfoutput>
	<cfelseif  listfindnocase(request.userlevel,"BU Admin") gt 0>
	
			<cfif  listfindnocase(request.userbus,getincidentdetails.Business_Line_ID) gt 0>
		<cfoutput><tr>
		
		<td bgcolor="ffffff"><br>&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Review"  onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=review','Info','500','420','no');" >&nbsp;&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfo','Info','500','420','no');" >&nbsp;&nbsp;<br><br></td>
	</tr></cfoutput>
				</cfif>
			<cfelseif listfindnocase(request.userlevel,"OU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 >
		<cfif  listfindnocase(request.userous,getincidentdetails.ouid) gt 0>
	<cfoutput><tr>
		
		<td bgcolor="ffffff"><br>&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Review"  onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=review','Info','500','420','no');" >&nbsp;&nbsp;&nbsp;<input type="button" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfo','Info','500','420','no');" >&nbsp;&nbsp;<br><br></td>
	</tr></cfoutput>
	</cfif>
			</cfif>
</cfif>
<cfif getinccomments.recordcount gt 0>
<tr >
	<td><strong class="bodytextwhite">Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfoutput query="getinccomments">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
</cfif>
<cfif getincamendcomments.recordcount gt 0>
<tr >
	<td><strong class="bodytextwhite">Reason for Amendment</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Reason</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfoutput query="getincamendcomments">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
</cfif>
</table>

<script type="text/javascript">
	
	
	<cfif listfind("1,5",getincidentdetails.primarytype) gt 0 or  listfind("1,5",getincidentdetails.secondarytype) gt 0>
		<cfif getincidentdetails.primarytype eq 1 or getincidentdetails.secondarytype eq 1> 
			<cfif getincidentdetails.isnearmiss eq "yes">
			document.getElementById("injuryfrm").style.display='none';
			<cfelse>
			document.getElementById("injuryfrm").style.display='';
			</cfif>
		<cfelseif getincidentdetails.primarytype eq 5 or getincidentdetails.secondarytype eq 5>
			
				<cfif getincidentdetails.isnearmiss eq "yes">
				document.getElementById("injuryfrm").style.display='none';
				<cfelse>
				document.getElementById("injuryfrm").style.display='';
				</cfif>
			<cfif getincidentdetails.primarytype neq 1>
					<cfif getincidentdetails.secondarytype neq 1>
						<cfif getincidentdetails.oidefinition eq 2>
							document.getElementById("oshacatfldsel").style.display='none';
							document.getElementById("oshacatlblsel").style.display='none'; 
						</cfif>
					</cfif>
				</cfif>
		</cfif>
		// document.getElementById("injuryfrm").style.display='';
	<cfelse>
		document.getElementById("injuryfrm").style.display='none';
	</cfif>
	<cfif getincidentdetails.primarytype eq 2 or getincidentdetails.secondarytype eq 2>
		<cfif getincidentdetails.isnearmiss eq "yes">
		document.getElementById("envirofrm").style.display='none';
		<cfelse>
		document.getElementById("envirofrm").style.display='';
		</cfif>
	<cfelse>
		document.getElementById("envirofrm").style.display='none';
	</cfif>
	<cfif getincidentdetails.primarytype eq 3 or getincidentdetails.secondarytype eq 3>
		<cfif getincidentdetails.isnearmiss eq "yes">
		document.getElementById("propertfrm").style.display='none';
		<cfelse>
		document.getElementById("propertfrm").style.display='';
		</cfif>
	<cfelse>
		document.getElementById("propertfrm").style.display='none';
	</cfif>
	<cfif getincidentdetails.primarytype eq 4 or getincidentdetails.secondarytype eq 4>
		<cfif getincidentdetails.isnearmiss eq "yes">
		document.getElementById("securityfrm").style.display='none';
		<cfelse>
		document.getElementById("securityfrm").style.display='';
		</cfif>
	<cfelse>
		document.getElementById("securityfrm").style.display='none';
	</cfif>
	
	
</script>

<!--- </div> --->