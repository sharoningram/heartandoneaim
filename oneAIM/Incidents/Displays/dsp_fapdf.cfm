<cfparam name="faid" default="0">
<cfset FileDirFA = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_fapdf.cfm", "uploads\incidents\#irn#\FA"))>
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_fapdf.cfm", "reports\displays\pdfgen"))>
<cfset deldir = trim(replacenocase(getcurrenttemplatepath(), "dsp_fapdf.cfm", ""))>	
<cfset fnamepdf = "FA_#getincidentdetails.trackingnum#.pdf">
<cfif directoryexists(FileDirFA)>
	<cfdirectory action="LIST" directory="#FileDirFA#" name="getincfileslist">
</cfif>
<cfif not directoryexists("#PDFFileDir#")>
	<cfdirectory action="CREATE" directory="#PDFFileDir#">
</cfif>
<cfdocument format="PDF" filename="#PDFFileDir#\#fnamepdf#"  overwrite="yes"  orientation="portrait">
<style>
.purplebg {
background: #5f2468;

}
.formlable{
background-color:#f7f1f9;
font-family: Segoe UI;
	font-size: 10px;

	color: #5f5f5f;
	font-style: normal;
}
.bodyText {
	font-family: Segoe UI;
	font-size: 10px;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
.bodyTextGrey{
font-family: Segoe UI;
	font-size: 10px;
	/* color: #5f2468; */
	color: #5f5f5f;
	font-style: normal;
	}
.bodyTextWhite {
	font-family: Segoe UI;
	font-size: 10px;
	color: #FFFFFF;
	font-style: normal;
}
.bodyTextbldsm {
	font-family: Segoe UI;
	font-size: 10px;
	/* color: #5f2468; */
	color: #000000;
	font-style: bold;
}
.bodyTextsm {
	font-family: Segoe UI;
	font-size: 12px;
	/* color: #5f2468; */
	color: #000000;
	font-style: normal;
}
</style>


<cfset potcolor="ffffff">
<cfset txtcolor="000000">
<table cellpadding="2" cellspacing="1" border="0" class="purplebg" width="100%">
	<tr>
		<cfoutput><td class="bodyTextWhite"><strong>Incident Report - #getincidentdetails.trackingnum# - First Alert</strong></td></cfoutput>
	</tr>

	<tr>
		<td align="center" bgcolor="ffffff">
		<cfoutput>
	<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
	
	<cfif irn gt 0>
	<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Classification</strong></td>
		
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Primary Incident Type:</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidenttypeFA.IncType#</td>
		<td class="formlable" align="left" width="25%"><strong>Is This a Near Miss Incident?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.isNearMiss#</td>
	</tr>
	<tr>
		<cfif listfind("1,5",getincidentdetails.primarytype) gt 0>
		<td class="formlable" align="left" width="25%"><strong>OSHA Classification:</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.category#</td>
		</cfif>
		<td class="formlable" align="left" width="25%"><strong>Incident Potential Rating</strong></td>
		<td class="bodyTextGrey" width="25%" >
		<cfswitch expression="#getincidentdetails.PotentialRating#">
			<cfcase value="A1,B1,C1,D1,A2,B2,C2">
				<cfset potcolor="00b050">
				<cfset txtcolor="ffffff">
			</cfcase>
			<cfcase value="E1,D2,E2,A3,B3,C3,D3">
				<cfset potcolor="ffc000">
				<cfset txtcolor="000000">
			</cfcase>
			<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
				<cfset potcolor="ff0000">
				<cfset txtcolor="ffffff">
			</cfcase>
		</cfswitch>
		<span style="background-color:###potcolor#;color:###txtcolor#;padding-left:8px;padding-right:8px;padding-bottom:2px;">#getincidentdetails.PotentialRating#</span></td>
	</tr>
	<cfif listfind("1,2",getincidentdetails.primarytype) gt 0>
	<tr>
		<cfif getincidentdetails.primarytype eq 1>
		<td class="formlable" align="left" width="25%"><strong>Is injury classified as a serious incident?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.isserious#</td>
		</cfif>
		<cfif getincidentdetails.primarytype eq 2>
		<td class="formlable" align="left" width="25%"><strong>Details of environmental permit or license breached</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentenv.breachdetail#</td>
		</cfif>
	</tr>
	</cfif>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Has an enforcement notice been issued?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.EnforcementNotice#</td>
		<cfif getincidentdetails.EnforcementNotice eq "Yes">
		<td class="formlable" align="left" width="25%"><strong>Type of enforcement notice</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.EnforcementNoticeType#</td>
		</cfif>
	</tr>
		<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Location</strong></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.buname#</td>
		<td class="formlable" align="left" width="25%"><strong>#request.oulabellong#</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.ouname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Business Stream</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.bsname#</td>
		<td class="formlable" align="left" width="25%"><strong>Country</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.countryname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Project/Office</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.group_name#</td>
		<td class="formlable" align="left" width="25%"><strong>Site/Office name</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.sitename#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Specific location where incident occurred</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.Specificlocation#</td>
		<td align="left" width="25%"><strong></strong></td>
		<td class="bodyTextGrey" width="25%" ></td>
	</tr>
		<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Details</strong></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident Date</strong></td>
		<td class="bodyTextGrey" width="25%" >#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
		<td class="formlable" align="left" width="25%"><strong>Time incident occurred</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.incidenttime#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident assigned to</strong></td>
		<td class="bodyTextGrey" width="25%" colspan="3">#getincidenttypeFA.IncAssignedTo#</td>
		
	</tr>
	<cfif listfind("1,5",getincidentdetails.primarytype) gt 0>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Injury Type:</strong></td>
		<td class="bodyTextGrey" width="25%" ><cfif trim(getintype.InjuryType) eq ''>N/A<cfelse>#replace(valuelist(getintype.InjuryType),",",", ","all")#</cfif></td>
		<td class="formlable" align="left" width="25%"><strong>Body Part Affected:</strong></td>
		<td class="bodyTextGrey" width="25%" ><cfif getfabp.recordcount eq 0>N/A<cfelse>#replace(valuelist(getfabp.bodypart),",",", ","all")#</cfif></td>
	</tr>
	
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Gender</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.ipgender#</td>
		<td class="formlable" align="left" width="25%"><strong>Occupation</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.Occupation#</td>
	</tr>
	</cfif>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Description of incident</strong></td>
		<td class="bodyTextGrey" colspan="3">#getFAdetail.description#</td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Immediate action taken</strong></td>
		<td class="bodyTextGrey" colspan="3">#getincidentdetails.actiontaken#</td>
		
	</tr>
		<!--- <tr>
		<td class="formlable" align="left" width="25%"><strong>Proposed action to prevent reocurrence</strong></td>
		<td class="bodyTextGrey" width="25%" colspan="3">#getFAdetail.PreventAction#</td>
	</tr> --->
		
	
	<tr>
		<td class="formlable" align="left"><strong>Attach Photograph(s):</strong></td>
		<td class="bodyTextGrey" colspan="3">
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfloop query="getincfileslist">
						#name#<br>
					</cfloop>
				</cfif>
			</cfif>
		
</td>
	</tr>
		
		
			
<cfif getfldaudit.recordcount gt 0>
<cfset oshacatstruct = {}>
<cfloop query="getOSHAcats">
	<cfset oshacatstruct[catid] = category>
</cfloop>
<tr class="purplebg">
	<td colspan="4"><strong class="bodytextwhite">Field Updates</strong></td>
</tr>
<tr>
	<td colspan="4">
		<table bgcolor="ffffff" cellpadding="4" cellspacing="1" width="100%">
		<tr>
			<td class="formlable" width="20%"><strong>Field</strong></td>
			<td class="formlable" align="center" width="20%"><strong>Old Value</strong></td>
			<td class="formlable" align="center" width="20%"><strong>New Value</strong></td>
			<td class="formlable" width="20%"><strong>Changed By</strong></td>
			<td class="formlable" width="20%"><strong>Date Changed</strong></td>
		</tr> 
		
		<cfloop query="getfldaudit">
			<cfif fieldname eq "invlevel">
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey" >Investigation Level</td>
					<td class="bodyTextGrey"  align="center"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>Level #oldvalue#</cfif></td>
					<td class="bodyTextGrey"  align="center">Level #newvalue#</td>
					<td class="bodyTextGrey" >#enteredby#</td>
					<td class="bodyTextGrey" >#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			<cfelseif fieldname eq "oshacat">
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey" >OSHA Classification</td>
					<td class="bodyTextGrey"  align="center"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse><cfif structkeyexists(oshacatstruct,oldvalue)>#oshacatstruct[oldvalue]#</cfif></cfif></td>
					<td class="bodyTextGrey"  align="center"><cfif structkeyexists(oshacatstruct,newvalue)>#oshacatstruct[newvalue]#</cfif></td>
					<td class="bodyTextGrey" >#enteredby#</td>
					<td class="bodyTextGrey" >#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			<cfelseif fieldname eq "potrating">
			<cfset potcolor = "ffffff">
				<cfset pottxt = "000000">
				<cfswitch expression="#oldvalue#">
					<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<cfset potcolor="00b050">
						<cfset pottxt = "ffffff">
					</cfcase>
					<cfcase value="E1,D2,E2,A3,B3,C3,D3">
						<cfset potcolor="ffc000">
						<cfset pottxt = "000000">
					</cfcase>
					<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<cfset potcolor="ff0000">
						<cfset pottxt = "ffffff">
					</cfcase>
				</cfswitch>
				<cfset potcolor2 = "ffffff">
				<cfset pottxt2 = "000000">
				<cfswitch expression="#newvalue#">
					<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<cfset potcolor2="00b050">
						<cfset pottxt2 = "ffffff">
					</cfcase>
					<cfcase value="E1,D2,E2,A3,B3,C3,D3">
						<cfset potcolor2="ffc000">
						<cfset pottxt2 = "000000">
					</cfcase>
					<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<cfset potcolor2="ff0000">
						<cfset pottxt2 = "ffffff">
					</cfcase>
				</cfswitch>
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey" >Potential Rating</td>
					<td class="bodyTextGrey"  align="center"><span style="color:#pottxt#;background-color:#potcolor#;padding-left:8px;padding-right:8px;padding-bottom:2px;"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>#oldvalue#</cfif></span></td>
					<td class="bodyTextGrey" align="center"><span style="color:#pottxt2#;background-color:#potcolor2#;padding-left:8px;padding-right:8px;padding-bottom:2px;">#newvalue#</span></td>
					<td class="bodyTextGrey" >#enteredby#</td>
					<td class="bodyTextGrey" >#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			</cfif>
		</cfloop>
		</table>
	</td>
</tr>
</cfif>


<cfif getincamendcomments.recordcount gt 0>
<tr class="purplebg">
	<td colspan="4"><strong class="bodytextwhite">Reason for Amendment</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff" colspan="4">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Reason</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getincamendcomments">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
</cfif>
		
	</cfif>

	</table>
	</td></tr>
	
	</cfoutput>
</table>

</cfdocument>






<cfif directoryexists(FileDirFA)>
			<cfdirectory action="LIST" directory="#FileDirFA#" name="getincfileslist4pdffa">
					<cfset ctr = 0>
					<cfloop query="getincfileslist4pdffa">
						<cftry> 
						<cfset ctr = ctr+1>
						<cfif right(name,4) eq ".pdf">
							<cfpdf action="merge" destination="#PDFFileDir#\#fnamepdf#" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="#PDFFileDir#\#fnamepdf#">
    								<cfpdfparam source="#FileDirFA#\#name#">
 							</cfpdf>
						<cfelse>
							<cfif not isImageFile("#FileDirFA#\#name#")>
							<cfdocument 
    							 format="pdf" 
    							 srcfile="#FileDirFA#\#name#" 
    							 filename="attc_#getincidentdetails.trackingnum#_#ctr#.pdf"
	  							overwrite="yes"> 
 							</cfdocument> 
							<cfpdf action="merge" destination="#PDFFileDir#\#fnamepdf#" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="#PDFFileDir#\#fnamepdf#">
    								<cfpdfparam source="attc_#getincidentdetails.trackingnum#_#ctr#.pdf">
 							</cfpdf>
							<cffile action="DELETE" file="#deldir#attc_#getincidentdetails.trackingnum#_#ctr#.pdf">
							<cfelse>
							<cfdocument 
    							 format="pdf" 
    							filename="attc_#getincidentdetails.trackingnum#_#ctr#.pdf"
	  							overwrite="yes"><cfoutput><img src="#starthttp#://#http_host#/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/FA/#name#" border="0"></cfoutput></cfdocument>
								<cfpdf action="merge" destination="#PDFFileDir#\#fnamepdf#" overwrite="yes" keepBookmark="yes">
    								<cfpdfparam source="#PDFFileDir#\#fnamepdf#">
    								<cfpdfparam source="attc_#getincidentdetails.trackingnum#_#ctr#.pdf">
 							</cfpdf>
							<cffile action="DELETE" file="#deldir#attc_#getincidentdetails.trackingnum#_#ctr#.pdf"> 
							</cfif>
							
						</cfif>
							
						<cfcatch><cfset a = "yes"></cfcatch>
						</cftry> 
					</cfloop>
			
		
	</cfif>





<cflocation url="/#getappconfig.oneAIMpath#/reports/displays/pdfgen/#fnamepdf#" addtoken="No">