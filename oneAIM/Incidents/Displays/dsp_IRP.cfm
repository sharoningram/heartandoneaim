<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_IRP.cfm", "uploads\incidents\#irn#\IRP"))>
<cfset PDFFileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_IRP.cfm", "uploads\incidents\#irn#\IRPPDF"))>
<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" bgcolor="5f2468" width="100%">
	<tr>
		<td align="center" bgcolor="ffffff">
		
			<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
				
				<cfoutput>
				<cfif fileexists("#PDFFileDir#\IRP_#getincidentdetails.trackingnum#.pdf")>
				<tr>	
					<cfoutput><td colspan="4" align="right"><a href="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/irppdf/IRP_#getincidentdetails.trackingnum#.pdf" target="_blank"><img src="images/pdf.png" border="0"></a></td></cfoutput>
				</tr>
				</cfif>
				<tr>
					<td colspan="2" valign="top" width="50%"><img src="images/irpleft.jpg" border="0"></td>
					<td colspan="2" valign="top" align="right" width="50%"><img src="images/logor.jpg" border="0"></td>
				</tr>
				<tr>
					<td colspan="2" valign="top">
					<table cellpadding="3" cellspacing="0" border="0" width="100%">
							<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 10pt;color: ffffff;" >The incident</strong></td>
							</tr>
							<tr>
								<td class="bodytext" width="40%" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">#request.bulabellong#</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" width="60%">#getincidentdetails.buname#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">#request.oulabellong#</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidentdetails.ouname#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Project/Office</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidentdetails.group_name#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Incident Date</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Primary Incident Type</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidenttypeFA.IncType#</td>
							</tr>
							
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">OSHA Classification</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" ><cfif trim(getincidentoi.oshaclass) eq ''>N/A<cfelse><cfloop query="getoshacats">
										<cfif getincidentoi.oshaclass eq catid>#Category#<cfbreak></cfif>
										</cfloop></cfif></td>
							</tr>
							<cfset potcolor="ffffff">
							<cfset pottxt="000000">
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Potential Rating</td>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">
											<cfswitch expression="#getincidentdetails.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset pottxt="ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottxt="000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset pottxt="ffffff">
												</cfcase>
											</cfswitch>
											<span style="background-color:###potcolor#;color:###pottxt#;">#getincidentdetails.PotentialRating#</span></td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Is this a near miss?</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidentdetails.isnearmiss#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Incident Assigned To</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidenttypeFA.IncAssignedTo#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Description of Incident</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidentdetails.description#</td>
							</tr>
							<tr>
								<td colspan="2"><br></td>
							</tr>
						</table>
					
					</td>
					<td colspan="2" valign="top">
					<table cellpadding="3" cellspacing="0" border="0" width="100%">
							<tr class="purplebg" bgcolor="5f2468" >
								<td><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 10pt;color: ffffff;">Incident learning for the business</strong></td>
							</tr>
							<tr>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#replacenocase(getirpdetail.incidentlearning,"#chr(13)#","<br>","all")#</td>
							</tr>
							<tr>
								<td colspan="2"><br></td>
							</tr>
							<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 10pt;color: ffffff;">Supporting Information</strong></td>
							</tr>
							<tr>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" ><cfif trim(getirpdetail.supportinginfo) neq ''>#replacenocase(getirpdetail.supportinginfo,"#chr(13)#","<br>","all")#<cfelse>N/A</cfif></td>
							</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top">
					
					<table cellpadding="3" cellspacing="0" border="0" width="100%">
							<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 10pt;color: ffffff;">Investigation Findings</strong></td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Immediate Cause</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" ><cfif getincazirp.recordcount gt 0>
									
										<table cellpadding="1" cellspacing="0" border="0" width="100%">
											<cfloop query="getincazirp">
											<cfif aztype eq 1>
											<tr><td valign="top" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#CategoryLetter#&nbsp;#categoryname#:&nbsp;#CategoryLetter##FactorNumber#&nbsp;#FactorName#</td></tr>
											</cfif>
											</cfloop>
											</table>
											</cfif>							
								</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Root Cause</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" ><cfif getincazirp.recordcount gt 0>
									
										<table cellpadding="1" cellspacing="0" border="0" width="100%">
											<cfloop query="getincazirp">
											<cfif aztype eq 2>
											<tr><td valign="top" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#CategoryLetter#&nbsp;#categoryname#:&nbsp;#CategoryLetter##FactorNumber#&nbsp;#FactorName#</td></tr>
											</cfif>
											</cfloop>
											</table>
											</cfif>							
								</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 10pt;color: 000000;">Immediate Action Taken</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >#getincidentdetails.actiontaken#</td>
							</tr>
						</table>
					</td>
					<td colspan="2" valign="top">
					<table cellpadding="3" cellspacing="0" border="0" width="100%">
					<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 10pt;color: ffffff;">Supporting Documentation</strong></td>
							</tr>
							<tr>
				<td class="bodyTextGrey"  style="font-family: Segoe UI;font-size: 10pt;color: 5f5f5f;" >
			
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfloop query="getincfileslist">
						<cfif left(name,6) neq ".~lock">
						<cfif isImageFile("#filedir#\#name#")>
						<img src="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/IRP/#name#" ><br>
						<cfelse>
						<a href="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/IRP/#name#" target="_blank">#name#</a><br>
						</cfif>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
				
</td>
	</tr>
						</table>
					</td>
				</tr>
				
				<cfif getirpdetail.status eq "Sent for Review">
				<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
				<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
				<tr>
					<td colspan="4">&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Approve" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewIRP','Info','500','420','no');">&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoIRP','Info','500','420','no');" ></td>
				</tr>
				</cfif>
				</cfif>
				</cfif>
				</cfoutput>
			</table>
		
		</td>
	</tr>
		<cfif getirpcomments.recordcount gt 0>
<tr >
	<td><strong class="bodytextwhite">Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfoutput query="getirpcomments">
			<tr>
				<td class="bodytext">#Comment#</td>
				<td class="bodytext">#EnteredBy#</td>
				<td class="bodytext">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
</cfif>
</table>
