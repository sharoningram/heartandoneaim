<cfif audittype eq "invlevel">
<cflayout name="thelayout"  type="vbox">
            <cflayoutarea style="height:300;width:100%;" overflow="auto">
<table bgcolor="ffffff" cellpadding="4" cellspacing="1" width="100%">
 <tr>
	<td class="formlable"><strong>Field</strong></td>
	<td class="formlable"><strong>Old Value</strong></td>
	<td class="formlable"><strong>New Value</strong></td>
	<td class="formlable"><strong>Changed By</strong></td>
	<td class="formlable"><strong>Date Changed</strong></td>
</tr> 
<cfoutput query="getaudit">

<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
	<td class="bodyTextsm" >Investigation Level</td>
	<td class="bodyTextsm" ><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>Level #oldvalue#</cfif></td>
	<td class="bodyTextsm" >Level #newvalue#</td>
	<td class="bodyTextsm" >#enteredby#</td>
	<td class="bodyTextsm" >#dateformat(dateentered,sysdateformat)#</td>
</tr>

</cfoutput>
</table></cflayoutarea></cflayout>
		
<cfelseif audittype eq "potrating">
<cflayout name="thelayout"  type="vbox">
            <cflayoutarea style="height:300;width:100%;" overflow="auto">
<table bgcolor="ffffff" cellpadding="4" cellspacing="1" width="100%">
 <tr>
	<td class="formlable"><strong>Field</strong></td>
	<td class="formlable"><strong>Old Value</strong></td>
	<td class="formlable"><strong>New Value</strong></td>
	<td class="formlable"><strong>Changed By</strong></td>
	<td class="formlable"><strong>Date Changed</strong></td>
</tr> 
<cfoutput query="getaudit">
<cfset potcolor = "ffffff">
<cfset pottxt = "000000">
<cfswitch expression="#oldvalue#">
	<cfcase value="A1,B1,C1,D1,A2,B2,C2">
		<cfset potcolor="00b050">
		<cfset pottxt = "ffffff">
	</cfcase>
	<cfcase value="E1,D2,E2,A3,B3,C3,D3">
		<cfset potcolor="ffc000">
		<cfset pottxt = "000000">
	</cfcase>
	<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
		<cfset potcolor="ff0000">
		<cfset pottxt = "ffffff">
	</cfcase>
</cfswitch>
<cfset potcolor2 = "ffffff">
<cfset pottxt2 = "000000">
<cfswitch expression="#newvalue#">
	<cfcase value="A1,B1,C1,D1,A2,B2,C2">
		<cfset potcolor2="00b050">
		<cfset pottxt2 = "ffffff">
	</cfcase>
	<cfcase value="E1,D2,E2,A3,B3,C3,D3">
		<cfset potcolor2="ffc000">
		<cfset pottxt2 = "000000">
	</cfcase>
	<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
		<cfset potcolor2="ff0000">
		<cfset pottxt2 = "ffffff">
	</cfcase>
</cfswitch>
<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
	<td class="bodyTextsm" >Potential Rating</td>
	<td class="bodyTextsm" bgcolor="#potcolor#" align="center"><span style="color:#pottxt#;"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>#oldvalue#</cfif></span></td>
	<td class="bodyTextsm" bgcolor="#potcolor2#" align="center"><span style="color:#pottxt2#;">#newvalue#</span></td>
	<td class="bodyTextsm" >#enteredby#</td>
	<td class="bodyTextsm" >#dateformat(dateentered,sysdateformat)#</td>
</tr>

</cfoutput>
</table></cflayoutarea></cflayout>
		
<cfelseif audittype eq "oshacat">
<cflayout name="thelayout"  type="vbox">
            <cflayoutarea style="height:300;width:100%;" overflow="auto">
<table bgcolor="ffffff" cellpadding="4" cellspacing="1" width="100%">
 <tr>
	<td class="formlable"><strong>Field</strong></td>
	<td class="formlable"><strong>Old Value</strong></td>
	<td class="formlable"><strong>New Value</strong></td>
	<td class="formlable"><strong>Changed By</strong></td>
	<td class="formlable"><strong>Date Changed</strong></td>
</tr> 
<cfoutput query="getaudit">

<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
	<td class="bodyTextsm" >OSHA Classification</td>
	<td class="bodyTextsm" ><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>#oldvalue#</cfif></td>
	<td class="bodyTextsm" >#newvalue#</td>
	<td class="bodyTextsm" >#enteredby#</td>
	<td class="bodyTextsm" >#dateformat(dateentered,sysdateformat)#</td>
</tr>

</cfoutput>
</table></cflayoutarea></cflayout>
</cfif>

<p align="center" style="font-family:Segoe UI;font-size:10pt;"><a href="javascript:void(0);" onclick="window.close();">close</a></p>