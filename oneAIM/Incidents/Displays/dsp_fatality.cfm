
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">


	<tr>
		<td align="center" bgcolor="ffffff">
		<cfoutput>
	<table cellpadding="4" cellspacing="1" border="0" width="100%" bgcolor="ffffff">
	
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident Report Number:</strong></td>
		<td class="bodyTextGrey" width="25%" colspan="3">#getincidentdetails.trackingnum#</td>
	
	</tr>
	
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Originator:</strong></td>
		<td class="bodyTextGrey" width="25%">#getincidentdetails.createdby#</td>
		
		<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#:</strong></td>
		<td class="bodyTextGrey" width="25%" id="BUTD">#getincidentdetails.buname#</td>
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Date Created:</strong></td>
		<td class="bodyTextGrey" width="25%">#dateformat(getincidentdetails.datecreated,sysdateformat)#</td>
		<td class="formlable" align="left" width="25%"><strong>#request.oulabellong#:</strong></td>
		<td class="bodyTextGrey" width="25%" id="OUTD">#getincidentdetails.ouname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Project/Office:*</strong></td>
		<td class="bodyTextGrey" width="25%"><cfloop query="getgroups">
										<cfif getincidentdetails.groupnumber eq group_number>#group_name#</cfif>
										</cfloop></td>
		<td class="formlable" align="left" width="25%"><strong>Business Stream:</strong></td>
		<td class="bodyTextGrey" width="25%" id="BSTD"><cfif trim(getincidentdetails.bsname) neq ''>#getincidentdetails.bsname#<cfelse>Not Applicable</cfif></td>
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Site/Office Name:</strong></td>
		<td class="bodyTextGrey" width="25%"  id="SOTD">#getincidentdetails.sitename#</td>
		<td class="formlable" align="left" width="25%"><strong>Country:</strong></td>
		<td class="bodyTextGrey" width="25%" id="CNTYTD">#getincidentdetails.CountryName#</td>
	</tr>
	<tr>
		<td class="formlable" align="left"><strong>Is This Incident Work Related?*</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.isworkrelated#</td>
									
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Potential Rating:*</strong></td><!--- this.style.border='1px solid 5f2468'; this.style.border='1px solid 5f2468';--->
		<td class="bodyTextGrey" width="25%"><span style="background-Color:ff0000;color:ffffff;">#getincidentdetails.potentialrating#</span></td>
		<td class="formlable" align="left" width="25%"></td>
		<td class="bodyTextGrey" width="25%"></td>
	</tr>
	

	
	<tr>
		<td class="formlable" align="left"><strong>Incident Date:*</strong></td>
		<td class="bodyTextGrey">#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
		<td class="formlable" align="left"><strong>Time Incident Occurred:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.incidenttime#</td>
	</tr>


	<tr>
		<td class="formlable" align="left"><strong>Incident Assigned To:*</strong></td>
		<td class="bodyTextGrey"><cfloop query="getincassignto"><cfif getincidentdetails.incassignedto eq IncAssignedID>#IncAssignedTo#</cfif></cfloop></td>
		<cfif trim(getincidentdetails.OtherAssignedTo) neq ''>
				<td class="formlable" align="left" id="otherialable"><strong class="formlable">Other Incident Assigned To:&nbsp;&nbsp;</strong></td>
				<td class="bodyTextGrey" id="otheriafld">#getincidentdetails.OtherAssignedTo#</td>
		<cfelse>
			<td></td>
			<td></td>
		</cfif>
	</tr>

	<tr>
		<td class="formlable" align="left"><strong>Incident Reported By:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.reportedby#</td>
	<td class="formlable" align="left"><strong>#request.bulabelshort# HSSE Director:*</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.HSSEBUDirector#</td>
		
	</tr>
	

		<tr>
		<td class="formlable" align="left"><strong>HSSE Advisor:*</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.hsseadvisor#</td>
		<td class="formlable" align="left"><strong>HSSE Manager:*</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.hssemanager#</td>
	</tr>
	
<tr>
		<td class="formlable" align="left"><strong>Reported to Statutory Body?*</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.wasreported#</td>
	<td></td><td></td>
		
	</tr>
	<cfif getincidentdetails.wasreported eq "yes">
	<tr id="statbodyrow">
		<td class="formlable" align="left"><strong>Statutory Body Reported To:</strong></td>
		<td class="bodyTextGrey">#getincidentdetails.StatutoryBody#</td>
		<td class="formlable" align="left"><strong>Date Reported to Statutory Body:</strong></td>
		<td class="bodyTextGrey">#dateformat(getincidentdetails.datestatreported,sysdateformat)#</td>
		
	</tr>
	</cfif>
	</cfoutput>

	
	<cfoutput>
	
	<Cfif getteammembers.recordcount gt 0>
				
				<Cfloop query="getteammembers">
				<tr id="tmrow_#currentrow#">
					<td class="formlable" align="left" width="25%"><strong>Team Member:</strong></td>
					<td class="bodyTextGrey" width="25%">#MemberName#</td>
					<td class="formlable" align="left" width="25%"><strong>Relevance to Investigation:</strong></td>
					<td class="bodyTextGrey" width="25%">#memberrelevance#</td>
				</tr>
				
				</cfloop>
				
				</cfif>
				<cfif listfindnocase("Sent for Review",getincidentdetails.status) gt 0>
				<cfif listfindnocase(request.userlevel,"Senior Reviewer") gt 0 or  listfindnocase(request.userlevel,"Global Admin") gt 0>
		<tr>
			<td colspan="4">&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Approve Report" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewfat','Info','500','420','no');">&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfofat','Info','500','420','no');" ></td>
		</tr>
				</cfif>
				</cfif>
</cfoutput>
	
</table>
</td></tr>

<cfif getinccomments.recordcount gt 0>
<tr >
	<td><strong class="bodytextwhite">Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfoutput query="getinccomments">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfoutput>
		</table>
	</td>
</tr>
</cfif>
</table>