<cfparam name="faid" default="0">
<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_firstalert.cfm", "uploads\incidents\#irn#\FA"))>
<cfif directoryexists(filedir)>
			<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">
</cfif>


<cfset potcolor="ffffff">
<cfset txtcolor="000000">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">


	<tr>
		<td align="center" bgcolor="ffffff">
		<cfoutput>
	<table cellpadding="4" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
	<cfif getFAdetail.status eq "Approved">
		<tr>
			<td colspan="4" align="right">
						<a href="index.cfm?fuseaction=incidents.fapdf&irn=#irn#" target="_blank" class="bodytextsmpurple"><img src="images/pdf.png" border="0"></a></td>
		</tr>
	</cfif>
	<cfif irn gt 0>
	<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Classification</strong></td>
		
	</tr>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Primary Incident Type:</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidenttypeFA.IncType#</td>
		<td class="formlable" align="left" width="25%"><strong>Is This a Near Miss Incident?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.isNearMiss#</td>
	</tr>
	<tr>
		<cfif listfind("1,5",getincidentdetails.primarytype) gt 0>
		<td class="formlable" align="left" width="25%"><strong>OSHA Classification:</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.category#</td>
		</cfif>
		<td class="formlable" align="left" width="25%"><strong>Incident Potential Rating</strong></td>
		<td class="bodyTextGrey" width="25%" >
		<cfswitch expression="#getincidentdetails.PotentialRating#">
			<cfcase value="A1,B1,C1,D1,A2,B2,C2">
				<cfset potcolor="00b050">
				<cfset txtcolor="ffffff">
			</cfcase>
			<cfcase value="E1,D2,E2,A3,B3,C3,D3">
				<cfset potcolor="ffc000">
				<cfset txtcolor="000000">
			</cfcase>
			<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
				<cfset potcolor="ff0000">
				<cfset txtcolor="ffffff">
			</cfcase>
		</cfswitch>
		<span style="background-color:###potcolor#;color:###txtcolor#;padding-left:8px;padding-right:8px;padding-bottom:2px;">#getincidentdetails.PotentialRating#</span></td>
	</tr>
	<cfif listfind("1,2",getincidentdetails.primarytype) gt 0>
	<tr>
		<cfif getincidentdetails.primarytype eq 1>
		<td class="formlable" align="left" width="25%"><strong>Is injury classified as a serious incident?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.isserious#</td>
		</cfif>
		<cfif getincidentdetails.primarytype eq 2>
		<td class="formlable" align="left" width="25%"><strong>Details of environmental permit or license breached</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentenv.breachdetail#</td>
		</cfif>
	</tr>
	</cfif>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Has an enforcement notice been issued?</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.EnforcementNotice#</td>
		<cfif getincidentdetails.EnforcementNotice eq "Yes">
		<td class="formlable" align="left" width="25%"><strong>Type of enforcement notice</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.EnforcementNoticeType#</td>
		</cfif>
	</tr>
		<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Location</strong></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>#request.bulabellong#</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.buname#</td>
		<td class="formlable" align="left" width="25%"><strong>#request.oulabellong#</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.ouname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Business Stream</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.bsname#</td>
		<td class="formlable" align="left" width="25%"><strong>Country</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.countryname#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Project/Office</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.group_name#</td>
		<td class="formlable" align="left" width="25%"><strong>Site/Office name</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.sitename#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Specific location where incident occurred</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.Specificlocation#</td>
		<td align="left" width="25%"><strong></strong></td>
		<td class="bodyTextGrey" width="25%" ></td>
	</tr>
		<tr>
		<td class="purplebg" align="left" colspan="4"><strong class="bodyTextWhite">Incident Details</strong></td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident Date</strong></td>
		<td class="bodyTextGrey" width="25%" >#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
		<td class="formlable" align="left" width="25%"><strong>Time incident occurred</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentdetails.incidenttime#</td>
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Incident assigned to</strong></td>
		<td class="bodyTextGrey" width="25%" colspan="3">#getincidenttypeFA.IncAssignedTo#</td>
		
	</tr>
	<cfif listfind("1,5",getincidentdetails.primarytype) gt 0>
	<tr>
		<td class="formlable" align="left" width="25%"><strong>Injury Type:</strong></td>
		<td class="bodyTextGrey" width="25%" ><cfif trim(getintype.InjuryType) eq ''>N/A<cfelse>#replace(valuelist(getintype.InjuryType),",",", ","all")#</cfif></td>
		<td class="formlable" align="left" width="25%"><strong>Body Part Affected:</strong></td>
		<td class="bodyTextGrey" width="25%" ><cfif getfabp.recordcount eq 0>N/A<cfelse>#replace(valuelist(getfabp.bodypart),",",", ","all")#</cfif></td>
	</tr>
	
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Gender</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.ipgender#</td>
		<td class="formlable" align="left" width="25%"><strong>Occupation</strong></td>
		<td class="bodyTextGrey" width="25%" >#getincidentoi.Occupation#</td>
	</tr>
	</cfif>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Description of incident</strong></td>
		<td class="bodyTextGrey" colspan="3">#getFAdetail.description#</td>
		
	</tr>
		<tr>
		<td class="formlable" align="left" width="25%"><strong>Immediate action taken</strong></td>
		<td class="bodyTextGrey" colspan="3">#getincidentdetails.actiontaken#</td>
		
	</tr>
		<!--- <tr>
		<td class="formlable" align="left" width="25%"><strong>Proposed action to prevent reocurrence</strong></td>
		<td class="bodyTextGrey" width="25%" colspan="3">#getFAdetail.PreventAction#</td>
	</tr> --->
		
	
	<tr>
		<td class="formlable" align="left"><strong>Attach Photograph(s):</strong></td>
		<td class="bodyTextGrey" colspan="3">
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfloop query="getincfileslist">
						<a href="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/FA/#name#" target="_blank">#name#</a><br>
					</cfloop>
				</cfif>
			</cfif>
		
</td>
	</tr>
		
		
		
<cfif getfldaudit.recordcount gt 0>
<cfset oshacatstruct = {}>
<cfloop query="getOSHAcats">
	<cfset oshacatstruct[catid] = category>
</cfloop>
<tr class="purplebg">
	<td colspan="4"><strong class="bodytextwhite">Field Updates</strong></td>
</tr>
<tr>
	<td colspan="4">
		<table bgcolor="ffffff" cellpadding="4" cellspacing="1" width="100%">
		<tr>
			<td class="formlable" width="20%"><strong>Field</strong></td>
			<td class="formlable" align="center" width="20%"><strong>Old Value</strong></td>
			<td class="formlable" align="center" width="20%"><strong>New Value</strong></td>
			<td class="formlable" width="20%"><strong>Changed By</strong></td>
			<td class="formlable" width="20%"><strong>Date Changed</strong></td>
		</tr> 
		
		<cfloop query="getfldaudit">
			<cfif fieldname eq "invlevel">
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey" >Investigation Level</td>
					<td class="bodyTextGrey"  align="center"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>Level #oldvalue#</cfif></td>
					<td class="bodyTextGrey"  align="center">Level #newvalue#</td>
					<td class="bodyTextGrey" >#enteredby#</td>
					<td class="bodyTextGrey" >#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			<cfelseif fieldname eq "oshacat">
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey" >OSHA Classification</td>
					<td class="bodyTextGrey"  align="center"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse><cfif structkeyexists(oshacatstruct,oldvalue)>#oshacatstruct[oldvalue]#</cfif></cfif></td>
					<td class="bodyTextGrey"  align="center"><cfif structkeyexists(oshacatstruct,newvalue)>#oshacatstruct[newvalue]#</cfif></td>
					<td class="bodyTextGrey" >#enteredby#</td>
					<td class="bodyTextGrey" >#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			<cfelseif fieldname eq "potrating">
			<cfset potcolor = "ffffff">
				<cfset pottxt = "000000">
				<cfswitch expression="#oldvalue#">
					<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<cfset potcolor="00b050">
						<cfset pottxt = "ffffff">
					</cfcase>
					<cfcase value="E1,D2,E2,A3,B3,C3,D3">
						<cfset potcolor="ffc000">
						<cfset pottxt = "000000">
					</cfcase>
					<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<cfset potcolor="ff0000">
						<cfset pottxt = "ffffff">
					</cfcase>
				</cfswitch>
				<cfset potcolor2 = "ffffff">
				<cfset pottxt2 = "000000">
				<cfswitch expression="#newvalue#">
					<cfcase value="A1,B1,C1,D1,A2,B2,C2">
						<cfset potcolor2="00b050">
						<cfset pottxt2 = "ffffff">
					</cfcase>
					<cfcase value="E1,D2,E2,A3,B3,C3,D3">
						<cfset potcolor2="ffc000">
						<cfset pottxt2 = "000000">
					</cfcase>
					<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
						<cfset potcolor2="ff0000">
						<cfset pottxt2 = "ffffff">
					</cfcase>
				</cfswitch>
				<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
					<td class="bodyTextGrey" >Potential Rating</td>
					<td class="bodyTextGrey"  align="center"><span style="color:#pottxt#;background-color:#potcolor#;padding-left:8px;padding-right:8px;padding-bottom:2px;"><cfif trim(oldvalue) eq '' or oldvalue eq 0>Not Selected<cfelse>#oldvalue#</cfif></span></td>
					<td class="bodyTextGrey" align="center"><span style="color:#pottxt2#;background-color:#potcolor2#;padding-left:8px;padding-right:8px;padding-bottom:2px;">#newvalue#</span></td>
					<td class="bodyTextGrey" >#enteredby#</td>
					<td class="bodyTextGrey" >#dateformat(dateentered,sysdateformat)#</td>
				</tr>
			</cfif>
		</cfloop>
		</table>
	</td>
</tr>
</cfif>


<cfif getincamendcomments.recordcount gt 0>
<tr class="purplebg">
	<td colspan="4"><strong class="bodytextwhite">Reason for Amendment</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff" colspan="4">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Reason</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getincamendcomments">
			<tr>
				<td class="bodyTextGrey">#Comment#</td>
				<td class="bodyTextGrey">#EnteredBy#</td>
				<td class="bodyTextGrey">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
</cfif>
		
	</cfif>
<cfif trim(getFAdetail.status) eq "Sent for Review">
<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Reviewer") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
<tr>
	<td colspan="4">
		&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Review" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=reviewFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');">&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" >
	</td>
</tr>
</cfif>
</cfif>
<cfelseif trim(getFAdetail.status) eq "Sent for Approval">
<cfif listfind("1,2",getincidentdetails.withdrawnstatus) eq 0>
<cfif listfindnocase(request.userlevel,"Global Admin") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"Senior Reviewer") gt 0>
<tr>
	<td colspan="4">
&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Review" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=SRreviewFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" >&nbsp;&nbsp;&nbsp;<input type="submit" class="selectGenBTN" value="Request More Info" onclick="NewWindow('#self#?fuseaction=incidents.reviewirn&irn=#irn#&action=requestinfoFA&fa=#getFAdetail.FirstAlertID#','Info','500','420','no');" >
	</td>
</tr>
</cfif>
</cfif>
</cfif>
	</table>
	</td></tr>
	<cfif getfacomments.recordcount gt 0>
<tr >
	<td><strong class="bodytextwhite">Comments</strong></td>
</tr>
<tr>
	<td bgcolor="ffffff">
		<table cellpadding="3" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="formlable"><strong>Comment</strong></td>
				<td class="formlable"><strong>Entered By</strong></td>
				<td class="formlable"><strong>Date Entered</strong></td>
			</tr>
			<cfloop query="getfacomments">
			<tr>
				<td class="bodytext">#Comment#</td>
				<td class="bodytext">#EnteredBy#</td>
				<td class="bodytext">#dateTimeFormat(dateentered,sysdatetimeformat)#</td>
			</tr>
			</cfloop>
		</table>
	</td>
</tr>
</cfif>

	</cfoutput>
</table>

