<cfset FileDir = trim(replacenocase(getcurrenttemplatepath(), "incidents\displays\dsp_IRPpdf.cfm", "uploads\incidents\#irn#\IRP"))>
<cfdirectory action="LIST" directory="#filedir#" name="getincfileslist">

<table cellpadding="0" cellspacing="1" border="0" class="purplebg" bgcolor="5f2468" width="900">
	<tr>
		<td align="center" bgcolor="ffffff">
		
			<table cellpadding="2" cellspacing="1" border="0" bgcolor="ffffff" width="900">
				
				<cfoutput>
				<tr>
					<td colspan="2" valign="top" width="440"><img src="images/irpleft.jpg" border="0"></td>
					<td colspan="2" valign="top" align="right" width="440"><img src="images/logor.jpg" border="0"></td>
				</tr>
				<tr>
					<td colspan="2" valign="top">
						<table cellpadding="5" cellspacing="0" border="0" width="440">
							<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;" >The incident</strong></td>
							</tr>
							<tr>
								<td class="bodytext" width="40%" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#request.bulabellong#</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" width="60%">#getincidentdetails.buname#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">#request.oulabellong#</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidentdetails.ouname#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Project/Office</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidentdetails.group_name#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Incident Date</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#dateformat(getincidentdetails.incidentdate,sysdateformat)#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Primary Incident Type</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidenttypeFA.IncType#</td>
							</tr>
							
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">OSHA Classification</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" ><cfif trim(getincidentoi.oshaclass) eq ''>N/A<cfelse><cfloop query="getoshacats">
										<cfif getincidentoi.oshaclass eq catid>#Category#<cfbreak></cfif>
										</cfloop></cfif></td>
							</tr>
							<cfset potcolor="ffffff">
							<cfset pottxt="000000">
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Potential Rating</td>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">
											<cfswitch expression="#getincidentdetails.PotentialRating#">
												<cfcase value="A1,B1,C1,D1,A2,B2,C2">
													<cfset potcolor="00b050">
													<cfset pottxt="ffffff">
												</cfcase>
												<cfcase value="E1,D2,E2,A3,B3,C3,D3">
													<cfset potcolor="ffc000">
													<cfset pottxt="000000">
												</cfcase>
												<cfcase value="E3,A4,B4,C4,D4,E4,A5,B5,C5,D5,E5">
													<cfset potcolor="ff0000">
													<cfset pottxt="ffffff">
												</cfcase>
											</cfswitch>
											<span style="background-color:#potcolor#;color:#pottxt#;">#getincidentdetails.PotentialRating#</span></td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Is this a near miss?</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidentdetails.isnearmiss#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Incident Assigned To</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidenttypeFA.IncAssignedTo#</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Description of Incident</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidentdetails.description#</td>
							</tr>
							<tr>
								<td colspan="2"><br></td>
							</tr>
						<!--- </table>
						<br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%"> --->
							<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Investigation Findings</strong></td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Immediate Cause</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" ><cfif getincazirp.recordcount gt 0>
									
										<table cellpadding="1" cellspacing="0" border="0" width="100%">
											<cfloop query="getincazirp">
											<cfif aztype eq 1>
											<tr><td valign="top" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#CategoryLetter#&nbsp;#categoryname#:&nbsp;#CategoryLetter##FactorNumber#&nbsp;#FactorName#</td></tr>
											</cfif>
											</cfloop>
											</table>
											</cfif>							
								</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Root Cause</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" ><cfif getincazirp.recordcount gt 0>
									
										<table cellpadding="1" cellspacing="0" border="0" width="100%">
											<cfloop query="getincazirp">
											<cfif aztype eq 2>
											<tr><td valign="top" class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#CategoryLetter#&nbsp;#categoryname#:&nbsp;#CategoryLetter##FactorNumber#&nbsp;#FactorName#</td></tr>
											</cfif>
											</cfloop>
											</table>
											</cfif>							
								</td>
							</tr>
							<tr>
								<td class="bodytext" style="font-family: Segoe UI;font-size: 9pt;color: 000000;">Immediate Action Taken</td>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#getincidentdetails.actiontaken#</td>
							</tr>
						</table>
					</td>
					<td colspan="2" valign="top">
					
						<table cellpadding="5" cellspacing="0" border="0" width="440">
							<tr class="purplebg" bgcolor="5f2468" >
								<td><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Incident learning for the business</strong></td>
							</tr>
							<tr>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#replacenocase(getirpdetail.incidentlearning,"#chr(13)#","<br>","all")#</td>
							</tr>
							<tr>
								<td colspan="2"><br></td>
							</tr>
							<tr class="purplebg" bgcolor="5f2468" >
								<td colspan="2"><strong class="bodytextwhite" style="font-family: Segoe UI;font-size: 9pt;color: ffffff;">Insert Images/Supporting Information</strong></td>
							</tr>
							<tr>
								<td class="bodyTextGrey" style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >#replacenocase(getirpdetail.supportinginfo,"#chr(13)#","<br>","all")#</td>
							</tr>
							<tr>
				<td class="bodyTextGrey"  style="font-family: Segoe UI;font-size: 9pt;color: 5f5f5f;" >
			
			<cfif isdefined("getincfileslist.recordcount")>
				<cfif getincfileslist.recordcount gt 0>
					<cfloop query="getincfileslist">
						<cfif isImageFile("#filedir#\#name#")>
						<img src="/#getappconfig.oneAIMpath#/uploads/incidents/#irn#/IRP/#name#" ><br>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
				
</td>
	</tr>
						</table>
						
					</td>
				</tr>
				
				
				</cfoutput>
			</table>
		
		</td>
	</tr>
</table>



