<p>
<cfquery name="chart1" datasource="safety_34">
SELECT        Groups.Group_Number, Groups.Group_Name, First_Incident_Notice.Injury_Illness, First_Incident_Notice.Vehicle_Damage, First_Incident_Notice.Property_Damage, 
                         First_Incident_Notice.Fire_Explosion, First_Incident_Notice.Spill_Release, First_Incident_Notice.Permit_Exceedence, First_Incident_Notice.High_Loss_Near_miss, 
                         First_Incident_Notice.Third_Party_Inspections, First_Incident_Notice.Substance_Abuse, First_Incident_Notice.Security, First_Incident_Notice.Other
FROM            First_Incident_Notice INNER JOIN
                         Groups ON First_Incident_Notice.Group_Number = Groups.Group_Number
WHERE        (First_Incident_Notice.TrackingDate = 2014) and groups.business_line_id = 1
ORDER BY Groups.Business_Line_ID, Groups.Group_Name
</cfquery>


<cfset catlist = "">
<cfset greenseries = "">
<cfset yellowseries = "">
<cfset redseries = "">

<cfset greenstruct = structnew()>
<cfset yellowstruct = structnew()>
<cfset redstruct = structnew()>

<cfoutput query="chart1" group="Group_Number">
	<cfset greencnt = 0>
	<cfset redcnt = 0>
	<cfset yellowcnt = 0>
		<cfoutput>
		<cfif left(trim(Injury_Illness),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		
		
		<cfif left(trim(Injury_Illness),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		
	
		<cfif left(trim(Injury_Illness),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		</cfoutput>
		
		<cfif not structkeyexists(greenstruct,group_number)>
			<cfset greenstruct[group_number] = greencnt>
		</cfif>
		<cfif not structkeyexists(yellowstruct,group_number)>
			<cfset yellowstruct[group_number] = yellowcnt>
		</cfif>
		<cfif not structkeyexists(redstruct,group_number)>
			<cfset redstruct[group_number] = redcnt>
		</cfif>
		
		
		  <cfset newname = replace(group_name,"""","","all")>
		  <cfset newname = replace(newname,"'","","all")>
		  <cfset catlist = catlist & "<category label='#trim(newname)#' />">
		
	
		<cfset greenseries = greenseries & "<set value='#greencnt#' />">
		 <cfset yellowseries = yellowseries & "<set value='#yellowcnt#' />">
		 <cfset redseries = redseries & "<set value='#redcnt#' />">
		</cfoutput>
<div class="content1of4"><table cellpadding="3" cellspacing="0" align="center"><tr><td><cfchart format="html" chartheight="300" chartwidth="296" scalefrom="0" scaleto="20"  show3d="yes" yaxistitle="## of incidents" xaxistitle="Location" gridlines="6" seriesplacement="cluster" font="Segoe UI" fontsize="10" databackgroundcolor="silver" showygridlines="Yes" foregroundcolor="0f0f0f" showlegend="false" title="2014">
	
	<cfchartseries type="bar" serieslabel="Minor" seriescolor="##00ff00">
	<cfoutput query="chart1" group="Group_Number">
		<cfif structkeyexists(greenstruct,group_number)>
			<cfchartdata item="#group_name#" value="#greenstruct[group_number]#">
		</cfif>
	
		</cfoutput> 
	</cfchartseries>
	<cfchartseries type="bar" serieslabel="Important" seriescolor="yellow" >
	<cfoutput query="chart1" group="Group_Number">
		<cfif structkeyexists(yellowstruct,group_number)>
			<cfchartdata item="#group_name#" value="#yellowstruct[group_number]#">
		</cfif>
	
	</cfoutput>
	</cfchartseries>
	
	<cfchartseries type="bar" serieslabel="Critical"  seriescolor="red" >
	<cfoutput query="chart1" group="Group_Number">
		<cfif structkeyexists(redstruct,group_number)>
			<cfchartdata item="#group_name#" value="#redstruct[group_number]#">
		</cfif>
	
		</cfoutput>
	
	</cfchartseries>
	
</cfchart></td></tr></table>


</div>

<cfquery name="chart2" datasource="safety_34">
SELECT        Groups.Group_Number, Groups.Group_Name, First_Incident_Notice.Injury_Illness, First_Incident_Notice.Vehicle_Damage, First_Incident_Notice.Property_Damage, 
                         First_Incident_Notice.Fire_Explosion, First_Incident_Notice.Spill_Release, First_Incident_Notice.Permit_Exceedence, First_Incident_Notice.High_Loss_Near_miss, 
                         First_Incident_Notice.Third_Party_Inspections, First_Incident_Notice.Substance_Abuse, First_Incident_Notice.Security, First_Incident_Notice.Other
FROM            First_Incident_Notice INNER JOIN
                         Groups ON First_Incident_Notice.Group_Number = Groups.Group_Number
WHERE        (First_Incident_Notice.TrackingDate = 2013) and groups.business_line_id = 1
ORDER BY Groups.Business_Line_ID, Groups.Group_Name
</cfquery>


<cfset catlist = "">
<cfset greenseries = "">
<cfset yellowseries = "">
<cfset redseries = "">

<cfset greenstruct = structnew()>
<cfset yellowstruct = structnew()>
<cfset redstruct = structnew()>

<cfoutput query="chart2" group="Group_Number">
	<cfset greencnt = 0>
	<cfset redcnt = 0>
	<cfset yellowcnt = 0>
		<cfoutput>
		<cfif left(trim(Injury_Illness),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		
		
		<cfif left(trim(Injury_Illness),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		
	
		<cfif left(trim(Injury_Illness),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		</cfoutput>
		
		<cfif not structkeyexists(greenstruct,group_number)>
			<cfset greenstruct[group_number] = greencnt>
		</cfif>
		<cfif not structkeyexists(yellowstruct,group_number)>
			<cfset yellowstruct[group_number] = yellowcnt>
		</cfif>
		<cfif not structkeyexists(redstruct,group_number)>
			<cfset redstruct[group_number] = redcnt>
		</cfif>
		
		
		  <cfset newname = replace(group_name,"""","","all")>
		  <cfset newname = replace(newname,"'","","all")>
		  <cfset catlist = catlist & "<category label='#trim(newname)#' />">
		
	
		<cfset greenseries = greenseries & "<set value='#greencnt#' />">
		 <cfset yellowseries = yellowseries & "<set value='#yellowcnt#' />">
		 <cfset redseries = redseries & "<set value='#redcnt#' />">
		</cfoutput>
<div class="content1of4r"><div align="center"><cfchart format="html" chartheight="300" chartwidth="296" scalefrom="0" scaleto="20"  show3d="yes" yaxistitle="## of incidents" xaxistitle="Location" gridlines="6" seriesplacement="cluster" font="Segoe UI" fontsize="10" databackgroundcolor="silver" showygridlines="Yes" foregroundcolor="0f0f0f" showlegend="false" title="2013">
	
	<cfchartseries type="bar" serieslabel="Minor" seriescolor="##00ff00">
	<cfoutput query="chart2" group="Group_Number">
		<cfif structkeyexists(greenstruct,group_number)>
			<cfchartdata item="#group_name#" value="#greenstruct[group_number]#">
		</cfif>
	
		</cfoutput> 
	</cfchartseries>
	<cfchartseries type="bar" serieslabel="Important" seriescolor="yellow" >
	<cfoutput query="chart2" group="Group_Number">
		<cfif structkeyexists(yellowstruct,group_number)>
			<cfchartdata item="#group_name#" value="#yellowstruct[group_number]#">
		</cfif>
	
	</cfoutput>
	</cfchartseries>
	
	<cfchartseries type="bar" serieslabel="Critical"  seriescolor="red" >
	<cfoutput query="chart2" group="Group_Number">
		<cfif structkeyexists(redstruct,group_number)>
			<cfchartdata item="#group_name#" value="#redstruct[group_number]#">
		</cfif>
	
		</cfoutput>
	
	</cfchartseries>
	
</cfchart></div></div>

<cfquery name="chart3" datasource="safety_34">
SELECT        Groups.Group_Number, Groups.Group_Name, First_Incident_Notice.Injury_Illness, First_Incident_Notice.Vehicle_Damage, First_Incident_Notice.Property_Damage, 
                         First_Incident_Notice.Fire_Explosion, First_Incident_Notice.Spill_Release, First_Incident_Notice.Permit_Exceedence, First_Incident_Notice.High_Loss_Near_miss, 
                         First_Incident_Notice.Third_Party_Inspections, First_Incident_Notice.Substance_Abuse, First_Incident_Notice.Security, First_Incident_Notice.Other
FROM            First_Incident_Notice INNER JOIN
                         Groups ON First_Incident_Notice.Group_Number = Groups.Group_Number
WHERE        (First_Incident_Notice.TrackingDate = 2012) and groups.business_line_id = 1
ORDER BY Groups.Business_Line_ID, Groups.Group_Name
</cfquery>


<cfset catlist = "">
<cfset greenseries = "">
<cfset yellowseries = "">
<cfset redseries = "">

<cfset greenstruct = structnew()>
<cfset yellowstruct = structnew()>
<cfset redstruct = structnew()>

<cfoutput query="chart3" group="Group_Number">
	<cfset greencnt = 0>
	<cfset redcnt = 0>
	<cfset yellowcnt = 0>
		<cfoutput>
		<cfif left(trim(Injury_Illness),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		
		
		<cfif left(trim(Injury_Illness),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		
	
		<cfif left(trim(Injury_Illness),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		</cfoutput>
		
		<cfif not structkeyexists(greenstruct,group_number)>
			<cfset greenstruct[group_number] = greencnt>
		</cfif>
		<cfif not structkeyexists(yellowstruct,group_number)>
			<cfset yellowstruct[group_number] = yellowcnt>
		</cfif>
		<cfif not structkeyexists(redstruct,group_number)>
			<cfset redstruct[group_number] = redcnt>
		</cfif>
		
		
		  <cfset newname = replace(group_name,"""","","all")>
		  <cfset newname = replace(newname,"'","","all")>
		  <cfset catlist = catlist & "<category label='#trim(newname)#' />">
		
	
		<cfset greenseries = greenseries & "<set value='#greencnt#' />">
		 <cfset yellowseries = yellowseries & "<set value='#yellowcnt#' />">
		 <cfset redseries = redseries & "<set value='#redcnt#' />">
		</cfoutput>
<div class="content1of4r"><div align="center"><cfchart format="html" chartheight="300" chartwidth="296" scalefrom="0" scaleto="20"  show3d="yes" yaxistitle="## of incidents" xaxistitle="Location" gridlines="6" seriesplacement="cluster" font="Segoe UI" fontsize="10" databackgroundcolor="silver" showygridlines="Yes" foregroundcolor="0f0f0f" showlegend="false" title="2012">
	
	<cfchartseries type="bar" serieslabel="Minor" seriescolor="##00ff00">
	<cfoutput query="chart3" group="Group_Number">
		<cfif structkeyexists(greenstruct,group_number)>
			<cfchartdata item="#group_name#" value="#greenstruct[group_number]#">
		</cfif>
	
		</cfoutput> 
	</cfchartseries>
	<cfchartseries type="bar" serieslabel="Important" seriescolor="yellow" >
	<cfoutput query="chart3" group="Group_Number">
		<cfif structkeyexists(yellowstruct,group_number)>
			<cfchartdata item="#group_name#" value="#yellowstruct[group_number]#">
		</cfif>
	
	</cfoutput>
	</cfchartseries>
	
	<cfchartseries type="bar" serieslabel="Critical"  seriescolor="red" >
	<cfoutput query="chart3" group="Group_Number">
		<cfif structkeyexists(redstruct,group_number)>
			<cfchartdata item="#group_name#" value="#redstruct[group_number]#">
		</cfif>
	
		</cfoutput>
	
	</cfchartseries>
	
</cfchart></div></div>

<cfquery name="chart4" datasource="safety_34">
SELECT        Groups.Group_Number, Groups.Group_Name, First_Incident_Notice.Injury_Illness, First_Incident_Notice.Vehicle_Damage, First_Incident_Notice.Property_Damage, 
                         First_Incident_Notice.Fire_Explosion, First_Incident_Notice.Spill_Release, First_Incident_Notice.Permit_Exceedence, First_Incident_Notice.High_Loss_Near_miss, 
                         First_Incident_Notice.Third_Party_Inspections, First_Incident_Notice.Substance_Abuse, First_Incident_Notice.Security, First_Incident_Notice.Other
FROM            First_Incident_Notice INNER JOIN
                         Groups ON First_Incident_Notice.Group_Number = Groups.Group_Number
WHERE        (First_Incident_Notice.TrackingDate = 2011) and groups.business_line_id = 1
ORDER BY Groups.Business_Line_ID, Groups.Group_Name
</cfquery>


<cfset catlist = "">
<cfset greenseries = "">
<cfset yellowseries = "">
<cfset redseries = "">

<cfset greenstruct = structnew()>
<cfset yellowstruct = structnew()>
<cfset redstruct = structnew()>

<cfoutput query="chart4" group="Group_Number">
	<cfset greencnt = 0>
	<cfset redcnt = 0>
	<cfset yellowcnt = 0>
		<cfoutput>
		<cfif left(trim(Injury_Illness),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "M">
				<cfset greencnt = greencnt + 1>
		</cfif>
		
		
		<cfif left(trim(Injury_Illness),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "I">
				<cfset yellowcnt = yellowcnt + 1>
		</cfif>
		
	
		<cfif left(trim(Injury_Illness),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Vehicle_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Property_Damage),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Fire_Explosion),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Spill_Release),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Permit_Exceedence),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(High_Loss_Near_miss),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Third_Party_Inspections),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Substance_Abuse),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Security),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		<cfif left(trim(Other),1) eq "C">
				<cfset redcnt = redcnt + 1>
		</cfif>
		</cfoutput>
		
		<cfif not structkeyexists(greenstruct,group_number)>
			<cfset greenstruct[group_number] = greencnt>
		</cfif>
		<cfif not structkeyexists(yellowstruct,group_number)>
			<cfset yellowstruct[group_number] = yellowcnt>
		</cfif>
		<cfif not structkeyexists(redstruct,group_number)>
			<cfset redstruct[group_number] = redcnt>
		</cfif>
		
		
		  <cfset newname = replace(group_name,"""","","all")>
		  <cfset newname = replace(newname,"'","","all")>
		  <cfset catlist = catlist & "<category label='#trim(newname)#' />">
		
	
		<cfset greenseries = greenseries & "<set value='#greencnt#' />">
		 <cfset yellowseries = yellowseries & "<set value='#yellowcnt#' />">
		 <cfset redseries = redseries & "<set value='#redcnt#' />">
		</cfoutput>
<div class="content1of4r"><div align="center"><cfchart format="html" chartheight="300" chartwidth="296" scalefrom="0" scaleto="30"  show3d="yes" yaxistitle="## of incidents" xaxistitle="Location" gridlines="6" seriesplacement="cluster" font="Segoe UI" fontsize="10" databackgroundcolor="silver" showygridlines="Yes" foregroundcolor="0f0f0f" showlegend="false" title="2011">
	
	<cfchartseries type="bar" serieslabel="Minor" seriescolor="##00ff00">
	<cfoutput query="chart4" group="Group_Number">
		<cfif structkeyexists(greenstruct,group_number)>
			<cfchartdata item="#group_name#" value="#greenstruct[group_number]#">
		</cfif>
	
		</cfoutput> 
	</cfchartseries>
	<cfchartseries type="bar" serieslabel="Important" seriescolor="yellow" >
	<cfoutput query="chart4" group="Group_Number">
		<cfif structkeyexists(yellowstruct,group_number)>
			<cfchartdata item="#group_name#" value="#yellowstruct[group_number]#">
		</cfif>
	
	</cfoutput>
	</cfchartseries>
	
	<cfchartseries type="bar" serieslabel="Critical"  seriescolor="red" >
	<cfoutput query="chart4" group="Group_Number">
		<cfif structkeyexists(redstruct,group_number)>
			<cfchartdata item="#group_name#" value="#redstruct[group_number]#">
		</cfif>
	
		</cfoutput>
	
	</cfchartseries>
	
</cfchart></div></div>
</p>
<p>
<div class="content3of4" style="background-color:#efefef">This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content. This is content.</div>
<div class="content1of4r" style="background-color:#54de34">More stuff goes here. More stuff goes here. More stuff goes here. More stuff goes here. More stuff goes here. More stuff goes here. </div>
</p>
<p>

<div class="content2of4" style="background-color:#f2ff76">Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. Left side. </div>
<div class="content2of4r" style="background-color:#8d4545">Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. Right side. </div>
</p>
<p><div class="content1of4" style="background-color:#445577">1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 1 of 4 </div>
<div class="content3of4r" style="background-color:#660099">3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 3 of 4 </div>
</p>
<p>
<div class="content1of4" style="background-color:#d04242"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 First 1 of 4 </td></tr></table></div>
<div class="content1of4r" style="background-color:#f39090"><table border="1"  cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 Second 1 of 4 </td></tr></table></div>
<div class="content1of4r" style="background-color:#336677"><table border="1"  cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 Third 1 of 4 </td></tr></table></div>
<div class="content1of4r"  style="background-color:#12ddbb"><table border="1"  cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 Fourth 1 of 4 </td></tr></table></div>
</p>

<p>
	<div class="content1of5" style="background-color:#f9dbca">1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 </div>
	<div class="content1of5r" style="background-color:#abcdef">2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 </div>
	<div class="content1of5r" style="background-color:#fedcba">3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 3 of 5 </div>
	<div class="content1of5r" style="background-color:#cabfed">4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 4 of 5 </div>
	<div class="content1of5r" style="background-color:#facdef">5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 5 of 5 </div>
</p>
<p>
	<div class="content2of5" style="background-color:#ee445e"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 </td></tr></table></div>
	<div class="content1of5r" style="background-color:#ccdd21"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 </td></tr></table></div>
	<div class="content1of5r" style="background-color:#7ade22"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 </td></tr></table></div>
<div class="content1of5r" style="background-color:#daa9dc"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 </td></tr></table></div>
	
</p>
<p>
	<div class="content2of5" style="background-color:#a22000"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 1 of 5 </td></tr></table></div>
	<div class="content1of5r" style="background-color:#2200fe"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 </td></tr></table></div>
	<div class="content2of5r" style="background-color:#88bbcc"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 2 of 5 </td></tr></table></div>

</p>
<p>
	<div class="content1of3" style="background-color:#446234"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 </td></tr></table></div>
	<div class="content1of3r" style="background-color:#fd3248"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 </td></tr></table></div>
	<div class="content1of3r" style="background-color:#acdef3"><table border="1"   cellpadding="3" cellspacing="0"><tr><td style="font-size:10pt;">1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 </td></tr></table></div>
</p>
<p>
	<div class="content1of3" style="background-color:#defdef">1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 </div>
	<div class="content2of3r" style="background-color:#337799">2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 </div>
	
</p>
<p>
	<div class="content2of3" style="background-color:#464321">2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 2 of 3 </div>
	<div class="content1of3r" style="background-color:#feedef">1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 1 of 3 </div>
	
	
</p>