<cfset nextincuser = "">
<table cellpadding="0" cellspacing="1" border="0" class="purplebg" width="100%">


	<tr>
		<td align="center" bgcolor="ffffff">
	
	<table cellpadding="0" cellspacing="1" border="0" bgcolor="ffffff" width="100%">
	
	<tr>
		<td  valign="top" width="50%">
			<cfoutput><table cellpadding="3" cellspacing="1" border="0" width="100%">
				<tr>
				<td class="formlable" align="left" width="50%"><strong>Incident Status:</strong></td>
				<td class="bodyTextGrey" width="50%" >
				<cfif getincidentdetails.withdrawnstatus eq 1>
					Withdrawn
				<cfelse>
					<cfswitch expression="#getincidentdetails.status#">
						<cfcase value="Started,Initiated">
							<cfset nextincuser = getincidentdetails.createdBy>
							Raised
						</cfcase>
						<cfcase value="Sent for Review">
							Initial Review
						</cfcase>
						<cfcase value="More Info Requested ">
							More Information Requested 
						</cfcase>
						<cfcase value="Review Completed">
							<cfif trim(getinvstat.status) eq ''>
								<cfif getincidentdetails.isworkrelated eq 1>
									<cfset nextincuser = getincidentdetails.createdBy>
									Investigation
								<cfelse>
									<cfset whypendlist = "">
									<cfquery name="getopenca" datasource="#request.dsn#">
										SELECT        status
										FROM            oneAIMCAPA
										WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (CAPAtype = 'corrective') AND (status = 'Open')
									</cfquery>
									<cfif getopenca.recordcount gt 0>
										<cfset whypendlist = listappend(whypendlist,"CA")>
									</cfif>
									<cfif listfind("1,5",getincidentdetails.PrimaryType) gt 0 or listfind("1,5",getincidentdetails.SecondaryType) gt 0>
										<cfquery name="getoshacat" datasource="#request.dsn#">
											SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
											FROM            oneAIMInjuryOI
											where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
										</cfquery>
										<cfif getoshacat.OSHAclass eq 2>
											<cfif trim(getoshacat.LTIDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"LTI")>
											</cfif>
										<cfelseif getoshacat.OSHAclass eq 4> 
											<cfif trim(getoshacat.RWCDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"RWC")>
											</cfif>
										</cfif> 
									</cfif>
					
									Pending Closure<cfif trim(whypendlist) neq ''> - #whypendlist#</cfif>
								</cfif>
							<cfelse>
							<cfswitch expression="#getinvstat.status#">
								<cfcase value="Started">
									<cfset nextincuser = getincidentdetails.createdBy>
									Investigation
								</cfcase>
								<cfcase value="Sent for Review,Sent for Senior Review">
									Investigation Review
								</cfcase>
								<cfcase value="More Info Requested,Senior More Info Requested">
									Investigation More Information Requested
								</cfcase>
								<cfcase value="IRP Pending">
									<cfset whypendlist = "">
									<cfquery name="getopenca" datasource="#request.dsn#">
										SELECT        status
										FROM            oneAIMCAPA
										WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (CAPAtype = 'corrective') AND (status = 'Open')
									</cfquery>
									<cfif getopenca.recordcount gt 0>
										<cfset whypendlist = listappend(whypendlist,"CA")>
									</cfif>
									<cfif listfind("1,5",getincidentdetails.PrimaryType) gt 0 or listfind("1,5",getincidentdetails.SecondaryType) gt 0>
										<cfquery name="getoshacat" datasource="#request.dsn#">
											SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
											FROM            oneAIMInjuryOI
											where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
										</cfquery>
										<cfif getoshacat.OSHAclass eq 2>
											<cfif trim(getoshacat.LTIDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"LTI")>
											</cfif>
										<cfelseif getoshacat.OSHAclass eq 4> 
											<cfif trim(getoshacat.RWCDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"RWC")>
											</cfif>
										</cfif> 
									</cfif>
					
									Pending Closure<cfif trim(whypendlist) neq ''> - #whypendlist#</cfif>
								</cfcase>
								<cfcase value="Approved">
									<cfset whypendlist = "">
									<cfquery name="getopenca" datasource="#request.dsn#">
										SELECT        status
										FROM            oneAIMCAPA
										WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (CAPAtype = 'corrective') AND (status = 'Open')
									</cfquery>
									<cfif getopenca.recordcount gt 0>
										<cfset whypendlist = listappend(whypendlist,"CA")>
									</cfif>
									<cfif listfind("1,5",getincidentdetails.PrimaryType) gt 0 or listfind("1,5",getincidentdetails.SecondaryType) gt 0>
										<cfquery name="getoshacat" datasource="#request.dsn#">
											SELECT        OSHAclass, LTIDateReturned, RWCDateReturned
											FROM            oneAIMInjuryOI
											where irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">
										</cfquery>
										<cfif getoshacat.OSHAclass eq 2>
											<cfif trim(getoshacat.LTIDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"LTI")>
											</cfif>
										<cfelseif getoshacat.OSHAclass eq 4> 
											<cfif trim(getoshacat.RWCDateReturned) eq ''>
												<cfset whypendlist = listappend(whypendlist,"RWC")>
											</cfif>
										</cfif> 
									</cfif>
					
									Pending Closure<cfif trim(whypendlist) neq ''> - #whypendlist#</cfif>
								</cfcase>
								
							</cfswitch>
							</cfif>
						</cfcase>
						<cfcase value="Closed">
							Closed
						</cfcase>
						<cfcase value="Cancel">
							Cancelled
						</cfcase>
						<cfdefaultcase>
							#getincidentdetails.status#
						</cfdefaultcase>
					</cfswitch>
					</cfif>
					<!--- <cfif listfindnocase("Started,Initiated",getincidentdetails.status) gt 0>Raised<cfelse>#getincidentdetails.status#</cfif>&nbsp; ---><!--- getincidentdetails.investigationlevel eq 2 or  --->
				<!--- <cfif getincidentdetails.status neq "cancel"><cfif getincidentdetails.needFA eq 1>
					(First Alert <cfif trim(getstat.status) eq ''>Needed<cfelse>#getstat.status#</cfif>)
				</cfif></cfif> ---></td>
				</tr>
				<cfif listfindnocase("Cancel,Closed",getincidentdetails.status) eq 0>
				<tr>
				<td class="formlable" align="left" width="50%"><strong>Pending Action By:</strong></td>
				<td class="bodyTextGrey" width="50%" ><cfif getincidentdetails.withdrawnstatus eq 1><cfif trim(altnextbywb) neq ''>#replace(altnextbywb,",",", ","all")#<cfelse>#getincidentdetails.withdrawnby#</cfif><cfelse><cfif trim(getnextstep.sentto) eq ''><cfif getincidentdetails.status neq "Closed"><cfif getinvstat.status eq "Sent for Review">#getincidentdetails.HSSEManager#<cfelse><cfif trim(altnextby) neq ''>#replace(altnextby,",",", ","all")#<cfelse>#getincidentdetails.createdby#</cfif></cfif></cfif><cfelse><cfif trim(nextincuser) neq ''><cfif trim(altnextby) neq ''>#replace(altnextby,",",", ","all")#<cfelse>#nextincuser#</cfif><cfelse><cfif getinvstat.status eq "Sent for Review">#getincidentdetails.HSSEManager#<cfelse>#getnextstep.sentto#</cfif></cfif></cfif></cfif></td>
				</tr>
				</cfif>
				
				<cfif getincidentdetails.needFA eq 1>
				<tr>
					<td colspan="2">
					<table cellpadding="0" cellspacing="0" border="0" class="purplebg" width="100%">
						<tr>
							<td><img src="images/spacer.gif" height="1" border="0">
						</tr>
					</table>
					</td>
				</tr>
				<tr>
				<td class="formlable" align="left" width="50%"><strong>First Alert Status:</strong></td>
				<td class="bodyTextGrey" width="50%" >
				<cfif trim(getstat.status) eq ''>FA Prepare<cfelse>
					<cfswitch expression="#getstat.status#">
						<cfcase value="Started,Initiated">
							FA Prepare
						</cfcase>
						<cfcase value="Sent for Review">
							FA Review
						</cfcase>
						<cfcase value="More Info Requested">
							FA More Information Requested
						</cfcase>
						<cfcase value="Sr More Info">
							FA More Information Requested - Sr. Reviewer
						</cfcase>
						<cfcase value="Sent for Approval">
							FA Approval
						</cfcase>
						<cfcase value="Approved">
							FA Approved
						</cfcase>
						<cfcase value="Declined">
							FA Declined
						</cfcase>
					</cfswitch>
					</cfif>
					</td>
				</tr>
					<cfif listfindnocase("Approved,Declined",getstat.status) eq 0>
					<tr>
						<td class="formlable" align="left" width="50%"><strong>Pending Action By:</strong></td>
						<td class="bodyTextGrey" width="50%" >#fanextuser#</td>
					</tr>
					</cfif>
				</cfif>
				<cfif getincidentdetails.needirp eq 1>
				<tr>
					<td colspan="2">
					<table cellpadding="0" cellspacing="0" border="0" class="purplebg" width="100%">
						<tr>
							<td><img src="images/spacer.gif" height="1" border="0">
						</tr>
					</table>
					</td>
				</tr>
				<tr>
				<td class="formlable" align="left" width="50%"><strong>IRP Status:</strong></td>
				<td class="bodyTextGrey" width="50%" >
					<cfif trim(getirpstat.status) eq ''>IRP Prepare<cfelse>
					<cfswitch expression="#getirpstat.status#">
						<cfcase value="Started,Initiated">
							IRP Prepare
						</cfcase>
						<cfcase value="Sent for Review">
							IRP Review
						</cfcase>
						<cfcase value="More Info Requested">
							IRP More Information Requested
						</cfcase>
						<cfcase value="Approved">
							IRP Approved
						</cfcase>
						<cfcase value="Complete">
							<Cfquery name="getirpauds" datasource="#request.dsn#">
							SELECT        ActionTaken
							FROM            IncidentAudits
							WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (ActionTaken LIKE 'IRP%')
							ORDER BY CompletedDate DESC
							</cfquery>
							<cfloop query="getirpauds">
								<cfif ActionTaken eq "IRP Declined">
									IRP Declined
									<cfbreak>
								<cfelseif ActionTaken eq "IRP Approved">
									IRP Approved
									<cfbreak>
								</cfif>
							</cfloop>
						</cfcase>
						<cfcase value="Declined">
							IRP Declined
						</cfcase>
					</cfswitch>
					</cfif>
					</td>
				</tr>
					<cfif listfindnocase("Approved,Declined,Complete",getirpstat.status) eq 0>
					<tr>
						<td class="formlable" align="left" width="50%"><strong>Pending Action By:</strong></td>
						<td class="bodyTextGrey" width="50%" >#irpnextuser#</td>
					</tr>
					</cfif>
				</cfif>
			</table></cfoutput>
		</td>
		<td  valign="top" width="50%"><cfif listfindnocase("Cancel",getincidentdetails.status) eq 0>
			<table cellpadding="4" cellspacing="1" border="0" width="100%">
				<tr>
				<td class="formlable" align="left" width="50%" valign="top"><strong>Accessible By:</strong></td>
				<td class="bodyTextGrey" width="50%"  valign="top">
				<table cellpadding="1" cellspacing="0" border="0" width="100%">
				<cfoutput query="getwhocansee" group="userrole">
				<tr>
					<td class="bodyTextGrey"><strong>#userrole#</strong></td>
				</tr>
				<cfoutput>
					<tr>
						<td class="bodyTextGrey">&nbsp;&nbsp;#name#</td>
					</tr>
				
				</cfoutput></cfoutput>
				</table>
				</td>
				</tr>
			</table></cfif>
		</td>			
	</tr>
	<tr>
		<td colspan="2" align="center" class="purplebg"><strong class="bodyTextWhite">Incident Audit Trail</strong></td>
	</tr>
	<tr>
		<td colspan="2">
			<table cellpadding="4" cellspacing="1" border="0" width="100%">
				<tr>
					<td class="formlable" valign="top" width="16%"><strong>From Status</strong></td>
					<td class="formlable" valign="top" width="16%"><strong>Action</strong></td>
					<td class="formlable" valign="top" width="16%"><strong>To Status</strong></td>
					<td class="formlable" valign="top" width="20%"><strong>Status</strong></td>
					<cfif qrysort eq "DESC">
						<cfset tosortres = "ASC">
					<cfelse>
						<cfset tosortres = "DESC">
					</cfif>
					<cfoutput><td class="formlable" valign="top" width="16%"><strong><a href="index.cfm?fuseaction=incidents.audit&irn=#irn#&qrysort=#tosortres#">Date/Time&nbsp;<cfif qrysort eq "DESC">&##x25BC;<cfelse>&##x25B2;</cfif></a></strong></td></cfoutput>
					<td class="formlable" valign="top" width="16%"><strong>User</strong></td>
				</tr>
				<cfoutput query="getincaudits">
					<tr <cfif currentrow mod 2 is 0>bgcolor="efefef"</cfif>>
						<td class="bodyTextGrey" valign="top">#fromstatus#</td>
						<td class="bodyTextGrey" valign="top">#actiontaken#</td>
						<td class="bodyTextGrey" valign="top">#status#</td>
						<td class="bodyTextGrey" valign="top">#StatusDesc#</td>
						<td class="bodyTextGrey" valign="top">#dateTimeFormat(completeddate,sysdatetimeformat)#&nbsp;GMT</td>
						<td class="bodyTextGrey" valign="top">#completedby#</td>
					</tr>
				</cfoutput>
				</table>
			</td>
		</tr>
		</table>
	</td>
	</tr>
</table>