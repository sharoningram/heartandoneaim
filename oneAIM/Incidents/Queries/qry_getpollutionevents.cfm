<cfquery name="getpollutionevents" datasource="#request.dsn#">
SELECT    PollEventID, IRN, PollutionEventType, SubstanceType, Quantity, SubstanceUnit, ReleaseSource, ReleaseDuration, Environment
FROM            oneAIMEnviroPollutionEvents
WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>