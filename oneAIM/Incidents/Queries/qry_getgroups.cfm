<cfset gnuml = "">
<cfif listfindnocase(request.userlevel,"user") gt 0>
<cfquery name="getugnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
and  groups.ouid in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserOUs#" list="Yes">)
		and group_number in (SELECT        GroupNumber
FROM            GroupUserAssignment
WHERE        (status = 1) AND (GroupRole = 'dataentry') AND (UserID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.UserID#">))
</cfquery>
<cfloop query="getugnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>

</cfif>

<cfif listfindnocase(request.userlevel,"BU Admin") gt 0>

<cfquery name="getbugnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
 and  groups.business_line_id in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'BU Admin')
</cfquery>
<cfloop query="getbugnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>


</cfif>

<cfif listfindnocase(request.userlevel,"oU Admin") gt 0>

<cfquery name="getougnum" datasource="#request.dsn#">
SELECT        Group_Number
from groups 
WHERE Active_Group = 1
 and   groups.ouid in (select assignedlocs from userroles where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#request.userid#"> and userrole = 'OU Admin')
</cfquery>
<cfloop query="getougnum">
	<cfif listfind(gnuml,Group_Number) eq 0>
		<cfset gnuml = listappend(gnuml,group_number)>
	</cfif>
</cfloop>


</cfif>
<cfif trim(gnuml) eq ''>
	<cfset gnuml = 0>
</cfif>

<cfquery name="getgroups" datasource="#request.dsn#">
SELECT DISTINCT 
                         Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, 
                         Groups.Active_Group
FROM            Groups INNER JOIN
                         GroupLocations ON Groups.Group_Number = GroupLocations.GroupNumber
WHERE        (Groups.Active_Group = 1) AND (GroupLocations.entryType IN ('both', 'IncidentOnly'))

<cfif listfindnocase(request.userlevel,"Global Admin") eq 0>

	<cfif listfindnocase(request.userlevel,"user") gt 0 or listfindnocase(request.userlevel,"BU Admin") gt 0 or listfindnocase(request.userlevel,"OU Admin") gt 0>
	 and Groups.group_number in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#gnuml#" list="Yes">)
	</cfif>
</cfif>
order by Groups.Group_Name
</cfquery>