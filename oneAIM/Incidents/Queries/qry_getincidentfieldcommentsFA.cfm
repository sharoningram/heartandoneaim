<cfquery name="getfldaudit" datasource="#request.dsn#">
SELECT        fieldauditid, fieldname, oldvalue, newvalue, enteredby, dateentered, irn
FROM            oneAIMfieldAudits
WHERE        (irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) and fieldname in ('potrating','invlevel','oshacat')
ORDER BY fieldname, dateentered DESC
</cfquery>
