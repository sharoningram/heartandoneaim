<cfquery name="getcapalist" datasource="#request.dsn#">
SELECT        oneAIMCAPA.CAPAid, oneAIMCAPA.CAPAtype, oneAIMCAPA.DueDate, oneAIMCAPA.AssignedTo, oneAIMCAPA.AssignedToEmail, oneAIMCAPA.ActionDetail, 
                         oneAIMCAPA.DateComplete, oneAIMCAPA.RevisionReason, oneAIMCAPA.EnteredBy, oneAIMCAPA.DateEntered, oneAIMCAPA.IRN, oneAIMCAPA.status, 
                         oneAIMCAPA.AssigneeEmail, oneAIMCAPAaudits.updateby, oneAIMCAPAaudits.updatebyemail, oneAIMCAPAaudits.dateupdated, oneAIMCAPAaudits.actiontaken
FROM            oneAIMCAPA LEFT OUTER JOIN
                         oneAIMCAPAaudits ON oneAIMCAPA.CAPAid = oneAIMCAPAaudits.CAPAid
WHERE        (oneAIMCAPA.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
ORDER BY oneAIMCAPA.status DESC, oneAIMCAPA.CAPAtype, oneAIMCAPA.DueDate,oneAIMCAPA.CAPAid, oneAIMCAPAaudits.dateupdated DESC
</cfquery><!--- SELECT        CAPAid, CAPAtype, DueDate, AssignedTo, AssignedToEmail, ActionDetail, DateComplete, RevisionReason, EnteredBy, DateEntered, IRN, status,AssigneeEmail
FROM            oneAIMCAPA
WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
ORDER BY status desc,CAPAtype, DueDate --->