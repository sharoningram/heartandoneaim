<cfparam name="qrysort" default="DESC">
<cfquery name="getincaudits" datasource="#request.dsn#">
SELECT        IRNAuditID, IRN, Status, CompletedBy, CompletedDate, SentTo, ActionTaken, fromstatus, StatusDesc, fromwithdraw
FROM            IncidentAudits
WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
ORDER BY CompletedDate #qrysort#
</cfquery>

<cfquery name="getnextstep" dbtype="query">
	select IRNAuditID, IRN, Status, CompletedBy, CompletedDate, SentTo, ActionTaken, fromstatus, StatusDesc, fromwithdraw
	from getincaudits
	where fromwithdraw <> 1
	ORDER BY CompletedDate DESC
</cfquery>