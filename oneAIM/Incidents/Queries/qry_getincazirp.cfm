<cfquery name="getincazirp" datasource="#request.dsn#">
SELECT        IncidentAZ.AZCatID, IncidentAZ.AZType, AZcategories.CategoryLetter, AZcategories.CategoryName, AZfactors.FactorName, AZfactors.FactorNumber
FROM            IncidentInvestigation INNER JOIN
                         IncidentAZ ON IncidentInvestigation.InvID = IncidentAZ.InvID INNER JOIN
                         AZcategories ON IncidentAZ.AZCatID = AZcategories.CategoryID INNER JOIN
                         AZfactors ON AZcategories.CategoryLetter = AZfactors.CategoryLetter AND IncidentAZ.AZFactorID = AZfactors.FactorID
WHERE        (IncidentInvestigation.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
ORDER BY IncidentAZ.AZType, AZcategories.CategoryLetter, AZcategories.CategoryName
</cfquery>