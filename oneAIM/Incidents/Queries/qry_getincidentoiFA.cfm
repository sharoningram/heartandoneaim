<cfquery name="getincidentoi" datasource="#request.dsn#">
SELECT        oneAIMInjuryOI.IOIid, oneAIMInjuryOI.IRN, oneAIMInjuryOI.OSHAclass, oneAIMInjuryOI.isSerious, oneAIMInjuryOI.HazardSource, oneAIMInjuryOI.otherHazard, 
                         oneAIMInjuryOI.ipName, oneAIMInjuryOI.ipOccupation, oneAIMInjuryOI.ipAge, oneAIMInjuryOI.ipGender, oneAIMInjuryOI.ipShift, oneAIMInjuryOI.injuryType, 
                         oneAIMInjuryOI.otherInjuryType, oneAIMInjuryOI.bodyPart, oneAIMInjuryOI.injuryCause, oneAIMInjuryOI.otherInjuryCause, oneAIMInjuryOI.fallHeight, 
                         oneAIMInjuryOI.droppedObject, oneAIMInjuryOI.DOheight, oneAIMInjuryOI.DOweight, oneAIMInjuryOI.DOenergy, oneAIMInjuryOI.DOcategory, 
                         oneAIMInjuryOI.IllnessType, oneAIMInjuryOI.OtherIllnessType, oneAIMInjuryOI.IllnessNature, oneAIMInjuryOI.OtherIllnessNature, oneAIMInjuryOI.LTIFirstDate, 
                         oneAIMInjuryOI.LTIDateReturned, oneAIMInjuryOI.LTIReturnRestricted, oneAIMInjuryOI.RWCFirstDate, oneAIMInjuryOI.RWCDateReturned, 
                         oneAIMInjuryOI.MedicalRestrictions, OSHACategories.Category, Occupations.Occupation
FROM            oneAIMInjuryOI LEFT OUTER JOIN
                         Occupations ON oneAIMInjuryOI.ipOccupation = Occupations.OccupationID LEFT OUTER JOIN
                         OSHACategories ON oneAIMInjuryOI.OSHAclass = OSHACategories.CatID
WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>

<cfquery name="getintype" datasource="#request.dsn#">
SELECT     InjTypeID, InjuryType, Status
FROM            InjuryTypes
where InjTypeID in (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentoi.injuryType#" list="Yes">)
</cfquery>