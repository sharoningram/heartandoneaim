<cfparam name="irn" default="0">
<cfquery name="getincidentcondetail" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.createdBy, oneAIMincidents.createdbyEmail, oneAIMincidents.HSSEBUDirector,oneAIMincidents.dateCreated,oneAIMincidents.needFA, 
                         oneAIMincidents.GroupNumber, oneAIMincidents.LocationID, oneAIMincidents.isWorkRelated, oneAIMincidents.PrimaryType, oneAIMincidents.isNearMiss, oneAIMincidents.needirp,
                         oneAIMincidents.isSecondary, oneAIMincidents.SecondaryType, oneAIMincidents.OIdefinition, oneAIMincidents.incidentDate, oneAIMincidents.incidentTime, 
                         oneAIMincidents.DayofWeek, oneAIMincidents.Weather, oneAIMincidents.ShortDesc, oneAIMincidents.IncAssignedTo, oneAIMincidents.Contractor, 
                         oneAIMincidents.OtherAssignedTo, oneAIMincidents.ReportedByCo, oneAIMincidents.ReportedBy, oneAIMincidents.NonAmecFWemail, oneAIMincidents.Description, 
                         oneAIMincidents.TrackingYear, oneAIMincidents.TrackingNum, oneAIMincidents.PotentialRating, oneAIMincidents.InvestigationLevel, 
                         oneAIMincidents.OccurAtLocation, oneAIMincidents.WhereOccur, oneAIMincidents.SpecificLocation, oneAIMincidents.HSSEAdvisor, oneAIMincidents.HSSEManager, 
                         oneAIMincidents.ActionTaken, oneAIMincidents.wasReported, oneAIMincidents.StatutoryBody, oneAIMincidents.DateStatReported, 
                         oneAIMincidents.EnforcementNotice, oneAIMincidents.EnforcementNoticeType, oneAIMincidents.Status, oneAIMincidents.withdrawnstatus, 
                         oneAIMincidents.DeleteStatus, oneAIMincidents.ReasonLate, Groups.Group_Name,  Groups.ouid,Groups.Business_Line_ID, GroupLocations.SiteName, Countries.CountryName, NewDials.Name AS buname, 
                         NewDials_1.Name AS ouname, NewDials_2.Name AS bsname,oneAIMincidents.withdrawnto,oneAIMincidents.withdrawndate,oneAIMincidents.withdrawnby,oneAIMincidents.otherContName,oneAIMincidents.DOheight ,oneAIMincidents.DOweight ,oneAIMincidents.DOenergy ,oneAIMincidents.DOcategory, oneAIMincidents.droppedObject
FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number ON GroupLocations.GroupLocID = oneAIMincidents.LocationID LEFT OUTER JOIN
                         NewDials AS NewDials_1 ON Groups.OUid = NewDials_1.ID LEFT OUTER JOIN
                         NewDials ON Groups.Business_Line_ID = NewDials.ID LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>

<cfif irn gt 0>
<cfquery name="getincsublist" datasource="#request.dsn#">
SELECT        ContractorID, ContractorName, ContractorNameID
FROM            Contractors
WHERE        (GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentcondetail.GroupNumber#">) and status = 1 
<cfif getincidentcondetail.IncAssignedTo eq 3>	
	and cotype = 'sub'
<cfelseif getincidentcondetail.IncAssignedTo eq 4>	
	and cotype = 'mgd'
<cfelseif getincidentcondetail.IncAssignedTo eq 11>	
	and cotype = 'jv'
<cfelse>
	and cotype = 'sub'
</cfif>
order by ContractorName
</cfquery>
<cfset currconlist = 0>
<cfif getincsublist.recordcount gt 0>
	<cfset currconlist = valuelist(getincsublist.ContractorNameID)>
</cfif>
<cfquery name="getconothers" datasource="#request.dsn#">
	SELECT        ContractorID, ContractorName
	FROM            ContractorNames
	WHERE status = 1
	order by ContractorName
</cfquery>
<cfquery name="getinclocations" datasource="#request.dsn#">
SELECT        Groups.Group_Number, Groups.Group_Name, Groups.Country_Code, Groups.Business_Line_ID, Groups.OUid, Groups.BusinessStreamID, Countries.CountryName, 
                         NewDials_1.Name AS BU, NewDials.Name AS OU, NewDials_2.Name AS BS, GroupLocations.GroupLocID, GroupLocations.SiteName
FROM            Countries INNER JOIN
                         GroupLocations ON Countries.CountryID = GroupLocations.CountryID RIGHT OUTER JOIN
                         Groups INNER JOIN
                         NewDials AS NewDials_1 ON Groups.Business_Line_ID = NewDials_1.ID INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON GroupLocations.GroupNumber = Groups.Group_Number LEFT OUTER JOIN
                         NewDials AS NewDials_2 ON Groups.BusinessStreamID = NewDials_2.ID
WHERE        (Groups.Group_Number = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getincidentcondetail.GroupNumber#">)
</cfquery>
</cfif>