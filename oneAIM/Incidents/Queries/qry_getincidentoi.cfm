<cfquery name="getincidentoi" datasource="#request.dsn#">
SELECT        IOIid, IRN, OSHAclass, isSerious, HazardSource, otherHazard, ipName, ipOccupation, ipAge, ipGender, ipShift, injuryType, otherInjuryType, bodyPart, injuryCause, 
                         otherInjuryCause, fallHeight, droppedObject, DOheight, DOweight, DOenergy, DOcategory, IllnessType, OtherIllnessType, IllnessNature, OtherIllnessNature, 
                         LTIFirstDate, LTIDateReturned, LTIReturnRestricted, RWCFirstDate, RWCDateReturned, MedicalRestrictions,shiftDayof,shiftDay,dayssinceoff,workingshift,dateretcomm
FROM            oneAIMInjuryOI
WHERE        (IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>