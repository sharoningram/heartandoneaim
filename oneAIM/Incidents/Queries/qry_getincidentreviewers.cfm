<cfquery name="getincidentreviewers" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, Groups.OUid, UserRoles.UserID, UserRoles.UserRole, Users.Firstname, Users.Lastname, Users.Useremail
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         UserRoles ON Groups.OUid = UserRoles.AssignedLocs INNER JOIN
                         Users ON UserRoles.UserID = Users.UserId
WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (UserRoles.UserRole = 'Reviewer') AND (Users.Status = 1)
</cfquery>