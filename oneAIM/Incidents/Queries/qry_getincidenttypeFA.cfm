<cfquery name="getincidenttypeFA" datasource="#request.dsn#">
SELECT        oneAIMincidents.IRN, IncidentTypes.IncType, IncidentAssignedTo.IncAssignedTo
FROM            oneAIMincidents LEFT OUTER JOIN
                         IncidentAssignedTo ON oneAIMincidents.IncAssignedTo = IncidentAssignedTo.IncAssignedID LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID
WHERE        (oneAIMincidents.IRN = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">)
</cfquery>