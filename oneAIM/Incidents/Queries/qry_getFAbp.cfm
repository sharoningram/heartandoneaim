<cfparam name="usbp" default="0">
<cfif trim(getincidentoi.bodypart) neq ''>
	<cfset usbp = getincidentoi.bodypart>
</cfif>
<cfquery name="getfabp" datasource="#request.dsn#">
SELECT        BodyPartID, BodyPart, Status
FROM            BodyParts
WHERE        (BodyPartID IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#usbp#" list="Yes">))
</cfquery>