<cfparam name="audittype" default="">
<cfparam name="irn" default="0">
<cfif audittype eq "invlevel">
	<cfquery name="getaudit" datasource="#request.dsn#">
	SELECT        fieldauditid, fieldname, oldvalue, newvalue, enteredby, dateentered, irn
FROM            oneAIMfieldAudits
WHERE        (irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (fieldname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#audittype#">)
ORDER BY dateentered DESC
	</cfquery>
<cfelseif audittype eq "potrating">
	<cfquery name="getaudit" datasource="#request.dsn#">
	SELECT        fieldauditid, fieldname, oldvalue, newvalue, enteredby, dateentered, irn
FROM            oneAIMfieldAudits
WHERE        (irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (fieldname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#audittype#">)
ORDER BY dateentered DESC
	</cfquery>
<cfelseif audittype eq "oshacat">
	<cfquery name="getaudit" datasource="#request.dsn#">
	SELECT        oneAIMfieldAudits.fieldauditid, oneAIMfieldAudits.fieldname, oneAIMfieldAudits.oldvalue AS oldval, oneAIMfieldAudits.newvalue AS newval, 
                         oneAIMfieldAudits.enteredby, oneAIMfieldAudits.dateentered, oneAIMfieldAudits.irn, OSHACategories_1.Category AS oldvalue, 
                         OSHACategories.Category AS newvalue
FROM            oneAIMfieldAudits LEFT OUTER JOIN
                         OSHACategories ON oneAIMfieldAudits.newvalue = OSHACategories.CatID LEFT OUTER JOIN
                         OSHACategories AS OSHACategories_1 ON oneAIMfieldAudits.oldvalue = OSHACategories_1.CatID
WHERE        (oneAIMfieldAudits.irn = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#irn#">) AND (oneAIMfieldAudits.fieldname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#audittype#">)
ORDER BY oneAIMfieldAudits.dateentered DESC
</cfquery>
</cfif>