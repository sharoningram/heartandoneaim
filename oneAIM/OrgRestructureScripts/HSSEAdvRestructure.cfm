<!--- STEP ONE UPDATE BUID LIST  --->
<!--- 
<cfquery name="getbuid" datasource="oneaim">
select  top 200 advid, FirstName, LastName, BU, OU, GroupName, SiteName, BUID, OUID, GroupNumber, LocationID, wasProcessed
FROM            Restruct_Adv_Import
WHERE        (BU IS NOT NULL) and buid is null
ORDER BY BU
</cfquery>

<cfquery name="getbulist" datasource="oneaim">
SELECT        ID, Name, Parent, Value, isgroupdial, GroupNumber, status, businessline, TrackingYear, ViewOnDashboard, GroupNumList
FROM            NewDials
WHERE        (Parent = 2707) AND (status <> 99) 
ORDER BY Name
</cfquery>
<cfset bunamelist = valuelist(getbulist.Name,"^")>
<cfset buliststruct = {}>

<cfloop query="getbulist">
	<cfset buliststruct[#id#] = name>
</cfloop>

<cfdump var="#buliststruct#">

<cfoutput query="getbuid">

<cfset tidlist = "">
	
		<cfif listfind(bunamelist,BU,"^") gt 0>
		<cfloop list="#structkeylist(buliststruct)#" index="i">
			<cfif buliststruct[i] eq BU>
				<cfset tidlist = listappend(tidlist,i,"^")>
			</cfif>
		</cfloop>
		<cfelse>
			<cfset tidlist = ''>
			
		</cfif>
	
<cfif trim(tidlist) neq ''>
	<cfquery name="updaterec" datasource="oneaim">
		update Restruct_Adv_Import
		set BUID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#tidlist#">
		where advid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#advid#">
	</cfquery>
</cfif> 
<cfif trim(tidlist) eq ''><span style="color:red"></cfif>#advid#  #firstname# #lastname# #bu# <strong>#tidlist#</strong><cfif trim(tidlist) eq ''></span></cfif><br>
</cfoutput>
 --->
<!---  STEP TWO UPDATE OUID LIST --->
 <!--- <cfquery name="getouid" datasource="oneaim">
select top 250  advid, FirstName, LastName, BU, OU, GroupName, SiteName, BUID, OUID, GroupNumber, LocationID, wasProcessed
FROM            Restruct_Adv_Import
WHERE        (OU IS NOT NULL) and ouid is null
ORDER BY OU
</cfquery>
<cfoutput query="getouid">

<cfquery name="getouidg" datasource="oneaim">
SELECT         OUID
FROM            Groups
WHERE        (Group_Name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#groupname#">) AND (business_line_id = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BUID#">)
</cfquery>

<cfif getouidg.recordcount eq 1>
	<cfquery name="updaterec" datasource="oneaim">
		update Restruct_Adv_Import
		set OUID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getouidg.OUID#">
		where advid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#advid#">
	</cfquery>
	#advid# #FirstName#, #LastName# #GroupName# #ou#<br>
<cfelse>
	<span style="color:red">#advid# #FirstName#, #LastName# #GroupName# #ou#</span><br>
</cfif>

</cfoutput> 
 --->

 
 <!--- STEP THREE UPDATE GROUP NUMBER  --->
 <!--- <cfquery name="getgneed" datasource="oneaim">
select top 250 advid, FirstName, LastName, BU, OU, GroupName, SiteName, BUID, OUID, GroupNumber, LocationID, wasProcessed
FROM            Restruct_Adv_Import
WHERE        (GroupName IS NOT NULL) and GroupNumber is null
ORDER BY GroupName
</cfquery>
<cfoutput query="getgneed">

<cfif trim(ouid) neq ''>
<cfquery name="getgnum" datasource="oneaim">
SELECT         Group_Number
FROM            Groups
WHERE        (Group_Name = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#groupname#">) AND (OUid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ouid#">)
</cfquery>

<cfif getgnum.recordcount eq 1>
	<cfquery name="updaterec" datasource="oneaim">
		update Restruct_Adv_Import
		set GroupNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgnum.Group_Number#">
		where advid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#advid#">
	</cfquery>
	#advid# #FirstName#, #LastName# #GroupName#<br>
<cfelse>
	<span style="color:red">#advid# #FirstName#, #LastName# #GroupName#</span><br>
</cfif>
<cfelse>
	<span style="color:red">#advid# #FirstName#, #LastName# #GroupName#</span><br>
</cfif>
</cfoutput> --->


<!--- STEP Four UPDATE GROUP Location --->
<!--- <cfquery name="getlocneed" datasource="oneaim">
select top 250 advid, FirstName, LastName, BU, OU, GroupName, SiteName, BUID, OUID, GroupNumber, LocationID, wasProcessed
FROM            Restruct_Adv_Import
WHERE        (SiteName IS NOT NULL) and LocationID is null
ORDER BY GroupName
</cfquery>
<cfoutput query="getlocneed">

<cfif trim(GroupNumber) neq ''>
<cfquery name="getlocid" datasource="oneaim">
SELECT         GroupLocID
FROM            GroupLocations
WHERE        (SiteName  = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#SiteName#">) AND (GroupNumber  = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#GroupNumber#">)
</cfquery>

<cfif getlocid.recordcount eq 1>
	<cfquery name="updaterec" datasource="oneaim">
		update Restruct_Adv_Import
		set LocationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getlocid.GroupLocID#">
		where advid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#advid#">
	</cfquery>
	#advid# #FirstName#, #LastName# #GroupName#<br>
<cfelse>
	<span style="color:red">#advid# #FirstName#, #LastName# #GroupName#</span><br>
</cfif>
<cfelse>
	<span style="color:red">#advid# #FirstName#, #LastName# #GroupName#</span><br>
</cfif>
</cfoutput> --->


<!--- Step 5 process records --->
<!--- <cfquery name="getadvs" datasource="oneaim">
select advid, FirstName, LastName, BU, OU, GroupName, SiteName, BUID, OUID, GroupNumber, LocationID, wasProcessed
FROM            Restruct_Adv_Import
WHERE      wasProcessed = 0 and ouid is not null
order by lastname, firstname
</cfquery>

<cfoutput query="getadvs" group="lastname">
<cfoutput group="firstname">
	<cfquery name="getcurruser" datasource="oneaim">
		select UserId, Useremail, status
		from users
		where firstname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#firstname#"> and lastname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Lastname#">
	</cfquery>
	<cfif getcurruser.recordcount gt 0>
	<cfquery name="deleteassign" datasource="oneaim">
		delete from LocationAdvisors
		where userid =  <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">
	</cfquery>

	<cfoutput>
	
	<cfquery name="geturec" datasource="oneaim">
		select RoleID from UserRoles
		where UserRole = 'HSSE Advisor' and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#"> and userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">
	</cfquery>
	
	
	<cfif geturec.recordcount eq 0>
		<cfquery name="addU" datasource="oneaim">
			insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
			values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'HSSE Advisor',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OUID#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
		</cfquery>
	</cfif>
	<cfif trim(LocationID) neq ''>
	<cfquery name="addassign" datasource="oneaim">
		insert into LocationAdvisors (userid, LocationID)
		values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#LocationID#">)
	</cfquery>
	<cfquery name="flagrec" datasource="oneaim">
		update Restruct_Adv_Import
		set wasprocessed = 1
		where advid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#advid#">
	</cfquery>
	
		#advid# #FirstName#, #LastName# #GroupName# #getcurruser.userid#<br>
	<cfelse>
	<span style="color:red">NOT FOUND #advid# #FirstName#, #LastName# #GroupName#</span><br>
	</cfif>
	</cfoutput>
	<cfelse>
	<span style="color:red">NOT FOUND #advid# #FirstName#, #LastName# #GroupName#</span><br>
	</cfif>
</cfoutput>
</cfoutput> --->