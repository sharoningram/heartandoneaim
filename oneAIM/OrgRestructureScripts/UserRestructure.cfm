<!--- STEP ONE UPDATE BUID LIST--->
<!--- 
<cfquery name="getbuid" datasource="oneaim">
SELECT        userimpid, Firstname, Lastname, Role, BU, OU, groupnumber, wasprocessed, email, BUID, OUID
FROM            Restruct_User_Import
WHERE        (BU IS NOT NULL)
ORDER BY BU
</cfquery>

<cfquery name="getbulist" datasource="oneaim">
SELECT        ID, Name, Parent, Value, isgroupdial, GroupNumber, status, businessline, TrackingYear, ViewOnDashboard, GroupNumList
FROM            NewDials
WHERE        (Parent = 2707) AND (status <> 99)
ORDER BY Name
</cfquery>
<cfset bunamelist = valuelist(getbulist.Name,"^")>
<cfset buliststruct = {}>

<cfloop query="getbulist">
	<cfset buliststruct[#id#] = name>
</cfloop>

<cfdump var="#buliststruct#">

<cfoutput query="getbuid">
	<cfset tidlist = "">
	<cfloop list="#bu#" delimiters="^" index="o">
		<cfif listfind(bunamelist,o,"^") gt 0>
		<cfloop list="#structkeylist(buliststruct)#" index="i">
			<cfif buliststruct[i] eq o>
				<cfset tidlist = listappend(tidlist,i,"^")>
			</cfif>
		</cfloop>
		<cfelse>
			<cfset tidlist = ''>
			<cfbreak>
		</cfif>
	</cfloop>
<cfif trim(tidlist) neq ''>
	<cfquery name="updaterec" datasource="oneaim">
		update Restruct_User_Import
		set BUID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#tidlist#">
		where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
	</cfquery>
</cfif>
<cfif trim(tidlist) eq ''><span style="color:red"></cfif>#userimpid# #bu# <strong>#tidlist#</strong><cfif trim(tidlist) eq ''></span></cfif><br>
</cfoutput> 
 --->
<!--- STEP 2 UPDATE OUID LIST --->
<!--- 

<cfquery name="getouid" datasource="oneaim">
SELECT        userimpid, Firstname, Lastname, Role, BU, OU, groupnumber, wasprocessed, email, BUID, OUID
FROM            Restruct_User_Import
WHERE        (OU IS NOT NULL)  AND (OUID IS NULL)
ORDER BY OU
</cfquery>

<cfquery name="getoulist" datasource="oneaim">
SELECT        ID, Name, Parent, Value, isgroupdial, GroupNumber, status, businessline, TrackingYear, ViewOnDashboard, GroupNumList
FROM            NewDials
WHERE     parent in (select id from newdials where parent = 2707)
and id not in (3230,3228,13380,3240,3231) AND (status <> 99) 
ORDER BY Name
</cfquery>
<cfset ounamelist = valuelist(getoulist.Name,"^")>
<cfset ouliststruct = {}>

<cfloop query="getoulist">
	<cfset ouliststruct[#id#] = name>
</cfloop>

<cfdump var="#ouliststruct#">

<cfoutput query="getouid">
	<cfset oidlist = "">
	<cfloop list="#ou#" delimiters="^" index="o">
		<cfif listfind(ounamelist,o,"^") gt 0>
		<cfloop list="#structkeylist(ouliststruct)#" index="i">
			<cfif ouliststruct[i] eq o>
				<cfset oidlist = listappend(oidlist,i,"^")>
			</cfif>
		</cfloop>
		<cfelse>
			<cfset oidlist = ''>
			<cfbreak> 
		</cfif>
	</cfloop>
<cfif trim(oidlist) neq ''>
	<cfquery name="updaterec" datasource="oneaim">
		update Restruct_User_Import
		set OUID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#oidlist#">
		where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
	</cfquery> 
</cfif>
<cfif trim(oidlist) eq ''><span style="color:red"></cfif>#userimpid# #ou# <strong>#oidlist#</strong><cfif trim(oidlist) eq ''></span></cfif><br>
</cfoutput>
 --->


<!--- STEP 3 Update users --->
<!--- 
<cfquery name="getuserimport" datasource="oneaim">
SELECT   userimpid, Firstname, Lastname, Role, BU, OU, groupnumber, wasprocessed, buid, ouid
FROM            Restruct_User_Import
WHERE        (wasprocessed = 0) AND (Role NOT IN ('Global IT', 'Global Admin'))
order by lastname, firstname
</cfquery> 

<cfoutput query="getuserimport" group="lastname">
<cfoutput group="firstname">
<cfquery name="getcurruser" datasource="oneaim">
select UserId, Useremail, status
from users
where firstname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#firstname#"> and lastname = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Lastname#">
</cfquery>


<!--- delete assignment --->
<cfif getcurruser.recordcount gt 0>
	<cfquery name="deleteassignment" datasource="oneaim">
		delete from UserRoles
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">
	</cfquery>
	<cfquery name="deletegrpassignment" datasource="oneaim">
		delete from GroupUserAssignment
		where userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">
	</cfquery>
	


<cfoutput>

	<cfswitch expression="#trim(role)#">
	
	<cfcase value="BU Admin">
		<cfloop list="#buid#" delimiters="^" index="i">
			<cfquery name="addBA" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'BU Admin',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="BU View Only">
		<cfloop list="#buid#" delimiters="^" index="i">
			<cfquery name="addBVO" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'BU View Only',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="Global Admin">
		
	</cfcase>
	<cfcase value="Global IT">
	
	</cfcase>
	<cfcase value="Man Hour Inputter">
	
		<cfloop list="#ouid#" delimiters="^" index="i">
			<cfquery name="geturec" datasource="oneaim">
				select RoleID from UserRoles
				where UserRole = 'User' and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#"> and userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">
			</cfquery>
			<cfif geturec.recordcount eq 0>
				<cfquery name="addU" datasource="oneaim">
					insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'User',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
				</cfquery>
			</cfif>
		</cfloop>
		<cfloop list="#groupnumber#" delimiters="^" index="g">
			<cfquery name="getgroup" datasource="oneaim">
				SELECT    Group_Number
				FROM            Groups
				WHERE     ('P' + CONVERT(varchar, TrackingDate) + RIGHT('0000' + CAST(Tracking_Num AS VARCHAR(4)), 4) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#g#">)
			</cfquery>
			<cfif getgroup.recordcount eq 1>
				<cfquery name="Addgassign" datasource="oneaim">
					insert into GroupUserAssignment (GroupNumber, UserID, GroupRole, status)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroup.Group_Number#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'manhours',1)
				</cfquery>
			</cfif>
		</cfloop>
		
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery> 
	</cfcase>
	<cfcase value="OU Admin">
		<cfloop list="#ouid#" delimiters="^" index="i">
			<cfquery name="addOA" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'OU Admin',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="OU View Only">
		<cfloop list="#ouid#" delimiters="^" index="i">
			<cfquery name="addOVO" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'OU View Only',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="Reviewer">
		<cfloop list="#ouid#" delimiters="^" index="i">
			<cfquery name="addR" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'Reviewer',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="Senior Executive View">
		<cfquery name="addSEV" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'Senior Executive View',0,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
			<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="Senior Reviewer">
		<cfloop list="#buid#" delimiters="^" index="i">
			<cfquery name="addSR" datasource="oneaim">
				insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
				values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'Senior Reviewer',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
			</cfquery>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>
	<cfcase value="User">
		<cfloop list="#ouid#" delimiters="^" index="i">
			<cfquery name="geturec" datasource="oneaim">
				select RoleID from UserRoles
				where UserRole = 'User' and AssignedLocs = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#"> and userid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">
			</cfquery>
			<cfif geturec.recordcount eq 0>
				<cfquery name="addU" datasource="oneaim">
					insert into UserRoles (UserID, UserRole, AssignedLocs, DateUpdated)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'User',<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">)
				</cfquery>
			</cfif>
		</cfloop>
		<cfloop list="#groupnumber#" delimiters="^" index="g">
			<cfquery name="getgroup" datasource="oneaim">
				SELECT    Group_Number
				FROM            Groups
				WHERE     ('P' + CONVERT(varchar, TrackingDate) + RIGHT('0000' + CAST(Tracking_Num AS VARCHAR(4)), 4) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#g#">)
			</cfquery>
			<cfif getgroup.recordcount eq 1>
				<cfquery name="Addgassign" datasource="oneaim">
					insert into GroupUserAssignment (GroupNumber, UserID, GroupRole, status)
					values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getgroup.Group_Number#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getcurruser.UserId#">,'dataentry',1)
				</cfquery>
			</cfif>
		</cfloop>
		<cfquery name="updateuser" datasource="oneaim">
			update Restruct_User_Import
			set wasprocessed = 1
			where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
		</cfquery>
	</cfcase>

	</cfswitch>
#userimpid#, #Firstname#, #Lastname#, #Role#, #BU#, #OU#, #groupnumber#  #getcurruser.userid# #getcurruser.status#<br> 


</cfoutput>
<cfelse>
<cfoutput>
	<cfquery name="updateuser" datasource="oneaim">
	update Restruct_User_Import
	set wasprocessed = 99
	where userimpid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userimpid#">
	</cfquery>
<span style="color:red;">#userimpid#, #Firstname#, #Lastname#, #Role#, #BU#, #OU#, #groupnumber# #getcurruser.recordcount# | #getcurruser.status#</span><br>
</cfoutput>
</cfif>

</cfoutput>
</cfoutput> --->