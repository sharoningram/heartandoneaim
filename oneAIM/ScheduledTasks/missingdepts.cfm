<cfquery name="getneededdepts" datasource="#oneaimdsn#">
SELECT DISTINCT Dept_home
FROM            LaborHoursOffice
WHERE       already_grouped = 0
and dept_home not in (SELECT DISTINCT Dept_Home
FROM         officeLabormap)
</cfquery>

<cfset autoassigndept = "">

<cfoutput query="getneededdepts">
	<cfquery name="getassignedalready" datasource="#oneaimdsn#">
		select distinct group_number,Office_Group, LocationID
		from OfficeLaborMap
		where dept_home like '#left(dept_home,3)#%'
		ORDER BY Group_Number DESC
	</cfquery>
#dept_home# #getassignedalready.recordcount#<br>
	<cfif getassignedalready.recordcount gt 0>
		<cfquery name="addjoin" datasource="#oneaimdsn#">
			insert into OfficeLaborMap (Group_Number, Office_Group, Dept_Home, Description,<cfif trim(getassignedalready.LocationID) neq ''> LocationID,</cfif> wasprocessed)
			values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getassignedalready.group_number#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getassignedalready.Office_Group#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getneededdepts.dept_home#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getneededdepts.dept_home#">,<cfif trim(getassignedalready.LocationID) neq ''><cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getassignedalready.LocationID#">,</cfif>1)
		</cfquery>
		<cfquery name="thisQuery" datasource="#oneaimdsn#">
			UPDATE LaborHoursOffice SET ALREADY_GROUPED = 1 WHERE dept_home = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getneededdepts.dept_home#">
		</cfquery> 
		<cfset autoassigndept = listappend(autoassigndept,"#trim(getneededdepts.dept_home)# assigned to #getassignedalready.Office_Group#")>
	
	</cfif>
</cfoutput>






<cfquery name="getassigned" datasource="#oneaimdsn#">
SELECT DISTINCT Dept_Home
FROM         officeLabormap
order by dept_home
</cfquery>
<cfset deptlist = "">

<cfloop query="getassigned">
	
	<cfset deptlist =  listappend(deptlist,trim(dept_home))>
</cfloop>

<cfquery name="getdata" datasource="#laborsafetydsn#"><!---QUERYREPORT:PASSED--->
	select distinct dept_home
	from vw_safety_labor_oh
	where year(week_ending_date) = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#year(now())#">
	order by dept_home
</cfquery><br><br>



 <cfif getdata.recordcount gt 0>
	<cfset needed = "">
	<cfloop query="getdata">
		<cfset needed =  listappend(needed,"#trim(dept_home)#")>
	</cfloop>

<cfoutput>
<cfset needtoadd = "">
<cfloop list="#needed#" index="i">
	
	<cfif listfind(deptlist,i) eq 0>
		<cfset needtoadd = listappend(needtoadd,i)>
		</cfif>
</cfloop>
(#needtoadd#)<br>
</cfoutput>
	
	<cfloop list="#needtoadd#" index="i">
	
	<cfoutput>
	<cfquery name="getassignedalready" datasource="#oneaimdsn#">
		select distinct group_number,Office_Group, LocationID
		from OfficeLaborMap
		where dept_home like '#left(i,3)#%'
		ORDER BY Group_Number DESC
	</cfquery>
#i# #getassignedalready.recordcount# #getassignedalready.group_number# #getassignedalready.Office_Group# #getassignedalready.LocationID#<br>
	<cfif getassignedalready.recordcount gt 0>
		<cfquery name="addjoin" datasource="#oneaimdsn#">
			insert into OfficeLaborMap (Group_Number, Office_Group, Dept_Home, Description, <cfif trim(getassignedalready.LocationID) neq ''>LocationID,</cfif> wasprocessed)
			values (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getassignedalready.group_number#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getassignedalready.Office_Group#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#">,<cfif trim(getassignedalready.LocationID) neq ''><cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#getassignedalready.LocationID#">,</cfif>1)
		</cfquery>
		<cfquery name="thisQuery" datasource="#oneaimdsn#">
			UPDATE LaborHoursOffice SET ALREADY_GROUPED = 1 WHERE dept_home = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#getneededdepts.dept_home#">
		</cfquery> 
		<cfset autoassigndept = listappend(autoassigndept,"#trim(i)# assigned to #getassignedalready.Office_Group#")>
	
	</cfif>
</cfoutput>
	
	
	</cfloop>
	
	
	
	
	
</cfif><br><br><br>
<strong><cfoutput>#autoassigndept#
	<cfmail to="#erroremail#" from="#fromemail#" subject="Amec-FW oneAIM Missing Departments" type="HTML">
		The following departments need to be added to the office labor map table:<br><br>
		<cfif listlen(needtoadd) neq 0>
		<cfloop list="#needtoadd#" index="o">
		#o#<br>
		</cfloop>
		<cfelse>
		None
		</cfif>
		<cfif listlen(autoassigndept) neq 0>
	<br><br>	
		The following departments have been added automatically to the office labor map table:<br><br>
		
		<cfloop list="#autoassigndept#" index="o">
		#o#<br>
		</cfloop>
	</cfif>
	</cfmail></cfoutput></strong>
