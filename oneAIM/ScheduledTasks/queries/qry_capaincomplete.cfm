<cfquery name="getincompletecapa" datasource="#oneaimdsn#">
SELECT        oneAIMCAPA.CAPAid, oneAIMCAPA.CAPAtype, NewDials.Name AS ouname, oneAIMCAPA.DueDate, oneAIMCAPA.AssignedTo, oneAIMCAPA.ActionDetail, 
                         oneAIMCAPA.EnteredBy, oneAIMCAPA.DateComplete, oneAIMincidents.IRN, Groups.Group_Name,oneAIMincidents.TrackingNum,oneAIMCAPA.DateEntered as capadate
FROM            oneAIMCAPA INNER JOIN
                         oneAIMincidents ON oneAIMCAPA.IRN = oneAIMincidents.IRN INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID
WHERE    oneAIMCAPA.status = 'open' and   (oneAIMCAPA.DueDate >= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#now()#">) AND (oneAIMCAPA.DateComplete IS NULL)  and oneAIMincidents.withdrawnto is null

	AND   (oneAIMCAPA.EnteredBy = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">)

ORDER BY oneAIMCAPA.CAPAtype
</cfquery>
<cfif getincompletecapa.recordcount gt 0>
<cfset hastasks = "yes">
</cfif>