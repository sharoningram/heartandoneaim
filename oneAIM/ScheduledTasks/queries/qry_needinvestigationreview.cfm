<cfquery name="needinvestingationreview" datasource="#oneaimdsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         IncidentInvestigation.Status, IncidentInvestigation.dateentered AS datecreated, MAX(IncidentAudits.CompletedDate) AS compdate, 
                         oneAIMincidents.InvestigationLevel, IncidentTypes.IncType AS sectype
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentInvestigation ON oneAIMincidents.IRN = IncidentInvestigation.IRN LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE      (oneAIMincidents.isWorkRelated = 'yes')  and oneAIMincidents.withdrawnto is null AND (oneAIMincidents.Status IN ('closed', 
                         'Review Completed')) AND (IncidentInvestigation.Status = 'Sent for Review') AND (IncidentAudits.Status = 'Investigation Sent for Review')

	<cfif listfindnocase(userlevels,"Reviewer") gt 0 or  listfindnocase(userlevels,"Senior Reviewer") gt 0> 
		<cfif listfindnocase(userlevels,"Reviewer") gt 0>
			AND (oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">) and oneAIMincidents.InvestigationLevel = 1
		</cfif>
		<cfif listfindnocase(userlevels,"Senior Reviewer") gt 0>
			and (Groups.business_line_id IN (<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#userBUs#" list="Yes">))  and oneAIMincidents.InvestigationLevel = 2
		</cfif>
		
	<cfelse>
		and 1 = 0
	</cfif>

GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         IncidentInvestigation.Status, IncidentInvestigation.dateentered, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.TrackingNum
</cfquery>
<cfif needinvestingationreview.recordcount gt 0>
<cfset hastasks = "yes">
</cfif>