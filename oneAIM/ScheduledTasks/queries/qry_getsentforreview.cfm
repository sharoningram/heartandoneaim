
<cfquery name="getsentforreview" datasource="#oneaimdsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes_1.IncType, Groups.OUid, oneAIMincidents.dateCreated, MAX(IncidentAudits.CompletedDate) AS compdate, oneAIMincidents.InvestigationLevel, 
                         IncidentTypes.IncType AS sectype
FROM            IncidentAudits INNER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentAudits.IRN = oneAIMincidents.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    oneAIMincidents.isfatality <> 'Yes'  and oneAIMincidents.withdrawnto is null and    (oneAIMincidents.Status IN ('sent for review'))  AND  (IncidentAudits.Status = 'Sent for Review')

			AND oneAIMincidents.reviewerEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">
 		
GROUP BY oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes_1.IncType, Groups.OUid, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.TrackingNum
</cfquery>

<Cfif getsentforreview.recordcount gt 0>
<Cfset hastasks = "yes">
</cfif>