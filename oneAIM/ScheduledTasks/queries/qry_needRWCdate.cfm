<cfquery name="needRWCdate" datasource="#oneaimdsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, oneAIMincidents.PotentialRating, Groups.Group_Name, NewDials.Name AS ouname, 
                         oneAIMInjuryOI.RWCFirstDate, oneAIMInjuryOI.RWCDateReturned, oneAIMincidents.incidentDate, oneAIMincidents.ShortDesc
FROM            oneAIMincidents INNER JOIN
                         oneAIMInjuryOI ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID
WHERE      oneAIMincidents.status <> 'Cancel'  and oneAIMincidents.withdrawnto is null and   (oneAIMInjuryOI.OSHAclass = 4)  AND (oneAIMInjuryOI.RWCDateReturned IS NULL) and oneAIMInjuryOI.RWCFirstDate is not null AND
                       ((oneAIMincidents.PrimaryType IN (1, 5))  or   (oneAIMincidents.SecondaryType IN (1, 5)))

	AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">) 

ORDER BY oneAIMincidents.TrackingNum
</cfquery>
<cfif needRWCdate.recordcount gt 0>
<cfset hastasks = "yes">
</cfif>