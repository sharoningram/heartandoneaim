<cfquery name="getpendingsfr" datasource="#oneaimdsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes_1.IncType AS sectype
FROM            IncidentTypes AS IncidentTypes_1 RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentTypes_1.IncTypeID = oneAIMincidents.SecondaryType LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE       (oneAIMincidents.Status in ('started','initiated','withdrawn to initiator')) and oneAIMincidents.withdrawnto is null

 AND (oneAIMincidents.createdbyEmail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">)
 	<cfif listfindnocase(userlevels,"Global Admin") eq 0 and  listfindnocase(userlevels,"BU Admin") eq 0>
		and isfatality = 'No'
	</cfif>

ORDER BY oneAIMincidents.isFatality, oneAIMincidents.TrackingNum
</cfquery>

<cfif getpendingsfr.recordcount gt 0>
<cfset hastasks = "yes">
</cfif>