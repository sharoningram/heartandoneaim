<cfquery name="getwithdrawn" datasource="#oneaimdsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.isFatality, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, 
                         OSHACategories.Category, oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, 
                         IncidentTypes.IncType, oneAIMincidents.dateCreated, oneAIMincidents.InvestigationLevel, IncidentTypes_1.IncType AS sectype, oneAIMincidents.withdrawnto, 
                         oneAIMincidents.withdrawndate
FROM            IncidentTypes AS IncidentTypes_1 RIGHT OUTER JOIN
                         oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID ON IncidentTypes_1.IncTypeID = oneAIMincidents.SecondaryType LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.PrimaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE     1 = 1

   and (oneAIMincidents.withdrawnto = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#UserID#">)

ORDER BY oneAIMincidents.TrackingNum
</cfquery>
<cfif getwithdrawn.recordcount gt 0>
<cfset hastasks = "yes">
</cfif>