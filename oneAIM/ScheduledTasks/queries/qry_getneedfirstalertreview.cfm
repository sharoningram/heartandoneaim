
<cfquery name="getneedfirstalertreview" datasource="#oneaimdsn#">
SELECT        oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name AS ouname, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         oneAimFirstAlerts.dateentered, MAX(IncidentAudits.CompletedDate) AS compdate, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType AS sectype
FROM            oneAIMincidents INNER JOIN
                         Groups ON oneAIMincidents.GroupNumber = Groups.Group_Number INNER JOIN
                         NewDials ON Groups.OUid = NewDials.ID INNER JOIN
                         oneAimFirstAlerts ON oneAIMincidents.IRN = oneAimFirstAlerts.IRN INNER JOIN
                         IncidentAudits ON oneAIMincidents.IRN = IncidentAudits.IRN LEFT OUTER JOIN
                         IncidentTypes ON oneAIMincidents.SecondaryType = IncidentTypes.IncTypeID LEFT OUTER JOIN
                         IncidentTypes AS IncidentTypes_1 ON oneAIMincidents.PrimaryType = IncidentTypes_1.IncTypeID LEFT OUTER JOIN
                         OSHACategories INNER JOIN
                         oneAIMInjuryOI ON OSHACategories.CatID = oneAIMInjuryOI.OSHAclass ON oneAIMincidents.IRN = oneAIMInjuryOI.IRN
WHERE    (oneAIMincidents.needFA = 1)  and   oneAimFirstAlerts.Status = 'Sent for Review'  and oneAIMincidents.withdrawnto is null AND (IncidentAudits.Status = 'First Alert Sent for Review')

	<cfif listfindnocase(userlevels,"Reviewer") gt 0>
		AND   (oneAIMincidents.revieweremail = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#useremail#">)
		
	<cfelse>
		and 1 = 2
	</cfif>

GROUP BY oneAIMincidents.IRN, oneAIMincidents.TrackingNum, NewDials.Name, Groups.Group_Name, oneAIMincidents.incidentDate, OSHACategories.Category, 
                         oneAIMincidents.isNearMiss, oneAIMincidents.PotentialRating, oneAIMincidents.ShortDesc, oneAIMincidents.PrimaryType, IncidentTypes_1.IncType, 
                         oneAimFirstAlerts.dateentered, oneAIMincidents.InvestigationLevel, IncidentTypes.IncType
ORDER BY oneAIMincidents.TrackingNum
</cfquery>
<cfif getneedfirstalertreview.recordcount gt 0>
<cfset hastasks = "yes">
</cfif>