<cfset end_date = DateFormat(CreateDate(Year(now()),Month(now()), Day(now())), "mm/dd/yy")>
<cfset begin_date = DateFormat(DateAdd('m', -1, end_date), "mm/dd/yy")> 

<cfquery name="getdata" datasource="#laborsafetydsn#">
	select  Dept_Home, OH_Hours, Week_Ending_Date
	from vw_safety_labor_oh
	where  left(dept_home,3) = '434' and week_ending_date between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#begin_date#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#end_date#">
	and year(week_ending_date) >= 2016
</cfquery>

<cfloop query="getdata">
	
	<cfquery name="selrecs" datasource="#oneaimdsn#">
		Select  Dept_home, WEEK_ENDING_DATE 
		from laborhoursoffice 
		where week_ending_date = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#week_ending_date#"> and Dept_home = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Dept_home#">
	</cfquery>
	
	<cfif selrecs.RecordCount eq 0>
	
		<cfquery name="ins_new_data" datasource="#oneaimdsn#">
			Insert into laborhoursoffice 
			(Dept_home,OH_HOURS,Week_Ending_Date,JDE,DATE_FRM_JDE,Already_Grouped)
				values
			(
			<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Dept_home#">,<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#OH_HOURS#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#Week_Ending_Date#">,'yes',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,0
			)
		</cfquery>
	
	<cfelse>
	
		<cfquery name="upd_data" datasource="#oneaimdsn#">
			Update laborhoursoffice
			set
			OH_HOURS=<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#OH_HOURS#">,
			DATE_FRM_JDE=<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">
			where
			Dept_home=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Dept_home#">
			and Week_Ending_Date=<cfqueryparam cfsqltype="CF_SQL_DATE" value="#week_ending_date#">
		</cfquery>
	
	</cfif>

</cfloop>
