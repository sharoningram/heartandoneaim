<cfsetting requesttimeout="4800" enablecfoutputonly="No">
<cfset end_date = DateFormat(CreateDate(Year(now()),Month(now()), Day(now())), "mm/dd/yy")>
<cfset begin_date = DateFormat(DateAdd('m', -1, end_date), "mm/dd/yy")>
<cfset insertdate = datepart("yyyy",Now())> 

<cfquery name="getdata" datasource="#laborsafetydsn#">
	select project_num, project_desc, week_ending_date, ho, field, craft, company_num, division
	from vw_safety_labor_1
	where  left(project_num,3) = '434' and week_ending_date between <cfqueryparam cfsqltype="CF_SQL_DATE" value="#begin_date#"> and <cfqueryparam cfsqltype="CF_SQL_DATE" value="#end_date#">
	and year(week_ending_date) >= 2016
</cfquery>
<cfset pnumlist = "">
<cfloop query="getdata">
	<cfif listfind(pnumlist,project_num) eq 0>
		<cfset pnumlist = listappend(pnumlist,project_num)>
	</cfif>
	<cfquery name="selrecs" datasource="#oneaimdsn#">
		Select project_number, HO_HRS, FIELD_HRS, CRAFT_HRS,  project_desc 
		from laborhours 
		where week_ending_date = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#week_ending_date#"> and Project_number = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#PROJECT_Num#">
	</cfquery>
	<cfif selrecs.recordcount eq 0>
		<cfquery name="ins_new_data" datasource="#oneaimdsn#">
			Insert into laborhours
			(PROJECT_NUMBER,PROJECT_DESC,Week_Ending_Date,HO_HRS,FIELD_HRS,CRAFT_HRS,JDE,DATE_FRM_JDE,Already_Grouped,division,company_num)
			values
			(
			<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#PROJECT_Num#">,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#PROJECT_DESC#">,<cfqueryparam cfsqltype="CF_SQL_DATE" value="#Week_Ending_Date#">,<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#HO#">,<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#FIELD#">,<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#CRAFT#">,'yes',<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,0,<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#division#">,<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#company_num#">
			)
		</cfquery>
	
	<cfelse>
		<cfset doupdate = "no">
		<cfif trim(selrecs.HO_HRS) neq trim(HO)>
			<cfset doupdate = "yes">
		</cfif>
		<cfif trim(selrecs.FIELD_HRS) neq trim(FIELD)>
			<cfset doupdate = "yes">
		</cfif>
		<cfif trim(selrecs.CRAFT_HRS) neq trim(CRAFT)>
			<cfset doupdate = "yes">
		</cfif>
		<cfif doupdate> 
		<cfquery name="upd_data" datasource="#oneaimdsn#">
			Update laborhours
			set
			HO_HRS=<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#HO#">,
			FIELD_HRS=<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#FIELD#">,
			CRAFT_HRS=<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#CRAFT#">,
			DATE_FRM_JDE=<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#now()#">,
			division=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#division#">,
			company_num=<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#company_num#">
			where
			PROJECT_NUMBER=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#PROJECT_Num#">
			and Week_Ending_Date= <cfqueryparam cfsqltype="CF_SQL_DATE" value="#week_ending_date#">
		</cfquery> 
		</cfif>
	
	
	</cfif>
	
	
		
	
</cfloop>

<cfloop list="#pnumlist#" index="i">
	<cfquery name="chkrecs" datasource="#oneaimdsn#">
			Select Project_Number from ProjectsbyGroup 
			where Project_number = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#"> 
		</cfquery>
		
	<cfif chkrecs.RecordCount neq 0>
		<cfquery name="updrecs" datasource="#oneaimdsn#" timeout="1200">
			Update laborhours
			set 
			Already_Grouped=1
			where Project_number = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#i#"> and already_grouped <> 1
		</cfquery> 
	</cfif>
</cfloop>


