<cfset reportsDir = trim(replacenocase(getcurrenttemplatepath(), "scheduledtasks\deletetempfiles.cfm", "reports\exports"))>
<cfset pdfDir = trim(replacenocase(getcurrenttemplatepath(), "scheduledtasks\deletetempfiles.cfm", "reports\displays\pdfgen"))>
<cfset incpdfdir = trim(replacenocase(getcurrenttemplatepath(), "scheduledtasks\deletetempfiles.cfm", "incidents\actions\pdfgen"))>
<cfset heartreportsDir = trim(replacenocase(getcurrenttemplatepath(), "#oneaimroot#\scheduledtasks\deletetempfiles.cfm", "#heartroot#\reports\exports"))>

<cfdirectory action="LIST" directory="#reportsDir#" name="repqry">

<cfloop query="repqry">
 <cftry>
		<cffile action="DELETE" file="#reportsDir#\#name#">
	<cfcatch>f</cfcatch>
</cftry>
</cfloop>


<cfdirectory action="LIST" directory="#pdfDir#" name="pdfqry">

<cfloop query="pdfqry">
<cftry>
		<cffile action="DELETE" file="#pdfDir#\#name#">
	<cfcatch>f</cfcatch>
</cftry>
</cfloop>

<cfdirectory action="LIST" directory="#incpdfdir#" name="incpdyqry">

<cfloop query="incpdyqry">
<cftry>
		<cffile action="DELETE" file="#incpdfdir#\#name#">
	<cfcatch>f</cfcatch>
</cftry>
</cfloop>


<cfdirectory action="LIST" directory="#heartreportsDir#" name="hrepqry">

<cfloop query="hrepqry">
 <cftry>
		<cffile action="DELETE" file="#heartreportsDir#\#name#">
	<cfcatch>f</cfcatch>
</cftry>
</cfloop>