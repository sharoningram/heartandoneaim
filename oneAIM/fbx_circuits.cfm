<!---
<fusedoc 
	fuse="FBX_Circuits.cfm" 
	language="ColdFusion" 
	version="2.0">
	<responsibilities>
		I define the Circuits structure used with Fusebox 3.0
	</responsibilities>	
	<io>
		<in>
			<structure name="fusebox.circuits">
			</structure>
		</in>
		<out>
			<string 
				name="fusebox.circuits.*" 
				comments="each circuit must have an entry into fusebox.circuits"> 
		</out>
	</io>
</fusedoc>
--->

<!--- this file contains all the circuit definitions for the fusebox --->

<cfinclude template="appconfig.cfm">

<cfset fusebox.Circuits.home="#oneaimroot#">
<cfset fusebox.Circuits.main="#oneaimroot#/main">
<cfset fusebox.Circuits.incidents="#oneaimroot#/incidents">
<cfset fusebox.Circuits.groups="#oneaimroot#/groups">
<cfset fusebox.Circuits.distribution="#oneaimroot#/distribution">
<cfset fusebox.Circuits.manhours="#oneaimroot#/manhours">
<cfset fusebox.Circuits.reports="#oneaimroot#/reports">
<cfset fusebox.Circuits.users="#oneaimroot#/users">
<cfset fusebox.Circuits.adminfunctions="#oneaimroot#/adminfunctions">

